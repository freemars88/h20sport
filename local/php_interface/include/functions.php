<?

function getCanonicalLink($link = false)
{
    global $APPLICATION;
    if (!$link)
        $link = $APPLICATION->GetCurDir();

    $arr = include $_SERVER["DOCUMENT_ROOT"] . "/include/canlinks.php";
    return $_SERVER["REQUEST_SCHEME"] . '://' . SITE_SERVER_NAME . $arr[$link];
}

function getPreviewPdf($fileName)
{
    //$im = new imagick($_SERVER['DOCUMENT_ROOT'] . $fileName . '[0]');
    /* Преобразуем в PNG */
    //$im->setImageFormat("png");
    /* выводим в бразер */
    //header("Content-Type: image/png");
    //echo $im->thumbnailImage(300, 600);


    $inputFile = $_SERVER['DOCUMENT_ROOT'] . $fileName;
    $outputFile = $inputFile . '.jpg';

    if (!file_exists($outputFile)) {
        exec('gs -dSAFER -dBATCH -sDEVICE=jpeg -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r300 -sOutputFile=' . $outputFile . ' ' . $inputFile);
    }

    return $outputFile;
}

?>