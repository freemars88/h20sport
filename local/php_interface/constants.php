<?

/**
 * Константы для сайта
 */
define("IBLOCK_CATALOG_1C", 1);
define("IBLOCK_CATALOG_1C_OFFERS", 2);
define("IBLOCK_CATALOG", 16);
define("IBLOCK_CATALOG_OFFERS", 17);
define("IBLOCK_OLD_CATALOG", 14);
define("IBLOCK_OLD_CATALOG_OFFERS", 15);
define("IBLOCK_SET_ALL", 36);
define("IBLOCK_BRANDS", 20);
/*
  define("IBLOCK_REFERENCE_SIZE", 5);
  define("IBLOCK_REFERENCE_COLOR", 6);
  define("IBLOCK_REFERENCE_COLLECTION", 7);
  define("IBLOCK_REFERENCE_MATERIALS", 8);
  define("IBLOCK_REFERENCE_BRAND", 9);
  define("IBLOCK_REFERENCE_COUNTRY", 10);
  define("IBLOCK_REFERENCE_SIZE_TABLE", 19);
 */
define("HL_NOMENKLATURA_ENTITY_ID", 5);
define("H2OSPORT_1C_PRICE_ID", 3);
define("H2OSPORT_1C_OLD_PRICE_ID", 0);
define("H2OSPORT_BASE_PRICE_ID", 1);
define("H2OSPORT_VAT_ID", 1);

define("DALLI_CARD_PAY_SYSTEM_ID", 3);
define("DELIVERY_DALLI1_ID", 2);
define("DELIVERY_DALLI2_ID", 3);
define("DELIVERY_DALLI3_ID", 4);
define("DELIVERY_DALLI4_ID", 5);
define("DELIVERY_DALLI5_ID", 6);
define("DELIVERY_DALLI6_ID", 7);
define("DELIVERY_DALLI7_ID", 8);
define("DELIVERY_DALLI8_ID", 9);


define("DELIVERY_DALLI_PVZ_DALLI_ID", 4);
define("DELIVERY_DALLI_PVZ_BOXBERRY_ID", 8);
define("DELIVERY_DALLI_PVZ_SDEK_ID", 5);
define("DELIVERY_DALLI_PVZ_PICKUP_ID", 9);
define("DELIVERY_DALLI_COURIER_MSK_ID", 3);
define("DELIVERY_DALLI_COURIER_MO_ID", 3);
define("DELIVERY_DALLI_COURIER_SPB_ID", 7);
define("DELIVERY_DALLI_COURIER_SDEK_ID", 6);

define("DELIVERY_PICKUP_ID", 12);
/*
  define("HL_1CLINKS_ID", 1);

  define("DEFAULT_CATEGORY_ID", 527);
 */
define("ORDER_PROP_FIZ_FIO", 1);
define("ORDER_PROP_FIZ_EMAIL", 2);
define("ORDER_PROP_FIZ_PHONE", 3);
define("ORDER_PROP_FIZ_ZIP", 4);
define("ORDER_PROP_FIZ_CITY", 5);
define("ORDER_PROP_FIZ_LOCATION", 6);
define("ORDER_PROP_FIZ_ADDRESS", 7);
define("ORDER_PROP_FIZ_STREET", 20);
define("ORDER_PROP_FIZ_HOUSE", 21);
define("ORDER_PROP_FIZ_FLAT", 22);

define("ORDER_PERSON_TYPE_FIZ", 1);

define("KLADR_API_TOKEN", "5a098a290a69de374a8b4569");

define("IBLOCK_CATALOG_PROPERTY_INFO_IN_SKLAD", 45);
define("OLD_PRICE_ID", 4);
/*
  define("GLOBAL_PARTNER_USER", 9);


  define("DALLI_STATUS_NEW", "AA");
  define("DALLI_STATUS_ACCEPTED", "AB");
  define("DALLI_STATUS_INVENTORY", "AC");
  define("DALLI_STATUS_DEPARTURING", "AD");
  define("DALLI_STATUS_DEPARTURE", "AE");
  define("DALLI_STATUS_DELIVERY", "AF");
  define("DALLI_STATUS_COURIERDELIVERED", "AG");
  define("DALLI_STATUS_COMPLETE", "AH");
  define("DALLI_STATUS_PARTIALLY", "AI");
  define("DALLI_STATUS_COURIERRETURN", "AJ");
  define("DALLI_STATUS_CANCELED", "AK");
  define("DALLI_STATUS_RETURNING", "AL");
  define("DALLI_STATUS_RETURNED", "AM");
  define("DALLI_STATUS_CONFIRM", "AN");
  define("DALLI_STATUS_DATECHANGE", "AO");
  define("DALLI_STATUS_NEWPICKUP", "AP");
  define("DALLI_STATUS_UNCONFIRM", "AQ");
  define("DALLI_STATUS_PICKUPREADY", "AR");

  define("FREE_DELIVERY_MSK_SUM", 10000);

  define("IBLOCK_ID_FILTER_DATA", 17);
  define("IBLOCK_ID_FILTER_URL", 18);

  define("PAYSYSTEM_SBERBANK_PRE_ID", 14);
  define("PAYSYSTEM_SBERBANK_ID", 13);
  define("FR_SITE_ID", "s1");

  define("DELIVERY_ICOURIER1_ID", 37);
  define("DELIVERY_ICOURIER2_ID", 38);

  define("STORE_ID_IGNORED", 4);
 */
define("NOREG_USERID", 1894);

define("BANNER_TYPE_VALUE_MAINSLIDER", 76);
define("BANNER_TYPE_VALUE_PRODUCT1", 77);
define("BANNER_TYPE_VALUE_PRODUCT2", 78);
define("BANNER_TYPE_VALUE_DISCOUNT", 79);

define("BANNER_TYPE_VALUE_CATALOG_LEFT", 80);
define("BANNER_TYPE_VALUE_CATALOG_TOP", 81);
define("BANNER_TYPE_VALUE_PAGE_TOP", 82);

define("DALLI_STATUS_NEW", "AA");
define("DALLI_STATUS_ACCEPTED", "AB");
define("DALLI_STATUS_INVENTORY", "AC");
define("DALLI_STATUS_DEPARTURING", "AD");
define("DALLI_STATUS_DEPARTURE", "AE");
define("DALLI_STATUS_DELIVERY", "AF");
define("DALLI_STATUS_COURIERDELIVERED", "AG");
define("DALLI_STATUS_COMPLETE", "AH");
define("DALLI_STATUS_PARTIALLY", "AI");
define("DALLI_STATUS_COURIERRETURN", "AJ");
define("DALLI_STATUS_CANCELED", "AK");
define("DALLI_STATUS_RETURNING", "AL");
define("DALLI_STATUS_RETURNED", "AM");
define("DALLI_STATUS_CONFIRM", "AN");
define("DALLI_STATUS_DATECHANGE", "AO");
define("DALLI_STATUS_NEWPICKUP", "AP");
define("DALLI_STATUS_UNCONFIRM", "AQ");
define("DALLI_STATUS_PICKUPREADY", "AR");

define("SALE_SECTION_ID", 5598);

define("PAYSYSTEM_CARD_IN_SITE_ID", 4);
define("NEW_COLLECTION_YEAR", 110795);

define("STORE_BASE_ALLOW", serialize(array(1,2,3,4,5)));