<?
require_once('include/debug.php');
require_once('include/functions.php');

CModule::IncludeModule("webavk.francesco");
CModule::IncludeModule("webavk.euroset");

function myPrint(&$Var, $bIsHtmlSpecialChars = true, $strFileName = false)
{
	if (defined("WEBAVK_CRON_UNIQ_IDENT") && $strFileName === false)
	{
		$bIsHtmlSpecialChars = false;
	}
	if ($strFileName)
	{
		ob_start();
	}
	echo '<pre style="text-align:left;background-color:#222222;color:#ffffff;font-size:11px;">';
	if ($bIsHtmlSpecialChars)
	{
		echo htmlspecialchars(print_r($Var, true));
	} else
	{
		print_r($Var);
	}
	echo '</pre>';
	if ($strFileName)
	{
		$c = ob_get_contents();
		ob_end_clean();
		file_put_contents($strFileName, $c);
	}
}

function mailPrint(&$Var)
{
	global $MAIL_PRINT_NUM;
	$MAIL_PRINT_NUM++;
	mail("alexmsk2006@gmail.com", "mail print", "[" . $MAIL_PRINT_NUM . "]\n\n" . var_export($Var, true));
}

include_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/constants.php");
include_once(dirname(__FILE__) . "/event.handler/include.php");

if ($_REQUEST['AUTH_FORM'] == "Y" && $_REQUEST['TYPE'] == "REGISTRATION")
{
	$_REQUEST['USER_LOGIN'] = $_REQUEST['USER_EMAIL'];
	$_POST['USER_LOGIN'] = $_REQUEST['USER_EMAIL'];
	$GLOBALS['USER_LOGIN'] = $_REQUEST['USER_EMAIL'];
}

$iphone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
if ($iphone)
{
	define("UA_IPHONE", true);
} else
{
	define("UA_IPHONE", false);
}
if ($android)
{
	define("UA_ANDROID", true);
} else
{
	define("UA_ANDROID", false);
}
if ($iphone || $android)
{
	define("UA_MOBILE", true);
} else
{
	define("UA_MOBILE", false);
}

function GetPadez($count, $nominative, $genitive_singular, $genitive_plural)
{
	$result = "";
	$last_digit = $count % 10;
	$last_two_digits = $count % 100;

	if (
			($last_digit == 1) &&
			($last_two_digits != 11)
	)
	{
		$result = $nominative;
	} else if
	(
			(($last_digit == 2) && ($last_two_digits != 12)) ||
			(($last_digit == 3) && ($last_two_digits != 13)) ||
			(($last_digit == 4) && ($last_two_digits != 14))
	)
	{
		$result = $genitive_singular;
	} else
	{
		$result = $genitive_plural;
	}

	return $result;
}

function doLoadAscaronSettings($strName)
{
	global $USER_FIELD_MANAGER;
	$obCache = new CPHPCache();
	if ($obCache->InitCache(3600 * 24, "AscaronSettings", "/"))
	{
		$arValues = $obCache->GetVars();
	} elseif ($obCache->StartDataCache())
	{
		$arUserFields = $USER_FIELD_MANAGER->GetUserFields("ASKARON_SETTINGS", 1, LANGUAGE_ID);
		$arValues = array();
		foreach ($arUserFields as $k => $v)
		{
			if (in_array($k, array("UF_BESSINI_STORES", "UF_UT_STORES", "UF_STORE_FOR_SUMM")))
			{
				$tmp = explode(",", $v['VALUE']);
				foreach ($tmp as $v1)
				{
					$v1 = trim($v1);
					if (strlen($v1) > 0)
					{
						$arValues[$k][] = $v1;
					}
				}
			} else
			{
				$arValues[$k] = $v['VALUE'];
			}
		}
		if (in_array(strtolower($_SERVER['HTTP_HOST']), array("bessini.web-avk.ru")))
		{
			$arValues['UF_YANDES_METRIKA'] = $arValues['UF_YANDES_METRIKA_D'];
			$arValues['UF_YANDEX_COUNTER'] = $arValues['UF_YANDEX_COUNTER_D'];
		}
		$obCache->EndDataCache($arValues);
	}
	return $arValues[$strName];
}

function CheckPhoneNumber($phone)
{
	$result = true;
	if (!preg_match("/^[0-9]{11,14}+$/", $phone))
	{
		$result = false;
	}
	return $result;
}

function MakePhoneNumber($phone)
{
	$result = preg_match_all('/\d/', $phone, $found);
	$res = implode('', $found[0]);
	if (($found[0][0] == '7' || $found[0][0] == '8') && strlen($res) >= '11' && $found[0][1] != 0)
	{
		$phone = '8' . substr($res, 1, 10);
	} elseif (($found[0][0] . $found[0][1] == '80') && strlen($res) >= '11')
	{
		$phone = '38' . substr($res, 1, 10);
	} elseif (($found[0][0] . $found[0][1] . $found[0][2] == '380') && strlen($res) >= '12')
	{
		$phone = '380' . substr($res, 3, 9);
	} elseif (($found[0][0] . $found[0][1] . $found[0][2] == '375') && strlen($res) >= '12')
	{
		$phone = '375' . substr($res, 3, 9);
	} elseif (strlen($res) == '10' && $res{0} == 0)
	{
		$phone = '38' . $res;
	} elseif (strlen($res) == '9')
	{
		$phone = '375' . $res;
	} elseif (strlen($res) == '10')
	{
		$phone = '8' . $res;
	} elseif (strlen($res) == '14')
	{
		$phone = $res;
	} else
	{
		$phone = '';
	}
	return $phone;
}

function doMakePhoneNumbersLinkForWeb($bForMobile, $arValues)
{
	$arResult = array();
	if (!is_array($arValues))
	{
		$arValues = array($arValues);
	}

	foreach ($arValues as $k => $v)
	{
		$v = trim($v);
		if (strlen($v) > 0)
		{
			$strRes = '';
			if ($bForMobile)
			{
				if (UA_ANDROID === true)
				{
					$strRes .= 'tel:' . MakePhoneNumber($v);
				} else
				{
					$strRes .= 'tel://' . MakePhoneNumber($v);
				}
			} else
			{
				$strRes .= $v;
			}
			$arResult[] = $strRes;
		}
	}
	return implode(", ", $arResult);
}

function doMakePhoneNumbersForWeb($bForMobile, $arValues, $arDescriptions)
{
	$arResult = array();
	if (!is_array($arValues))
	{
		$arValues = array($arValues);
	}
	if (!is_array($arDescriptions))
	{
		$arDescriptions = array($arDescriptions);
	}

	foreach ($arValues as $k => $v)
	{
		$v = trim($v);
		if (strlen($v) > 0)
		{
			$strRes = '';
			if (strlen($arDescriptions[$k]) <= 0)
			{
				$strRes = 'т. ';
			}
			if ($bForMobile)
			{
				if (UA_ANDROID === true)
				{
					$strRes .= '<a href="tel:' . MakePhoneNumber($v) . '">' . $v . '</a>';
				} else
				{
					$strRes .= '<a href="tel://' . MakePhoneNumber($v) . '">' . $v . '</a>';
				}
			} else
			{
				$strRes .= $v;
			}
			if (strlen($arDescriptions[$k]) > 0)
			{
				$strRes .= ' (' . $arDescriptions[$k] . ')';
			}
			$arResult[] = $strRes;
		}
	}
	return implode(", ", $arResult);
}

function getCountProductsInBasket()
{
	$iResult = 0;
	CModule::IncludeModule("sale");
	$dbBasketItems = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N"));
	while ($arItem = $dbBasketItems->Fetch())
	{
		$iResult += $arItem['QUANTITY'];
	}
	return $iResult;
}

function IsShowAdminSitePanel()
{
	global $USER;
	if ($USER->IsAuthorized())
	{
		if ($_REQUEST['hidepanel'] == "Y")
		{
			$_SESSION['hideadminsitepanel'] = true;
		}
		if ($_REQUEST['hidepanel'] == "N")
		{
			$_SESSION['hideadminsitepanel'] = false;
		}
		if ($_SESSION['hideadminsitepanel'])
		{
			return false;
		}
		$arUserGroup = $USER->GetUserGroupArray();
		foreach ($arUserGroup as $groupId)
		{
			if (in_array($groupId, array(1)))
			{
				return true;
			}
		}
	}
	return false;
}

function doUpdateUrl($strUrl)
{
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/filter.url.php"))
	{
		$arRules = include($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/filter.url.php");
		foreach ($arRules as $k => $v)
		{
			if (strpos($strUrl, $v) === 0)
			{
				$strUrl = $k . substr($strUrl, strlen($v));
				return $strUrl;
			}
		}
	}
	return $strUrl;
}

function doModifyItemPrice($arPrice, $arOldPrice)
{
	if ($arOldPrice['VALUE'] > 0)
	{
		if ($arPrice['DISCOUNT'] > 0)
		{
			if ($arPrice['BASE_PRICE'] < $arOldPrice['VALUE'])
			{
				$arPrice['BASE_PRICE'] = $arOldPrice['VALUE'];
				$arPrice['DISCOUNT'] = $arPrice['BASE_PRICE'] - $arPrice['PRICE'];
				$arPrice['PERCENT'] = round($arPrice['DISCOUNT'] / $arPrice['BASE_PRICE'] * 100, 0);
				$arPrice['RATIO_BASE_PRICE'] = $arPrice['BASE_PRICE'];
				$arPrice['RATIO_DISCOUNT'] = $arPrice['DISCOUNT'];
				$arPrice['PRINT_BASE_PRICE'] = FormatCurrency($arPrice['BASE_PRICE'], $arPrice['CURRENCY']);
				$arPrice['PRINT_RATIO_BASE_PRICE'] = FormatCurrency($arPrice['RATIO_BASE_PRICE'], $arPrice['CURRENCY']);
				$arPrice['PRINT_DISCOUNT'] = FormatCurrency($arPrice['DISCOUNT'], $arPrice['CURRENCY']);
				$arPrice['PRINT_RATIO_DISCOUNT'] = FormatCurrency($arPrice['RATIO_DISCOUNT'], $arPrice['CURRENCY']);
			}
		} else
		{
			if ($arPrice['BASE_PRICE'] < $arOldPrice['VALUE'])
			{
				$arPrice['BASE_PRICE'] = $arOldPrice['VALUE'];
				$arPrice['DISCOUNT'] = $arPrice['BASE_PRICE'] - $arPrice['PRICE'];
				$arPrice['PERCENT'] = round($arPrice['DISCOUNT'] / $arPrice['BASE_PRICE'] * 100, 0);
				$arPrice['RATIO_BASE_PRICE'] = $arPrice['BASE_PRICE'];
				$arPrice['RATIO_DISCOUNT'] = $arPrice['DISCOUNT'];
				$arPrice['PRINT_BASE_PRICE'] = FormatCurrency($arPrice['BASE_PRICE'], $arPrice['CURRENCY']);
				$arPrice['PRINT_RATIO_BASE_PRICE'] = FormatCurrency($arPrice['RATIO_BASE_PRICE'], $arPrice['CURRENCY']);
				$arPrice['PRINT_DISCOUNT'] = FormatCurrency($arPrice['DISCOUNT'], $arPrice['CURRENCY']);
				$arPrice['PRINT_RATIO_DISCOUNT'] = FormatCurrency($arPrice['RATIO_DISCOUNT'], $arPrice['CURRENCY']);
			}
		}
	}
	return $arPrice;
}

function getElementData($id)
{
	static $arCache = array();
	if ($id <= 0)
	{
		return false;
	}
	if (!isset($arCache[$id]))
	{
		$arFields = false;
		if ($id > 0)
		{
			$rElement = \CIBlockElement::GetList(array(), array("ID" => $id));
			if ($rsElement = $rElement->GetNextElement(false, false))
			{
				$arFields = $rsElement->GetFields();
				$arFields['PROPERTIES'] = $rsElement->GetProperties();
			}
		}
		$arCache[$id] = $arFields;
	}
	return $arCache[$id];
}

function getElementDataFull($id, $bClearAllCache = false)
{
	static $arCache = array();
	if ($bClearAllCache)
	{
		$arCache = array();
	}
	if ($id <= 0)
	{
		return false;
	}
	if (!isset($arCache[$id]))
	{
		$arFields = false;
		if ($id > 0)
		{
			$rElement = \CIBlockElement::GetList(array(), array("ID" => $id));
			if ($rsElement = $rElement->GetNextElement())
			{
				$arFields = $rsElement->GetFields();
				$arFields['PROPERTIES'] = $rsElement->GetProperties();
			}
		}
		$arCache[$id] = $arFields;
	}
	return $arCache[$id];
}

function getSectionData($id)
{
	static $arCache = array();
	if ($id <= 0)
	{
		return false;
	}
	if (!isset($arCache[$id]))
	{
		$arSection = false;
		if ($id > 0)
		{
			$rSection = \CIBlockSection::GetList(array(), array("ID" => $id));
			$arSection = $rSection->Fetch();
		}
		$arCache[$id] = $arSection;
	}
	return $arCache[$id];
}

function doCheckPhotoToSize($strPhotoPath, $iWidth, $iHight)
{
	$arPath = pathinfo($strPhotoPath);
	$newName = $arPath['dirname'] . "/" . $arPath['filename'] . ".jpg";
	$arSize = getimagesize($strPhotoPath);
	$width = $arSize[0];
	$height = $arSize[1];
	if ($width > 0 && $height > 0)
	{
		$p = $width / $height;
		$h = intval($height * ($iWidth / $width));
		$origP = $iWidth / $iHight;
		if ($p > $origP)
		{
			$newWidth = $width;
			$newHeight = intval($width / $origP);
		} else
		{
			$newHeight = $height;
			$newWidth = intval($height * $origP);
		}
		if (intval($p * 100) != intval($origP * 100))
		{
			//echo $strPhotoPath . "\n";
			$imgOriginal = imagecreatefromstring(file_get_contents($strPhotoPath));
			$img = imagecreatetruecolor($newWidth, $newHeight);
			$c = imagecolorallocate($img, 255, 255, 255);
			imagefill($img, 0, 0, $c);
			imagecopy($img, $imgOriginal, intval(($newWidth - $width) / 2), intval(($newHeight - $height) / 2), 0, 0, $width, $height);
			imagejpeg($img, $newName, 90);
		}
	}
}

function xmlstr_to_array($xmlstr)
{
	$doc = new DOMDocument();
	$doc->loadXML($xmlstr);
	$root = $doc->documentElement;
	$output = domnode_to_array($root);
	$output['@root'] = $root->tagName;
	return $output;
}

function domnode_to_array($node)
{
	$output = array();
	switch ($node->nodeType)
	{
		case XML_CDATA_SECTION_NODE:
		case XML_TEXT_NODE:
			$output = trim($node->textContent);
			break;
		case XML_ELEMENT_NODE:
			for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++)
			{
				$child = $node->childNodes->item($i);
				$v = domnode_to_array($child);
				if (isset($child->tagName))
				{
					$t = $child->tagName;
					if (!isset($output[$t]))
					{
						$output[$t] = array();
					}
					$output[$t][] = $v;
				} elseif ($v || $v === '0')
				{
					$output = (string) $v;
				}
			}
			if ($node->attributes->length && !is_array($output))
			{ //Has attributes but isn't an array
				$output = array('@content' => $output); //Change output into an array.
			}
			if (is_array($output))
			{
				if ($node->attributes->length)
				{
					$a = array();
					foreach ($node->attributes as $attrName => $attrNode)
					{
						$a[$attrName] = (string) $attrNode->value;
					}
					$output['@attributes'] = $a;
				}
				foreach ($output as $t => $v)
				{
					if (is_array($v) && count($v) == 1 && $t != '@attributes')
					{
						$output[$t] = $v[0];
					}
				}
			}
			break;
	}
	return $output;
}
