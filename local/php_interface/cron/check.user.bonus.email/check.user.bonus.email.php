<?

namespace Webavk\H2OSport\Cron;

class CheckUserBonus
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arEmailFields = array();
	protected $arProducts = array();
	protected $arCards = array();
	protected $bIsCardSend = false;

	function __construct($arOptions, $bIsCardSend = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsCardSend = $bIsCardSend;
	}

	function Start()
	{
		//$this->doCheckProductRatings();
		if ($this->bIsCardSend)
		{
			$this->doMakeCardsForEmail();
			$this->doMakeProductsWithCardsForEmail();
			$this->doSendUsersBonusCardsInfo();
		} else
		{
			$this->doMakeProductsForEmail2();
			$this->doSendUsersBonusInfo();
		}
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doMakeProductsForEmail()
	{
		$this->arProducts = array();
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("BASE", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"NAME",
			"CATALOG_TYPE",
			"PREVIEW_PICTURE",
			"DETAIL_PAGE_URL"
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		$rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"), false, false, $arSelect);
		while ($arElement = $rElements->GetNext())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
			if (count($arResult['ITEMS']) >= 8)
			{
				break;
			}
		}

		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}
		foreach ($arResult['ITEMS'] as $k => $arItem)
		{
			$arItem["PRICES"] = \CIBlockPriceTools::GetItemPrices(IBLOCK_CATALOG, $arResult["PRICES"], $arItem, "Y");
			$percent = 0;
			$iPrice = 0;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$ak = array_keys($arItem['PRICES']);
				$price = $arItem['PRICES'][$ak[0]];
				$price['BASE_PRICE'] = $price['VALUE'];
				$price['VALUE'] = $price['DISCOUNT_VALUE'];
				$price['PRICE'] = $price['DISCOUNT_VALUE'];

				$arItem['RESULT_PRICE'] = $this->doModifyItemPrice($price, $oldPrice);
				$iPrice = $price['PRICE'];
				$percent = $arItem['RESULT_PRICE']['DISCOUNT'] / $arItem['RESULT_PRICE']['BASE_PRICE'];
			} else
			{
				$ak = array_keys($arItem['PRICES']);
				$arItem['RESULT_PRICE'] = $arItem['PRICES'][$ak[0]];
				$iPrice = $arItem['RESULT_PRICE']['DISCOUNT_VALUE'];
				$percent = ($arItem['RESULT_PRICE']['VALUE'] - $arItem['RESULT_PRICE']['DISCOUNT_VALUE']) / $arItem['RESULT_PRICE']['VALUE'];
			}
			$arItem['percent'] = round($percent, 2);

			$arPhoto = false;
			if ($arItem['PREVIEW_PICTURE'] > 0)
			{
				$arPhoto = $arItem['PREVIEW_PICTURE'];
			}
			if ($arPhoto > 0)
			{
				$arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
			} else
			{
				$arPhoto = array(
					"src" => "/local/templates/h2osport/img/empty.270.270.jpg"
				);
			}
			$index = count($this->arProducts) + 1;
			ob_start();
			?>
			<table width="280" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="center" valign="top" style="padding: 10px 5px 0 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank">
							<img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="270" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
						</a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arItem['NAME'] ?></a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
						<table width="280" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
									<?= $arItem['RESULT_PRICE']['PRINT_DISCOUNT_VALUE'] ?>
								</td>
								<td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
									<?= $arItem['RESULT_PRICE']['PRINT_BASE_PRICE'] ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
			$cont = ob_get_contents();
			ob_end_clean();
			$this->arProducts[$index] = $cont;
		}
		/*
		  $rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"));
		  while ($arElement = $rElements->GetNext())
		  {
		  $index = count($this->arProducts) + 1;
		  $arPhoto = false;
		  if ($arElement['PREVIEW_PICTURE'] > 0)
		  {
		  $arPhoto = $arElement['PREVIEW_PICTURE'];
		  }
		  if ($arPhoto > 0)
		  {
		  $arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
		  } else
		  {
		  $arPhoto = array(
		  "src" => "/local/templates/h2osport/img/empty.270.270.jpg"
		  );
		  }
		  ob_start();
		  ?>
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="center" valign="top" style="padding: 10px 5px 0 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank">
		  <img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="270" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
		  </a>
		  </td>
		  </tr>
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arElement['NAME'] ?></a>
		  </td>
		  </tr>
		  <? /*
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
		  <?= $arProduct['PRICE'] ?>
		  </td>
		  <td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
		  <?= $arProduct['OLDPRICE'] ?>
		  </td>
		  <td align="right" valign="top" style="padding: 10px 10px 0 10px;color:#ff0000; font-size:16px;">
		  <?= $arProduct['DISCOUNT'] ?>
		  </td>
		  </tr>
		  </table>
		  </td>
		  </tr>
		 * 
		 *//* ?>
		  </table>
		  <?
		  $cont = ob_get_contents();
		  ob_end_clean();
		  $this->arProducts[$index] = $cont;
		  }
		 * 
		 */
	}

	function doMakeProductsForEmail2()
	{
		$this->arProducts = array();
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("BASE", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"NAME",
			"CATALOG_TYPE",
			"PREVIEW_PICTURE",
			"DETAIL_PAGE_URL"
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		$rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"), false, false, $arSelect);
		while ($arElement = $rElements->GetNext())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
			if (count($arResult['ITEMS']) >= 6)
			{
				break;
			}
		}

		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}
		foreach ($arResult['ITEMS'] as $k => $arItem)
		{
			$arItem["PRICES"] = \CIBlockPriceTools::GetItemPrices(IBLOCK_CATALOG, $arResult["PRICES"], $arItem, "Y");
			$percent = 0;
			$iPrice = 0;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$ak = array_keys($arItem['PRICES']);
				$price = $arItem['PRICES'][$ak[0]];
				$price['BASE_PRICE'] = $price['VALUE'];
				$price['VALUE'] = $price['DISCOUNT_VALUE'];
				$price['PRICE'] = $price['DISCOUNT_VALUE'];

				$arItem['RESULT_PRICE'] = $this->doModifyItemPrice($price, $oldPrice);
				$iPrice = $price['PRICE'];
				$percent = $arItem['RESULT_PRICE']['DISCOUNT'] / $arItem['RESULT_PRICE']['BASE_PRICE'];
			} else
			{
				$ak = array_keys($arItem['PRICES']);
				$arItem['RESULT_PRICE'] = $arItem['PRICES'][$ak[0]];
				$iPrice = $arItem['RESULT_PRICE']['DISCOUNT_VALUE'];
				$percent = ($arItem['RESULT_PRICE']['VALUE'] - $arItem['RESULT_PRICE']['DISCOUNT_VALUE']) / $arItem['RESULT_PRICE']['VALUE'];
			}
			$arItem['percent'] = round($percent, 2);

			$arPhoto = false;
			if ($arItem['PREVIEW_PICTURE'] > 0)
			{
				$arPhoto = $arItem['PREVIEW_PICTURE'];
			}
			if ($arPhoto > 0)
			{
				$arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 170, 'height' => 170), BX_RESIZE_IMAGE_EXACT, true);
			} else
			{
				$arPhoto = array(
					"src" => "/local/templates/h2osport/img/empty.270.270.jpg"
				);
			}
			$index = count($this->arProducts) + 1;
			ob_start();
			?>
			<table width="180" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="center" valign="top" style="padding: 10px 5px 0 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank">
							<img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="170" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
						</a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arItem['NAME'] ?></a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
						<table width="180" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
									<?= $arItem['RESULT_PRICE']['PRINT_DISCOUNT_VALUE'] ?>
								</td>
								<td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
									<?= $arItem['RESULT_PRICE']['PRINT_BASE_PRICE'] ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
			$cont = ob_get_contents();
			ob_end_clean();
			$this->arProducts[$index] = $cont;
		}
		/*
		  $rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"));
		  while ($arElement = $rElements->GetNext())
		  {
		  $index = count($this->arProducts) + 1;
		  $arPhoto = false;
		  if ($arElement['PREVIEW_PICTURE'] > 0)
		  {
		  $arPhoto = $arElement['PREVIEW_PICTURE'];
		  }
		  if ($arPhoto > 0)
		  {
		  $arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
		  } else
		  {
		  $arPhoto = array(
		  "src" => "/local/templates/h2osport/img/empty.270.270.jpg"
		  );
		  }
		  ob_start();
		  ?>
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="center" valign="top" style="padding: 10px 5px 0 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank">
		  <img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="270" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
		  </a>
		  </td>
		  </tr>
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arElement['NAME'] ?></a>
		  </td>
		  </tr>
		  <? /*
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
		  <?= $arProduct['PRICE'] ?>
		  </td>
		  <td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
		  <?= $arProduct['OLDPRICE'] ?>
		  </td>
		  <td align="right" valign="top" style="padding: 10px 10px 0 10px;color:#ff0000; font-size:16px;">
		  <?= $arProduct['DISCOUNT'] ?>
		  </td>
		  </tr>
		  </table>
		  </td>
		  </tr>
		 * 
		 *//* ?>
		  </table>
		  <?
		  $cont = ob_get_contents();
		  ob_end_clean();
		  $this->arProducts[$index] = $cont;
		  }
		 * 
		 */
	}

	function doMakeProductsWithCardsForEmail()
	{
		$this->arProducts = array();
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("BASE", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"NAME",
			"CATALOG_TYPE",
			"PREVIEW_PICTURE",
			"DETAIL_PAGE_URL"
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		$rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"), false, false, $arSelect);
		while ($arElement = $rElements->GetNext())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
			if (count($arResult['ITEMS']) >= 3)
			{
				break;
			}
		}

		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}
		foreach ($arResult['ITEMS'] as $k => $arItem)
		{
			$arItem["PRICES"] = \CIBlockPriceTools::GetItemPrices(IBLOCK_CATALOG, $arResult["PRICES"], $arItem, "Y");
			$percent = 0;
			$iPrice = 0;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$ak = array_keys($arItem['PRICES']);
				$price = $arItem['PRICES'][$ak[0]];
				$price['BASE_PRICE'] = $price['VALUE'];
				$price['VALUE'] = $price['DISCOUNT_VALUE'];
				$price['PRICE'] = $price['DISCOUNT_VALUE'];

				$arItem['RESULT_PRICE'] = $this->doModifyItemPrice($price, $oldPrice);
				$iPrice = $price['PRICE'];
				$percent = $arItem['RESULT_PRICE']['DISCOUNT'] / $arItem['RESULT_PRICE']['BASE_PRICE'];
			} else
			{
				$ak = array_keys($arItem['PRICES']);
				$arItem['RESULT_PRICE'] = $arItem['PRICES'][$ak[0]];
				$iPrice = $arItem['RESULT_PRICE']['DISCOUNT_VALUE'];
				$percent = ($arItem['RESULT_PRICE']['VALUE'] - $arItem['RESULT_PRICE']['DISCOUNT_VALUE']) / $arItem['RESULT_PRICE']['VALUE'];
			}
			$arItem['percent'] = round($percent, 2);

			$arPhoto = false;
			if ($arItem['PREVIEW_PICTURE'] > 0)
			{
				$arPhoto = $arItem['PREVIEW_PICTURE'];
			}
			if ($arPhoto > 0)
			{
				$arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
			} else
			{
				$arPhoto = array(
					"src" => "/local/templates/h2osport/img/empty.270.270.jpg"
				);
			}
			$index = count($this->arProducts) + 1;
			ob_start();
			?>
			<table width="280" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="center" valign="top" style="padding: 10px 5px 0 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank">
							<img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="270" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
						</a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arItem['NAME'] ?></a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
						<table width="280" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
									<?= $arItem['RESULT_PRICE']['PRINT_DISCOUNT_VALUE'] ?>
								</td>
								<td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
									<?= $arItem['RESULT_PRICE']['PRINT_BASE_PRICE'] ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
			$cont = ob_get_contents();
			ob_end_clean();
			$this->arProducts[$index] = $cont;
		}
		/*
		  $rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"));
		  while ($arElement = $rElements->GetNext())
		  {
		  $index = count($this->arProducts) + 1;
		  $arPhoto = false;
		  if ($arElement['PREVIEW_PICTURE'] > 0)
		  {
		  $arPhoto = $arElement['PREVIEW_PICTURE'];
		  }
		  if ($arPhoto > 0)
		  {
		  $arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
		  } else
		  {
		  $arPhoto = array(
		  "src" => "/local/templates/h2osport/img/empty.270.270.jpg"
		  );
		  }
		  ob_start();
		  ?>
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="center" valign="top" style="padding: 10px 5px 0 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank">
		  <img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="270" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
		  </a>
		  </td>
		  </tr>
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arElement['NAME'] ?></a>
		  </td>
		  </tr>
		  <? /*
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
		  <?= $arProduct['PRICE'] ?>
		  </td>
		  <td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
		  <?= $arProduct['OLDPRICE'] ?>
		  </td>
		  <td align="right" valign="top" style="padding: 10px 10px 0 10px;color:#ff0000; font-size:16px;">
		  <?= $arProduct['DISCOUNT'] ?>
		  </td>
		  </tr>
		  </table>
		  </td>
		  </tr>
		 * 
		 *//* ?>
		  </table>
		  <?
		  $cont = ob_get_contents();
		  ob_end_clean();
		  $this->arProducts[$index] = $cont;
		  }
		 * 
		 */
	}

	function doMakeCardsForEmail()
	{
		$this->arCards = array();
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("BASE", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"NAME",
			"CATALOG_TYPE",
			"PREVIEW_PICTURE",
			"DETAIL_PAGE_URL"
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		$rElements = \CIBlockElement::GetList(array("sort" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_CARD_SHOW_IN_SENDER" => "Y"), false, false, $arSelect);
		while ($arElement = $rElements->GetNext())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
			if (count($arResult['ITEMS']) >= 3)
			{
				break;
			}
		}

		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}
		foreach ($arResult['ITEMS'] as $k => $arItem)
		{
			$arItem["PRICES"] = \CIBlockPriceTools::GetItemPrices(IBLOCK_CATALOG, $arResult["PRICES"], $arItem, "Y");
			$percent = 0;
			$iPrice = 0;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$ak = array_keys($arItem['PRICES']);
				$price = $arItem['PRICES'][$ak[0]];
				$price['BASE_PRICE'] = $price['VALUE'];
				$price['VALUE'] = $price['DISCOUNT_VALUE'];
				$price['PRICE'] = $price['DISCOUNT_VALUE'];

				$arItem['RESULT_PRICE'] = $this->doModifyItemPrice($price, $oldPrice);
				$iPrice = $price['PRICE'];
				$percent = $arItem['RESULT_PRICE']['DISCOUNT'] / $arItem['RESULT_PRICE']['BASE_PRICE'];
			} else
			{
				$ak = array_keys($arItem['PRICES']);
				$arItem['RESULT_PRICE'] = $arItem['PRICES'][$ak[0]];
				$iPrice = $arItem['RESULT_PRICE']['DISCOUNT_VALUE'];
				$percent = ($arItem['RESULT_PRICE']['VALUE'] - $arItem['RESULT_PRICE']['DISCOUNT_VALUE']) / $arItem['RESULT_PRICE']['VALUE'];
			}
			$arItem['percent'] = round($percent, 2);

			$arPhoto = false;
			if ($arItem['PREVIEW_PICTURE'] > 0)
			{
				$arPhoto = $arItem['PREVIEW_PICTURE'];
			}
			if ($arPhoto > 0)
			{
				$arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 170, 'height' => 170), BX_RESIZE_IMAGE_EXACT, true);
			} else
			{
				$arPhoto = array(
					"src" => "/local/templates/h2osport/img/empty.270.270.jpg"
				);
			}
			$index = count($this->arCards) + 1;
			ob_start();
			?>
			<table width="180" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="center" valign="top" style="padding: 10px 5px 0 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank">
							<img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="170" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
						</a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
						<a href="http://www.h2osport.ru<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arItem['NAME'] ?></a>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
						<table width="180" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
									<?= $arItem['RESULT_PRICE']['PRINT_DISCOUNT_VALUE'] ?>
								</td>
								<td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
									<?= $arItem['RESULT_PRICE']['PRINT_BASE_PRICE'] ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
			$cont = ob_get_contents();
			ob_end_clean();
			$this->arCards[$index] = $cont;
		}
		/*
		  $rElements = \CIBlockElement::GetList(array("rand" => "asc"), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_MANUAL_SHOW_IN_SENDER" => "Y"));
		  while ($arElement = $rElements->GetNext())
		  {
		  $index = count($this->arProducts) + 1;
		  $arPhoto = false;
		  if ($arElement['PREVIEW_PICTURE'] > 0)
		  {
		  $arPhoto = $arElement['PREVIEW_PICTURE'];
		  }
		  if ($arPhoto > 0)
		  {
		  $arPhoto = \CFile::ResizeImageGet($arPhoto, array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
		  } else
		  {
		  $arPhoto = array(
		  "src" => "/local/templates/h2osport/img/empty.270.270.jpg"
		  );
		  }
		  ob_start();
		  ?>
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="center" valign="top" style="padding: 10px 5px 0 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank">
		  <img src="http://www.h2osport.ru<?= $arPhoto['src'] ?>" width="270" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
		  </a>
		  </td>
		  </tr>
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 5px 10px 5px;">
		  <a href="http://www.h2osport.ru<?= $arElement['DETAIL_PAGE_URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arElement['NAME'] ?></a>
		  </td>
		  </tr>
		  <? /*
		  <tr>
		  <td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
		  <table width="280" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		  <td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
		  <?= $arProduct['PRICE'] ?>
		  </td>
		  <td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
		  <?= $arProduct['OLDPRICE'] ?>
		  </td>
		  <td align="right" valign="top" style="padding: 10px 10px 0 10px;color:#ff0000; font-size:16px;">
		  <?= $arProduct['DISCOUNT'] ?>
		  </td>
		  </tr>
		  </table>
		  </td>
		  </tr>
		 * 
		 *//* ?>
		  </table>
		  <?
		  $cont = ob_get_contents();
		  ob_end_clean();
		  $this->arProducts[$index] = $cont;
		  }
		 * 
		 */
	}

	function doModifyItemPrice($arPrice, $arOldPrice)
	{
		if ($arOldPrice['VALUE'] > 0)
		{
			if ($arPrice['DISCOUNT'] > 0)
			{
				if ($arPrice['BASE_PRICE'] < $arOldPrice['VALUE'])
				{
					$arPrice['BASE_PRICE'] = $arOldPrice['VALUE'];
					$arPrice['DISCOUNT'] = $arPrice['BASE_PRICE'] - $arPrice['PRICE'];
					$arPrice['PERCENT'] = round($arPrice['DISCOUNT'] / $arPrice['BASE_PRICE'] * 100, 0);
					$arPrice['RATIO_BASE_PRICE'] = $arPrice['BASE_PRICE'];
					$arPrice['RATIO_DISCOUNT'] = $arPrice['DISCOUNT'];
					$arPrice['PRINT_BASE_PRICE'] = FormatCurrency($arPrice['BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_BASE_PRICE'] = FormatCurrency($arPrice['RATIO_BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_DISCOUNT'] = FormatCurrency($arPrice['DISCOUNT'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_DISCOUNT'] = FormatCurrency($arPrice['RATIO_DISCOUNT'], $arPrice['CURRENCY']);
				}
			} else
			{
				if ($arPrice['BASE_PRICE'] < $arOldPrice['VALUE'])
				{
					$arPrice['BASE_PRICE'] = $arOldPrice['VALUE'];
					$arPrice['DISCOUNT'] = $arPrice['BASE_PRICE'] - $arPrice['PRICE'];
					$arPrice['PERCENT'] = round($arPrice['DISCOUNT'] / $arPrice['BASE_PRICE'] * 100, 0);
					$arPrice['RATIO_BASE_PRICE'] = $arPrice['BASE_PRICE'];
					$arPrice['RATIO_DISCOUNT'] = $arPrice['DISCOUNT'];
					$arPrice['PRINT_BASE_PRICE'] = FormatCurrency($arPrice['BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_BASE_PRICE'] = FormatCurrency($arPrice['RATIO_BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_DISCOUNT'] = FormatCurrency($arPrice['DISCOUNT'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_DISCOUNT'] = FormatCurrency($arPrice['RATIO_DISCOUNT'], $arPrice['CURRENCY']);
				}
			}
		}
		return $arPrice;
	}

	function doSendUsersBonusInfo()
	{
		global $USER;
		$oAccount = new \CSaleUserAccount();
		$rUsers = \CUser::GetList($b, $o, array(), array("SELECT" => array("UF_*")));
		while ($arUser = $rUsers->Fetch())
		{
			/*
			if (!in_array($arUser['ID'], array(1)))
			{
				continue;
			}*/
			if ($arUser['UF_NOTSEND_BONUS'] == 1)
			{
				continue;
			}
			$arBonus = $oAccount->GetByUserID($arUser['ID'], "RUB");
			$arEvent = array(
				"EMAIL" => $arUser['EMAIL'],
				"DATE" => date("d.m.Y"),
				"BONUS" => round($arBonus['CURRENT_BUDGET'], 2) . " бонусов",
				"USER_ID" => $arUser['ID']
			);
			foreach ($this->arProducts as $k => $v)
			{
				$arEvent['PRODUCT_' . $k] = $v;
			}
			\CEvent::Send("USER_BONUS_INFO", array("s1"), $arEvent);
		}
	}

	function doSendUsersBonusCardsInfo()
	{
		global $USER;
		$oAccount = new \CSaleUserAccount();
		$rUsers = \CUser::GetList($b, $o, array(), array("SELECT" => array("UF_*")));
		while ($arUser = $rUsers->Fetch())
		{
			if (!in_array($arUser['ID'], array(1)))
			{
				continue;
			}
			if ($arUser['UF_NOTSEND_BONUS'] == 1)
			{
				continue;
			}
			$arBonus = $oAccount->GetByUserID($arUser['ID'], "RUB");
			$arEvent = array(
				"EMAIL" => $arUser['EMAIL'],
				"DATE" => date("d.m.Y"),
				"BONUS" => round($arBonus['CURRENT_BUDGET'], 2) . " бонусов",
				"USER_ID" => $arUser['ID']
			);
			foreach ($this->arCards as $k => $v)
			{
				$arEvent['CARD_' . $k] = $v;
			}
			foreach ($this->arProducts as $k => $v)
			{
				$arEvent['PRODUCT_' . $k] = $v;
			}
			\CEvent::Send("USER_BONUS_CARD_INFO", array("s1"), $arEvent);
		}
	}

	function doCheckProductRatings()
	{
		$obElement = new \CIBlockElement();
		$rElements = \CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "PROPERTY_vote_count", "PROPERTY_vote_sum", "PROPERTY_rating"));
		while ($arElement = $rElements->Fetch())
		{
			if ($arElement['PROPERTY_rating_VALUE'] <= 0)
			{
				$cnt = rand(20, 50);
				$sum = $cnt * 4.0;
				$rating = 4.0;
				$obElement->SetPropertyValueCode($arElement['ID'], "vote_count", $cnt);
				$obElement->SetPropertyValueCode($arElement['ID'], "vote_sum", $sum);
				$obElement->SetPropertyValueCode($arElement['ID'], "rating", $rating);
			}
		}
	}

}
