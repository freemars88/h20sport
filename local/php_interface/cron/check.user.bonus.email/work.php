<?

$GLOBALS['WEBAVK_CRON_CURRENT_REL_FILE'] = "/local/php_interface/cron/check.user.bonus.email/work.php";
define("WEBAVK_CRON_UNIQ_IDENT", "check.user.bonus.email");
define("WEBAVK_CRON_USE_LOCKRUN", false);
define("WEBAVK_CRON_USE_LOCK", true);
include(dirname(__FILE__) . "/../include.php");

declare(ticks = 1);
pcntl_async_signals(true);
pcntl_signal(SIGTERM, 'sig_handler');
pcntl_signal(SIGINT, 'sig_handler');
pcntl_signal(SIGCHLD, 'sig_handler');

function sig_handler($sig)
{
	switch ($sig) {
		case SIGTERM:
		case SIGINT:
			$GLOBALS['CNT_SIGTERM'] ++;
			if ($GLOBALS['CNT_SIGTERM'] > 3)
				exit();
			if (!defined("WEBAVK_SIGTERM")) {
				define("WEBAVK_SIGTERM", true);
				if (is_object($GLOBALS['CRON_OBJECT'])) {
					$GLOBALS['CRON_OBJECT']->Stop();
				}
			}
			break;
		case SIGCHLD:
			pcntl_waitpid(-1, $status);
			break;
	}
}

define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

set_time_limit(0);
@ini_set("memory_limit", "512M");
error_reporting(E_ERROR);
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("highloadblock");
CModule::IncludeModule("webavk.h2osport");
CModule::IncludeModule("mail");

include_once(dirname(__FILE__) . "/check.user.bonus.email.php");
$USER->Authorize(DEFAULT_USER_ID_CRON);
$arOptions = include(dirname(__FILE__) . "/.options.php");
$GLOBALS['CRON_OBJECT'] = new \Webavk\H2OSport\Cron\CheckUserBonus($arOptions, $_SERVER['argv'][1] == 'card' ? true : false);

$GLOBALS['CRON_OBJECT']->Start();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
