<?

namespace Webavk\H2OSport\Cron;

class ManualExportSections
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arData = array();
	protected $arHeader = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		//$this->doLog("Start doExportFile");
		//$this->doExportFile();
		$this->doLog("Start doImportFile");
		$this->doImportFile();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog) {
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doExportFile()
	{
		$rFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv", "w+t");
		if ($rFile) {
			$arHeader = array(
				"ID",
				"Активность",
				"Название",
				"Обновить до названия",
			);
			fputcsv($rFile, $arHeader, ';', '"');
			$rSections = \CIBlockSection::GetTreeList(array("IBLOCK_ID" => IBLOCK_CATALOG));
			while ($arSection = $rSections->Fetch()) {
				$arData = array(
					$arSection['ID'],
					($arSection['ACTIVE'] == "Y" ? "Да" : "Нет"),
					str_repeat(" . ", $arSection['DEPTH_LEVEL'] - 1) . $arSection['NAME'],
					$arSection['NAME'],
				);
				fputcsv($rFile, $arData, ';', '"');
			}
			fclose($rFile);
		}
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv", iconv("utf-8", "windows-1251", file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv")));
	}

	function doImportFile()
	{
		$obSection = new \CIBlockSection();
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv", iconv("windows-1251", "utf-8", file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv")));
		$arHeader = array();
		$rFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv", "rt");
		if ($rFile) {
			while ($ar = fgetcsv($rFile, 2000, ";", '"')) {
				if (empty(arHeader)) {
					$arHeader = $ar;
				} else {
					foreach ($ar as $k => $v) {
						$ar[$k] = trim($v);
					}
					if ($ar[0] > 0 && strlen($ar[3]) > 0) {
						$arSection = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $ar[0]))->Fetch();
						if ($arSection) {
							if ($arSection['NAME'] != $ar[3]) {
								$obSection->Update($arSection['ID'], array(
									"NAME" => $ar[3],
								));
							}
						}
					}
				}
			}
			fclose($rFile);
		}
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv", iconv("utf-8", "windows-1251", file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.export.sections.csv")));
	}

	function doLoadFile()
	{
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.csv", iconv("windows-1251", "utf-8", file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.csv")));
		$rFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.csv", "rt");
		if ($rFile) {
			while ($ar = fgetcsv($rFile, 2000, ";", '"')) {
				if (empty($this->arHeader)) {
					$this->arHeader = $ar;
				} else {
					foreach ($ar as $k => $v) {
						$ar[$k] = trim($v);
					}
					$strSize = strtolower(trim($ar[3]));
					if (strlen($strSize) <= 0) {
						$strSize = "-";
					}
					$strColor = strtolower(trim($ar[4]));
					if (strlen($strColor) <= 0) {
						$strColor = "-";
					}
					$this->arData[strtolower(trim($ar[0]))][$strSize][$strColor] = $ar;
				}
			}
			fclose($rFile);
		}
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.csv", iconv("utf-8", "windows-1251", file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.csv")));
	}

	function doFillData()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_MORE_PHOTOS", "PROPERTY_CML2_ARTICLE"));
		while ($arElement = $rElements->Fetch()) {
			$strArticle = strtolower(trim($arElement['PROPERTY_CML2_ARTICLE_VALUE']));
			$arPhotosByColor = array();
			if ($arElement['PREVIEW_PICTURE'] > 0) {
				$arPhotosByColor['-'][] = $arElement['PREVIEW_PICTURE'];
			}
			if (!empty($arElement['PROPERTY_MORE_PHOTOS_VALUE'])) {
				foreach ($arElement['PROPERTY_MORE_PHOTOS_VALUE'] as $k => $v) {
					if (strlen($arElement['PROPERTY_MORE_PHOTOS_DESCRIPTION'][$k]) > 0) {
						$arPhotosByColor[trim(strtolower($arElement['PROPERTY_MORE_PHOTOS_DESCRIPTION'][$k]))][] = $v;
					} else {
						$arPhotosByColor['-'][] = $v;
					}
				}
			}
			if (isset($this->arData[$strArticle])) {
				$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $arElement['ID']), false, false, array("ID", "NAME", "PROPERTY_COLOR", "PROPERTY_COLOR_NAME" => "PROPERTY_COLOR.NAME", "PROPERTY_SIZE", "PROPERTY_SIZE_NAME" => "PROPERTY_SIZE.NAME"));
				while ($arOffer = $rOffers->Fetch()) {
					$strSize = trim(strtolower($arOffer['PROPERTY_SIZE_NAME']));
					$strColor = trim(strtolower($arOffer['PROPERTY_COLOR_NAME']));
					if (strlen($strSize) <= 0) {
						$strSize = "-";
					}
					if (strlen($strColor) <= 0) {
						$strColor = "-";
					}
					if (isset($this->arData[$strArticle][$strSize][$strColor])) {
						$arSetPhotos = array();
						if (isset($arPhotosByColor[$strColor])) {
							$arSetPhotos = $arPhotosByColor[$strColor];
						} elseif (isset($arPhotosByColor['-'])) {
							$arSetPhotos = $arPhotosByColor["-"];
						}
						if (!empty($arSetPhotos)) {
							$index = 5;
							foreach ($arSetPhotos as $k => $v) {
								$strPath = "http://img.vtsport.ru" . \CFile::GetPath($v);
								$this->arData[$strArticle][$strSize][$strColor][$index] = $strPath;
								$index++;
							}
						}
					}
				}
			}
		}
	}

	function doSaveData()
	{
		$rFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.new.csv", "w+t");
		if ($rFile) {
			fputcsv($rFile, $this->arHeader, ';', '"');
			foreach ($this->arData as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					foreach ($v2 as $k3 => $v3) {
						fputcsv($rFile, $v3, ';', '"');
					}
				}
			}
			fclose($rFile);
		}
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.new.csv", iconv("utf-8", "windows-1251", file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/file.photos.new.csv")));
	}

}
