<?

namespace Webavk\H2OSport\Cron;

class ManualRedirects
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arArticlesToOldSections = array();
	protected $arRedirects = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		//Грузим текущие элементы
		$arArticlesToPath = array();
		$arArticlesToSections = array();
		$arOldToNewSections = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_CML2_ARTICLE"));
		while ($arElement = $rElements->GetNext())
		{
			$arArticlesToSections[$arElement['~PROPERTY_CML2_ARTICLE_VALUE']] = $arElement['IBLOCK_SECTION_ID'];
			$arArticlesToPath[$arElement['~PROPERTY_CML2_ARTICLE_VALUE']] = $arElement['DETAIL_PAGE_URL'];
		}
		//Грузим старые элементы и разделы
		$arOldSectionsList = array();
		$rSections = \CIBlockSection::GetTreeList(array("IBLOCK_ID" => IBLOCK_OLD_CATALOG));
		while ($arSection = $rSections->Fetch())
		{
			$arSection['SECTION_PAGE_URL'] = "/catalog/" . $arSection['XML_ID'] . "/";
			$arOldSectionsList[$arSection['ID']] = array(
				"ID" => $arSection['ID'],
				"XML_ID" => $arSection['XML_ID'],
				"SECTION_PAGE_URL" => $arSection['SECTION_PAGE_URL'],
				"IBLOCK_SECTION_ID" => $arSection['IBLOCK_SECTION_ID']
			);
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_OLD_CATALOG), false, false, array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_CML2_ARTICLE", "XML_ID"));
		while ($arElement = $rElements->Fetch())
		{
			$arElement['DETAIL_PAGE_URL'] = "/catalog/" . $arOldSectionsList[$arElement['IBLOCK_SECTION_ID']]['XML_ID'] . "/" . $arElement['XML_ID'] . "/";
			$this->arArticlesToOldSections[$arElement['PROPERTY_CML2_ARTICLE_VALUE']] = $arElement['IBLOCK_SECTION_ID'];
			if (isset($arArticlesToPath[$arElement['PROPERTY_CML2_ARTICLE_VALUE']]))
			{
				$this->arRedirects[$arElement['DETAIL_PAGE_URL']] = $arArticlesToPath[$arElement['PROPERTY_CML2_ARTICLE_VALUE']];
				$arOldToNewSections[$arElement['IBLOCK_SECTION_ID']][$arArticlesToSections[$arElement['PROPERTY_CML2_ARTICLE_VALUE']]] ++;
			}
		}
		foreach ($arOldToNewSections as $k => $v)
		{
			asort($v, SORT_NUMERIC);
			$ak = array_keys($v);
			$arOldToNewSections[$k] = $ak[0];
		}
		foreach ($arOldToNewSections as $k => $v)
		{
			$arSectionOld = $arOldSectionsList[$k];
			$arSectionNew = \CIBlockSection::GetList(array(), array("ID" => $v))->GetNext();
			if ($arSectionOld && $arSectionNew)
			{
				$this->arRedirects[$arSectionOld['SECTION_PAGE_URL']] = $arSectionNew['SECTION_PAGE_URL'];
				while ($arSectionOld['IBLOCK_SECTION_ID'] > 0)
				{
					$arSectionOld = $arOldSectionsList[$arSectionOld['IBLOCK_SECTION_ID']];
					$arSectionNew = \CIBlockSection::GetList(array(), array("ID" => $arSectionNew['IBLOCK_SECTION_ID']))->GetNext();
					if ($arSectionOld && $arSectionNew)
					{
						$this->arRedirects[$arSectionOld['SECTION_PAGE_URL']] = $arSectionNew['SECTION_PAGE_URL'];
					} else
					{
						break;
					}
				}
			}
		}
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/local/redirects.old.php", '<' . '? return ' . var_export($this->arRedirects, true) . ';');
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

}
