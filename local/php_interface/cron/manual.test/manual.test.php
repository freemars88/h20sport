<?

namespace Webavk\H2OSport\Cron;

class ManualTest
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arProducts = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		\CEvent::SendImmediate("SENDER_SUBSCRIBE_CONFIRM", "s1", array(
			"EMAIL" => "alexmsk2006@gmail.com",
			"DATE" => "01.08.2019 03:38:08",
			"CONFIRM_URL" => "/bitrix/tools/sender_sub_confirm.php?sender_subscription=confirm&tag=sender.eyJFTUFJTCI6ImFsZXhtc2syMDA2QGdtYWlsLmNvbSIsIlNJVEVfSUQiOiJzMSIsIk1BSUxJTkdfTElTVCI6WyIxIl19.24b9e6a96fef0e9311a21590e1e520fb396000a691a0a4e5e651e2cd47789ba5",
			"MAILING_LIST" => "Первая кампания",
		));
	}

	function Stop()
	{
		$this->bIsStop = true;
	}
}
