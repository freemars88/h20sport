<?

$GLOBALS['WEBAVK_CRON_CURRENT_REL_FILE'] = "/local/php_interface/cron/manual.test/work.php";
define("WEBAVK_CRON_UNIQ_IDENT", "manual.test");
define("WEBAVK_CRON_USE_LOCKRUN", false);
define("WEBAVK_CRON_USE_LOCK", true);
include(dirname(__FILE__) . "/../include.php");

declare(ticks=1);
pcntl_async_signals(true);
pcntl_signal(SIGTERM, 'sig_handler');
pcntl_signal(SIGINT, 'sig_handler');
pcntl_signal(SIGCHLD, 'sig_handler');

function sig_handler($sig)
{
	switch ($sig) {
		case SIGTERM:
		case SIGINT:
			$GLOBALS['CNT_SIGTERM']++;
			if ($GLOBALS['CNT_SIGTERM'] > 3)
				exit();
			if (!defined("WEBAVK_SIGTERM")) {
				define("WEBAVK_SIGTERM", true);
				if (is_object($GLOBALS['CRON_OBJECT'])) {
					$GLOBALS['CRON_OBJECT']->Stop();
				}
			}
			break;
		case SIGCHLD:
			pcntl_waitpid(-1, $status);
			break;
	}
}

define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

set_time_limit(0);
@ini_set("memory_limit", "2560M");
error_reporting(E_ERROR);
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("highloadblock");
CModule::IncludeModule("webavk.h2osport");
/*
global $arAutoLoadSpread;
$arAutoLoadSpread = array();
function SpreadLoadFull($strDir, $strBaseClass)
{
	global $arAutoLoadSpread;
	$arFiles = scandir($strDir);
	foreach ($arFiles as $k => $v) {
		if (in_array($v, array(".", ".."))) {
			continue;
		}
		$strFullName = $strDir . $v;
		$arPath = pathinfo($strFullName);
		if (is_file($strFullName)) {
			if ($arPath['extension'] == "php") {
				$arAutoLoadSpread[$strBaseClass . $arPath['filename']] = substr($strFullName,strlen(realpath($_SERVER['DOCUMENT_ROOT'])));
			}
		} elseif (is_dir($strFullName . "/")) {
			SpreadLoadFull($strFullName . "/", $strBaseClass . $arPath['filename'].'\\');
		}
	}
}

SpreadLoadFull(dirname(__FILE__) . "/PhpSpreadsheet/", 'PhpOffice\\PhpSpreadsheet\\');
CModule::AddAutoloadClasses(NULL,$arAutoLoadSpread);
*/
include_once(dirname(__FILE__) . "/manual.test.php");

$arOptions = include(dirname(__FILE__) . "/.options.php");
$GLOBALS['CRON_OBJECT'] = new \WebAVK\H2OSport\Cron\ManualTest($arOptions, $_SERVER['argv'][1] == 'log' ? true : false);

$GLOBALS['CRON_OBJECT']->Start();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
