<?

namespace Webavk\H2OSport\Cron;

class CheckBrands
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start CheckBrands");
		$this->doCheckBrands();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog) {
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doCheckBrands()
	{
		$obElement = new \CIBlockElement();
		$arAllBrands = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y"), false, false, array("ID", "CODE", "PROPERTY_BRAND"));
		while ($arElement = $rElements->Fetch()) {
			$arAllBrands[$arElement['PROPERTY_BRAND_VALUE']]++;
		}
		$rBrands = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_BRANDS));
		while ($arBrand = $rBrands->Fetch()) {
			if (isset($arAllBrands[$arBrand['ID']])) {
				if ($arBrand['ACTIVE'] != "Y") {
					$obElement->Update($arBrand['ID'], array("ACTIVE" => "Y"));
				}
			} else {
				if ($arBrand['ACTIVE'] == "Y") {
					$obElement->Update($arBrand['ID'], array("ACTIVE" => "N"));
				}
			}
		}
	}

}
