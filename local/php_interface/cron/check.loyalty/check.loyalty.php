<?

namespace Webavk\H2OSport\Cron;

class CheckLoyalty
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arUserTotalOrders = array();

	function __construct($arOptions)
	{
		$this->arOptions = $arOptions;
	}

	function Start()
	{
		global $USER;
		$rOrder = \CSaleOrder::GetList(array("ID" => "ASC"), array("CANCELED" => "N", "PAYED" => "Y"));
		while ($arOrder = $rOrder->Fetch())
		{
			$this->arUserTotalOrders[$arOrder['USER_ID']]['ALL_ORDERS'][$arOrder['ID']] = $arOrder['PRICE'];
		}
		$rOrder = \CSaleOrder::GetList(array("ID" => "ASC"), array("CANCELED" => "Y"));
		while ($arOrder = $rOrder->Fetch())
		{
			$this->arUserTotalOrders[$arOrder['USER_ID']]['CANCEL_ORDERS'][$arOrder['ID']] = $arOrder['PRICE'];
		}
		$rUsers = \CUser::GetList($b, $o, array(), array("SELECT" => array("UF_*")));
		while ($arUser = $rUsers->Fetch())
		{
			/*
			  $rGroup = \CUser::GetUserGroupEx($arUser['ID']);
			  $arGroup = array();
			  while ($ar = $rGroup->Fetch())
			  {
			  $arGroup[] = $ar["GROUP_ID"];
			  } */
			if (isset($this->arUserTotalOrders[$arUser['ID']]))
			{
				$this->arUserTotalOrders[$arUser['ID']]['CHECKED_ORDERS'] = $arUser['UF_CHECKED_ORDERS'];
				$this->arUserTotalOrders[$arUser['ID']]['ORDERS_SUM'] = $arUser['UF_ORDERS_SUM'];
				//$this->arUserTotalOrders[$arUser['ID']]['USER_GROUPS'] = $arGroup;
				$this->arUserTotalOrders[$arUser['ID']]['EMAIL'] = $arUser['EMAIL'];
			}
		}
		foreach ($this->arUserTotalOrders as $userID => &$arUserOrders)
		{
			if (!is_array($arUserOrders['CHECKED_ORDERS']))
			{
				$arUserOrders['CHECKED_ORDERS'] = array($arUserOrders['CHECKED_ORDERS']);
			}
			$bUpdate = false;
			$bUpdateGroup = false;
			foreach ($arUserOrders['ALL_ORDERS'] as $orderId => &$orderSum)
			{
				if (!in_array($orderId, $arUserOrders['CHECKED_ORDERS']))
				{
					$bUpdate = true;
					$addBonus = 0;
					//$strBonusLevel = "Silver";
					$arPrice = $this->getOrderLoyaltyPrice($orderId);
					$arUserOrders['ORDERS_SUM_NEW'] = $arUserOrders['ORDERS_SUM'] + $arPrice['ORDER_PRICE'];
					$arUserOrders['CHECKED_ORDERS'][] = $orderId;
					$addBonus = $arPrice['ACTUAL'] * $this->arOptions['PERCENT'] / 100;
					/* if (in_array(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS']))
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.05 + $arPrice['SALE'] * 0.02;
					  if ($arUserOrders['ORDERS_SUM_NEW'] >= 150000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  unset($arUserOrders['USER_GROUPS'][array_search(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS'])]);
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_3;
					  $strBonusLevel = "Platinum";
					  } elseif ($arUserOrders['ORDERS_SUM_NEW'] >= 100000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.1 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  unset($arUserOrders['USER_GROUPS'][array_search(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS'])]);
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_2;
					  $strBonusLevel = "Gold";
					  }
					  } elseif (in_array(LOYALTY_GROUP_2, $arUserOrders['USER_GROUPS']))
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.10 + $arPrice['SALE'] * 0.02;
					  if ($arUserOrders['ORDERS_SUM_NEW'] >= 150000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  unset($arUserOrders['USER_GROUPS'][array_search(LOYALTY_GROUP_2, $arUserOrders['USER_GROUPS'])]);
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_3;
					  $bUpdateGroup = true;
					  $strBonusLevel = "Platinum";
					  }
					  } elseif (in_array(LOYALTY_GROUP_3, $arUserOrders['USER_GROUPS']))
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  } else
					  {
					  if ($arUserOrders['ORDERS_SUM_NEW'] >= 150000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_3;
					  $strBonusLevel = "Platinum";
					  } elseif ($arUserOrders['ORDERS_SUM_NEW'] >= 100000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.10 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_2;
					  $strBonusLevel = "Gold";
					  } elseif ($arUserOrders['ORDERS_SUM_NEW'] >= 50000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.05 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_1;
					  $strBonusLevel = "Silver";
					  }
					  }
					 * 
					 */
					$arUserOrders['ORDERS_SUM'] = $arUserOrders['ORDERS_SUM_NEW'];
					if ($addBonus > 0)
					{
						$oAccount = new \CSaleUserAccount();
						$oAccount->UpdateAccount($userID, $addBonus, "RUB", "bonus by order", $orderId, "bonus by order #" . $orderId . ". BasePrice actual " . $arPrice['ACTUAL']);
					}
				}
			}
			foreach ($arUserOrders['CANCEL_ORDERS'] as $orderId => &$orderSum)
			{
				if (in_array($orderId, $arUserOrders['CHECKED_ORDERS']))
				{
					$bUpdate = true;
					$delBonus = 0;
					//$strBonusLevel = "Silver";
					$arPrice = $this->getOrderLoyaltyPrice($orderId);
					$arUserOrders['ORDERS_SUM_NEW'] = $arUserOrders['ORDERS_SUM'] - $arPrice['ORDER_PRICE'];
					$key = array_search($orderId, $arUserOrders['CHECKED_ORDERS']);
					unset($arUserOrders['CHECKED_ORDERS'][$key]);
					$delBonus = $arPrice['ACTUAL'] * $this->arOptions['PERCENT'] / 100;
					/* if (in_array(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS']))
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.05 + $arPrice['SALE'] * 0.02;
					  if ($arUserOrders['ORDERS_SUM_NEW'] >= 150000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  unset($arUserOrders['USER_GROUPS'][array_search(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS'])]);
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_3;
					  $strBonusLevel = "Platinum";
					  } elseif ($arUserOrders['ORDERS_SUM_NEW'] >= 100000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.1 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  unset($arUserOrders['USER_GROUPS'][array_search(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS'])]);
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_2;
					  $strBonusLevel = "Gold";
					  }
					  } elseif (in_array(LOYALTY_GROUP_2, $arUserOrders['USER_GROUPS']))
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.10 + $arPrice['SALE'] * 0.02;
					  if ($arUserOrders['ORDERS_SUM_NEW'] >= 150000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  unset($arUserOrders['USER_GROUPS'][array_search(LOYALTY_GROUP_2, $arUserOrders['USER_GROUPS'])]);
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_3;
					  $bUpdateGroup = true;
					  $strBonusLevel = "Platinum";
					  }
					  } elseif (in_array(LOYALTY_GROUP_3, $arUserOrders['USER_GROUPS']))
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  } else
					  {
					  if ($arUserOrders['ORDERS_SUM_NEW'] >= 150000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.15 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_3;
					  $strBonusLevel = "Platinum";
					  } elseif ($arUserOrders['ORDERS_SUM_NEW'] >= 100000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.10 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_2;
					  $strBonusLevel = "Gold";
					  } elseif ($arUserOrders['ORDERS_SUM_NEW'] >= 50000)
					  {
					  $addBonus = $arPrice['ACTUAL'] * 0.05 + $arPrice['SALE'] * 0.02;
					  $bUpdateGroup = true;
					  $arUserOrders['USER_GROUPS'][] = LOYALTY_GROUP_1;
					  $strBonusLevel = "Silver";
					  }
					  }
					 * 
					 */
					$arUserOrders['ORDERS_SUM'] = $arUserOrders['ORDERS_SUM_NEW'];
					if ($delBonus > 0)
					{
						$oAccount = new \CSaleUserAccount();
						$oAccount->UpdateAccount($userID, 0 - $delBonus, "RUB", "bonus deleted order", $orderId, "bonus deleted order #" . $orderId . ". BasePrice actual " . $arPrice['ACTUAL']);
					}
				}
			}
			if ($bUpdate)
			{
				$USER->Update($userID, array(
					"UF_CHECKED_ORDERS" => $arUserOrders['CHECKED_ORDERS'],
					"UF_ORDERS_SUM" => $arUserOrders['ORDERS_SUM']
				));
			}
			/* if ($bUpdateGroup)
			  {
			  \CUser::SetUserGroup($userID, $arUserOrders['USER_GROUPS']);
			  }
			 * 
			 *//*
			  if ($bUpdate || $bUpdateGroup)
			  {
			  //Формирование и отправка мейла
			  $arFields = array(
			  "EMAIL" => $arUserOrders['EMAIL']
			  );
			  if (in_array(LOYALTY_GROUP_1, $arUserOrders['USER_GROUPS']))
			  {
			  $arFields['STATUS'] = "SILVER";
			  $arFields['DESCRIPTION'] = "Вы получаете на бонусный счет для оплаты покупок:<br/>
			  2% от стоимости акционных товаров<br/>
			  5% от стоимости остальных товаров";
			  } elseif (in_array(LOYALTY_GROUP_2, $arUserOrders['USER_GROUPS']))
			  {
			  $arFields['STATUS'] = "GOLD";
			  $arFields['DESCRIPTION'] = "Вы получаете на бонусный счет для оплаты покупок:<br/>
			  2% от стоимости акционных товаров<br/>
			  10% от стоимости остальных товаров";
			  } elseif (in_array(LOYALTY_GROUP_3, $arUserOrders['USER_GROUPS']))
			  {
			  $arFields['STATUS'] = "PLATINUM";
			  $arFields['DESCRIPTION'] = "Вы получаете на бонусный счет для оплаты покупок:<br/>
			  2% от стоимости акционных товаров<br/>
			  15% от стоимости остальных товаров";
			  }
			  $oAccount = new \CSaleUserAccount();
			  $arAccount = $oAccount->GetByUserID($userID, "RUB");
			  $arFields['SUM'] = FormatCurrency($arAccount['CURRENT_BUDGET'], "RUB");
			  \CEvent::Send("SALE_LOYALTY", array("es"), $arFields);
			  }
			 * 
			 */
		}
		//myPrint($this->arUserTotalOrders[172]);
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function getOrderLoyaltyPrice($orderId)
	{
		$arResult = array();
		$rOrder = \CSaleOrder::GetList(array(), array("ID" => $orderId));
		if ($arOrder = $rOrder->Fetch())
		{
			$arResult['ORDER_PRICE'] = $arOrder['PRICE']; // - $arOrder['PRICE_DELIVERY'];
			$rBasket = \CSaleBasket::GetList(array(), array("ORDER_ID" => $arOrder['ID']));
			while ($arBasket = $rBasket->Fetch())
			{
				$arResult['ACTUAL'] += $arBasket['PRICE'] * $arBasket['QUANTITY'];
				/*
				  $rOffer = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "ID" => $arBasket['PRODUCT_ID']), false, false, array("ID", "PROPERTY_CML2_LINK"));
				  if ($arOffer = $rOffer->Fetch())
				  {
				  $rElement = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $arOffer['PROPERTY_CML2_LINK_VALUE']), false, false, array("ID", "PROPERTY_COLLECTION_TYPE", "PROPERTY_SYS_DISCOUNT_SIZE"));
				  if ($arElement = $rElement->Fetch())
				  {
				  if ($arElement['PROPERTY_SYS_DISCOUNT_SIZE_VALUE'] > 0)
				  {
				  $arResult['SALE'] += $arBasket['PRICE'] * $arBasket['QUANTITY'];
				  } elseif (in_array(ACTUAL_COLLECTION_TYPE, $arElement['PROPERTY_COLLECTION_TYPE_VALUE']))
				  {
				  $arResult['ACTUAL'] += $arBasket['PRICE'] * $arBasket['QUANTITY'];
				  } else
				  {
				  $arResult['SALE'] += $arBasket['PRICE'] * $arBasket['QUANTITY'];
				  }
				  }
				  }
				 * 
				 */
			}
		}
		return $arResult;
	}

}
