<?

namespace Webavk\H2OSport\Cron;

class CatalogCheckSet
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arLinksSections = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start CheckComplectsAll");
		$this->doCheckComplectsAll();
		/*
		  $this->doLog("Start CheckSectionsActive");
		  $this->doCheckSectionsActive(); */
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog)
		{
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doCheckComplectsAll()
	{
		$rComplect = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_SET_IS_SET" => "Y"), false, false, array("ID"));
		while ($arComplect = $rComplect->Fetch())
		{
			$this->doCheckOneComplect($arComplect['ID']);
		}
	}

	function doCheckOneComplect($COMPLECT_ID)
	{
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("BASE", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);
		/*
		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"CATALOG_TYPE",
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		myPrint($arSelect);
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, $arSelect);
		while ($arElement = $rElements->Fetch())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
		}
		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, $arSelect);
		while ($arElement = $rElements->Fetch())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["OFFERS"][$arElement['ID']] = $arElement['ID'];
		}
		if (!empty($arResult["OFFERS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["OFFERS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["OFFERS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["OFFERS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["OFFERS"], array('IBLOCK_ID' => IBLOCK_CATALOG_OFFERS, 'GET_BY_ID' => 'Y'));
		}
		 * 
		 */
		$obElement = new \CIBlockElement();
		$arCurrentComplects = array();
		$rExistsAllComplect = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_SET_ALL, "PROPERTY_LINK_SET_CATALOG" => $COMPLECT_ID), false, false, array("ID", "ACTIVE", "NAME", "PROPERTY_SYS_IDENT"));
		while ($arExistsComplect = $rExistsAllComplect->Fetch())
		{
			$arCurrentComplects[$arExistsComplect['PROPERTY_SYS_IDENT_VALUE']] = $arExistsComplect;
		}
		$fMinPriceComplect = false;
		$fOldMinPrice = false;
		$rComplect = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $COMPLECT_ID), false, false, array("ID", "NAME", "DETAIL_TEXT", "ACTIVE", "PROPERTY_SET_ELEMENTS", "PROPERTY_SET_PRICE", "PROPERTY_SET_DISCOUNT_PERCENT", "PROPERTY_SET_DISCOUNT_RUB"));
		if ($arComplect = $rComplect->Fetch())
		{
			if ($arComplect['ACTIVE'] == "Y")
			{
				$arAllVariants = array();
				$arTotalVariants = array();

				//Загружаем все варианты по товарам (SKU)
				foreach ($arComplect['PROPERTY_SET_ELEMENTS_VALUE'] as $k => $v)
				{
					$arAllVariants[$v] = array(
						"COUNT" => $arComplect['PROPERTY_SET_ELEMENTS_DESCRIPTION'][$k],
						"ITEMS" => array()
					);
					$arTotalVariants[$v] = 0;
					if ($arAllVariants[$v]['COUNT'] < 1)
					{
						$arAllVariants[$v]['COUNT'] = 1;
					}
					$rSkuElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $v, "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y"), false, false, array("ID", "NAME", "CATALOG_GROUP_" . H2OSPORT_BASE_PRICE_ID));
					while ($arSkuElement = $rSkuElements->Fetch())
					{
						$arSkuElement["PRICES"] = \CCatalogProduct::GetOptimalPrice($arSkuElement['ID'], 1, array(2));
						$arAllVariants[$v]['ITEMS'][$arSkuElement['ID']] = $arSkuElement;
						$arTotalVariants[$v] ++;
					}
				}

				//Генерируем все варианты комплектов
				$arCurrent = array();
				$bCheck = true;
				foreach ($arTotalVariants as $k => $v)
				{
					if ($v <= 0)
					{
						$bCheck = false;
					}
					$arCurrent[$k] = 1;
				}
				if ($bCheck)
				{
					$bComplete = false;
					do
					{
						$fTotalPrice = 0;
						$arSkuData = array();
						foreach ($arCurrent as $k => $v)
						{
							$ak = array_keys($arAllVariants[$k]['ITEMS']);
							$arSkuData[$ak[$v - 1]] = $arAllVariants[$k]['COUNT'];
							$fTotalPrice+=$arAllVariants[$k]['ITEMS'][$ak[$v - 1]]['PRICES']['RESULT_PRICE']['DISCOUNT_PRICE'] * $arAllVariants[$k]['COUNT'];
							//$fTotalPrice += $arAllVariants[$k]['ITEMS'][$ak[$v - 1]]['CATALOG_PRICE_' . H2OSPORT_BASE_PRICE_ID] * $arAllVariants[$k]['COUNT'];
						}
						if ($fOldMinPrice === false)
						{
							$fOldMinPrice = $fTotalPrice;
						} else if ($fOldMinPrice > $fTotalPrice)
						{
							$fOldMinPrice = $fTotalPrice;
						}
						if ($arComplect['PROPERTY_SET_PRICE_VALUE'] > 0)
						{
							$fTotalPrice = $arComplect['PROPERTY_SET_PRICE_VALUE'];
						} elseif ($arComplect['PROPERTY_SET_DISCOUNT_PERCENT_VALUE'] > 0)
						{
							$fTotalPrice = $fTotalPrice * ((100 - $arComplect['PROPERTY_SET_DISCOUNT_PERCENT_VALUE']) / 100);
						} elseif ($arComplect['PROPERTY_SET_DISCOUNT_RUB_VALUE'] > 0)
						{
							$fTotalPrice = $fTotalPrice - $arComplect['PROPERTY_SET_DISCOUNT_RUB_VALUE'];
							if ($fTotalPrice <= 0)
							{
								$fTotalPrice = 1;
							}
						}
						if ($fMinPriceComplect === false)
						{
							$fMinPriceComplect = $fTotalPrice;
						} else if ($fMinPriceComplect > $fTotalPrice)
						{
							$fMinPriceComplect = $fTotalPrice;
						}
						ksort($arSkuData, SORT_NUMERIC);
						//Проверяем идентификатор есть ли такой набор
						$arIdent = array();
						foreach ($arSkuData as $k => $v)
						{
							$arIdent[] = $k . "=" . $v;
						}
						$strIdent = implode("/", $arIdent);
						if (!isset($arCurrentComplects[$strIdent]))
						{
							$arFields = array(
								"IBLOCK_ID" => IBLOCK_SET_ALL,
								"NAME" => $arComplect['NAME'],
								"ACTIVE" => "Y",
								"PROPERTY_VALUES" => array(
									"LINK_SET_CATALOG" => $arComplect['ID'],
									"SYS_IDENT" => $strIdent
								)
							);
							$newID = $obElement->Add($arFields);
							if ($newID > 0)
							{
								$bResult = \CCatalogProduct::Add(array(
											"ID" => $newID,
											"TYPE" => \Bitrix\Catalog\ProductTable::TYPE_PRODUCT,
											"WEIGHT" => 0,
											"VAT_ID" => H2OSPORT_VAT_ID,
											"VAT_INCLUDED" => "Y",
											"QUANTITY" => 0,
											"CAN_BUY_ZERO" => "D",
											"QUANTITY_TRACE" => "D",
											"NEGATIVE_AMOUNT_TRACE" => "D"
								));
								if ($bResult)
								{
									$arNewComplectData = array(
										'TYPE' => \CCatalogProductSet::TYPE_SET,
										'ITEM_ID' => $newID,
										'ACTIVE' => "Y",
										'ITEMS' => array()
									);
									foreach ($arSkuData as $k => $v)
									{
										$arNewComplectData['ITEMS'][] = array(
											"ITEM_ID" => $k,
											"QUANTITY" => $v,
											"SORT" => 100,
											"DISCOUNT_PERCENT" => false
										);
									}
									//myPrint($arNewComplectData);
									$bNewComplect = \CCatalogProductSet::add($arNewComplectData);
									\CPrice::SetBasePrice($newID, $fTotalPrice, "RUB");
									//$this->SetProductOldPrice($newID, $fOldPrice, "RUB");
									//OLD_PRICE_ID
									//$fOldPrice
									\CCatalogProductSet::recalculateSetsByProduct($newID);
								}
							}
						} else
						{
							\CPrice::SetBasePrice($arCurrentComplects[$strIdent]['ID'], $fTotalPrice, "RUB");
							//$this->SetProductOldPrice($arCurrentComplects[$strIdent]['ID'], $fOldPrice, "RUB");
							\CCatalogProductSet::recalculateSetsByProduct($arCurrentComplects[$strIdent]['ID']);
							unset($arCurrentComplects[$strIdent]);
						}
						$arCurrent = $this->doNextArrayValue($arCurrent, $arTotalVariants);
						if (!$arCurrent)
						{
							$bComplete = true;
						}
					} while (!$bComplete);
					\CPrice::SetBasePrice($arComplect['ID'], $fMinPriceComplect, "RUB");
					$this->SetProductOldPrice($arComplect['ID'], $fOldMinPrice, "RUB");
				}
			}
		}
		foreach ($arCurrentComplects as $arExistsComplect)
		{
			$obElement->Delete($arExistsComplect['ID']);
		}
	}

	function doNextArrayValue($arCurrent, $arMax)
	{
		$bFullCheck = true;
		$arKeys = array_keys($arCurrent);
		foreach ($arKeys as $key)
		{
			$arCurrent[$key] ++;
			if ($arCurrent[$key] <= $arMax[$key])
			{
				$bFullCheck = false;
				break;
			} else
			{
				$arCurrent[$key] = 1;
			}
		}
		if ($bFullCheck)
		{
			$arCurrent = false;
		}
		return $arCurrent;
	}

	public function SetProductOldPrice($ProductID, $Price, $Currency, $quantityFrom = false, $quantityTo = false, $bGetID = false)
	{
		$bGetID = ($bGetID == true);

		$arFields = array();
		$arFields["PRICE"] = (float) $Price;
		$arFields["CURRENCY"] = $Currency;
		$arFields["QUANTITY_FROM"] = ($quantityFrom == false ? false : (int) $quantityFrom);
		$arFields["QUANTITY_TO"] = ($quantityTo == false ? false : (int) $quantityTo);
		$arFields["EXTRA_ID"] = false;

		if ($arBasePrice = $this->GetProductOldPrice($ProductID, $quantityFrom, $quantityTo, false))
		{
			if ($Price === false)
			{
				\CPrice::Delete($arBasePrice["ID"]);
			} else
			{
				$ID = \CPrice::Update($arBasePrice["ID"], $arFields);
			}
		} else
		{
			//$arBaseGroup = CCatalogGroup::GetBaseGroup();
			$arFields["CATALOG_GROUP_ID"] = OLD_PRICE_ID; //$arBaseGroup["ID"];
			$arFields["PRODUCT_ID"] = $ProductID;

			$ID = \CPrice::Add($arFields);
		}
		if (!$ID)
			return false;

		return ($bGetID ? $ID : true);
	}

	function GetProductOldPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int) $productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => OLD_PRICE_ID
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int) $quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int) $quantityTo;

		if ($boolExt === false)
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID'
			);
		} else
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID'
			);
		}

		$db_res = \CPrice::GetListEx(
						array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch())
		{
			return $res;
		}

		return false;
	}

}
