<?

namespace Webavk\H2OSport\Cron;

class CatalogCheckSale
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arLinksSections = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start CheckSaleSections");
		$this->doCheckSaleSections();
		$this->doLog("Start CheckSectionsActive");
		$this->doCheckSectionsActive();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog)
		{
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doCheckSaleSectionsStructure($ID, $SALE_ID)
	{
		$obSection = new \CIBlockSection();
		$rParentSections = \CIBlockSection::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $ID));
		while ($arParentSection = $rParentSections->Fetch())
		{
			if ($arParentSection['ID'] == SALE_SECTION_ID)
			{
				continue;
			}
			$arElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_LABEL_SALE" => "Y", "SECTION_ID" => $arParentSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"))->Fetch();
			if ($arElements)
			{
				//Проверяем наличие раздела в sale и создаем
				$arSaleSection = \CIBlockSection::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $SALE_ID, "NAME" => $arParentSection['NAME']))->Fetch();
				if ($arSaleSection)
				{
					$this->arLinksSections[$arParentSection['ID']] = $arSaleSection['ID'];
					if ($arSaleSection["ACTIVE"] != "Y")
					{
						$obSection->Update($arSaleSection['ID'], array("ACTIVE" => "Y"));
					}
					$this->doCheckSaleSectionsStructure($arParentSection['ID'], $arSaleSection['ID']);
				} else
				{
					$arFields = array(
						"IBLOCK_ID" => IBLOCK_CATALOG,
						"ACTIVE" => "Y",
						"NAME" => $arParentSection['NAME'],
						"SORT" => $arParentSection['SORT'],
						"CODE" => $arParentSection['CODE'],
						"IBLOCK_SECTION_ID" => $SALE_ID
					);
					$SECTION_ID = $obSection->Add($arFields);
					if ($SECTION_ID > 0)
					{
						$this->arLinksSections[$arParentSection['ID']] = $SECTION_ID;
						$this->doCheckSaleSectionsStructure($arParentSection['ID'], $SECTION_ID);
					}
				}
			}
		}
		unset($obSection);
	}

	function doCheckSaleSections()
	{
		$obSection = new \CIBlockSection();
		$this->doCheckSaleSectionsStructure(false, SALE_SECTION_ID);
		$arExistSections = array();
		$rTree = \CIBlockSection::GetTreeList(array("IBLOCK_ID" => IBLOCK_CATALOG));
		$bSave = false;
		while ($arTree = $rTree->Fetch())
		{
			if ($arTree['ID'] == SALE_SECTION_ID && $arTree['DEPTH_LEVEL'] == 1)
			{
				$bSave = true;
			} elseif ($arTree['ID'] != SALE_SECTION_ID && $arTree['DEPTH_LEVEL'] == 1)
			{
				$bSave = false;
			} elseif ($bSave)
			{
				$arExistSections[$arTree['ID']] = $arTree['ID'];
			}
		}
		//Удаляем товары из безраспродажных разделов и деактивируем их
		foreach ($arExistSections as $SECTION_ID)
		{
			if (!isset($this->arLinksSections[$SECTION_ID]))
			{
				$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $SECTION_ID), false, false, array("ID"));
				while ($arElement = $rElements->Fetch())
				{
					$this->doDeleteElementFromSection($arElement['ID'], $SECTION_ID);
				}
				//$obSection->Update($SECTION_ID, array("ACTIVE" => "N"));
			}
		}
		//Проходимся по разделам и добавляем 
		foreach ($this->arLinksSections as $SECTION_ID => $SALE_SECTION_ID)
		{
			$allElementsInSection = array();
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $SALE_SECTION_ID), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				$allElementsInSection[$arElement['ID']] = $arElement['ID'];
			}
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $SECTION_ID, "PROPERTY_LABEL_SALE" => "Y"), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				unset($allElementsInSection[$arElement['ID']]);
				$this->doAddElementToSection($arElement['ID'], $SALE_SECTION_ID);
			}
			foreach ($allElementsInSection as $elementId)
			{
				$this->doDeleteElementFromSection($elementId, $SALE_SECTION_ID);
			}
		}
		/*
		  $arNewSections = array();
		  $arLinks = array();
		  $rTree = \CIBlockSection::GetTreeList(array("IBLOCK_ID" => IBLOCK_CATALOG), array("UF_*"));
		  while ($arTree = $rTree->Fetch())
		  {
		  if (!isset($arLinks[$arTree['IBLOCK_SECTION_ID']]))
		  {
		  $arNewSections[$arTree['ID']] = $arTree;
		  $arLinks[$arTree['ID']] = &$arNewSections[$arTree['ID']];
		  } else
		  {
		  $arLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']] = $arTree;
		  $arLinks[$arTree['ID']] = &$arLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']];
		  }
		  }
		  $arIsSaleSections = array();
		  foreach ($arLinks as $k => $v)
		  {
		  $arElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_LABEL_SALE" => "Y", "SECTION_ID" => $v['ID']), false, false, array("ID"))->Fetch();
		  if ($arElements)
		  {
		  $arIsSaleSections[$v['ID']] = $v['ID'];
		  $iSection = $arLinks[$v['ID']]['IBLOCK_SECTION_ID'];
		  while ($iSection > 0)
		  {
		  $arIsSaleSections[$iSection] = $iSection;
		  $iSection = $arLinks[$iSection]['IBLOCK_SECTION_ID'];
		  }
		  }
		  }
		  foreach ($arLinks as $k => $v)
		  {
		  if ($v['UF_IS_SALE'] == 1)
		  {
		  if (!isset($arIsSaleSections[$v['ID']]))
		  {
		  $obSection->Update($v['ID'], array("UF_IS_SALE" => 0));
		  }
		  } else
		  {
		  if (isset($arIsSaleSections[$v['ID']]))
		  {
		  $obSection->Update($v['ID'], array("UF_IS_SALE" => 1));
		  }
		  }
		  }
		 * 
		 */
	}

	function doAddElementToSection($iElementId, $iSectionId)
	{
		$obElement = new \CIBlockElement();
		$arElementSections = array();
		$rSections = $obElement->GetElementGroups($iElementId, true, array("ID"));
		while ($arSection = $rSections->Fetch())
		{
			$arElementSections[] = $arSection['ID'];
		}
		$arElementSections[] = $iSectionId;
		$obElement->SetElementSection($iElementId, $arElementSections);
		unset($obElement);
	}

	function doDeleteElementFromSection($iElementId, $iSectionId)
	{
		$obElement = new \CIBlockElement();
		$arElementSections = array();
		$rSections = $obElement->GetElementGroups($iElementId, true, array("ID"));
		while ($arSection = $rSections->Fetch())
		{
			if ($arSection['ID'] != $iSectionId)
			{
				$arElementSections[] = $arSection['ID'];
			}
		}
		$obElement->SetElementSection($iElementId, $arElementSections);
		unset($obElement);
	}

	function doCheckSectionsActive()
	{
		$obSection = new \CIBlockSection();
		$obElements = new \CIBlockElement();
		$rSections = \CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG), false);
		while ($arSection = $rSections->Fetch())
		{
			/*
			  if ($arSection['ID'] == DEFAULT_CATEGORY_ID)
			  {
			  continue;
			  } */
			$arElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y", "CATALOG_AVAILABLE" => "Y"), array("IBLOCK_ID"))->Fetch();
			if ($arElements['CNT'] > 0)
			{
				if ($arSection['ACTIVE'] != "Y")
				{
					$obSection->Update($arSection['ID'], array("ACTIVE" => "Y"));
				}
				//Активируем товары в разделе
				/* $rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "N", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
				  while ($arElement = $rElements->Fetch())
				  {
				  //if (in_array($arElement['ID'], $this->arActiveProductsSet))
				  //{
				  $obElements->Update($arElement['ID'], array("ACTIVE" => "Y"));
				  //}
				  } */
			} else
			{
				if ($arSection['ACTIVE'] == "Y")
				{
					$obSection->Update($arSection['ID'], array("ACTIVE" => "N"));
				}
				//Деактивируем товары в разделе
				/* $rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
				  while ($arElement = $rElements->Fetch())
				  {
				  $obElements->Update($arElement['ID'], array("ACTIVE" => "N"));
				  }
				 * 
				 */
			}
		}
	}
}
