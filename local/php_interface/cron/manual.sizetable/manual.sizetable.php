<?

namespace Webavk\H2OSport\Cron;

class ManualSizetable
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arArticlesToOldSections = array();
	protected $arRedirects = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$arCurrentItemsSize = array();
		$arOldItemsSize = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => 33));
		while ($arElement = $rElements->Fetch())
		{
			$arCurrentItemsSize[$arElement['XML_ID']] = $arElement['ID'];
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => 11));
		while ($arElement = $rElements->Fetch())
		{
			$arOldItemsSize[$arElement['ID']] = $arElement['XML_ID'];
		}
		//Проходимся по старому каталогу. Если есть ссылка на таблицу размеров, то ищем товар в текущем каталоге и прописываем туда
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => 14), false, false, array("ID", "XML_ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_SIZE_TABLE"));
		while ($arElement = $rElements->Fetch())
		{
			if ($arElement['PROPERTY_SIZE_TABLE_VALUE'] > 0 && strlen($arElement['PROPERTY_CML2_ARTICLE_VALUE']) > 0)
			{
				$rNewElement = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => 16, "PROPERTY_CML2_ARTICLE" => $arElement['PROPERTY_CML2_ARTICLE_VALUE']), false, false, array("ID"));
				if ($arNewElement = $rNewElement->Fetch())
				{
					$iVal = $arCurrentItemsSize[$arOldItemsSize[$arElement['PROPERTY_SIZE_TABLE_VALUE']]];
					if ($iVal > 0)
					{
						\CIBlockElement::SetPropertyValueCode($arNewElement['ID'], "SIZE_TABLE", $iVal);
					}
				}
			}
		}
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

}
