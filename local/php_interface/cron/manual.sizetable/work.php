<?

$GLOBALS['WEBAVK_CRON_CURRENT_REL_FILE'] = "/local/php_interface/cron/manual.sizetable/work.php";
define("WEBAVK_CRON_UNIQ_IDENT", "manual.sizetable");
define("WEBAVK_CRON_USE_LOCKRUN", false);
define("WEBAVK_CRON_USE_LOCK", true);
include(dirname(__FILE__) . "/../include.php");

declare(ticks = 1);
pcntl_async_signals(true);
pcntl_signal(SIGTERM, 'sig_handler');
pcntl_signal(SIGINT, 'sig_handler');
pcntl_signal(SIGCHLD, 'sig_handler');

function sig_handler($sig)
{
	switch ($sig)
	{
		case SIGTERM:
		case SIGINT:
			$GLOBALS['CNT_SIGTERM'] ++;
			if ($GLOBALS['CNT_SIGTERM'] > 3)
				exit();
			if (!defined("WEBAVK_SIGTERM"))
			{
				define("WEBAVK_SIGTERM", true);
				if (is_object($GLOBALS['CRON_OBJECT']))
				{
					$GLOBALS['CRON_OBJECT']->Stop();
				}
			}
			break;
		case SIGCHLD:
			pcntl_waitpid(-1, $status);
			break;
	}
}

define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

set_time_limit(0);
@ini_set("memory_limit", "2560M");
error_reporting(E_ERROR);
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("highloadblock");
CModule::IncludeModule("webavk.h2osport");

include_once(dirname(__FILE__) . "/manual.sizetable.php");

$arOptions = include(dirname(__FILE__) . "/.options.php");
$GLOBALS['CRON_OBJECT'] = new \WebAVK\H2OSport\Cron\ManualSizetable($arOptions, $_SERVER['argv'][1] == 'log' ? true : false);

$GLOBALS['CRON_OBJECT']->Start();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
