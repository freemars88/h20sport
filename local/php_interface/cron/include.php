<?

define("STOP_STATISTICS", true);
error_reporting(7);
if (!defined("WEBAVK_CRON_UNIQ_IDENT") || strlen(WEBAVK_CRON_UNIQ_IDENT) < 1)
	die("Not set constant WEBAVK_CRON_UNIQ_IDENT");
if (strlen($_SERVER['PATH_TRANSLATED']) > 0) {
	$s = $_SERVER['PATH_TRANSLATED'];
} else {
	$s = __FILE__;
}
$pos = strpos($s, $GLOBALS['WEBAVK_CRON_CURRENT_REL_FILE']);
$_SERVER["DOCUMENT_ROOT"] = substr($s, 0, $pos);
$_SERVER["SCRIPT_NAME"] = $GLOBALS['WEBAVK_CRON_CURRENT_REL_FILE'];
$_SERVER["SCRIPT_FILENAME"] = $_SERVER["DOCUMENT_ROOT"] . $GLOBALS['WEBAVK_CRON_CURRENT_REL_FILE'];
chdir($_SERVER['DOCUMENT_ROOT']);

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/")) {
	mkdir($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/", 0777);
	chmod($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/", 0777);
}

if (defined("WEBAVK_CRON_USE_LOCKRUN") && WEBAVK_CRON_USE_LOCKRUN) {
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/" . WEBAVK_CRON_UNIQ_IDENT . ".lockrun")) {
		die();
	}
	if ($f = fopen($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/" . WEBAVK_CRON_UNIQ_IDENT . ".lockrun", "w+")) {
		fwrite($f, date("Y-m-d H:i:s"));
	}
	fclose($f);
	chmod($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/" . WEBAVK_CRON_UNIQ_IDENT . ".lockrun", 0777);
}

if (defined("WEBAVK_CRON_USE_LOCK") && WEBAVK_CRON_USE_LOCK) {
	$UNLINK_LOCK_FILE = true;

	function __EndScript()
	{
		global $UNLINK_LOCK_FILE;
		if ($UNLINK_LOCK_FILE)
			@unlink($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/" . WEBAVK_CRON_UNIQ_IDENT . ".lock");
	}

	register_shutdown_function("__EndScript");
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/" . WEBAVK_CRON_UNIQ_IDENT . ".lock")) {
		$UNLINK_LOCK_FILE = false;
		die();
	}
	if ($f = fopen($_SERVER['DOCUMENT_ROOT'] . "/local/cron-run/" . WEBAVK_CRON_UNIQ_IDENT . ".lock", "w+")) {
		fwrite($f, date("Y-m-d H:i:s"));
	}
	fclose($f);
}

if (!function_exists("pcntl_signal")){
	function pcntl_signal()
{

}
}