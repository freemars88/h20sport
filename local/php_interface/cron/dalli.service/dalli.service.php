<?

namespace Webavk\Francesco\Cron;

class DalliService
{

	protected $bIsStop = false;
	protected $arOptions = array();

	function __construct($arOptions)
	{
		$this->arOptions = $arOptions;
	}

	function Start()
	{
		//Проверка автоотправок
		$rSendData = \WebAVK\DalliService\OrdersTable::getList(array(
					"order" => array("ORDER_ID" => "ASC"),
					"filter" => array("IS_SEND" => "N", "ALLOW_AUTO_SEND" => "Y")
		));
		while ($arSendData = $rSendData->fetch())
		{
			if (function_exists("IsAllowSendOrderToDalliService"))
			{
				if (IsAllowSendOrderToDalliService($arSendData['ORDER_ID']))
				{
					\CDeliveryServiceDalliDriver::doSendOrder($arSendData['ID']);
				}
			} else
			{
				\CDeliveryServiceDalliDriver::doSendOrder($arSendData['ID']);
			}
		}
		//Проверка статусов
		$rSendData = \WebAVK\DalliService\OrdersTable::getList(array(
					"order" => array("ORDER_ID" => "ASC"),
					"filter" => array("IS_SEND" => "Y", array("LOGIC"=>"OR",array("@STATUS" => array("UN", "NEW", "ACCEPTED", "INVENTORY", "DEPARTURING", "DEPARTURE", "DELIVERY", "COURIERDELIVERED", "COURIERRETURN", "RETURNING", "CONFIRM", "DATECHANGE", "NEWPICKUP", "UNCONFIRM", "PICKUPREADY")),array("STATUS"=>false)), array("LOGIC" => "OR", array("<=DATE_CHECK" => ConvertTimeStamp(time() - 15 * 60, "FULL")), array("DATE_CHECK" => false)), ">=DATE_SEND" => ConvertTimeStamp(time() - 3600 * 24 * 30, "FULL"))
		));
		while ($arSendData = $rSendData->fetch())
		{
			\CDeliveryServiceDalliDriver::doCheckStatus($arSendData['ID']);
		}
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

}
