<?

namespace Webavk\H2OSport\Cron;

class CatalogMake
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $ar1CStructure = array();
	protected $ar1CStructureLinks = array();
	protected $arExistsElements = array();
	protected $arFromToSections = array(-1 => false);
	protected $arGroupElementsFrom1C = array();
	protected $arCacheElementsData = array();
	protected $arExistsProductOffers = array();
	protected $arStorePropVariants = array();
	protected $arNomenklaturaData = array();
	protected $arAfterTestProducts = array();
	protected $arNotChangedElementsID = array();
	protected $arPricesFromFile = array();
	protected $arBlackFridayArticles = array();
	protected $arPhotosDirNormalized = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start doFirstCheckQuantity");
		$this->doFirstCheckQuantity();
		$this->doLog("Start NormalizedPhotosDir");
		$this->doNormalizedPhotosDir();
		$this->doLog("Start LoadPricesFromFile");
		$this->doLoadPricesFromFile();
		//$this->doLog("Start doExportOldPhotos");
		//$this->doExportOldPhotos();
		//$this->doLog("Start doExportOldDescrioption");
		//$this->doExportOldDescription();
		//$this->doLog("Start CheckPhotosProportions");
		//$this->doCheckPhotosProportions();
		$this->doLog("Start doLoadNotChangedData");
		$this->doLoadNotChangedData();
		$this->doLog("Start doLoadNomenklaturaData");
		$this->doLoadNomenklaturaData();
		$this->doLog("Start doMakeCatalogStructure");
		$this->doMakeCatalogStructure();
		$this->doLog("Start LoadExistsElements");
		$this->doLoadExistsElements();
		$this->doLog("Start LoadGroupElementsFrom1C");
		$this->LoadGroupElementsFrom1C();
		$this->doLog("Start MakeElements");
		$this->doMakeElements();
		$this->doLog("Start doCheckElementsActive");
		$this->doCheckElementsActive();
		$this->doLog("Start doCheckElementsPreSale");
		$this->doCheckElementsPreSale();
		$this->doLog("Start doUpdateNames");
		$this->doUpdateNames();
		//$this->doLog("Start doUpdateDescriptions");
		//$this->doUpdateDescriptions();
		$this->doLog("Start CheckSectionsActive");
		$this->doCheckSectionsActive();
		$this->doLog("Start doSetAfterTestProducts");
		$this->doSetAfterTestProducts();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog) {
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doNormalizedPhotosDir()
	{
		$arFiles = scandir($this->arOptions['PHOTOS_DIR']);
		foreach ($arFiles as $strFile) {
			if (in_array($strFile, array(".", ".."))) {
				continue;
			}
			$this->arPhotosDirNormalized[strtolower($strFile)] = $strFile;
		}
	}

	function doLoadPricesFromFile()
	{
		if (file_exists(dirname(__FILE__) . "/files/file.csv")) {
			file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/tmpprices.csv", iconv("windows-1251", "utf-8", file_get_contents(dirname(__FILE__) . "/files/file.csv")));
			$f = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/tmpprices.csv", "r+t");
			if ($f) {
				$ar = fgetcsv($f, 2048, ";", '"');
				while ($ar = fgetcsv($f, 2048, ";", '"')) {
					$this->arPricesFromFile[trim(strtolower($ar[5]))] = array(
						"PRICE" => round(floatval(trim(str_replace(",", ".", str_replace(" ", "", $ar[10])))), -1),
						"OLD_PRICE" => round(floatval(trim(str_replace(",", ".", str_replace(" ", "", $ar[11])))), -1),
					);
				}
				fclose($f);
			}
			@unlink($_SERVER['DOCUMENT_ROOT'] . "/upload/tmpprices.csv");
		}
		/*
		  if (file_exists(dirname(__FILE__) . "/files/file.blackfriday.csv"))
		  {
		  file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/tmpprices.csv", iconv("windows-1251", "utf-8", file_get_contents(dirname(__FILE__) . "/files/file.blackfriday.csv")));
		  $f = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/tmpprices.csv", "r+t");
		  if ($f)
		  {
		  $ar = fgetcsv($f, 2048, ";", '"');
		  while ($ar = fgetcsv($f, 2048, ";", '"'))
		  {
		  $this->arPricesFromFile[trim(strtolower($ar[5]))] = array(
		  "PRICE" => round(floatval(trim(str_replace(",", ".", str_replace(" ", "", $ar[14])))), -1),
		  "OLD_PRICE" => round(floatval(trim(str_replace(",", ".", str_replace(" ", "", $ar[12])))), -1)
		  );
		  $this->arBlackFridayArticles[trim(strtolower($ar[5]))] = trim(strtolower($ar[5]));
		  }
		  fclose($f);
		  }
		  @unlink($_SERVER['DOCUMENT_ROOT'] . "/upload/tmpprices.csv");
		  } */
	}

	function doLoadNotChangedData()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_MANUAL_NOT_CHANGE_SCRIPT" => "Y"), false, false, array("ID"));
		while ($arElement = $rElements->Fetch()) {
			$this->arNotChangedElementsID[] = $arElement['ID'];
		}
		if (!empty($this->arNotChangedElementsID)) {
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $this->arNotChangedElementsID), false, false, array("ID"));
			while ($arElement = $rElements->Fetch()) {
				$this->arNotChangedElementsID[] = $arElement['ID'];
			}
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_SET_IS_SET" => "Y"), false, false, array("ID"));
		while ($arElement = $rElements->Fetch()) {
			$this->arNotChangedElementsID[] = $arElement['ID'];
		}
	}

	function doCheckPhotosProportions()
	{
		$arAllPhotos = array_merge($this->GetPhotosRecursive($this->arOptions['PHOTOS_DIR']));
		foreach ($arAllPhotos as $strFullPath) {
			$this->doCheckPhotoProportions($strFullPath);
		}
	}

	function GetPhotosRecursive($strDir)
	{
		$arResult = array();
		$arPhotos = scandir($strDir);
		foreach ($arPhotos as $strName) {
			if (in_array($strName, array(".", ".."))) {
				continue;
			}
			$strFullName = $strDir . $strName;
			if (is_dir($strFullName)) {
				$arResult = array_merge($arResult, $this->GetPhotosRecursive($strFullName . "/"));
			} elseif (is_file($strFullName)) {
				$arResult[] = $strFullName;
			}
		}
		return $arResult;
	}

	function doCheckPhotoProportions($strPhotoPath)
	{
		$arPath = pathinfo($strPhotoPath);
		$newName = $arPath['dirname'] . "/" . $arPath['filename'] . ".jpg";
		$arSize = getimagesize($strPhotoPath);
		$width = $arSize[0];
		$height = $arSize[1];
		if ($width > 0 && $height > 0) {
			$p = $width / $height;
			$h = intval($height * (1600 / $width));
			$origP = 1600 / 1600;
			if ($p > $origP) {
				$newWidth = $width;
				$newHeight = intval($width / $origP);
			} else {
				$newHeight = $height;
				$newWidth = intval($height * $origP);
			}
			if (intval($p * 100) != intval($origP * 100)) {
				echo $strPhotoPath . "\n";
				$imgOriginal = imagecreatefromstring(file_get_contents($strPhotoPath));
				$img = imagecreatetruecolor($newWidth, $newHeight);
				$c = imagecolorallocate($img, 255, 255, 255);
				imagefill($img, 0, 0, $c);
				imagecopy($img, $imgOriginal, intval(($newWidth - $width) / 2), intval(($newHeight - $height) / 2), 0, 0, $width, $height);
				@unlink($strPhotoPath);
				imagejpeg($img, $newName, 90);
				imagedestroy($img);
			}
		}
	}

	function doMakeCatalogStructure()
	{
		$rSectionsTree = \CIBlockSection::GetList(array("left_margin" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG_1C), false, array("UF_*"));
		while ($arSection = $rSectionsTree->Fetch()) {
			if ($arSection['IBLOCK_SECTION_ID'] > 0) {
				$this->ar1CStructureLinks[$arSection['IBLOCK_SECTION_ID']]['CHILD'][$arSection['ID']] = $arSection;
				$this->ar1CStructureLinks[$arSection['ID']] = &$this->ar1CStructureLinks[$arSection['IBLOCK_SECTION_ID']]['CHILD'][$arSection['ID']];
			} else {
				$this->ar1CStructure[$arSection['ID']] = $arSection;
				$this->ar1CStructureLinks[$arSection['ID']] = &$this->ar1CStructure[$arSection['ID']];
			}
		}
		foreach ($this->ar1CStructure as $ID => $arSectionLink) {
			$this->doMakeCatalogFrom1CSection($arSectionLink['ID']);
		}
	}

	function doMakeCatalogFrom1CSection($ID)
	{
		$obSection = new \CIBlockSection();
		$arCurrentSection = &$this->ar1CStructureLinks[$ID];
		if ($arCurrentSection['UF_IS_LOAD']) {
			$bUpdateSection = $arCurrentSection['B_UPDATE'];
			if ($arCurrentSection['UF_TO_SECTIONS'] > 0) {
				$arSect = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $arCurrentSection['UF_TO_SECTIONS']))->Fetch();
				if (!$arSect) {
					$arCurrentSection['UF_TO_SECTIONS'] = false;
				}
			}
			if ($arCurrentSection['UF_TO_SECTIONS'] <= 0) {
				$parentSectionId = $this->ar1CStructureLinks[$arCurrentSection['IBLOCK_SECTION_ID']]['UF_TO_SECTIONS'];
				$arFields = array(
					"IBLOCK_ID" => IBLOCK_CATALOG,
					"ACTIVE" => "Y",
					"NAME" => $arCurrentSection['NAME'],
					"CODE" => $this->doTranslitName($arCurrentSection['NAME']),
					"IBLOCK_SECTION_ID" => $parentSectionId,
				);
				$arCurrentSection['UF_TO_SECTIONS'] = $obSection->Add($arFields);
				$bUpdateSection = true;
			}
			$this->arFromToSections[$arCurrentSection['ID']] = $arCurrentSection['UF_TO_SECTIONS'];
			if ($bUpdateSection) {
				$obSection->Update($arCurrentSection['ID'], array(
					"UF_TO_SECTIONS" => $arCurrentSection['UF_TO_SECTIONS'],
					//"UF_IS_LOAD" => $arCurrentSection['UF_IS_LOAD'],
					//"UF_CHILD_STRUCTURE" => $arCurrentSection['UF_CHILD_STRUCTURE']
				));
			}

			if (isset($arCurrentSection['CHILD']) && !empty($arCurrentSection['CHILD'])) {
				foreach ($arCurrentSection['CHILD'] as $k => $arChildSection) {
					if ($arCurrentSection['CHILD'][$k]['UF_IS_LOAD'] != 1) {
						$arCurrentSection['CHILD'][$k]['UF_IS_LOAD'] = 1;
						$arCurrentSection['CHILD'][$k]['B_UPDATE'] = true;
					}
					if ($arCurrentSection['UF_CHILD_STRUCTURE'] == 1) {
						if ($arCurrentSection['CHILD'][$k]['UF_CHILD_STRUCTURE'] != 1) {
							$arCurrentSection['CHILD'][$k]['UF_CHILD_STRUCTURE'] = 1;
							$arCurrentSection['CHILD'][$k]['B_UPDATE'] = true;
						}
					} else {
						if ($arCurrentSection['CHILD'][$k]['UF_TO_SECTIONS'] <= 0) {
							$arCurrentSection['CHILD'][$k]['UF_TO_SECTIONS'] = $arCurrentSection['UF_TO_SECTIONS'];
						}
					}
					$this->doMakeCatalogFrom1CSection($arChildSection['ID']);
				}
			}
		}
		if (isset($arCurrentSection['CHILD']) && !empty($arCurrentSection['CHILD'])) {
			foreach ($arCurrentSection['CHILD'] as $arChildSection) {
				$this->doMakeCatalogFrom1CSection($arChildSection['ID']);
			}
		}
		unset($obSection);
	}

	function doTranslitName($strName)
	{
		$strName = trim($strName);
		return \CUtil::translit($strName, "ru", array(
			"max_len" => 100,
			"change_case" => 'L',
			"replace_space" => '-',
			"replace_other" => '-',
			"delete_repeat_replace" => true,
			"safe_chars" => '',
		));
	}

	/**
	 * Загружаем список ID элементов каталога
	 */
	function doLoadExistsElements()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID"));
		while ($arElement = $rElements->Fetch()) {
			if (!in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				$this->arExistsElements['ELEMENTS'][$arElement['ID']] = $arElement['ID'];
			}
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID"));
		while ($arElement = $rElements->Fetch()) {
			if (!in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				$this->arExistsElements['OFFERS'][$arElement['ID']] = $arElement['ID'];
			}
		}
	}

	function LoadGroupElementsFrom1C()
	{
		$rElements = \CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG_1C, "SECTION_ID" => array_keys($this->arFromToSections), "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID", "PROPERTY_CML2_ARTICLE"));
		while ($arElement = $rElements->Fetch()) {
			$this->arGroupElementsFrom1C[trim(strtolower($arElement['PROPERTY_CML2_ARTICLE_VALUE']))][] = $arElement['ID'];
		}
	}

	function doMakeElements()
	{
		$i = 1;
		$cnt = count($this->arGroupElementsFrom1C);
		foreach ($this->arGroupElementsFrom1C as $article => $arItemsID) {
			$iMainID = $this->doCheckMainElement($article, $arItemsID[0]);
			if ($iMainID > 0) {
				$this->doMakeOffers($arItemsID, $iMainID, $article);
			}
			unset($this->arCacheElementsData);
			$this->doLog("Make element " . $i . " of " . $cnt);
			$i++;
		}
	}

	function doExportOldPhotos()
	{
		$arAllPhotos = array();
		$rOldProducts = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_OLD_CATALOG), false, false, array("ID"));
		while ($arOldProducts = $rOldProducts->Fetch()) {
			$arOldElement = $this->getElementData($arOldProducts['ID']);
			$strArticle = $arOldElement['PROPERTIES']['CML2_ARTICLE']['VALUE'];
			$i = 1;
			$picture = false;
			if ($arOldElement['DETAIL_PICTURE'] > 0) {
				$picture = $arOldElement['DETAIL_PICTURE'];
			} elseif ($arOldElement['PREVIEW_PICTURE'] > 0) {
				$picture = $arOldElement['PREVIEW_PICTURE'];
			}
			if ($picture > 0) {
				$arAllPhotos[$strArticle][] = $picture;
				$i++;
			}
			if (!empty($arOldElement['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
				$arPhotoList = $arOldElement['PROPERTIES']['MORE_PHOTO']['VALUE'];
				if (!is_array($arPhotoList)) {
					$arPhotoList = array($arPhotoList);
				}
				foreach ($arPhotoList as $photo) {
					$arAllPhotos[$strArticle][] = $photo;
					$i++;
				}
			}
			//Проходимся по торгпредам
			$rOldElementOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_OLD_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $arOldElement['ID']), false, false, array("ID"));
			while ($arOldElementOffers = $rOldElementOffers->Fetch()) {
				if ($arOldElementOffers) {
					$arOldElementOffers = $this->getElementData($arOldElementOffers['ID']);
					if (!empty($arOldElementOffers['PROPERTIES']['PICTURES']['VALUE'])) {
						$arPhotoList = $arOldElementOffers['PROPERTIES']['PICTURES']['VALUE'];
						if (!is_array($arPhotoList)) {
							$arPhotoList = array($arPhotoList);
						}
						foreach ($arPhotoList as $photo) {
							$arAllPhotos[$strArticle][] = $photo;
							$i++;
						}
					}
				}
			}
		}

		foreach ($arAllPhotos as $strArticle => $arPhotos) {
			$strArticle = $this->doNormalizeArticlePhoto($strArticle);
			$strDir = $this->arOptions['PHOTOS_DIR'] . $strArticle . "/";
			CheckDirPath($strDir);
			$arNewPhotos = array();
			foreach ($arPhotos as $k => $v) {
				$strPath = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($v);
				$hash = filesize($strPath) . "|" . md5_file($strPath) . "|" . crc32(file_get_contents($strPath));
				$arNewPhotos[$hash] = $strPath;
			}
			$i = 1;
			foreach ($arNewPhotos as $strPath) {
				$arParse = pathinfo($strPath);
				$strNewPath = $strDir . $i . "." . $arParse['extension'];
				copy($strPath, $strNewPath);
				$i++;
			}
		}
	}

	function doExportOldDescription()
	{
		$arAllPhotos = array();
		CheckDirPath($this->arOptions['DESCRIPTION_DIR']);
		$rOldProducts = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_OLD_CATALOG), false, false, array("ID", "PROPERTY_CML2_ARTICLE", "DETAIL_TEXT"));
		while ($arOldProducts = $rOldProducts->Fetch()) {
			$strArticle = $arOldProducts['PROPERTY_CML2_ARTICLE_VALUE'];
			$strArticle = $this->doNormalizeArticlePhoto($strArticle);
			if (strlen($arOldProducts['DETAIL_TEXT']) > 0) {
				$strDescription = $arOldProducts['DETAIL_TEXT'];
				$strFile = $this->arOptions['DESCRIPTION_DIR'] . $strArticle . ".html";
				file_put_contents($strFile, $strDescription);
			}
		}
	}

	function doCheckMainElement($strArticle, $i1CElementId)
	{
		$iResult = false;
		$ar1CElement = $this->getElementData($i1CElementId);
		$obElement = new \CIBlockElement();
		$rElement = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_CML2_ARTICLE" => $strArticle), false, false, array("ID"));
		if ($arElement = $rElement->Fetch()) {
			$iResult = $arElement['ID'];
		} else {
			if (strpos(strtolower($ar1CElement['NAME']), "прокат") !== false) {
				return false;
			}
			//Ищем и загружаем товар со старого сайта
			$arOldElement = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_OLD_CATALOG, "PROPERTY_CML2_ARTICLE" => $strArticle), false, false, array("ID"))->Fetch();
			if ($arOldElement) {
				$arOldElement = $this->getElementData($arOldElement['ID']);
			}
			$arFields = array(
				"IBLOCK_ID" => IBLOCK_CATALOG,
				"ACTIVE" => "Y",
				"NAME" => $ar1CElement['NAME'],
				"CODE" => $this->doTranslitName($ar1CElement['NAME']),
				"IBLOCK_SECTION_ID" => $this->arFromToSections[$ar1CElement['IBLOCK_SECTION_ID']],
				"PREVIEW_TEXT" => $arOldElement['~PREVIEW_TEXT'],
				"PREVIEW_TEXT_TYPE" => $arOldElement['~PREVIEW_TEXT_TYPE'],
				"DETAIL_TEXT" => $arOldElement['~DETAIL_TEXT'],
				"DETAIL_TEXT_TYPE" => $arOldElement['~DETAIL_TEXT_TYPE'],
				"PROPERTY_VALUES" => array(
					"CML2_ARTICLE" => $strArticle,
					"SYS_ORIGINAL_NAME" => $ar1CElement['NAME'],
					//"BRAND"=>$this->doCheckProperty($iElementIdCatalog, $strPropertyCode, $arProperty1C, $arPropertyCatalog)
				),
			);
			$iResult = $obElement->Add($arFields);
			if ($iResult) {
				if ($arOldElement) {
					//Сохраняем фото если каталог с фото пустой
					$strDir = $this->arOptions['PHOTOS_DIR'] . $this->doNormalizeArticlePhoto($strArticle) . "/";
					$i = 1;
					$picture = false;
					if ($arOldElement['DETAIL_PICTURE'] > 0) {
						$picture = $arOldElement['DETAIL_PICTURE'];
					} elseif ($arOldElement['PREVIEW_PICTURE'] > 0) {
						$picture = $arOldElement['PREVIEW_PICTURE'];
					}
					if ($picture > 0) {
						CheckDirPath($strDir);
						$strPath = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($picture);
						$arParse = pathinfo($strPath);
						$strNewPath = $strDir . $i . "." . $arParse['extension'];
						copy($strPath, $strNewPath);
						$i++;
					}
					if (!empty($arOldElement['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
						CheckDirPath($strDir);
						$arPhotoList = $arOldElement['PROPERTIES']['MORE_PHOTO']['VALUE'];
						if (!is_array($arPhotoList)) {
							$arPhotoList = array($arPhotoList);
							foreach ($arPhotoList as $photo) {
								$strPath = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($photo);
								$arParse = pathinfo($strPath);
								$strNewPath = $strDir . $i . "." . $arParse['extension'];
								copy($strPath, $strNewPath);
								$i++;
							}
						}
					}
					//Проходимся по торгпредам
					$rOldElementOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_OLD_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $arOldElement['ID']), false, false, array("ID"));
					while ($arOldElementOffers = $rOldElementOffers->Fetch()) {
						if ($arOldElementOffers) {
							$arOldElementOffers = $this->getElementData($arOldElementOffers['ID']);
							if (!empty($arOldElementOffers['PROPERTIES']['PICTURES']['VALUE'])) {
								CheckDirPath($strDir);
								$arPhotoList = $arOldElementOffers['PROPERTIES']['PICTURES']['VALUE'];
								if (!is_array($arPhotoList)) {
									$arPhotoList = array($arPhotoList);
									foreach ($arPhotoList as $photo) {
										$strPath = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($photo);
										$arParse = pathinfo($strPath);
										$strNewPath = $strDir . $i . "." . $arParse['extension'];
										copy($strPath, $strNewPath);
										$i++;
									}
								}
							}
						}
					}
				}
			}
		}
		if ($iResult) {
			$arCatalogElement = $this->getElementData($iResult, true);
			if ($arCatalogElement['PROPERTIES']['MANUAL_NOT_CHANGE_PHOTO']['VALUE'] != "Y") {
				$this->doCheckElementPhotos($arCatalogElement['ID']);
			}
			if (!$ar1CElement) {
				$ar1CElement = $this->getElementData($i1CElementId);
			}
			if (strlen($ar1CElement['PROPERTIES']['CML2_MANUFACTURER']['VALUE']) > 0) {
				$this->doCheckProperty($arCatalogElement['ID'], "MANUFACTURE", $ar1CElement['PROPERTIES']['CML2_MANUFACTURER']['VALUE'], $arCatalogElement['PROPERTIES']['MANUFACTURE']);
			}
			if (isset($this->arNomenklaturaData[$ar1CElement['XML_ID']])) {
				if (strlen($this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFBREND']) > 0) {
					$this->doCheckProperty($arCatalogElement['ID'], "BRAND", $this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFBREND'], $arCatalogElement['PROPERTIES']['BRAND']);
				}
				if (strlen($this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFGODVYPUSKA']) > 0) {
					$this->doCheckProperty($arCatalogElement['ID'], "YEAR", $this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFGODVYPUSKA'], $arCatalogElement['PROPERTIES']['YEAR']);
				}
				if (strlen($this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFRAZMERNYYRYAD']) > 0) {
					$this->doCheckProperty($arCatalogElement['ID'], "SIZE_LINE", $this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFRAZMERNYYRYAD'], $arCatalogElement['PROPERTIES']['SIZE_LINE']);
				}
				if (strlen($this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFSTRANAPROISKHOZ']) > 0) {
					$this->doCheckProperty($arCatalogElement['ID'], "COUNTRY", $this->arNomenklaturaData[$ar1CElement['XML_ID']]['UF_PFSTRANAPROISKHOZ'], $arCatalogElement['PROPERTIES']['COUNTRY']);
				}
				if ($arCatalogElement['PROPERTIES']['SYS_ORIGINAL_NAME']['VALUE'] != $ar1CElement['NAME']) {
					$obElement->SetPropertyValueCode($arCatalogElement['ID'], "SYS_ORIGINAL_NAME", $ar1CElement['NAME']);
				}
			}
			if (isset($this->arPricesFromFile[strtolower($strArticle)])) {
				if ($arCatalogElement['PROPERTIES']['IS_PRICE_FROM_FILE']['VALUE'] != "Y") {
					$obElement->SetPropertyValueCode($arCatalogElement['ID'], "IS_PRICE_FROM_FILE", "Y");
				}
				if (isset($this->arBlackFridayArticles[strtolower($strArticle)])) {
					if ($arCatalogElement['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] != "Y") {
						$obElement->SetPropertyValueCode($arCatalogElement['ID'], "LABEL_BLACK_FRIDAY", "Y");
					}
					if ($arCatalogElement['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y") {
						$obElement->SetPropertyValueCode($arCatalogElement['ID'], "LABEL_SALE", "N");
					}
				} else {
					if ($arCatalogElement['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] == "Y") {
						$obElement->SetPropertyValueCode($arCatalogElement['ID'], "LABEL_BLACK_FRIDAY", "N");
					}
					if ($arCatalogElement['PROPERTIES']['LABEL_SALE']['VALUE'] != "Y") {
						$obElement->SetPropertyValueCode($arCatalogElement['ID'], "LABEL_SALE", "Y");
					}
				}
			} else {
				if ($arCatalogElement['PROPERTIES']['IS_PRICE_FROM_FILE']['VALUE'] != "N") {
					$obElement->SetPropertyValueCode($arCatalogElement['ID'], "IS_PRICE_FROM_FILE", "N");
				}
				if ($arCatalogElement['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] != "N") {
					$obElement->SetPropertyValueCode($arCatalogElement['ID'], "LABEL_BLACK_FRIDAY", "N");
				}
				if ($arCatalogElement['PROPERTIES']['LABEL_SALE']['VALUE'] != "N") {
					$obElement->SetPropertyValueCode($arCatalogElement['ID'], "LABEL_SALE", "N");
				}
			}
		}
		unset($obElement);
		/*if (strpos(strtolower($ar1CElement['NAME']), "(бу)") !== false) {
			$this->arAfterTestProducts[] = $iResult;
		}*/
		return $iResult;
	}

	/**
	 * Проверяем торговые предложения
	 *
	 * @param int $arElementId1C
	 * @param int $iElementIdCatalog
	 */
	function doMakeOffers($arElementId1C, $iElementIdCatalog, $strArticle)
	{
		/* $strOldPrice = trim(str_replace(" ", "", $strOldPrice));
		  $newPrice = "";
		  for ($i = 0; $i < strlen($strOldPrice); $i++)
		  {
		  $c = substr($strOldPrice, $i, 1);
		  if (strpos('0123456789.', $c) !== false)
		  {
		  $newPrice .= $c;
		  }
		  }
		  $strOldPrice = $newPrice; */
		$obElement = new \CIBlockElement();
		$arProduct = $this->getElementData($iElementIdCatalog);
		$arPriceFromFile = false;
		if (isset($this->arPricesFromFile[strtolower($strArticle)])) {
			$arPriceFromFile = $this->arPricesFromFile[strtolower($strArticle)];
		}
		//Загружаем имеющиеся предложения
		$this->arExistsProductOffers = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch()) {
			$this->arExistsProductOffers[$arOffer['ID']] = $arOffer['ID'];
		}
		foreach ($arElementId1C as $i1COfferID) {
			$arOffer = $this->getElementData($i1COfferID);
			$offerId = $this->findCatalogOfferIdBy1COfferId($arOffer['ID'], $iElementIdCatalog);
			unset($this->arExistsProductOffers[$offerId]);
			$this->doMakeOffer($iElementIdCatalog, $offerId, $arOffer['ID'], $arOffer['NAME'], $strArticle, $arPriceFromFile);
			unset($this->arCacheElementsData[$offerId]);
			unset($this->arCacheElementsData[$arOffer['ID']]);
		}
		//Находим минимальную цену и устанавливаем
		$minPrice = false;
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID", "CATALOG_GROUP_" . $this->arOptions['BASE_PRICE_ID']));
		while ($arOffer = $rOffers->Fetch()) {
			if ($minPrice === false && $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']] > 0) {
				$minPrice = $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']];
			} elseif ($minPrice !== false && $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']] > 0 && $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']] < $minPrice) {
				$minPrice = $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']];
			}
		}
		$minPrice = ceil($minPrice / 10) * 10;
		if ($minPrice !== false && $minPrice > 0) {
			$arPriceCatalog = \CPrice::GetBasePrice($iElementIdCatalog);
			if ($arPriceCatalog['PRICE'] != $minPrice) {
				\CPrice::SetBasePrice($iElementIdCatalog, $minPrice, "RUB");
			}
			/*
			  if (intval($strOldPrice) <= 0)
			  {
			  $minOldPrice = false;
			  $rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID", "CATALOG_GROUP_" . OLD_PRICE_ID));
			  while ($arOffer = $rOffers->Fetch())
			  {
			  if ($minOldPrice === false && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] > 0)
			  {
			  $minOldPrice = $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID];
			  } else if ($minOldPrice !== false && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] > 0 && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] < $minOldPrice)
			  {
			  $minOldPrice = $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID];
			  }
			  }
			  if ($minOldPrice > 0)
			  {
			  $strOldPrice = $minOldPrice;
			  }
			  }

			  if (intval($strOldPrice) > 0 && intval($strOldPrice) <= $minPrice)
			  {
			  $strOldPrice = "";
			  }
			  //Проверка старой цены
			  $arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			  if ($arOldPrice)
			  {
			  if ($arOldPrice['PRICE'] != floatval($strOldPrice))
			  {
			  \CPrice::Update($arOldPrice['ID'], array(
			  "PRICE" => $strOldPrice,
			  "CURRENCY" => "RUB"
			  ));
			  } elseif ($arOldPrice['PRICE'] == 0)
			  {
			  \CPrice::Delete($arOldPrice['ID']);
			  }
			  } elseif (strlen($strOldPrice) > 0)
			  {
			  \CPrice::Add(array(
			  "PRODUCT_ID" => $iElementIdCatalog,
			  "CATALOG_GROUP_ID" => OLD_PRICE_ID,
			  "PRICE" => $strOldPrice,
			  "CURRENCY" => "RUB"
			  ));
			  }
			  if ($strOldPrice > 0)
			  {
			  if ($arProduct['PROPERTIES']['LABEL_SALE']['VALUE'] != "Y")
			  {
			  $obElement->SetPropertyValueCode($iElementIdCatalog, "LABEL_SALE", "Y");
			  }
			  if ($arProduct['PROPERTIES']['SYS_IS_OLD_PRICE']['VALUE'] != "Y")
			  {
			  $obElement->SetPropertyValueCode($arProduct['ID'], "SYS_IS_OLD_PRICE", "Y");
			  }
			  } else
			  {
			  if ($arProduct['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
			  {
			  $obElement->SetPropertyValueCode($iElementIdCatalog, "LABEL_SALE", "N");
			  }
			  if ($arProduct['PROPERTIES']['SYS_IS_OLD_PRICE']['VALUE'] == "Y")
			  {
			  $obElement->SetPropertyValueCode($arCatalogElement['ID'], "SYS_IS_OLD_PRICE", "N");
			  }
			  } */
			/*
			  if ($arProduct['PROPERTIES']['SYS_LINK_1C']['VALUE'] <= 0)
			  {
			  if ($arProduct['ACTIVE'] == "Y")
			  {
			  $obElement->Update($arProduct['ID'], array("ACTIVE" => "N"));
			  }
			  }
			 *
			 */
		} else {
			/*			 * *
			  if ($arProduct['ACTIVE'] == "Y")
			  {
			  $obElement->Update($arProduct['ID'], array("ACTIVE" => "N"));
			  }
			 *
			 */
		}
		if ($arPriceFromFile !== false) {
			$strOldPrice = $arPriceFromFile['OLD_PRICE'];
			$arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			if ($arOldPrice) {
				if ($arOldPrice['PRICE'] != floatval($strOldPrice)) {
					\CPrice::Update($arOldPrice['ID'], array(
						"PRICE" => $strOldPrice,
						"CURRENCY" => "RUB",
					));
				} elseif ($arOldPrice['PRICE'] == 0) {
					\CPrice::Delete($arOldPrice['ID']);
				}
			} elseif (strlen($strOldPrice) > 0) {
				\CPrice::Add(array(
					"PRODUCT_ID" => $iElementIdCatalog,
					"CATALOG_GROUP_ID" => OLD_PRICE_ID,
					"PRICE" => $strOldPrice,
					"CURRENCY" => "RUB",
				));
			}
		} else {
			$arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			if ($arOldPrice) {
				\CPrice::Delete($arOldPrice['ID']);
			}
		}
		//Проверка остатков по складам
		//Загружаем текущие данные
		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		while ($arStoreData = $rStoreData->Fetch()) {
			$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		}

		//Смотрим что в 1с
		$arDataBySklad = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch()) {
			$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arOffer['ID']));
			while ($arStoreData = $rStoreData->Fetch()) {
				$arDataBySklad[$arStoreData['STORE_ID']] += $arStoreData['AMOUNT'];
			}
		}
		$totalQuantity = 0;
		foreach ($arDataBySklad as $storeId => $amount) {
			$totalQuantity += $amount;
			if (isset($arCurrentStoreData[$storeId])) {
				\CCatalogStoreProduct::Update($arCurrentStoreData[$storeId]['ID'], array(
					"AMOUNT" => $amount,
				));
				unset($arCurrentStoreData[$storeId]);
			} else {
				\CCatalogStoreProduct::Add(array(
					"PRODUCT_ID" => $iElementIdCatalog,
					"STORE_ID" => $storeId,
					"AMOUNT" => $amount,
				));
			}
		}

		foreach ($arCurrentStoreData as $arStoreData) {
			\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
				"AMOUNT" => 0,
			));
		}

		$arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		if ($arCatalogProduct) {
			$arProductFields = array(
				"QUANTITY" => $totalQuantity,
				"QUANTITY_TRACE" => "D",
				//"CAN_BUY_ZERO" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
			);
			\CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		} else {
			$arProductFields = array(
				"ID" => $iElementIdCatalog,
				"QUANTITY_TRACE" => "D",
				"PRICE_TYPE" => "S",
				"QUANTITY" => $totalQuantity,
				"QUANTITY_TRACE" => "D",
				"CAN_BUY_ZERO" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
			);
			\CCatalogProduct::Add($arProductFields);
		}

		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		while ($arStoreData = $rStoreData->Fetch()) {
			if ($arStoreData['AMOUNT'] > 0) {
				$arCurrentStoreData[] = $arStoreData['STORE_NAME'];
			}
		}

		$arValues = array();
		foreach ($arCurrentStoreData as $strStoreName) {
			if (isset($this->arStorePropVariants[$strStoreName])) {
				$arValues[] = $this->arStorePropVariants[$strStoreName];
			} else {
				$rPropertyVariant = \CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "CODE" => "INFO_IN_SKLAD", "VALUE" => $strStoreName));
				if ($arPropertyVariant = $rPropertyVariant->Fetch()) {
					$this->arStorePropVariants[$strStoreName] = $arPropertyVariant['ID'];
					$arValues[] = $this->arStorePropVariants[$strStoreName];
				} else {
					$obPropEnum = new \CIBlockPropertyEnum();
					$this->arStorePropVariants[$strStoreName] = $obPropEnum->Add(array(
						"PROPERTY_ID" => IBLOCK_CATALOG_PROPERTY_INFO_IN_SKLAD,
						"VALUE" => $strStoreName,
						"SORT" => 100,
					));
					$arValues[] = $this->arStorePropVariants[$strStoreName];
				}
			}
		}
		if (empty($arValues)) {
			$arValues = false;
		}
		$obElement->SetPropertyValueCode($iElementIdCatalog, "INFO_IN_SKLAD", $arValues);
		/*

		  $arProduct = $this->getElementData($iElementIdCatalog);
		  //Загружаем имеющиеся предложения
		  $this->arExistsProductOffers = array();
		  $rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		  while ($arOffer = $rOffers->Fetch())
		  {
		  $this->arExistsProductOffers[$arOffer['ID']] = $arOffer['ID'];
		  }
		  $rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_UT_OFFERS, "PROPERTY_CML2_LINK" => $iElementId1C), false, false, array("ID"));
		  while ($arOffer = $rOffers->Fetch())
		  {
		  $offerId = $this->findCatalogOfferIdBy1COfferId($arOffer['ID'], $iElementIdCatalog);
		  unset($this->arExistsProductOffers[$offerId]);
		  $this->doMakeOffer($iElementIdCatalog, $offerId, $arOffer['ID'], $arProduct['NAME'], $strArticle);
		  unset($this->arCacheElementsData[$offerId]);
		  unset($this->arCacheElementsData[$arOffer['ID']]);
		  }
		  //Находим минимальную цену и устанавливаем
		  $minPrice = false;
		  $rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID", "CATALOG_GROUP_" . $this->arOptions['BASE_PRICE_ID']));
		  while ($arOffer = $rOffers->Fetch())
		  {
		  if ($minPrice === false && $arOffer['CATALOG_PRICE_1'] > 0)
		  {
		  $minPrice = $arOffer['CATALOG_PRICE_1'];
		  } else if ($minPrice !== false && $arOffer['CATALOG_PRICE_1'] > 0 && $arOffer['CATALOG_PRICE_1'] < $minPrice)
		  {
		  $minPrice = $arOffer['CATALOG_PRICE_1'];
		  }
		  }
		  if ($minPrice !== false && $minPrice > 0)
		  {
		  $arPriceCatalog = \CPrice::GetBasePrice($iElementIdCatalog);
		  if ($arPriceCatalog['PRICE'] != $minPrice)
		  {
		  \CPrice::SetBasePrice($iElementIdCatalog, $minPrice, "RUB");
		  }

		  if (intval($strOldPrice) <= 0)
		  {
		  $minOldPrice = false;
		  $rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID", "CATALOG_GROUP_" . OLD_PRICE_ID));
		  while ($arOffer = $rOffers->Fetch())
		  {
		  if ($minOldPrice === false && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] > 0)
		  {
		  $minOldPrice = $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID];
		  } else if ($minOldPrice !== false && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] > 0 && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] < $minOldPrice)
		  {
		  $minOldPrice = $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID];
		  }
		  }
		  if ($minOldPrice > 0)
		  {
		  $strOldPrice = $minOldPrice;
		  }
		  }

		  if (intval($strOldPrice) > 0 && intval($strOldPrice) <= $minPrice)
		  {
		  $strOldPrice = "";
		  }
		  //Проверка старой цены
		  $arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
		  if ($arOldPrice)
		  {
		  if ($arOldPrice['PRICE'] != floatval($strOldPrice))
		  {
		  \CPrice::Update($arOldPrice['ID'], array(
		  "PRICE" => $strOldPrice,
		  "CURRENCY" => "RUB"
		  ));
		  } elseif ($arOldPrice['PRICE'] == 0)
		  {
		  \CPrice::Delete($arOldPrice['ID']);
		  }
		  } elseif (strlen($strOldPrice) > 0)
		  {
		  \CPrice::Add(array(
		  "PRODUCT_ID" => $iElementIdCatalog,
		  "CATALOG_GROUP_ID" => OLD_PRICE_ID,
		  "PRICE" => $strOldPrice,
		  "CURRENCY" => "RUB"
		  ));
		  }
		  if ($strOldPrice > 0)
		  {
		  if ($arProduct['PROPERTIES']['LABEL_SALE']['VALUE'] != "Y")
		  {
		  $obElement->SetPropertyValueCode($iElementIdCatalog, "LABEL_SALE", "Y");
		  }
		  if ($arProduct['PROPERTIES']['SYS_IS_OLD_PRICE']['VALUE'] != "Y")
		  {
		  $obElement->SetPropertyValueCode($arProduct['ID'], "SYS_IS_OLD_PRICE", "Y");
		  }
		  } else
		  {
		  if ($arProduct['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
		  {
		  $obElement->SetPropertyValueCode($iElementIdCatalog, "LABEL_SALE", "N");
		  }
		  if ($arProduct['PROPERTIES']['SYS_IS_OLD_PRICE']['VALUE'] == "Y")
		  {
		  $obElement->SetPropertyValueCode($arCatalogElement['ID'], "SYS_IS_OLD_PRICE", "N");
		  }
		  }
		  if ($arProduct['PROPERTIES']['SYS_LINK_1C']['VALUE'] <= 0)
		  {
		  if ($arProduct['ACTIVE'] == "Y")
		  {
		  $obElement->Update($arProduct['ID'], array("ACTIVE" => "N"));
		  }
		  }
		  } else
		  {
		  if ($arProduct['ACTIVE'] == "Y")
		  {
		  $obElement->Update($arProduct['ID'], array("ACTIVE" => "N"));
		  }
		  }
		  //Проверка остатков по складам
		  $totalQuantity = 0;
		  //Загружаем текущие данные
		  $arCurrentStoreData = array();
		  $rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		  while ($arStoreData = $rStoreData->Fetch())
		  {
		  $arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		  }

		  //Смотрим что в 1с
		  $arDataBySklad = array();
		  $rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		  while ($arOffer = $rOffers->Fetch())
		  {
		  $rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arOffer['ID']));
		  while ($arStoreData = $rStoreData->Fetch())
		  {
		  $arDataBySklad[$arStoreData['STORE_ID']] += $arStoreData['AMOUNT'];
		  $totalQuantity += $arStoreData['AMOUNT'];
		  }
		  }
		  foreach ($arDataBySklad as $storeId => $amount)
		  {
		  if (isset($arCurrentStoreData[$storeId]))
		  {
		  \CCatalogStoreProduct::Update($arCurrentStoreData[$storeId]['ID'], array(
		  "AMOUNT" => $amount
		  ));
		  unset($arCurrentStoreData[$storeId]);
		  } else
		  {
		  \CCatalogStoreProduct::Add(array(
		  "PRODUCT_ID" => $iElementIdCatalog,
		  "STORE_ID" => $storeId,
		  "AMOUNT" => $amount
		  ));
		  }
		  }

		  foreach ($arCurrentStoreData as $arStoreData)
		  {
		  \CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
		  "AMOUNT" => 0
		  ));
		  }

		  //Проверка остатков
		  $arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		  if ($arCatalogProduct)
		  {
		  $arProductFields = array(
		  "QUANTITY" => $totalQuantity,
		  "CAN_BUY_ZERO" => "D",
		  "QUANTITY_TRACE" => "D",
		  "NEGATIVE_AMOUNT_TRACE" => "D"
		  );
		  \CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		  } else
		  {
		  $arProductFields = array(
		  "ID" => $iElementIdCatalog,
		  "QUANTITY_TRACE" => "D",
		  "PRICE_TYPE" => "S",
		  "QUANTITY" => $totalQuantity,
		  "CAN_BUY_ZERO" => "D",
		  "QUANTITY_TRACE" => "D",
		  "NEGATIVE_AMOUNT_TRACE" => "D"
		  );
		  \CCatalogProduct::Add($arProductFields);
		  }

		  $arCurrentStoreData = array();
		  $rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		  while ($arStoreData = $rStoreData->Fetch())
		  {
		  $arCurrentStoreData[] = $arStoreData['STORE_NAME'];
		  }

		  $arValues = array();
		  foreach ($arCurrentStoreData as $strStoreName)
		  {
		  if (isset($this->arStorePropVariants[$strStoreName]))
		  {
		  $arValues[] = $this->arStorePropVariants[$strStoreName];
		  } else
		  {
		  $rPropertyVariant = \CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "CODE" => "INFO_IN_SKLAD", "VALUE" => $strStoreName));
		  if ($arPropertyVariant = $rPropertyVariant->Fetch())
		  {
		  $this->arStorePropVariants[$strStoreName] = $arPropertyVariant['ID'];
		  $arValues[] = $this->arStorePropVariants[$strStoreName];
		  } else
		  {
		  $obPropEnum = new \CIBlockPropertyEnum();
		  $this->arStorePropVariants[$strStoreName] = $obPropEnum->Add(array(
		  "PROPERTY_ID" => IBLOCK_CATALOG_PROPERTY_INFO_IN_SKLAD,
		  "VALUE" => $strStoreName,
		  "SORT" => 100
		  ));
		  $arValues[] = $this->arStorePropVariants[$strStoreName];
		  }
		  }
		  }
		  if (empty($arValues))
		  {
		  $arValues = false;
		  }
		  $obElement->SetPropertyValueCode($iElementIdCatalog, "INFO_IN_SKLAD", $arValues);
		 */
		unset($obElement);
	}

	/**
	 * Проверка торгового предложения
	 *
	 * @param int $iElementIdCatalog
	 * @param int $iOfferId1C
	 */
	function doMakeOffer($iElementIdCatalog, $iOfferIdCatalog, $iOfferId1C, $strBaseName, $strArticle, $arPriceFromFile = false)
	{
		$obElement = new \CIBlockElement();
		$ar1COffer = $this->getElementData($iOfferId1C);
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arCatalogOffer = $this->getElementData($iOfferIdCatalog);

		$arFields = array(
			"IBLOCK_ID" => IBLOCK_CATALOG_OFFERS,
			"NAME" => $strBaseName,
			"CODE" => $this->doTranslitName($ar1COffer['NAME']),
			"ACTIVE" => "Y",
			"XML_ID" => $ar1COffer['XML_ID'],
			"PROPERTY_VALUES" => array(
				"SYS_LINK_1C" => $ar1COffer['ID'],
				"CML2_LINK" => $iElementIdCatalog,
				"CML2_ARTICLE" => $strArticle,
				"SYS_ORIGINAL_NAME" => $ar1COffer['NAME'],
			),
		);
		if (!$arCatalogOffer) {
			$iOfferIdCatalog = $obElement->Add($arFields);
			if ($iOfferIdCatalog > 0) {
				$arCatalogOffer = $this->getElementData($iOfferIdCatalog, true);
			} else {
				echo $obElement->LAST_ERROR . " (" . __LINE__ . ")\n";
			}
		}
		if ($arCatalogOffer) {
			if ($arCatalogOffer['NAME'] != $arFields['NAME']) {
				$obElement->Update($arCatalogOffer['ID'], array("NAME" => $arFields['NAME']));
			}
			if ($arCatalogOffer['CODE'] != $arFields['CODE']) {
				$obElement->Update($arCatalogOffer['ID'], array("CODE" => $arFields['CODE']));
			}
			$this->doCheckProperty($arCatalogOffer['ID'], "CML2_BARCODE", $ar1COffer['PROPERTIES']['CML2_BAR_CODE']['VALUE'], $arCatalogOffer['PROPERTIES']['CML2_BARCODE']);
			if (isset($this->arNomenklaturaData[$ar1COffer['XML_ID']])) {
				$iValue = $this->doFindReferenceValue($this->arNomenklaturaData[$ar1COffer['XML_ID']]['UF_PFTSVET'], $arCatalogOffer['PROPERTIES']['COLOR']['LINK_IBLOCK_ID']);
				$arValueData = $this->getElementData($iValue);
				if ($arValueData['PROPERTIES']['IS_WIDTH']['VALUE'] == "Y") {
					$this->doCheckProperty($arCatalogOffer['ID'], "COLOR", false, $arCatalogOffer['PROPERTIES']['COLOR']);
					$this->doCheckProperty($arCatalogOffer['ID'], "WIDTH", $this->arNomenklaturaData[$ar1COffer['XML_ID']]['UF_PFTSVET'], $arCatalogOffer['PROPERTIES']['WIDTH']);
				} else {
					$this->doCheckProperty($arCatalogOffer['ID'], "COLOR", $this->arNomenklaturaData[$ar1COffer['XML_ID']]['UF_PFTSVET'], $arCatalogOffer['PROPERTIES']['COLOR']);
					$this->doCheckProperty($arCatalogOffer['ID'], "WIDTH", false, $arCatalogOffer['PROPERTIES']['WIDTH']);
				}
				$this->doCheckProperty($arCatalogOffer['ID'], "SIZE", $this->arNomenklaturaData[$ar1COffer['XML_ID']]['UF_PFRAZMER'], $arCatalogOffer['PROPERTIES']['SIZE']);
			}
			if ($arCatalogOffer['PROPERTIES']['SYS_ORIGINAL_NAME']['VALUE'] != $ar1COffer['NAME']) {
				$obElement->SetPropertyValueCode($arCatalogOffer['ID'], "SYS_ORIGINAL_NAME", $ar1COffer['NAME']);
			}
		}

		//Проверка остатков
		$arCatalogProduct = \CCatalogProduct::GetByID($iOfferIdCatalog);
		$ar1CProduct = \CCatalogProduct::GetByID($iOfferId1C);
		if ($arCatalogProduct) {
			$arProductFields = array(
				"WEIGHT" => $ar1CProduct['WEIGHT'],
				"MEASURE" => $ar1CProduct['MEASURE'],
				"VAT_ID" => $ar1CProduct['VAT_ID'],
				"VAT_INCLUDED" => $ar1CProduct['VAT_INCLUDED'],
				"QUANTITY" => $ar1CProduct['QUANTITY'],
				//"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
				//"PURCHASING_PRICE" => $ar1CProduct[''],
				//"PURCHASING_CURRENCY" => $ar1CProduct[''],
			);
			\CCatalogProduct::Update($iOfferIdCatalog, $arProductFields);
		} else {
			$arProductFields = array(
				"ID" => $iOfferIdCatalog,
				"QUANTITY_TRACE" => "D",
				"WEIGHT" => $ar1CProduct['WEIGHT'],
				"MEASURE" => $ar1CProduct['MEASURE'],
				"VAT_ID" => $ar1CProduct['VAT_ID'],
				"VAT_INCLUDED" => $ar1CProduct['VAT_INCLUDED'],
				"PRICE_TYPE" => "S",
				"QUANTITY" => $ar1CProduct['QUANTITY'],
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
				//"PURCHASING_PRICE" => $ar1CProduct[''],
				//"PURCHASING_CURRENCY" => $ar1CProduct[''],
			);
			\CCatalogProduct::Add($arProductFields);
		}
		//Проверка цены
		$iPriceProduct = false;
		if ($arPriceFromFile !== false) {
			$arPriceCatalog = \CPrice::GetBasePrice($iOfferIdCatalog);
			if ($arPriceCatalog['PRICE'] != $arPriceFromFile['PRICE']) {
				\CPrice::SetBasePrice($iOfferIdCatalog, $arPriceFromFile['PRICE'], "RUB");
			}
			$iPriceProduct = $arPriceFromFile['PRICE'];
			$strOldPrice = $arPriceFromFile['OLD_PRICE'];
			$arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			if ($arOldPrice) {
				if ($arOldPrice['PRICE'] != floatval($strOldPrice)) {
					\CPrice::Update($arOldPrice['ID'], array(
						"PRICE" => $strOldPrice,
						"CURRENCY" => "RUB",
					));
				} elseif ($arOldPrice['PRICE'] == 0) {
					\CPrice::Delete($arOldPrice['ID']);
				}
			} elseif (strlen($strOldPrice) > 0) {
				\CPrice::Add(array(
					"PRODUCT_ID" => $iOfferIdCatalog,
					"CATALOG_GROUP_ID" => OLD_PRICE_ID,
					"PRICE" => $strOldPrice,
					"CURRENCY" => "RUB",
				));
			}
		} else {
			$arPriceOffer1C = $this->GetProductPrice($iOfferId1C);
			if (!in_array($strArticle, array("sert1", "sert2", "sert3")) && !in_array($arCatalogElement['PROPERTIES']['YEAR']['VALUE'], array(NEW_COLLECTION_YEAR))) {
				$arPriceOffer1C['PRICE'] = ceil($arPriceOffer1C['PRICE'] / 10) * 10;
				$arPriceOffer1C['PRICE'] = $arPriceOffer1C['PRICE'] / ((100 - $this->arOptions['PERCENT']) / 100);
				$arPriceOffer1C['PRICE'] = ceil($arPriceOffer1C['PRICE'] / 10) * 10;
				$iPriceProduct = $arPriceOffer1C['PRICE'];
			}
			$arPriceCatalog = \CPrice::GetBasePrice($iOfferIdCatalog);
			if ($arPriceCatalog['PRICE'] != $arPriceOffer1C['PRICE']) {
				\CPrice::SetBasePrice($iOfferIdCatalog, $arPriceOffer1C['PRICE'], $arPriceOffer1C['CURRENCY']);
			}
			$arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			if ($arOldPrice) {
				\CPrice::Delete($arOldPrice['ID']);
			}
		}
		/*
		  //проверка первоначальной цены и установка старой
		  $strOldPrice = "";
		  $arOldPrice = $this->GetProductStartPrice($iOfferId1C);
		  if ($arOldPrice)
		  {
		  $strOldPrice = $arOldPrice['PRICE'];
		  }
		  if (intval($strOldPrice) > 0 && intval($strOldPrice) <= $arPriceOffer1C['PRICE'])
		  {
		  $strOldPrice = "";
		  }
		  //Проверка старой цены
		  $arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
		  if ($arOldPrice)
		  {
		  if ($arOldPrice['PRICE'] != floatval($strOldPrice))
		  {
		  \CPrice::Update($arOldPrice['ID'], array(
		  "PRICE" => $strOldPrice,
		  "CURRENCY" => "RUB"
		  ));
		  } elseif ($arOldPrice['PRICE'] == 0)
		  {
		  \CPrice::Delete($arOldPrice['ID']);
		  }
		  } elseif (strlen($strOldPrice) > 0)
		  {
		  \CPrice::Add(array(
		  "PRODUCT_ID" => $iOfferIdCatalog,
		  "CATALOG_GROUP_ID" => OLD_PRICE_ID,
		  "PRICE" => $strOldPrice,
		  "CURRENCY" => "RUB"
		  ));
		  } */
		/**
		 * if (($arPriceOffer1C['PRICE'] <= 0 && $arCatalogOffer['ACTIVE'] == "Y")/* || $arCatalogOffer['PROPERTIES']['SYS_LINK_1C']['VALUE'] <= 0 *//* )
		  {
		  $obElement->Update($arCatalogOffer['ID'], array("ACTIVE" => "N"));
		  }
		 */

		//Проверка остатков по складам
		//Загружаем текущие данные
		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog));
		while ($arStoreData = $rStoreData->Fetch()) {
			$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		}
		//Смотрим что в 1с

		$bSetNolAmount = false;
		if ($iPriceProduct < 7000) {
			$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferId1C, "STORE_ID" => 1));
			if ($arStoreData = $rStoreData->Fetch()) {
				if ($arStoreData['AMOUNT'] <= 0) {
					$bSetNolAmount = true;
				}
			}
		}

		$totalQuantity = 0;
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferId1C));
		while ($arStoreData = $rStoreData->Fetch()) {
			if ($bSetNolAmount) {
				$arStoreData['AMOUNT'] = 0;
			}
			if (isset($arCurrentStoreData[$arStoreData['STORE_ID']])) {
				$totalQuantity += $arStoreData['AMOUNT'];
				\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
					"AMOUNT" => $arStoreData['AMOUNT'],
				));
				unset($arCurrentStoreData[$arStoreData['STORE_ID']]);
			} else {
				$totalQuantity += $arStoreData['AMOUNT'];
				\CCatalogStoreProduct::Add(array(
					"PRODUCT_ID" => $iOfferIdCatalog,
					"STORE_ID" => $arStoreData['STORE_ID'],
					"AMOUNT" => $arStoreData['AMOUNT'],
				));
			}
		}

		foreach ($arCurrentStoreData as $arStoreData) {
			\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
				"AMOUNT" => 0,
			));
		}
		//Проверка остатков
		$arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		if ($arCatalogProduct) {
			$arProductFields = array(
				"QUANTITY" => $totalQuantity,
				//"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
			);
			\CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		} else {
			$arProductFields = array(
				"ID" => $iElementIdCatalog,
				"QUANTITY_TRACE" => "D",
				"PRICE_TYPE" => "S",
				"QUANTITY" => $totalQuantity,
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
			);
			\CCatalogProduct::Add($arProductFields);
		}
		unset($obElement);
	}

	/**
	 * Получаем данные элемента
	 *
	 * @param int $id
	 */
	function getElementData($id, $fRenew = false)
	{
		if ($id <= 0) {
			return false;
		}
		if ($fRenew && isset($this->arCacheElementsData[$id])) {
			unset($this->arCacheElementsData[$id]);
		}
		if (!isset($this->arCacheElementsData[$id])) {
			$arFields = false;
			if ($id > 0) {
				$rElement = \CIBlockElement::GetList(array(), array("ID" => $id));
				if ($rsElement = $rElement->GetNextElement(true, true)) {
					$arFields = $rsElement->GetFields();
					foreach ($arFields as $k => $v) {
						if (substr($k, 0, 1) == "~") {
							$arFields[substr($k, 1)] = $v;
							unset($arFields[$k]);
						}
					}
					$arFields['PROPERTIES'] = $rsElement->GetProperties();
				}
				unset($rElement);
				unset($rsElement);
			}
			if (!$this->arOptions['NO_CACHE_ELEMENTS']) {
				$this->arCacheElementsData[$id] = $arFields;
			} else {
				return $arFields;
			}
		}
		if ($this->arOptions['NO_CACHE_ELEMENTS']) {
			$res = $this->arCacheElementsData[$id];
			unset($this->arCacheElementsData[$id]);
			return $res;
		}
		return $this->arCacheElementsData[$id];
	}

	/**
	 * Проверка свойства
	 *
	 * @param int $iElementIdCatalog
	 * @param string $strPropertyCode
	 * @param array $arProperty1C
	 * @param array $arPropertyCatalog
	 */
	function doCheckProperty($iElementIdCatalog, $strPropertyCode, $arProperty1C, $arPropertyCatalog)
	{
		$newValue = false;
		if ($arPropertyCatalog['PROPERTY_TYPE'] == "E") {
			//Ищем или создаем запись в справочнике
			if ((is_array($arProperty1C['VALUE']) && !empty($arProperty1C['VALUE'])) || (!is_array($arProperty1C['VALUE']) && strlen($arProperty1C['VALUE']) > 0)) {
				$newValue = $this->doFindReferenceValue(is_array($arProperty1C) ? $arProperty1C['VALUE'] : $arProperty1C, $arPropertyCatalog['LINK_IBLOCK_ID']);
			}
		} elseif ($arPropertyCatalog['PROPERTY_TYPE'] == "S") {
			if (is_array($arProperty1C) && isset($arProperty1C['VALUE'])) {
				$newValue = $arProperty1C['VALUE'];
			} else {
				$newValue = $arProperty1C;
			}
		} else {
			$newValue = $arProperty1C['VALUE'];
		}

		$newValue = trim($newValue);
		$isMath = true;
		if (!is_array($arPropertyCatalog['VALUE'])) {
			if ($newValue != $arPropertyCatalog['VALUE']) {
				$isMath = false;
			}
		} else {
			if (in_array($newValue, $arPropertyCatalog['VALUE'])) {
				unset($arPropertyCatalog['VALUE'][array_search($newValue, $arPropertyCatalog['VALUE'])]);
				if (!empty($arPropertyCatalog['VALUE'])) {
					$isMath = false;
				}
			} else {
				$isMath = false;
			}
		}
		if (!$isMath) {
			$obElement = new \CIBlockElement();
			$obElement->SetPropertyValueCode($iElementIdCatalog, $strPropertyCode, $newValue);
			unset($obElement);
		}
	}

	/**
	 * Поиск и создание записи справочника
	 *
	 * @param string $strValue
	 * @param int $IBLOCK_ID
	 *
	 * @return int
	 */
	function doFindReferenceValue($strValue, $IBLOCK_ID)
	{
		$iResult = false;
		$strValue = trim($strValue);
		$obElement = new \CIBlockElement();
		$rElement = \CIBlockElement::GetList(array(), array("PROPERTY_SYNONIM" => $strValue, "IBLOCK_ID" => $IBLOCK_ID), false, false, array("ID", "CODE"));
		if ($arElement = $rElement->Fetch()) {
			$iResult = $arElement['ID'];
			if ($arElement['CODE'] != $this->doTranslitName($strValue)) {
				$obElement->Update($arElement['ID'], array("CODE" => $this->doTranslitName($strValue)));
			}
		} else {
			$iResult = $obElement->Add(array(
				"IBLOCK_ID" => $IBLOCK_ID,
				"NAME" => $strValue,
				"CODE" => $this->doTranslitName($strValue),
				"PROPERTY_VALUES" => array(
					"SYNONIM" => array($strValue),
				),
			));
		}
		unset($obElement);
		return $iResult;
	}

	function doNormalizeArticlePhoto($strArticle)
	{
		$strArticle = str_replace("/", "_", $strArticle);
		$strArticle = str_replace("\\", "_", $strArticle);
		return $strArticle;
	}

	/**
	 * Проверка фотографий товара
	 *
	 * @param int $iElementIdCatalog
	 */
	function doCheckElementPhotos($iElementIdCatalog)
	{
		$obElement = new \CIBlockElement();
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arPhotosInDir = $this->doGetElementPhotos($arCatalogElement['PROPERTIES']['CML2_ARTICLE']['~VALUE'], false);

		//Включаем/выключаем товар в зависимости от наличия фото
		/*
		  if ($arCatalogElement['ACTIVE'] == "Y")
		  {
		  if (empty($arPhotosInDir))
		  {
		  $obElement->Update($arCatalogElement['ID'], array("ACTIVE" => "N"));
		  }
		  } else
		  {
		  if (!empty($arPhotosInDir))
		  {
		  $obElement->Update($arCatalogElement['ID'], array("ACTIVE" => "Y"));
		  }
		  } */
		/*
		  if (count($arPhotosInDir) == 1)
		  {
		  if ($arCatalogElement['SORT'] != 1000000)
		  {
		  $obElement->Update($arCatalogElement['ID'], array("SORT" => 1000000));
		  }
		  } else if (count($arPhotosInDir) > 1)
		  {
		  if ($arCatalogElement['SORT'] == 1000000)
		  {
		  $obElement->Update($arCatalogElement['ID'], array("SORT" => 500));
		  }
		  } */

		//Проверяем и обновляем главное фото
		$strMainPhotoNew = false;
		$strMainPhotoOld = false;
		$iMainPhotoOldId = false;
		if (isset($arPhotosInDir[0])) {
			$strMainPhotoNew = $arPhotosInDir[0];
		}
		if ($arCatalogElement['PREVIEW_PICTURE'] > 0) {
			$iMainPhotoOldId = $arCatalogElement['PREVIEW_PICTURE'];
			$strMainPhotoOld = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iMainPhotoOldId);
		}
		$arFields = array();
		if ($strMainPhotoNew === false && $strMainPhotoOld !== false) {
			//Удаляем фото анонса
			$arFields = array(
				"PREVIEW_PICTURE" => array(
					"del" => "Y",
					"old_file" => $iMainPhotoOldId,
				),
			);
		} elseif ($strMainPhotoNew !== false && $strMainPhotoOld === false) {
			//Устанавливаем фото
			$arFields = array(
				"PREVIEW_PICTURE" => \CFile::MakeFileArray($strMainPhotoNew),
			);
		} elseif ($strMainPhotoNew !== false && $strMainPhotoOld !== false) {
			if (!$this->ComparePhotos($strMainPhotoNew, $strMainPhotoOld)) {
				$arFields = array(
					"PREVIEW_PICTURE" => \CFile::MakeFileArray($strMainPhotoNew),
				);
				$arFields['PREVIEW_PICTURE']['old_file'] = $iMainPhotoOldId;
			}
		}
		if (!empty($arFields)) {
			if (!$obElement->Update($arCatalogElement['ID'], $arFields)) {
				echo $obElement->LAST_ERROR . " (" . __LINE__ . ")\n";
			}
		}

		//Проверка и обновление дополнительных фото
		//Сначала формируем массив хэшей и проверяем
		$arHashOld = array();
		$arHashNew = array();
		foreach ($arPhotosInDir as $strFile) {
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashNew[$strHash] = $strHash;
		}
		if (!is_array($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'])) {
			$arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] = array($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE']);
		}
		foreach ($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $strFile) {
			$strFile = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iMainPhotoOldId);
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashOld[$strHash] = $strHash;
		}
		//Сравниваем массивы
		foreach ($arHashNew as $k => $v) {
			if (isset($arHashOld[$k])) {
				unset($arHashOld[$k]);
				unset($arHashNew[$k]);
			}
		}
		if (!empty($arHashOld) || !empty($arHashNew)) {
			//Обновляем дополнительные фото.
			//Сначала удалим старые значения
			$arProp = array();
			foreach ($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $strFile) {
				$arProp[$arCatalogElement['PROPERTIES']['MORE_PHOTOS']['PROPERTY_VALUE_ID'][$k]] = array(
					"del" => "Y",
					"old_file" => $strFile,
				);
			}

			//И сформируем новые
			foreach ($arPhotosInDir as $k => $strFile) {
				$arProp['n' . $k] = array(
					"VALUE" => \CFile::MakeFileArray($strFile),
					"DESCRIPTION" => "",
				);
			}
			if (!empty($arProp)) {
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "MORE_PHOTOS", $arProp);
			}
		}

		unset($obElement);
		$arCatalogElement = $this->getElementData($iElementIdCatalog, true);
	}

	/**
	 * Сраваниваем фотографии по содержимому
	 *
	 * @param string $strFile1
	 * @param string $strFile2
	 */
	function ComparePhotos($strFile1, $strFile2)
	{
		$hash1 = filesize($strFile1) . "|" . md5_file($strFile1) . "|" . crc32(file_get_contents($strFile1));
		$hash2 = filesize($strFile2) . "|" . md5_file($strFile2) . "|" . crc32(file_get_contents($strFile2));
		return $hash1 == $hash2;
	}

	/**
	 * Получаем массив фотографий по артикулу
	 *
	 * @param string $strElementArticle
	 */
	function doGetElementPhotos($strElementArticle)
	{
		$strElementArticle = $this->arPhotosDirNormalized[strtolower($strElementArticle)];
		if (strlen($strElementArticle) <= 0) {
			return array();
		}
		$strScanDir = $this->arOptions['PHOTOS_DIR'];

		/**
		 * @todo Добавить поиск папки по артикулу
		 */
		$strScanDir .= $strElementArticle . "/";
		if (!file_exists($strScanDir)) {
			if (substr($strElementArticle, strlen($strElementArticle) - 1, 1) == ".") {
				$strScanDir = $this->arOptions['PHOTOS_DIR'] . substr($strElementArticle, 0, strlen($strElementArticle) - 1) . "/";
			}
		}
		if (!file_exists($strScanDir)) {
			$checkArticle = str_replace("/", "_", $strElementArticle);
			$strScanDir = $this->arOptions['PHOTOS_DIR'] . $checkArticle . "/";
			if (!file_exists($strScanDir)) {
				if (substr($checkArticle, strlen($checkArticle) - 1, 1) == ".") {
					$strScanDir = $this->arOptions['PHOTOS_DIR'] . substr($checkArticle, 0, strlen($checkArticle) - 1) . "/";
				}
			}
		}
		$arFiles = scandir($strScanDir);
		$arNormalizeFiles = array();
		foreach ($arFiles as $strFile) {
			if (in_array($strFile, array(".", "..")) || is_dir($strScanDir . "/" . $strFile)) {
				continue;
			}
			$strFullName = $strScanDir . "/" . $strFile;
			$arPathInfo = pathinfo($strFullName);
			if (in_array(strtolower($arPathInfo['extension']), array("jpg", "gif", "jpeg", "png"))) {
				$arNormalizeFiles[$arPathInfo['filename']] = $strFullName;
			}
		}
		ksort($arNormalizeFiles, SORT_NUMERIC);
		$arNormalizeFiles = array_values($arNormalizeFiles);
		return $arNormalizeFiles;
	}

	/**
	 * Ищем предложение из каталога сайта по ID товара из каталога 1С
	 *
	 * @param int $id
	 * @param int $iCml2Link
	 */
	function findCatalogOfferIdBy1COfferId($id, $iCml2Link)
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_SYS_LINK_1C" => $id, "PROPERTY_CML2_LINK" => $iCml2Link), false, false, array("ID"));
		if ($arElement = $rElements->Fetch()) {
			return $arElement['ID'];
		}
		return false;
	}

	public function SetProductPrice($ProductID, $Price, $Currency, $quantityFrom = false, $quantityTo = false, $bGetID = false)
	{
		$bGetID = ($bGetID == true);

		$arFields = array();
		$arFields["PRICE"] = (float)$Price;
		$arFields["CURRENCY"] = $Currency;
		$arFields["QUANTITY_FROM"] = ($quantityFrom == false ? false : (int)$quantityFrom);
		$arFields["QUANTITY_TO"] = ($quantityTo == false ? false : (int)$quantityTo);
		$arFields["EXTRA_ID"] = false;

		if ($arBasePrice = $this->GetProductPrice($ProductID, $quantityFrom, $quantityTo, false)) {
			$ID = \CPrice::Update($arBasePrice["ID"], $arFields);
		} else {
			//$arBaseGroup = CCatalogGroup::GetBaseGroup();
			$arFields["CATALOG_GROUP_ID"] = H2OSPORT_1C_PRICE_ID; //$arBaseGroup["ID"];
			$arFields["PRODUCT_ID"] = $ProductID;

			$ID = \CPrice::Add($arFields);
		}
		if (!$ID)
			return false;

		return ($bGetID ? $ID : true);
	}

	function GetProductPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int)$productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => H2OSPORT_1C_PRICE_ID,
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int)$quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int)$quantityTo;

		if ($boolExt === false) {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
			);
		} else {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID',
			);
		}

		$db_res = \CPrice::GetListEx(
			array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch()) {
			return $res;
		}

		return false;
	}

	public function SetProductOldPrice($ProductID, $Price, $Currency, $quantityFrom = false, $quantityTo = false, $bGetID = false)
	{
		$bGetID = ($bGetID == true);

		$arFields = array();
		$arFields["PRICE"] = (float)$Price;
		$arFields["CURRENCY"] = $Currency;
		$arFields["QUANTITY_FROM"] = ($quantityFrom == false ? false : (int)$quantityFrom);
		$arFields["QUANTITY_TO"] = ($quantityTo == false ? false : (int)$quantityTo);
		$arFields["EXTRA_ID"] = false;

		if ($arBasePrice = $this->GetProductOldPrice($ProductID, $quantityFrom, $quantityTo, false)) {
			if ($Price === false) {
				\CPrice::Delete($arBasePrice["ID"]);
			} else {
				$ID = \CPrice::Update($arBasePrice["ID"], $arFields);
			}
		} else {
			//$arBaseGroup = CCatalogGroup::GetBaseGroup();
			$arFields["CATALOG_GROUP_ID"] = H2OSPORT_1C_OLD_PRICE_ID; //$arBaseGroup["ID"];
			$arFields["PRODUCT_ID"] = $ProductID;

			$ID = \CPrice::Add($arFields);
		}
		if (!$ID)
			return false;

		return ($bGetID ? $ID : true);
	}

	function GetProductOldPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int)$productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => H2OSPORT_1C_OLD_PRICE_ID,
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int)$quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int)$quantityTo;

		if ($boolExt === false) {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
			);
		} else {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID',
			);
		}

		$db_res = \CPrice::GetListEx(
			array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch()) {
			return $res;
		}

		return false;
	}

	function doLoadNomenklaturaData()
	{
		$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HL_NOMENKLATURA_ENTITY_ID)->fetch();
		$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
		/** @var \Bitrix\Highloadblock\DataManager $entity_data_class */
		$entity_data_class = $entity->getDataClass();
		$rList = $entity_data_class::getList(array());
		while ($arList = $rList->fetch()) {
			$this->arNomenklaturaData[$arList['UF_XML_ID']] = $arList;
		}
	}

	function doCheckSectionsActive()
	{
		$obSection = new \CIBlockSection();
		$obElements = new \CIBlockElement();
		$rSections = \CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG), false);
		while ($arSection = $rSections->Fetch()) {
			/*
			  if ($arSection['ID'] == DEFAULT_CATEGORY_ID)
			  {
			  continue;
			  } */
			$arElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y", "CATALOG_AVAILABLE" => "Y"), array("IBLOCK_ID"))->Fetch();
			if ($arElements['CNT'] > 0) {
				if ($arSection['ACTIVE'] != "Y") {
					$obSection->Update($arSection['ID'], array("ACTIVE" => "Y"));
				}
				//Активируем товары в разделе
				/* $rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "N", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
				  while ($arElement = $rElements->Fetch())
				  {
				  //if (in_array($arElement['ID'], $this->arActiveProductsSet))
				  //{
				  $obElements->Update($arElement['ID'], array("ACTIVE" => "Y"));
				  //}
				  } */
			} else {
				if ($arSection['ACTIVE'] == "Y") {
					$obSection->Update($arSection['ID'], array("ACTIVE" => "N"));
				}
				//Деактивируем товары в разделе
				/* $rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
				  while ($arElement = $rElements->Fetch())
				  {
				  $obElements->Update($arElement['ID'], array("ACTIVE" => "N"));
				  }
				 *
				 */
			}
		}
	}

	function doUpdateNames()
	{
		$arElementsSections = array();
		$obElement = new \CIBlockElement();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_SYS_ORIGINAL_NAME", "PROPERTY_NAME_PREFIX"));
		while ($arElement = $rElements->Fetch()) {
			$arElementsSections[$arElement['ID']] = $arElement['IBLOCK_SECTION_ID'];
			if (in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				continue;
			}
			/*
			  $strNewName = $arElement['PROPERTY_SYS_ORIGINAL_NAME_VALUE'];
			  $strPrefix = "";
			  if (strlen(trim($arElement['PROPERTY_NAME_PREFIX_VALUE'])) > 0)
			  {
			  $strPrefix = $arElement['PROPERTY_NAME_PREFIX_VALUE'];
			  } else
			  {
			  $strPrefix = $this->doGetPrefixName($arElement['IBLOCK_SECTION_ID']);
			  }
			  $strPrefix = trim($strPrefix);
			  $strNewName = trim($strNewName);
			  if ($strPrefix == "-")
			  {
			  $strPrefix = "";
			  } elseif (strlen($strPrefix) > 0)
			  {
			  $strNewName = trim($strPrefix . " " . $strNewName);
			  }

			  if ($strNewName != $arElement['NAME'])
			  {
			  $arFields = array(
			  "NAME" => $strNewName,
			  "CODE" => $this->doTranslitName($strNewName)
			  );
			  $obElement->Update($arElement['ID'], $arFields);
			  } */
		}

		//Проходимся по торгпредам
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "NAME", "PROPERTY_CML2_LINK", "PROPERTY_SYS_ORIGINAL_NAME", "PROPERTY_NAME_PREFIX"));
		while ($arElement = $rElements->Fetch()) {
			if (in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				continue;
			}
			if (in_array($arElement['PROPERTY_CML2_LINK_VALUE'], $this->arNotChangedElementsID)) {
				continue;
			}
			$strNewName = $arElement['PROPERTY_SYS_ORIGINAL_NAME_VALUE'];
			$strPrefix = "";
			if (strlen(trim($arElement['PROPERTY_NAME_PREFIX_VALUE'])) > 0) {
				$strPrefix = $arElement['PROPERTY_NAME_PREFIX_VALUE'];
			} else {
				$strPrefix = $this->doGetPrefixName($arElementsSections[$arElement['PROPERTY_CML2_LINK_VALUE']]);
			}
			$strPrefix = trim($strPrefix);
			$strNewName = trim($strNewName);
			if ($strPrefix == "-") {
				$strPrefix = "";
			} elseif (strlen($strPrefix) > 0) {
				$strNewName = trim($strPrefix . " " . $strNewName);
			}
			if ($strNewName != $arElement['NAME']) {
				$arFields = array(
					"NAME" => $strNewName,
					//"CODE" => $this->doTranslitName($strNewName)
				);
				$obElement->Update($arElement['ID'], $arFields);
			}
		}
	}

	function doUpdateDescriptions()
	{
		$obElement = new \CIBlockElement();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "PROPERTY_CML2_ARTICLE"));
		while ($arElement = $rElements->Fetch()) {
			if (in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				continue;
			}
			$strArticle = $arElement['PROPERTY_CML2_ARTICLE_VALUE'];
			$strArticle = $this->doNormalizeArticlePhoto($strArticle);
			$strFile = $this->arOptions['DESCRIPTION_DIR'] . $strArticle . ".html";
			if (file_exists($strFile)) {
				$arFields = array(
					"DETAIL_TEXT" => file_get_contents($strFile),
					"DETAIL_TEXT_TYPE" => "html",
				);
				$obElement->Update($arElement['ID'], $arFields);
			}
		}
	}

	function doGetPrefixName($iSectionId)
	{
		static $arCacheSections = array();
		if (!isset($arCacheSections[$iSectionId])) {
			$strPrefix = "";
			$rSectionsTree = \CIBlockSection::GetNavChain(IBLOCK_CATALOG, $iSectionId);
			while ($arSectionTree = $rSectionsTree->Fetch()) {
				$rSection = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $arSectionTree['ID']), false, array("UF_*"));
				if ($arSection = $rSection->Fetch()) {
					if (strlen($arSection['UF_NAME_PREFIX']) > 0) {
						$strPrefix = $arSection['UF_NAME_PREFIX'];
					}
				}
			}
			$arCacheSections[$iSectionId] = trim($strPrefix);
		}
		return $arCacheSections[$iSectionId];
	}

	function doSetAfterTestProducts()
	{
		$obElement = new \CIBlockElement();
		$obElement->SetPropertyValueCode($this->arOptions['ACTION_AFTER_TEST'], "PRODUCTS_AUTO", $this->arAfterTestProducts);
	}

	function doCheckElementsActive()
	{
		$obElement = new \CIBlockElement();
		//Проходимся по основным товарам и ищем что будем принудительно отключать
		$arPreventInactive = array();
		$arMainPresale = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "ACTIVE", "PROPERTY_CML2_ARTICLE", "PROPERTY_IS_PRESALE"));
		while ($arElement = $rElements->Fetch()) {
			if ($arElement['PROPERTY_IS_PRESALE_VALUE'] == "Y") {
				$arMainPresale[] = $arElement['ID'];
			}
			if (strpos(strtolower($arElement['PROPERTY_CML2_ARTICLE_VALUE']), "(брак)") !== false) {
				$arPreventInactive[] = $arElement['ID'];
			}
			/*if (strpos(strtolower($arElement['PROPERTY_CML2_ARTICLE_VALUE']), "(бу)") !== false) {
				$arPreventInactive[] = $arElement['ID'];
			}
			if (strpos(strtolower($arElement['PROPERTY_CML2_ARTICLE_VALUE']), "_бу") !== false) {
				$arPreventInactive[] = $arElement['ID'];
			}*/
		}

		$arActiveByMain = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "ACTIVE", "PROPERTY_CML2_LINK", "CATALOG_QUANTITY"));
		while ($arElement = $rElements->Fetch()) {
			if (in_array($arElement['PROPERTY_CML2_LINK_VALUE'], $arPreventInactive)) {
				if ($arElement['ACTIVE'] != "N") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "N"));
				}
				continue;
			}
			if (in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				continue;
			}
			if (in_array($arElement['PROPERTY_CML2_LINK_VALUE'], $this->arNotChangedElementsID)) {
				continue;
			}
			$newActive = "Y";
			$arPrice = \CPrice::GetBasePrice($arElement['ID']);
			if (!in_array($arElement['PROPERTY_CML2_LINK_VALUE'], $arMainPresale) && $arElement['CATALOG_QUANTITY'] <= 0) {
				$newActive = "N";
			}
			if ($arPrice['PRICE'] <= 0) {
				$newActive = "N";
			}
			if ($newActive == "Y") {
				$arActiveByMain[$arElement['PROPERTY_CML2_LINK_VALUE']]++;
				if ($arElement['ACTIVE'] != "Y") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "Y"));
				}
			} else {
				if ($arElement['ACTIVE'] == "Y") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "N"));
				}
			}
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "ACTIVE"));
		while ($arElement = $rElements->Fetch()) {
			if (in_array($arElement['ID'], $arMainPresale)) {
				if ($arElement['ACTIVE'] != "Y") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "Y"));
				}
				continue;
			}
			if (in_array($arElement['ID'], $arPreventInactive)) {
				if ($arElement['ACTIVE'] != "N") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "N"));
				}
				continue;
			}
			if (in_array($arElement['ID'], $this->arNotChangedElementsID)) {
				continue;
			}
			if ($arActiveByMain[$arElement['ID']] > 0) {
				if ($arElement['ACTIVE'] != "Y") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "Y"));
				}
			} else {
				if ($arElement['ACTIVE'] == "Y") {
					$obElement->Update($arElement['ID'], array("ACTIVE" => "N"));
				}
			}
		}
	}

	function doCheckElementsPreSale()
	{
		$obElement = new \CIBlockElement();
		$arMainPresale = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "ACTIVE", "PROPERTY_CML2_ARTICLE", "PROPERTY_IS_PRESALE", "CATALOG_QUANTITY"));
		while ($arElement = $rElements->Fetch()) {
			if ($arElement['PROPERTY_IS_PRESALE_VALUE'] == "Y") {
				$arMainPresale[] = $arElement['ID'];
				if ($arElement['CATALOG_CAN_BUY_ZERO_ORIG'] != "Y") {
					$arProductFields = array(
						"CAN_BUY_ZERO" => "Y",
					);
					\CCatalogProduct::Update($arElement['ID'], $arProductFields);
				}
			} else {
				if ($arElement['CATALOG_CAN_BUY_ZERO'] == "Y") {
					$arProductFields = array(
						"CAN_BUY_ZERO" => "D",
					);
					\CCatalogProduct::Update($arElement['ID'], $arProductFields);
				}
			}
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "ACTIVE", "PROPERTY_CML2_LINK", "CATALOG_QUANTITY"));
		while ($arElement = $rElements->Fetch()) {
			if (in_array($arElement['PROPERTY_CML2_LINK_VALUE'], $arMainPresale)) {
				if ($arElement['CATALOG_CAN_BUY_ZERO_ORIG'] != "Y") {
					$arProductFields = array(
						"CAN_BUY_ZERO" => "Y",
					);
					\CCatalogProduct::Update($arElement['ID'], $arProductFields);
				}
			} else {
				if ($arElement['CATALOG_CAN_BUY_ZERO'] == "Y") {
					$arProductFields = array(
						"CAN_BUY_ZERO" => "D",
					);
					\CCatalogProduct::Update($arElement['ID'], $arProductFields);
				}
			}
		}
	}

	function doFirstCheckQuantity()
	{
		$rSkuElement = \CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "PROPERTY_SYS_LINK_1C"));
		while ($arSkuElement = $rSkuElement->Fetch()) {
			if ($arSkuElement['PROPERTY_SYS_LINK_1C_VALUE'] <= 0) {
				continue;
			}
			$iTotalQuantity = 0;
			$arCurrentStoreData = array();
			$ar1CStoreData = array();
			$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arSkuElement['ID']));
			while ($arStoreData = $rStoreData->Fetch()) {
				$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
			}

			$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arSkuElement['PROPERTY_SYS_LINK_1C_VALUE']));
			while ($arStoreData = $rStoreData->Fetch()) {
				$ar1CStoreData[$arStoreData['STORE_ID']] = $arStoreData;
			}

			foreach ($ar1CStoreData as $k => $v) {
				$iTotalQuantity += $v['AMOUNT'];
				if (isset($arCurrentStoreData[$k]) && $v['AMOUNT'] != $arCurrentStoreData[$k]['AMOUNT']) {
					\CCatalogStoreProduct::Update($arCurrentStoreData[$k]['ID'], array(
						"AMOUNT" => $v['AMOUNT'],
					));
				}
				unset($arCurrentStoreData[$k]);
			}
			foreach ($arCurrentStoreData as $k => $v) {
				\CCatalogStoreProduct::Update($v['ID'], array(
					"AMOUNT" => 0,
				));
			}

			$arCatalogProduct = \CCatalogProduct::GetByID($arSkuElement['ID']);
			if ($arCatalogProduct && $arCatalogProduct['QUANTITY'] != $iTotalQuantity) {
				$arProductFields = array(
					"QUANTITY" => $iTotalQuantity,
				);
				\CCatalogProduct::Update($arSkuElement['ID'], $arProductFields);
			}
		}
	}
}
