<?

namespace Webavk\H2OSport\Cron;

class CatalogMake
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arLinks = array();
	protected $arExistsElements = array();
	protected $arExistsProductOffers = array();
	protected $arStorePropVariants = array();
	protected $arPreventLoadItems = array();
	protected $arOnePhotos = array();
	protected $bIsLog = false;
	protected $arCacheElementsData = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		
		
		/*
		$this->doLog("Start CheckPhotosProportions");
		$this->doCheckPhotosProportions();
		$this->doLog("Start LoadOnePhotos");
		$this->doLoadOnePhotos();
		$this->doLog("Start LoadReCatalogStructure");
		$this->doLoadReCatalogStructure();
		$this->doLog("Start LoadExistsElements");
		$this->doLoadExistsElements();
		$this->doLog("Start LoadPreventLoadElements");
		$this->doLoadPreventLoadElements();
		$this->doLog("Start MakeElements");
		$this->doMakeElements();
		$this->doLog("Start CheckDiscountPercent");
		$this->doCheckDiscountPercent();
		$this->doLog("Start CheckSectionsActive");
		$this->doCheckSectionsActive();
		$this->doLog("Start CheckBuyPrices");
		$this->doCheckBuyPrices();
		 * 
		 */
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLoadOnePhotos()
	{
		$arPhotos = scandir($this->arOptions['PHOTOS_ONE_DIR']);
		foreach ($arPhotos as $strName)
		{
			if (in_array($strName, array(".", "..")))
			{
				continue;
			}
			$strFullName = $this->arOptions['PHOTOS_ONE_DIR'] . $strName;
			if (is_file($strFullName))
			{
				$ar = pathinfo($strFullName);
				$this->arOnePhotos[strtolower(trim($ar['filename']))] = $strFullName;
			}
		}
	}

	function doCheckSectionsActive()
	{
		$obSection = new \CIBlockSection();
		$rSections = \CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG), false);
		while ($arSection = $rSections->Fetch())
		{
			if ($this->bIsStop)
			{
				break;
			}
			if ($arSection['ID'] == DEFAULT_CATEGORY_ID)
			{
				continue;
			}
			$arElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y", "CATALOG_AVAILABLE" => "Y"), array("IBLOCK_ID"))->Fetch();
			if ($arElements['CNT'] > 0)
			{
				if ($arSection['ACTIVE'] != "Y")
				{
					$obSection->Update($arSection['ID'], array("ACTIVE" => "Y"));
				}
			} else
			{
				if ($arSection['ACTIVE'] == "Y")
				{
					$obSection->Update($arSection['ID'], array("ACTIVE" => "N"));
				}
			}
		}
	}

	/**
	 * Загружаем структуру каталогов
	 */
	function doLoadReCatalogStructure()
	{
		$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HL_1CLINKS_ID)->fetch();
		$oHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
		$oHLBlockClass = $oHLBlock->getDataClass();
		$rDataCurrent = $oHLBlockClass::getList();
		while ($arDataCurrent = $rDataCurrent->fetch())
		{
			if (is_array($arDataCurrent['UF_1C_ELEMENTS']))
			{
				foreach ($arDataCurrent['UF_1C_ELEMENTS'] as $val)
				{
					$this->arLinks[$val] = $arDataCurrent['UF_CATALOG_SECTION'];
				}
			}
		}
		//Остальные товары запихиваем в одну категорию-заглушку
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_UT), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			if (!isset($this->arLinks[$arElement['ID']]))
			{
				$this->arLinks[$arElement['ID']] = DEFAULT_CATEGORY_ID;
			}
		}
	}

	/**
	 * Загружаем список ID элементов каталога
	 */
	function doLoadExistsElements()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			$this->arExistsElements['ELEMENTS'][$arElement['ID']] = $arElement['ID'];
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			$this->arExistsElements['OFFERS'][$arElement['ID']] = $arElement['ID'];
		}
	}

	/**
	 * Определяем перечень товаров, для которых обязательна принудительная загрузка
	 */
	function doLoadPreventLoadElements()
	{
		$rSections = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_UT, "UF_MAKE_CATALOG" => 1));
		while ($arSection = $rSections->Fetch())
		{
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_UT, "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				$this->arPreventLoadItems[] = $arElement['ID'];
			}
		}
	}

	/**
	 * Проверка элементов каталога
	 */
	function doMakeElements()
	{
		$i = 1;
		foreach ($this->arLinks as $iElement1CId => $iCategoryId)
		{
			if ($this->bIsStop)
			{
				break;
			}
			/*
			  if ($iElement1CId != 291767)
			  {
			  continue;
			  } */
			/* if ($iElement1CId != 2237)
			  {
			  continue;
			  } */
			$this->doLog("Make element " . $i . " of " . count($this->arLinks) . " [" . $iElement1CId . "]");
			$productId = $this->findCatalogProductIdBy1CProductId($iElement1CId);
			$this->doMakeElement($iElement1CId, $productId, $iCategoryId);
			unset($this->arCacheElementsData[$iElement1CId]);
			unset($this->arCacheElementsData[$productId]);
			$i++;
		}
	}

	/**
	 * Проверка элемента каталога
	 * @param int $iElementId1C
	 * @param int $iElementIdCatalog
	 * @param int $iCategoryId
	 */
	function doMakeElement($iElementId1C, $iElementIdCatalog, $iCategoryId)
	{
		$obElement = new \CIBlockElement();
		$ar1CElement = $this->getElementData($iElementId1C);
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arPropsByArticle = $this->doParseArticle($ar1CElement['PROPERTIES']['CML2_ARTICLE']['~VALUE']);
		$bIsNoRename = false;
		if (in_array("неизвестный TYPE", $arPropsByArticle))
		{
			$bIsNoRename = true;
		}
		$arFields = array(
			"IBLOCK_ID" => IBLOCK_CATALOG,
			"NAME" => $ar1CElement['NAME'],
			"CODE" => $this->doTranslitName($ar1CElement['NAME']),
			"IBLOCK_SECTION_ID" => $iCategoryId,
			"ACTIVE" => "Y",
			"XML_ID" => $ar1CElement['XML_ID'],
			"PROPERTY_VALUES" => array(
				"CML2_ARTICLE" => $ar1CElement['PROPERTIES']['CML2_ARTICLE']['~VALUE'],
				"SYS_LINK_1C" => $ar1CElement['ID'],
				"INFO_PROPS_FROM_ARTICLE" => implode("\n", $arPropsByArticle),
				"INFO_NAME_1C" => $ar1CElement['NAME'],
				"MANUAL_UPDATE_URL_NAME" => "Y"
			//"INFO_NAME_PREFIX" => $this->doParseNamePrefix($ar1CElement['NAME'])
			)
		);
		if ($arCatalogElement)
		{
			if (strlen($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']) > 0)
			{
				$arFields['NAME'] = trim($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']);
				$arFields['CODE'] = $this->doTranslitName($arFields['NAME']);
			}
		}
		$strSeason = "";
		if (is_array($arPropsByArticle))
		{
			foreach ($arPropsByArticle as $v)
			{
				$ar = explode(":", $v);
				if ($ar[0] == "Сезон")
				{
					$strSeason = $ar[1];
				}
			}
		}
		if ($bIsNoRename)
		{
			$strNamePrefix = false;
		} else
		{
			$strNamePrefix = $this->doParseNamePrefix($ar1CElement['NAME'], $arFields['PROPERTY_VALUES']['CML2_ARTICLE']);
		}
		if (!$arCatalogElement)
		{
			$arPhotosInDir = $this->doGetElementPhotos($this->doNormalizeArticlePhoto($arFields['PROPERTY_VALUES']['CML2_ARTICLE']), false);
			if (!empty($arPhotosInDir) || in_array($iElementId1C, $this->arPreventLoadItems))
			{
				$iElementIdCatalog = $obElement->Add($arFields);
				if ($iElementIdCatalog > 0)
				{
					$arCatalogElement = $this->getElementData($iElementIdCatalog, true);
				} else
				{
					echo $obElement->LAST_ERROR . "\n";
				}
			}
		}
		if ($arCatalogElement)
		{
			$this->doCheckProperty($arCatalogElement['ID'], "MATERIAL_TOP", $ar1CElement['PROPERTIES']['MATERIAL_VERKHA'], $arCatalogElement['PROPERTIES']['MATERIAL_TOP']);
			if (strlen($strNamePrefix) > 0)
			{
				$this->doCheckProperty($arCatalogElement['ID'], "INFO_NAME_PREFIX", $strNamePrefix, $arCatalogElement['PROPERTIES']['INFO_NAME_PREFIX']);
			} else
			{
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "INFO_NAME_PREFIX", false);
			}
			$arCatalogElement = $this->getElementData($iElementIdCatalog, true);
			$arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE'] = trim($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']);
			if ($bIsNoRename)
			{
				$strNewnameGenerated = "";
			} else
			{
				$strNewnameGenerated = $this->doGenerateName($arCatalogElement['PROPERTIES']['INFO_NAME_PREFIX'], $arCatalogElement['PROPERTIES']['CML2_ARTICLE'], $arCatalogElement['PROPERTIES']['MATERIAL_TOP']);
			}
			$this->doCheckProperty($arCatalogElement['ID'], "INFO_NAME_GENERATED", $strNewnameGenerated, $arCatalogElement['PROPERTIES']['INFO_NAME_GENERATED']);
			$arCatalogElement['PROPERTIES']['INFO_NAME_GENERATED']['VALUE'] = $strNewnameGenerated;
			$strName = $ar1CElement['NAME'];
			if (strlen($arCatalogElement['PROPERTIES']['INFO_NAME_GENERATED']['VALUE']) > 0)
			{
				$strName = $arCatalogElement['PROPERTIES']['INFO_NAME_GENERATED']['VALUE'];
			} else
			{
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "INFO_NAME_GENERATED", false);
			}
			if (strlen($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']) > 0)
			{
				$strName = $arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE'];
			}

			if (strlen($strName) > 0)
			{
				$arUpdate = array(
					"NAME" => $strName
				);
				if ($arCatalogElement['PROPERTIES']['MANUAL_UPDATE_URL_NAME']['VALUE'] == "Y")
				{
					if (strpos($arUpdate['NAME'], $arFields['PROPERTY_VALUES']['CML2_ARTICLE']) === false)
					{
						$arUpdate['CODE'] = $this->doTranslitName($arUpdate['NAME'] . " " . $arFields['PROPERTY_VALUES']['CML2_ARTICLE']);
					} else
					{
						$arUpdate['CODE'] = $this->doTranslitName($arUpdate['NAME']);
					}
				}
				$obElement->Update($arCatalogElement['ID'], $arUpdate);
			}
			$this->doCheckElementPhotos($arCatalogElement['ID']);
			//Проверка свойств
			$obElement->SetPropertyValueCode($arCatalogElement['ID'], "INFO_PROPS_FROM_ARTICLE", $arFields['PROPERTY_VALUES']['INFO_PROPS_FROM_ARTICLE']);
			if (strlen($strSeason) > 0)
			{
				$strSeasonGroup = false;
				$this->doCheckProperty($arCatalogElement['ID'], "COLLECTION", $strSeason, $arCatalogElement['PROPERTIES']['COLLECTION']);
				//Загружаем значение свойства сохраненное
				$arEl = \CIBlockElement::GetList(array(), array("ID" => $arCatalogElement['ID']), false, false, array("ID", "PROPERTY_COLLECTION"))->Fetch();
				//Получаем сезон элемента справочника
				if ($arEl && $arEl['PROPERTY_COLLECTION_VALUE'] > 0)
				{
					$arElSeason = \CIBlockElement::GetList(array(), array("ID" => $arEl['PROPERTY_COLLECTION_VALUE'], "IBLOCK_ID" => 7), false, false, array("ID", "PROPERTY_SEASON"))->Fetch();
					if ($arElSeason && $arElSeason['PROPERTY_SEASON_VALUE'] > 0)
					{
						$arElSeasonGroup = \CIBlockElement::GetList(array(), array("ID" => $arElSeason['PROPERTY_SEASON_VALUE']), false, false, array("ID", "NAME"))->Fetch();
						if ($arElSeasonGroup)
						{
							$strSeasonGroup = $arElSeasonGroup['NAME'];
						}
					}
				}
				//прописываем у товара
				if (strlen($strSeasonGroup) > 0)
				{
					$this->doCheckProperty($arCatalogElement['ID'], "SEASON", $strSeasonGroup, $arCatalogElement['PROPERTIES']['SEASON']);
				}
			}
			$this->doCheckProperty($arCatalogElement['ID'], "COLOR", $ar1CElement['PROPERTIES']['TSVET'], $arCatalogElement['PROPERTIES']['COLOR']);
			$this->doCheckProperty($arCatalogElement['ID'], "INFO_NAME_1C", $ar1CElement['NAME'], $arCatalogElement['PROPERTIES']['INFO_NAME_1C']);
			$this->doCheckProperty($arCatalogElement['ID'], "MATERIAL_LINING", $ar1CElement['PROPERTIES']['MATERIAL_PODKLADKI'], $arCatalogElement['PROPERTIES']['MATERIAL_LINING']);

			$this->doCheckProperty($arCatalogElement['ID'], "COUNTRY", $ar1CElement['PROPERTIES']['STRANA_IZGOTOVITEL'], $arCatalogElement['PROPERTIES']['COUNTRY']);
			$this->doCheckProperty($arCatalogElement['ID'], "BRAND", "Francesco Donni", $arCatalogElement['PROPERTIES']['BRAND']);

			$arOldPrice = $this->GetProductStartPrice($ar1CElement['ID']);
			$this->doMakeOffers($ar1CElement['ID'], $arCatalogElement['ID'], $arOldPrice['PRICE'], $ar1CElement['PROPERTIES']['CML2_ARTICLE']['~VALUE']);
			
			//Грузим список доступных размеров и обновляем
			$arAvailable = array();
			$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $arCatalogElement['ID'], "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y"), false, false, array("ID", "PROPERTY_SIZE"));
			while ($arOffer = $rOffers->Fetch())
			{
				$arAvailable[] = $arOffer['PROPERTY_SIZE_VALUE'];
			}
			$obElement->SetPropertyValueCode($arCatalogElement['ID'], "SYS_SIZE_AVAILABLE", $arAvailable);
		}
		unset($obElement);
	}

	/**
	 * Разбираем название на префикс названия
	 * @param string $strName
	 * @param string $strArticle
	 * @return string
	 */
	function doParseNamePrefix($strName, $strArticle)
	{
		$strResult = "";
		$sPos = strpos($strName, $strArticle);
		if ($sPos !== false)
		{
			$strResult = trim(substr($strName, 0, $sPos - 1));
		}
		return $strResult;
	}

	function doGenerateName($arPropertyNamePrefix, $arPropertyArticle, $arPropertyMaterialTop)
	{
		$strResult = "";
		if ($arPropertyNamePrefix['VALUE'] > 0)
		{
			$arData = array();
			$arElement = \CIBlockElement::GetList(array(), array("ID" => $arPropertyNamePrefix['VALUE']), false, false, array("ID", "NAME"))->Fetch();
			if ($arElement)
			{
				$arData[] = $arElement['NAME'];
			}
			if ($arPropertyMaterialTop["VALUE"] > 0)
			{
				$arElement = \CIBlockElement::GetList(array(), array("ID" => $arPropertyMaterialTop['VALUE']), false, false, array("ID", "NAME", "PROPERTY_SUFFIX_FOR_NAME_PRODUCT"))->Fetch();
				if ($arElement)
				{
					if (strlen($arElement['PROPERTY_SUFFIX_FOR_NAME_PRODUCT_VALUE']) > 0)
					{
						$arData[] = $arElement['PROPERTY_SUFFIX_FOR_NAME_PRODUCT_VALUE'];
					} else
					{
						$arData[] = $arElement['NAME'];
					}
				}
			}
			if (strlen($arPropertyArticle['VALUE']) > 0)
			{
				//$arData[] = $arPropertyArticle['VALUE'];
			}
			$strResult = trim(implode(" ", $arData));
		}
		return $strResult;
	}

	/**
	 * Проверка свойства
	 * @param int $iElementIdCatalog
	 * @param string $strPropertyCode
	 * @param array $arProperty1C
	 * @param array $arPropertyCatalog
	 */
	function doCheckProperty($iElementIdCatalog, $strPropertyCode, $arProperty1C, $arPropertyCatalog)
	{
		$newValue = false;
		if ($arPropertyCatalog['PROPERTY_TYPE'] == "E")
		{
			//Ищем или создаем запись в справочнике
			if ((is_array($arProperty1C['VALUE']) && !empty($arProperty1C['VALUE'])) || (!is_array($arProperty1C['VALUE']) && strlen($arProperty1C['VALUE']) > 0))
			{
				$newValue = $this->doFindReferenceValue(is_array($arProperty1C) ? $arProperty1C['VALUE'] : $arProperty1C, $arPropertyCatalog['LINK_IBLOCK_ID']);
			}
		} elseif ($arPropertyCatalog['PROPERTY_TYPE'] == "S")
		{
			if (is_array($arProperty1C) && isset($arProperty1C['VALUE']))
			{
				$newValue = $arProperty1C['VALUE'];
			} else
			{
				$newValue = $arProperty1C;
			}
		} else
		{
			$newValue = $arProperty1C['VALUE'];
		}

		$newValue = trim($newValue);
		$isMath = true;
		if (!is_array($arPropertyCatalog['VALUE']))
		{
			if ($newValue != $arPropertyCatalog['VALUE'])
			{
				$isMath = false;
			}
		} else
		{
			if (in_array($newValue, $arPropertyCatalog['VALUE']))
			{
				unset($arPropertyCatalog['VALUE'][array_search($newValue, $arPropertyCatalog['VALUE'])]);
				if (!empty($arPropertyCatalog['VALUE']))
				{
					$isMath = false;
				}
			} else
			{
				$isMath = false;
			}
		}
		if (!$isMath)
		{
			$obElement = new \CIBlockElement();
			$obElement->SetPropertyValueCode($iElementIdCatalog, $strPropertyCode, $newValue);
			unset($obElement);
		}
	}

	/**
	 * Поиск и создание записи справочника
	 * @param string $strValue
	 * @param int $IBLOCK_ID
	 * @return int
	 */
	function doFindReferenceValue($strValue, $IBLOCK_ID)
	{
		$iResult = false;
		$strValue = trim($strValue);
		$obElement = new \CIBlockElement();
		$rElement = \CIBlockElement::GetList(array(), array("PROPERTY_SYNONIM" => $strValue, "IBLOCK_ID" => $IBLOCK_ID), false, false, array("ID"));
		if ($arElement = $rElement->Fetch())
		{
			$iResult = $arElement['ID'];
		} else
		{
			$iResult = $obElement->Add(array(
				"IBLOCK_ID" => $IBLOCK_ID,
				"NAME" => $strValue,
				"PROPERTY_VALUES" => array(
					"SYNONIM" => array($strValue)
				)
			));
		}
		unset($obElement);
		return $iResult;
	}

	/**
	 * Проверка фотографий товара
	 * @param int $iElementIdCatalog
	 */
	function doCheckElementPhotos($iElementIdCatalog)
	{
		$obElement = new \CIBlockElement();
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arPhotosInDir = $this->doGetElementPhotos($arCatalogElement['PROPERTIES']['CML2_ARTICLE']['~VALUE'], false);

		//Включаем/выключаем товар в зависимости от наличия фото
		if ($arCatalogElement['ACTIVE'] == "Y")
		{
			if (empty($arPhotosInDir))
			{
				$obElement->Update($arCatalogElement['ID'], array("ACTIVE" => "N"));
			}
		} else
		{
			if (!empty($arPhotosInDir))
			{
				$obElement->Update($arCatalogElement['ID'], array("ACTIVE" => "Y"));
			}
		}

		if (count($arPhotosInDir) == 1)
		{
			if ($arCatalogElement['SORT'] != 1000000)
			{
				$obElement->Update($arCatalogElement['ID'], array("SORT" => 1000000));
			}
		} else if (count($arPhotosInDir) > 1)
		{
			if ($arCatalogElement['SORT'] == 1000000)
			{
				$obElement->Update($arCatalogElement['ID'], array("SORT" => 500));
			}
		}

		//Проверяем и обновляем главное фото
		$strMainPhotoNew = false;
		$strMainPhotoOld = false;
		$iMainPhotoOldId = false;
		if (isset($arPhotosInDir[0]))
		{
			$strMainPhotoNew = $arPhotosInDir[0];
		}
		if ($arCatalogElement['PREVIEW_PICTURE'] > 0)
		{
			$iMainPhotoOldId = $arCatalogElement['PREVIEW_PICTURE'];
			$strMainPhotoOld = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iMainPhotoOldId);
		}
		$arFields = array();
		if ($strMainPhotoNew === false && $strMainPhotoOld !== false)
		{
			//Удаляем фото анонса
			$arFields = array(
				"PREVIEW_PICTURE" => array(
					"del" => "Y",
					"old_file" => $iMainPhotoOldId
				)
			);
		} elseif ($strMainPhotoNew !== false && $strMainPhotoOld === false)
		{
			//Устанавливаем фото
			$arFields = array(
				"PREVIEW_PICTURE" => \CFile::MakeFileArray($strMainPhotoNew)
			);
		} elseif ($strMainPhotoNew !== false && $strMainPhotoOld !== false)
		{
			if (!$this->ComparePhotos($strMainPhotoNew, $strMainPhotoOld))
			{
				$arFields = array(
					"PREVIEW_PICTURE" => \CFile::MakeFileArray($strMainPhotoNew)
				);
				$arFields['PREVIEW_PICTURE']['old_file'] = $iMainPhotoOldId;
			}
		}
		if (!empty($arFields))
		{
			if (!$obElement->Update($arCatalogElement['ID'], $arFields))
			{
				echo $obElement->LAST_ERROR . "\n";
			}
		}

		//Проверка и обновление дополнительных фото
		//Сначала формируем массив хэшей и проверяем
		$arHashOld = array();
		$arHashNew = array();
		foreach ($arPhotosInDir as $strFile)
		{
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashNew[$strHash] = $strHash;
		}
		if (!is_array($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE']))
		{
			$arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] = array($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE']);
		}
		foreach ($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $strFile)
		{
			$strFile = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iMainPhotoOldId);
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashOld[$strHash] = $strHash;
		}
		//Сравниваем массивы
		foreach ($arHashNew as $k => $v)
		{
			if (isset($arHashOld[$k]))
			{
				unset($arHashOld[$k]);
				unset($arHashNew[$k]);
			}
		}
		if (!empty($arHashOld) || !empty($arHashNew))
		{
			//Обновляем дополнительные фото.
			//Сначала удалим старые значения
			$arProp = array();
			foreach ($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $strFile)
			{
				$arProp[$arCatalogElement['PROPERTIES']['MORE_PHOTOS']['PROPERTY_VALUE_ID'][$k]] = array(
					"del" => "Y",
					"old_file" => $strFile
				);
			}

			//И сформируем новые
			foreach ($arPhotosInDir as $k => $strFile)
			{
				$arProp['n' . $k] = array(
					"VALUE" => \CFile::MakeFileArray($strFile),
					"DESCRIPTION" => ""
				);
			}
			if (!empty($arProp))
			{
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "MORE_PHOTOS", $arProp);
			}
		}

		unset($obElement);
		$arCatalogElement = $this->getElementData($iElementIdCatalog, true);
	}

	/**
	 * Сраваниваем фотографии по содержимому
	 * @param string $strFile1
	 * @param string $strFile2
	 */
	function ComparePhotos($strFile1, $strFile2)
	{
		$hash1 = filesize($strFile1) . "|" . md5_file($strFile1) . "|" . crc32(file_get_contents($strFile1));
		$hash2 = filesize($strFile2) . "|" . md5_file($strFile2) . "|" . crc32(file_get_contents($strFile2));
		return $hash1 == $hash2;
	}

	/**
	 * Получаем массив фотографий по артикулу (либо случайный набор фото)
	 * @param string $strElementArticle
	 * @param bool $bRandom
	 */
	function doGetElementPhotos($strElementArticle, $bRandom = false)
	{
		$strScanDir = $this->arOptions['PHOTOS_DIR'];
		if ($bRandom)
		{
			$strScanDir = $this->arOptions['PHOTOS_RANDOM_DIR'];
		} else
		{
			/**
			 * @todo Добавить поиск папки по артикулу
			 */
			$strScanDir .= $strElementArticle . "/";
			if (!file_exists($strScanDir))
			{
				if (substr($strElementArticle, strlen($strElementArticle) - 1, 1) == ".")
				{
					$strScanDir = $this->arOptions['PHOTOS_DIR'] . substr($strElementArticle, 0, strlen($strElementArticle) - 1) . "/";
				}
			}
			if (!file_exists($strScanDir))
			{
				$checkArticle = str_replace("/", "_", $strElementArticle);
				$strScanDir = $this->arOptions['PHOTOS_DIR'] . $checkArticle . "/";
				if (!file_exists($strScanDir))
				{
					if (substr($checkArticle, strlen($checkArticle) - 1, 1) == ".")
					{
						$strScanDir = $this->arOptions['PHOTOS_DIR'] . substr($checkArticle, 0, strlen($checkArticle) - 1) . "/";
					}
				}
			}
		}
		$arFiles = scandir($strScanDir);
		$arNormalizeFiles = array();
		foreach ($arFiles as $strFile)
		{
			if (in_array($strFile, array(".", "..")) || is_dir($strScanDir . "/" . $strFile))
			{
				continue;
			}
			$strFullName = $strScanDir . "/" . $strFile;
			$arPathInfo = pathinfo($strFullName);
			if (in_array(strtolower($arPathInfo['extension']), array("jpg", "gif", "jpeg", "png")))
			{
				$arNormalizeFiles[$arPathInfo['filename']] = $strFullName;
			}
		}
		if ($bRandom)
		{
			shuffle($arNormalizeFiles);
			$tmp = $arNormalizeFiles;
			$arNormalizeFiles = array();
			foreach ($tmp as $v)
			{
				if (count($arNormalizeFiles) >= rand(1, count($tmp)))
				{
					break;
				}
				$arNormalizeFiles[] = $v;
			}
		} else
		{
			ksort($arNormalizeFiles, SORT_NUMERIC);
			$arNormalizeFiles = array_values($arNormalizeFiles);
		}
		if (empty($arNormalizeFiles))
		{
			$strNormalizedArticle = strtolower(trim($strElementArticle));
			if (isset($this->arOnePhotos[$strNormalizedArticle]))
			{
				$arNormalizeFiles[1] = $this->arOnePhotos[$strNormalizedArticle];
			} else
			{
				foreach ($this->arOnePhotos as $k => $v)
				{
					if (strpos($strNormalizedArticle, $k) === 0)
					{
						$arNormalizeFiles[0] = $v;
						break;
					}
				}
			}
		}
		if (empty($arNormalizeFiles))
		{
			$strNormalizedArticle = str_replace(" ", "-", $strNormalizedArticle);
			if (isset($this->arOnePhotos[$strNormalizedArticle]))
			{
				$arNormalizeFiles[1] = $this->arOnePhotos[$strNormalizedArticle];
			} else
			{
				foreach ($this->arOnePhotos as $k => $v)
				{
					if (strpos($strNormalizedArticle, $k) === 0)
					{
						$arNormalizeFiles[0] = $v;
						break;
					}
				}
			}
		}
		return $arNormalizeFiles;
	}

	/**
	 * Проверяем торговые предложения
	 * @param int $iElementId1C
	 * @param int $iElementIdCatalog
	 */
	function doMakeOffers($iElementId1C, $iElementIdCatalog, $strOldPrice, $strArticle)
	{
		/* $strOldPrice = trim(str_replace(" ", "", $strOldPrice));
		  $newPrice = "";
		  for ($i = 0; $i < strlen($strOldPrice); $i++)
		  {
		  $c = substr($strOldPrice, $i, 1);
		  if (strpos('0123456789.', $c) !== false)
		  {
		  $newPrice .= $c;
		  }
		  }
		  $strOldPrice = $newPrice; */
		$obElement = new \CIBlockElement();
		$arProduct = $this->getElementData($iElementIdCatalog);
		//Загружаем имеющиеся предложения
		$this->arExistsProductOffers = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch())
		{
			$this->arExistsProductOffers[$arOffer['ID']] = $arOffer['ID'];
		}
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_UT_OFFERS, "PROPERTY_CML2_LINK" => $iElementId1C), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch())
		{
			$offerId = $this->findCatalogOfferIdBy1COfferId($arOffer['ID'], $iElementIdCatalog);
			unset($this->arExistsProductOffers[$offerId]);
			$this->doMakeOffer($iElementIdCatalog, $offerId, $arOffer['ID'], $arProduct['NAME'], $strArticle);
			unset($this->arCacheElementsData[$offerId]);
			unset($this->arCacheElementsData[$arOffer['ID']]);
		}
		//Находим минимальную цену и устанавливаем
		$minPrice = false;
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID", "CATALOG_GROUP_" . $this->arOptions['BASE_PRICE_ID']));
		while ($arOffer = $rOffers->Fetch())
		{
			if ($minPrice === false && $arOffer['CATALOG_PRICE_1'] > 0)
			{
				$minPrice = $arOffer['CATALOG_PRICE_1'];
			} else if ($minPrice !== false && $arOffer['CATALOG_PRICE_1'] > 0 && $arOffer['CATALOG_PRICE_1'] < $minPrice)
			{
				$minPrice = $arOffer['CATALOG_PRICE_1'];
			}
		}
		if ($minPrice !== false && $minPrice > 0)
		{
			$arPriceCatalog = \CPrice::GetBasePrice($iElementIdCatalog);
			if ($arPriceCatalog['PRICE'] != $minPrice)
			{
				\CPrice::SetBasePrice($iElementIdCatalog, $minPrice, "RUB");
			}

			if (intval($strOldPrice) <= 0)
			{
				$minOldPrice = false;
				$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID", "CATALOG_GROUP_" . OLD_PRICE_ID));
				while ($arOffer = $rOffers->Fetch())
				{
					if ($minOldPrice === false && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] > 0)
					{
						$minOldPrice = $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID];
					} else if ($minOldPrice !== false && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] > 0 && $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID] < $minOldPrice)
					{
						$minOldPrice = $arOffer['CATALOG_PRICE_' . OLD_PRICE_ID];
					}
				}
				if ($minOldPrice > 0)
				{
					$strOldPrice = $minOldPrice;
				}
			}

			if (intval($strOldPrice) > 0 && intval($strOldPrice) <= $minPrice)
			{
				$strOldPrice = "";
			}
			//Проверка старой цены
			$arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			if ($arOldPrice)
			{
				if ($arOldPrice['PRICE'] != floatval($strOldPrice))
				{
					\CPrice::Update($arOldPrice['ID'], array(
						"PRICE" => $strOldPrice,
						"CURRENCY" => "RUB"
					));
				} elseif ($arOldPrice['PRICE'] == 0)
				{
					\CPrice::Delete($arOldPrice['ID']);
				}
			} elseif (strlen($strOldPrice) > 0)
			{
				\CPrice::Add(array(
					"PRODUCT_ID" => $iElementIdCatalog,
					"CATALOG_GROUP_ID" => OLD_PRICE_ID,
					"PRICE" => $strOldPrice,
					"CURRENCY" => "RUB"
				));
			}
			if ($strOldPrice > 0)
			{
				if ($arProduct['PROPERTIES']['LABEL_SALE']['VALUE'] != "Y")
				{
					$obElement->SetPropertyValueCode($iElementIdCatalog, "LABEL_SALE", "Y");
				}
				if ($arProduct['PROPERTIES']['SYS_IS_OLD_PRICE']['VALUE'] != "Y")
				{
					$obElement->SetPropertyValueCode($arProduct['ID'], "SYS_IS_OLD_PRICE", "Y");
				}
			} else
			{
				if ($arProduct['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
				{
					$obElement->SetPropertyValueCode($iElementIdCatalog, "LABEL_SALE", "N");
				}
				if ($arProduct['PROPERTIES']['SYS_IS_OLD_PRICE']['VALUE'] == "Y")
				{
					$obElement->SetPropertyValueCode($arCatalogElement['ID'], "SYS_IS_OLD_PRICE", "N");
				}
			}
			if ($arProduct['PROPERTIES']['SYS_LINK_1C']['VALUE'] <= 0)
			{
				if ($arProduct['ACTIVE'] == "Y")
				{
					$obElement->Update($arProduct['ID'], array("ACTIVE" => "N"));
				}
			}
		} else
		{
			if ($arProduct['ACTIVE'] == "Y")
			{
				$obElement->Update($arProduct['ID'], array("ACTIVE" => "N"));
			}
		}
		//Проверка остатков по складам
		$totalQuantity = 0;
		//Загружаем текущие данные
		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		while ($arStoreData = $rStoreData->Fetch())
		{
			$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		}

		//Смотрим что в 1с
		$arDataBySklad = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch())
		{
			$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arOffer['ID']));
			while ($arStoreData = $rStoreData->Fetch())
			{
				$arDataBySklad[$arStoreData['STORE_ID']] += $arStoreData['AMOUNT'];
				$totalQuantity += $arStoreData['AMOUNT'];
			}
		}
		foreach ($arDataBySklad as $storeId => $amount)
		{
			if (isset($arCurrentStoreData[$storeId]))
			{
				\CCatalogStoreProduct::Update($arCurrentStoreData[$storeId]['ID'], array(
					"AMOUNT" => $amount
				));
				unset($arCurrentStoreData[$storeId]);
			} else
			{
				\CCatalogStoreProduct::Add(array(
					"PRODUCT_ID" => $iElementIdCatalog,
					"STORE_ID" => $storeId,
					"AMOUNT" => $amount
				));
			}
		}

		foreach ($arCurrentStoreData as $arStoreData)
		{
			\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
				"AMOUNT" => 0
			));
		}

		//Проверка остатков
		$arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		if ($arCatalogProduct)
		{
			$arProductFields = array(
				"QUANTITY" => $totalQuantity,
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
			);
			\CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		} else
		{
			$arProductFields = array(
				"ID" => $iElementIdCatalog,
				"QUANTITY_TRACE" => "D",
				"PRICE_TYPE" => "S",
				"QUANTITY" => $totalQuantity,
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
			);
			\CCatalogProduct::Add($arProductFields);
		}

		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		while ($arStoreData = $rStoreData->Fetch())
		{
			$arCurrentStoreData[] = $arStoreData['STORE_NAME'];
		}

		$arValues = array();
		foreach ($arCurrentStoreData as $strStoreName)
		{
			if (isset($this->arStorePropVariants[$strStoreName]))
			{
				$arValues[] = $this->arStorePropVariants[$strStoreName];
			} else
			{
				$rPropertyVariant = \CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "CODE" => "INFO_IN_SKLAD", "VALUE" => $strStoreName));
				if ($arPropertyVariant = $rPropertyVariant->Fetch())
				{
					$this->arStorePropVariants[$strStoreName] = $arPropertyVariant['ID'];
					$arValues[] = $this->arStorePropVariants[$strStoreName];
				} else
				{
					$obPropEnum = new \CIBlockPropertyEnum();
					$this->arStorePropVariants[$strStoreName] = $obPropEnum->Add(array(
						"PROPERTY_ID" => IBLOCK_CATALOG_PROPERTY_INFO_IN_SKLAD,
						"VALUE" => $strStoreName,
						"SORT" => 100
					));
					$arValues[] = $this->arStorePropVariants[$strStoreName];
				}
			}
		}
		if (empty($arValues))
		{
			$arValues = false;
		}
		$obElement->SetPropertyValueCode($iElementIdCatalog, "INFO_IN_SKLAD", $arValues);
		unset($obElement);
	}

	/**
	 * Проверка торгового предложения
	 * @param int $iElementIdCatalog
	 * @param int $iOfferId1C
	 */
	function doMakeOffer($iElementIdCatalog, $iOfferIdCatalog, $iOfferId1C, $strBaseName, $strArticle)
	{
		$obElement = new \CIBlockElement();
		$ar1COffer = $this->getElementData($iOfferId1C);
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arCatalogOffer = $this->getElementData($iOfferIdCatalog);

		$arFields = array(
			"IBLOCK_ID" => IBLOCK_CATALOG_OFFERS,
			"NAME" => $strBaseName,
			"CODE" => $this->doTranslitName($ar1COffer['NAME']),
			"ACTIVE" => "Y",
			"XML_ID" => $ar1COffer['XML_ID'],
			"PROPERTY_VALUES" => array(
				"SYS_LINK_1C" => $ar1COffer['ID'],
				"CML2_LINK" => $iElementIdCatalog,
				"CML2_ARTICLE" => $strArticle
			)
		);
		$strOfferName = trim($ar1COffer['NAME']);
		$strSize = false;
		$end = strrpos($strOfferName, ")");
		if ($end == (strlen($strOfferName) - 1))
		{
			$start = strrpos($strOfferName, "(");
			if ($start !== false)
			{
				$strSize = substr($strOfferName, $start + 1, $end - $start - 1);
			}
		}
		$strSize = trim($strSize);
		if (strlen($strSize) > 0)
		{
			$arFields['NAME'] .= " (" . $strSize . ")";
			$arFields['CODE'] = trim($this->doTranslitName($strSize));
		}
		if (!$arCatalogOffer)
		{
			$iOfferIdCatalog = $obElement->Add($arFields);
			if ($iOfferIdCatalog > 0)
			{
				$arCatalogOffer = $this->getElementData($iOfferIdCatalog, true);
			} else
			{
				echo $obElement->LAST_ERROR . "\n";
			}
		}
		if ($arCatalogOffer)
		{
			if ($arCatalogOffer['NAME'] != $arFields['NAME'])
			{
				$obElement->Update($arCatalogOffer['ID'], array("NAME" => $arFields['NAME']));
			}
			if ($arCatalogOffer['CODE'] != $arFields['CODE'])
			{
				$obElement->Update($arCatalogOffer['ID'], array("CODE" => $arFields['CODE']));
			}
			if (strlen($strSize) > 0)
			{
				$arFields['PROPERTY_VALUES']['SIZE'] = $this->doFindReferenceValue($strSize, $arCatalogOffer['PROPERTIES']['SIZE']['LINK_IBLOCK_ID']);
				if ($arFields['PROPERTY_VALUES']['SIZE'] != $arCatalogOffer['PROPERTIES']['SIZE']['VALUE'])
				{
					$obElement->SetPropertyValueCode($arCatalogOffer['ID'], "SIZE", $arFields['PROPERTY_VALUES']['SIZE']);
				}
			}
			if (strlen($arFields['PROPERTY_VALUES']['CML2_ARTICLE']) > 0)
			{
				if ($arFields['PROPERTY_VALUES']['CML2_ARTICLE'] != $arCatalogOffer['PROPERTIES']['CML2_ARTICLE']['VALUE'])
				{
					$obElement->SetPropertyValueCode($arCatalogOffer['ID'], "CML2_ARTICLE", $arFields['PROPERTY_VALUES']['CML2_ARTICLE']);
				}
			}
		}

		//Проверка остатков
		$arCatalogProduct = \CCatalogProduct::GetByID($iOfferIdCatalog);
		$ar1CProduct = \CCatalogProduct::GetByID($iOfferId1C);
		if ($arCatalogProduct)
		{
			$arProductFields = array(
				"WEIGHT" => $ar1CProduct['WEIGHT'],
				"MEASURE" => $ar1CProduct['MEASURE'],
				"VAT_ID" => $ar1CProduct['VAT_ID'],
				"VAT_INCLUDED" => $ar1CProduct['VAT_INCLUDED'],
				"QUANTITY" => $ar1CProduct['QUANTITY'],
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
					//"PURCHASING_PRICE" => $ar1CProduct[''],
					//"PURCHASING_CURRENCY" => $ar1CProduct[''],
			);
			\CCatalogProduct::Update($iOfferIdCatalog, $arProductFields);
		} else
		{
			$arProductFields = array(
				"ID" => $iOfferIdCatalog,
				"QUANTITY_TRACE" => "D",
				"WEIGHT" => $ar1CProduct['WEIGHT'],
				"MEASURE" => $ar1CProduct['MEASURE'],
				"VAT_ID" => $ar1CProduct['VAT_ID'],
				"VAT_INCLUDED" => $ar1CProduct['VAT_INCLUDED'],
				"PRICE_TYPE" => "S",
				"QUANTITY" => $ar1CProduct['QUANTITY'],
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
					//"PURCHASING_PRICE" => $ar1CProduct[''],
					//"PURCHASING_CURRENCY" => $ar1CProduct[''],
			);
			\CCatalogProduct::Add($arProductFields);
		}
		//Проверка цены
		$arPriceOffer1C = \CPrice::GetBasePrice($iOfferId1C);
		$arPriceCatalog = \CPrice::GetBasePrice($iOfferIdCatalog);
		if ($arPriceCatalog['PRICE'] != $arPriceOffer1C['PRICE'])
		{
			\CPrice::SetBasePrice($iOfferIdCatalog, $arPriceOffer1C['PRICE'], $arPriceOffer1C['CURRENCY']);
		}

		//проверка первоначальной цены и установка старой
		$strOldPrice = "";
		$arOldPrice = $this->GetProductStartPrice($iOfferId1C);
		if ($arOldPrice)
		{
			$strOldPrice = $arOldPrice['PRICE'];
		}
		if (intval($strOldPrice) > 0 && intval($strOldPrice) <= $arPriceOffer1C['PRICE'])
		{
			$strOldPrice = "";
		}
		//Проверка старой цены
		$arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
		if ($arOldPrice)
		{
			if ($arOldPrice['PRICE'] != floatval($strOldPrice))
			{
				\CPrice::Update($arOldPrice['ID'], array(
					"PRICE" => $strOldPrice,
					"CURRENCY" => "RUB"
				));
			} elseif ($arOldPrice['PRICE'] == 0)
			{
				\CPrice::Delete($arOldPrice['ID']);
			}
		} elseif (strlen($strOldPrice) > 0)
		{
			\CPrice::Add(array(
				"PRODUCT_ID" => $iOfferIdCatalog,
				"CATALOG_GROUP_ID" => OLD_PRICE_ID,
				"PRICE" => $strOldPrice,
				"CURRENCY" => "RUB"
			));
		}

		if (($arPriceOffer1C['PRICE'] <= 0 && $arCatalogOffer['ACTIVE'] == "Y") || $arCatalogOffer['PROPERTIES']['SYS_LINK_1C']['VALUE'] <= 0)
		{
			$obElement->Update($arCatalogOffer['ID'], array("ACTIVE" => "N"));
		}

		//Проверка остатков по складам
		//Загружаем текущие данные
		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog));
		while ($arStoreData = $rStoreData->Fetch())
		{
			$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		}
		//Смотрим что в 1с
		$totalQuantity = 0;
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferId1C));
		while ($arStoreData = $rStoreData->Fetch())
		{
			if (isset($arCurrentStoreData[$arStoreData['STORE_ID']]))
			{
				$totalQuantity += $arStoreData['AMOUNT'];
				\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
					"AMOUNT" => $arStoreData['AMOUNT']
				));
				unset($arCurrentStoreData[$arStoreData['STORE_ID']]);
			} else
			{
				$totalQuantity += $arStoreData['AMOUNT'];
				\CCatalogStoreProduct::Add(array(
					"PRODUCT_ID" => $iOfferIdCatalog,
					"STORE_ID" => $arStoreData['STORE_ID'],
					"AMOUNT" => $arStoreData['AMOUNT']
				));
			}
		}
		foreach ($arCurrentStoreData as $arStoreData)
		{
			\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
				"AMOUNT" => 0
			));
		}
		//Проверка остатков
		$arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		if ($arCatalogProduct)
		{
			$arProductFields = array(
				"QUANTITY" => $totalQuantity,
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
			);
			\CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		} else
		{
			$arProductFields = array(
				"ID" => $iElementIdCatalog,
				"QUANTITY_TRACE" => "D",
				"PRICE_TYPE" => "S",
				"QUANTITY" => $totalQuantity,
				"CAN_BUY_ZERO" => "D",
				"QUANTITY_TRACE" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D"
			);
			\CCatalogProduct::Add($arProductFields);
		}
		unset($obElement);
	}

	/**
	 * Ищем товар из каталога сайта по ID товара из каталога 1С
	 * @param int $id
	 */
	function findCatalogProductIdBy1CProductId($id)
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_SYS_LINK_1C" => $id), false, false, array("ID"));
		if ($arElement = $rElements->Fetch())
		{
			return $arElement['ID'];
		}
		return false;
	}

	/**
	 * Ищем предложение из каталога сайта по ID товара из каталога 1С
	 * @param int $id
	 * @param int $iCml2Link
	 */
	function findCatalogOfferIdBy1COfferId($id, $iCml2Link)
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_SYS_LINK_1C" => $id, "PROPERTY_CML2_LINK" => $iCml2Link), false, false, array("ID"));
		if ($arElement = $rElements->Fetch())
		{
			return $arElement['ID'];
		}
		return false;
	}

	/**
	 * Получаем данные элемента
	 * @param int $id
	 */
	function getElementData($id, $fRenew = false)
	{
		if ($id <= 0)
		{
			return false;
		}
		if ($fRenew && isset($this->arCacheElementsData[$id]))
		{
			unset($this->arCacheElementsData[$id]);
		}
		if (!isset($this->arCacheElementsData[$id]))
		{
			$arFields = false;
			if ($id > 0)
			{
				$rElement = \CIBlockElement::GetList(array(), array("ID" => $id));
				if ($rsElement = $rElement->GetNextElement(false, false))
				{
					$arFields = $rsElement->GetFields();
					$arFields['PROPERTIES'] = $rsElement->GetProperties();
				}
			}
			$this->arCacheElementsData[$id] = $arFields;
		}
		return $this->arCacheElementsData[$id];
	}

	/**
	 * Транслитерация названия
	 * @param string $strName
	 */
	function doTranslitName($strName)
	{
		$strName = trim($strName);
		return \CUtil::translit($strName, "ru", array(
					"max_len" => 100,
					"change_case" => 'L',
					"replace_space" => '-',
					"replace_other" => '-',
					"delete_repeat_replace" => true,
					"safe_chars" => '',
		));
	}

	function doParseArticle($strArticle)
	{
		$arResult = array();
		$strArticle = trim($strArticle);
		if (count(explode(" ", $strArticle)) == 2 && count(explode("-", $strArticle)) == 2)
		{
			$arResult = $this->doParseArticleType1($strArticle);
		} elseif (count(explode(" ", $strArticle)) == 2 && count(explode("-", $strArticle)) == 3)
		{
			$arResult = $this->doParseArticleType2($strArticle);
		} elseif (count(explode(" ", $strArticle)) == 1 && count(explode("-", $strArticle)) == 3)
		{
			$arResult = $this->doParseArticleType3($strArticle);
		} else
		{
			$arResult[] = "неизвестный TYPE";
		}
		return $arResult;
	}

	function doParseArticleType1($strArticle)
	{
		$arResult = array();
		$strArticle = trim($strArticle);
		$arType = $this->arOptions['ARTICUL_DECODE']['TYPE1'];
		$arDataExplode = explode(" ", $strArticle);
		$ar = explode("-", $arDataExplode[1]);
		unset($arDataExplode[1]);
		$arDataExplode = array_merge($arDataExplode, $ar);
		$arData = array();
		$arData[1] = substr($arDataExplode[0], 0, 1);
		$arData[2] = substr($arDataExplode[0], 1, 1);
		$arData[3] = substr($arDataExplode[0], 2, 1);
		$arData[4] = substr($arDataExplode[0], 3, 1);

		$arData[5] = substr($arDataExplode[1], 0, 2);
		$arData[6] = substr($arDataExplode[1], 2, strlen($arDataExplode[1]) - 2);

		$arData[7] = substr($arDataExplode[2], 0, 2);
		$arData[8] = substr($arDataExplode[2], 2, 1);
		$arData[9] = substr($arDataExplode[2], 3, strlen($arDataExplode[2]) - 3);

		foreach ($arType as $k => $v)
		{
			if (isset($v['VALUES'][$arData[$k]]))
			{
				$str = $v['NAME'] . ":" . $v['VALUES'][$arData[$k]];
			} else
			{
				$str = $v['NAME'] . ":" . $arData[$k];
			}
			$arResult[] = $str;
		}

		return $arResult;
	}

	function doParseArticleType2($strArticle)
	{
		$arResult = array();
		$strArticle = trim($strArticle);
		$arType = $this->arOptions['ARTICUL_DECODE']['TYPE2'];
		$arDataExplode = explode(" ", $strArticle);
		$ar = explode("-", $arDataExplode[1]);
		unset($arDataExplode[1]);
		$arDataExplode = array_merge($arDataExplode, $ar);
		$arData = array();
		$arData[0] = substr($arDataExplode[0], 0, 1);
		$arData[1] = substr($arDataExplode[0], 1, 1);
		$arData[2] = substr($arDataExplode[0], 2, 1);
		$arData[3] = substr($arDataExplode[0], 3, 1);

		$arData[4] = substr($arDataExplode[1], 0, 3);
		$arData[5] = substr($arDataExplode[1], 3, strlen($arDataExplode[1]) - 3);

		$arData[6] = $arDataExplode[2];

		$arData[7] = substr($arDataExplode[3], 0, 2);
		$arData[8] = substr($arDataExplode[3], 2, 1);
		$arData[9] = substr($arDataExplode[3], 3, strlen($arDataExplode[2]) - 3);

		foreach ($arType as $k => $v)
		{
			if (isset($v['VALUES'][$arData[$k]]))
			{
				$str = $v['NAME'] . ":" . $v['VALUES'][$arData[$k]];
			} else
			{
				$str = $v['NAME'] . ":" . $arData[$k];
			}
			$arResult[] = $str;
		}

		return $arResult;
	}

	function doParseArticleType3($strArticle)
	{
		$arResult = array();
		$strArticle = trim($strArticle);
		$arType = $this->arOptions['ARTICUL_DECODE']['TYPE3'];
		$arDataExplode = explode(" ", $strArticle);
		$ar = explode("-", $arDataExplode[1]);
		unset($arDataExplode[1]);
		$arDataExplode = array_merge($arDataExplode, $ar);
		$arData = array();
		$arData[0] = substr($arDataExplode[0], 0, 1);
		$arData[1] = substr($arDataExplode[0], 1, 1);

		$arData[2] = substr($arDataExplode[1], 0, 1);
		$arData[3] = substr($arDataExplode[1], 1, strlen($arDataExplode[1]) - 2);
		$arData[4] = substr($arDataExplode[1], strlen($arDataExplode[1]) - 1, 1);

		$arData[5] = substr($arDataExplode[2], 0, 2);
		$arData[6] = substr($arDataExplode[2], 2, strlen($arDataExplode[1]) - 2);

		foreach ($arType as $k => $v)
		{
			if (isset($v['VALUES'][$arData[$k]]))
			{
				$str = $v['NAME'] . ":" . $v['VALUES'][$arData[$k]];
			} else
			{
				$str = $v['NAME'] . ":" . $arData[$k];
			}
			$arResult[] = $str;
		}

		return $arResult;
	}

	function doCheckBuyPrices()
	{
		$arPrices = array();
		if (file_exists(dirname(__FILE__) . "/buyprice.csv"))
		{
			file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/buyprice.csv", iconv("windows-1251", "utf-8", file_get_contents(dirname(__FILE__) . "/buyprice.csv")));
			$rFile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/buyprice.csv", "rt");
			if ($rFile)
			{
				while ($ar = fgetcsv($rFile, 10000, ";", '"'))
				{
					if ($this->bIsStop)
					{
						break;
					}
					$price = floatval($ar[2]);
					$article = trim($ar[0]);
					if (strlen($article) > 0)
					{
						$arMainElement = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_CML2_ARTICLE" => $article), false, false, array("ID"))->Fetch();
						if ($arMainElement)
						{
							$arProduct = \CCatalogProduct::GetByID($arMainElement['ID']);
							if (!$arProduct)
							{
								$arProductFields = array(
									"ID" => $arMainElement['ID'],
									"QUANTITY_TRACE" => "D",
									"PRICE_TYPE" => "S",
									"QUANTITY" => 0,
									"CAN_BUY_ZERO" => "D",
									"QUANTITY_TRACE" => "D",
									"NEGATIVE_AMOUNT_TRACE" => "D",
									"PURCHASING_PRICE" => $price,
									"PURCHASING_CURRENCY" => "RUB"
								);
								\CCatalogProduct::Add($arProductFields);
							} else
							{
								if ($arProduct['PURCHASING_PRICE'] != $price)
								{
									$arProductFields = array(
										"PURCHASING_PRICE" => $price,
										"PURCHASING_CURRENCY" => "RUB"
									);
									\CCatalogProduct::Update($arProduct['ID'], $arProductFields);
								}
							}
							$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $arMainElement['ID']), false, false, array("ID"));
							while ($arOffer = $rOffers->Fetch())
							{
								$arProduct = \CCatalogProduct::GetByID($arOffer['ID']);
								if (!$arProduct)
								{
									$arProductFields = array(
										"ID" => $arOffer['ID'],
										"QUANTITY_TRACE" => "D",
										"PRICE_TYPE" => "S",
										"QUANTITY" => 0,
										"CAN_BUY_ZERO" => "D",
										"QUANTITY_TRACE" => "D",
										"NEGATIVE_AMOUNT_TRACE" => "D",
										"PURCHASING_PRICE" => $price,
										"PURCHASING_CURRENCY" => "RUB"
									);
									\CCatalogProduct::Add($arProductFields);
								} else
								{
									if ($arProduct['PURCHASING_PRICE'] != $price)
									{
										$arProductFields = array(
											"PURCHASING_PRICE" => $price,
											"PURCHASING_CURRENCY" => "RUB"
										);
										\CCatalogProduct::Update($arProduct['ID'], $arProductFields);
									}
								}
							}
						}
					}
				}
				fclose($rFile);
			}
		}
	}

	function GetPhotosRecursive($strDir)
	{
		$arResult = array();
		$arPhotos = scandir($strDir);
		foreach ($arPhotos as $strName)
		{
			if (in_array($strName, array(".", "..")))
			{
				continue;
			}
			$strFullName = $strDir . $strName;
			if (is_dir($strFullName))
			{
				$arResult = array_merge($arResult, $this->GetPhotosRecursive($strFullName . "/"));
			} elseif (is_file($strFullName))
			{
				$arResult[] = $strFullName;
			}
		}
		return $arResult;
	}

	function doCheckPhotosProportions()
	{
		$arAllPhotos = array_merge($this->GetPhotosRecursive($this->arOptions['PHOTOS_DIR']), $this->GetPhotosRecursive($this->arOptions['PHOTOS_ONE_DIR']));
		foreach ($arAllPhotos as $strFullPath)
		{
			$this->doCheckPhotoProportions($strFullPath);
		}
	}

	function doCheckPhotoProportions($strPhotoPath)
	{
		$arPath = pathinfo($strPhotoPath);
		$newName = $arPath['dirname'] . "/" . $arPath['filename'] . ".jpg";
		$arSize = getimagesize($strPhotoPath);
		$width = $arSize[0];
		$height = $arSize[1];
		if ($width > 0 && $height > 0)
		{
			$p = $width / $height;
			$h = intval($height * (1600 / $width));
			$origP = 1600 / 1920;
			if ($p > $origP)
			{
				$newWidth = $width;
				$newHeight = intval($width / $origP);
			} else
			{
				$newHeight = $height;
				$newWidth = intval($height * $origP);
			}
			if (intval($p * 100) != intval($origP * 100))
			{
				echo $strPhotoPath . "\n";
				$imgOriginal = imagecreatefromstring(file_get_contents($strPhotoPath));
				$img = imagecreatetruecolor($newWidth, $newHeight);
				$c = imagecolorallocate($img, 255, 255, 255);
				imagefill($img, 0, 0, $c);
				imagecopy($img, $imgOriginal, intval(($newWidth - $width) / 2), intval(($newHeight - $height) / 2), 0, 0, $width, $height);
				@unlink($strPhotoPath);
				imagejpeg($img, $newName, 90);
			}
		}
	}

	function doNormalizeArticlePhoto($strArticle)
	{
		$strArticle = str_replace("/", "_", $strArticle);
		$strArticle = str_replace("\\", "_", $strArticle);
		return $strArticle;
	}

	function GetProductStartPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int) $productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => $this->arOptions['START_PRICE_ID']
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int) $quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int) $quantityTo;

		if ($boolExt === false)
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID'
			);
		} else
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID'
			);
		}

		$db_res = \CPrice::GetListEx(
						array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch())
		{
			return $res;
		}

		return false;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog)
		{
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doCheckDiscountPercent()
	{
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("Розничная Москва", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"CATALOG_TYPE",
			"PROPERTY_SYS_DISCOUNT_SIZE",
			"PROPERTY_SYS_DISCOUNT_PRICE"
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, $arSelect);
		while ($arElement = $rElements->Fetch())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
		}
		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}

		foreach ($arResult['ITEMS'] as $k => $arItem)
		{
			$arItem["PRICES"] = \CIBlockPriceTools::GetItemPrices(IBLOCK_CATALOG, $arResult["PRICES"], $arItem, "Y");
			$percent = 0;
			$iPrice = 0;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$ak = array_keys($arItem['PRICES']);
				$price = $arItem['PRICES'][$ak[0]];
				$price['BASE_PRICE'] = $price['VALUE'];
				$price['VALUE'] = $price['DISCOUNT_VALUE'];
				$price['PRICE'] = $price['DISCOUNT_VALUE'];

				$arItem['RESULT_PRICE'] = $this->doModifyItemPrice($price, $oldPrice);
				$iPrice = $price['PRICE'];
				$percent = $arItem['RESULT_PRICE']['DISCOUNT'] / $arItem['RESULT_PRICE']['BASE_PRICE'];
			} else
			{
				$ak = array_keys($arItem['PRICES']);
				$arItem['RESULT_PRICE'] = $arItem['PRICES'][$ak[0]];
				$iPrice = $arItem['RESULT_PRICE']['DISCOUNT_VALUE'];
				$percent = ($arItem['RESULT_PRICE']['VALUE'] - $arItem['RESULT_PRICE']['DISCOUNT_VALUE']) / $arItem['RESULT_PRICE']['VALUE'];
			}
			$arItem['percent'] = round($percent, 2);
			
			if ($arItem['PROPERTY_SYS_DISCOUNT_SIZE_VALUE'] != $arItem['percent'])
			{
				\CIBlockElement::SetPropertyValueCode($arItem['ID'], "SYS_DISCOUNT_SIZE", $arItem['percent']);
			}
			if ($arItem['PROPERTY_SYS_DISCOUNT_PRICE_VALUE'] != $iPrice)
			{
				\CIBlockElement::SetPropertyValueCode($arItem['ID'], "SYS_DISCOUNT_PRICE", $iPrice);
			}
		}
	}

	function doModifyItemPrice($arPrice, $arOldPrice)
	{
		if ($arOldPrice['VALUE'] > 0)
		{
			if ($arPrice['DISCOUNT'] > 0)
			{
				if ($arPrice['BASE_PRICE'] < $arOldPrice['VALUE'])
				{
					$arPrice['BASE_PRICE'] = $arOldPrice['VALUE'];
					$arPrice['DISCOUNT'] = $arPrice['BASE_PRICE'] - $arPrice['PRICE'];
					$arPrice['PERCENT'] = round($arPrice['DISCOUNT'] / $arPrice['BASE_PRICE'] * 100, 0);
					$arPrice['RATIO_BASE_PRICE'] = $arPrice['BASE_PRICE'];
					$arPrice['RATIO_DISCOUNT'] = $arPrice['DISCOUNT'];
					$arPrice['PRINT_BASE_PRICE'] = FormatCurrency($arPrice['BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_BASE_PRICE'] = FormatCurrency($arPrice['RATIO_BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_DISCOUNT'] = FormatCurrency($arPrice['DISCOUNT'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_DISCOUNT'] = FormatCurrency($arPrice['RATIO_DISCOUNT'], $arPrice['CURRENCY']);
				}
			} else
			{
				if ($arPrice['BASE_PRICE'] < $arOldPrice['VALUE'])
				{
					$arPrice['BASE_PRICE'] = $arOldPrice['VALUE'];
					$arPrice['DISCOUNT'] = $arPrice['BASE_PRICE'] - $arPrice['PRICE'];
					$arPrice['PERCENT'] = round($arPrice['DISCOUNT'] / $arPrice['BASE_PRICE'] * 100, 0);
					$arPrice['RATIO_BASE_PRICE'] = $arPrice['BASE_PRICE'];
					$arPrice['RATIO_DISCOUNT'] = $arPrice['DISCOUNT'];
					$arPrice['PRINT_BASE_PRICE'] = FormatCurrency($arPrice['BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_BASE_PRICE'] = FormatCurrency($arPrice['RATIO_BASE_PRICE'], $arPrice['CURRENCY']);
					$arPrice['PRINT_DISCOUNT'] = FormatCurrency($arPrice['DISCOUNT'], $arPrice['CURRENCY']);
					$arPrice['PRINT_RATIO_DISCOUNT'] = FormatCurrency($arPrice['RATIO_DISCOUNT'], $arPrice['CURRENCY']);
				}
			}
		}
		return $arPrice;
	}

}
