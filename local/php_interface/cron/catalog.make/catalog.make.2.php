<?

namespace Webavk\Odri\Cron;

class CatalogMake
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arLinks = array();
	protected $arExistsElements = array();
	protected $arExistsProductOffers = array();
	protected $arStorePropVariants = array();
	protected $arPreventLoadItems = array();
	protected $arAllPhotos = array();
	protected $arAllPhotosFinded = array();
	protected $arSynonimSubSection = array();
	protected $arColorNames = array();
	protected $arCollectionTypesByCollection = array();
	protected $arCollections = array();
	protected $iOnly1CElement = false;
	protected $arActiveProductsSet = array();

	function __construct($arOptions)
	{
		$this->arOptions = $arOptions;
	}

	function Start()
	{
		$this->doCheckPreviewPhotosExists();
		$this->doLoadColorTable();
		$this->doLoadCollectionTypesByCollection();
		$this->doLoadAllPhotos();
		$this->doLoadReCatalogStructure();
		$this->doLoadExistsElements();
		$this->doLoadPreventLoadElements();
		$this->doMakeElements();
		$this->doCheckElementsSort();
		$this->doCheckDescriptions();
		$this->doCheckUnifySections();
		$this->doCheckSectionOldSize();
		$this->doCheckSectionSale();
		$this->doCheckDiscountPercent();
		$this->doCheckSectionsActive();
		$this->doUpdateNewCollection();
		$this->doLoadColorFiles();
		$this->doUpdateNameGenerated();
		$this->doLogPhotos();
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doCheckPreviewPhotosExists()
	{
		$obElement = new \CIBlockElement();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "PREVIEW_PICTURE"));
		while ($arElement = $rElements->Fetch())
		{
			if ($arElement['PREVIEW_PICTURE'] > 0)
			{
				$arFile = \CFile::MakeFileArray($arElement['PREVIEW_PICTURE']);
				if (empty($arFile))
				{
					$obElement->Update($arElement['ID'], array("PREVIEW_PICTURE" => \CFile::MakeFileArray("/local/templates/" . SITE_TEMPLATE_ID . "/img/noimg.jpg")));
				}
			}
		}
		unset($obElement);
	}

	function doLoadColorTable()
	{
		$obElement = new \CIBlockElement();
		$strName = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/catalog.make.colors.csv";
		file_put_contents($strName, iconv("windows-1251", 'utf-8', file_get_contents(dirname(__FILE__) . "/files/colors.csv")));
		$f = @fopen($strName, "r+t");
		if ($f)
		{
			fgetcsv($f, 2048, ";", '"');
			while ($ar = fgetcsv($f, 2048, ";", '"'))
			{
				if (strlen(trim($ar[0])) <= 0)
				{
					continue;
				}
				$arFltColor = explode(",", $ar[2]);
				foreach ($arFltColor as $k => $v)
				{
					$v = trim($v);
					$arFltColor[$k] = $v;
					if (strlen($arFltColor[$k]) <= 0)
					{
						unset($arFltColor[$k]);
					}
				}
				$this->arColorNames[trim(strtolower($ar[0]))] = array(
					"NAME" => $ar[0],
					"NAME_KT" => $ar[1],
					"NAME_FILTER" => $arFltColor
				);
			}
			fclose($f);
		}
		foreach ($this->arColorNames as $k => $v)
		{
			$iPropColor = $this->doFindReferenceValue($v['NAME'], IBLOCK_REFERENCE_COLOR);
			if ($iPropColor > 0 && strlen($v['NAME_KT']) > 0)
			{
				$obElement->SetPropertyValueCode($iPropColor, "NAME_IN_KT", $v['NAME_KT']);
			}
			foreach ($v['NAME_FILTER'] as $k1 => $v1)
			{
				if (strlen($v1) > 0)
				{
					$iProp = $this->doFindReferenceValue($v1, IBLOCK_REFERENCE_COLOR_GROUP);
					$this->arColorNames[$k]['NAME_FILTER'][$k1] = $iProp;
				}
			}
		}
		//Нормализуем цвета
		$arNew = array();
		foreach ($this->arColorNames as $k => $v)
		{
			$k = $this->doNormalizeColorForPhoto($k);
			$arNew[$k] = $v;
		}
		$this->arColorNames = $arNew;
	}

	function doLoadColorFiles()
	{
		$obElement = new \CIBlockElement();
		//Загружаем список цветов
		$arAllColors = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_REFERENCE_COLOR), false, false, array("ID", "NAME", "PREVIEW_PICTURE"));
		while ($arElement = $rElements->Fetch())
		{
			$strColorIdent = $this->doNormalizeColorForPhoto($arElement['NAME']);
			$arAllColors[$strColorIdent] = $arElement;
		}

		$arDirs = scandir($this->arOptions['COLOR_DIR']);
		foreach ($arDirs as $strFile)
		{
			if (in_array($strFile, array(".", "..")))
			{
				continue;
			}
			$strFullName = $this->arOptions['COLOR_DIR'] . $strFile;
			$arFile = pathinfo($strFullName);
			$strColorIdent = $this->doNormalizeColorForPhoto($arFile['filename']);
			if (!isset($arAllColors[$strColorIdent]))
			{
				echo "not find color: " . $arFile['filename'] . "\n";
			} else
			{
				if ($arAllColors[$strColorIdent]['PREVIEW_PICTURE'] > 0)
				{
					if (!$this->ComparePhotos($_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($arAllColors[$strColorIdent]['PREVIEW_PICTURE']), $strFullName))
					{
						$arFile = \CFile::MakeFileArray($strFullName);
						$arFile['PREVIEW_PICTURE']['old_file'] = $arAllColors[$strColorIdent]['PREVIEW_PICTURE'];
						$obElement->Update($arAllColors[$strColorIdent]['ID'], array(
							"PREVIEW_PICTURE" => $arFile
						));
					}
				} else
				{
					$obElement->Update($arAllColors[$strColorIdent]['ID'], array(
						"PREVIEW_PICTURE" => \CFile::MakeFileArray($strFullName)
					));
				}
			}
		}
	}

	function doLoadCollectionTypesByCollection()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_REFERENCE_COLECTION), false, false, array("ID", "NAME", "PROPERTY_COLLECTION_TYPE"));
		while ($arElement = $rElements->Fetch())
		{
			if ($arElement['PROPERTY_COLLECTION_TYPE_VALUE'] > 0)
			{
				$this->arCollectionTypesByCollection[$arElement['ID']] = $arElement['PROPERTY_COLLECTION_TYPE_VALUE'];
			}
			$this->arCollections[$arElement["ID"]] = $arElement['NAME'];
		}
	}

	function doLoadAllPhotos()
	{
		$arDirs = scandir($this->arOptions['PHOTOS_DIR']);
		foreach ($arDirs as $strDir)
		{
			if (in_array($strDir, array(".", "..")))
			{
				continue;
			}
			$strFullName = $this->arOptions['PHOTOS_DIR'] . $strDir . "/";
			$strArticle = $this->doNormalizeArticleForPhotos($strDir);
			$arColors = scandir($strFullName);
			foreach ($arColors as $strColor)
			{
				if (in_array($strColor, array(".", "..")))
				{
					continue;
				}
				$strColorFullName = $strFullName . $strColor . "/";
				$strColor = $this->doNormalizeColorForPhoto($strColor);
				$arFiles = scandir($strColorFullName);
				foreach ($arFiles as $strFile)
				{
					if (in_array($strFile, array(".", "..")))
					{
						continue;
					}
					$this->arAllPhotos[$strArticle][$strColor][] = $strColorFullName . $strFile;
				}
			}
		}
	}

	function doNormalizeArticleForPhotos($strArticle)
	{
		$strArticle = trim(strtolower($strArticle));
		$strArticle = str_replace(".", "", $strArticle);
		$strArticle = str_replace(",", "", $strArticle);
		$strArticle = str_replace(" ", "", $strArticle);
		$strArticle = str_replace("-", "", $strArticle);
		$strArticle = str_replace("_", "", $strArticle);
		$strArticle = str_replace("/", "", $strArticle);
		return trim($strArticle);
	}

	function doNormalizeColorForPhoto($strColor)
	{
		$strColor = trim(strtolower($strColor));
		$strColor = str_replace(".", " ", $strColor);
		$strColor = str_replace(",", " ", $strColor);
		$strColor = str_replace("/", " ", $strColor);
		$strColor = str_replace("_", " ", $strColor);
		$strColor = str_replace("-", " ", $strColor);
		$strColor = str_replace("(", " ", $strColor);
		$strColor = str_replace(")", " ", $strColor);
		$strColor = str_replace("  ", " ", $strColor);
		$strColor = str_replace("\xc2\xa0", '', $strColor);
		$strColor = str_replace(" ", "", $strColor);
		return trim($strColor);
	}

	function doCheckElementsSort()
	{
		$obElement = new \CIBlockElement();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "SORT", "ACTIVE", "CATALOG_QUANTITY"));
		while ($arElement = $rElements->Fetch())
		{
			$newSort = 500;
			if ($arElement['CATALOG_AVAILABLE'] != "Y")
			{
				$newSort = 100000;
			}
			if ($arElement['SORT'] != $newSort)
			{
				$obElement->Update($arElement['ID'], array("SORT" => $newSort));
			}
		}
		unset($obElement);
	}

	function doCheckSectionsActive()
	{
		$obSection = new \CIBlockSection();
		$obElements = new \CIBlockElement();
		$rSections = \CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), array("IBLOCK_ID" => IBLOCK_CATALOG), false);
		while ($arSection = $rSections->Fetch())
		{
			if ($arSection['ID'] == DEFAULT_CATEGORY_ID)
			{
				continue;
			}
			$arElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y", "CATALOG_AVAILABLE" => "Y"), array("IBLOCK_ID"))->Fetch();
			if ($arElements['CNT'] > 0)
			{
				if ($arSection['ACTIVE'] != "Y")
				{
					$obSection->Update($arSection['ID'], array("ACTIVE" => "Y"));
				}
				//Активируем товары в разделе
				$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "N", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
				while ($arElement = $rElements->Fetch())
				{
					if (in_array($arElement['ID'], $this->arActiveProductsSet))
					{
						$obElements->Update($arElement['ID'], array("ACTIVE" => "Y"));
					}
				}
			} else
			{
				if ($arSection['ACTIVE'] == "Y")
				{
					$obSection->Update($arSection['ID'], array("ACTIVE" => "N"));
				}
				//Деактивируем товары в разделе
				$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
				while ($arElement = $rElements->Fetch())
				{
					$obElements->Update($arElement['ID'], array("ACTIVE" => "N"));
				}
			}
		}
	}

	/**
	 * Загружаем структуру каталогов
	 */
	function doLoadReCatalogStructure()
	{
		/* $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HL_1CLINKS_ID)->fetch();
		  $oHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
		  $oHLBlockClass = $oHLBlock->getDataClass();
		  $rDataCurrent = $oHLBlockClass::getList();
		  while ($arDataCurrent = $rDataCurrent->fetch())
		  {
		  if (is_array($arDataCurrent['UF_1C_ELEMENTS']))
		  {
		  foreach ($arDataCurrent['UF_1C_ELEMENTS'] as $val)
		  {
		  $this->arLinks[$val] = $arDataCurrent['UF_CATALOG_SECTION'];
		  }
		  }
		  }
		 * 
		 */
		//Остальные товары запихиваем в одну категорию-заглушку
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			if (!isset($this->arLinks[$arElement['ID']]))
			{
				$this->arLinks[$arElement['ID']] = DEFAULT_CATEGORY_ID;
			}
		}
	}

	/**
	 * Загружаем список ID элементов каталога
	 */
	function doLoadExistsElements()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			$this->arExistsElements['ELEMENTS'][$arElement['ID']] = $arElement['ID'];
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			$this->arExistsElements['OFFERS'][$arElement['ID']] = $arElement['ID'];
		}
	}

	/**
	 * Определяем перечень товаров, для которых обязательна принудительная загрузка
	 */
	function doLoadPreventLoadElements()
	{
		$rSections = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C, "UF_MAKE_CATALOG" => 1));
		while ($arSection = $rSections->Fetch())
		{
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C, "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				$this->arPreventLoadItems[] = $arElement['ID'];
			}
		}
	}

	/**
	 * Проверка элементов каталога
	 */
	function doMakeElements()
	{
		foreach ($this->arLinks as $iElement1CId => $iCategoryId)
		{
			if ($this->iOnly1CElement > 0)
			{
				if ($iElement1CId != $this->iOnly1CElement)
				{
					continue;
				}
			}
			/* if ($iElement1CId != 2237)
			  {
			  continue;
			  } */
			$productId = false;
			//$productId = $this->findCatalogProductIdBy1CProductId($iElement1CId);
			$this->doMakeElement($iElement1CId, $productId, $iCategoryId);
		}
	}

	/**
	 * Проверка элемента каталога
	 * @param int $iElementId1C
	 * @param int $iElementIdCatalog
	 * @param int $iCategoryId
	 */
	function doMakeElement($iElementId1C, $iElementIdCatalog, $iCategoryId)
	{
		//Загружаем торговые предложения и определяем уникальные цвета
		$arUniqColor = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C_OFFERS, "PROPERTY_CML2_LINK" => $iElementId1C), false, false, array("ID", "PROPERTY__1_TSVET"));
		while ($arOffer = $rOffers->Fetch())
		{
			$color = trim($arOffer['PROPERTY__1_TSVET_VALUE']);
			if (strlen($color) > 0 && strpos(strtolower($color), "образец") === false)
			{
				$arUniqColor[$color] = $color;
			}
		}
		foreach ($arUniqColor as $color)
		{
			$this->doMakeElementByColor($iElementId1C, $iElementIdCatalog, $iCategoryId, $color);
		}
	}

	/**
	 * Проверка элемента каталога с разбивкой по цвету
	 * @param int $iElementId1C
	 * @param int $iElementIdCatalog
	 * @param int $iCategoryId
	 */
	function doMakeElementByColor($iElementId1C, $iElementIdCatalog, $iCategoryId, $strColor)
	{
		$obElement = new \CIBlockElement();
		$ar1CElement = $this->getElementData($iElementId1C);
		$iColorReference = $this->doFindReferenceValue($strColor, IBLOCK_REFERENCE_COLOR);
		if ($iElementIdCatalog === false)
		{
			$iElementIdCatalog = $this->findCatalogProductIdBy1CProductId($iElementId1C, $iColorReference);
		}
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$strColorName = "";
		$strColorCode = "";
		$arColorGroup = false;
		$strColorKTName = "";
		if ($iColorReference > 0)
		{
			$arColor = \CIBlockElement::GetList(array(), array("ID" => $iColorReference), false, false, array("ID", "NAME", "CODE", "PROPERTY_IDENT"))->Fetch();
			$strColorName = $arColor['NAME'];
			$strColorCode = $arColor['PROPERTY_IDENT_VALUE'];
			$strFindColor = $this->doNormalizeColorForPhoto($strColor);
			if (isset($this->arColorNames[$strFindColor]))
			{
				$arColorGroupFull = $this->arColorNames[$strFindColor];
				$arColorGroup = $arColorGroupFull['NAME_FILTER'];
				$strColorKTName = trim($arColorGroupFull['NAME_KT']);
			}
		}
		if (strlen($strColorName) <= 0)
		{
			$strColorName = $strColor;
		}
		if (strlen($strColorCode) <= 0)
		{
			$strColorCode = $strColor;
		}
		$strColorCode = $this->doNormalizeColorForPhoto($strColorCode);
		$ar1CElement['NAME'] .= " " . $strColorName;
		$ar1CElement['NAME'] = trim($ar1CElement['NAME']);

		if ($iCategoryId == DEFAULT_CATEGORY_ID)
		{
			$iCategoryId = $this->doGetDefaultCategoryByProperties($ar1CElement['NAME'], $ar1CElement['PROPERTIES']['BREND']['VALUE'], $ar1CElement['PROPERTIES']['POL']['VALUE'], $ar1CElement['PROPERTIES']['GRUPPA_TOVARA']['VALUE']);
		}
		$iCollectionType = $this->doGetCollectionTypeByProperties($ar1CElement['NAME'], $ar1CElement['PROPERTIES']['BREND']['VALUE'], $ar1CElement['PROPERTIES']['POL']['VALUE'], $ar1CElement['PROPERTIES']['GRUPPA_TOVARA']['VALUE']);


		$arFields = array(
			"IBLOCK_ID" => IBLOCK_CATALOG,
			"NAME" => $ar1CElement['NAME'],
			"CODE" => $this->doTranslitName($ar1CElement['NAME']),
			"IBLOCK_SECTION_ID" => $iCategoryId,
			"ACTIVE" => "Y",
			"XML_ID" => $ar1CElement['XML_ID'],
			"PROPERTY_VALUES" => array(
				"CML2_ARTICLE" => $ar1CElement['PROPERTIES']['CML2_ARTICLE']['~VALUE'],
				"SYS_LINK_1C" => $ar1CElement['ID'],
				"INFO_NAME_1C" => $ar1CElement['NAME'],
				"MANUAL_UPDATE_URL_NAME" => "Y",
				"COLOR" => $iColorReference,
				"COLOR_FILTER" => $arColorGroup,
				"COLOR_KT_NAME" => $strColorKTName
			//"INFO_NAME_PREFIX" => $this->doParseNamePrefix($ar1CElement['NAME'])
			)
		);
		if ($arCatalogElement)
		{
			if (strlen($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']) > 0)
			{
				$arFields['NAME'] = trim($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']);
				$arFields['CODE'] = $this->doTranslitName($arFields['NAME']);
			}
		}
		if (!$arCatalogElement)
		{
			//Проверяем, есть ли цена у товара и остаток. если нет - не делаем ничего
			$isExistsPrice = false;
			$isExistsQuantity = false;

			$arPriceOffer1C = $this->GetProductPrice($ar1CElement['ID']);
			if ($arPriceOffer1C['PRICE'] > 0)
			{
				$isExistsPrice = true;
			}
			$arProduct = \CCatalogProduct::GetByID($ar1CElement['ID']);
			if ($arProduct['QUANTITY'] > 0)
			{
				$isExistsQuantity = true;
			}
			if (!$isExistsPrice || !$isExistsQuantity)
			{
				$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C_OFFERS, "PROPERTY_CML2_LINK" => $ar1CElement['ID']), false, false, array("ID"));
				while ($arOffer = $rOffers->Fetch())
				{
					if (!$isExistsPrice)
					{
						$arPriceOffer1C = $this->GetProductPrice($arOffer['ID']);
						if ($arPriceOffer1C['PRICE'] > 0)
						{
							$isExistsPrice = true;
						}
					}
					if (!$isExistsQuantity)
					{
						$arProduct = \CCatalogProduct::GetByID($arOffer['ID']);
						if ($arProduct['QUANTITY'] > 0)
						{
							$isExistsQuantity = true;
						}
					}
					if ($isExistsPrice && $isExistsQuantity)
					{
						break;
					}
				}
			}

			if (!$isExistsPrice/* || !$isExistsQuantity */)
			{
				return;
			}

			$arPhotosInDir = $this->doGetElementPhotos($arFields['PROPERTY_VALUES']['CML2_ARTICLE'], $strColorCode);
			if (/* $this->IsExistsProductPrices($iElementId1C) */!empty($arPhotosInDir) || in_array($iElementId1C, $this->arPreventLoadItems))
			{
				$iElementIdCatalog = $obElement->Add($arFields);
				if ($iElementIdCatalog > 0)
				{
					$arCatalogElement = $this->getElementData($iElementIdCatalog);
				} else
				{
					echo $obElement->LAST_ERROR . "\n";
				}
			}
		}
		if ($arCatalogElement)
		{
			$strName = $ar1CElement['NAME'];
			if (strlen($arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE']) > 0)
			{
				$strName = $arCatalogElement['PROPERTIES']['MANUAL_NAME_SITE']['VALUE'];
			}

			if (strlen($strName) > 0)
			{
				$arUpdate = array(
					"NAME" => $strName
				);
				if ($arCatalogElement['PROPERTIES']['MANUAL_UPDATE_URL_NAME']['VALUE'] == "Y")
				{
					if (strpos($arUpdate['NAME'], $arFields['PROPERTY_VALUES']['CML2_ARTICLE']) === false)
					{
						$arUpdate['CODE'] = $this->doTranslitName($arUpdate['NAME'] . " " . $arFields['PROPERTY_VALUES']['CML2_ARTICLE']);
					} else
					{
						$arUpdate['CODE'] = $this->doTranslitName($arUpdate['NAME']);
					}
				}
				$obElement->Update($arCatalogElement['ID'], $arUpdate);
			}
			$this->doCheckElementPhotos($arCatalogElement['ID'], $strColorCode);
			//Проверка фото из 1с
			$arPhotos1C = array();
			if (strlen($ar1CElement['PREVIEW_PICTURE']) > 0)
			{
				$arPhotos1C[] = $ar1CElement['PREVIEW_PICTURE'];
			}
			if (strlen($ar1CElement['DETAIL_PICTURE']) > 0)
			{
				$arPhotos1C[] = $ar1CElement['DETAIL_PICTURE'];
			}
			foreach ($ar1CElement['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo)
			{
				$arPhotos1C[] = $photo;
			}
			$this->doCheckElementPhotosFrom1C($arCatalogElement['ID'], $arPhotos1C);
			//Проверка свойств
			$this->doCheckProperty($arCatalogElement['ID'], "INFO_NAME_1C", $ar1CElement['NAME'], $arCatalogElement['PROPERTIES']['INFO_NAME_1C']);
			$this->doCheckProperty($arCatalogElement['ID'], "FUR", $ar1CElement['PROPERTIES']['MEKH'], $arCatalogElement['PROPERTIES']['FUR']);
			$this->doCheckProperty($arCatalogElement['ID'], "COLLECTION", $ar1CElement['PROPERTIES']['GOD_SEZON'], $arCatalogElement['PROPERTIES']['COLLECTION']);
			$this->doCheckProperty($arCatalogElement['ID'], "COMPOSITION", $ar1CElement['PROPERTIES']['NAPOLNENIE_SOSTAV'], $arCatalogElement['PROPERTIES']['COMPOSITION']);
			$this->doCheckProperty($arCatalogElement['ID'], "BASE_TOP", $ar1CElement['PROPERTIES']['OSNOVNAYAVERKHA1_SOSTAV'], $arCatalogElement['PROPERTIES']['BASE_TOP']);
			$this->doCheckProperty($arCatalogElement['ID'], "LINING", $ar1CElement['PROPERTIES']['PODKLAD_SOSTAV'], $arCatalogElement['PROPERTIES']['LINING']);
			$this->doCheckProperty($arCatalogElement['ID'], "BRAND", $ar1CElement['PROPERTIES']['BREND'], $arCatalogElement['PROPERTIES']['BRAND']);
			$this->doCheckProperty($arCatalogElement['ID'], "YEAR", $ar1CElement['PROPERTIES']['GOD'], $arCatalogElement['PROPERTIES']['YEAR']);
			$this->doCheckProperty($arCatalogElement['ID'], "SEASON", $ar1CElement['PROPERTIES']['SEZON'], $arCatalogElement['PROPERTIES']['SEASON']);
			$this->doCheckProperty($arCatalogElement['ID'], "GROUP", $ar1CElement['PROPERTIES']['GRUPPA_TOVARA'], $arCatalogElement['PROPERTIES']['GROUP']);
			$this->doCheckProperty($arCatalogElement['ID'], "GENDER", $ar1CElement['PROPERTIES']['POL'], $arCatalogElement['PROPERTIES']['GENDER']);
			$this->doCheckProperty($arCatalogElement['ID'], "MANUFACTURER", $ar1CElement['PROPERTIES']['PROIZVODITEL'], $arCatalogElement['PROPERTIES']['MANUFACTURER']);
			$this->doCheckProperty($arCatalogElement['ID'], "COUNTRY_DESIGN", $ar1CElement['PROPERTIES']['STRANA_DIZAYNA'], $arCatalogElement['PROPERTIES']['COUNTRY_DESIGN']);
			$this->doCheckProperty($arCatalogElement['ID'], "COLOR_KT_NAME", $arFields['PROPERTY_VALUES']['COLOR_KT_NAME'], $arCatalogElement['PROPERTIES']['COLOR_KT_NAME']);
			$obElement->SetPropertyValueCode($arCatalogElement['ID'], "COLOR_FILTER", $arFields['PROPERTY_VALUES']['COLOR_FILTER']);

			$arCatalogElement = $this->getElementData($arCatalogElement['ID']);

			if ($arCatalogElement['PROPERTIES']['INFO_COLLECTION_NAME']['VALUE'] != $this->arCollections[$arCatalogElement['PROPERTIES']['COLLECTION']['VALUE']])
			{
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "INFO_COLLECTION_NAME", $this->arCollections[$arCatalogElement['PROPERTIES']['COLLECTION']['VALUE']]);
			}
			$arCollectionType = array();
			if ($iCollectionType > 0)
			{
				$arCollectionType[] = $iCollectionType;
			}
			if ($this->arCollectionTypesByCollection[$arCatalogElement['PROPERTIES']['COLLECTION']['VALUE']] > 0)
			{
				$arCollectionType[] = $this->arCollectionTypesByCollection[$arCatalogElement['PROPERTIES']['COLLECTION']['VALUE']];
			}
			$obElement->SetPropertyValueCode($arCatalogElement['ID'], "COLLECTION_TYPE", $arCollectionType);

			$this->doMakeOffers($ar1CElement['ID'], $arCatalogElement['ID'], $strColor);

			//Грузим список доступных размеров и обновляем
			$arAvailable = array();
			$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $arCatalogElement['ID'], "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y"), false, false, array("ID", "PROPERTY_SIZE"));
			while ($arOffer = $rOffers->Fetch())
			{
				$arAvailable[] = $arOffer['PROPERTY_SIZE_VALUE'];
			}
			$obElement->SetPropertyValueCode($arCatalogElement['ID'], "SYS_SIZE_AVAILABLE", $arAvailable);
		}
		unset($obElement);
	}

	function IsExistsProductPrices($iElementId1C)
	{
		$arPriceOffer1C = $this->GetProductPrice($iElementId1C);
		if (!empty($arPriceOffer1C))
		{
			return true;
		}
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C_OFFERS, "PROPERTY_CML2_LINK" => $iElementId1C), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch())
		{
			$arPriceOffer1C = $this->GetProductPrice($arOffer['ID']);
			if (!empty($arPriceOffer1C))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Проверка свойства
	 * @param int $iElementIdCatalog
	 * @param string $strPropertyCode
	 * @param array $arProperty1C
	 * @param array $arPropertyCatalog
	 */
	function doCheckProperty($iElementIdCatalog, $strPropertyCode, $arProperty1C, $arPropertyCatalog)
	{
		$newValue = false;
		if ($arPropertyCatalog['PROPERTY_TYPE'] == "E")
		{
			//Ищем или создаем запись в справочнике
			if ((is_array($arProperty1C['VALUE']) && !empty($arProperty1C['VALUE'])) || (!is_array($arProperty1C['VALUE']) && strlen($arProperty1C['VALUE']) > 0))
			{
				$newValue = $this->doFindReferenceValue(is_array($arProperty1C) ? $arProperty1C['VALUE'] : $arProperty1C, $arPropertyCatalog['LINK_IBLOCK_ID']);
			}
		} elseif ($arPropertyCatalog['PROPERTY_TYPE'] == "S")
		{
			if (is_array($arProperty1C) && isset($arProperty1C['VALUE']))
			{
				$newValue = $arProperty1C['VALUE'];
			} else
			{
				$newValue = $arProperty1C;
			}
		} else
		{
			$newValue = $arProperty1C['VALUE'];
		}
		$newValue = trim($newValue);
		$isMath = true;
		if (!is_array($arPropertyCatalog['VALUE']))
		{
			if ($newValue != $arPropertyCatalog['VALUE'])
			{
				$isMath = false;
			}
		} else
		{
			if (in_array($newValue, $arPropertyCatalog['VALUE']))
			{
				unset($arPropertyCatalog['VALUE'][array_search($newValue, $arPropertyCatalog['VALUE'])]);
				if (!empty($arPropertyCatalog['VALUE']))
				{
					$isMath = false;
				}
			}
		}
		if (!$isMath)
		{
			$obElement = new \CIBlockElement();
			$obElement->SetPropertyValueCode($iElementIdCatalog, $strPropertyCode, $newValue);
			unset($obElement);
		}
	}

	/**
	 * Поиск и создание записи справочника
	 * @param string $strValue
	 * @param int $IBLOCK_ID
	 * @return int
	 */
	function doFindReferenceValue($strValue, $IBLOCK_ID)
	{
		$iResult = false;
		$strValue = trim($strValue);
		$obElement = new \CIBlockElement();
		$rElement = \CIBlockElement::GetList(array(), array("PROPERTY_SYNONIM" => $strValue, "IBLOCK_ID" => $IBLOCK_ID), false, false, array("ID"));
		if ($arElement = $rElement->Fetch())
		{
			$iResult = $arElement['ID'];
		} else
		{
			$iResult = $obElement->Add(array(
				"IBLOCK_ID" => $IBLOCK_ID,
				"NAME" => $strValue,
				"CODE" => $this->doTranslitName($strValue),
				"PROPERTY_VALUES" => array(
					"SYNONIM" => array($strValue)
				)
			));
		}
		unset($obElement);
		return $iResult;
	}

	/**
	 * Проверка фотографий товара
	 * @param int $iElementIdCatalog
	 */
	function doCheckElementPhotos($iElementIdCatalog, $strColorCode)
	{
		$obElement = new \CIBlockElement();
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arPhotosInDir = $this->doGetElementPhotos($arCatalogElement['PROPERTIES']['CML2_ARTICLE']['~VALUE'], $strColorCode);

		//Включаем/выключаем товар в зависимости от наличия фото
		if ($arCatalogElement['ACTIVE'] == "Y")
		{
			if (empty($arPhotosInDir))
			{
				$obElement->Update($arCatalogElement['ID'], array("ACTIVE" => "N"));
			} else
			{
				$this->arActiveProductsSet[] = $arCatalogElement['ID'];
			}
		} else
		{
			if (!empty($arPhotosInDir))
			{
				$obElement->Update($arCatalogElement['ID'], array("ACTIVE" => "Y"));
				$this->arActiveProductsSet[] = $arCatalogElement['ID'];
			}
		}

		//Проверяем и обновляем главное фото
		$strMainPhotoNew = false;
		$strMainPhotoOld = false;
		$iMainPhotoOldId = false;
		if (isset($arPhotosInDir[0]))
		{
			$strMainPhotoNew = $arPhotosInDir[0];
		}
		if ($arCatalogElement['PREVIEW_PICTURE'] > 0)
		{
			$iMainPhotoOldId = $arCatalogElement['PREVIEW_PICTURE'];
			$strMainPhotoOld = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iMainPhotoOldId);
		}
		$arFields = array();
		if ($strMainPhotoNew === false && $strMainPhotoOld !== false)
		{
			//Удаляем фото анонса
			$arFields = array(
				"PREVIEW_PICTURE" => array(
					"del" => "Y",
					"old_file" => $iMainPhotoOldId
				)
			);
		} elseif ($strMainPhotoNew !== false && $strMainPhotoOld === false)
		{
			//Устанавливаем фото
			$arFields = array(
				"PREVIEW_PICTURE" => \CFile::MakeFileArray($strMainPhotoNew)
			);
		} elseif ($strMainPhotoNew !== false && $strMainPhotoOld !== false)
		{
			if (!$this->ComparePhotos($strMainPhotoNew, $strMainPhotoOld))
			{
				$arFields = array(
					"PREVIEW_PICTURE" => \CFile::MakeFileArray($strMainPhotoNew)
				);
				$arFields['PREVIEW_PICTURE']['old_file'] = $iMainPhotoOldId;
			}
		}
		if (!empty($arFields))
		{
			if (!$obElement->Update($arCatalogElement['ID'], $arFields))
			{
				echo $obElement->LAST_ERROR . "\n";
			}
		}

		//Проверка и обновление дополнительных фото
		//Сначала формируем массив хэшей и проверяем
		$arHashOld = array();
		$arHashNew = array();
		foreach ($arPhotosInDir as $strFile)
		{
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashNew[$strHash] = $strHash;
		}
		if (!is_array($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE']))
		{
			$arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] = array($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE']);
		}
		foreach ($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $iPhotoOldId)
		{
			$strFile = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iPhotoOldId);
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashOld[$strHash] = $strHash;
		}
		//Сравниваем массивы
		foreach ($arHashNew as $k => $v)
		{
			if (isset($arHashOld[$k]))
			{
				unset($arHashOld[$k]);
				unset($arHashNew[$k]);
			}
		}
		if (!empty($arHashOld) || !empty($arHashNew))
		{
			//Обновляем дополнительные фото.
			//Сначала удалим старые значения
			$arProp = array();
			foreach ($arCatalogElement['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $strFile)
			{
				$arProp[$arCatalogElement['PROPERTIES']['MORE_PHOTOS']['PROPERTY_VALUE_ID'][$k]] = array(
					"del" => "Y",
					"old_file" => $strFile
				);
			}

			//И сформируем новые
			foreach ($arPhotosInDir as $k => $strFile)
			{
				$arProp['n' . $k] = array(
					"VALUE" => \CFile::MakeFileArray($strFile),
					"DESCRIPTION" => ""
				);
			}
			if (!empty($arProp))
			{
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "MORE_PHOTOS", $arProp);
			}
		}

		unset($obElement);
	}

	/**
	 * Сраваниваем фотографии по содержимому
	 * @param string $strFile1
	 * @param string $strFile2
	 */
	function ComparePhotos($strFile1, $strFile2)
	{
		$hash1 = filesize($strFile1) . "|" . md5_file($strFile1) . "|" . crc32(file_get_contents($strFile1));
		$hash2 = filesize($strFile2) . "|" . md5_file($strFile2) . "|" . crc32(file_get_contents($strFile2));
		return $hash1 == $hash2;
	}

	/**
	 * Получаем массив фотографий по артикулу
	 * @param string $strElementArticle
	 * @param string $strColorCode
	 */
	function doGetElementPhotos($strElementArticle, $strColorCode)
	{
		$strElementArticle = $this->doNormalizeArticleForPhotos($strElementArticle);
		$strColorCode = trim(strtolower($strColorCode));
		$arFiles = $this->arAllPhotos[$strElementArticle][$strColorCode];
		$arNormalizeFiles = array();
		foreach ($arFiles as $strFullName)
		{
			$arPathInfo = pathinfo($strFullName);
			if (in_array(strtolower($arPathInfo['extension']), array("jpg", "gif", "jpeg", "png")))
			{
				$arNormalizeFiles[$arPathInfo['filename']] = $strFullName;
			}
		}
		ksort($arNormalizeFiles, SORT_NUMERIC);
		$arNormalizeFiles = array_values($arNormalizeFiles);
		if (!empty($arNormalizeFiles))
		{
			$this->arAllPhotosFinded[$strElementArticle][] = $strColorCode;
		}
		return $arNormalizeFiles;
	}

	/**
	 * Проверяем торговые предложения
	 * @param int $iElementId1C
	 * @param int $iElementIdCatalog
	 */
	function doMakeOffers($iElementId1C, $iElementIdCatalog, $strColor)
	{
		$obElement = new \CIBlockElement();
		$arProduct = $this->getElementData($iElementIdCatalog);
		//Загружаем имеющиеся предложения
		$this->arExistsProductOffers = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch())
		{
			$this->arExistsProductOffers[$arOffer['ID']] = $arOffer['ID'];
		}
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_1C_OFFERS, "PROPERTY_CML2_LINK" => $iElementId1C), false, false, array("ID", "PROPERTY__1_TSVET"));
		while ($arOffer = $rOffers->Fetch())
		{
			$color = trim($arOffer['PROPERTY__1_TSVET_VALUE']);
			if (strlen($color) > 0 && strpos(strtolower($color), "образец") === false && $color == $strColor)
			{
				$offerId = $this->findCatalogOfferIdBy1COfferId($arOffer['ID'], $iElementIdCatalog);
				unset($this->arExistsProductOffers[$offerId]);
				$this->doMakeOffer($iElementId1C, $iElementIdCatalog, $offerId, $arOffer['ID'], $arProduct['NAME']);
			}
		}

		//Находим минимальную цену и устанавливаем
		$minPrice = false;
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog, "ACTIVE" => "Y"), false, false, array("ID", "CATALOG_GROUP_" . $this->arOptions['BASE_PRICE_ID']));
		while ($arOffer = $rOffers->Fetch())
		{
			if ($minPrice === false && $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']] > 0)
			{
				$minPrice = $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']];
			} else if ($minPrice !== false && $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']] > 0 && $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']] < $minPrice)
			{
				$minPrice = $arOffer['CATALOG_PRICE_' . $this->arOptions['BASE_PRICE_ID']];
			}
		}
		$oldPrice = false;
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog, "ACTIVE" => "Y"), false, false, array("ID", "CATALOG_GROUP_" . ODRISHOP_1C_OLD_PRICE_ID));
		while ($arOffer = $rOffers->Fetch())
		{
			if ($oldPrice === false && $arOffer['CATALOG_PRICE_' . ODRISHOP_1C_OLD_PRICE_ID] > 0)
			{
				$oldPrice = $arOffer['CATALOG_PRICE_' . ODRISHOP_1C_OLD_PRICE_ID];
			} else if ($oldPrice !== false && $arOffer['CATALOG_PRICE_' . ODRISHOP_1C_OLD_PRICE_ID] > 0 && $arOffer['CATALOG_PRICE_' . ODRISHOP_1C_OLD_PRICE_ID] < $oldPrice)
			{
				$oldPrice = $arOffer['CATALOG_PRICE_' . ODRISHOP_1C_OLD_PRICE_ID];
			}
		}
		if ($minPrice !== false)
		{
			/* $oldPrice = false;
			  if ($arProduct['PROPERTIES']['INFO_MANUAL_PRICE']['VALUE'] > 0 && $minPrice > $arProduct['PROPERTIES']['INFO_MANUAL_PRICE']['VALUE'])
			  {
			  $oldPrice = $minPrice;
			  $minPrice = $arProduct['PROPERTIES']['INFO_MANUAL_PRICE']['VALUE'];
			  }
			 * 
			 */
			$arPriceCatalog = $this->GetProductPrice($iElementIdCatalog);
			if ($arPriceCatalog['PRICE'] != $minPrice)
			{
				$this->SetProductPrice($iElementIdCatalog, $minPrice, "RUB");
			}
			$arOldPriceCatalog = $this->GetProductOldPrice($iElementIdCatalog);
			if ($arOldPriceCatalog['PRICE'] != $oldPrice)
			{
				$this->SetProductOldPrice($iElementIdCatalog, $oldPrice, "RUB");
			}
			/*
			  if (intval($strOldPrice) > 0 && intval($strOldPrice) <= $minPrice)
			  {
			  $strOldPrice = "";
			  }
			  //Проверка старой цены
			  $arOldPrice = \CPrice::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog, "CATALOG_GROUP_ID" => OLD_PRICE_ID))->Fetch();
			  if ($arOldPrice)
			  {
			  if ($arOldPrice['PRICE'] != floatval($strOldPrice))
			  {
			  \CPrice::Update($arOldPrice['ID'], array(
			  "PRICE" => $strOldPrice,
			  "CURRENCY" => "RUB"
			  ));
			  } elseif ($arOldPrice['PRICE'] == 0)
			  {
			  \CPrice::Delete($arOldPrice['ID']);
			  }
			  } elseif (strlen($strOldPrice) > 0)
			  {
			  \CPrice::Add(array(
			  "PRODUCT_ID" => $iElementIdCatalog,
			  "CATALOG_GROUP_ID" => OLD_PRICE_ID,
			  "PRICE" => $strOldPrice,
			  "CURRENCY" => "RUB"
			  ));
			  } */
		}

		//Проверка остатков по складам
		//Загружаем текущие данные
		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		while ($arStoreData = $rStoreData->Fetch())
		{
			$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		}

		//Смотрим что в 1с
		$arDataBySklad = array();
		$rOffers = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_CML2_LINK" => $iElementIdCatalog), false, false, array("ID"));
		while ($arOffer = $rOffers->Fetch())
		{
			$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arOffer['ID']));
			while ($arStoreData = $rStoreData->Fetch())
			{
				$arDataBySklad[$arStoreData['STORE_ID']] += $arStoreData['AMOUNT'];
			}
		}
		$totalQuantity = 0;
		foreach ($arDataBySklad as $storeId => $amount)
		{
			$totalQuantity += $amount;
			if (isset($arCurrentStoreData[$storeId]))
			{
				\CCatalogStoreProduct::Update($arCurrentStoreData[$storeId]['ID'], array(
					"AMOUNT" => $amount
				));
				unset($arCurrentStoreData[$storeId]);
			} else
			{
				\CCatalogStoreProduct::Add(array(
					"PRODUCT_ID" => $iElementIdCatalog,
					"STORE_ID" => $storeId,
					"AMOUNT" => $amount
				));
			}
		}

		foreach ($arCurrentStoreData as $arStoreData)
		{
			\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
				"AMOUNT" => 0
			));
		}

		$arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		if ($arCatalogProduct)
		{
			$arProductFields = array(
				"QUANTITY" => $totalQuantity,
				"QUANTITY_TRACE" => "D",
				"CAN_BUY_ZERO" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
			);
			\CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		} else
		{
			$arProductFields = array(
				"ID" => $iElementIdCatalog,
				"QUANTITY_TRACE" => "D",
				"PRICE_TYPE" => "S",
				"QUANTITY" => $totalQuantity,
				"QUANTITY_TRACE" => "D",
				"CAN_BUY_ZERO" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
			);
			\CCatalogProduct::Add($arProductFields);
		}

		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iElementIdCatalog));
		while ($arStoreData = $rStoreData->Fetch())
		{
			$arCurrentStoreData[] = $arStoreData['STORE_NAME'];
		}
		/*
		  $arValues = array();
		  foreach ($arCurrentStoreData as $strStoreName)
		  {
		  if (isset($this->arStorePropVariants[$strStoreName]))
		  {
		  $arValues[] = $this->arStorePropVariants[$strStoreName];
		  } else
		  {
		  $rPropertyVariant = \CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "CODE" => "INFO_IN_SKLAD", "VALUE" => $strStoreName));
		  if ($arPropertyVariant = $rPropertyVariant->Fetch())
		  {
		  $this->arStorePropVariants[$strStoreName] = $arPropertyVariant['ID'];
		  $arValues[] = $this->arStorePropVariants[$strStoreName];
		  } else
		  {
		  $obPropEnum = new \CIBlockPropertyEnum();
		  $this->arStorePropVariants[$strStoreName] = $obPropEnum->Add(array(
		  "PROPERTY_ID" => IBLOCK_CATALOG_PROPERTY_INFO_IN_SKLAD,
		  "VALUE" => $strStoreName,
		  "SORT" => 100
		  ));
		  $arValues[] = $this->arStorePropVariants[$strStoreName];
		  }
		  }
		  }
		  if (empty($arValues))
		  {
		  $arValues = false;
		  }
		  $obElement->SetPropertyValueCode($iElementIdCatalog, "INFO_IN_SKLAD", $arValues); */
		unset($obElement);
	}

	/**
	 * Проверка торгового предложения
	 * @param int $iElementIdCatalog
	 * @param int $iOfferId1C
	 */
	function doMakeOffer($iElementId1C, $iElementIdCatalog, $iOfferIdCatalog, $iOfferId1C, $strBaseName)
	{
		$obElement = new \CIBlockElement();
		$ar1COffer = $this->getElementData($iOfferId1C);
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		$arCatalogOffer = $this->getElementData($iOfferIdCatalog);

		$arFields = array(
			"IBLOCK_ID" => IBLOCK_CATALOG_OFFERS,
			"NAME" => $strBaseName,
			"CODE" => $this->doTranslitName($ar1COffer['NAME']),
			"ACTIVE" => "Y",
			"XML_ID" => $ar1COffer['XML_ID'],
			"PROPERTY_VALUES" => array(
				"SYS_LINK_1C" => $ar1COffer['ID'],
				"CML2_LINK" => $iElementIdCatalog
			)
		);
		$strOfferName = trim($ar1COffer['NAME']);
		$strSize = $ar1COffer['PROPERTIES']['_2_RAZMER']['VALUE'];
		$strSize = trim($strSize);
		if (strpos(strtolower($strSize), "образец") !== false)
		{
			return;
		}
		if (strlen($strSize) > 0)
		{
			$arFields['NAME'] .= " (" . $strSize . ")";
		}
		if (!$arCatalogOffer)
		{
			$iOfferIdCatalog = $obElement->Add($arFields);
			if ($iOfferIdCatalog > 0)
			{
				$arCatalogOffer = $this->getElementData($iOfferIdCatalog);
			} else
			{
				echo $obElement->LAST_ERROR . "\n";
			}
		}
		if ($arCatalogOffer)
		{
			if ($arCatalogOffer['NAME'] != $arFields['NAME'])
			{
				$obElement->Update($arCatalogOffer['ID'], array("NAME" => $arFields['NAME']));
			}
			if (strlen($strSize) > 0)
			{
				$arFields['PROPERTY_VALUE']['SIZE'] = $this->doFindReferenceValue($strSize, $arCatalogOffer['PROPERTIES']['SIZE']['LINK_IBLOCK_ID']);
				if ($arFields['PROPERTY_VALUE']['SIZE'] != $arCatalogOffer['PROPERTIES']['SIZE']['VALUE'])
				{
					$obElement->SetPropertyValueCode($arCatalogOffer['ID'], "SIZE", $arFields['PROPERTY_VALUE']['SIZE']);
				}
			}
			//myPrint($ar1COffer['PROPERTIES']);
			$this->doCheckProperty($arCatalogOffer['ID'], "CML2_BARCODE", $ar1COffer['PROPERTIES']['CML2_BAR_CODE']['VALUE'], $arCatalogOffer['PROPERTIES']['CML2_BARCODE']);
		}

		//Проверка остатков
		$arCatalogProduct = \CCatalogProduct::GetByID($iOfferIdCatalog);
		$ar1CProduct = \CCatalogProduct::GetByID($iOfferId1C);
		if ($arCatalogProduct)
		{
			$arProductFields = array(
				"WEIGHT" => $ar1CProduct['WEIGHT'],
				"MEASURE" => $ar1CProduct['MEASURE'],
				"VAT_ID" => $ar1CProduct['VAT_ID'],
				"VAT_INCLUDED" => $ar1CProduct['VAT_INCLUDED'],
				"QUANTITY" => $ar1CProduct['QUANTITY'],
				"QUANTITY_TRACE" => "D",
				"CAN_BUY_ZERO" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
					//"PURCHASING_PRICE" => $ar1CProduct[''],
					//"PURCHASING_CURRENCY" => $ar1CProduct[''],
			);
			\CCatalogProduct::Update($iOfferIdCatalog, $arProductFields);
		} else
		{
			$arProductFields = array(
				"ID" => $iOfferIdCatalog,
				"QUANTITY_TRACE" => "D",
				"CAN_BUY_ZERO" => "D",
				"NEGATIVE_AMOUNT_TRACE" => "D",
				"WEIGHT" => $ar1CProduct['WEIGHT'],
				"MEASURE" => $ar1CProduct['MEASURE'],
				"VAT_ID" => $ar1CProduct['VAT_ID'],
				"VAT_INCLUDED" => $ar1CProduct['VAT_INCLUDED'],
				"PRICE_TYPE" => "S",
				"QUANTITY" => $ar1CProduct['QUANTITY']
					//"PURCHASING_PRICE" => $ar1CProduct[''],
					//"PURCHASING_CURRENCY" => $ar1CProduct[''],
			);
			\CCatalogProduct::Add($arProductFields);
		}
		//Проверка цены
		//\CPrice::get
		$arPriceOffer1C = $this->GetProductPrice($iOfferId1C);
		if (empty($arPriceOffer1C))
		{
			$arPriceOffer1C = $this->GetProductPrice($iElementId1C);
		}

		$oldPrice = false;
		if ($arCatalogElement['PROPERTIES']['INFO_MANUAL_PRICE']['VALUE'] > 0 && $arPriceOffer1C['PRICE'] > $arCatalogElement['PROPERTIES']['INFO_MANUAL_PRICE']['VALUE'])
		{
			$oldPrice = $arPriceOffer1C['PRICE'];
			$arPriceOffer1C['PRICE'] = $arCatalogElement['PROPERTIES']['INFO_MANUAL_PRICE']['VALUE'];
		}
		$arPriceCatalog = $this->GetProductPrice($iOfferIdCatalog); //\CPrice::GetBasePrice($iOfferIdCatalog);
		if ($arPriceCatalog['PRICE'] != $arPriceOffer1C['PRICE'])
		{
			$this->SetProductPrice($iOfferIdCatalog, $arPriceOffer1C['PRICE'], $arPriceOffer1C['CURRENCY']);
		}
		$arOldPriceCatalog = $this->GetProductOldPrice($iOfferIdCatalog);
		if ($arOldPriceCatalog['PRICE'] != $oldPrice)
		{
			$this->SetProductOldPrice($iOfferIdCatalog, $oldPrice, "RUB");
		}

		/*
		  $arPriceCatalog = $this->GetProductPrice($iOfferIdCatalog); //\CPrice::GetBasePrice($iOfferIdCatalog);
		  if ($arPriceCatalog['PRICE'] != $arPriceOffer1C['PRICE'])
		  {
		  $this->SetProductPrice($iOfferIdCatalog, $arPriceOffer1C['PRICE'], $arPriceOffer1C['CURRENCY']);
		  } */

		//Проверка остатков по складам
		//Загружаем текущие данные
		$arCurrentStoreData = array();
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferIdCatalog));
		while ($arStoreData = $rStoreData->Fetch())
		{
			$arCurrentStoreData[$arStoreData['STORE_ID']] = $arStoreData;
		}
		//Смотрим что в 1с
		$totalQuantity = 0;
		$rStoreData = \CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $iOfferId1C));
		while ($arStoreData = $rStoreData->Fetch())
		{
			if (isset($arCurrentStoreData[$arStoreData['STORE_ID']]))
			{
				$totalQuantity += $arStoreData['AMOUNT'];
				\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
					"AMOUNT" => $arStoreData['AMOUNT']
				));
				unset($arCurrentStoreData[$arStoreData['STORE_ID']]);
			} else
			{
				$totalQuantity += $arStoreData['AMOUNT'];
				\CCatalogStoreProduct::Add(array(
					"PRODUCT_ID" => $iOfferIdCatalog,
					"STORE_ID" => $arStoreData['STORE_ID'],
					"AMOUNT" => $arStoreData['AMOUNT']
				));
			}
		}
		foreach ($arCurrentStoreData as $arStoreData)
		{
			\CCatalogStoreProduct::Update($arCurrentStoreData[$arStoreData['STORE_ID']]['ID'], array(
				"AMOUNT" => 0
			));
		}
		/*
		  //Проверка остатков
		  $arCatalogProduct = \CCatalogProduct::GetByID($iElementIdCatalog);
		  if ($arCatalogProduct)
		  {
		  $arProductFields = array(
		  "QUANTITY" => $totalQuantity,
		  "QUANTITY_TRACE" => "D",
		  "CAN_BUY_ZERO" => "D",
		  "NEGATIVE_AMOUNT_TRACE" => "D",
		  );
		  \CCatalogProduct::Update($iElementIdCatalog, $arProductFields);
		  } else
		  {
		  $arProductFields = array(
		  "ID" => $iElementIdCatalog,
		  "QUANTITY_TRACE" => "D",
		  "PRICE_TYPE" => "S",
		  "QUANTITY" => $totalQuantity,
		  "QUANTITY_TRACE" => "D",
		  "CAN_BUY_ZERO" => "D",
		  "NEGATIVE_AMOUNT_TRACE" => "D",
		  );
		  \CCatalogProduct::Add($arProductFields);
		  }
		 * 
		 */
		unset($obElement);
	}

	/**
	 * Ищем товар из каталога сайта по ID товара из каталога 1С
	 * @param int $id
	 */
	function findCatalogProductIdBy1CProductId($id, $iColor = false)
	{
		$arFilter = array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_SYS_LINK_1C" => $id);
		if ($iColor !== false)
		{
			$arFilter['PROPERTY_COLOR'] = $iColor;
		}
		$rElements = \CIBlockElement::GetList(array(), $arFilter, false, false, array("ID"));
		if ($arElement = $rElements->Fetch())
		{
			return $arElement['ID'];
		}
		return false;
	}

	/**
	 * Ищем предложение из каталога сайта по ID товара из каталога 1С
	 * @param int $id
	 * @param int $iCml2Link
	 */
	function findCatalogOfferIdBy1COfferId($id, $iCml2Link)
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "PROPERTY_SYS_LINK_1C" => $id, "PROPERTY_CML2_LINK" => $iCml2Link), false, false, array("ID"));
		if ($arElement = $rElements->Fetch())
		{
			return $arElement['ID'];
		}
		return false;
	}

	/**
	 * Получаем данные элемента
	 * @param int $id
	 */
	function getElementData($id)
	{
		$arFields = false;
		if ($id > 0)
		{
			$rElement = \CIBlockElement::GetList(array(), array("ID" => $id));
			if ($rsElement = $rElement->GetNextElement(false, false))
			{
				$arFields = $rsElement->GetFields();
				$arFields['PROPERTIES'] = $rsElement->GetProperties();
			}
		}
		return $arFields;
	}

	/**
	 * Транслитерация названия
	 * @param string $strName
	 */
	function doTranslitName($strName)
	{
		$strName = trim($strName);
		return \CUtil::translit($strName, "ru", array(
					"max_len" => 100,
					"change_case" => 'L',
					"replace_space" => '-',
					"replace_other" => '-',
					"delete_repeat_replace" => true,
					"safe_chars" => '',
		));
	}

	public function SetProductPrice($ProductID, $Price, $Currency, $quantityFrom = false, $quantityTo = false, $bGetID = false)
	{
		$bGetID = ($bGetID == true);

		$arFields = array();
		$arFields["PRICE"] = (float) $Price;
		$arFields["CURRENCY"] = $Currency;
		$arFields["QUANTITY_FROM"] = ($quantityFrom == false ? false : (int) $quantityFrom);
		$arFields["QUANTITY_TO"] = ($quantityTo == false ? false : (int) $quantityTo);
		$arFields["EXTRA_ID"] = false;

		if ($arBasePrice = $this->GetProductPrice($ProductID, $quantityFrom, $quantityTo, false))
		{
			$ID = \CPrice::Update($arBasePrice["ID"], $arFields);
		} else
		{
			//$arBaseGroup = CCatalogGroup::GetBaseGroup();
			$arFields["CATALOG_GROUP_ID"] = ODRISHOP_1C_PRICE_ID; //$arBaseGroup["ID"];
			$arFields["PRODUCT_ID"] = $ProductID;

			$ID = \CPrice::Add($arFields);
		}
		if (!$ID)
			return false;

		return ($bGetID ? $ID : true);
	}

	function GetProductPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int) $productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => ODRISHOP_1C_PRICE_ID
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int) $quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int) $quantityTo;

		if ($boolExt === false)
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID'
			);
		} else
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID'
			);
		}

		$db_res = \CPrice::GetListEx(
						array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch())
		{
			return $res;
		}

		return false;
	}

	public function SetProductOldPrice($ProductID, $Price, $Currency, $quantityFrom = false, $quantityTo = false, $bGetID = false)
	{
		$bGetID = ($bGetID == true);

		$arFields = array();
		$arFields["PRICE"] = (float) $Price;
		$arFields["CURRENCY"] = $Currency;
		$arFields["QUANTITY_FROM"] = ($quantityFrom == false ? false : (int) $quantityFrom);
		$arFields["QUANTITY_TO"] = ($quantityTo == false ? false : (int) $quantityTo);
		$arFields["EXTRA_ID"] = false;

		if ($arBasePrice = $this->GetProductOldPrice($ProductID, $quantityFrom, $quantityTo, false))
		{
			if ($Price === false)
			{
				\CPrice::Delete($arBasePrice["ID"]);
			} else
			{
				$ID = \CPrice::Update($arBasePrice["ID"], $arFields);
			}
		} else
		{
			//$arBaseGroup = CCatalogGroup::GetBaseGroup();
			$arFields["CATALOG_GROUP_ID"] = ODRISHOP_1C_OLD_PRICE_ID; //$arBaseGroup["ID"];
			$arFields["PRODUCT_ID"] = $ProductID;

			$ID = \CPrice::Add($arFields);
		}
		if (!$ID)
			return false;

		return ($bGetID ? $ID : true);
	}

	function GetProductOldPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int) $productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => ODRISHOP_1C_OLD_PRICE_ID
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int) $quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int) $quantityTo;

		if ($boolExt === false)
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID'
			);
		} else
		{
			$arSelect = array('ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID'
			);
		}

		$db_res = \CPrice::GetListEx(
						array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch())
		{
			return $res;
		}

		return false;
	}

	function doGetDefaultCategoryByProperties($strName, $strType, $strGender, $strGroup)
	{
		$iResult = DEFAULT_CATEGORY_ID;
		if ($strType != "ODRI MIO" && $strType != "ODRI")
		{
			if (strpos(strtolower($strName), "odri mio") !== false)
			{
				$strType == "ODRI MIO";
			} elseif (strpos(strtolower($strName), "odri") !== false)
			{
				$strType == "ODRI";
			}
		}
		if ($strType == "ODRI MIO")
		{
			$iResult = DEFAULT_CATEGORY_ODRI_MIO_ID;
			if ($strGroup == "Детская одежда")
			{
				if ($strGender == "Девочки")
				{
					$iResult = DEFAULT_CATEGORY_ODRI_MIO_GIRLS_ID;
					$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
				} elseif ($strGender == "Мальчики")
				{
					$iResult = DEFAULT_CATEGORY_ODRI_MIO_BOYS_ID;
					$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
				}
			} elseif ($strGroup == "Женская обувь")
			{
				$iResult = DEFAULT_CATEGORY_ODRI_MIO_WOMAN_SHOES_ID;
			} elseif ($strGroup == "Женская одежда")
			{
				$iResult = DEFAULT_CATEGORY_ODRI_MIO_WOMAN_ID;
				$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
			} elseif ($strGroup == "Мужская одежда")
			{
				$iResult = DEFAULT_CATEGORY_ODRI_MIO_MAN_ID;
				$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
			}
		} elseif ($strType == "ODRI")
		{
			$iResult = DEFAULT_CATEGORY_ODRI_ID;
			if ($strGroup == "Детская одежда")
			{
				if ($strGender == "Девочки")
				{
					$iResult = DEFAULT_CATEGORY_ODRI_GIRLS_ID;
					$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
				} elseif ($strGender == "Мальчики")
				{
					$iResult = DEFAULT_CATEGORY_ODRI_BOYS_ID;
					$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
				}
			} elseif ($strGroup == "Женская обувь")
			{
				$iResult = DEFAULT_CATEGORY_ODRI_WOMAN_SHOES_ID;
			} elseif ($strGroup == "Женская одежда")
			{
				$iResult = DEFAULT_CATEGORY_ODRI_WOMAN_ID;
				$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
			} elseif ($strGroup == "Мужская одежда")
			{
				$iResult = DEFAULT_CATEGORY_ODRI_MAN_ID;
				$iResult = $this->doUpdateRecatalogChildSection($strName, $iResult);
			}
		}
		return $iResult;
	}

	function doGetCollectionTypeByProperties($strName, $strType, $strGender, $strGroup)
	{
		$iResult = false;
		if ($strType != "ODRI MIO" && $strType != "ODRI")
		{
			if (strpos(strtolower($strName), "odri mio") !== false)
			{
				$strType == "ODRI MIO";
			} elseif (strpos(strtolower($strName), "odri") !== false)
			{
				$strType == "ODRI";
			}
		}
		if ($strType == "ODRI MIO")
		{
			$iResult = COLLECTION_TYPE_ODRI_MIO_ID;
		} elseif ($strType == "ODRI")
		{
			$iResult = COLLECTION_TYPE_ODRI_ID;
		}
		return $iResult;
	}

	function doUpdateRecatalogChildSection($strName, $iMainSection)
	{
		if (empty($this->arSynonimSubSection))
		{
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_REFERENCE_SUBSECTION_SINONYM), false, false, array("ID", "NAME", "PROPERTY_SYNONIM"));
			while ($arElement = $rElements->Fetch())
			{
				if (!empty($arElement['PROPERTY_SYNONIM_VALUE']))
				{
					foreach ($arElement['PROPERTY_SYNONIM_VALUE'] as $val)
					{
						if (strlen($val) > 0)
						{
							$this->arSynonimSubSection[$val] = $arElement['NAME'];
						}
					}
				}
			}
		}
		$iResult = $iMainSection;
		foreach ($this->arSynonimSubSection as $k => $v)
		{
			if (strpos(strtolower($strName), " " . $k . " ") !== false || strpos(strtolower($strName), " " . $k) !== false || strpos(strtolower($strName), $k . " ") !== false)
			{
				$arSubSection = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "NAME" => $v, "SECTION_ID" => $iMainSection))->Fetch();
				if ($arSubSection)
				{
					$iResult = $arSubSection['ID'];
					break;
				}
			}
		}
		return $iResult;
	}

	function doCheckElementPhotosFrom1C($iElementIdCatalog, $arPhotos)
	{
		$obElement = new \CIBlockElement();
		$arCatalogElement = $this->getElementData($iElementIdCatalog);
		foreach ($arPhotos as $photo)
		{
			$strFile = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($photo);
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHash[$strHash] = $strFile;
		}
		$arPhotosInDir = array_values($arHash);

		//Проверка и обновление дополнительных фото
		//Сначала формируем массив хэшей и проверяем
		$arHashOld = array();
		$arHashNew = array();
		foreach ($arPhotosInDir as $strFile)
		{
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashNew[$strHash] = $strHash;
		}
		if (!is_array($arCatalogElement['PROPERTIES']['INFO_PHOTO_1C']['VALUE']))
		{
			$arCatalogElement['PROPERTIES']['INFO_PHOTO_1C']['VALUE'] = array($arCatalogElement['PROPERTIES']['INFO_PHOTO_1C']['VALUE']);
		}
		foreach ($arCatalogElement['PROPERTIES']['INFO_PHOTO_1C']['VALUE'] as $iPhotoOldId)
		{
			$strFile = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($iPhotoOldId);
			$strHash = filesize($strFile) . "|" . md5_file($strFile) . "|" . crc32(file_get_contents($strFile));
			$arHashOld[$strHash] = $strHash;
		}
		//Сравниваем массивы
		foreach ($arHashNew as $k => $v)
		{
			if (isset($arHashOld[$k]))
			{
				unset($arHashOld[$k]);
				unset($arHashNew[$k]);
			}
		}
		if (!empty($arHashOld) || !empty($arHashNew))
		{
			//Обновляем дополнительные фото.
			//Сначала удалим старые значения
			$arProp = array();
			foreach ($arCatalogElement['PROPERTIES']['INFO_PHOTO_1C']['VALUE'] as $k => $strFile)
			{
				$arProp[$arCatalogElement['PROPERTIES']['INFO_PHOTO_1C']['PROPERTY_VALUE_ID'][$k]] = array(
					"del" => "Y",
					"old_file" => $strFile
				);
			}

			//И сформируем новые
			foreach ($arPhotosInDir as $k => $strFile)
			{
				$arProp['n' . $k] = array(
					"VALUE" => \CFile::MakeFileArray($strFile),
					"DESCRIPTION" => ""
				);
			}
			if (!empty($arProp))
			{
				$obElement->SetPropertyValueCode($arCatalogElement['ID'], "INFO_PHOTO_1C", $arProp);
			}
		}

		unset($obElement);
	}

	function doCheckDescriptions()
	{
		$obElement = new \CIBlockElement();
		$arListElements = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "PROPERTY_SYS_LINK_1C", "PROPERTY_SYS_LINK_1C.NAME"));
		while ($arElement = $rElements->Fetch())
		{
			$arListElements[$arElement['PROPERTY_SYS_LINK_1C_VALUE']]['NAME'] = $arElement['PROPERTY_SYS_LINK_1C_NAME'];
			$arListElements[$arElement['PROPERTY_SYS_LINK_1C_VALUE']]['ITEMS'][] = $arElement['ID'];
			$arDescription = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_DESCRIPTIONS, "PROPERTY_PRODUCTS" => $arElement['ID']), false, false, array("ID"))->Fetch();
			if ($arDescription)
			{
				$arListElements[$arElement['PROPERTY_SYS_LINK_1C_VALUE']]['DESCRIPTION'][$arDescription['ID']][] = $arElement['ID'];
			}
		}
		foreach ($arListElements as $k => $v)
		{
			if (!isset($v['DESCRIPTION']))
			{
				$arFields = array(
					"NAME" => $v['NAME'],
					"ACTIVE" => "Y",
					"IBLOCK_ID" => IBLOCK_DESCRIPTIONS,
					"PROPERTY_VALUES" => array(
						"PRODUCTS" => $v['ITEMS']
					)
				);
				$obElement->Add($arFields);
			} else
			{
				foreach ($v['DESCRIPTION'] as $idDesc => $arChild)
				{
					$obElement->SetPropertyValueCode($idDesc, "PRODUCTS", $v['ITEMS']);
				}
			}
		}
		unset($obElement);
	}

	function doCheckDiscountPercent()
	{
		$arResult = array();
		$arResult["PRICES"] = \CIBlockPriceTools::GetCatalogPrices(IBLOCK_CATALOG, array("РРЦ одришоп  (руб)", "OLDPRICE"));
		$arResult['PRICES_ALLOW'] = \CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"CATALOG_TYPE",
			"PROPERTY_SYS_DISCOUNT_SIZE",
			"PROPERTY_SYS_DISCOUNT_PRICE"
		);
		foreach ($arResult["PRICES"] as $arPrice)
		{
			$arSelect[] = "CATALOG_GROUP_" . $arPrice['ID'];
		}
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, $arSelect);
		while ($arElement = $rElements->Fetch())
		{
			$arResult['ITEMS'][$arElement['ID']] = $arElement;
			$arResult["ELEMENTS"][$arElement['ID']] = $arElement['ID'];
		}
		if (!empty($arResult["ELEMENTS"]))
		{
			\Bitrix\Catalog\Discount\DiscountManager::preloadPriceData($arResult["ELEMENTS"], $arResult['PRICES_ALLOW']);
			\Bitrix\Catalog\Discount\DiscountManager::preloadProductDataToExtendOrder($arResult["ELEMENTS"], array(2));
			\CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
			\CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => IBLOCK_CATALOG, 'GET_BY_ID' => 'Y'));
		}

		foreach ($arResult['ITEMS'] as $k => $arItem)
		{
			$arItem["PRICES"] = \CIBlockPriceTools::GetItemPrices(IBLOCK_CATALOG, $arResult["PRICES"], $arItem, "Y");
			$percent = 0;
			$iPrice = 0;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$ak = array_keys($arItem['PRICES']);
				$price = $arItem['PRICES'][$ak[0]];
				$price['BASE_PRICE'] = $price['VALUE'];
				$price['VALUE'] = $price['DISCOUNT_VALUE'];
				$price['PRICE'] = $price['DISCOUNT_VALUE'];

				$arItem['RESULT_PRICE'] = doModifyItemPrice($price, $oldPrice);
				$iPrice = $price['PRICE'];
				$percent = $arItem['RESULT_PRICE']['DISCOUNT'] / $arItem['RESULT_PRICE']['BASE_PRICE'];
			} else
			{
				$ak = array_keys($arItem['PRICES']);
				$arItem['RESULT_PRICE'] = $arItem['PRICES'][$ak[0]];
				$iPrice = $arItem['RESULT_PRICE']['DISCOUNT_VALUE'];
				$percent = ($arItem['RESULT_PRICE']['VALUE'] - $arItem['RESULT_PRICE']['DISCOUNT_VALUE']) / $arItem['RESULT_PRICE']['VALUE'];
			}
			$arItem['percent'] = round($percent, 2);
			if ($arItem['PROPERTY_SYS_DISCOUNT_SIZE_VALUE'] != $arItem['percent'])
			{
				\CIBlockElement::SetPropertyValueCode($arItem['ID'], "SYS_DISCOUNT_SIZE", $arItem['percent']);
			}
			if ($arItem['PROPERTY_SYS_DISCOUNT_PRICE_VALUE'] != $iPrice)
			{
				\CIBlockElement::SetPropertyValueCode($arItem['ID'], "SYS_DISCOUNT_PRICE", $iPrice);
			}
		}
	}

	function doUpdateNameGenerated()
	{
		$obElement = new \CIBlockElement();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "PROPERTY_COLOR", "PROPERTY_COLOR.NAME", "PROPERTY_SYS_NAME_GENERATED", "PROPERTY_CML2_ARTICLE", "PROPERTY_BRAND", "PROPERTY_PARTS_NAME"));
		while ($arElement = $rElements->Fetch())
		{
			$strName = "";
			$arName = doParseName($arElement['NAME'], $arElement['PROPERTY_CML2_ARTICLE_VALUE'], $arElement['PROPERTY_BRAND_VALUE'], $arElement['PROPERTY_PARTS_NAME_VALUE']);
			if (!empty($arName))
			{
				$strName = $arName['ARTICLE'] . " " . $arName['PRODUCT_TYPE'] . " " . $arName['NAME'] . " " . $arElement['PROPERTY_COLOR_NAME'];
			}
			if ($arElement['PROPERTY_SYS_NAME_GENERATED_VALUE'] != $strName)
			{
				$obElement->SetPropertyValueCode($arElement['ID'], "SYS_NAME_GENERATED", $strName);
			}
		}
	}

	function doCheckUnifySections()
	{
		//Загружаем объединяющие разделы
		$rSections = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "UF_UNIFING_SECTION" => 1), false, array("UF_*"));
		while ($arSection = $rSections->Fetch())
		{
			$arSectionElements = array();
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				$arSectionElements[$arElement['ID']] = $arElement['ID'];
			}
			if (!empty($arSection['UF_UNIFING_SECTIONS']))
			{
				//Загружаем перечень товаров которые есть в связанных разделах
				if (!is_array($arSection['UF_UNIFING_SECTIONS']))
				{
					$arSection['UF_UNIFING_SECTIONS'] = array($arSection['UF_UNIFING_SECTIONS']);
				}
				$arElementsInSections = array();
				foreach ($arSection['UF_UNIFING_SECTIONS'] as $sectionId)
				{
					if ($sectionId > 0)
					{
						$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => $sectionId, "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
						while ($arElement = $rElements->Fetch())
						{
							$arElementsInSections[] = $arElement['ID'];
						}
					}
				}
				foreach ($arElementsInSections as $elementId)
				{
					if (!in_array($elementId, $arSectionElements))
					{
						$this->doAddElementToSection($elementId, $arSection['ID']);
					} else
					{
						unset($arSectionElements[$elementId]);
					}
				}
			}
			foreach ($arSectionElements as $id)
			{
				$this->doDeleteElementFromSection($id, $arSection['ID']);
			}
		}
	}

	function doAddElementToSection($iElementId, $iSectionId)
	{
		$obElement = new \CIBlockElement();
		$arElementSections = array();
		$rSections = $obElement->GetElementGroups($iElementId, true, array("ID"));
		while ($arSection = $rSections->Fetch())
		{
			$arElementSections[] = $arSection['ID'];
		}
		$arElementSections[] = $iSectionId;
		$obElement->SetElementSection($iElementId, $arElementSections);
		unset($obElement);
	}

	function doDeleteElementFromSection($iElementId, $iSectionId)
	{
		$obElement = new \CIBlockElement();
		$arElementSections = array();
		$rSections = $obElement->GetElementGroups($iElementId, true, array("ID"));
		while ($arSection = $rSections->Fetch())
		{
			if ($arSection['ID'] != $iSectionId)
			{
				$arElementSections[] = $arSection['ID'];
			}
		}
		$obElement->SetElementSection($iElementId, $arElementSections);
		unset($obElement);
	}

	function doLogPhotos()
	{
		$arNotFinded = array();
		foreach ($this->arAllPhotos as $strArticle => $arArticle)
		{
			foreach ($arArticle as $strColor => $arColor)
			{
				if (!in_array($strColor, $this->arAllPhotosFinded[$strArticle]))
				{
					$arNotFinded[$strArticle][] = $strColor;
				}
			}
		}
		myPrint($arNotFinded);
	}

	function doCheckSectionSale()
	{
		$arCurrentElements = array();
		//Грузим текущий перечень товаров в разделе
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => SALE_SECTION_ID), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			$arCurrentElements[$arElement['ID']] = $arElement['ID'];
		}
		//Ищем товары для распродажи
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, ">PROPERTY_SYS_DISCOUNT_SIZE" => 0, "!PROPERTY_SYS_OLD_SIZE" => "Y"), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			if (isset($arCurrentElements[$arElement['ID']]))
			{
				unset($arCurrentElements[$arElement['ID']]);
			} else
			{
				$this->doAddElementToSection($arElement['ID'], SALE_SECTION_ID);
			}
		}
		foreach ($arCurrentElements as $id)
		{
			$this->doDeleteElementFromSection($id, SALE_SECTION_ID);
		}
	}

	function doCheckSectionOldSize()
	{
		$arCurrentElements = array();
		//Грузим текущий перечень товаров последнего раздела
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "SECTION_ID" => OLDSIZE_SECTION_ID), false, false, array("ID"));
		//$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_SYS_OLD_SIZE" => "Y"), false, false, array("ID"));
		while ($arElement = $rElements->Fetch())
		{
			$arCurrentElements[$arElement['ID']] = $arElement['ID'];
		}

		//Собираем ID игнорируемых товаров
		$arIgnoredElementsId = array();
		if (intval(IGNORED_OLDSIZE_SECTION_ID1) > 0)
		{
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "INCLUDE_SUBSECTIONS" => "Y", "SECTION_ID" => IGNORED_OLDSIZE_SECTION_ID1), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				$arIgnoredElementsId[$arElement['ID']] = $arElement['ID'];
			}
		}
		if (intval(IGNORED_OLDSIZE_SECTION_ID2) > 0)
		{
			$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "INCLUDE_SUBSECTIONS" => "Y", "SECTION_ID" => IGNORED_OLDSIZE_SECTION_ID2), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				$arIgnoredElementsId[$arElement['ID']] = $arElement['ID'];
			}
		}
		//Ищем товары для последнего размера
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "PROPERTY_SYS_SIZE_AVAILABLE"));
		while ($arElement = $rElements->Fetch())
		{
			if (count($arElement['PROPERTY_SYS_SIZE_AVAILABLE_VALUE']) != 1)
			{
				continue;
			}
			if (isset($arIgnoredElementsId[$arElement['ID']]))
			{
				continue;
			}
			if (isset($arCurrentElements[$arElement['ID']]))
			{
				unset($arCurrentElements[$arElement['ID']]);
			} else
			{
				$this->doAddElementToSection($arElement['ID'], OLDSIZE_SECTION_ID);
				\CIBlockElement::SetPropertyValueCode($arElement['ID'], "SYS_OLD_SIZE", "Y");
			}
		}
		foreach ($arCurrentElements as $id)
		{
			$this->doDeleteElementFromSection($id, OLDSIZE_SECTION_ID);
			\CIBlockElement::SetPropertyValueCode($id, "SYS_OLD_SIZE", "N");
		}
	}

	function doUpdateNewCollection()
	{
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "PROPERTY_SYS_NEW_SORTINDEX", "PROPERTY_COLLECTION.SORT"));
		while ($arElement = $rElements->Fetch())
		{
			if ($arElement['PROPERTY_SYS_NEW_SORTINDEX_VALUE'] != $arElement['PROPERTY_COLLECTION_SORT'])
			{
				\CIBlockElement::SetPropertyValueCode($arElement['ID'], "SYS_NEW_SORTINDEX", $arElement['PROPERTY_COLLECTION_SORT']);
			}
			/*
			  if ($arElement['PROPERTY_COLLECTION_VALUE'] == NEW_COLLECTION_ID)
			  {
			  if ($arElement['PROPERTY_LABEL_NEW_VALUE'] != "Y")
			  {
			  \CIBlockElement::SetPropertyValueCode($arElement['ID'], "LABEL_NEW", "Y");
			  }
			  } else
			  {
			  if ($arElement['PROPERTY_LABEL_NEW_VALUE'] != "N")
			  {
			  \CIBlockElement::SetPropertyValueCode($arElement['ID'], "LABEL_NEW", "N");
			  }
			  }
			 * 
			 */
		}
	}

}
