<?

namespace Webavk\H2OSport\Cron;

use Bitrix\Iblock\InheritedProperty\SectionTemplates;
use Bitrix\Iblock\InheritedProperty\SectionValues;

class CatalogCheckSeo
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arLinksSections = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start doCheckSeoSections");
		$this->doCheckSeoSections();
		//$this->doLog("Start CheckSectionsActive");
		//$this->doCheckSectionsActive();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog) {
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doCheckSeoSections()
	{
		$bClear = false;
		$obSection = new \CIBlockSection();
		$rSections = \CIBlockSection::GetTreeList(array("IBLOCK_ID" => IBLOCK_CATALOG));
		while ($arSection = $rSections->Fetch()) {
			if (in_array(strtolower($arSection['NAME']), array("детские", "детское", "для собак", "женское", "мужское", "унисекс", "стандарт", "комплект"))) {
				$ibProp = new SectionTemplates(IBLOCK_CATALOG, $arSection['ID']);
				$arTemplates = $ibProp->findTemplates();
				$strTemplateSECTION_META_TITLE = "";
				$strTemplateSECTION_META_DESCRIPTION = "";
				$strTemplateSECTION_PAGE_TITLE = "";
				if (in_array(strtolower($arSection['NAME']), array("детские", "детское"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} для детей - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} для детей - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} для детей";
				} elseif (in_array(strtolower($arSection['NAME']), array("для собак"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} для собак - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} для собак - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} для собак";
				} elseif (in_array(strtolower($arSection['NAME']), array("женское"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} для женщин - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} для женщин - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} для женщин";
				} elseif (in_array(strtolower($arSection['NAME']), array("мужское"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} для мужчин - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} для мужчин - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} для мужчин";
				} elseif (in_array(strtolower($arSection['NAME']), array("унисекс"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} унисекс - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} унисекс - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} унисекс";
				} elseif (in_array(strtolower($arSection['NAME']), array("стандарт"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} стандарт - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} стандарт - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} стандарт";
				} elseif (in_array(strtolower($arSection['NAME']), array("комплект"))) {
					$strTemplateSECTION_META_TITLE = "{=parent.Name} комплект - купить недорого в интернет-магазине H2OSport";
					$strTemplateSECTION_META_DESCRIPTION = "{=parent.Name} комплект - помимо подробного описания к каждому виду товара и его характеристик, в каталоге есть фотоизображение, а также отзывы покупателей";
					$strTemplateSECTION_PAGE_TITLE = "{=parent.Name} комплект";
				}
				$arNewTemplates = array();
				foreach ($arTemplates as $k => $v) {
					if ($v['INHERITED'] != "Y") {
						$arNewTemplates[$k] = $v['TEMPLATE'];
					}
				}
				$bUpdate = false;
				if (isset($arNewTemplates['SECTION_META_TITLE']) && $arNewTemplates['SECTION_META_TITLE'] != $strTemplateSECTION_META_TITLE) {
					$bUpdate = true;
					$arNewTemplates['SECTION_META_TITLE'] = $strTemplateSECTION_META_TITLE;
				} elseif (!isset($arNewTemplates['SECTION_META_TITLE'])) {
					$bUpdate = true;
					$arNewTemplates['SECTION_META_TITLE'] = $strTemplateSECTION_META_TITLE;
				}
				if (isset($arNewTemplates['SECTION_META_DESCRIPTION']) && $arNewTemplates['SECTION_META_DESCRIPTION'] != $strTemplateSECTION_META_DESCRIPTION) {
					$bUpdate = true;
					$arNewTemplates['SECTION_META_DESCRIPTION'] = $strTemplateSECTION_META_DESCRIPTION;
				} elseif (!isset($arNewTemplates['SECTION_META_DESCRIPTION'])) {
					$bUpdate = true;
					$arNewTemplates['SECTION_META_DESCRIPTION'] = $strTemplateSECTION_META_DESCRIPTION;
				}
				if (isset($arNewTemplates['SECTION_PAGE_TITLE']) && $arNewTemplates['SECTION_PAGE_TITLE'] != $strTemplateSECTION_PAGE_TITLE) {
					$bUpdate = true;
					$arNewTemplates['SECTION_PAGE_TITLE'] = $strTemplateSECTION_PAGE_TITLE;
				} elseif (!isset($arNewTemplates['SECTION_PAGE_TITLE'])) {
					$bUpdate = true;
					$arNewTemplates['SECTION_PAGE_TITLE'] = $strTemplateSECTION_PAGE_TITLE;
				}
				if ($bUpdate) {
					$bClear = true;
					$res = $obSection->Update($arSection['ID'], array(
						"IPROPERTY_TEMPLATES" => $arNewTemplates,
					));
					if (!$res) {
						echo $obSection->LAST_ERROR . "\n";
					}
					myPrint($arNewTemplates);
				}
			}
		}
		if ($bClear) {
			$ipropValues = new SectionValues(IBLOCK_CATALOG, 0);
			$ipropValues->clearValues();
		}
	}
}
