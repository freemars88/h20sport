<?

namespace Webavk\H2OSport\Cron;

class CatalogCheckCodes
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start CheckCodes");
		$this->doCheckCodes();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog)
		{
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doCheckCodes()
	{
		$obElement = new \CIBlockElement();
		$arAllCodes = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y"), false, false, array("ID", "CODE"));
		while ($arElement = $rElements->Fetch())
		{
			$arAllCodes[$arElement['CODE']][$arElement['ID']] = $arElement['ID'];
		}
		foreach ($arAllCodes as $strCode => $arID)
		{
			if (count($arID) > 0)
			{
				$i = 0;
				foreach ($arID as $k => $v)
				{
					if ($i > 0)
					{
						$strNewCode = $strCode . "-" . $v;
						$obElement->Update($v, array(
							"CODE" => $strNewCode
						));
					}
					$i++;
				}
			}
		}
	}

}
