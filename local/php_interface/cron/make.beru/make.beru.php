<?

namespace Webavk\H2OSport\Cron;

use Bitrix\Iblock\InheritedProperty\SectionValues;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class MakeBeru
{

	protected $bIsStop = false;
	protected $arOptions = array();
	protected $arCacheData = array();
	protected $arProducts = array();

	function __construct($arOptions, $bLog = false)
	{
		$this->arOptions = $arOptions;
		$this->bIsLog = $bLog;
	}

	function Start()
	{
		$this->doLog("Start doMakeData");
		$this->doMakeData();
		$this->doLog("Start doMakeFile");
		$this->doMakeFile();
		$this->doLog("Finish");
	}

	function Stop()
	{
		$this->bIsStop = true;
	}

	function doLog($strMessage)
	{
		if ($this->bIsLog) {
			echo date("d.m.Y H:i:s") . ": " . $strMessage . "\n";
			ob_end_flush();
			ob_end_flush();
			ob_end_flush();
		}
	}

	function doMakeData()
	{
		$obElement = new \CIBlockElement();
		$arAllCodes = array();
		$rElements = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y"));
		while ($rsElement = $rElements->GetNextElement()) {
			$arElement = $rsElement->GetFields();
			$arElement['PROPERTIES'] = $rsElement->GetProperties();
			$arCategory = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $arElement['IBLOCK_SECTION_ID']))->GetNext(false, false);
			$ipropValues = new SectionValues($arCategory['IBLOCK_ID'], $arCategory['ID']);
			$arCategory['IPROPERTY_VALUES'] = $ipropValues->getValues();
			$rSkuElement = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_CATALOG_OFFERS, "ACTIVE" => "Y", "CATALOG_AVAILABLE" => "Y", "PROPERTY_CML2_LINK" => $arElement['ID']), false, false, array("ID", "NAME", "DETAIL_PAGE_URL"));
			while ($arSkuElement = $rSkuElement->GetNext(false)) {
				//$arSkuElement = $rsSkuElement->GetFields();
				//$arSkuElement['PROPERTIES'] = $rsSkuElement->GetProperties();
				$arPrice = $this->GetProductPrice($arSkuElement['ID']);
				$arPriceOld = $this->GetProductOldPrice($arSkuElement['ID']);
				$arDataElement = array(
					"ID" => $arSkuElement['ID'],
					"NAME" => $arSkuElement['NAME'],
					"CATEGORY" => (strlen($arCategory['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']) > 0 ? $arCategory['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] : $arCategory['NAME']),
					"BRAND" => ($arElement['PROPERTIES']['BRAND']['VALUE'] > 0 ? $this->getBrandName($arElement['PROPERTIES']['BRAND']['VALUE']) : ""),
					"LINK" => "https://www.h2osport.ru" . $arSkuElement['DETAIL_PAGE_URL'],
					"MANUFACTURE" => ($arElement['PROPERTIES']['MANUFACTURE']['VALUE'] > 0 ? $this->getBrandName($arElement['PROPERTIES']['MANUFACTURE']['VALUE']) : ""),
					"COUNTRY" => ($arElement['PROPERTIES']['COUNTRY']['VALUE'] > 0 ? $this->getBrandName($arElement['PROPERTIES']['COUNTRY']['VALUE']) : ""),
					"BARCODE" => "",
					"ARTICLE" => $arElement['PROPERTIES']['CML2_ARTICLE']['VALUE'],
					"DESCRIPTION" => substr(strip_tags($arElement['~DETAIL_TEXT']), 0, 2990),
					"PRICE" => $arPrice['PRICE'],
					"OLDPRICE" => $arPriceOld['PRICE'],
					"NDS" => "VAT_0",
				);
				$this->arProducts[] = $arDataElement;
			}
			//break;
		}
//myPrint($this->arProducts);
	}

	function GetProductPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int)$productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => H2OSPORT_BASE_PRICE_ID,
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int)$quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int)$quantityTo;

		if ($boolExt === false) {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
			);
		} else {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID',
			);
		}

		$db_res = \CPrice::GetListEx(
			array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch()) {
			return $res;
		}

		return false;
	}

	function GetProductOldPrice($productID, $quantityFrom = false, $quantityTo = false, $boolExt = true)
	{
		$productID = (int)$productID;
		if ($productID <= 0)
			return false;

		$arFilter = array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => OLD_PRICE_ID,
		);

		if ($quantityFrom !== false)
			$arFilter['QUANTITY_FROM'] = (int)$quantityFrom;
		if ($quantityTo !== false)
			$arFilter['QUANTITY_TO'] = (int)$quantityTo;

		if ($boolExt === false) {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
			);
		} else {
			$arSelect = array(
				'ID', 'PRODUCT_ID', 'EXTRA_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY', 'TIMESTAMP_X',
				'QUANTITY_FROM', 'QUANTITY_TO', 'TMP_ID',
				'PRODUCT_QUANTITY', 'PRODUCT_QUANTITY_TRACE', 'PRODUCT_CAN_BUY_ZERO',
				'PRODUCT_NEGATIVE_AMOUNT_TRACE', 'PRODUCT_WEIGHT', 'ELEMENT_IBLOCK_ID',
			);
		}

		$db_res = \CPrice::GetListEx(
			array('QUANTITY_FROM' => 'ASC', 'QUANTITY_TO' => 'ASC'), $arFilter, false, array('nTopCount' => 1), $arSelect
		);
		if ($res = $db_res->Fetch()) {
			return $res;
		}

		return false;
	}

	function getBrandName($iBrand)
	{
		$arElement = \CIBlockElement::GetList(array(), array("ID" => $iBrand), false, false, array("ID", "NAME"))->Fetch();
		return $arElement['NAME'];
	}

	function doMakeFile()
	{
		global $APPLICATION;
		$strFile = $_SERVER['DOCUMENT_ROOT'] . "/upload/prod.csv";
		$f = fopen($strFile, "wt");
		if ($f) {
			foreach ($this->arProducts as $arData) {
				$arRow = array(
					"",
					$arData['ID'],
					$arData['NAME'],
					$arData['CATEGORY'],
					$arData['BRAND'],
					$arData['LINK'],
					$arData['MANUFACTURE'],
					$arData['COUNTRY'],
					$arData['BARCODE'],
					$arData['ARTICLE'],
					$arData['DESCRIPTION'],
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					str_replace(".", ",", $arData['PRICE']),
					str_replace(".", ",", $arData['OLDPRICE']),
					$arData['NDS'],
					"",
					"",
					"",
				);
				fputcsv($f, $arRow, ';', '"');
			}
			fclose($f);
			file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/upload/prod.win.csv", $APPLICATION->ConvertCharset(file_get_contents($strFile),"UTF-8","WINDOWS-1251"));
		}
	}
}

