function showWidget(pvzObj, partner, showSuccessPvzWrite){
    if(partner == 'DS')
        partner = 'DALLI-SERVICE';
    var partnersblock = '<div class="partnerblock"><div class="partner_img"><img src="/bitrix/images/dalliservicecom.delivery/'+partner+'.png"></div><div class="partner_delivery_info">';
    if(typeof pvzObj.deliveryPrice != 'undefined' && pvzObj.deliveryPrice.length > 0)
        partnersblock += '<div class="delivery_info_row"><div class="vidget_icon vidget_cost"></div><div class="ds_price">'+pvzObj.deliveryPrice+'</div></div>';
    if(typeof pvzObj.deliveryPeriod != 'undefined' && pvzObj.deliveryPeriod.length > 0)
        partnersblock += '<div class="delivery_info_row"><div class="vidget_icon vidget_delivery"></div><div class="ds_date">'+pvzObj.deliveryPeriod+'</div></div>';
    partnersblock = partnersblock+'</div></div><div id="partners_info"></div>';

    vidgetContent = '<div id="ds_vidget_sidebar"><div class="sidebar_wrapper">' + partnersblock + '</div><button disabled id="confirmChooze" onclick="confirmChooze(showSuccessPvzWrite_)">'+BX.message('CHOOZE')+'</button></div><div id="ds_vidget_map"></div>';

    DsPvzVidget = new BX.PopupWindow("ds_pvz_popup", null, {
        content: vidgetContent,
        closeIcon: {right: "-5px", top: "-5px"},
        zIndex: 0,
        autoHide: true,
        offsetLeft: 0,
        lightShadow : true,
        closeByEsc : true,
        offsetTop: 0,
        draggable: {restrict: false},
        overlay: {backgroundColor: 'grey', opacity: '30' },
        events: {
            onPopupClose: function () {
                DsPvzVidget.destroy();
            },
            onAfterPopupShow: function () {
                showSuccessPvzWrite_ = showSuccessPvzWrite;
            }

        }
    });

    // var myArray = [ 3, 5, 7, 9, 4, 8, 2, 1, 6 ];
    //myArray.forEach( function(item){ alert(item); } );

    ymaps.ready(init);
    function init(){
        center =
            myMap = new ymaps.Map("ds_vidget_map", {
                center: [50, 50],
                zoom: 15,
                controls: ['zoomControl', 'typeSelector', 'routeEditor', 'trafficControl']
            });
        if(typeof pvzObj.points != 'undefined'){
            myCollection = new ymaps.GeoObjectCollection();
            $.each(pvzObj.points, function(index, value) {
                var balloonContent = '';
                if (typeof value.ADDRESS != 'undefined' && value.ADDRESS.length > 0){
                    balloonContent = balloonContent+'<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>'+value.ADDRESS+'</div>';
                }
                if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0){
                    balloonContent = balloonContent+'<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div><div>'+value.PHONE+'</div></div>';
                }
                if (typeof value.WORKSHEDULE != 'undefined' && value.WORKSHEDULE.length > 0){
                    balloonContent = balloonContent+'<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>'+value.WORKSHEDULE+'</div>';
                }
                if (typeof value.GPS != 'undefined'){
                    arrCoords = value.GPS.split(',')
                }

                var PVZPoint = new ymaps.Placemark([arrCoords['0'], arrCoords['1']], {
                    hintContent:value.address,
                    balloonContent: balloonContent
                }, {
                    iconLayout: 'default#image',
                });
                /*PVZPoint.events.add('balloonclose', function(e) {
                 document.getElementById('confirmChooze').setAttribute("disabled", "disabled");
                 $('#confirmChooze').fadeTo(0, 0.5);
                 partners_info.innerHTML = '';
                 });*/
                PVZPoint.events.add('click', function(e) {
                    if(typeof value.ADDRESS != 'undefined' && value.ADDRESS.length > 0)
                        address = '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>'+value.ADDRESS+'</div>';
                    else address = '';
                    if(typeof value.PHONE != 'undefined' && value.PHONE.length > 0)
                        phone = '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div>'+value.PHONE+'</div>';
                    else phone = '';
                    if(typeof value.WORKSHEDULE != 'undefined' && value.WORKSHEDULE.length > 0)
                        shedule = '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>'+value.WORKSHEDULE+'</div>';
                    else shedule = '';
                    if(typeof value.DESCRIPTION != 'undefined' && value.DESCRIPTION.length > 0)
                        description = '<div class="vidget_desc_row"><div class="vidget_icon vidget_description"></div>'+value.DESCRIPTION+'</div>';
                    else description = '';
                    content = address+phone+shedule+description;
                    partners_info.innerHTML = content;
                    document.getElementById('confirmChooze').removeAttribute("disabled");
                    $('#confirmChooze').fadeTo(0, 1);
                });

                myCollection.add(PVZPoint);
            });
            myMap.geoObjects.add( myCollection );
            myMap.setBounds(myCollection.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 14) myMap.setZoom(13);});
        }
    }
    DsPvzVidget.show();
}

function send2Dalli(){

    BX.showWait('send2Dalli');
    obj = $('#dalliform').serialize();
    $.ajax({
        type: "POST",
        data: obj,
        dataType: 'json',
        success: function(data){
            if(data != null){
                if(parseInt(data.error)>0){
                    $('#dalliform').hide();
                    $('#dallianswer').html(data.errormsg);
                    $('#responce').show();
                }else{
                    $('#dalliform').hide();
                    $('#responce').html('');
                    $('#responce').html(data.errormsg+' '+data.barcode);
                    $('#responce').show();
                }
            }
            BX.closeWait('send2Dalli')

        }
    });
}

function hideBlock(){
    $('#dalliform').show();
    $('#responce').hide();
}

function confirmChooze(showSuccessPvzWrite_){
    address = strip_tags(address);
    $.ajax({
        url: '/bitrix/js/dalliservicecom.delivery/getAddressProp.php',
        type: "POST",
        dataType: "json",
        success: function(data){
            console.log(showSuccessPvzWrite_);
            $("[name=ORDER_PROP_"+data.addressprop+"]").val(address);
            DsPvzVidget.destroy();
            if(showSuccessPvzWrite_ == 'Y'){
                DsPvzWriteSuccess = new BX.PopupWindow("ds_pvz_write_success", null, {
                    content: BX.message('PVZ_WRITE_SUCCESS'),
                    closeIcon: {right: "-5px", top: "-5px"},
                    zIndex: 0,
                    autoHide: true,
                    offsetLeft: 0,
                    lightShadow : true,
                    closeByEsc : true,
                    offsetTop: 0,
                    draggable: {restrict: false},
                    overlay: {backgroundColor: 'grey', opacity: '30' },
                    events: {
                        onPopupClose: function () {
                            DsPvzWriteSuccess.destroy();
                        },
                    }
                });
                DsPvzWriteSuccess.show();
            }
        }
    });
    /*if(typeof submitFormProxy == 'function')
     submitFormProxy();
     else if (typeof submitForm == 'function')
     submitForm();*/
    return false;
}

function updateOrders(e){
    BX.showWait();
    e.setAttribute('disabled', 'disabled');
    $.ajax({
        url: '/bitrix/js/dalliservicecom.delivery/updateOrders.php',
        type: "POST",
        dataType: "json",
        success: function(data){
            if(data.ok == '1'){
                e.removeAttribute('disabled');
                BX.closeWait();
                alert(BX.message('ORDERS_UPDATE'));
                window.location.reload(true);
            }else{
                e.removeAttribute('disabled');
                BX.closeWait();
                alert(BX.message('ORDERS_UPDATE_ERROR'));
                window.location.reload(true);
            }
        }
    });
}

function updatePvz(e){
    BX.showWait();
    e.setAttribute('disabled', 'disabled');
    $.ajax({
        url: '/bitrix/js/dalliservicecom.delivery/updatePvz.php',
        type: "POST",
        dataType: "json",
        success: function(data){
            if(data.ok == '1'){
                e.removeAttribute('disabled');
                BX.closeWait();
                e.parentNode.nextElementSibling.innerHTML = BX.message('PVZ_UPDATE');
            }else{
                e.removeAttribute('disabled');
                BX.closeWait();
                e.parentNode.nextElementSibling.innerHTML = BX.message('PVZ_UPDATE_ERROR');
            }
        }
    });
}

function strip_tags( str ){
    return str.replace(/<\/?[^>]+>/gi, '');
}
function remove_prod_pos_line(e){
    e.closest("tr").parentNode.removeChild(e.closest("tr"));
}

//==============================================

function ds_time_change_min(){
    if($('#time_min').val() > $('#time_max').val()){
        $('#time_max').val($('#time_min').val());
    }

    var time_prop_id = $('#time_prop_id').val();
        time_min = $('#time_min option:selected').text();
        time_max = $('#time_max option:selected').text();

    $('#soa-property-' + time_prop_id).val(time_min + '-' + time_max);
}
function ds_time_change_max(){
    if($('#time_max').val() < $('#time_min').val()){
        $('#time_min').val($('#time_max').val());
    }

    var time_prop_id = $('#time_prop_id').val();
        time_min = $('#time_min option:selected').text();
        time_max = $('#time_max option:selected').text();

    $('#soa-property-' + time_prop_id).val(time_min + '-' + time_max);
}
$(document).ready(function(){
    var time_prop_id = $('#time_prop_id').val();
    time_def = $('#time_def').val();

    $('#soa-property-' + time_prop_id).prop('type', 'hidden');
    $('label[for="soa-property-23"]').hide();
    $('#soa-property-' + time_prop_id).val(time_def);

});