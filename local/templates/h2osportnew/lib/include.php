<?
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-jquery/jquery-3.4.1.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-jquery/jquery-ui.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-jquery/datepicker-ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.carousel.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.autorefresh.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.lazyload.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.autoheight.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.video.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.animate.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.autoplay.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.navigation.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.hash.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-owlcarousel/owl.support.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/03-fancybox/jquery.fancybox.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.validation/jquery.validate.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.validation/additional-methods.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.validation/localization/messages_ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-jquery.maskedinput/jquery.maskedinput.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-kladr/jquery.kladr.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/dalliservicecom.js", INCLUDE_MINIFY_WEB_CONTENT);

