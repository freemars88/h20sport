<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$buttonId = $this->randString();
?>
<div class="bx-subscribe" id="sender-subscribe">
	<? if (isset($arResult['MESSAGE'])): CJSCore::Init(array("popup")); ?>
		<div id="sender-subscribe-response-cont" style="display: none;">
			<div class="bx_subscribe_response_container">
				<table>
					<tr>
						<td style="padding-right: 40px; padding-bottom: 0px;"><img src="<?= ($this->GetFolder() . '/images/' . ($arResult['MESSAGE']['TYPE'] == 'ERROR' ? 'icon-alert.png' : 'icon-ok.png')) ?>" alt=""></td>
						<td>
							<div style="font-size: 22px;"><?= GetMessage('subscr_form_response_' . $arResult['MESSAGE']['TYPE']) ?></div>
							<div style="font-size: 16px;"><?= htmlspecialcharsbx($arResult['MESSAGE']['TEXT']) ?></div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<script>
			BX.ready(function () {
				var oPopup = BX.PopupWindowManager.create('sender_subscribe_component', window.body, {
					autoHide: true,
					offsetTop: 1,
					offsetLeft: 0,
					lightShadow: true,
					closeIcon: true,
					closeByEsc: true,
					overlay: {
						backgroundColor: 'rgba(57,60,67,0.82)', opacity: '80'
					}
				});
				oPopup.setContent(BX('sender-subscribe-response-cont'));
				oPopup.show();
			});
		</script>
	<? endif; ?>

	<script>
		(function () {
			var btn = BX('bx_subscribe_btn_<?= $buttonId ?>');
			var form = BX('bx_subscribe_subform_<?= $buttonId ?>');

			if (!btn)
			{
				return;
			}

			function mailSender()
			{
				setTimeout(function () {
					if (!btn)
					{
						return;
					}

					BX.addClass(btn, "send");
				}, 400);
			}

			BX.ready(function ()
			{
				BX.bind(btn, 'click', function () {
					setTimeout(mailSender, 250);
					return false;
				});
			});

			BX.bind(form, 'submit', function () {
				btn.disabled = true;
				setTimeout(function () {
					btn.disabled = false;
				}, 2000);

				return true;
			});
		})();
	</script>

	<form id="bx_subscribe_subform_<?= $buttonId ?>" role="form" class="bx-subscribe__form" method="post" action="<?= $arResult["FORM_ACTION"] ?>">
		<?= bitrix_sessid_post() ?>
		<input type="hidden" name="sender_subscription" value="add">
		<input type="text" name="SENDER_SUBSCRIBE_EMAIL" class="bx-subscribe__input" value="" title="<?= GetMessage("subscr_form_email_title") ?>" placeholder="<?= htmlspecialcharsbx(GetMessage('subscr_form_email_title')) ?>" />
		<button type="submit" class="bx-subscribe__button" data-text="Подписаться" id="bx_subscribe_btn_<?= $buttonId ?>">Подписаться</button>
		<?
		foreach ($arResult["RUBRICS"] as $itemID => $itemValue)
		{
			?>
			<input type="hidden" name="SENDER_SUBSCRIBE_RUB_ID[]" value="<?= $itemValue["ID"] ?>" />
		<? }
		?>
	</form>
</div>
