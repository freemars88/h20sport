<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

$availablePages = array();
global $LK_CURRENT_PAGE;

if ($arParams['SHOW_ORDER_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'],
		"name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "order" && $_REQUEST['filter_history'] != "Y"),
	);
}

if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ACCOUNT'],
		"name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "account"),
	);
}

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PRIVATE'],
		"name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "private"),
	);
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y') {

	$delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'] . $delimeter . "filter_history=Y",
		"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
		"selected" => ($LK_CURRENT_PAGE == "order" && $_REQUEST['filter_history'] == "Y"),
	);
}

if ($arParams['SHOW_PROFILE_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PROFILE'],
		"name" => Loc::getMessage("SPS_PROFILE_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "зкщашду"),
	);
}

if ($arParams['SHOW_BASKET_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_BASKET'],
		"name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "ифылуе"),
	);
}

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_SUBSCRIBE'],
		"name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "subscribe"),
	);
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y') {
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_CONTACT'],
		"name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
		"selected" => ($LK_CURRENT_PAGE == "contact"),
	);
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList) {
	foreach ($customPagesList as $page) {
		$availablePages[] = array(
			"path" => $page[0],
			"name" => $page[1],
			"selected" => ($LK_CURRENT_PAGE == $page[2]),
		);
	}
}
?>

<div class="lk">
	<div class="row lk__title-row">
		<div class="col-60 order-2 order-lg-0 col-lg-30 col-xl-40">
			<h1>Личный кабинет</h1>
		</div>
		<div class="col-60 col-lg-30 col-xl-20">
			<?
			if ($USER->IsAuthorized()) {
				$APPLICATION->IncludeComponent(
					"bitrix:sale.personal.account", "", Array(
					"SET_TITLE" => "N",
				), $component
				);
			}
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-60">
			<ul class="lk-menu">
				<?
				foreach ($availablePages as $blockElement) {
					?>
					<li class="lk-menu__item<?= ($blockElement['selected'] ? " lk-menu__item-selected" : "") ?>">
						<a href="<?= htmlspecialcharsbx($blockElement['path']) ?>" class="lk-menu__link"><?= htmlspecialcharsbx($blockElement['name']) ?></a>
					</li>
					<?
				}
				?>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-60">


