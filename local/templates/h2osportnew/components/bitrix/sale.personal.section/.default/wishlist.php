<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

global $LK_CURRENT_PAGE;
$LK_CURRENT_PAGE = "wishlist";

if ($arParams['SHOW_ORDER_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$APPLICATION->AddChainItem("Список желаний", "/personal/wishlist/");

include(dirname(__FILE__) . "/.prolog.php");

$APPLICATION->IncludeComponent("webavk:blank.component", "personal.wishlist", Array(
	"CACHE_TIME" => "3600", // Время кеширования (сек.)
	"CACHE_TYPE" => "N", // Тип кеширования
		), false
);

include(dirname(__FILE__) . "/.epilog.php");
