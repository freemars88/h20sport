<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

global $LK_CURRENT_PAGE;
$LK_CURRENT_PAGE = "subscribe";

if ($arParams['SHOW_SUBSCRIBE_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_SUBSCRIBE_NEW"));
include(dirname(__FILE__) . "/.prolog.php");

$APPLICATION->IncludeComponent(
		'bitrix:catalog.product.subscribe.list', '', array('SET_TITLE' => $arParams['SET_TITLE'])
		, $component
);

include(dirname(__FILE__) . "/.epilog.php");
