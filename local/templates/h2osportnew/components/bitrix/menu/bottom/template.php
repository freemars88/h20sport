<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult))
{
	return;
}
?>
<ul class="footer-menu">
	<?
	foreach ($arResult as $arItem)
	{
		?>
		<li class="footer-menu__item">
			<a href="<?= $arItem["LINK"] ?>" class="footer-menu__item-link"><?= $arItem["TEXT"] ?></a>
		</li>
		<?
	}
	?>
</ul>