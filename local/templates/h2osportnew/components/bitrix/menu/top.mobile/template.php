<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult)) {
	return;
}
?>

<nav class="menu-mobile-top">
	<ul class="menu-mobile-top__items">
		<?
		foreach ($arResult['ITEMS'] as $arItem) {
			$arExtClass = array(
				"menu-mobile-top__item-link",
			);
			if (!empty($arItem['ITEMS'])) {
				$arExtClass[] = "menu-mobile-top__item-link_child";
			}
			?>
			<li class="menu-mobile-top__item">
				<a href="<?= $arItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>"><?= $arItem["TEXT"] ?></a>
				<?
				if (!empty($arItem['ITEMS'])) {
					$arLevel2 = array();
					$arLevel3 = array();
					$arLevel4 = array();
					?>
					<div class="menu-mobile-top-mobile">
						<div class="menu-mobile-top-content">
							<div class="container">
								<ul class="mobilemenu-lev1">
									<?
									foreach ($arItem['ITEMS'] as $k => $arChildItem) {
										$arExtClass = array(
											"mobilemenu-lev1__link",
										);
										if (isset($arChildItem['ITEMS'])) {
											$arExtClass[] = "mobilemenu-lev1__link_parent";
										}
										if ($arChildItem['SELECTED']) {
											$arExtClass[] = "mobilemenu-lev1__link_selected";
										}
										$arExtClass2 = array(
											"mobilemenu-lev1__item",
										);
										if ($arChildItem['SELECTED']) {
											$arExtClass2[] = "mobilemenu-lev1__item_selected";
										}
										?>
										<li class="<?= implode(" ", $arExtClass2) ?>">
											<a href="<?= $arChildItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem["TEXT"] ?></a>
											<?
											if (isset($arChildItem['ITEMS'])) {
												?>
												<ul class="mobilemenu-lev2">
													<?
													foreach ($arChildItem['ITEMS'] as $k2 => $arChildItem2) {
														$arExtClass = array(
															"mobilemenu-lev2__link",
														);
														if (isset($arChildItem2['ITEMS'])) {
															$arExtClass[] = "mobilemenu-lev2__link_parent";
														}
														if ($arChildItem2['SELECTED']) {
															$arExtClass[] = "mobilemenu-lev2__link_selected";
														}
														$arExtClass2 = array(
															"mobilemenu-lev2__item",
														);
														if ($arChildItem2['SELECTED']) {
															$arExtClass2[] = "mobilemenu-lev2__item_selected";
														}
														?>
														<li class="<?= implode(" ", $arExtClass2) ?>">
															<a href="<?= $arChildItem2["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem2['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem2["TEXT"] ?></a>
															<?
															if (isset($arChildItem2['ITEMS'])) {
																?>
																<ul class="mobilemenu-lev3">
																	<?
																	foreach ($arChildItem2['ITEMS'] as $k3 => $arChildItem3) {
																		$arExtClass = array(
																			"mobilemenu-lev3__link",
																		);
																		if (isset($arChildItem3['ITEMS'])) {
																			$arExtClass[] = "mobilemenu-lev3__link_parent";
																		}
																		if ($arChildItem3['SELECTED']) {
																			$arExtClass[] = "mobilemenu-lev3__link_selected";
																		}
																		$arExtClass3 = array(
																			"mobilemenu-lev3__item",
																		);
																		if ($arChildItem3['SELECTED']) {
																			$arExtClass2[] = "mobilemenu-lev3__item_selected";
																		}
																		?>
																		<li class="<?= implode(" ", $arExtClass2) ?>">
																			<a href="<?= $arChildItem3["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem3['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem3["TEXT"] ?></a>
																			<?
																			if (isset($arChildItem3['ITEMS'])) {
																				?>
																				<ul class="mobilemenu-lev4">
																					<?
																					foreach ($arChildItem3['ITEMS'] as $k4 => $arChildItem4) {
																						$arExtClass = array(
																							"mobilemenu-lev4__link",
																						);
																						if ($arChildItem4['SELECTED']) {
																							$arExtClass[] = "mobilemenu-lev4__link_selected";
																						}
																						?>
																						<li class="mobilemenu-lev4__item">
																							<a href="<?= $arChildItem4["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem4['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem4["TEXT"] ?></a>
																						</li>
																						<?
																					}
																					?>
																				</ul>
																				<?
																			}
																			?>
																		</li>
																		<?
																	}
																	?>
																</ul>
																<?
															}
															?>
														</li>
														<?
													}
													?>
												</ul>
												<?
											}
											?>
										</li>
										<?
									}
									?>
								</ul>
							</div>
						</div>
					</div>
					<?
				}
				?>
			</li>
			<?
		}
		?>
	</ul>
</nav>
