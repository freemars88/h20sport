<?

$arNewResult = array();
$arLinks = array();
foreach ($arResult as $arItem)
{
	if ($arItem['DEPTH_LEVEL'] == 1)
	{
		$arNewResult[] = $arItem;
		$arLinks = array(1 => &$arNewResult[count($arNewResult) - 1]);
	} else
	{
		$arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS'][] = $arItem;
		$arLinks[$arItem['DEPTH_LEVEL']] = &$arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS'][count($arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS']) - 1];
	}
}
$arResult = array("ITEMS" => $arNewResult);
