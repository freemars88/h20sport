<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult))
{
	return;
}
$iIndex = 0;
foreach ($arResult['ITEMS'] as $arItem)
{
	$iIndex++;
	if (empty($arItem['ITEMS']))
	{
		?>
		<li>
			<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
		</li>
		<?
	} else
	{
		?>
		<li>
			<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
			<div class="mega-menu menu-scroll">
				<div class="table">
					<div class="table-cell">
						<?
						$isChildExists = false;
						//	myPrint($arItem['ITEMS']);
						foreach ($arItem['ITEMS'] as $arChildItem)
						{
							if (isset($arChildItem['ITEMS']) && count($arChildItem['ITEMS']) > 0)
							{
								$isChildExists = true;
							}
						}
						if ($isChildExists)
						{
							foreach ($arItem['ITEMS'] as $arChildItem)
							{
								?>
								<div class="half-width">
									<ul>
										<li class="menu-title"><a href="<?= $arChildItem["LINK"] ?>"><?= $arChildItem["TEXT"] ?></a></li>
										<?
										foreach ($arChildItem['ITEMS'] as $arSubChildItem)
										{
											?>
											<li><a href="<?= $arSubChildItem["LINK"] ?>"><?= $arSubChildItem["TEXT"] ?></a></li>
											<?
										}
										?>
									</ul>
								</div>
								<?
							}
						} else
						{
							?>
							<div class="half-width">
								<ul>
									<?
									foreach ($arItem['ITEMS'] as $arChildItem)
									{
										?>

										<li><a href="<?= $arChildItem["LINK"] ?>"><?= $arChildItem["TEXT"] ?></a></li>
										<?
									}
									?>
								</ul>
							</div>
							<?
						}
						?>
						<? /*
						  <div class="full-width">
						  <div class="mega-menu-img">
						  <a href="single-product.html"><img src="<?= SITE_TEMPLATE_PATH ?>/img/megamenu/1.jpg" alt="" /></a>
						  </div>
						  </div>
						 */ ?>
						<div class="pb-80"></div>
					</div>
				</div>
			</div>
		</li>
		<?
	}
}