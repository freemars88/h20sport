<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult)) {
	return;
}
?>
<nav class="menu-mainpage-left<?= ($arParams['IS_MAINPAGE'] == "Y" ? "" : " menu-mainpage-left__mt0") ?>">
	<ul class="menu-mainpage-left__items">
		<?
		foreach ($arResult['ITEMS'] as $arItem) {
			$arExtClass = array(
				"menu-mainpage-left__item-link",
			);
			if ($arItem['SELECTED']) {
				$arExtClass[] = "menu-mainpage-left__item-link_selected";
			}
			?>
			<li class="menu-mainpage-left__item">
				<a href="<?= $arItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>"><?= $arItem["TEXT"] ?></a>
				<?
						/*
				if ($arItem['SELECTED'] && !empty($arItem['ITEMS'])) {
					?>
					<ul class="menu-mainpage-left2__items">
						<?
						foreach ($arItem['ITEMS'] as $arItem2) {
							$arExtClass = array(
								"menu-mainpage-left2__item-link",
							);
							if ($arItem2['SELECTED']) {
								$arExtClass[] = "menu-mainpage-left2__item-link_selected";
							}
							?>
							<li class="menu-mainpage-left2__item">
								<a href="<?= $arItem2["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>"><?= $arItem2["TEXT"] ?></a>
								<?
								if ($arItem2['SELECTED'] && !empty($arItem2['ITEMS'])) {
									?>
									<ul class="menu-mainpage-left3__items">
										<?
										foreach ($arItem2['ITEMS'] as $arItem3) {
											$arExtClass = array(
												"menu-mainpage-left3__item-link",
											);
											if ($arItem3['SELECTED']) {
												$arExtClass[] = "menu-mainpage-left3__item-link_selected";
											}
											?>
											<li class="menu-mainpage-left3__item">
												<a href="<?= $arItem3["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>"><?= $arItem3["TEXT"] ?></a>
											</li>
											<?
										}
										?>
									</ul>
									<?
								}
								?>
							</li>
							<?
						}
						?>
					</ul>
					<?
				}
				*/
				?>
			</li>
			<?
		}
		?>
	</ul>
</nav>
