<?

$arNewResult = array();
$arLinks = array();

foreach ($arResult as $arItem) {
	if ($arItem['DEPTH_LEVEL'] == 1) {
		$arNewResult[] = $arItem;
		$arLinks = array(1 => &$arNewResult[count($arNewResult) - 1]);
	} else {
		$arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS'][] = $arItem;
		$arLinks[$arItem['DEPTH_LEVEL']] = &$arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS'][count($arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS']) - 1];
	}
}
$arResult = array("ITEMS" => $arNewResult);
if (!function_exists("doCheckMenuShowLeftMenu")) {

	function doCheckMenuShowLeftMenu($arMenu)
	{
		foreach ($arMenu as $k => $arItem) {
			if ($arItem['PARAMS']['FROM_IBLOCK'] && array_key_exists("UF_TOPMENU", $arItem['PARAMS'])) {
				if (!$arItem['PARAMS']['UF_TOPMENU']) {
					unset($arMenu[$k]);
					continue;
				}
			}
			if (array_key_exists("ITEMS", $arItem) && !empty($arItem['ITEMS'])) {
				$arRes = doCheckMenuShowLeftMenu($arItem['ITEMS']);
				if (is_array($arRes) && !empty($arRes)) {
					$arMenu[$k]['ITEMS'] = $arRes;
				} else {
					unset($arMenu[$k]['ITEMS']);
				}
			}
		}
		return $arMenu;
	}

}
$arResult['ITEMS'] = doCheckMenuShowLeftMenu($arResult['ITEMS']);

if (!function_exists("doMakeMenuLeftOnlySelected")) {
	function doMakeMenuLeftOnlySelected($arMenu)
	{
		foreach ($arMenu as $k=>$v)
		{
			if ($v['SELECTED'])
			{
				if (!empty($v['ITEMS']))
				{
					return doMakeMenuLeftOnlySelected($v['ITEMS']);
				} else {
					break;
				}
			}
		}
		return $arMenu;
	}
}

$arResult['ITEMS'] = doMakeMenuLeftOnlySelected($arResult['ITEMS']);
/*
foreach ($arResult['ITEMS'] as $k => $v) {
	if ($v['LINK'] == "/catalog/") {
		$arResult['ITEMS'] = $v['ITEMS'];
		break;
	}
}
$arResult['ITEMS'] = array_values($arResult['ITEMS']);
*/
//myPrint($arResult['ITEMS']);
/*
CModule::IncludeModule("advertising");
$by = "s_weight";
$order = "asc";
$is_filtered = false;
$arFilter = array(
	"LAMP" => "green",
	"TYPE_SID" => "TOP_MENU_BANNER_%",
	"SITE" => SITE_ID
);
$rBanners = CAdvBanner::GetList($by, $order, $arFilter, $is_filtered, "N");
while ($arBanner = $rBanners->Fetch())
{
	$arResult["EXISTS_BANNERS"][] = substr($arBanner['TYPE_SID'], 16);
}
*/