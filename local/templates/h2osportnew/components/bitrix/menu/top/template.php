<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult)) {
	return;
}
?>

<nav class="menu-top">
	<ul class="menu-top__items">
		<?
		foreach ($arResult['ITEMS'] as $arItem) {
			$arExtClass = array(
				"menu-top__item-link",
			);
			if (!empty($arItem['ITEMS'])) {
				$arExtClass[] = "menu-top__item-link_child";
			}
			?>
			<li class="menu-top__item">
				<a href="<?= $arItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>"><?= $arItem["TEXT"] ?></a>
				<?
				if (!empty($arItem['ITEMS'])) {
					$arLevel2 = array();
					$arLevel3 = array();
					$arLevel4 = array();
					?>
					<div class="menu-top-popup">
						<div class="menu-top-popup-content">
							<div class="menu-top-popup-content__level1">
								<ul class="popupmenu-lev1">
									<?
									foreach ($arItem['ITEMS'] as $k => $arChildItem) {
										if (isset($arChildItem['ITEMS'])) {
											$arLevel2[$arChildItem['PARAMS']['SECTION_ID']] = $arChildItem['ITEMS'];
										}
										$arExtClass = array(
											"popupmenu-lev1__link",
										);
										if ($arChildItem['SELECTED']) {
											$arExtClass[] = "popupmenu-lev1__link_selected";
										}
										?>
										<li class="popupmenu-lev1__item">
											<a href="<?= $arChildItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem["TEXT"] ?></a>
										</li>
										<?
									}
									?>
								</ul>
							</div>
							<div class="menu-top-popup-content__level2">
								<?
								foreach ($arLevel2 as $k => $arChildItems) {
									?>
									<ul data-sid="<?= $k ?>" class="popupmenu-lev2">
										<?
										foreach ($arChildItems as $k1 => $arChildItem) {
											if (isset($arChildItem['ITEMS'])) {
												$arLevel3[$arChildItem['PARAMS']['SECTION_ID']] = $arChildItem['ITEMS'];
											}
											$arExtClass = array(
												"popupmenu-lev2__link",
											);
											if ($arChildItem['SELECTED']) {
												$arExtClass[] = "popupmenu-lev2__link_selected";
											}
											?>
											<li class="popupmenu-lev2__item">
												<a href="<?= $arChildItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem["TEXT"] ?></a>
											</li>
											<?
										}
										?>
									</ul>
									<?
								}
								?>
							</div>
							<div class="menu-top-popup-content__level3">
								<?
								foreach ($arLevel3 as $k => $arChildItems) {
									?>
									<ul data-sid="<?= $k ?>" class="popupmenu-lev3">
										<?
										foreach ($arChildItems as $k1 => $arChildItem) {
											if (isset($arChildItem['ITEMS'])) {
												$arLevel4[$arChildItem['PARAMS']['SECTION_ID']] = $arChildItem['ITEMS'];
											}
											$arExtClass = array(
												"popupmenu-lev3__link",
											);
											if ($arChildItem['SELECTED']) {
												$arExtClass[] = "popupmenu-lev3__link_selected";
											}
											?>
											<li class="popupmenu-lev3__item">
												<a href="<?= $arChildItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem["TEXT"] ?></a>
											</li>
											<?
										}
										?>
									</ul>
									<?
								}
								?>
							</div>
							<div class="menu-top-popup-content__level4">
								<?
								foreach ($arLevel4 as $k => $arChildItems) {
									?>
									<ul data-sid="<?= $k ?>" class="popupmenu-lev4">
										<?
										foreach ($arChildItems as $k1 => $arChildItem) {
											$arExtClass = array(
												"popupmenu-lev4__link",
											);
											if ($arChildItem['SELECTED']) {
												$arExtClass[] = "popupmenu-lev4__link_selected";
											}
											?>
											<li class="popupmenu-lev4__item">
												<a href="<?= $arChildItem["LINK"] ?>" class="<?= implode(" ", $arExtClass) ?>" data-sid="<?= $arChildItem['PARAMS']['SECTION_ID'] ?>"><?= $arChildItem["TEXT"] ?></a>
											</li>
											<?
										}
										?>
									</ul>
									<?
								}
								?>
							</div>
						</div>
					</div>
					<?
				}
				?>
			</li>
			<?
		}
		?>
	</ul>
</nav>
