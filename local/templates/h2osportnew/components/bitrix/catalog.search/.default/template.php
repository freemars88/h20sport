<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
	<div class="col-60 pb-20">
		<div class="row">
			<div class="col-60">
				<? $APPLICATION->IncludeComponent(
					"bitrix:search.title",
					"catalog",
					Array(
						"CATEGORY_0" => array("iblock_catalog"),
						"CATEGORY_0_TITLE" => "Каталог",
						"CATEGORY_0_iblock_catalog" => array("16", "17"),
						"CHECK_DATES" => "N",
						"CONTAINER_ID" => "title-search",
						"CONVERT_CURRENCY" => "N",
						"INPUT_ID" => "title-search-input",
						"NUM_CATEGORIES" => "1",
						"ORDER" => "date",
						"PAGE" => "#SITE_DIR#search/index.php",
						"PREVIEW_HEIGHT" => "75",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PREVIEW_WIDTH" => "75",
						"PRICE_CODE" => array("BASE", "OLDPRICE"),
						"PRICE_VAT_INCLUDE" => "Y",
						"SHOW_INPUT" => "Y",
						"SHOW_OTHERS" => "N",
						"SHOW_PREVIEW" => "Y",
						"TOP_COUNT" => "10",
						"USE_LANGUAGE_GUESS" => "N",
						"QUERY" => $_REQUEST['q'],
					)
				);
				$arElements = $APPLICATION->IncludeComponent(
					"bitrix:search.page", "notview", Array(
					"RESTART" => $arParams["RESTART"],
					"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
					"USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
					"CHECK_DATES" => $arParams["CHECK_DATES"],
					"arrFILTER" => array("iblock_" . $arParams["IBLOCK_TYPE"]),
					"arrFILTER_iblock_" . $arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
					"USE_TITLE_RANK" => "N",
					"DEFAULT_SORT" => "rank",
					"FILTER_NAME" => "",
					"SHOW_WHERE" => "N",
					"arrWHERE" => array(),
					"SHOW_WHEN" => "N",
					"PAGE_RESULT_COUNT" => 50,
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "N",
				), $component, array('HIDE_ICONS' => 'Y')
				); ?>
			</div>
			<div class="col-60">
				<?
				if (!empty($arElements) && is_array($arElements)) {
					global $searchFilter;
					$searchFilter = array(
						"=ID" => $arElements,
					);
					ob_start();
					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section", ".default", array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
						"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
						"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
						"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
						"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
						"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
						"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
						"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
						"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
						"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
						"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
						"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
						"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
						"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
						"SECTION_URL" => $arParams["SECTION_URL"],
						"DETAIL_URL" => $arParams["DETAIL_URL"],
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
						"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
						"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
						"CURRENCY_ID" => $arParams["CURRENCY_ID"],
						"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
						"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"FILTER_NAME" => "searchFilter",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_USER_FIELDS" => array(),
						"INCLUDE_SUBSECTIONS" => "Y",
						"SHOW_ALL_WO_SECTION" => "Y",
						"META_KEYWORDS" => "",
						"META_DESCRIPTION" => "",
						"BROWSER_TITLE" => "",
						"ADD_SECTIONS_CHAIN" => "N",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
					), $arResult["THEME_COMPONENT"], array('HIDE_ICONS' => 'Y')
					);
					$strContent = ob_get_contents();
					ob_end_clean();
					while (strpos($strContent, "#VOTE_") !== false) {
						$start = strpos($strContent, "#VOTE_");
						$end = strpos($strContent, "#", $start + 6);
						$id = substr($strContent, $start + 6, $end - $start - 6);
						ob_start();
						$APPLICATION->IncludeComponent("bitrix:iblock.vote", "single", Array(
							"EXTCLASS" => "floatright",
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"DISPLAY_AS_RATING" => "rating",
							"ELEMENT_CODE" => "",
							"ELEMENT_ID" => $id,
							"IBLOCK_ID" => $arParams['IBLOCK_ID'],
							"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
							"MAX_VOTE" => "5",
							"MESSAGE_404" => "",
							"SET_STATUS_404" => "N",
							"SHOW_RATING" => "N",
							"VOTE_NAMES" => array(
								0 => "1",
								1 => "2",
								2 => "3",
								3 => "4",
								4 => "5",
								5 => "",
							),
						), false
						);
						$vote = ob_get_contents();
						ob_end_clean();
						$strContent = str_replace("#VOTE_" . $id . "#", $vote, $strContent);
					}
					echo $strContent;
				} elseif (is_array($arElements)) {
					echo GetMessage("CT_BCSE_NOT_FOUND");
				}
				?>
			</div>
		</div>
	</div>
</div>

