<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<div class="catalog-pagenav">
	<div class="catalog-pagenav__seealso">
		<?
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
			?>
			<div class="catalog-pagenav__seealso-area">
				<a href="javascript:void(0)" data-href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>" class="catalog-pagenav__seealso-link catalog-showmore">Показать
					еще</a>
			</div>
			<div class="catalog-pagenav__seealso-loader">
				<img src="<?= SITE_TEMPLATE_PATH ?>/images/30.gif"/>
			</div>
			<?
		}
		?>
	</div>
	<div class="catalog-pagenav__pagination">
		<ul class="catalog-pagenav-pagination">
			<?
			if ($arResult["NavPageNomer"] > 1) {
				if ($arResult["bSavePage"]) {
					?>
					<li class="catalog-pagenav-pagination__item">
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_prev"></a>
					</li>
					<li class="catalog-pagenav-pagination__item">
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_first">1</a>
					</li>
					<?
				} else {
					if ($arResult["NavPageNomer"] > 2) {
						?>
						<li class="catalog-pagenav-pagination__item">
							<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_prev"></a>
						</li>
						<?
					} else {
						?>
						<li class="catalog-pagenav-pagination__item">
							<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_prev"></a>
						</li>
					<? } ?>
					<li class="catalog-pagenav-pagination__item">
						<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="catalog-pagenav-pagination__item-link">1</a></li>
				<? }

				if ($arResult["NavPageCount"] > 5 && $arResult["NavPageNomer"] > 3) {
					?>
					<li class="catalog-pagenav-pagination__item"><span class="catalog-pagenav-pagination__item-more">...</span></li>
					<?
				}
			} else {
				?>
				<li class="catalog-pagenav-pagination__item catalog-pagenav-pagination__item">
					<a href="javascript:void(0)" class="catalog-pagenav-pagination__item-link"></a></li>
				<li class="catalog-pagenav-pagination__item catalog-pagenav-pagination__item_active">
					<a href="javascript:void(0)" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_active">1</a></li>
				<?
			}

			$arResult["nStartPage"]++;
			while ($arResult["nStartPage"] <= $arResult["nEndPage"] - 1) {
				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) {
					?>
					<li class="catalog-pagenav-pagination__item catalog-pagenav-pagination__item_active">
						<a href="javascript:void(0)" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_active"><?= $arResult["nStartPage"] ?></a></li>
					<?
				} else {
					?>
					<li class="catalog-pagenav-pagination__item">
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>" class="catalog-pagenav-pagination__item-link"><?= $arResult["nStartPage"] ?></a>
					</li>
					<?
				}
				$arResult["nStartPage"]++;
			}
			if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
				if ($arResult["NavPageCount"] > 5 && $arResult["NavPageNomer"] < ($arResult["NavPageCount"] - 2)) {
					?>
					<li class="catalog-pagenav-pagination__item"><span class="catalog-pagenav-pagination__item-more">...</span></li>
					<?
				}
				if ($arResult["NavPageCount"] > 1) {
					?>
					<li class="catalog-pagenav-pagination__item">
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>" class="catalog-pagenav-pagination__item-link"><?= $arResult["NavPageCount"] ?></a>
					</li>
				<? } ?>
				<li class="catalog-pagenav-pagination__item">
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_next"></a>
				</li>
				<?
			} else {
				if ($arResult["NavPageCount"] > 1) {
					?>
					<li class="catalog-pagenav-pagination__item catalog-pagenav-pagination__item_active">
						<a href="javascript:void(0)" class="catalog-pagenav-pagination__item-link catalog-pagenav-pagination__item-link_active"><?= $arResult["NavPageCount"] ?></a></li>
				<? }
				?>
				<li class="catalog-pagenav-pagination__item catalog-pagenav-pagination__item">
					<a href="javascript:void(0)" class="catalog-pagenav-pagination__item-link"></a></li>
			<? } ?>
		</ul>
	</div>
</div>
