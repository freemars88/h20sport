<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="articles">
	<div class="row">
		<?
		foreach ($arResult["ITEMS"] as $arItem) {
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			//$arPhoto = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 320, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true);
			$strTS = MakeTimeStamp($arItem['ACTIVE_FROM'], CSite::GetDateFormat());
			$arMonths = array(
				1 => "Января",
				2 => "Февраля",
				3 => "Марта",
				4 => "Апреля",
				5 => "Мая",
				6 => "Июня",
				7 => "Июля",
				8 => "Августа",
				9 => "Сентября",
				10 => "Октября",
				11 => "Ноября",
				12 => "Декабря",
			);
			?>
			<div class="col-60 col-lg-30 col-xl-60">
				<div class="articles-item" style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>);">
					<div class="articles-item__data">
						<span><?= date("d", $strTS); ?> <?= $arMonths[intval(date("m", $strTS))] ?> <?= date("Y", $strTS); ?></span>
					</div>
					<div class="articles-item__content">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="articles-item__content-name"><?= $arItem["NAME"] ?></a></a>
						<div class="articles-item__content-text"><?= $arItem["PREVIEW_TEXT"] ?></div>
					</div>
					<div class="articles-item__detail">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="articles-item__detail-button">Подробнее</a>
					</div>
				</div>
			</div>
			<?
		}
		?>
	</div>
</div>
