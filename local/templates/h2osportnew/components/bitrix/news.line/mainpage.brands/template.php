<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="brand-cards">
<?
foreach ($arResult["ITEMS"] as $arItem)
{
	?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$arPhoto = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 270, 'height' => 120), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	?>
	<div class="brand-card">
		<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" target="_blank" title="<?= $arItem["NAME"] ?>" class="brand-card__link"><img src="<?= $arPhoto['src'] ?>" alt="" class="brand-card__img" /></a>
	</div>
	<?
}
?>
</div>
