<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="blog-area blog-2 blog-details-area pt-80 pb-80">
	<div class="container">	
		<div class="blog">
			<div class="row">
				<div class="col-md-3">
					<aside class="widget widget-search mb-30">
						<form action="/search/">
							<input type="text" placeholder="Поиск..." name="q" />
							<button type="submit" name="search">
								<i class="zmdi zmdi-search"></i>
							</button>
						</form>
					</aside>
					<aside class="widget widget-categories  mb-30">
						<div class="widget-title">
							<div class="h4">Разделы</div>
						</div>
						<div id="cat-treeview" class="widget-info product-cat">
							<?
							$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.menu.left", Array(
								"CACHE_TIME" => "3600", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"IBLOCK_ID" => 16,
								"SECTION_ID" => "",
								"SECTION_CODE" => "",
								"SECTION_CODE_PATH" => "",
								"IS_CATALOG"=>"Y"
									), false
							);
							?>
						</div>
					</aside>
					<aside class="widget widget-product mb-30">
						<div class="widget-title">
							<div class="h4">Последние новости</div>
						</div>
						<div class="widget-info sidebar-product clearfix">
							<?
							$APPLICATION->IncludeComponent("bitrix:news.line", "sidebar", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
								"CACHE_GROUPS" => "Y", // Учитывать права доступа
								"CACHE_TIME" => "300", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
								"FIELD_CODE" => array(// Поля
									0 => "NAME",
									1 => "PREVIEW_TEXT",
									2 => "PREVIEW_PICTURE",
									3 => "",
								),
								"IBLOCKS" => array(// Код информационного блока
									0 => "28",
								),
								"IBLOCK_TYPE" => "news", // Тип информационного блока
								"NEWS_COUNT" => "5", // Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM", // Поле для первой сортировки новостей
								"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
								"SORT_ORDER1" => "DESC", // Направление для первой сортировки новостей
								"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
									), false
							);
							?>
						</div>
					</aside>
					<?
					$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.banner", Array(
						"CACHE_TIME" => "3600", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"CATALOG_IBLOCK_ID" => 16,
						"IBLOCK_ID" => "26",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_CODE_PATH" => "",
						"ELEMENT_ID" => false
							), false
					);
					?>
				</div>
				<div class="col-md-9">
					<?
					$ElementID = $APPLICATION->IncludeComponent(
							"bitrix:news.detail", "", Array(
						"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
						"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"META_KEYWORDS" => $arParams["META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
						"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
						"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"SET_TITLE" => "Y",
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
						"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
						"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
						"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
						"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
						"CHECK_DATES" => $arParams["CHECK_DATES"],
						"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
						"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
						"SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],
						"USE_SHARE" => $arParams["USE_SHARE"],
						"SHARE_HIDE" => $arParams["SHARE_HIDE"],
						"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
						"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
						"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
						"USE_RATING" => $arParams["USE_RATING"],
						"MAX_VOTE" => $arParams["MAX_VOTE"],
						"VOTE_NAMES" => $arParams["VOTE_NAMES"],
						"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
						"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
						"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
							), $component
					);
					?>
				</div>
			</div>
		</div>
	</div>
</div>


