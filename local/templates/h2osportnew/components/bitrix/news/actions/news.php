<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arResult["VARIABLES"] = array(
	"ELEMENT_ID" => 108177
);
?>
<div class="blog-area blog-2 blog-details-area pt-80 pb-80 product-style-2">
	<div class="container">	
		<div class="blog">
			<div class="row">
				<div class="col-md-3">
					<aside class="widget widget-search mb-30">
						<form action="/search/">
							<input type="text" placeholder="Поиск..." name="q" />
							<button type="submit" name="search">
								<i class="zmdi zmdi-search"></i>
							</button>
						</form>
					</aside>
					<aside class="widget widget-categories  mb-30">
						<div class="widget-title">
							<div class="h4">Разделы</div>
						</div>
						<div id="cat-treeview" class="widget-info product-cat">
							<?
							$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.menu.left", Array(
								"CACHE_TIME" => "3600", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"IBLOCK_ID" => 16,
								"SECTION_ID" => "",
								"SECTION_CODE" => "",
								"SECTION_CODE_PATH" => "",
								"IS_CATALOG"=>"Y"
									), false
							);
							?>
						</div>
					</aside>
					<aside class="widget widget-product mb-30">
						<div class="widget-title">
							<div class="h4">Последние новости</div>
						</div>
						<div class="widget-info sidebar-product clearfix">
							<?
							$APPLICATION->IncludeComponent("bitrix:news.line", "sidebar", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
								"CACHE_GROUPS" => "Y", // Учитывать права доступа
								"CACHE_TIME" => "300", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
								"FIELD_CODE" => array(// Поля
									0 => "NAME",
									1 => "PREVIEW_TEXT",
									2 => "PREVIEW_PICTURE",
									3 => "",
								),
								"IBLOCKS" => array(// Код информационного блока
									0 => "28",
								),
								"IBLOCK_TYPE" => "news", // Тип информационного блока
								"NEWS_COUNT" => "5", // Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM", // Поле для первой сортировки новостей
								"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
								"SORT_ORDER1" => "DESC", // Направление для первой сортировки новостей
								"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
									), false
							);
							?>
						</div>
					</aside>
					<?
					$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.banner", Array(
						"CACHE_TIME" => "3600", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"CATALOG_IBLOCK_ID" => 16,
						"IBLOCK_ID" => "26",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_CODE_PATH" => "",
						"ELEMENT_ID" => false
							), false
					);
					?>
				</div>
				<div class="col-md-9">
					<?
					$ElementID = $APPLICATION->IncludeComponent(
							"bitrix:news.detail", "", Array(
						"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
						"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"META_KEYWORDS" => $arParams["META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
						"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
						"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"SET_TITLE" => "Y",
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
						"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
						"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
						"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
						"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
						"CHECK_DATES" => $arParams["CHECK_DATES"],
						"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
						"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
						"SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],
						"USE_SHARE" => $arParams["USE_SHARE"],
						"SHARE_HIDE" => $arParams["SHARE_HIDE"],
						"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
						"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
						"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
						"USE_RATING" => $arParams["USE_RATING"],
						"MAX_VOTE" => $arParams["MAX_VOTE"],
						"VOTE_NAMES" => $arParams["VOTE_NAMES"],
						"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
						"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
						"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
							), $component
					);
					if ($ElementID > 0)
					{
						$arElements = array("-1");
						$arNewElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams['IBLOCK_ID']), false, false, array("ID", "PROPERTY_PRODUCTS", "PROPERTY_PRODUCTS_AUTO"))->Fetch();
						global $searchFilter;
						$searchFilter = array(
							"=ID" => array_merge($arElements, $arNewElement['PROPERTY_PRODUCTS_VALUE'], $arNewElement['PROPERTY_PRODUCTS_AUTO_VALUE']),
						);
						$arSectionParams = array(
							"ACTION_VARIABLE" => "action",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"BASKET_URL" => "/personal/cart/",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"CHECK_DATES" => "N",
							"CONVERT_CURRENCY" => "N",
							"DETAIL_URL" => "",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"DISPLAY_COMPARE" => "N",
							"DISPLAY_TOP_PAGER" => "N",
							"ELEMENT_SORT_FIELD" => "shows",
							"ELEMENT_SORT_FIELD2" => "id",
							"ELEMENT_SORT_ORDER" => "asc",
							"ELEMENT_SORT_ORDER2" => "desc",
							"HIDE_NOT_AVAILABLE" => "Y",
							"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
							"IBLOCK_ID" => "16",
							"IBLOCK_TYPE" => "catalog",
							"LINE_ELEMENT_COUNT" => "3",
							"NO_WORD_LOGIC" => "N",
							"OFFERS_CART_PROPERTIES" => array(),
							"OFFERS_FIELD_CODE" => array("", ""),
							"OFFERS_LIMIT" => "0",
							"OFFERS_PROPERTY_CODE" => array("CML2_LINK", ""),
							"OFFERS_SORT_FIELD" => "sort",
							"OFFERS_SORT_FIELD2" => "id",
							"OFFERS_SORT_ORDER" => "asc",
							"OFFERS_SORT_ORDER2" => "desc",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => "catalog",
							"PAGER_TITLE" => "Товары",
							"PAGE_ELEMENT_COUNT" => "30",
							"PRICE_CODE" => array("BASE", "OLDPRICE"),
							"PRICE_VAT_INCLUDE" => "Y",
							"PRODUCT_ID_VARIABLE" => "id",
							"PRODUCT_PROPERTIES" => array(),
							"PRODUCT_PROPS_VARIABLE" => "prop",
							"PRODUCT_QUANTITY_VARIABLE" => "quantity",
							"PROPERTY_CODE" => array("CML2_ARTICLE", ""),
							"RESTART" => "N",
							"SECTION_ID_VARIABLE" => "SECTION_ID",
							"SECTION_URL" => "",
							"SHOW_PRICE_COUNT" => "1",
							"USE_LANGUAGE_GUESS" => "N",
							"USE_PRICE_COUNT" => "N",
							"USE_PRODUCT_QUANTITY" => "N"
						);
						ob_start();
						$APPLICATION->IncludeComponent(
								"bitrix:catalog.section", ".default", array(
							"IBLOCK_TYPE" => $arSectionParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arSectionParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => $arSectionParams["ELEMENT_SORT_FIELD"],
							"ELEMENT_SORT_ORDER" => $arSectionParams["ELEMENT_SORT_ORDER"],
							"ELEMENT_SORT_FIELD2" => $arSectionParams["ELEMENT_SORT_FIELD2"],
							"ELEMENT_SORT_ORDER2" => $arSectionParams["ELEMENT_SORT_ORDER2"],
							"PAGE_ELEMENT_COUNT" => $arSectionParams["PAGE_ELEMENT_COUNT"],
							"LINE_ELEMENT_COUNT" => $arSectionParams["LINE_ELEMENT_COUNT"],
							"PROPERTY_CODE" => $arSectionParams["PROPERTY_CODE"],
							"OFFERS_CART_PROPERTIES" => $arSectionParams["OFFERS_CART_PROPERTIES"],
							"OFFERS_FIELD_CODE" => $arSectionParams["OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arSectionParams["OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arSectionParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arSectionParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arSectionParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arSectionParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arSectionParams["OFFERS_LIMIT"],
							"SECTION_URL" => $arSectionParams["SECTION_URL"],
							"DETAIL_URL" => $arSectionParams["DETAIL_URL"],
							"BASKET_URL" => $arSectionParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arSectionParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arSectionParams["PRODUCT_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arSectionParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arSectionParams["PRODUCT_PROPS_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arSectionParams["SECTION_ID_VARIABLE"],
							"CACHE_TYPE" => $arSectionParams["CACHE_TYPE"],
							"CACHE_TIME" => $arSectionParams["CACHE_TIME"],
							"DISPLAY_COMPARE" => $arSectionParams["DISPLAY_COMPARE"],
							"PRICE_CODE" => $arSectionParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arSectionParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arSectionParams["SHOW_PRICE_COUNT"],
							"PRICE_VAT_INCLUDE" => $arSectionParams["PRICE_VAT_INCLUDE"],
							"PRODUCT_PROPERTIES" => $arSectionParams["PRODUCT_PROPERTIES"],
							"USE_PRODUCT_QUANTITY" => $arSectionParams["USE_PRODUCT_QUANTITY"],
							"CONVERT_CURRENCY" => $arSectionParams["CONVERT_CURRENCY"],
							"CURRENCY_ID" => $arSectionParams["CURRENCY_ID"],
							"HIDE_NOT_AVAILABLE" => $arSectionParams["HIDE_NOT_AVAILABLE"],
							"HIDE_NOT_AVAILABLE_OFFERS" => $arSectionParams["HIDE_NOT_AVAILABLE_OFFERS"],
							"DISPLAY_TOP_PAGER" => $arSectionParams["DISPLAY_TOP_PAGER"],
							"DISPLAY_BOTTOM_PAGER" => $arSectionParams["DISPLAY_BOTTOM_PAGER"],
							"PAGER_TITLE" => $arSectionParams["PAGER_TITLE"],
							"PAGER_SHOW_ALWAYS" => $arSectionParams["PAGER_SHOW_ALWAYS"],
							"PAGER_TEMPLATE" => $arSectionParams["PAGER_TEMPLATE"],
							"PAGER_DESC_NUMBERING" => $arSectionParams["PAGER_DESC_NUMBERING"],
							"PAGER_DESC_NUMBERING_CACHE_TIME" => $arSectionParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
							"PAGER_SHOW_ALL" => $arSectionParams["PAGER_SHOW_ALL"],
							"FILTER_NAME" => "searchFilter",
							"SECTION_ID" => "",
							"SECTION_CODE" => "",
							"SECTION_USER_FIELDS" => array(),
							"INCLUDE_SUBSECTIONS" => "Y",
							"SHOW_ALL_WO_SECTION" => "Y",
							"META_KEYWORDS" => "",
							"META_DESCRIPTION" => "",
							"BROWSER_TITLE" => "",
							"ADD_SECTIONS_CHAIN" => "N",
							"SET_TITLE" => "N",
							"SET_STATUS_404" => "N",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "N",
								), $component, array('HIDE_ICONS' => 'Y')
						);
						$strContent = ob_get_contents();
						ob_end_clean();
						while (strpos($strContent, "#VOTE_") !== false)
						{
							$start = strpos($strContent, "#VOTE_");
							$end = strpos($strContent, "#", $start + 6);
							$id = substr($strContent, $start + 6, $end - $start - 6);
							ob_start();
							$APPLICATION->IncludeComponent("bitrix:iblock.vote", "single", Array(
								"EXTCLASS" => "floatright",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"DISPLAY_AS_RATING" => "rating",
								"ELEMENT_CODE" => "",
								"ELEMENT_ID" => $id,
								"IBLOCK_ID" => $arSectionParams['IBLOCK_ID'],
								"IBLOCK_TYPE" => $arSectionParams['IBLOCK_TYPE'],
								"MAX_VOTE" => "5",
								"MESSAGE_404" => "",
								"SET_STATUS_404" => "N",
								"SHOW_RATING" => "N",
								"VOTE_NAMES" => array(
									0 => "1",
									1 => "2",
									2 => "3",
									3 => "4",
									4 => "5",
									5 => "",
								)
									), false
							);
							$vote = ob_get_contents();
							ob_end_clean();
							$strContent = str_replace("#VOTE_" . $id . "#", $vote, $strContent);
						}
						echo $strContent;
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?
/*
?>
<div class="blog-area blog-2 pt-80 pb-80">
	<div class="container">	
		<div class="blog">
			<div class="row">
				<div class="col-md-3">
					<aside class="widget widget-search mb-30">
						<form action="/search/">
							<input type="text" placeholder="Поиск..." name="q" />
							<button type="submit" name="search">
								<i class="zmdi zmdi-search"></i>
							</button>
						</form>
					</aside>
					<aside class="widget widget-categories  mb-30">
						<div class="widget-title">
							<h4>Разделы</h4>
						</div>
						<div id="cat-treeview" class="widget-info product-cat">
							<?
							$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.menu.left", Array(
								"CACHE_TIME" => "3600", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"IBLOCK_ID" => 16,
								"SECTION_ID" => "",
								"SECTION_CODE" => "",
								"SECTION_CODE_PATH" => ""
									), false
							);
							?>
						</div>
					</aside>
					<aside class="widget widget-product mb-30">
						<div class="widget-title">
							<h4>Последние новости</h4>
						</div>
						<div class="widget-info sidebar-product clearfix">
							<?
							$APPLICATION->IncludeComponent("bitrix:news.line", "sidebar", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
								"CACHE_GROUPS" => "Y", // Учитывать права доступа
								"CACHE_TIME" => "300", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
								"FIELD_CODE" => array(// Поля
									0 => "NAME",
									1 => "PREVIEW_TEXT",
									2 => "PREVIEW_PICTURE",
									3 => "",
								),
								"IBLOCKS" => array(// Код информационного блока
									0 => "28",
								),
								"IBLOCK_TYPE" => "news", // Тип информационного блока
								"NEWS_COUNT" => "5", // Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM", // Поле для первой сортировки новостей
								"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
								"SORT_ORDER1" => "DESC", // Направление для первой сортировки новостей
								"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
									), false
							);
							?>
						</div>
					</aside>
					<?
					$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.banner", Array(
						"CACHE_TIME" => "3600", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"CATALOG_IBLOCK_ID" => 16,
						"IBLOCK_ID" => "26",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_CODE_PATH" => "",
						"ELEMENT_ID" => false
							), false
					);
					?>
				</div>
				<div class="col-md-9">
					<?
					$APPLICATION->IncludeComponent(
							"bitrix:news.list", "", Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"NEWS_COUNT" => $arParams["NEWS_COUNT"],
						"SORT_BY1" => $arParams["SORT_BY1"],
						"SORT_ORDER1" => $arParams["SORT_ORDER1"],
						"SORT_BY2" => $arParams["SORT_BY2"],
						"SORT_ORDER2" => $arParams["SORT_ORDER2"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"CHECK_DATES" => $arParams["CHECK_DATES"],
						"IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
						"SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
						"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
						"SET_TITLE" => $arParams["SET_TITLE"],
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
						"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
						"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
						"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
						"USE_RATING" => $arParams["USE_RATING"],
						"DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
						"MAX_VOTE" => $arParams["MAX_VOTE"],
						"VOTE_NAMES" => $arParams["VOTE_NAMES"],
						"USE_SHARE" => $arParams["LIST_USE_SHARE"],
						"SHARE_HIDE" => $arParams["SHARE_HIDE"],
						"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
						"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
						"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
							), $component
					);
					?>
				</div>
			</div>
		</div>
	</div>
</div>
*/