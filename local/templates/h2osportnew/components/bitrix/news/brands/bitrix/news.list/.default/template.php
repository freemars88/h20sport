<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
	?>
	<div id="catalog-section-items">
		<div class="row">
			<?
			foreach ($arResult['ITEMS'] as $arItem) {
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				$arPhoto = false;
				if ($arItem['PREVIEW_PICTURE']['ID'] > 0) {
					$arPhoto = $arItem['PREVIEW_PICTURE'];
				}
				if ($arPhoto) {
					$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 320, 'height' => 200), BX_RESIZE_IMAGE_EXACT_ALT, true);
					doCheckPhotoToSize($_SERVER['DOCUMENT_ROOT'] . $arPhoto['src'], 320, 200);
					$arPhoto['src'] = str_replace(".png", ".jpg", $arPhoto['src']);
				} else {
					$arPhoto = array(
						"src" => SITE_TEMPLATE_PATH . "/img/empty.320.200.jpg",
					);
				}
				?>
				<div class="col-60 col-md-30 col-lg-20">
					<div class="product-card">
						<div class="product-card__inner">
							<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="product-card__image-link">
								<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" class="product-card__image"/>
							</a>
							<div class="product-card__info">
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="product-card__name"><?= $arItem['NAME'] ?></a>
							</div>
							<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						</div>
					</div>
				</div>
				<?
			}
			?>
		</div>
	</div>
	<?
	if (strlen($arResult['NAV_STRING']) > 0) {
		?>
		<div id="catalog-section-navs">
			<div class="row">
				<div class="col-60">
					<?
					echo $arResult['NAV_STRING'];
					?>
				</div>
			</div>
		</div>
		<?
	}
}
