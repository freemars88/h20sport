<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arPhoto = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width' => 1090, 'height' => 450), BX_RESIZE_IMAGE_EXACT, true);
?>


<?
foreach ($arResult['PROPERTIES']['CATALOG']['VALUE'] as $key => $catalogId) {
	$tmpFile = "/upload/catalogPreview/{$catalogId}.jpg";
	$tmpFileDest = $_SERVER['DOCUMENT_ROOT'] . $tmpFile;
	$arCatalogPhoto = CFile::ResizeImageFile(getPreviewPdf(CFile::GetPath($catalogId)), $tmpFileDest, array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL);
?>
<a href="<?=CFile::GetPath($catalogId)?>" target="_blank"><img src="<?=$tmpFile?>" alt="<?=$arResult['PROPERTIES']['CATALOG']['DESCRIPTION'][$key]?>" title="<?=$arResult['PROPERTIES']['CATALOG']['DESCRIPTION'][$key]?>" /></a>

<?}?>

<div class="single-blog mb-30" id="<? echo $this->GetEditAreaId($arResult['ID']) ?>">
	<div class="blog-photo">
		<img src="<?= $arPhoto['src'] ?>" alt="" />
		<? /*
		  <div class="like-share fix">
		  <a href="#"><i class="zmdi zmdi-account"></i><span>James</span></a>
		  <a href="#"><i class="zmdi zmdi-favorite"></i><span>89 Like</span></a>
		  <a href="#"><i class="zmdi zmdi-comments"></i><span>59 Comments</span></a>
		  <a href="#"><i class="zmdi zmdi-share"></i><span>29 Share</span></a>
		  </div> */ ?>
	</div>
	<div class="blog-info blog-details-info">
			<?= $arResult['DETAIL_TEXT'] ?>
	</div>
</div>