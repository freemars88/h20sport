<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="articles" id="catalog-section-items">
	<div class="row">
		<?
		$i = 0;
		foreach ($arResult["ITEMS"] as $arItem) {
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			//$arPhoto = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 320, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true);
			$strTS = MakeTimeStamp($arItem['ACTIVE_FROM'], CSite::GetDateFormat());
			$arMonths = array(
				1 => "Января",
				2 => "Февраля",
				3 => "Марта",
				4 => "Апреля",
				5 => "Мая",
				6 => "Июня",
				7 => "Июля",
				8 => "Августа",
				9 => "Сентября",
				10 => "Октября",
				11 => "Ноября",
				12 => "Декабря",
			);
			if ($i == 0 || $i == 3) {
				$nCol = 40;
			} else {
				$nCol = 20;
			}
			?>
			<div class="col-60 col-md-30 col-lg-<?= $nCol ?>">
				<div class="articles-item" style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>);">
					<div class="articles-item__data">
						<span><?= date("d", $strTS); ?> <?= $arMonths[intval(date("m", $strTS))] ?> <?= date("Y", $strTS); ?></span>
					</div>
					<div class="articles-item__content">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="articles-item__content-name"><?= $arItem["NAME"] ?></a></a>
						<div class="articles-item__content-text"><?= $arItem["PREVIEW_TEXT"] ?></div>
					</div>
					<div class="articles-item__detail">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="articles-item__detail-button">Подробнее</a>
					</div>
				</div>
			</div>
			<? /*
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
			<div class="single-blog mb-30">
				<div class="blog-photo">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arPhoto['src'] ?>" alt=""/></a>
				</div>
				<div class="blog-info">
					<div class="post-meta fix">
						<div class="post-date floatleft"><span class="text-dark-red"><?= date("d", $strTS); ?></span>
						</div>
						<div class="post-year floatleft">
							<p class="text-uppercase text-dark-red mb-0"><?= $arMonths[intval(date("m", $strTS))] ?>
								, <?= date("Y", $strTS); ?></p>
							<h4 class="post-title">
								<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" tabindex="0"><?= $arItem["NAME"] ?></a></h4>
						</div>
					</div>
					<p><?= $arItem["PREVIEW_TEXT"] ?></p>
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="button-2 text-dark-red">Подробнее...</a>
				</div>
			</div>
		</div>
	<?
*/
			$i++;
			if ($i >= 4) {
				$i = 0;
			}
		} ?>
	</div>
</div>
<div id="catalog-section-navs">
	<div class="row">
		<div class="col-md-60">
			<?= $arResult["NAV_STRING"] ?>
		</div>
	</div>
</div>