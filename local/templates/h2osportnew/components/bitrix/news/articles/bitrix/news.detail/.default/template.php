<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//$arPhoto = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width' => 1090, 'height' => 450), BX_RESIZE_IMAGE_EXACT, true);
$strTS = MakeTimeStamp($arResult['ACTIVE_FROM'], CSite::GetDateFormat());
$arMonths = array(
	1 => "Января",
	2 => "Февраля",
	3 => "Марта",
	4 => "Апреля",
	5 => "Мая",
	6 => "Июня",
	7 => "Июля",
	8 => "Августа",
	9 => "Сентября",
	10 => "Октября",
	11 => "Ноября",
	12 => "Декабря",
);
$picture = $arResult['DETAIL_PICTURE'];
if (empty($picture)) {
	$picture = $arResult['PREVIEW_PICTURE'];
}
if (strpos($arResult['DETAIL_TEXT'], 'height=') !== false) {
	$source = mb_convert_encoding($arResult['DETAIL_TEXT'], 'HTML-ENTITIES', 'utf-8');
	$oDom = new DOMDocument("", "utf-8");
	$oDom->loadHTML($source);
	$oPath = new DOMXPath($oDom);
	$oNodes = $oPath->query("//img");
	for ($i = 0; $i < $oNodes->length; $i++) {
		$oImg = $oNodes->item($i);
		$oAttr = $oImg->attributes;
		for ($j = 0; $j <= $oAttr->length; $j++) {
			$oCurAttr = $oAttr->item($j);
			if ($oCurAttr->nodeName == "height") {
				$oCurAttr->nodeValue = "";
			}
		}
	}
	$arResult['DETAIL_TEXT'] = $oDom->saveHTML();
}
?>
<div class="article-detail" id="<? echo $this->GetEditAreaId($arResult['ID']) ?>">
	<div class="row">
		<div class="col-md-60">
			<div class="article-detail__banner" style="background-image:url(<?= $picture['SRC'] ?>);">
				<div class="article-detail__banner-data">
					<span><?= date("d", $strTS); ?> <?= $arMonths[intval(date("m", $strTS))] ?> <?= date("Y", $strTS); ?></span>
				</div>
				<div class="article-detail__banner-content">
					<h1><?= $arResult['NAME'] ?></h1>
				</div>
			</div>
		</div>
		<div class="col-60 col-xl-40">
			<div class="article-detail__content">
				<?= $arResult['DETAIL_TEXT'] ?>
			</div>
		</div>
		<div class="col-60 col-xl-20">
			<div class="article-detail__lates">
				<h2>Последние статьи</h2>
				<?
				$APPLICATION->IncludeComponent("bitrix:news.line", "sidebar", Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
					"CACHE_GROUPS" => "Y", // Учитывать права доступа
					"CACHE_TIME" => "300", // Время кеширования (сек.)
					"CACHE_TYPE" => "A", // Тип кеширования
					"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
					"FIELD_CODE" => array(// Поля
						0 => "NAME",
						1 => "PREVIEW_TEXT",
						2 => "PREVIEW_PICTURE",
						3 => "",
					),
					"IBLOCKS" => array(// Код информационного блока
						0 => "30",
					),
					"IBLOCK_TYPE" => "news", // Тип информационного блока
					"NEWS_COUNT" => "3", // Количество новостей на странице
					"SORT_BY1" => "ACTIVE_FROM", // Поле для первой сортировки новостей
					"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
					"SORT_ORDER1" => "DESC", // Направление для первой сортировки новостей
					"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
				), false
				);
				?>
			</div>
		</div>
	</div>
</div>
