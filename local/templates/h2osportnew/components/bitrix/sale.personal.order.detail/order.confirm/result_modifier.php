<?
$isExistsPresaleProducts = false;
foreach ($arResult['BASKET'] as $k => $arBasketItem) {
	$arItemProduct = CIBlockElement::GetList(array(), array("ID" => $arBasketItem['PRODUCT_ID'], "IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "NAME", "PROPERTY_SIZE", "PROPERTY_SIZE.NAME", "PROPERTY_COLOR", "PROPERTY_COLOR.NAME", "PROPERTY_CML2_LINK", "CATALOG_QUANTITY"))->Fetch();
	if ($arItemProduct) {
		$arItemProduct['MAIN'] = CIBlockElement::GetList(array(), array("ID" => $arItemProduct['PROPERTY_CML2_LINK_VALUE'], "IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "PROPERTY_CML2_ARTICLE", "PROPERTY_SYS_NAME_GENERATED", "PROPERTY_BRAND", "PREVIEW_PICTURE", "DETAIL_PICTURE"))->Fetch();
	}

	$arResult['BASKET'][$k]['ELEMENT'] = $arItemProduct;
}
foreach ($arResult['BASKET'] as $k => $arBasketItem) {
	$arElement = getElementData($arBasketItem['PRODUCT_ID']);
	$arElCatalog = CIBlockElement::GetList(array(), array("ID" => $arBasketItem['PRODUCT_ID']), false, false, array("ID", "CATALOG_QUANTITY"))->Fetch();
	if ($arElement['PROPERTIES']['CML2_LINK']['VALUE'] > 0) {
		$arElement = getElementData($arElement['PROPERTIES']['CML2_LINK']['VALUE']);
	}
	if ($arElement['PROPERTIES']['IS_PRESALE']['VALUE'] == "Y" && $arElCatalog['CATALOG_QUANTITY'] <= 0) {
		$isExistsPresaleProducts = true;
		break;
	}
}


$arResult['IS_PRESALE_PRODUCTS'] = $isExistsPresaleProducts;

$arResult['ALLOW_PAYMENT'] = true;
if ($arResult['PAY_SYSTEM_ID'] == PAYSYSTEM_CARD_IN_SITE_ID && $isExistsPresaleProducts) {
	$arResult['ALLOW_PAYMENT'] = false;
}