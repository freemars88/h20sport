<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

if (!empty($arResult['ERRORS']['FATAL'])) {
	foreach ($arResult['ERRORS']['FATAL'] as $error) {
		ShowError($error);
	}

	$component = $this->__component;

	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
} else {
	if (!empty($arResult['ERRORS']['NONFATAL'])) {
		foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
			ShowError($error);
		}
	}
	?>
	<div class="order-detail-info">
		<div class="row row-order-detail-header">
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-15">Заказ</div>
			<div class="col-md-15">Дата заказа</div>
			<div class="col-md-15">Статус</div>
			<div class="col-md-12">Сумма</div>
		</div>
		<div class="row row-order-detail-content">
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-15"><?= $arResult["ACCOUNT_NUMBER"] ?></div>
			<div class="col-md-15"><?= $arResult["DATE_INSERT_FORMATED"] ?></div>
			<div class="col-md-15"><?= $arResult['STATUS']['NAME'] ?></div>
			<div class="col-md-12"><?= $arResult["PRICE_FORMATED"] ?></div>
		</div>
		<div class="row row-order-detail-basket">
			<table class="basket-table">
				<thead>
				<tr>
					<th class="basket-table__header basket-table__header_photo">Фото</th>
					<th class="basket-table__header basket-table__header_name">Наименование товара</th>
					<th class="basket-table__header basket-table__header_article">Артикул</th>
					<th class="basket-table__header basket-table__header_size">Размер</th>
					<th class="basket-table__header basket-table__header_color">Цвет</th>
					<th class="basket-table__header basket-table__header_cnt">Количество</th>
					<th class="basket-table__header basket-table__header_price">Цена</th>
				</tr>
				</thead>
				<tbody>
				<?
				foreach ($arResult['BASKET'] as $arItem) {
					?>
					<tr>
						<td class="basket-table__body basket-table__body_photo">
							<a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>">
								<?
								if (strlen($arItem["PICTURE"]['SRC']) > 0) {
									$url = $arItem["PICTURE"]['SRC'];
								} else {
									$url = SITE_TEMPLATE_PATH . "/img/empty.150.150.jpg";
								}
								if (strlen($arItem["DETAIL_PAGE_URL"]) > 0) {
									?>
									<img width="150" height="150" src="<?= $url ?>" alt=""/>
									<?
								} else {
									?>
									<img width="150" height="150" src="<?= $url ?>" alt=""/>
									<?
								}
								?>
							</a>
						</td>
						<td class="basket-table__body basket-table__body_name">
							<a class="basket-table__body-link" href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['NAME'] ?></a>
						</td>
						<td class="basket-table__body basket-table__body_article"><?= $arItem['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'] ?></td>
						<td class="basket-table__body basket-table__body_size"><?= $arItem['ELEMENT']['PROPERTY_SIZE_NAME'] ?></td>
						<td class="basket-table__body basket-table__body_color"><?= $arItem['ELEMENT']['PROPERTY_COLOR_NAME'] ?></td>
						<td class="basket-table__body basket-table__body_cnt">
							<?
							$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
							$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
							$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
							?>
							<div class="basket-table__body-plusminus">
								<input type="text" size="3" maxlength="18" value="<?= $arItem["QUANTITY"] ?>" data-max="<?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?>" class="basket_qnt basket-table__body-plusminus-box" data-title="Доступно для покупки - <?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?> шт." name="QUANTITY_INPUT[<?= $arItem["ID"] ?>]" disabled="disabled"/>
							</div>
						</td>
						<td class="basket-table__body basket-table__body_price">
							<?
							if ($arItem['DISCOUNT_PRICE'] != 0) {
								?>
								<span class="oldprice"><?= $arItem['FULL_PRICE_FORMATED'] ?></span>
								<?= $arItem["PRICE_FORMATED"] ?>
								<?
							} else {
								echo $arItem["PRICE_FORMATED"];
							}
							?>
						</td>
					</tr>
					<?
				}
				?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-60">

			</div>
			<div class="col-md-60">
				<?
				foreach ($arResult['PAYMENT'] as $payment) {
					?>
					<?= $payment['PAY_SYSTEM_NAME'] ?>
					<?
					if ($arResult['ALLOW_PAYMENT']) {
						if ($payment['PAY_SYSTEM']["IS_CASH"] !== "Y") {
							if ($payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] === 'Y' && $arResult["IS_ALLOW_PAY"] !== "N") {
								?>
								<a href="<?= htmlspecialcharsbx($payment['PAY_SYSTEM']['PSA_ACTION_FILE']) ?>" target="_blank" class="button-one submit-button mt-15" data-text="Оформить заказ">Оплатить</a>
								<?
							}
						}
						if ($payment["PAID"] !== "Y" && $payment['PAY_SYSTEM']["IS_CASH"] !== "Y" && $payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] !== 'Y' && $arResult['CANCELED'] !== 'Y' && $arResult["IS_ALLOW_PAY"] !== "N") {
							?>
							<?= $payment['BUFFERED_OUTPUT'] ?>
							<?
						}
					}
				}
				?>
			</div>
		</div>
	</div>

	<?
}
