<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$rSizes = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_REFERENCE_SIZE, "ACTIVE" => "Y"), false, false, array("ID", "NAME"));
while ($arSize = $rSizes->Fetch()) {
	$arResult['SIZES'][$arSize['ID']] = $arSize;
}

if ($arResult['ID'] > 0) {
	global $arrFilter;

	if ($arrFilter['=PROPERTY_58']) {
		$brandTitle = [];
		foreach ($arrFilter['=PROPERTY_58'] as $brandId) {
			$brandTitle[] = CIBlockElement::GetByID(intval($brandId))->Fetch()['NAME'];
		}

		$arResult['BRANDS'] = implode(', ', $brandTitle);
	}

	$arSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arResult['IBLOCK_ID'], "ID" => $arResult['ID']), false, array("UF_H1"))->Fetch();
	$arResult['UF_H1'] = $arSection['UF_H1'];
}

$this->__component->setResultCacheKeys(array(
	"~DESCRIPTION",
	"~DESCRIPTION_TYPE",
	"UF_H1",
));

