<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES'])) {
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS'],
	),
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId . '_dsc_pict',
	'STICKER_ID' => $mainId . '_sticker',
	'BIG_SLIDER_ID' => $mainId . '_big_slider',
	'BIG_IMG_CONT_ID' => $mainId . '_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId . '_slider_cont',
	'OLD_PRICE_ID' => $mainId . '_old_price',
	'PRICE_ID' => $mainId . '_price',
	'DISCOUNT_PRICE_ID' => $mainId . '_price_discount',
	'PRICE_TOTAL' => $mainId . '_price_total',
	'SLIDER_CONT_OF_ID' => $mainId . '_slider_cont_',
	'QUANTITY_ID' => $mainId . '_quantity',
	'QUANTITY_DOWN_ID' => $mainId . '_quant_down',
	'QUANTITY_UP_ID' => $mainId . '_quant_up',
	'QUANTITY_MEASURE' => $mainId . '_quant_measure',
	'QUANTITY_LIMIT' => $mainId . '_quant_limit',
	'BUY_LINK' => $mainId . '_buy_link',
	'ADD_BASKET_LINK' => $mainId . '_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId . '_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId . '_not_avail',
	'COMPARE_LINK' => $mainId . '_compare_link',
	'TREE_ID' => $mainId . '_skudiv',
	'DISPLAY_PROP_DIV' => $mainId . '_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
	'OFFER_GROUP' => $mainId . '_set_group_',
	'BASKET_PROP_DIV' => $mainId . '_basket_prop',
	'SUBSCRIBE_LINK' => $mainId . '_subscribe',
	'TABS_ID' => $mainId . '_tabs',
	'TAB_CONTAINERS_ID' => $mainId . '_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId . '_small_card_panel',
	'TABS_PANEL_ID' => $mainId . '_tabs_panel',
);
$obName = $templateData['JS_OBJ'] = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'] : $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'] : $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]) ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']] : reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer) {
		if ($offer['MORE_PHOTO_COUNT'] > 1) {
			$showSliderControls = true;
			break;
		}
	}
} else {
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$oldPrice = false;
if (isset($arResult['PRICES']['OLDPRICE']) && $arResult['PRICES']['OLDPRICE']['VALUE'] >= 0) {
	$oldPrice = $arResult['PRICES']['OLDPRICE'];
	$price = doModifyItemPrice($price, $oldPrice);
}
$showDiscount = $price['PERCENT'] > 0;
foreach ($arResult['OFFERS'] as $k => $arOffer) {
	$priceOffer = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];
	$oldPriceOffer = false;
	if (isset($arOffer['PRICES']['OLDPRICE']) && $arOffer['PRICES']['OLDPRICE']['VALUE'] >= 0) {
		$oldPriceOffer = $arOffer['PRICES']['OLDPRICE'];
		$priceOffer = doModifyItemPrice($priceOffer, $oldPriceOffer);
	}
	$showDiscountOffer = $priceOffer['PERCENT'] > 0;
	$arResult['OFFERS_PRICES'][$arOffer['ID']] = array(
		"price" => $priceOffer,
		"oldPrice" => $oldPriceOffer,
		"showDiscount" => $showDiscountOffer,
		"CAN_BUY" => $arOffer['CAN_BUY'],
		"PRICE_TEXT" => ($arOffer['CAN_BUY'] ? $priceOffer['PRINT_RATIO_PRICE'] : "Товар продан"),
		"OLD_PRICE_TEXT" => ($arOffer['CAN_BUY'] ? $priceOffer['PRINT_RATIO_BASE_PRICE'] : ""),
		"SHOW_DISCOUNT" => $showDiscountOffer ? "Y" : "N",
	);
}
$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top',
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION'])) {
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos) {
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION'])) {
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos) {
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
	}
}
$arOffersVariants = array();
?>
<div class="row prodcard" itemscope itemtype="http://schema.org/Product">
	<div class="col-27 d-none d-lg-block">
		<div class="prodcard-photos">
			<div class="owl-carousel owl-theme prodcard-photos-carousel">
				<?
				$i = 0;
				foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $arPhoto) {
					$strOriginalPhoto = CFile::GetPath($arPhoto);
					$arPhoto = CFile::ResizeImageGet($arPhoto, array('width' => 580, 'height' => 705), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);

					?>
					<div style="text-align: center; display: block;">
						<a href="<?= $strOriginalPhoto ?>" class="prodcard-photos__link" data-index="<?= intval($i) ?>" data-fancybox="mainphoto">
							<img src="<?= $arPhoto['src'] ?>" alt="" itemprop="image" class="prodcard-photos__img"/>
						</a>
					</div>
					<?
					$i++;
				}
				?>
			</div>
			<div class="owl-carousel owl-theme prodcard-photos-carousel-title">
				<?
				$i = 0;
				foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $arPhoto) {
					$strDescription = $arResult['PROPERTIES']['MORE_PHOTOS']['DESCRIPTION'][$k];
					$strOriginalPhoto = CFile::GetPath($arPhoto);
					$arPhoto = CFile::ResizeImageGet($arPhoto, array('width' => 215, 'height' => 260), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					?>
					<div class="prodcard-photos__title<?= ($i == 0 ? ' prodcard-photos__title_active' : '') ?>" data-index="<?= intval($i) ?>">
						<img src="<?= $arPhoto['src'] ?>" alt="" class="prodcard-photos__title-img" itemprop="image"/>
						<?
						if (strlen($strDescription) > 0 || true) {
							?>
							<p><?= $strDescription ?></p>
							<?
						}
						?>
					</div>
					<?
					$i++;
				}
				?>
			</div>
		</div>
	</div>
	<div class="col-60 col-lg-33">
		<h1 itemprop="name"><?= $arResult['NAME'] ?></h1>
		<div class="prodcard__rating">
			#VOTE_<?= $arResult['ID'] ?>#
		</div>
		<div class="prodcard-photos d-lg-none">
			<div class="owl-carousel owl-theme prodcard-photos-mobile-carousel">
				<?
				$i = 0;
				foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $arPhoto) {
					$strOriginalPhoto = CFile::GetPath($arPhoto);
					$arPhoto = CFile::ResizeImageGet($arPhoto, array('width' => 215, 'height' => 260), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					?>
					<div class="prodcard-photos-mobile__title<?= ($i == 0 ? ' prodcard-photos-mobile__title_active' : '') ?>" data-index="<?= intval($i) ?>">
						<a href="<?= $strOriginalPhoto ?>" data-fancybox="mobile-photos">
							<img src="<?= $arPhoto['src'] ?>" alt="" class="prodcard-photos-mobile__title-img" itemprop="image"/>
						</a>
					</div>
					<?
					$i++;
				}
				?>
			</div>
		</div>
		<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<?
			$arVariantStat = array();
			ob_start();
			if ($arResult['PROPERTIES']['SET_IS_SET']['VALUE'] != "Y") {
				$iDefaultOffer = false;
				if ($arResult['IS_EXISTS_COLOR'] && $arResult['IS_EXISTS_SIZE']) {
					$strCurrentColor = "";
					$strCurrentSize = "";
					foreach ($arResult['TREE'] as $iColor => $arSizes) {
						$arColor = getElementData($iColor);
						$strCurrentColor = $arColor['NAME'];
						foreach ($arSizes as $iSize => $iOffer) {
							$arSize = getElementData($iSize);
							$strCurrentSize = $arSize['NAME'];
							break;
						}
						break;
					}


					foreach ($arResult['TREE'] as $iColor => $arSizes) {
						if ($iCurrentSize = array_search($_REQUEST['offerId'], $arResult['TREE'][$iColor])) {
							$iCurrentColor = $iColor;
							$arColor = getElementData($iColor);
							$strCurrentColor = $arColor['NAME'];
							$arSize = getElementData($iCurrentSize);
							$strCurrentSize = $arSize['NAME'];
							break;
						}
					}
					?>
					<div class="row">
						<div class="col-60 col-lg-30">
							<div class="prodcard-sel-color b-color-size-select">
								<div class="prodcard-sel-color__title">
									<div class="prodcard-sel-color__title-text">
										Цвет:
									</div>
									<div class="prodcard-sel-color__title-current">
										<?= $strCurrentColor ?>
									</div>
								</div>
								<ul class="prodcard-sel-color__list">
									<?
									$i = 1;
									foreach ($arResult['TREE'] as $iColor => $arSize) {
										$arColor = getElementData($iColor);
										$strStyle = "";
										if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0) {
											$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
										} elseif ($arColor['PREVIEW_PICTURE'] > 0) {
											$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
											$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
										}
										if ($i == 1) {
											$arVariantStat[] = "Цвет " . $arColor['NAME'];
										}
										$class = '';

										if ($i == 1 && !$iCurrentColor) {
											$class = ' prodcard-sel-color__item_active';
											$iDefaultOffer = $iOffer;
										} elseif ($iCurrentColor && $iCurrentColor == $iColor) {
											$class = ' prodcard-sel-color__item_active';
											$iDefaultOffer = $iOffer;
										}
										?>
										<li class="prodcard-sel-color__item<?=$class?>">
											<a href="javascript:void(0)" data-color-id="<?= $iColor ?>" data-title="<?= $arColor['NAME'] ?>" title="<?= $arColor['NAME'] ?>" class="prodcard-sel-color__link" style="<?= $strStyle ?>">&nbsp;</a>
										</li>
										<?
										$i++;
									}
									?>
								</ul>
							</div>
						</div>
						<div class="col-60 col-lg-30">
							<div class="prodcard-sel-size">
								<div class="prodcard-sel-size__title">
									<div class="prodcard-sel-size__title-text">
										Размер:
									</div>
									<div class="prodcard-sel-size__title-current"><?= $strCurrentSize ?></div>
								</div>
								<ul class="prodcard-sel-size__list">
									<?
									$i = 1;
									foreach ($arResult['TREE'] as $iColor => $arSizes) {
										$j = 1;
										foreach ($arSizes as $iSize => $iOffer) {
											$arSize = getElementData($iSize);
											if ($i == 1 && $j == 1) {
												$iDefaultOffer = $iOffer;
											}
											if ($i == 1 && $j == 1) {
												$arVariantStat[] = "Размер " . $arSize['NAME'];
											}

											$class = '';
											if ($i == 1 && $j == 1 && !$iCurrentSize) {
												$class = ' prodcard-sel-size__item_active';
												$iDefaultOffer = $iOffer;
											} elseif (
												$iCurrentSize &&
												$iCurrentColor &&
												$iCurrentSize == $iSize &&
												$iCurrentColor == $iColor
											) {
												$class = ' prodcard-sel-size__item_active';
												$iDefaultOffer = $iOffer;
											}

											?>
											<li class="prodcard-sel-size__item<?=$class?>" data-isoffer="1" data-color-id="<?= $iColor ?>"<?= ((($iColor != $iCurrentColor && $iCurrentColor) || (!$iCurrentColor && $i != 1)) ? ' style="display:none;"' : "") ?>>
												<a href="javascript:void(0)" data-offer-id="<?= $iOffer ?>" data-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['PRICE_TEXT'] ?>" data-old-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['OLD_PRICE_TEXT'] ?>" data-show-discount="<?= $arResult['OFFERS_PRICES'][$iOffer]['SHOW_DISCOUNT'] ?>" data-offer-quantity="<?= $arResult['OFFERS'][$iOffer]['CATALOG_QUANTITY'] ?>" class="prodcard-sel-size__link"><?= $arSize['NAME'] ?></a>
											</li>
											<?
											$j++;
										}
										$i++;
									}
									?>
								</ul>
							</div>
						</div>
					</div>
					<?
				} elseif ($arResult['IS_EXISTS_COLOR']) {
					$strCurrentColor = "";
					foreach ($arResult['TREE'] as $iColor => $arSizes) {
						$arColor = getElementData($iColor);
						$strCurrentColor = $arColor['NAME'];
						break;
					}

					foreach ($arResult['TREE'] as $iColor => $arSizes) {
						if ($iCurrentSize = array_search($_REQUEST['offerId'], $arResult['TREE'][$iColor])) {
							$iCurrentColor = $iColor;
							$arColor = getElementData($iColor);
							$strCurrentColor = $arColor['NAME'];
							$arSize = getElementData($iCurrentSize);
							$strCurrentSize = $arSize['NAME'];
							break;
						}
					}
					?>
					<div class="row">
						<div class="col-60">
							<div class="prodcard-sel-color b-only-color-select">
								<div class="prodcard-sel-color__title">
									<div class="prodcard-sel-color__title-text">
										Цвет:
									</div>
									<div class="prodcard-sel-color__title-current"><?= $strCurrentColor ?></div>
								</div>
								<ul class="prodcard-sel-color__list">
									<?
									$i = 1;
									foreach ($arResult['TREE'] as $iColor => $arOffer) {
										$iOffer = $arOffer['NOTREF'];
										if ($i == 1) {
											$iDefaultOffer = $iOffer;
										}
										$arColor = getElementData($iColor);
										$strStyle = "";
										if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0) {
											$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
										} elseif ($arColor['PREVIEW_PICTURE'] > 0) {
											$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
											$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
										}
										if ($i == 1) {
											$arVariantStat[] = "Цвет " . $arColor['NAME'];
										}

										$class = '';
										if ($i == 1 && !$iCurrentColor) {
											$class = ' prodcard-sel-color__item_active';
											$iDefaultOffer = $iOffer;
										} elseif ($iCurrentColor && $iCurrentColor == $iColor) {
											$class = ' prodcard-sel-color__item_active';
											$iDefaultOffer = $iOffer;
										}
										?>
										<li class="prodcard-sel-color__item<?=$class?>" data-isoffer="1" data-color-id="<?= $iColor ?>">
											<a href="javascript:void(0)" data-color-id="<?= $iColor ?>" data-title="<?= $arColor['NAME'] ?>" title="<?= $arColor['NAME'] ?>" data-offer-id="<?= $iOffer ?>" data-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['PRICE_TEXT'] ?>" data-old-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['OLD_PRICE_TEXT'] ?>" data-show-discount="<?= $arResult['OFFERS_PRICES'][$iOffer]['SHOW_DISCOUNT'] ?>" data-offer-quantity="<?= $arResult['OFFERS'][$iOffer]['CATALOG_QUANTITY'] ?>" class="prodcard-sel-color__link" style="<?= $strStyle ?>">&nbsp;</a>
										</li>
										<?
										$i++;
									}
									?>
								</ul>
							</div>
						</div>
					</div>
					<?
				} elseif ($arResult['IS_EXISTS_SIZE']) {
					$strCurrentSize = "";
					foreach ($arResult['TREE'] as $iColor => $arSizes) {
						foreach ($arSizes as $iSize => $iOffer) {
							$arSize = getElementData($iSize);
							$strCurrentSize = $arSize['NAME'];
							break;
						}
						break;
					}

					foreach ($arResult['TREE'] as $iColor => $arSizes) {
						if ($iCurrentSize = array_search($_REQUEST['offerId'], $arResult['TREE'][$iColor])) {
							$iCurrentColor = $iColor;
							$arColor = getElementData($iColor);
							$strCurrentColor = $arColor['NAME'];
							$arSize = getElementData($iCurrentSize);
							$strCurrentSize = $arSize['NAME'];
							break;
						}
					}
					?>
					<div class="row">
						<div class="col-60">
							<div class="prodcard-sel-size b-only-size-select">
								<div class="prodcard-sel-size__title">
									<div class="prodcard-sel-size__title-text">
										Размер:
									</div>
									<div class="prodcard-sel-size__title-current"><?= $strCurrentSize ?></div>
								</div>
								<ul class="prodcard-sel-size__list">
									<?
									$i = 1;
									foreach ($arResult['TREE'] as $iColor => $arSizes) {
										$j = 1;
										foreach ($arSizes as $iSize => $iOffer) {
											$arSize = getElementData($iSize);
											if ($i == 1 && $j == 1) {
												$iDefaultOffer = $iOffer;
											}
											if ($i == 1 && $j == 1) {
												$arVariantStat[] = "Размер " . $arSize['NAME'];
											}

											$class = '';
											if ($i == 1 && $j == 1 && !$iCurrentSize) {
												$class = ' prodcard-sel-size__item_active';
												$iDefaultOffer = $iOffer;
											} elseif ($iCurrentSize && $iCurrentSize == $iSize) {
												$class = ' prodcard-sel-size__item_active';
												$iDefaultOffer = $iOffer;
											}
											?>
											<li class="prodcard-sel-size__item<?=$class?>" data-isoffer="1" data-color-id="<?= $iColor ?>"<?= ($i != 1 ? ' style="display:none;"' : "") ?>>
												<a href="javascript:void(0)" data-offer-id="<?= $iOffer ?>" data-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['PRICE_TEXT'] ?>" data-old-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['OLD_PRICE_TEXT'] ?>" data-show-discount="<?= $arResult['OFFERS_PRICES'][$iOffer]['SHOW_DISCOUNT'] ?>" data-offer-quantity="<?= $arResult['OFFERS'][$iOffer]['CATALOG_QUANTITY'] ?>" class="prodcard-sel-size__link"><?= $arSize['NAME'] ?></a>
											</li>
											<?
											$j++;
										}
										$i++;
									}
									?>
								</ul>
							</div>
						</div>
					</div>
					<?
				} else {
					$ak = array_keys($arResult['OFFERS']);
					$iDefaultOffer = $ak[0];
				}
				$ak = array_keys($arResult['OFFERS']);
			}
			$contSelector = ob_get_contents();
			ob_end_clean();
			$strPresale = "Нет";
			if ($arResult['PROPERTIES']['TEXT_PRESALE']['VALUE'] > 0) {
				$arElementText = CIBlockElement::GetList(array(), array("ID" => $arResult['PROPERTIES']['TEXT_PRESALE']['VALUE']))->Fetch();
				if ($arElementText) {
					$strPresale = htmlspecialchars($arElementText['PREVIEW_TEXT']);
				}
			} elseif ($arResult['IS_STORE_OTHER']) {
				$strPresale = htmlspecialchars("Срок поставки от 3-х дней");
			}
			?>
			<div class="row">
				<div class="col-60 prodcard__mobile-prices d-lg-none">
					<span class="prodcard__price-title">Цена</span>
					<span class="prodcard__price"><?= $price['PRINT_RATIO_PRICE'] ?></span>
					<span class="prodcard__price-old" style="<?= ($showDiscount ? '' : 'display: none;') ?>"><?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?></span>
				</div>
				<div class="col-60 d-xl-none">
					<div class="prodcard__available">Наличие:
						<span data-presale="<?= $strPresale ?>" class="prodcard__available-cnt prodcard__available-cnt_<?= ($arResult['OFFERS'][$iDefaultOffer]['CATALOG_QUANTITY'] > 0 ? "yes" : "no") ?>"><?= ($arResult['OFFERS'][$iDefaultOffer]['CATALOG_QUANTITY'] > 0 ? "Есть" : $strPresale) ?></span>
					</div>
				</div>
			</div>
			<?= $contSelector ?>
			<div class="row mt35 d-none d-lg-flex">
				<div class="col-60 col-xl-32 prodcard-pricelg">
					<span class="prodcard__price-title">Цена</span>
					<span class="prodcard__price" itemprop="price" content="<?= $price['RATIO_PRICE'] ?>"><?= $price['PRINT_RATIO_PRICE'] ?></span>
					<span class="prodcard__price-old" style="<?= ($showDiscount ? '' : 'display: none;') ?>"><?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?></span>
					<meta itemprop="priceCurrency" content="RUB"/>
					<?
					/*
					if ($arResult['PROPERTIES']['IS_PRESALE']['VALUE'] == "Y" || $arResult['IS_STORE_OTHER']) {
						if ($arResult['PROPERTIES']['SET_IS_SET']['VALUE'] != "Y") {
							$iDefaultOffer = false;
							if ($arResult['IS_EXISTS_COLOR'] && $arResult['IS_EXISTS_SIZE']) {
								$i = 1;
								foreach ($arResult['TREE'] as $iColor => $arSizes) {
									$j = 1;
									foreach ($arSizes as $iSize => $iOffer) {
										if ($i == 1 && $j == 1) {
											$iDefaultOffer = $iOffer;
											break(2);
										}
										$j++;
									}
									$i++;
								}
							} elseif ($arResult['IS_EXISTS_COLOR']) {
								$i = 1;
								foreach ($arResult['TREE'] as $iSize => $arOffer) {
									$iOffer = $arOffer['NOTREF'];
									if ($i == 1) {
										$iDefaultOffer = $iOffer;
										break;
									}
								}
							} elseif ($arResult['IS_EXISTS_SIZE']) {
								$i = 1;
								foreach ($arResult['TREE']['NOTREF'] as $iSize => $iOffer) {
									if ($i == 1) {
										$iDefaultOffer = $iOffer;
										break;
									}
								}
							}
						}

						$strHide = "";
						if (($arResult['PROPERTIES']['IS_PRESALE']['VALUE'] == "Y" && $arResult['OFFERS'][$iDefaultOffer]['CATALOG_QUANTITY'] > 0) || ($arResult['IS_STORE_OTHER'] && !$arResult['OFFERS'][$iDefaultOffer]['IS_STORE_OTHER'])) {
							$strHide = ' style="display:none;"';
						}
						if (!$arResult['IS_STORE_OTHER']) {
							?>
							<div class="presale-note"<?= $strHide ?>>
								Товар под заказ
							</div>
							<?
						}
						if ($arResult['PROPERTIES']['TEXT_PRESALE']['VALUE'] > 0) {
							$arElementText = CIBlockElement::GetList(array(), array("ID" => $arResult['PROPERTIES']['TEXT_PRESALE']['VALUE']))->Fetch();
							if ($arElementText) {
								?>
								<div class="presale-text"<?= $strHide ?>>
									<?= $arElementText['PREVIEW_TEXT'] ?>
								</div>
								<?
							}
						} elseif ($arResult['IS_STORE_OTHER']) {
							?>
							<div class="presale-text"<?= $strHide ?>>
								Срок поставки от 3-х дней
							</div>
							<?
						}
					}
					*/
					?>
				</div>
				<div class="col-60 col-xl-28 prodcard-buyarea">
					<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" data-offer-id="<?= (count($arResult['OFFERS']) > 1 ? $iDefaultOffer : $arResult['OFFERS'][$ak[0]]['ID']) ?>" class="prodcard__buy" type="button">Добавить
						в корзину</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-60 col-xl-32 d-none d-xl-block">
				<div class="prodcard__available">Наличие:
					<span data-presale="<?= $strPresale ?>" class="prodcard__available-cnt prodcard__available-cnt_<?= ($arResult['OFFERS'][$iDefaultOffer]['CATALOG_QUANTITY'] > 0 ? "yes" : "no") ?>"><?= ($arResult['OFFERS'][$iDefaultOffer]['CATALOG_QUANTITY'] > 0 ? "Есть" : $strPresale) ?></span>
				</div>
			</div>
			<div class="col-60 d-lg-none prodcard-buyarea">
				<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" data-offer-id="<?= (count($arResult['OFFERS']) > 1 ? $iDefaultOffer : $arResult['OFFERS'][$ak[0]]['ID']) ?>" class="prodcard__buy">Добавить
					в корзину</a>
			</div>
			<div class="col-60 col-xl-28 prodcard-buyarea">
				<a href="javascript:void(0)" id="b-onelicklink" data-fancybox data-src="#oneclickbuy" title="Купить в 1 клик" class="prodcard__oneclickbuy">Купить
					в 1 клик</a>
				<div class="prodcard__oneclickbuy-form" id="oneclickbuy">
					<div class="prodcard__oneclickbuy-title">Купить в 1 клик</div>
					<form action="javascript:void(0)" id="popup-element-oneclick-form">
						<div class="row">
							<div class="col-15">
								<?
								foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $arPhoto) {
									$strOriginalPhoto = CFile::GetPath($arPhoto);
									$arPhoto = CFile::ResizeImageGet($arPhoto, array('width' => 140, 'height' => 170), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
									?>
									<img src="<?= $arPhoto['src'] ?>" alt="" class="prodcard__oneclickbuy-photo"/>
									<?
									break;
								}
								?>
							</div>
							<div class="col-45">
								<h2><?
									if (!empty($arResult['PARSE_NAME'])) {
										?>
										<?= $arResult['PARSE_NAME']['ARTICLE'] ?> <?= $arResult['PARSE_NAME']['PRODUCT_TYPE'] ?> <?= $arResult['PARSE_NAME']['NAME'] ?> <?= $arResult["COLOR_ITEMS"][$arResult['ID']]['COLOR_NAME'] ?>
										<?
									} else {
										echo $arResult['NAME'];
									}
									?></h2>
								<div class="prodcard__oneclickbuy-price">
									<span class="prodcard__price"><?= $price['PRINT_RATIO_PRICE'] ?></span>
								</div>
							</div>
						</div>
						<div class="row oneclickform-field">
							<div class="col-sm-15 oneclickform-field__title">
								ФИО
							</div>
							<div class="col-sm-45">
								<input type="text" class="oneclickform-field__field" id="oneclick-fio" placeholder="Введите ФИО">
							</div>
						</div>
						<div class="row oneclickform-field">
							<div class="col-sm-15 oneclickform-field__title">
								Телефон
							</div>
							<div class="col-sm-45">
								<input type="text" class="oneclickform-field__field" id="oneclick-phone" placeholder="Введите телефон">
							</div>
						</div>
						<button type="submit" onclick="yaCounter48772145.reachGoal('1clilck'); return true;" class="btn btn-primary product-oneclick-buy-button" data-id="">
							<span>Отправить заказ</span></button>
						<div class="error-text"></div>
						<div class="product-oneclick-buy-text">Оформите заказ и наш менеджер свяжется с вами в течении
							10 минут
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row d-flex">
			<div class="col-60 col-xl-20">
				<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в список желаний" data-id="<?= $arResult['ID'] ?>" class="product-card__action-button product-card__action-add-favorite add-favorite prodcard__favorite">В
					избранное</a>
			</div>
			<div class="col-60 col-xl-20 d-none d-sm-block">
				<?
				$APPLICATION->IncludeComponent("bitrix:main.share", "detail", Array(
					"HANDLERS" => array(// Используемые соц. закладки и сети
						0 => "vk",
						/*1 => "gplus",
						2 => "twitter",
						3 => "pinterest",*/
						4 => "facebook",
					),
					"PAGE_TITLE" => $arResult['NAME'], // Заголовок страницы
					"PAGE_URL" => $APPLICATION->GetCurPage(), // URL страницы относительно корня сайта
					"SHORTEN_URL_KEY" => "", // Ключ API для bit.ly
					"SHORTEN_URL_LOGIN" => "", // Логин для bit.ly
				), false
				);
				?>
			</div>
		</div>
		<?
		if (!empty($arResult['DISPLAY_PROPERTIES'])) {
			?>
			<div class="row">
				<?
				foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty) {
					/*if (in_array($arProperty['CODE'], array("BRAND"))) {
						continue;
					}*/
					if (!empty($arProperty['DISPLAY_VALUE'])) {
						?>
						<div class="col-60 col-xl-30 prodcard-prop">
							<label class="prodcard-prop__title"><?= $arProperty['NAME'] ?>:</label>
							<span class="prodcard-prop__val"><?
								if (in_array($arProperty['CODE'], array("BRAND"))) {
									echo(is_array($arProperty['DISPLAY_VALUE']) ? implode(' / ', $arProperty['DISPLAY_VALUE']) : $arProperty['DISPLAY_VALUE']);
								} else {
									echo strip_tags((is_array($arProperty['DISPLAY_VALUE']) ? implode(' / ', $arProperty['DISPLAY_VALUE']) : $arProperty['DISPLAY_VALUE']));
								}
								?></span>
						</div>
						<?
					}
				}
				?>
			</div>
		<? }
		?>
		<?
		if ($arResult['PROPERTIES']['SIZE_TABLE']['VALUE'] > 0) {
			$arSizeTable = getElementDataFull($arResult['PROPERTIES']['SIZE_TABLE']['VALUE']);
			?>
			<div class="row">
				<div class="col-60">
					<a href="javascript:void(0)" data-fancybox data-src="#sizetable" class="prodcard-size-table__link">Таблица
						размеров</a>
					<div class="prodcard-size-table" id="sizetable">
						<?
						if ($arSizeTable['PREVIEW_PICTURE'] > 0) {
							?>
							<img src="<?= CFile::GetPath($arSizeTable['PREVIEW_PICTURE']) ?>" style="margin-top:40px;"/>
							<?
						} else {
							echo $arSizeTable['~PREVIEW_TEXT'];
						}
						?>
					</div>
				</div>
			</div>
			<?
		}
		?>
		<div class="tabsblock">
			<div class="tabsblock__title">
				<ul class="tabsblock__title-list">
					<li class="tabsblock__title-item tabsblock__title-item_active">
						Описание
					</li>
					<li class="tabsblock__title-item">
						Оплата
					</li>
					<li class="tabsblock__title-item">
						Доставка
					</li>
				</ul>
			</div>
			<div class="tabsblock__content">
				<div class="tabsblock__content-item tabsblock__content-item_active">
					<?= $arResult['~DETAIL_TEXT'] ?>
				</div>
				<div class="tabsblock__content-item">
					<noindex>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:main.include", "", Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/include/element.pay.php",
							)
						);
						?>
					</noindex>
				</div>
				<div class="tabsblock__content-item">
					<noindex>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:main.include", "", Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/include/element.delivery.php",
							)
						);
						?>
					</noindex>
				</div>
			</div>
		</div>
	</div>
</div>
<?
$ar = array();
foreach ($arResult['SECTION']['PATH'] as $arPath) {
	$ar[] = $arPath['NAME'];
}
$arMainProduct = array(
	"id" => $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'],
	"name" => $arResult['NAME'],
	"price" => $price['PRICE'],
	"brand" => strip_tags($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']),
	"category" => implode("/", $ar),
);
$arOffersVariants = array(
	$arResult['ID'] => $arMainProduct,
);
foreach ($arResult['OFFERS'] as $arOffer) {
	$arVariant = array();
	if ($arOffer['PROPERTIES']['COLOR']['VALUE'] > 0) {
		$arColor = getElementData($arOffer['PROPERTIES']['COLOR']['VALUE']);
		$arVariant[] = "Цвет " . $arColor['NAME'];
	}
	if ($arOffer['PROPERTIES']['SIZE']['VALUE'] > 0) {
		$arSize = getElementData($arOffer['PROPERTIES']['SIZE']['VALUE']);
		$arVariant[] = "Размер " . $arSize['NAME'];
	}
	$arMainProduct['variant'] = implode(", ", $arVariant);
	$arOffersVariants[$arOffer['ID']] = $arMainProduct;
}
?>
<script type="text/javascript">
	<!--
	var productOffersECommerce =<?=CUtil::PhpToJSObject($arOffersVariants)?>;
	window.dataLayer.push({
		"ecommerce": {
			"currencyCode": "<?=$arResult['MIN_PRICE']['CURRENCY']?>",
			"detail": {
				"products": [
					{
						"id": "<?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?>",
						"name": "<?=$arResult['NAME']?>",
						"price": <?=$price['PRICE']?>,
						"brand": "<?=strip_tags($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE'])?>",
						"category": "<?
							$ar = array();
							foreach ($arResult['SECTION']['PATH'] as $arPath) {
								$ar[] = $arPath['NAME'];
							}
							echo implode("/", $ar);
							?>",
						"variant": "<?=implode(", ", $arVariantStat)?>"
					}
				]
			}
		}
	});
	-->
</script>
