<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
//$component = $this->getComponent();
$arNewOffers = array();
foreach ($arResult['OFFERS'] as $arOffer) {
	$arNewOffers[$arOffer['ID']] = $arOffer;
}

$arResult['OFFERS'] = $arNewOffers;
$arData = array();
$arResult['IS_EXISTS_COLOR'] = false;
$arResult['IS_EXISTS_SIZE'] = false;
foreach ($arResult['OFFERS'] as $arOffer) {
	$color = $arOffer['PROPERTIES']['COLOR']['VALUE'];
	$size = $arOffer['PROPERTIES']['SIZE']['VALUE'];
	if ($color <= 0) {
		$color = "NOTREF";
	} else {
		$arResult['IS_EXISTS_COLOR'] = true;
	}
	if ($size <= 0) {
		$size = "NOTREF";
	} else {
		$arResult['IS_EXISTS_SIZE'] = true;
	}
	$arData[$color][$size] = $arOffer['ID'];
}

$STORE_BASE_ALLOW = unserialize(STORE_BASE_ALLOW);

$arResult['IS_STORE_OTHER'] = false;
foreach ($arResult['OFFERS'] as $k => $arOffer) {
	$rStoreAmount = CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arOffer['ID']), false, false, array("STORE_ID", "AMOUNT"));
	$arCnt = array();
	while ($arStoreAmount = $rStoreAmount->Fetch()) {
		$arResult['OFFERS'][$k]['PRODUCT_STORES'][$arStoreAmount['STORE_ID']] = $arStoreAmount['AMOUNT'];
		if ($arStoreAmount['AMOUNT'] > 0) {
			if (in_array($arStoreAmount['STORE_ID'], $STORE_BASE_ALLOW)) {
				$arCnt['BASE'] += $arStoreAmount['AMOUNT'];
			} else {
				$arCnt['OTHER'] += $arStoreAmount['AMOUNT'];
			}
		}
	}
	if ($arCnt['BASE'] <= 0 && $arCnt['OTHER'] > 0) {
		$arResult['OFFERS'][$k]['IS_STORE_OTHER'] = true;
		$arResult['OFFERS'][$k]['CATALOG_QUANTITY'] = 0;
		$arResult['IS_STORE_OTHER'] = true;
	}
}

$arResult['TREE'] = $arData;
