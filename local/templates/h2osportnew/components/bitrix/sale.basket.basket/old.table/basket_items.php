<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */

/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0)
{
	?>
	<div class="block">
		<div class="product-list">
			<table class="table">
				<thead>
					<tr>
						<th class="">Товар</th>
						<th class="">Наименование</th>
						<th class="">Цена</th>
						<th class="">Кол-во</th>
						<th class="">Стоимость</th>
						<th class=""></th>
					</tr>
				</thead>
				<tbody>
					<?
					foreach ($arResult["GRID"]["ROWS"] as $k => $arItem)
					{
						if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
						{
							?>
							<tr data-id="<?= $arItem["ID"] ?>" data-item-name="<?= $arItem["NAME"] ?>" data-item-brand="<?= $arItem[$arParams['BRAND_PROPERTY'] . "_VALUE"] ?>" data-item-price="<?= $arItem["PRICE"] ?>" data-item-currency="<?= $arItem["CURRENCY"] ?>">
								<td class="">
									<div class="product-info">
										<?
										//myPrint($arItem);
										if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0)
										{
											$url = $arItem["PREVIEW_PICTURE_SRC"];
										} elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0)
										{
											$url = $arItem["DETAIL_PICTURE_SRC"];
										} else
										{
											$url = $templateFolder . "/images/no_photo.png";
										}

										if (strlen($arItem["DETAIL_PAGE_URL"]) > 0)
										{
											?>
											<img width="80" src="<?= $url ?>" alt="" />
											<?
										} else
										{
											?>
											<img width="80" src="<?= $url ?>" alt="" />
											<?
										}
										?>
									</div>
								</td>
								<td class="cart-name">
									<a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['ELEMENT']['MAIN']['BASKET_NAME'] ?></a>
									<p>
										Артикул: <strong><?= $arItem['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'] ?></strong>
										<span>Цвет: <strong><?= $arItem['ELEMENT']['MAIN']['PROPERTY_COLOR_NAME'] ?></strong><br/>
											Размер: <strong><?= (strlen($arItem['ELEMENT']['PROPERTY_SIZE_PROPERTY_NAME_FOR_KT_VALUE']) > 0 ? $arItem['ELEMENT']['PROPERTY_SIZE_PROPERTY_NAME_FOR_KT_VALUE'] : $arItem['ELEMENT']['PROPERTY_SIZE_NAME']) ?></strong></span>

									</p>
								</td>
								<td class="price-cell">
									<?
									if ($arItem['DISCOUNT_PRICE'] != 0)
									{
										?>
										<span class="oldprice"><?= doPriceChangeRub($arItem['FULL_PRICE_FORMATED']) ?></span>
										<?= doPriceChangeRub($arItem["PRICE_FORMATED"]) ?>
										<span class="discount-percent">скидка -<?= intval($arItem['DISCOUNT_PRICE_PERCENT']) ?>%</span>
										<?
									} else
									{
										echo doPriceChangeRub($arItem["PRICE_FORMATED"]);
									}
									?>
								</td>
								<td class="">
									<?
									$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
									$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
									$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
									?>
									<div class="product-quantity-slider">
										<input type="text" size="3" id="QUANTITY_INPUT_<?= $arItem["ID"] ?>" name="QUANTITY_INPUT[<?= $arItem["ID"] ?>]" maxlength="18" value="<?= $arItem["QUANTITY"] ?>" data-max="<?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?>" class="basket_qnt" data-title="Доступно для покупки - <?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?> шт." />
									</div>
								</td>
								<td class="priceTotalItem"><?= doPriceChangeRub($arItem["SUM"]) ?></td>
								<td class="">
									<a class="del-from-basket" href="javascript:void(0)" data-id="<?= $arItem["ID"] ?>"><i class="fa-icon-close"></i> Удалить</a>
								</td>
							</tr>
							<?
						}
					}
					?>
				</tbody>
			</table>
			<div class="row">
				<div class="col-md-3">
					<?
					if ($arParams["HIDE_COUPON"] != "Y")
					{
						?>
						<div class="form-group">
							<div class="input-group">
								<input type="text" id="coupon" name="COUPON" value="<?= $arResult['COUPON_LIST'][0]['COUPON'] ?>" onchange="enterCoupon();" placeholder="Промокод" class="form-control" />
								<div class="input-group-addon">
									<a href="javascript:void(0)" id="apply-coupon" title="Применить скидочную карту" class="btn-apply-coupon"><i class="linear-icon-check"></i></a>
								</div>
							</div>
						</div>
						<?
					}
					?>
				</div>
				<div class="col-md-3 offset-md-6 b-cart__total-price">
					Итоговая стоимость: <span id="allSum_FORMATED"><?= doPriceChangeRub($arResult['allSum_FORMATED']) ?></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="javascript:void(0)" class="btn btn-primary btn-make-order" id="checkout">Оформить заказ</a>
				</div>
			</div>
		</div>
	</div>
	<?
	/*
	  <div id="basket_items_list">
	  <input type="hidden" id="column_headers" value="<?= htmlspecialcharsbx(implode($arHeaders, ",")) ?>" />
	  <input type="hidden" id="offers_props" value="<?= htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ",")) ?>" />
	  <input type="hidden" id="action_var" value="<?= htmlspecialcharsbx($arParams["ACTION_VARIABLE"]) ?>" />
	  <input type="hidden" id="quantity_float" value="<?= ($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="price_vat_show_value" value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="auto_calculation" value="<?= ($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y" ?>" />

	  <div class="bx_ordercart_order_pay">

	  <div class="bx_ordercart_order_pay_left" id="coupons_block">
	  <?
	  if ($arParams["HIDE_COUPON"] != "Y")
	  {
	  ?>
	  <div class="bx_ordercart_coupon">
	  <span><?= GetMessage("STB_COUPON_PROMT") ?></span><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">&nbsp;<a class="bx_bt_button bx_big" href="javascript:void(0)" onclick="enterCoupon();" title="<?= GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?= GetMessage('SALE_COUPON_APPLY'); ?></a>
	  </div><?
	  if (!empty($arResult['COUPON_LIST']))
	  {
	  foreach ($arResult['COUPON_LIST'] as $oneCoupon)
	  {
	  $couponClass = 'disabled';
	  switch ($oneCoupon['STATUS'])
	  {
	  case DiscountCouponsManager::STATUS_NOT_FOUND:
	  case DiscountCouponsManager::STATUS_FREEZE:
	  $couponClass = 'bad';
	  break;
	  case DiscountCouponsManager::STATUS_APPLYED:
	  $couponClass = 'good';
	  break;
	  }
	  ?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?= htmlspecialcharsbx($oneCoupon['COUPON']); ?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
	  if (isset($oneCoupon['CHECK_CODE_TEXT']))
	  {
	  echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
	  }
	  ?></div></div><?
	  }
	  unset($couponClass, $oneCoupon);
	  }
	  } else
	  {
	  ?>&nbsp;<?
	  }
	  ?>
	  </div>
	  <div class="bx_ordercart_order_pay_right">
	  <table class="bx_ordercart_order_sum">
	  <? if ($bWeightColumn && floatval($arResult['allWeight']) > 0): ?>
	  <tr>
	  <td class="custom_t1"><?= GetMessage("SALE_TOTAL_WEIGHT") ?></td>
	  <td class="custom_t2" id="allWeight_FORMATED"><?= $arResult["allWeight_FORMATED"] ?>
	  </td>
	  </tr>
	  <? endif; ?>
	  <? if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"): ?>
	  <tr>
	  <td><? echo GetMessage('SALE_VAT_EXCLUDED') ?></td>
	  <td id="allSum_wVAT_FORMATED"><?= $arResult["allSum_wVAT_FORMATED"] ?></td>
	  </tr>
	  <?
	  $showTotalPrice = (float) $arResult["DISCOUNT_PRICE_ALL"] > 0;
	  ?>
	  <tr style="display: <?= ($showTotalPrice ? 'table-row' : 'none'); ?>;">
	  <td class="custom_t1"></td>
	  <td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
	  <?= ($showTotalPrice ? $arResult["PRICE_WITHOUT_DISCOUNT"] : ''); ?>
	  </td>
	  </tr>
	  <?
	  if (floatval($arResult['allVATSum']) > 0):
	  ?>
	  <tr>
	  <td><? echo GetMessage('SALE_VAT') ?></td>
	  <td id="allVATSum_FORMATED"><?= $arResult["allVATSum_FORMATED"] ?></td>
	  </tr>
	  <?
	  endif;
	  ?>
	  <? endif; ?>
	  <tr>
	  <td class="fwb"><?= GetMessage("SALE_TOTAL") ?></td>
	  <td class="fwb" id="allSum_FORMATED"><?= str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"]) ?></td>
	  </tr>


	  </table>
	  <div style="clear:both;"></div>
	  </div>
	  <div style="clear:both;"></div>
	  <div class="bx_ordercart_order_pay_center">

	  <? if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0): ?>
	  <?= $arResult["PREPAY_BUTTON"] ?>
	  <span><?= GetMessage("SALE_OR") ?></span>
	  <? endif; ?>
	  <?
	  if ($arParams["AUTO_CALCULATION"] != "Y")
	  {
	  ?>
	  <a href="javascript:void(0)" onclick="updateBasket();" class="checkout refresh"><?= GetMessage("SALE_REFRESH") ?></a>
	  <?
	  }
	  ?>
	  <a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?= GetMessage("SALE_ORDER") ?></a>
	  </div>
	  </div>
	  </div>
	  <?
	 */
} else
{
	?>
	<div id="basket_items_list">
		<table>
			<tbody>
				<tr>
					<td style="text-align:center">
						<div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?
}