<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */

/** @var array $arHeaders */

use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0) {
	?>
	<table class="basket-table">
		<thead>
		<tr>
			<th class="basket-table__header basket-table__header_photo">Фото</th>
			<th class="basket-table__header basket-table__header_name">Наименование товара</th>
			<th class="basket-table__header basket-table__header_article">Артикул</th>
			<th class="basket-table__header basket-table__header_size">Размер</th>
			<th class="basket-table__header basket-table__header_color">Цвет</th>
			<th class="basket-table__header basket-table__header_cnt">Количество</th>
			<th class="basket-table__header basket-table__header_price">Цена</th>
			<th class="basket-table__header basket-table__header_delete">
				<span class="basket-table__header-delete-icon"></span></th>
		</tr>
		</thead>
		<tbody>
		<?
		foreach ($arResult["GRID"]["ROWS"] as $k => $arItem) {
			if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y") {
				?>
				<tr data-id="<?= $arItem["ID"] ?>" data-item-name="<?= $arItem["NAME"] ?>" data-item-brand="<?= $arItem[$arParams['BRAND_PROPERTY'] . "_VALUE"] ?>" data-item-price="<?= $arItem["PRICE"] ?>" data-item-currency="<?= $arItem["CURRENCY"] ?>" class="basket-table__body-row">
					<td class="basket-table__body basket-table__body_photo">
						<input type="hidden" class="b-basket-qnt__send" name="QUANTITY_INPUT[<?= $arItem["ID"] ?>]" value="<?= $arItem["QUANTITY"] ?>"/>
						<a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>">
							<?
							if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0) {
								$url = $arItem["PREVIEW_PICTURE_SRC"];
							} elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0) {
								$url = $arItem["DETAIL_PICTURE_SRC"];
							} else {
								$url = SITE_TEMPLATE_PATH . "/img/empty.150.150.jpg";
							}
							if (strlen($arItem["DETAIL_PAGE_URL"]) > 0) {
								?>
								<img width="150" height="150" src="<?= $url ?>" alt=""/>
								<?
							} else {
								?>
								<img width="150" height="150" src="<?= $url ?>" alt=""/>
								<?
							}
							?>
						</a>
					</td>
					<td class="basket-table__body basket-table__body_name">
						<a class="basket-table__body-link" href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['NAME'] ?></a>
					</td>
					<td class="basket-table__body basket-table__body_article"><?= $arItem['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'] ?></td>
					<td class="basket-table__body basket-table__body_size<?= (strlen($arItem['ELEMENT']['PROPERTY_SIZE_NAME']) <= 0 ? ' basket-table__body_size-empty' : '') ?>">
						<div class="basket-table__body-prop">
							<span class="basket-table__body-prop-title">Размер</span>
							<span class="basket-table__body-prop-val"><?= $arItem['ELEMENT']['PROPERTY_SIZE_NAME'] ?></span>
						</div>
					</td>
					<td class="basket-table__body basket-table__body_color<?= (strlen($arItem['ELEMENT']['PROPERTY_COLOR_NAME']) <= 0 ? ' basket-table__body_color-empty' : '') ?>">
						<div class="basket-table__body-prop">
							<span class="basket-table__body-prop-title">Цвет</span>
							<span class="basket-table__body-prop-val"><?= $arItem['ELEMENT']['PROPERTY_COLOR_NAME'] ?></span>
						</div>
					</td>
					<td class="basket-table__body basket-table__body_cnt">
						<?
						$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
						$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
						$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
						?>
						<div class="basket-table__body-plusminus">
							<span class="basket-table__body-minus"></span>
							<input type="text" size="3" maxlength="18" value="<?= $arItem["QUANTITY"] ?>" data-max="<?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?>" class="basket_qnt basket-table__body-plusminus-box" data-title="Доступно для покупки - <?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?> шт." name="QUANTITY_INPUT[<?= $arItem["ID"] ?>]"/>
							<span class="basket-table__body-plus"></span>
						</div>
					</td>
					<td class="basket-table__body basket-table__body_price">
						<div class="basket-table__body-price">
							<div class="basket-table__body-price-title">
								Цена:
							</div>
							<div class="basket-table__body-price-val">
								<?
								if ($arItem['DISCOUNT_PRICE'] != 0) {
									?>
									<span class="oldprice"><?= $arItem['FULL_PRICE_FORMATED'] ?></span>
									<?= $arItem["PRICE_FORMATED"] ?>
									<?
								} else {
									echo $arItem["PRICE_FORMATED"];
								}
								?>
							</div>
						</div>
					</td>
					<td class="basket-table__body basket-table__body_delete">
						<a href="javascript:void(0)" class="basket-table__body-delete del-from-basket" data-id="<?= $arItem["ID"] ?>"></a>
					</td>
				</tr>
				<?
			}
		}
		?>
		</tbody>
		<tfoot>
		<tr class="basket-table__footer-row">
			<td class="basket-table__footer basket-table__footer-coupon" colspan="5">
				<?
				if ($arParams["HIDE_COUPON"] != "Y") {
					?>
					<div class="basket-table__footer-coupon-area">
						<div class="basket-table__footer-coupon-area-text">Купон на скидку</div>
						<div class="basket-table__footer-coupon-area-inputarea">
							<input type="text" id="coupon" name="COUPON" value="<?= $arResult['COUPON_LIST'][0]['COUPON'] ?>" placeholder="Купон на скидку" class="basket-table__footer-coupon-area-input"/>
						</div>
						<div class="basket-table__footer-coupon-area-button-area">
							<button type="button" id="apply-coupon" title="Применить скидочную карту" data-text="Применить" class="submit-button basket-table__footer-coupon-area-button" id="apply-coupon">
								Применить
							</button>
						</div>
					</div>
					<?
				}
				?>
			</td>
			<td class="basket-table__footer basket-table__footer-total" colspan="3">
				<div class="basket-table__footer-total-area">
					Итого: <span id="allSum_FORMATED"><?= $arResult['allSum_FORMATED'] ?></span>
					<button class="submit-button basket-table__footer-checkout" id="checkout" data-text="Оформить заказ" type="button">
						Оформить заказ
					</button>
				</div>
			</td>
		</tr>
		</tfoot>
	</table>
	<? /*
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="customer-login mt-30">


			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="customer-login payment-details mt-30">
				<h4 class="title-1 title-border text-uppercase">Итог</h4>
				<table>
					<tbody>
					<?
					$showTotalPrice = (float)$arResult["DISCOUNT_PRICE_ALL"] > 0;
					?>
					<tr>
						<td class="text-left">Стоимость товаров:</td>
						<td class="text-right">
							<span id="PRICE_WITHOUT_DISCOUNT"><?= ($showTotalPrice ? $arResult["PRICE_WITHOUT_DISCOUNT"] : ''); ?></span>
						</td>
					</tr>
					<tr class="b-cart__totaldiscount"<?= ($showTotalPrice ? '' : ' style="display:none;"') ?>>
						<td class="text-left">Скидка:</td>
						<td class="text-right">
							<span id="DISCOUNT_PRICE_ALL"><?= $arResult['DISCOUNT_PRICE_ALL_FORMATED'] ?></span></td>
					</tr>
					<tr>
						<td class="text-left">Итоговая стоимость:</td>
						<td class="text-right"><span id="allSum_FORMATED"><?= $arResult['allSum_FORMATED'] ?></span>
						</td>
					</tr>
					</tbody>
				</table>

			</div>
		</div>
	</div>

	<?*/
} else {
	?>
	<div id="basket_items_list">
		<table>
			<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<?
}