<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */

/** @var array $arResult */
use Bitrix\Main;

$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);
if (count($arResult["ITEMS"]["AnDelCanBuy"]) > 0)
{

	$products = array();
	$podarokIdArray = array();
	$podarokMainIdArray = array();
	$podarokPriceArray = array();

	foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arBasketItems)
	{

		$podarokIdArray[] = $arBasketItems["PRODUCT_ID"];

		$products[$arBasketItems['PRODUCT_ID']] = $arBasketItems['PRODUCT_ID'];
		$total_summ = $arResult["ITEMS"]["AnDelCanBuy"][$key]["TOTAL_SUMM"] = round($arBasketItems["PRICE"] * $arBasketItems["QUANTITY"]);
		$total_summ_old = $arResult["ITEMS"]["AnDelCanBuy"][$key]["TOTAL_SUMM_OLD"] = round($arBasketItems["FULL_PRICE"] * $arBasketItems["QUANTITY"]);

		$podarokPriceArray[$arBasketItems["PRODUCT_ID"]] = $total_summ;

		$arResult["ITEMS"]["AnDelCanBuy"][$key]["TOTAL_SUMM_FORMATED"] = SaleFormatCurrency(
				$total_summ, $allCurrency
		);
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["TOTAL_SUMM_OLD_FORMATED"] = SaleFormatCurrency(
				$total_summ_old, $allCurrency
		);
	}

	//ЗДЕСЬ БУДЕТ ВЫЧИСЛЕНИЕ ПОДАРОЧНЫХ ПОЗИЦИЙ
	$rMainItems = CIBlockElement::GetList(array(), array("ID" => $podarokIdArray, "IBLOCK_ID" => 4), false, false, array("ID", "PROPERTY_CML2_LINK"));
	while ($arMainItem = $rMainItems->Fetch())
	{
		$podarokMainIdArray[$arMainItem['PROPERTY_CML2_LINK_VALUE']] = $arMainItem['ID'];
	}

	$summForBrand = 0;
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_BRAND.NAME");
	$arFilter = Array("IBLOCK_ID" => 3, "ID" => array_keys($podarokMainIdArray), "PROPERTY_BRAND.NAME" => "AVANTRE");
	$rElements = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while ($arElement = $rElements->Fetch())
	{
		$summForBrand = $summForBrand + $podarokPriceArray[$podarokMainIdArray[$arElement["ID"]]];
		$podarochnieTovary[$arElement["ID"]] = $podarokPriceArray[$podarokMainIdArray[$arElement["ID"]]];
	}

	$arResult["PODAROCHNIE_TOVARY"] = $podarochnieTovary;
	$arResult["PODAROK_SUMM"] = $summForBrand;

	$arResult['PRODUCTS'] = array();
	$rsProducts = CCatalogProduct::GetList(array(), array('ID' => $products), false, false, array('ID', 'QUANTITY'));
	while ($arProduct = $rsProducts->Fetch())
	{
		$arResult['PRODUCTS'][$arProduct['ID']] = $arProduct;
	}

	$arResult['COUPONS_CNT'] = CCatalogDiscountCoupon::GetList(array(), array('ACTIVE' => 'Y'), array());

	if ($arResult['DISCOUNT_PRICE_ALL'] > 0)
	{
		$arResult['allSum_BEFORE_DISCOUNT'] = $arResult['allSum'] + $arResult['DISCOUNT_PRICE_ALL'];
		$arResult['allSum_BEFORE_DISCOUNT_FORMATED'] = SaleFormatCurrency(
				$arResult['allSum_BEFORE_DISCOUNT'], $allCurrency
		);
	}

	foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arBasketItems)
	{
		/*
		$rElement = CIBlockElement::GetList(array(), array("ID" => $arBasketItems['PRODUCT_ID']), false, false, array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_COLOR", "PROPERTY_COLOR.NAME", "PROPERTY_COLOR.PREVIEW_PICTURE", "PROPERTY_COLOR.DETAIL_PICTURE"));
		if ($arElement = $rElement->Fetch())
		{
			$photo = false;
			if ($arElement['PROPERTY_COLOR_VALUE'] > 0 && $arElement['PROPERTY_COLOR_PREVIEW_PICTURE'] > 0)
			{
				$photo = $arElement['PROPERTY_COLOR_PREVIEW_PICTURE'];
			} elseif ($arElement['PROPERTY_COLOR_VALUE'] > 0 && $arElement['PROPERTY_COLOR_DETAIL_PICTURE'] > 0)
			{
				$photo = $arElement['PROPERTY_COLOR_DETAIL_PICTURE'];
			} elseif ($arElement['PREVIEW_PICTURE'] > 0)
			{
				$photo = $arElement['PREVIEW_PICTURE'];
			} elseif ($arElement['DETAIL_PICTURE'] > 0)
			{
				$photo = $arElement['DETAIL_PICTURE'];
			}
			$arResult["ITEMS"]["AnDelCanBuy"][$key]['PICTURE'] = $photo;
		}*/
	}
}

//$arResult['BASKET_CNT'] = getCountProductsInBasket();
