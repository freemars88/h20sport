<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!empty($arResult["CATEGORIES"])) {
	?>
	<table class="title-search-result">
		<?
		foreach ($arResult["CATEGORIES"] as $category_id => $arCategory) {
			?>
			<tr>
				<td class="title-search-separator">&nbsp;</td>
			</tr>
			<?
			foreach ($arCategory["ITEMS"] as $i => $arItem) {
				?>
				<tr>
					<?
					if ($category_id === "all") {
						?>
						<td class="title-search-all"><a href="<?
							echo $arItem["URL"] ?>"><?
								echo $arItem["NAME"] ?></a></td>
						<?
					} elseif (isset($arResult["ELEMENTS"][$arItem["ITEM_ID"]])) {
						$arElement = $arResult["ELEMENTS"][$arItem["ITEM_ID"]];
						?>
						<td class="title-search-item">
							<a href="<?
							echo $arItem["URL"] ?>"><?
								if (is_array($arElement["PICTURE"])) {
									?>
									<img align="left" src="<?
									echo $arElement["PICTURE"]["src"] ?>" width="<?
									echo $arElement["PICTURE"]["width"] ?>" height="<?
									echo $arElement["PICTURE"]["height"] ?>">
								<? }
								echo $arItem["NAME"] ?></a>
						</td>
					<? } elseif (isset($arItem["ICON"])) { ?>
						<td class="title-search-item"><a href="<?
							echo $arItem["URL"] ?>"><?
								echo $arItem["NAME"] ?></a></td>
					<? } ?>
				</tr>
			<? }
		} ?>
		<tr>
			<td class="title-search-separator">&nbsp;</td>
		</tr>
	</table>
	<div class="title-search-fader"></div>
	<?
}
?>