<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
	$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
	$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
	$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));
	?>
	<div class="owl-carousel owl-theme product-card-carousel">
		<?
		foreach ($arResult['ITEMS'] as $arItem) {
			$arPhoto = false;
			if ($arItem['PREVIEW_PICTURE']['ID'] > 0) {
				$arPhoto = $arItem['PREVIEW_PICTURE'];
			}
			if ($arPhoto) {
				$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
			} else {
				$arPhoto = array(
					"src" => SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg",
				);
			}
			if (!empty($arItem['OFFERS'])) {
				$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
			} else {
				$actualItem = $arItem;
			}
			$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
			$discount = 0;
			$oldPrice = false;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0) {
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$price = doModifyItemPrice($price, $oldPrice);
			}
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0) {

				$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
				$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
			}
			?>
			<div class="product-card">
				<div class="product-card__inner">
					<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="product-card__image-link">
						<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" class="product-card__image"/>
					</a>
					<div class="product-card__rating">
						#VOTE_<?= $arItem['ID'] ?>#
					</div>
					<div class="product-card__info">
						<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="product-card__name"><?= $arItem['NAME'] ?></a>
					</div>
					<div class="product-card__actions">
						<div class="product-card__prices">
							<?
							if ($price['PERCENT'] > 0) {
								?>
								<div class="product-card__price-old"><?= $price['PRINT_RATIO_BASE_PRICE'] ?></div>
								<div class="product-card__price-current"><?= $price['PRINT_RATIO_PRICE'] ?></div>
								<?
							} else {
								?>
								<div class="product-card__price-old product-card__price-old_empty">&nbsp;</div>
								<div class="product-card__price-current"><?= $price['PRINT_RATIO_PRICE'] ?></div>
								<?
							}
							?>
						</div>
						<div class="product-card__action-area">
							<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в список желаний" data-id="<?= $arItem['ID'] ?>" class="product-card__action-button product-card__action-add-favorite add-favorite"></a>
							<?
							if (count($arItem['OFFERS']) == 1) {
								?>
								<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" class="product-card__action-button product-card__action-add-basket<?/* add-to-basket*/?>" data-element-id="<?= $arItem['OFFERS'][0]['ID'] ?>"></a>
								<?
							} else {
								?>
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-toggle="tooltip" data-placement="top" title="Купить" class="product-card__action-button product-card__action-buy"></a>
								<?
							}
							?>
						</div>
					</div>
					<div class="product-card__label">
						<?
						if ($arItem['PROPERTIES']['LABEL_NEW']['VALUE'] == "Y") {
							?>
							<span class="product-card__label-text product-card__label-text_new">Новинка</span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y") {
							?>
							<span class="product-card__label-text product-card__label-text_action">Акция</span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_POPULAR']['VALUE'] == "Y") {
							?>
							<span class="product-card__label-text product-card__label-text_popular">Хит продаж</span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] == "Y") {
							?>
							<span class="product-card__label-text product-card__label-text_blackfriday">Чёрная пятница</span>
						<? }
						?>
					</div>
				</div>
			</div>
			<?
		}
		?>
	</div>
	<?
}
