<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);
if (!empty($arResult['ITEMS']))
{
	$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
	$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
	$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));
	$fPriceProducts = 0;
	?>
	<form id="set-product-form">
		<input type="hidden" name="product-id" value="<?= $arParams['MAIN_PRODUCT_ID'] ?>" />
		<?
		foreach ($arResult['ITEMS'] as $arItem)
		{
			?>
			<div class="row row-set-product">
				<?
				$arPhoto = false;
				if ($arItem['PREVIEW_PICTURE']['ID'] > 0)
				{
					$arPhoto = $arItem['PREVIEW_PICTURE'];
				}
				if ($arPhoto)
				{
					$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 130, 'height' => 130), BX_RESIZE_IMAGE_EXACT, true);
				} else
				{
					$arPhoto = array(
						"src" => SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg"
					);
				}
				if (!empty($arItem['OFFERS']))
				{
					$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
				} else
				{
					$actualItem = $arItem;
				}
				$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
				$discount = 0;
				$oldPrice = false;
				if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
				{
					$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
					$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
				}
				?>
				<div class="col-sm-2">
					<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
						<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" />
					</a>
				</div>
				<div class="col-sm-10">
					<h4 class="post-title"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h4>
					<?
					$i = 0;
					foreach ($arItem['OFFERS'] as $arOffer)
					{
						$bSelected = false;
						if ($i == 0)
						{
							$bSelected = true;
						}
						?>
						<div class="row row-set-product-sku<?= ($bSelected ? " selected" : "") ?>">
							<div class="col-sm-1">
								<input type="radio" name="SET[<?= $arItem['ID'] ?>]" value="<?= $arOffer['ID'] ?>" id="SET_<?= $arItem['ID'] ?>_<?= $arOffer['ID'] ?>" data-product="<?= $arItem['ID'] ?>" data-offer="<?= $arOffer['ID'] ?>"<?
								if ($bSelected)
								{
									?> checked="checked"<? } ?> />
							</div>
							<div class="col-sm-8">
								<label for="SET_<?= $arItem['ID'] ?>_<?= $arOffer['ID'] ?>">
									<?
									if (!empty($arOffer['DISPLAY_PROPERTIES']))
									{
										$ar = array();
										foreach ($arOffer['DISPLAY_PROPERTIES'] as $arProp)
										{
											if (!empty($arProp['DISPLAY_VALUE']))
											{
												$ar[] = $arProp['NAME'] . ": " . strip_tags($arProp['DISPLAY_VALUE']);
											}
										}
										echo implode(", ", $ar);
									} else
									{
										echo $arOffer['NAME'];
									}
									?>
								</label>
							</div>
							<div class="col-sm-3 col-price">
								<?
								$priceOffer = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];
								$discount = 0;
								$oldPrice = false;
								if (isset($arOffer['PRICES']['OLDPRICE']) && $arOffer['PRICES']['OLDPRICE']['VALUE'] >= 0)
								{
									$oldPrice = $arOffer['PRICES']['OLDPRICE']['PRINT_VALUE'];
									$discount = round((($arOffer['PRICES']['OLDPRICE']['VALUE'] - $priceOffer['PRICE']) / $arOffer['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
								}
								if ($bSelected)
								{
									$fPriceProducts += $price['RATIO_PRICE'] * $arParams['~MAIN_PRODUCT_INFO'][$arItem['ID']];
								}
								?>
								<span class="pro-price floatleft"><?
									if ($priceOffer['PERCENT'] > 0)
									{
										?><i><?= $priceOffer['PRINT_RATIO_BASE_PRICE'] ?><br/></i>
										<?= $priceOffer['PRINT_RATIO_PRICE'] ?>
										<?
									} else
									{
										?>
										<?= $priceOffer['PRINT_RATIO_PRICE'] ?>
										<?
									}
									if ($arParams['~MAIN_PRODUCT_INFO'][$arItem['ID']] >= 2)
									{
										?> <span class="set-price-cnt">X <?= $arParams['~MAIN_PRODUCT_INFO'][$arItem['ID']] ?></span><?
									}
									?></span>
							</div>
						</div>
						<?
						$i++;
					}
					?>
				</div>
				<? /*
				  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				  <div class="single-product">
				  <div class="product-img">
				  <div class="product-action clearfix">
				  <?
				  if (count($arItem['OFFERS']) == 1)
				  {
				  ?>
				  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" class="add-to-basket" data-element-id="<?= $arItem['OFFERS'][0]['ID'] ?>"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
				  <?
				  } else
				  {
				  ?>
				  <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-toggle="tooltip" data-placement="top" title="Купить"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
				  <?
				  }
				  ?>
				  </div>
				  </div>
				  <div class="product-info clearfix">
				  <div class="fix">
				  <h4 class="post-title floatleft"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h4>
				  </div>
				  <div class="fix">
				  <span class="pro-price floatleft"><?
				  if ($price['PERCENT'] > 0)
				  {
				  ?><i><?= $price['PRINT_RATIO_BASE_PRICE'] ?></i>
				  <?= $price['PRINT_RATIO_PRICE'] ?>
				  <?
				  } else
				  {
				  ?>
				  <?= $price['PRINT_RATIO_PRICE'] ?>
				  <i>&nbsp;</i>
				  <?
				  }
				  ?></span>
				  </div>
				  </div>
				  </div>
				  </div>
				 * 
				 */ ?>
			</div>
			<?
		}
		?>
	</form>
	<div class="row">
		<div class="col-sm-12 text-right">
			<p>Общая стоимость товаров: <strong><?= FormatCurrency($fPriceProducts, "RUB") ?></strong></p>
			<p>Стоимость товаров в комплекте: <strong><?= $arParams['~MAIN_PRICE']['PRINT_RATIO_PRICE'] ?></strong></p>
			<p>Ваша экономия: <strong><?= FormatCurrency($fPriceProducts - $arParams['~MAIN_PRICE']['RATIO_PRICE'], "RUB") ?></strong></p>
		</div>
	</div>
	<div class="quick-add-to-cart quick-add-to-cart-set text-center mt-10">
		<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" class="b-detail-add-basket-set single_add_to_cart_button" type="button">Добавить в корзину</a>
	</div>
	<?
}
