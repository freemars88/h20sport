<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
if (!empty($arResult["ORDER_PROP"]["USER_PROFILES"])) {
	$ak = $arResult["ORDER_PROP"]["USER_PROFILES"];
	$arUserProfiles = $arResult["ORDER_PROP"]["USER_PROFILES"][$ak[0]];
	?>
	<input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?= $arUserProfiles["ID"] ?>"/>
	<?
}
$curPerson = false;
foreach ($arResult["PERSON_TYPE"] as $arPerson) {
	if ($curPerson === false || $arPerson['CHECKED'] == "Y") {
		$curPerson = $arPerson;
	}
}

if ($curPerson['ID'] == ORDER_PERSON_TYPE_FIZ) {
	?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "FIO", "Ваше ФИО"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "PHONE", "Телефон"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "EMAIL", "Эл. почта"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "LOCATION", "Регион"); ?>
	<?
	if (!in_array($arResult['USER_VALS']['DELIVERY_ID'], array(DELIVERY_DALLI_PVZ_DALLI_ID, DELIVERY_DALLI_PVZ_BOXBERRY_ID, DELIVERY_DALLI_PVZ_SDEK_ID, DELIVERY_DALLI_PVZ_PICKUP_ID, DELIVERY_PICKUP_ID))) {
		?>
		</div><div class="col-md-3">&nbsp;</div><div class="col-md-13" style="padding-top: 125px">
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "STREET", "Улица"); ?>
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "HOUSE", "Дом"); ?>
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "FLAT", "Квартира"); ?>
		<?
	}
} else {
	?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "COMPANY_NAME", "Название компании"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "INN", "ИНН"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "KPP", "КПП"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "OKPO", "ОКПО"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "YURADDRESS", "Юридический адрес"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "FACTADDRESS", "Фактический адрес"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "BIK", "БИК"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "BANK", "Название банка"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "RS_BANK", "Расчетный счет"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "KS_BANK", "Корр. счет"); ?>
	</div><div class="col-md-3">&nbsp;</div><div class="col-md-13" style="padding-top: 125px">
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "GENDIR", "Генеральный директор"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "COMPANY_PHONE", "Телефон компании"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "CONTACT_NAME", "Контактное лицо"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "CONTACT_PHONE", "Контактный телефон"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "EMAIL", "E-Mail"); ?>
	<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "LOCATION", "Местоположение"); ?>
	<?
	if (!in_array($arResult['USER_VALS']['DELIVERY_ID'], array(DELIVERY_DALLI_PVZ_DALLI_ID, DELIVERY_DALLI_PVZ_BOXBERRY_ID, DELIVERY_DALLI_PVZ_SDEK_ID, DELIVERY_DALLI_PVZ_PICKUP_ID, DELIVERY_PICKUP_ID))) {
		?>
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "STREET", "Улица"); ?>
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "HOUSE", "Дом"); ?>
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "FLAT", "Квартира"); ?>
		<?
	}
}
foreach ($arResult["ORDER_PROP"] as $v) {
	if (!in_array($arResult['USER_VALS']['DELIVERY_ID'], array(DELIVERY_DALLI_PVZ_DALLI_ID, DELIVERY_DALLI_PVZ_BOXBERRY_ID, DELIVERY_DALLI_PVZ_SDEK_ID, DELIVERY_DALLI_PVZ_PICKUP_ID, DELIVERY_PICKUP_ID))) {
		PrintPropsFormOther($v);
	} else {
		PrintPropsFormOther($v, array("STREET", "HOUSE", "FLAT"));
	}
}
if (!CSaleLocation::isLocationProEnabled()) {
	?>
	<div style="display:none;">

		<?
		$APPLICATION->IncludeComponent(
			"bitrix:sale.ajax.locations", $arParams["TEMPLATE_LOCATION"], array(
			"AJAX_CALL" => "N",
			"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
			"REGION_INPUT_NAME" => "REGION_tmp",
			"CITY_INPUT_NAME" => "tmp",
			"CITY_OUT_LOCATION" => "Y",
			"LOCATION_VALUE" => "",
			"ONCITYCHANGE" => "submitForm()",
		), NULL, array('HIDE_ICONS' => 'Y')
		);
		?>

	</div>
	<?
}
