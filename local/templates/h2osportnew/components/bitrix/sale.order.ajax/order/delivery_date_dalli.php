<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
CModule::IncludeModule("webavk.deliverydate");
$arElementsID = array();
foreach ($arResult["BASKET_ITEMS"] as $arBasketItem) {
	$arElementsID[$arBasketItem['PRODUCT_ID']] = $arBasketItem['QUANTITY'];
}
$iDeliveryId = false;
if ($arResult['USER_VALS']['DELIVERY_ID'] > 0) {
	$iDeliveryId = CSaleDelivery::getCodeById($arResult['USER_VALS']['DELIVERY_ID']);
}
$arDatesAllowDelivery = CWebavkDeliveryDateTools::GetAllowDeliveryDatesForOrder($iDeliveryId, 30, $arElementsID);
$currentDeliveryDate = '';
foreach ($arResult["ORDER_PROP"] as $arOrderProperties) {
	foreach ($arOrderProperties as $arProperties) {
		if ($arProperties["CODE"] == "F_DELIVERY_DATE") {
			foreach ($arDatesAllowDelivery as $curDate) {
				if ($arProperties["VALUE"] == $curDate)
					$currentDeliveryDate = $curDate;
			}
		}
	}
}
$arTimeIntervalsDelivery = array();
if ($currentDeliveryDate == '')
	$currentDeliveryDate = $arDatesAllowDelivery[0];

if (strlen($currentDeliveryDate) > 0) {
	$arTimeIntervalsDelivery = CWebavkDeliveryDateTools::GetTimeIntervalsForOrder($iDeliveryId, MakeTimeStamp($currentDeliveryDate), $arElementsID);
}
?>
	<script type="text/javascript">
		<!--
		var webavkDeliveryAllowDates =<?= CUtil::PhpToJSObject($arDatesAllowDelivery) ?>;
		-->
	</script>
<?
if (!empty($arDatesAllowDelivery)) {
	$arPropertyDeliveryDate = false;
	$arPropertyDeliveryTime = false;
	foreach ($arResult["ORDER_PROP"] as $k => $v) {
		foreach ($v as $arProp) {
			if ($arProp["CODE"] == "F_DELIVERY_DATE") {
				$arPropertyDeliveryDate = $arProp;
			} elseif ($arProp["CODE"] == "F_DELIVERY_TIME") {
				$arPropertyDeliveryTime = $arProp;
			}
		}
	}
	$isSel = false;
	foreach ($arDatesAllowDelivery as $curDate) {
		if (trim($arPropertyDeliveryDate["VALUE"]) == trim($curDate)) {
			$isSel = true;
		}
	}
	if (!$isSel) {
		foreach ($arDatesAllowDelivery as $k => $curDate) {
			$arPropertyDeliveryDate["VALUE"] = trim($curDate);
			break;
		}
	}

	$isSel = false;
	foreach ($arTimeIntervalsDelivery as $k => $curTime) {
		if (trim($arPropertyDeliveryTime["VALUE"]) == trim($curTime)) {
			$isSel = true;
		}
	}
	if (!$isSel) {
		foreach ($arTimeIntervalsDelivery as $k => $curTime) {
			$arPropertyDeliveryTime["VALUE"] = trim($curTime);
			break;
		}
	}
	$ar = array();
	if (strlen($arPropertyDeliveryDate['VALUE']) > 0) {
		$ar[] = $arPropertyDeliveryDate['VALUE'];
	}
	if (strlen($arPropertyDeliveryTime['VALUE']) > 0) {
		$ar[] = $arPropertyDeliveryTime['VALUE'];
	}
	?>
	<div class="form-check__prop">
		<div class="delivery-date-area">
			<div class="row delivery-date-row">
				<div class="col-md-60">
					<strong>Желаемая дата и время доставки *: <?= (count($ar) > 0 ? implode(", ", $ar) : "") ?></strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="row delivery-date-row">
				<div class="col-xl-20">
					<select name="<?= $arPropertyDeliveryDate["FIELD_NAME"] ?>" id="F_DELIVERY_DATE">
						<?
						foreach ($arDatesAllowDelivery as $curDate) {
							?>
							<option value="<?= $curDate ?>"<?
							if ($arPropertyDeliveryDate["VALUE"] == $curDate) {
								?> selected="selected"<? } ?>><?= $curDate ?></option>
							<?
						}
						?>
					</select>
					<div id="div_F_DELIVERY_DATE" class="order-calendar"></div>
				</div>

				<div class="col-xl-20 deliveryTime">
					<?
					foreach ($arTimeIntervalsDelivery as $k => $curTime) {
						?>
						<div class="mb10 form-radio">
							<input type="radio" name="<?= $arPropertyDeliveryTime["FIELD_NAME"] ?>" class="styler deliveryTime" value="<?= trim($curTime) ?>"<?
							if (trim($arPropertyDeliveryTime["VALUE"]) == trim($curTime)) {
								?> checked="checked"<? } ?> id="<?= $arPropertyDeliveryTime["FIELD_NAME"] ?>_<?= intval($k) ?>"/>
							<label for="<?= $arPropertyDeliveryTime["FIELD_NAME"] ?>_<?= intval($k) ?>"><?= $curTime ?></label>
							<div class="clearfix"></div>
						</div>
						<?
					}
					?>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<?
} ?>