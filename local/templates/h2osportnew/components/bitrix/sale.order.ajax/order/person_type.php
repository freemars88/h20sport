<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (count($arResult["PERSON_TYPE"]) > 1) {
	$i = 0;
	$activeId = false;
	foreach ($arResult["PERSON_TYPE"] as $v) {
		if ($v["CHECKED"] == "Y") {
			$activeId = $v['ID'];
		}
		?>
		<div class="order-field form-radio<? if ($v["CHECKED"] == "Y") echo " selected"; ?>">
			<input type="radio" id="PERSON_TYPE_ID_<?= $v["ID"] ?>" name="PERSON_TYPE" value="<?= $v["ID"] ?>" <? if ($v["CHECKED"] == "Y") echo " checked=\"checked\""; ?> onchange="submitForm()"/>
			<label for="PERSON_TYPE_ID_<?= $v["ID"] ?>"><?= $v["NAME"]; ?></label>
		</div>
		<?
		/* <div class="col-md-30 col-md-offset-<?= ($i == 0 ? "4" : "2") ?>">
		  <input type="radio" id="PERSON_TYPE_<?= $v["ID"] ?>" name="PERSON_TYPE" value="<?= $v["ID"] ?>"<? if ($v["CHECKED"] == "Y") echo " checked=\"checked\""; ?> class="styler" onchange="submitForm()"> <label for="PERSON_TYPE_<?= $v["ID"] ?>"><?= $v["NAME"] ?></label>
		  </div>
		  <? */
		$i++;
	}
	?>
	<input type="hidden" name="PERSON_TYPE_OLD" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>"/>
	<?
}/* else {
	if (IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0) {
		//for IE 8, problems with input hidden after ajax
		?>
		<span style="display:none;">
			<input type="text" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>" />
			<input type="text" name="PERSON_TYPE_OLD" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>" />
		</span>
		<?
	} else {
		foreach ($arResult["PERSON_TYPE"] as $v) {
			?>
			<input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?= $v["ID"] ?>" />
			<input type="hidden" name="PERSON_TYPE_OLD" value="<?= $v["ID"] ?>" />
			<?
		}
	}
}
 * 
 */
