<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script type="text/javascript">
	<!--
	function changePaySystem(param) {
		if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
		{
			if (param == 'account') {
				if (BX("PAY_CURRENT_ACCOUNT")) {
					BX("PAY_CURRENT_ACCOUNT").checked = true;
					BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
					BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

					// deselect all other
					var el = document.getElementsByName("PAY_SYSTEM_ID");
					for (var i = 0; i < el.length; i++)
						el[i].checked = false;
				}
			} else {
				BX("PAY_CURRENT_ACCOUNT").checked = false;
				BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
				BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
			}
		} else if (BX("account_only") && BX("account_only").value == 'N') {
			if (param == 'account') {
				if (BX("PAY_CURRENT_ACCOUNT")) {
					//BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

					if (BX("PAY_CURRENT_ACCOUNT").checked) {
						BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
						BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
					} else {
						BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
						BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
					}
				}
			}
		}

		submitForm();
	}

	//-->
</script>
<?
if ($arResult["PAY_FROM_ACCOUNT"] == "Y") {
	$accountOnly = ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y") ? "Y" : "N";
	?>
	<input type="hidden" id="account_only" value="<?= $accountOnly ?>"/>
	<div class="form-check<?
	if ($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y") {
		echo " selected";
	}
	?>">
		<input type="checkbox" name="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT" value="Y"<? if ($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y") echo " checked=\"checked\""; ?> onchange="changePaySystem('account');"/>
		<label for="PAY_CURRENT_ACCOUNT" onclick="" class="delivery-label">Оплатить с бонусного счета</label>
	</div>
	<div onclick="BX('PAY_CURRENT_ACCOUNT').checked = true;changePaySystem('account');" class="description delivery-description">
		На вашем счете: <?= $arResult["CURRENT_BUDGET_FORMATED"] ?><br/>
		Вы можете оплатить бонусами: <?= $arResult['MAX_PAY_BUDGET_FORMATED'] ?>
	</div>

	<?
}
?>
<div class="row">
	<?
	if (!empty($arResult["PAY_SYSTEM"])) {
		//uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value
		$ind = 0;
		foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
			if (count($arResult["PAY_SYSTEM"]) == 1) {
				?>
				<div class="col-md-30">
					<input type="hidden" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>">
					<div class="form-radio<?
					if ($arPaySystem["CHECKED"] == "Y") {
						echo " selected";
					}
					?>">
						<input type="radio" id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>" <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?> onchange="changePaySystem();"/>
						<label for="radio10" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked = true;
								changePaySystem();" class="delivery-label"><?= $arPaySystem["PSA_NAME"]; ?></label>
						<div onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked = true;
								changePaySystem();" class="description">
							<? // $arPaySystem["DESCRIPTION"]
							?>
						</div>
					</div>
				</div>
				<div class="col-md-30 order-total">
					<?= $arResult['TOTAL']['ORDER_PRICE_FORMATED'] ?>
				</div>
				<?
			} else { // more than one
				?>
				<div class="col-md-30">
					<div class="form-radio<? if ($arPaySystem["CHECKED"] == "Y") {
						echo " selected";
					} ?>">
						<input type="radio" id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>" <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?> onchange="changePaySystem();"/>
						<label for="radio10" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked = true;
								changePaySystem();" class="delivery-label"><?= $arPaySystem["PSA_NAME"]; ?></label>
						<div onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked = true;
								changePaySystem();" class="description">
							<? // $arPaySystem["DESCRIPTION"]
							?>
						</div>
					</div>
				</div>
				<?
				if ($arPaySystem["CHECKED"] == "Y") {
					?>
					<div class="col-md-30 order-total">
						<?= $arResult['JS_DATA']['TOTAL']['ORDER_PRICE_FORMATED'] ?>
					</div>
					<?
				} else {
					?>
					<div class="col-md-30">&nbsp;</div><?
				}
			}
			$ind++;
		}
	}
	?>
</div>
