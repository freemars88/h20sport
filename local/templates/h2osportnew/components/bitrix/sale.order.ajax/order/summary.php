<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
foreach ($arResult["GRID"]["ROWS"] as $k => $arData)
{
	?>
	<tr>
		<td><a href="<?= $arData["data"]["DETAIL_PAGE_URL"] ?>"><?= $arData["data"]["NAME"] ?></a> x <?= $arData["data"]["QUANTITY"] ?></td>
		<td class="text-right"><?= FormatCurrency($arData["data"]["BASE_PRICE"] * $arData["data"]["QUANTITY"], $arData['data']['CURRENCY']) ?></td>
	</tr>
	<?
}
