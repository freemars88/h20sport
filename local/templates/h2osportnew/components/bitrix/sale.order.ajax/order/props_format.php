<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!function_exists("showFilePropertyField")) {

	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show = 50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N") {
			$res = "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[0]\" id=\"" . $name . "[0]\"></label>";
		} else {
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[0]\" id=\"" . $name . "[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[1]\" id=\"" . $name . "[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}

}

if (!function_exists("PrintPropsForm")) {

	function PrintPropsForm($arSource = array(), $locationTemplate = ".default")
	{
		global $SHOW_ORDER_PROPERTIES;
		if (!empty($arSource)) {
			foreach ($arSource as $arProperties) {
				$SHOW_ORDER_PROPERTIES[] = $arProperties['ID'];
				?>
				<div class="order-field">
					<?
					if ($arProperties["TYPE"] == "CHECKBOX") {
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" value="">
						<input type="checkbox" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" value="Y"<? if ($arProperties["CHECKED"] == "Y") echo " checked"; ?>>
						<?
					} elseif ($arProperties["TYPE"] == "TEXT") {
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<input type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>" value="<?= $arProperties["VALUE"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" title="<?= $arProperties["NAME"] ?>" data-code="<?= $arProperties["CODE"] ?>"/>
						<?
					} elseif ($arProperties["TYPE"] == "SELECT") {
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<select name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" size="<?= $arProperties["SIZE1"] ?>" data-code="<?= $arProperties["CODE"] ?>" class="form-control">
							<?
							foreach ($arProperties["VARIANTS"] as $arVariants) {
								?>
								<option value="<?= $arVariants["VALUE"] ?>"<?= $arVariants["SELECTED"] == "Y" ? " selected" : '' ?>><?= $arVariants["NAME"] ?></option>
							<? } ?>
						</select>
						<?
					} elseif ($arProperties["TYPE"] == "MULTISELECT") {
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<select multiple name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" size="<?= $arProperties["SIZE1"] ?>" data-code="<?= $arProperties["CODE"] ?>">
							<?
							foreach ($arProperties["VARIANTS"] as $arVariants) {
								?>
								<option value="<?= $arVariants["VALUE"] ?>"<?= $arVariants["SELECTED"] == "Y" ? " selected" : '' ?>><?= $arVariants["NAME"] ?></option>
							<? } ?>
						</select>
						<?
					} elseif ($arProperties["TYPE"] == "TEXTAREA") {
						$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<textarea rows="<?= $rows ?>" cols="<?= $arProperties["SIZE1"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" title="<?= $arProperties["NAME"] ?>" class="form-control" data-code="<?= $arProperties["CODE"] ?>"><?= $arProperties["VALUE"] ?></textarea>
						<?
					} elseif ($arProperties["TYPE"] == "LOCATION") {
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<?
						$value = 0;
						if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0) {
							foreach ($arProperties["VARIANTS"] as $arVariant) {
								if ($arVariant["SELECTED"] == "Y") {
									$value = $arVariant["ID"];
									break;
								}
							}
						}

						// here we can get '' or 'popup'
						// map them, if needed
						if (CSaleLocation::isLocationProMigrated()) {
							$locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
							$locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
						}
						if ($locationTemplateP == 'steps') {
							?>
							<input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?= intval($arProperties["ID"]) ?>]" name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?= intval($arProperties["ID"]) ?>]" value="<?= ($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0') ?>"/>
							<?
						}
						CSaleLocation::proxySaleAjaxLocationsComponent(array(
							"INCLUDE_KLADR" => "Y",
							"AJAX_CALL" => "N",
							"COUNTRY_INPUT_NAME" => "COUNTRY",
							"REGION_INPUT_NAME" => "REGION",
							"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
							"CITY_OUT_LOCATION" => "Y",
							"LOCATION_VALUE" => $value,
							"ORDER_PROPS_ID" => $arProperties["ID"],
							"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
							"SIZE1" => $arProperties["SIZE1"],
						), array(
							"ID" => $value,
							"CODE" => "",
							"SHOW_DEFAULT_LOCATIONS" => "N",
							// function called on each location change caused by user or by program
							// it may be replaced with global component dispatch mechanism coming soon
							"JS_CALLBACK" => "submitFormProxy",
							// function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
							// it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
							"JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),
							// an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
							// it may be replaced with global component dispatch mechanism coming soon
							"JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),
							"DISABLE_KEYBOARD_INPUT" => "Y",
							"PRECACHE_LAST_LEVEL" => "Y",
							"PRESELECT_TREE_TRUNK" => "Y",
							"SUPPRESS_ERRORS" => "Y",
						), $locationTemplateP, true, 'location-block-wrapper form-control'
						);
					} elseif ($arProperties["TYPE"] == "RADIO") {
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<?
						if (is_array($arProperties["VARIANTS"])) {
							foreach ($arProperties["VARIANTS"] as $arVariants) {
								?>
								<input type="radio" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>" value="<?= $arVariants["VALUE"] ?>" <? if ($arVariants["CHECKED"] == "Y") echo " checked"; ?> />
								<label for="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"><?= $arVariants["NAME"] ?></label>
								<?
							}
						}
					} elseif ($arProperties["TYPE"] == "FILE") {
						?>
						<?= showFilePropertyField("ORDER_PROP_" . $arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"]) ?>
						<?
					} elseif ($arProperties["TYPE"] == "DATE") {
						global $APPLICATION;
						?>
						<label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
						<?
						$APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
							'SHOW_INPUT' => 'Y',
							'INPUT_NAME' => "ORDER_PROP_" . $arProperties["ID"],
							'INPUT_VALUE' => $arProperties["VALUE"],
							'SHOW_TIME' => 'N',
						), NULL, array('HIDE_ICONS' => 'N'));
					}
					?>
				</div>
				<?
				if (CSaleLocation::isLocationProEnabled()) {

					$propertyAttributes = array(
						'type' => $arProperties["TYPE"],
						'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
					);

					if (intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
						$propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

					if (intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
						$propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

					if ($arProperties['IS_ZIP'] == 'Y')
						$propertyAttributes['isZip'] = true;
					?>

					<script type="text/javascript">
						<!--
						(window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=
							CUtil::PhpToJSObject(array(
								'id' => intval($arProperties["ID"]),
								'attributes' => $propertyAttributes,
							))
							?>);
						//-->
					</script>
					<?
				}
			}
		}
	}

}

if (!function_exists("PrintPropsFormProperty")) {

	function PrintPropsFormProperty($arSource, $locationTemplate, $strPropertyCode, $strTitle = "")
	{
		global $USER;
		foreach ($arSource as $k => $v) {
			foreach ($v as $k1 => $v1) {
				if ($v1['CODE'] == $strPropertyCode) {
					if (strlen($strTitle) > 0) {
						$v1['NAME'] = $strTitle;
					}
					if (in_array($strPropertyCode, array("FIO", "EMAIL", "PHONE")) && strlen($v1['VALUE']) <= 0 && $USER->IsAuthorized()) {
						switch ($strPropertyCode) {
							case "FIO":
								$v1['VALUE'] = $USER->GetLastName() . " " . $USER->GetFirstName();
								break;
							case "EMAIL":
								$v1['VALUE'] = $USER->GetEmail();
								break;
							case "PHONE":
								$rUser = $USER->GetList($b, $o, array("ID" => $USER->GetID()), array("FIELDS" => "PERSONAL_PHONE"));
								if ($arUser = $rUser->Fetch()) {
									$v1['VALUE'] = $arUser['PERSONAL_PHONE'];
								}
								break;
						}
					}
					PrintPropsForm(array($v1), $locationTemplate, $strTitle);
				}
			}
		}
	}

}

if (!function_exists("PrintPropsFormOther")) {

	function PrintPropsFormOther($arSource = array(), $arSetEmptyValue = array())
	{
		global $SHOW_ORDER_PROPERTIES;
		if (!empty($arSource)) {
			foreach ($arSource as $arProperties) {
				if (!in_array($arProperties['ID'], $SHOW_ORDER_PROPERTIES) && !in_array($arProperties['CODE'], array("F_DELIVERY_DATE", "F_DELIVERY_TIME")) && strlen($arProperties["FIELD_NAME"]) > 0) {
					if ($arProperties['CODE'] == "EMAIL" && strlen($arProperties['VALUE']) <= 0) {
						$arProperties['VALUE'] = date("dmYHis-u") . "@francesco-donni-m.ru";
					}
					if ($arProperties["TYPE"] == "CHECKBOX") {
						if ($arProperties["CHECKED"] == "Y") {
							?>
							<input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" value="Y" data-code="<?= $arProperties["CODE"] ?>" /><?
						}
					} elseif ($arProperties['CODE'] == "PHONE_PRESS") {
						?>
						<input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" value="<?= $arProperties["VALUE"] ?>" rel="phone-press" data-code="<?= $arProperties["CODE"] ?>" /><?
					} else {
						if (is_array($arProperties["VALUE"])) {
							if (in_array($arProperties["CODE"], $arSetEmptyValue)) {
								$arProperties["VALUE"] = array("");
							}
							foreach ($arProperties["VALUE"] as $k => $v) {
								?>
								<input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>[]" id="<?= $arProperties["FIELD_NAME"] ?>_<?= $k ?>" value="<?= $v ?>" data-code="<?= $arProperties["CODE"] ?>" /><?
							}
						} else {
							if (in_array($arProperties["CODE"], $arSetEmptyValue)) {
								$arProperties["VALUE"] = "";
							}
							?>
							<input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" value="<?= $arProperties["VALUE"] ?>" data-code="<?= $arProperties["CODE"] ?>" /><?
						}
					}
				}
			}
		}
	}

}