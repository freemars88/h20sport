<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$arPath = array_column($arResult["arMap"], 'FULL_PATH');
$lastIndex = 0;
foreach ($arPath as $index => $arItem) {
    $pos = strpos($arItem, '/catalog/veykbording/');
    if ($pos !== false) {
        $lastIndex = $index;
    }
}
$arAdd = [
    [
        'LEVEL' => 2,
        'IS_DIR' => 'N',
        'NAME' => 'Вейкбординг. Оборудование JOBE',
        'PATH' => '/var/www/dev2.h2osport.ru/web/catalog/',
        'FULL_PATH' => '/catalog/veykbording/jobe/',
        'SEARCH_PATH' => '/catalog/veykbording/jobe/',
        'DESCRIPTION' => '',
    ],
    [
        'LEVEL' => 2,
        'IS_DIR' => 'N',
        'NAME' => 'Вейкбординг. Оборудование NP',
        'PATH' => '/var/www/dev2.h2osport.ru/web/catalog/',
        'FULL_PATH' => '/catalog/veykbording/np/',
        'SEARCH_PATH' => '/catalog/veykbording/np/',
        'DESCRIPTION' => '',
    ],
    [
        'LEVEL' => 2,
        'IS_DIR' => 'N',
        'NAME' => 'Детский вейкбординг',
        'PATH' => '/var/www/dev2.h2osport.ru/web/catalog/',
        'FULL_PATH' => '/catalog/veykbording/detskoe/',
        'SEARCH_PATH' => '/catalog/veykbording/detskoe/',
        'DESCRIPTION' => '',
    ],
    [
        'LEVEL' => 2,
        'IS_DIR' => 'N',
        'NAME' => 'Женский вейкбординг',
        'PATH' => '/var/www/dev2.h2osport.ru/web/catalog/',
        'FULL_PATH' => '/catalog/veykbording/zhenskoe/',
        'SEARCH_PATH' => '/catalog/veykbording/zhenskoe/',
        'DESCRIPTION' => '',
    ],
    [
        'LEVEL' => 2,
        'IS_DIR' => 'N',
        'NAME' => 'Мужской вейкбординг',
        'PATH' => '/var/www/dev2.h2osport.ru/web/catalog/',
        'FULL_PATH' => '/catalog/veykbording/muzhskoe/',
        'SEARCH_PATH' => '/catalog/veykbording/muzhskoe/',
        'DESCRIPTION' => '',
    ],
];
array_splice($arResult["arMap"], $lastIndex+1, 0, $arAdd);