<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arTopComponentParams = array(
	"ACTION_VARIABLE" => "action",
	"ADD_PICT_PROP" => "-",
	"ADD_PROPERTIES_TO_BASKET" => "Y",
	"ADD_TO_BASKET_ACTION" => "ADD",
	"BASKET_URL" => "/personal/basket.php",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"CACHE_TIME" => "36000000",
	"CACHE_TYPE" => "A",
	"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
	"COMPATIBLE_MODE" => "Y",
	"CONVERT_CURRENCY" => "N",
	"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
	"DETAIL_URL" => "",
	"DISPLAY_COMPARE" => "N",
	"ELEMENT_COUNT" => "9",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_ORDER2" => "desc",
	"ENLARGE_PRODUCT" => "STRICT",
	"FILTER_NAME" => "arrFilter",
	"HIDE_NOT_AVAILABLE" => "N",
	"HIDE_NOT_AVAILABLE_OFFERS" => "N",
	"IBLOCK_ID" => "3",
	"IBLOCK_TYPE" => "catalog",
	"LABEL_PROP" => array(),
	"LINE_ELEMENT_COUNT" => "3",
	"MESS_BTN_ADD_TO_BASKET" => "В корзину",
	"MESS_BTN_BUY" => "Купить",
	"MESS_BTN_COMPARE" => "Сравнить",
	"MESS_BTN_DETAIL" => "Подробнее",
	"MESS_NOT_AVAILABLE" => "Нет в наличии",
	"OFFERS_CART_PROPERTIES" => array(),
	"OFFERS_FIELD_CODE" => array("NAME", ""),
	"OFFERS_LIMIT" => "0",
	"OFFERS_PROPERTY_CODE" => array("", ""),
	"OFFERS_SORT_FIELD" => "sort",
	"OFFERS_SORT_FIELD2" => "id",
	"OFFERS_SORT_ORDER" => "asc",
	"OFFERS_SORT_ORDER2" => "desc",
	"PARTIAL_PRODUCT_PROPERTIES" => "N",
	"PRICE_CODE" => array("Розничная", "SKU_MIN"),
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
	"PRODUCT_DISPLAY_MODE" => "N",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_PROPERTIES" => array(),
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'0','BIG_DATA':false}]",
	"PRODUCT_SUBSCRIPTION" => "Y",
	"PROPERTY_CODE" => array("BRAND", ""),
	"PROPERTY_CODE_MOBILE" => array(),
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"SECTION_URL" => "",
	"SEF_MODE" => "N",
	"SHOW_CLOSE_POPUP" => "N",
	"SHOW_DISCOUNT_PERCENT" => "N",
	"SHOW_MAX_QUANTITY" => "N",
	"SHOW_OLD_PRICE" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"SHOW_SLIDER" => "Y",
	"SLIDER_INTERVAL" => "3000",
	"SLIDER_PROGRESS" => "N",
	"TEMPLATE_THEME" => "blue",
	"USE_ENHANCED_ECOMMERCE" => "N",
	"USE_PRICE_COUNT" => "N",
	"USE_PRODUCT_QUANTITY" => "N",
	"VIEW_MODE" => "SECTION"
);

global $arrFilter;
$arrFilter = array();
$arrFilter['ID'] = $arResult['PRODUCTS'];
?>
<div class="b-order b-order_prod js-show">
	<div class="b-order__title js-show-button">
		<span>Избранное / <b>товары</b> </span>
		<div class="b-order__controls">
			<? /* <a href="#" class="b-button b-button_rounded -gray-bd"><i class="b-icon b-icon_svg -link-gray"></i></a> */ ?>
			<a href="/personal/wishlist/" class="b-button b-button_rounded -gray-bd"><i class="b-icon b-icon_svg -star-gray"></i></a>
		</div>
	</div>
	<div class="b-order__carousel js-show-target">
		<?$APPLICATION->IncludeComponent("bitrix:catalog.top", "personal.slider", $arTopComponentParams, false);?>
		<?/*
		<div class="b-carousel js-carousel">
			<div class="b-carousel__viewport js-carousel-viewport">
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c1.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c2.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Бумага для акварели <span class="-bold -uppercase"> saunders waterford</span> rough крупное зерно, 56х76 см 300 г белоснежный </p>
						</div>
						<div class="b-carousel-item__footer">
							<span>313 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c3.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c4.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header">
							<div class="b-carousel-item__header-image">
								<img src="/dist/img/carousel/c5.jpg" alt="">
							</div>
						</div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c1.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c2.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Бумага для акварели <span class="-bold -uppercase"> saunders waterford</span> rough крупное зерно, 56х76 см 300 г белоснежный </p>
						</div>
						<div class="b-carousel-item__footer">
							<span>313 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded  -is-active"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c3.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c4.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded -is-active"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header">
							<div class="b-carousel-item__header-image">
								<img src="/dist/img/carousel/c5.jpg" alt="">
							</div>
						</div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded -is-active"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c1.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c2.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Бумага для акварели <span class="-bold -uppercase"> saunders waterford</span> rough крупное зерно, 56х76 см 300 г белоснежный </p>
						</div>
						<div class="b-carousel-item__footer">
							<span>313 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c3.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded -is-active"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header"><img src="/dist/img/carousel/c4.jpg" alt=""></div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="b-carousel__item">
					<div class="b-carousel-item__inner">
						<div class="b-carousel-item__header">
							<div class="b-carousel-item__header-image">
								<img src="/dist/img/carousel/c5.jpg" alt="">
							</div>
						</div>
						<div class="b-carousel-item__content">
							<p>Набор акрила schmincke <span class="-bold">"AKADEMIE"</span> 12 ЦВ*60 МЛ в деревянной упаковке</p>
						</div>
						<div class="b-carousel-item__footer">
							<span>от 456 <i class="fa fa-rub"></i></span>
							<div class="b-carousel-item__price">
								<a href="#" class="b-icon b-icon_svg -busket-rounded -is-active"></a>
								<a href="#" class="b-icon b-icon_svg -fav-rounded"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="b-carousel__controls js-carousel-controls">
				<div class="b-carousel-controls__arrows js-carousel-arrows -gray">
				</div>
				<div class="b-carousel-controls__dots js-carousel-dots -gray">
				</div>
			</div>
		</div>
		*/?>
	</div>
</div>
<?
return;

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$arParams = array(
	"ACTION_VARIABLE" => "action",
	"ADD_ELEMENT_CHAIN" => "N",
	"ADD_PICT_PROP" => "MORE_PHOTO",
	"ADD_PROPERTIES_TO_BASKET" => "N",
	"ADD_SECTIONS_CHAIN" => "Y",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_ADDITIONAL" => "",
	"AJAX_OPTION_HISTORY" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"ALSO_BUY_ELEMENT_COUNT" => "20",
	"ALSO_BUY_MIN_BUYES" => "1",
	"BASKET_URL" => "/personal/cart/",
	"BIG_DATA_RCM_TYPE" => "bestsell",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "Y",
	"CACHE_TIME" => "36000000",
	"CACHE_TYPE" => "A",
	"COMMON_ADD_TO_BASKET_ACTION" => "",
	"COMMON_SHOW_CLOSE_POPUP" => "Y",
	"CONVERT_CURRENCY" => "N",
	"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
	"DETAIL_ADD_TO_BASKET_ACTION" => array("ADD"),
	"DETAIL_BACKGROUND_IMAGE" => "-",
	"DETAIL_BRAND_USE" => "N",
	"DETAIL_BROWSER_TITLE" => "-",
	"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
	"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
	"DETAIL_DISPLAY_NAME" => "Y",
	"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
	"DETAIL_META_DESCRIPTION" => "-",
	"DETAIL_META_KEYWORDS" => "-",
	"DETAIL_OFFERS_FIELD_CODE" => array("NAME", ""),
	"DETAIL_OFFERS_PROPERTY_CODE" => array("COLOR", "NUMBER_COLOR", "DIAMETER", "FORMAT", "VOLUME", "WEIGHT", "SIZE_MM", "DENSITY", "HARDNESS", "NUMBER_BRUSH", "SIZE_SM", "TEXTURE", "LENGTH_HANDLE", "SOSTAV", "CML2_ARTICLE", "CML2_BAR_CODE", ""),
	"DETAIL_PROPERTY_CODE" => array("BRAND", "COUNTRY"),
	"DETAIL_SET_CANONICAL_URL" => "Y",
	"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
	"DETAIL_SHOW_MAX_QUANTITY" => "N",
	"DETAIL_USE_COMMENTS" => "N",
	"DETAIL_USE_VOTE_RATING" => "N",
	"DISABLE_INIT_JS_IN_COMPONENT" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_ORDER2" => "desc",
	"FILE_404" => "",
	"FILTER_FIELD_CODE" => array("", ""),
	"FILTER_NAME" => "",
	"FILTER_OFFERS_FIELD_CODE" => array("", ""),
	"FILTER_OFFERS_PROPERTY_CODE" => array("COLOR", "NUMBER_COLOR", "DIAMETER", "FORMAT", "VOLUME", "WEIGHT", "SIZE_MM", "DENSITY", "HARDNESS", "NUMBER_BRUSH", "SIZE_SM", "TEXTURE", "LENGTH_HANDLE", "SOSTAV", ""),
	"FILTER_PRICE_CODE" => array("Розничная"),
	"FILTER_PROPERTY_CODE" => array("BRAND", "COUNTRY", ""),
	"FILTER_VIEW_MODE" => "VERTICAL",
	"FORUM_ID" => "",
	"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
	"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
	"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
	"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
	"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
	"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
	"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
	"GIFTS_MESS_BTN_BUY" => "Выбрать",
	"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
	"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
	"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "3",
	"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
	"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
	"GIFTS_SHOW_IMAGE" => "Y",
	"GIFTS_SHOW_NAME" => "Y",
	"GIFTS_SHOW_OLD_PRICE" => "Y",
	"HIDE_NOT_AVAILABLE" => "Y",
	"IBLOCK_ID" => "3",
	"IBLOCK_TYPE" => "catalog",
	"INCLUDE_SUBSECTIONS" => "Y",
	"LABEL_PROP" => "-",
	"LINE_ELEMENT_COUNT" => "3",
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
	"LINK_IBLOCK_ID" => "",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_PROPERTY_SID" => "",
	"LIST_BROWSER_TITLE" => "UF_META_TITLE",
	"LIST_META_DESCRIPTION" => "UF_META_DESCRIPTION",
	"LIST_META_KEYWORDS" => "UF_META_KEYWORDS",
	"LIST_OFFERS_FIELD_CODE" => array("NAME", ""),
	"LIST_OFFERS_LIMIT" => "1",
	"LIST_OFFERS_PROPERTY_CODE" => array("CML2_ARTICLE", ""),
	"LIST_PROPERTY_CODE" => array("BRAND", "COUNTRY", "CML2_ARTICLE"),
	"MESSAGES_PER_PAGE" => "10",
	"MESSAGE_404" => "",
	"MESS_BTN_ADD_TO_BASKET" => "В корзину",
	"MESS_BTN_BUY" => "Купить",
	"MESS_BTN_COMPARE" => "Сравнение",
	"MESS_BTN_DETAIL" => "Подробнее",
	"MESS_NOT_AVAILABLE" => "Нет в наличии",
	"OFFERS_CART_PROPERTIES" => array(""),
	"OFFERS_SORT_FIELD" => "name",
	"OFFERS_SORT_FIELD2" => "id",
	"OFFERS_SORT_ORDER" => "asc",
	"OFFERS_SORT_ORDER2" => "desc",
	"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
	"OFFER_TREE_PROPS" => array(),
	"PAGER_BASE_LINK_ENABLE" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "catalog",
	"PAGER_TITLE" => "Товары",
	"PAGE_ELEMENT_COUNT" => "20",
	"PARTIAL_PRODUCT_PROPERTIES" => "N",
	"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
	"PRICE_CODE" => array("OLD_PRICE", "Розничная", "SKU_MIN", "SKU_MAX"),
	"PRICE_VAT_INCLUDE" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"PRODUCT_DISPLAY_MODE" => "N",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_PROPERTIES" => array(""),
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"PRODUCT_QUANTITY_VARIABLE" => "",
	"REVIEW_AJAX_POST" => "Y",
	"SECTIONS_HIDE_SECTION_NAME" => "N",
	"SECTIONS_SHOW_PARENT_NAME" => "Y",
	"SECTIONS_VIEW_MODE" => "TILE",
	"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
	"SECTION_BACKGROUND_IMAGE" => "-",
	"SECTION_COUNT_ELEMENTS" => "N",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"SECTION_TOP_DEPTH" => "2",
	"SEF_FOLDER" => "/catalog/",
	"SEF_MODE" => "Y",
	"SEF_URL_TEMPLATES" => Array("compare" => "compare.php?action=#ACTION_CODE#", "element" => "#SECTION_CODE#/#ELEMENT_CODE#.html", "section" => "#SECTION_CODE#/", "sections" => "", "smart_filter" => "#SECTION_CODE#/#SMART_FILTER_PATH#/"),
	"SET_LAST_MODIFIED" => "Y",
	"SET_STATUS_404" => "Y",
	"SET_TITLE" => "Y",
	"SHOW_404" => "Y",
	"SHOW_DEACTIVATED" => "N",
	"SHOW_DISCOUNT_PERCENT" => "N",
	"SHOW_LINK_TO_FORUM" => "Y",
	"SHOW_OLD_PRICE" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"SHOW_TOP_ELEMENTS" => "N",
	"SIDEBAR_DETAIL_SHOW" => "Y",
	"SIDEBAR_PATH" => "",
	"SIDEBAR_SECTION_SHOW" => "Y",
	"TEMPLATE_THEME" => "blue",
	"TOP_ADD_TO_BASKET_ACTION" => "ADD",
	"TOP_ELEMENT_COUNT" => "9",
	"TOP_ELEMENT_SORT_FIELD" => "sort",
	"TOP_ELEMENT_SORT_FIELD2" => "id",
	"TOP_ELEMENT_SORT_ORDER" => "asc",
	"TOP_ELEMENT_SORT_ORDER2" => "desc",
	"TOP_LINE_ELEMENT_COUNT" => "3",
	"TOP_OFFERS_FIELD_CODE" => array(0 => "", 1 => "",),
	"TOP_OFFERS_LIMIT" => "5",
	"TOP_OFFERS_PROPERTY_CODE" => array(0 => "", 1 => "",),
	"TOP_PROPERTY_CODE" => array(0 => "", 1 => "",),
	"TOP_VIEW_MODE" => "SECTION",
	"URL_TEMPLATES_READ" => "",
	"USE_ALSO_BUY" => "Y",
	"USE_BIG_DATA" => "N",
	"USE_CAPTCHA" => "Y",
	"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
	"USE_COMPARE" => "N",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_FILTER" => "Y",
	"USE_GIFTS_DETAIL" => "N",
	"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
	"USE_GIFTS_SECTION" => "N",
	"USE_MAIN_ELEMENT_SECTION" => "Y",
	"USE_PRICE_COUNT" => "N",
	"USE_PRODUCT_QUANTITY" => "N",
	"USE_REVIEW" => "N",
	"USE_SALE_BESTSELLERS" => "Y",
	"USE_STORE" => "N",
	"VARIABLE_ALIASES" => array("compare" => array("ACTION_CODE" => "action",),)
);
$arSortNames = array(
	"CATALOG_PRICE_3" => "цена",
	"NAME" => "наименование",
	"VIEWS" => "популярность"
);

$viewType = "plitka";
$sortBy = "CATALOG_PRICE_3";
$sortOrder = "desc";
/*
  if (isset($_REQUEST['v']) && in_array($_REQUEST['v'], array("p", "t"))) {
  $_SESSION['viewType'] = $_REQUEST['v'];
  } elseif (isset($_SESSION['viewType']) && in_array($_SESSION['viewType'], array("p", "t"))) {

  } elseif (strlen($APPLICATION->get_cookie("viewType")) > 0 && in_array($APPLICATION->get_cookie("viewType"), array("p", "t"))) {
  $_SESSION['viewType'] = $APPLICATION->get_cookie("viewType");
  }
  if ($APPLICATION->get_cookie("viewType") != $_SESSION['viewType']) {
  $APPLICATION->set_cookie("viewType", $_SESSION['viewType']);
  }
  if ($_SESSION['viewType'] == "t") {
  $viewType = "table";
  } else {
  $viewType = "plitka";
  $_SESSION['viewType'] = "p";
  }
 */
if (isset($_REQUEST['s']) && in_array($_REQUEST['s'], array("pa", "pd", "na", "nd"))) {
	$_SESSION['sortBy'] = $_REQUEST['s'];
} elseif (isset($_SESSION['sortBy']) && in_array($_SESSION['sortBy'], array("pa", "pd", "na", "nd"))) {
	
} elseif (strlen($APPLICATION->get_cookie("sortBy")) > 0 && in_array($APPLICATION->get_cookie("sortBy"), array("pa", "pd", "na", "nd"))) {
	$_SESSION['sortBy'] = $APPLICATION->get_cookie("sortBy");
}
if ($APPLICATION->get_cookie("sortBy") != $_SESSION['sortBy']) {
	$APPLICATION->set_cookie("sortBy", $_SESSION['sortBy']);
}
if ($_SESSION['sortBy'] == "na") {
	$sortBy = "NAME";
	$sortOrder = "asc";
} elseif ($_SESSION['sortBy'] == "nd") {
	$sortBy = "NAME";
	$sortOrder = "desc";
} elseif ($_SESSION['sortBy'] == "pa") {
	$sortBy = "CATALOG_PRICE_3";
	$sortOrder = "asc";
} else {
	$sortBy = "CATALOG_PRICE_3";
	$sortOrder = "desc";
	$_SESSION['sortBy'] = "pd";
}
if (isset($_REQUEST['ov']) && in_array($_REQUEST['ov'], array("n", "a", "m", "p", "all"))) {
	$_SESSION['onlyView'] = $_REQUEST['ov'];
	if ($_SESSION['onlyView'] == "all") {
		LocalRedirect($APPLICATION->GetCurPageParam("", array("ov")));
	}
}


if (!isset($arParams['FILTER_VIEW_MODE']) || (string) $arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
?>
<section class="b-section -mobile-full" id="catalog-section">
    <div class="container">
        <div class="row row_top row_sm_column">
			<?
			global $arrFilter;

			$APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list", "notview", array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
				"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
				"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
				"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
				"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
				"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
				"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
				"DATA_FILTER" => $arDataFilter,
				""
					), false, array("HIDE_ICONS" => "Y")
			);
			if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
				$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
			} else {
				$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
			}

			$intSectionID = 0;
			?>
            <div class="col b-catalogue b-catalogue-full last">
                <div class="b-sort ">
                    <div class="row b-sort__row">
                        <div class="col b-sort__col b-sort__items -mobile-hidden">
                            <ul class="row">
                                <li class="col b-sort__item">
                                    <div class="b-form__select js-select">
                                        <label for="select-sortby">Сортировка:</label>
                                        <select name="s" id="select-sortby">
											<option value="pd" data-url="<?= $APPLICATION->GetCurPageParam("s=pd", array("s")) ?>"<? if ($_SESSION['sortBy'] == "pd") { ?> selected="selected"<? } ?>>цена, сначала дорогие</option>
                                            <option value="pa" data-url="<?= $APPLICATION->GetCurPageParam("s=pa", array("s")) ?>"<? if ($_SESSION['sortBy'] == "pa") { ?> selected="selected"<? } ?>>цена, сначала дешевые</option>
                                            <option value="na" data-url="<?= $APPLICATION->GetCurPageParam("s=na", array("s")) ?>"<? if ($_SESSION['sortBy'] == "na") { ?> selected="selected"<? } ?>>название, от А до Я</option>
                                            <option value="nd" data-url="<?= $APPLICATION->GetCurPageParam("s=nd", array("s")) ?>"<? if ($_SESSION['sortBy'] == "nd") { ?> selected="selected"<? } ?>>название, от Я до А</option>
                                        </select>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col b-sort__col b-sort__switcher last">
                            <div class="b-switcher">
                                <div class="b-switcher__button<?= ($APPLICATION->get_cookie("catSw") == "row" ? " -is-active" : "") ?> js-catalogue-view-switch" data-view="row">
                                    <i class="b-icon b-icon_svg -tile-gray"></i>
                                </div>
                                <div class="b-switcher__button<?= ($APPLICATION->get_cookie("catSw") == "column" ? " -is-active" : "") ?> js-catalogue-view-switch" data-view="column">
                                    <i class="b-icon b-icon_svg -list-gray"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?
				global $arrFilter;
				$arrFilter = array();
				$arrFilter['ID'] = $arResult['PRODUCTS'];
				?>
				<div id="filterTarget">
					<?
					$intSectionID = $APPLICATION->IncludeComponent(
							"bitrix:catalog.section", "", array(
						"BRANDS_CONTENT" => $cont,
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_SORT_FIELD" => $sortBy,
						"ELEMENT_SORT_ORDER" => $sortOrder,
						"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
						"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
						"SHOW_ALL_WO_SECTION" => "Y",
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
						"FILTER_NAME" => "arrFilter",
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_META_DESCRIPTION" => "N",
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
						"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
						"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
						"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
						"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
						"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
						"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
						"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
						"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
						"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
						"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
						"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
						"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
						"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
						'LABEL_PROP' => $arParams['LABEL_PROP'],
						'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
						'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
						'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
						'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
						'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
						'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
						'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
						'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
						'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
						'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
						'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
						'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
						'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
						"ADD_SECTIONS_CHAIN" => "N",
						'ADD_TO_BASKET_ACTION' => $basketAction,
						'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
						'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
						'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
						'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
						"AJAX_MODE" => "Y",
						"AJAX_OPTION_JUMP" => (strtoupper($GLOBALS['nojump']) == "Y" ? "N" : "Y"),
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "Y",
						"AJAX_OPTION_ADDITIONAL" => "",
						"DATA_FILTER" => $arDataFilter,
						"viewType" => $viewType,
						"sortOrder" => $sortOrder,
						"sortBy" => $sortBy,
						"sortName" => $arSortNames[$sortBy],
						"MODE_VIEW" => $APPLICATION->get_cookie("catSw"),
						"MICROTITLE" => "Избранные товары",
						"DELETE_FROM_WISHLIST" => "Y"
							), false
					);
					?>
					<? /* <div class="section-description">
					  <? $APPLICATION->ShowProperty("SECTION_DESCRIPTION") ?>
					  </div>
					  <div class="clearfix"></div>
					 * 
					 */ ?>
				</div>
            </div>
        </div>
    </div>
</section>