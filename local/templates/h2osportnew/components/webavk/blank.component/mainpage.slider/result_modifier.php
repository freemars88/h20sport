<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");
$rElements = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "ACTIVE_DATE" => "Y", "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_MAINSLIDER), false, false, array("ID", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_LINK"));
while ($arElement = $rElements->Fetch())
{
	$arResult['BANNERS'][] = $arElement;
}
