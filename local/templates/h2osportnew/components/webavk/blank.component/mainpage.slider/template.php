<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="banners">
	<div class="owl-carousel owl-theme mainpage-banners-carousel">
		<?
		$i = 0;
		foreach ($arResult['BANNERS'] as $arBanner) {
			?>
			<div>
				<a href="<?= $arBanner['PROPERTY_LINK_VALUE'] ?>" class="mainpage-banners__link" data-index="<?= intval($i) ?>">
					<img src="<?= CFile::GetPath($arBanner['DETAIL_PICTURE']) ?>" alt="" class="mainpage-banners__img"/>
				</a>
			</div>
			<?
			$i++;
		}
		?>
	</div>
	<div class="mainpage-banners-carousel-title-small"><?= $arResult['BANNERS'][0]['DETAIL_TEXT'] ?></div>
	<div class="owl-carousel owl-theme mainpage-banners-carousel-title">
		<?
		$i = 0;
		foreach ($arResult['BANNERS'] as $arBanner) {
			?>
			<div class="mainpage-banners__title<?= ($i == 0 ? ' mainpage-banners__title_active' : '') ?>" data-index="<?= intval($i) ?>">
				<span><?= $arBanner['DETAIL_TEXT'] ?></span>
			</div>
			<?
			$i++;
		}
		?>
	</div>
</section>