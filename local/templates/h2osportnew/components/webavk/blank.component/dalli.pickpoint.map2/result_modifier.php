<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();


if(CModule::IncludeModule("webavk.dalli")) {

    $arResult = CDeliveryServiceDalliDriver::doGetAllPVZList();
    $arNew = array();
    $arAllowSymbols = array();
    foreach ($arResult as $k => $v) {
        if ($v['PARTNERID'] != "BOXBERRY") {
            unset($arResult[$k]);
            continue;
        }
        $ar = array();
        if (strlen($v['TOWN']) > 0 && strpos($v['ADDRESS'], $v['TOWN']) === false) {
            $ar[] = $v['TOWN'];
        }
        $ar[] = $v['ADDRESS'];
        $arResult[$k]['ADDRESS_FULL'] = implode(", ", $ar);
        $arNew[strtolower($v['TOWN'])]['ITEMS'][] = $arResult[$k];
        $arNew[strtolower($v['TOWN'])]['TOWN'] = $v['TOWN'];
        $symbol = strtoupper(substr($v['TOWN'], 0, 1));
        $arAllowSymbols[$symbol][strtolower($v['TOWN'])] = $v['TOWN'];
    }
    ksort($arNew, SORT_NATURAL);
    $arResult = array(
        "ITEMS" => $arNew,
        "ALLOW_SYMBOLS" => $arAllowSymbols,
        "CURRENT" => array(),
    );
}

if (strlen($arParams['CITY']) > 0) {
	$arResult["CURRENT"] = $arResult['ITEMS'][strtolower($arParams['~CITY'])]['ITEMS'];
	$arResult["CURRENT_CITY"] = htmlspecialcharsbx($arResult['ITEMS'][strtolower($arParams['~CITY'])]['TOWN']);
	$arResult['SELECTED_SYMBOL'] = strtoupper(substr($arParams['CITY'], 0, 1));
}
