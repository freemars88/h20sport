<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//myPrint($arResult["CURRENT_CITY"]);
if (strlen($arResult["CURRENT_CITY"]) > 0) {
	$APPLICATION->SetTitle("Доставка и оплата, город " . $arResult["CURRENT_CITY"]);
	$APPLICATION->SetPageProperty("title", "Экипировка для водных видов спорта по всей России. Информация о доставке и оплате, город " . $arResult["CURRENT_CITY"]);
	$APPLICATION->SetPageProperty("description", "Доставка и оплата город " . $arResult["CURRENT_CITY"] . " - Вы всегда можете отправить свой заказ или вопрос на почту info@h2osports.ru или позвонить по телефону +7 (965) 119-23-10 и мы с удовольствием ответим, проконсультируем и подберем все, что нужно.");
}