<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

	<div id="alphaDalliPickpont">
		<div class="alpha-list">
			<ul>
				<?
				$strSymbols = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ";
				$isSelected = false;
                for ($i = 0; $i < strlen($strSymbols); $i++) {
					$symbol = strtoupper(substr($strSymbols, $i, 1));
					$arClass = array();
					if (!isset($arResult['ALLOW_SYMBOLS'][$symbol])) {
						$arClass[] = "disabled";
					} elseif (!$isSelected && empty($arResult['SELECTED_SYMBOL'])) {
						$arClass[] = "selected";
						$isSelected = true;
						$arResult['SELECTED_SYMBOL'] = $symbol;
					} elseif (!empty($arResult['SELECTED_SYMBOL']) && $arResult['SELECTED_SYMBOL'] == $symbol) {
						$arClass[] = "selected";
						$isSelected = true;
					}
					?>
					<li>
						<a href="javascript:void(0)" class="<?= implode(",", $arClass) ?>" data-symbol="<?= $symbol ?>"><?= $symbol ?></a>
					</li>
					<?
				}
				?>
			</ul>
		</div>
		<?
		foreach ($arResult['ALLOW_SYMBOLS'] as $symbol => $arCities) {
			?>
			<div class="alpha-cities" data-symbol="<?= $symbol ?>"<?
			if ($arResult['SELECTED_SYMBOL'] == $symbol) {
				?> style="display:block;"<? } ?>>
				<ul>
					<?
					asort($arCities);
					foreach ($arCities as $strCity) {
						$arClass = array();
						if (strlen($arParams['CITY']) > 0 && strtolower($arParams['CITY']) == strtolower($strCity)) {
							$arClass[] = "selected";
						}
						?>
						<li>
							<a href="/delivery/<?= urlencode($strCity) ?>/" class="<?= implode(",", $arClass) ?>"><?= $strCity ?></a>
						</li>
						<?
					}
					?>
				</ul>
			</div>
			<?
		}
		?>
	</div>
<?
if (!empty($arResult['CURRENT'])) {
	?>
	<div id="mapDalliPickpont"></div>
	<script type="text/javascript">
		var pvzList =<?= CUtil::PhpToJSObject($arResult['CURRENT']) ?>;
		ymaps.ready(initMap);

		function initMap() {
			center =
				myMap = new ymaps.Map("mapDalliPickpont", {
					center: [50, 50],
					zoom: 15,
					controls: ['zoomControl', 'typeSelector', 'routeEditor', 'trafficControl', 'searchControl']
				});
			if (typeof pvzList != 'undefined') {
				myCollection = new ymaps.GeoObjectCollection();
				$.each(pvzList, function (index, value) {
					var balloonContent = '';
					if (typeof value.PARTNERID != 'undefined' && value.PARTNERID.length > 0) {
						if (value.PARTNERID == "DS") {
							value.PARTNERID = "DALLI-SERVICE";
						}
						balloonContent = balloonContent + '<div class="vidget_desc_row"><img src="/bitrix/images/dalliservicecom.delivery/' + value.PARTNERID + '.png" style="max-height:30px;" />' + '</div>';
					}
					if (typeof value.ADDRESS_FULL != 'undefined' && value.ADDRESS_FULL.length > 0) {
						balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>' + value.ADDRESS_FULL + '</div>';
					}
					if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0) {
						balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div><div>' + value.PHONE + '</div></div>';
					}
					if (typeof value.WORK_SHEDULE != 'undefined' && value.WORK_SHEDULE.length > 0) {
						balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>' + value.WORK_SHEDULE + '</div>';
					}
					balloonContent = balloonContent + '<p><a href="javascript:void(0)" class="b-pickup-link" data-id="' + value.ID + '">Подробнее...</a></p>';
					if (typeof value.GPS != 'undefined') {
						arrCoords = value.GPS.split(',')
					}
					sColor = '';
					if (value.PARTNERID == "BOXBERRY") {
						sColor = '#e51a4b';
					} else if (value.PARTNERID == "PICKUP") {
						sColor = '#3bbb98';
					} else if (value.PARTNERID == "SDEK") {
						sColor = '#06843c';
					} else if (value.PARTNERID == "DALLI-SERVICE") {
						sColor = '#0055ab';
					}
					var PVZPoint = new ymaps.Placemark([arrCoords['0'], arrCoords['1']], {
						hintContent: value.address,
						balloonContent: balloonContent
					}, {
						preset: "islands#dotCircleIcon",
						iconColor: sColor
					});
					/*
					 PVZPoint.events.add('click', function (e) {
					 if (typeof value.ADDRESS != 'undefined' && value.ADDRESS.length > 0)
					 address = '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>' + value.ADDRESS + '</div>';
					 else
					 address = '';
					 if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0)
					 phone = '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div>' + value.PHONE + '</div>';
					 else
					 phone = '';
					 if (typeof value.WORK_SHEDULE != 'undefined' && value.WORK_SHEDULE.length > 0)
					 shedule = '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>' + value.WORK_SHEDULE + '</div>';
					 else
					 shedule = '';
					 if (typeof value.DESCRIPTION != 'undefined' && value.DESCRIPTION.length > 0)
					 description = '<div class="vidget_desc_row"><div class="vidget_icon vidget_description"></div>' + value.DESCRIPTION + '</div>';
					 else
					 description = '';
					 if (typeof value.PARTNER != 'undefined' && value.PARTNER.length > 0)
					 partnername = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_partnername"></div>' + value.PARTNER + '</div>';
					 else
					 partnername = '';
					 if (typeof value.PARTNERID != 'undefined' && value.PARTNERID.length > 0)
					 partnerid = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_partnerid"></div>' + value.PARTNERID + '</div>';
					 else
					 partnerid = '';
					 if (typeof value.ID != 'undefined' && value.ID.length > 0)
					 id = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_id"></div>' + value.ID + '</div>';
					 else
					 id = '';
					 content = address + phone + shedule + description + partnername + id;
					 partners_info.innerHTML = content;
					 document.getElementById('confirmChooze').removeAttribute("disabled");
					 $('#confirmChooze').fadeTo(0, 1);
					 });
					 */
					myCollection.add(PVZPoint);
				});
				myMap.geoObjects.add(myCollection);
				myMap.setBounds(myCollection.getBounds(), {checkZoomRange: true}).then(function () {
					if (myMap.getZoom() > 14)
						myMap.setZoom(13);
				});
			}
		}
	</script>
	<div class="b-pickup-list">
		<div class="b-pickup-list-content row">
			<?
			$i = 0;
			$bIsShowStart = false;
			foreach ($arResult['CURRENT'] as $arItem)
			{
			?>
			<div class="b-pickup-list-item col-md-20" data-id="<?= $arItem['ID'] ?>">
				<div class="b-pickup-list-item-content">
					<div class="vidget_desc_row">
						<div class="vidget_icon vidget_address"></div><?= $arItem['ADDRESS_FULL'] ?></div>
					<div class="vidget_desc_row">
						<div class="vidget_icon vidget_phone"></div>
						<div><?= $arItem['PHONE'] ?></div>
					</div>
					<div class="vidget_desc_row">
						<div class="vidget_icon vidget_workshedule"></div><?= $arItem['WORK_SHEDULE'] ?></div>
					<div class="vidget_desc_row"><?= $arItem['DESCRIPTION'] ?></div>
				</div>
			</div>
			<?
			$i++;
			if ($i >= 6 && !$bIsShowStart)
			{
			$bIsShowStart = true;
			?>
		</div>
		<div class="clearfix"></div>
		<div id="b-deliveryall__area">
			<div id="b-deliveryall__area-button" class="text-center">
				<a href="javascript:void(0)" id="b-deliveryall__area-btn" class="button-one submit-button" data-text="Показать все">Показать
					все</a>
			</div>
			<div id="b-deliveryall__area-content">
				<div class="b-pickup-list-content row">
					<?
					}
					}
					if ($bIsShowStart)
					{
					?>
				</div>
			</div>
			<?
			}
			?>
		</div>
	</div>
	<?
}