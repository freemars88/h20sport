<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
global $APPLICATION;
$regionLevel = 0;
?>

<div class="places">
	<div class="row">
		<div class="col-md-12">
			<div class="places-menu">
				<div class="places-menu__title">Виды спорта</div>
				<ul class="places-menu__list">
					<li class="places-menu__list-item">
						<a href="/places/<?= (strlen($_REQUEST['region']) > 0 ? "?region=" . $_REQUEST['region'] : "") ?>" class="places-menu__list-link">Все</a>
					</li>
					<?
					foreach ($arResult['SPORT_TYPE'] as $arSection) {
						?>
						<li class="places-menu__list-item">
							<a href="/places/?sport=<?= $arSection['ID'] ?>" class="places-menu__list-link <?= ($arSection['SELECTED'] ? ' places-menu__list-link_selected' : '') ?>"><?= $arSection['NAME'] ?></a>
						</li>
						<?
					}
					?>
				</ul>
			</div>
			<div class="places-menu">
				<div class="places-menu__title">Регионы</div>
				<ul class="places-menu__list">
					<li class="places-menu__list-item">
						<a href="/places/<?= (strlen($_REQUEST['sport']) > 0 ? "?sport=" . $_REQUEST['sport'] : "") ?>">Все</a>
					</li>
					<?
					foreach ($arResult['REGIONS'] as $arRegion) {
						if ($arRegion['SELECTED']) {
							$regionLevel = 1;
							?>
							<li class="places-menu__list-item places-menu__list-item_open">
								<a href="/places/?region=<?= $arRegion['ID'] ?>" class="places-menu__list-link places-menu__list-link_selected"><?= $arRegion['NAME'] ?></a>
								<?
								if (isset($arRegion['ITEMS'])) {
									?>
									<ul class="places-menu__list">
										<?
										foreach ($arRegion['ITEMS'] as $arCity) {
											if ($arCity['SELECTED']) {
												$regionLevel = 2;
											}
											?>
											<li class="places-menu__list-item">
												<a href="?region=<?= $arCity['ID'] ?>" class="places-menu__list-link <?= ($arCity['SELECTED'] ? ' places-menu__list-link_selected' : '') ?>"><?= $arCity['NAME'] ?></a>
											</li>
											<?
										}
										?>
									</ul>
									<?
								}
								?>
							</li>
							<?
						} else {
							?>
							<li class="places-menu__list-item">
								<a href="/places/?region=<?= $arRegion['ID'] ?>" class="places-menu__list-link"><?= $arRegion['NAME'] ?></a>
							</li>
							<?
						}
					}
					?>
				</ul>
			</div>
		</div>
		<div class="col-md-3">&nbsp;</div>
		<div class="col-md-45">
			<?
			$fLon = 0;
			$fLat = 0;
			if (!empty($arResult['PLACE_DETAIL'])) {
				$arMaps = explode(",", $arResult['PLACE_DETAIL']['PROPERTY_PLACE_VALUE']);
				$lat = $arMaps[0];
				$lon = $arMaps[1];
				$fLon += $lon;
				$fLat += $lat;
				$arPlacemark[] = array(
					"LON" => $lon,
					"LAT" => $lat,
					"TEXT" => $arResult['PLACE_DETAIL']['~NAME'] . '<br/><a href="' . $arResult['PLACE_DETAIL']['DETAIL_PAGE_URL'] . '" class="detail-place" data-id="' . $arResult['PLACE_DETAIL']['ID'] . '">Подробнее...</a>',
				);
			} else {
				foreach ($arResult['PLACES'] as $arItem) {
					$arMaps = explode(",", $arItem['PROPERTY_PLACE_VALUE']);
					$lat = $arMaps[0];
					$lon = $arMaps[1];
					$fLon += $lon;
					$fLat += $lat;
					$arPlacemark[] = array(
						"LON" => $lon,
						"LAT" => $lat,
						"TEXT" => $arItem['~NAME'] . '<br/><a href="' . $arItem['DETAIL_PAGE_URL'] . '" class="detail-place" data-id="' . $arItem['ID'] . '">Подробнее...</a>',
					);
				}
				$fLon = $fLon / count($arResult['PLACES']);
				$fLat = $fLat / count($arResult['PLACES']);
			}
			?>
			<div class="row">
				<div class="col-md-60">
					<?
					$APPLICATION->IncludeComponent(
						"bitrix:map.google.view", "", Array(
							"API_KEY" => "AIzaSyBJzmozyXXaTQQhqRIB2tIf7eYJOF-4jjE",
							"CONTROLS" => array("SMALL_ZOOM_CONTROL", "TYPECONTROL", "SCALELINE"),
							"INIT_MAP_TYPE" => "ROADMAP",
							"MAP_DATA" => serialize(array(
								"google_lat" => $fLat,
								"google_lon" => $fLon,
								"google_scale" => ($regionLevel == 2 ? 10 : ($regionLevel == 1 ? 5 : 3)),
								"PLACEMARKS" => $arPlacemark,
							)),
							"MAP_HEIGHT" => "500",
							"MAP_ID" => "map1",
							"MAP_WIDTH" => "100%",
							"OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING", "ENABLE_KEYBOARD"),
						)
					);
					?>
				</div>
				<div class="col-md-60">
					<?
					if (!empty($arResult['PLACE_DETAIL'])) {
						$arPlace = $arResult['PLACE_DETAIL'];
						?>
						<div class="places-detail" data-id="<?= $arPlace['ID'] ?>" style="display:block;">
							<a name="item<?= $arPlace['ID'] ?>"></a>
							<div class="row">
								<?
								if ($arPlace['DETAIL_PICTURE'] > 0) {
									$strPath = CFile::GetPath($arPlace['DETAIL_PICTURE']);
									?>
									<div class="col-md-20">
										<img src="<?= $strPath ?>" style="max-width:100%;"/>
									</div>
									<div class="col-md-1">&nbsp;</div>
									<div class="col-md-39">
										<h1><?= (strlen($arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) > 0 ? $arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arPlace['~NAME']) ?></h1>
										<?= $arPlace['~DETAIL_TEXT'] ?>
									</div>
									<?
								} else {
									?>
									<div class="col-md-60">
										<h1><?= (strlen($arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) > 0 ? $arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arPlace['~NAME']) ?></h1>
										<?= $arPlace['~DETAIL_TEXT'] ?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
					} else {
						foreach ($arResult['PLACES'] as $arPlace) {
							?>
							<div class="places-detail" data-id="<?= $arPlace['ID'] ?>" style="<?= (count($arResult['PLACES']) == 1 ? 'display:block;' : '') ?>">
								<a name="item<?= $arPlace['ID'] ?>"></a>
								<div class="row">
									<?
									if ($arPlace['DETAIL_PICTURE'] > 0) {
										$strPath = CFile::GetPath($arPlace['DETAIL_PICTURE']);
										?>
										<div class="col-md-20">
											<img src="<?= $strPath ?>" style="max-width:100%;"/>
										</div>
										<div class="col-md-1">&nbsp;</div>
										<div class="col-md-39">
											<h3><?= $arPlace['~NAME'] ?></h3>
											<?= $arPlace['~DETAIL_TEXT'] ?>
										</div>
										<?
									} else {
										?>
										<div class="col-md-60">
											<h3><?= $arPlace['~NAME'] ?></h3>
											<?= $arPlace['~DETAIL_TEXT'] ?>
										</div>
										<?
									}
									?>
								</div>
							</div>
							<?
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
