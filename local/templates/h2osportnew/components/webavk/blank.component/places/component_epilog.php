<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
if (!empty($arResult['PLACE_DETAIL']))
{
	if (strlen($arResult['PLACE_DETAIL']['IPROPERTY_VALUES']['ELEMENT_META_TITLE']) > 0)
	{
		$APPLICATION->SetTitle($arResult['PLACE_DETAIL']['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
		$APPLICATION->SetPageProperty("title", $arResult['PLACE_DETAIL']['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
	}
	if (strlen($arResult['PLACE_DETAIL']['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']) > 0)
	{
		$APPLICATION->SetPageProperty("description", $arResult['PLACE_DETAIL']['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']);
	}
}
if (count($arResult['PLACES']) == 1)
{
	$ak = array_keys($arResult['PLACES']);
	LocalRedirect($arResult['PLACES'][$ak[0]]['DETAIL_PAGE_URL']);
}