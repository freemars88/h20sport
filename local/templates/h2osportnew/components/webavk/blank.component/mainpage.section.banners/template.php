<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
if (!function_exists("doShowMainpageSectionBannerIndex")) {
	function doShowMainpageSectionBannerIndex($arItem)
	{
		//myPrint($arItem);
		?>
		<div class="section-banners__content" style="background-image: url(<?= CFile::GetPath($arItem['UF_MAINPAGECAT_IMG']) ?>);">
			<div class="section-banners__content-text">
				<a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="section-banners__content-link"><?= (strlen($arItem['UF_MAINPAGECAT_TITLE']) > 0 ? $arItem['UF_MAINPAGECAT_TITLE'] : $arItem['NAME']) ?></a>
			</div>
		</div>
		<?
	}
}
?>
<section class="section-banners">
	<div class="container">
		<div class="row section-banners-row">
			<div class="col-30 col-xl-39">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][0]); ?>
			</div>
			<div class="col-30 col-xl-21 section-banners-col__ml">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][1]); ?>
			</div>
		</div>
		<div class="row section-banners-row">
			<div class="col-30 col-xl-21">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][2]); ?>
			</div>
			<div class="col-30 col-xl-39 section-banners-col__ml">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][3]); ?>
			</div>
		</div>
		<div class="row section-banners-row d-xl-none">
			<div class="col-30 col-xl-21">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][4]); ?>
			</div>
			<div class="col-30 col-xl-39 section-banners-col__ml">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][5]); ?>
			</div>
		</div>
		<div class="row section-banners-row d-xl-none">
			<div class="col-30 col-xl-21">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][6]); ?>
			</div>
			<div class="col-30 col-xl-39 section-banners-col__ml">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][7]); ?>
			</div>
		</div>
		<div class="row section-banners-row d-none d-xl-flex">
			<div class="col-39">
				<div class="row section-banners-row">
					<div class="col-30">
						<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][4]); ?>
					</div>
					<div class="col-30 section-banners-col__ml">
						<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][5]); ?>
					</div>
				</div>
				<div class="row section-banners-row">
					<div class="col-60">
						<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][6]); ?>
					</div>
				</div>
			</div>
			<div class="col-21 section-banners-col__ml">
				<? doShowMainpageSectionBannerIndex($arResult['ITEMS'][7]); ?>
			</div>
		</div>
	</div>
</section>

<section class="section-banners-mobile">
	<div class="container">
		<div class="section-banners-mobile-content owl-carousel owl-theme">
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][0]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][1]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][2]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][3]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][4]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][5]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][6]); ?></div>
			<div><? doShowMainpageSectionBannerIndex($arResult['ITEMS'][7]); ?></div>
		</div>
	</div>
</section>