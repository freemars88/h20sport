<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div id="mapDalliPickpont"></div>
<script type="text/javascript">
	var pvzList =<?= CUtil::PhpToJSObject($arResult) ?>;
	ymaps.ready(initMap);
	function initMap() {
		center =
				myMap = new ymaps.Map("mapDalliPickpont", {
					center: [50, 50],
					zoom: 15,
					controls: ['zoomControl', 'typeSelector', 'routeEditor', 'trafficControl', 'searchControl']
				});
		if (typeof pvzList != 'undefined') {
			myCollection = new ymaps.GeoObjectCollection();
			$.each(pvzList, function (index, value) {
				var balloonContent = '';
				if (typeof value.PARTNERID != 'undefined' && value.PARTNERID.length > 0) {
					if (value.PARTNERID == "DS")
					{
						value.PARTNERID = "DALLI-SERVICE";
					}
					balloonContent = balloonContent + '<div class="vidget_desc_row"><img src="/bitrix/images/dalliservicecom.delivery/' + value.PARTNERID + '.png" style="max-height:30px;" />' + '</div>';
				}
				if (typeof value.ADDRESS_FULL != 'undefined' && value.ADDRESS_FULL.length > 0) {
					balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>' + value.ADDRESS_FULL + '</div>';
				}
				if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0) {
					balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div><div>' + value.PHONE + '</div></div>';
				}
				if (typeof value.WORK_SHEDULE != 'undefined' && value.WORK_SHEDULE.length > 0) {
					balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>' + value.WORK_SHEDULE + '</div>';
				}
				if (typeof value.GPS != 'undefined') {
					arrCoords = value.GPS.split(',')
				}
				sColor = '';
				if (value.PARTNERID == "BOXBERRY")
				{
					sColor = '#e51a4b';
				} else if (value.PARTNERID == "PICKUP")
				{
					sColor = '#3bbb98';
				} else if (value.PARTNERID == "SDEK")
				{
					sColor = '#06843c';
				} else if (value.PARTNERID == "DALLI-SERVICE")
				{
					sColor = '#0055ab';
				}
				var PVZPoint = new ymaps.Placemark([arrCoords['0'], arrCoords['1']], {
					hintContent: value.address,
					balloonContent: balloonContent
				}, {
					preset: "islands#dotCircleIcon",
					iconColor: sColor
				});
				/*
				 PVZPoint.events.add('click', function (e) {
				 if (typeof value.ADDRESS != 'undefined' && value.ADDRESS.length > 0)
				 address = '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>' + value.ADDRESS + '</div>';
				 else
				 address = '';
				 if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0)
				 phone = '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div>' + value.PHONE + '</div>';
				 else
				 phone = '';
				 if (typeof value.WORK_SHEDULE != 'undefined' && value.WORK_SHEDULE.length > 0)
				 shedule = '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>' + value.WORK_SHEDULE + '</div>';
				 else
				 shedule = '';
				 if (typeof value.DESCRIPTION != 'undefined' && value.DESCRIPTION.length > 0)
				 description = '<div class="vidget_desc_row"><div class="vidget_icon vidget_description"></div>' + value.DESCRIPTION + '</div>';
				 else
				 description = '';
				 if (typeof value.PARTNER != 'undefined' && value.PARTNER.length > 0)
				 partnername = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_partnername"></div>' + value.PARTNER + '</div>';
				 else
				 partnername = '';
				 if (typeof value.PARTNERID != 'undefined' && value.PARTNERID.length > 0)
				 partnerid = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_partnerid"></div>' + value.PARTNERID + '</div>';
				 else
				 partnerid = '';
				 if (typeof value.ID != 'undefined' && value.ID.length > 0)
				 id = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_id"></div>' + value.ID + '</div>';
				 else
				 id = '';
				 content = address + phone + shedule + description + partnername + id;
				 partners_info.innerHTML = content;
				 document.getElementById('confirmChooze').removeAttribute("disabled");
				 $('#confirmChooze').fadeTo(0, 1);
				 });
				 */
				myCollection.add(PVZPoint);
			});
			myMap.geoObjects.add(myCollection);
			myMap.setBounds(myCollection.getBounds(), {checkZoomRange: true}).then(function () {
				if (myMap.getZoom() > 14)
					myMap.setZoom(13);
			});
		}
	}
</script>