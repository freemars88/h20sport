<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
use \Bitrix\Main;


global $APPLICATION;
$GLOBALS['CATALOG_CURRENT_SECTION_ELEMENTS_ID'] = $arResult['LIST_ID'];
//myPrint($arResult);
//$APPLICATION->SetPageProperty("navpages",$arResult["NAV_STRING"]);

if ($arResult["ID"] > 0)
{
	if ($arParams['~DATA_FILTER'] == "N")
	{
		$arParams['~DATA_FILTER'] = array();
	}
	if (is_array($arParams['~DATA_FILTER']) && strlen($arParams['~DATA_FILTER']['PROPERTY_H1_TITLE_VALUE']) > 0)
	{
		$APPLICATION->SetPageProperty("page_title", $arParams['~DATA_FILTER']['PROPERTY_H1_TITLE_VALUE']);
		$APPLICATION->SetTitle($arParams['~DATA_FILTER']['PROPERTY_H1_TITLE_VALUE'], $this->storage['TITLE_OPTIONS']);
	} else
	{
		if (strlen($arResult['UF_H1'])>0)
		{
			$APPLICATION->SetTitle($arResult['UF_H1'], $this->storage['TITLE_OPTIONS']);
		} elseif ($arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] != '')
		{
			$APPLICATION->SetTitle($arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $this->storage['TITLE_OPTIONS']);
		} elseif (isset($arResult['NAME']))
		{
			$APPLICATION->SetTitle($arResult['NAME'], $this->storage['TITLE_OPTIONS']);
		}
	}
	if (is_array($arParams['~DATA_FILTER']) && strlen($arParams['~DATA_FILTER']['PROPERTY_TITLE_PAGE_VALUE']) > 0)
	{
		$APPLICATION->SetPageProperty("title", $arParams['~DATA_FILTER']['PROPERTY_TITLE_PAGE_VALUE']);
		//$APPLICATION->SetTitle($arParams['~DATA_FILTER']['PROPERTY_TITLE_PAGE_VALUE']);
	} else
	{
		$browserTitle = Main\Type\Collection::firstNotEmpty(
						$arResult, $this->arParams['BROWSER_TITLE'],$arResult['IPROPERTY_VALUES'], 'SECTION_META_TITLE'
		);
		if (is_array($browserTitle))
		{
			$APPLICATION->SetPageProperty('title', implode(' ', $browserTitle), $this->storage['TITLE_OPTIONS']);
		} elseif ($browserTitle != '')
		{
			$APPLICATION->SetPageProperty('title', $browserTitle, $this->storage['TITLE_OPTIONS']);
		}
	}
	if (is_array($arParams['~DATA_FILTER']) && strlen($arParams['~DATA_FILTER']['PROPERTY_META_DESCRIPTION_VALUE']) > 0)
	{
		$APPLICATION->SetPageProperty("description", $arParams['~DATA_FILTER']['PROPERTY_META_DESCRIPTION_VALUE']);
	} else
	{
		$metaDescription = Main\Type\Collection::firstNotEmpty(
						$arResult, $this->arParams['META_DESCRIPTION'], $arResult['IPROPERTY_VALUES'], 'SECTION_META_DESCRIPTION'
		);
		if (is_array($metaDescription))
		{
			$APPLICATION->SetPageProperty('description', implode(' ', $metaDescription), $this->storage['TITLE_OPTIONS']);
		} elseif ($metaDescription != '')
		{
			$APPLICATION->SetPageProperty('description', $metaDescription, $this->storage['TITLE_OPTIONS']);
		}
	}
	if (is_array($arParams['~DATA_FILTER']) && strlen($arParams['~DATA_FILTER']['DETAIL_TEXT']) > 0)
	{
		$APPLICATION->SetPageProperty("SECTION_DESCRIPTION", $arParams['~DATA_FILTER']['DETAIL_TEXT']);
	} else
	{
		$APPLICATION->SetPageProperty("SECTION_DESCRIPTION", $arResult['DESCRIPTION']);
	}
}

if (strlen($_REQUEST['bxajaxid']) > 0)
{
	$strFullText = $APPLICATION->GetProperty("SECTION_DESCRIPTION");
	//$arDescr = explode("<BREAK />", $strFullText);
	$arData = array(
		"H1" => $APPLICATION->GetProperty("page_title"),
		"TITLE" => $APPLICATION->GetTitle(),
		"META_DESCRIPTION" => $APPLICATION->GetProperty("description"),
		"LINK_ADMIN_FILTER_EDIT" => $APPLICATION->GetProperty("LINK_TO_ADMIN_CHANGE_FILTER_DESCRIPTIONS"),
		"SECTION_DESCRIPTION"=>$strFullText
	);
	?>
	<script type="text/javascript">
		<!--
		$(document).ready(function () {
			var dataMeta =<?= CUtil::PhpToJSObject($arData) ?>;
			$("h1").html(dataMeta.H1);
			$("title").html(dataMeta.TITLE);
			$("#section_description").html(dataMeta.SECTION_DESCRIPTION);
			$("meta[name='description']").attr("content", dataMeta.META_DESCRIPTION);
			$("#change-filter-descriptions").html(dataMeta.LINK_ADMIN_FILTER_EDIT);
		});
	-->
	</script>
	<?
}