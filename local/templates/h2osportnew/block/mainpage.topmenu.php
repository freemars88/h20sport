<?
$APPLICATION->IncludeComponent(
	"bitrix:menu", "top", Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"IS_MAIN_PAGE" => ($APPLICATION->GetCurPage(true) == "/index.php" ? "Y" : "N"),
	)
);
