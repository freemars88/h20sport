<section class="brands-top">
	<div class="container">
		<div class="row">
			<div class="col-60">
				<h2><span>Наши бренды</span></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-60">
				<?
				$GLOBALS['arrFilterBarnd'] = array(
					"PROPERTY_SHOW_IN_MAINPAGE" => "Y",
				);
				$APPLICATION->IncludeComponent("bitrix:news.line", "mainpage.brands", Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
					"CACHE_GROUPS" => "Y", // Учитывать права доступа
					"CACHE_TIME" => "300", // Время кеширования (сек.)
					"CACHE_TYPE" => "A", // Тип кеширования
					"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
					"FIELD_CODE" => array(// Поля
						0 => "NAME",
						1 => "PREVIEW_TEXT",
						2 => "PREVIEW_PICTURE",
						3 => "",
					),
					"IBLOCKS" => array(// Код информационного блока
						0 => "20",
					),
					"IBLOCK_TYPE" => "references", // Тип информационного блока
					"NEWS_COUNT" => "20", // Количество новостей на странице
					"SORT_BY1" => "RAND", // Поле для первой сортировки новостей
					"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
					"SORT_ORDER1" => "ASC", // Направление для первой сортировки новостей
					"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
					"FILTER_NAME" => "arrFilterBarnd",
				), false
				);
				?>
			</div>
		</div>
	</div>
</section>