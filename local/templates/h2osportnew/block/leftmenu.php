<?
$APPLICATION->IncludeComponent(
	"bitrix:menu", "mainpage.left", Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left2",
		"DELAY" => "N",
		"MAX_LEVEL" => "3",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "left2",
		"USE_EXT" => "Y",
		"IS_MAINPAGE" => (strpos($APPLICATION->GetCurPage(true), "/index.php") === 0 ? "Y" : "N"),
	)
);
