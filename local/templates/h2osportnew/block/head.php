<title><? $APPLICATION->ShowTitle(); ?></title>
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<meta name="google-site-verification" content="xXiXfVBiBtexfKh4BXxtUqVGcsAhzTQ54tP_QBK3MRQ"/>
<?
$bXhtmlStyle = true;
echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . ($bXhtmlStyle ? ' /' : '') . '>' . "\n";
$APPLICATION->ShowMeta("robots", false, $bXhtmlStyle);
$APPLICATION->ShowMeta("description", false, $bXhtmlStyle);
$APPLICATION->ShowLink("canonical", NULL, $bXhtmlStyle);
$APPLICATION->ShowCSS(true, $bXhtmlStyle);
$APPLICATION->ShowHeadStrings();
$APPLICATION->ShowHeadScripts();
//ob_start();
if($canonicalLink = getCanonicalLink())
    $APPLICATION->SetPageProperty('canonical', $canonicalLink);
?>
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119127502-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}

	gtag('js', new Date());

	gtag('config', 'UA-119127502-1');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function () {
			try {
				w.yaCounter48772145 = new Ya.Metrika2({
					id: 48772145,
					clickmap: true,
					trackLinks: true,
					accurateTrackBounce: true,
					webvisor: true,
					trackHash: true,
					ecommerce: "dataLayer"
				});
			} catch (e) {
			}
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () {
				n.parentNode.insertBefore(s, n);
			};
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/tag.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else {
			f();
		}
	})(document, window, "yandex_metrika_callbacks2");
</script>
<!-- /Yandex.Metrika counter -->
<?
if (IS_DEV_SITE !== true) {
	?>
	<script type="text/javascript">!function () {
			var t = document.createElement("script");
			t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?159", t.onload = function () {
				VK.Retargeting.Init("VK-RTRG-288931-23aoJ"), VK.Retargeting.Hit()
			}, document.head.appendChild(t)
		}();</script>
	<?
}?>

<!-- Global site tag (gtag.js) - Google Ads: 1027698379 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1027698379"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1027698379');
</script>

<?
/*
$cont=ob_get_contents();
ob_end_clean();
?>
<script type="text/javascript">
	$(window).load(function () {
		window.setTimeout(function(){
			var arCont =<?=CUtil::PhpToJSObject(array("text" => $cont))?>;
			$("body").append($(arCont.text));
		}, 1500);
	});
</script>
*/