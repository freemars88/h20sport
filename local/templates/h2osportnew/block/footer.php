<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-60 col-lg-18">
				<? CWebavkTmplProTools::IncludeBlockFromDir("footer.contacts", SITE_TEMPLATE_PATH . "/include/"); ?>
				<div class="d-none d-lg-block">
					<? CWebavkTmplProTools::IncludeBlockFromDir("footer.sonet", SITE_TEMPLATE_PATH . "/include/"); ?>
				</div>
			</div>
			<div class="d-none d-lg-block col-lg-3"></div>
			<div class="col-60 col-lg-39 col-xl-30">
				<div class="row">
					<div class="col-60">
						<p class="footer-title">Будь в курсе наших новостей и событий:</p>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:sender.subscribe", "", Array(
								"AJAX_MODE" => "Y",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"CONFIRMATION" => "Y",
								"HIDE_MAILINGS" => "Y",
								"SET_TITLE" => "N",
								"SHOW_HIDDEN" => "N",
								"USER_CONSENT" => "N",
								"USER_CONSENT_ID" => "0",
								"USER_CONSENT_IS_CHECKED" => "Y",
								"USER_CONSENT_IS_LOADED" => "N",
								"USE_PERSONALIZATION" => "N",
							)
						);
						?>
						<div class="d-block d-lg-none">
							<? CWebavkTmplProTools::IncludeBlockFromDir("footer.sonet", SITE_TEMPLATE_PATH . "/include/"); ?>
						</div>
					</div>
					<div class="col-60 col-lg-18">
						<p class="footer-title">Магазин</p>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:menu", "bottom", Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_USE_GROUPS" => "N",
								"ROOT_MENU_TYPE" => "bottom1",
								"USE_EXT" => "Y",
							)
						);
						?>
					</div>
					<div class="d-none d-lg-block col-lg-6"></div>
					<div class="col-60 col-lg-18">
						<p class="footer-title">Мой кабинет</p>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:menu", "bottom", Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_USE_GROUPS" => "N",
								"ROOT_MENU_TYPE" => "bottom2",
								"USE_EXT" => "Y",
							)
						);
						?>
					</div>
					<div class="d-none d-lg-block col-lg-6"></div>
					<div class="col-60 col-lg-12">
						<p class="footer-title">Выгодно</p>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:menu", "bottom", Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_USE_GROUPS" => "N",
								"ROOT_MENU_TYPE" => "bottom3",
								"USE_EXT" => "Y",
							)
						);
						?>
					</div>
				</div>
			</div>
			<div class="d-none d-xl-block col-xl-3"></div>
			<div class="col-60 col-xl-6">
				<ul class="footer-payment">
					<li class="footer-payment__item footer-payment__item_visa"></li>
					<li class="footer-payment__item footer-payment__item_maestro"></li>
					<li class="footer-payment__item footer-payment__item_mastercard"></li>
					<li class="footer-payment__item footer-payment__item_mir"></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
