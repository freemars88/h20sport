<header class="header">
	<div class="container">
		<div class="row">
			<div class="col-15 d-lg-none"><a href="javascript:void(0);" class="header-menu__button"></a></div>
			<div class="col-30 col-lg-20 col-xl-12">
				<a class="logo header__logo" href="/"></a>
			</div>
			<div class="col-3 d-none d-lg-block"></div>
			<div class="col-18 d-none d-xl-block">
				<div class="header-phones">
					<span class="header-phones__whatsapp"></span>
					<span class="header-phones__viber"></span>
					<span class="header-phones__telegram"></span>
					<span class="header-phones__phone"><a href="tel:<?=MakePhoneNumber("+7 977 702 52-17")?>">+7 977 702 52-17</a></span>
				</div>
			</div>
			<div class="col-15 d-lg-none text-right">
				<a href="javascript:void(0);" class="header-search__button"></a>
			</div>
			<div class="col-33 col-lg-25 col-xl-15 d-none d-lg-block">
				<div class="header-phones">
					<span class="header-phones__call"></span>
					<span class="header-phones__phone"><a href="tel:<?=MakePhoneNumber("+7 800 777 14-24")?>">+7 800 777 14-24</a></span>
				</div>
			</div>
			<div class="col-60 col-lg-12">
				<hr class="full-width-hr full-width-hr__mb25 d-block d-lg-none b-menu-hr"/>
				<div class="header-user">
					<div class="header-user__item">
						<a href="/personal/" title="Авторизоваться" class="header-user__link header-user__link_personal"></a>
					</div>
					<div class="header-user__item">
						<?
						$APPLICATION->IncludeComponent(
							"webavk:blank.component", "header.wishlist", Array(
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "N",
							)
						);
						?>
					</div>
					<div class="header-user__item" id="header-cart-content">
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:sale.basket.basket.line", "header", Array(
								"HIDE_ON_BASKET_PAGES" => "N",
								"PATH_TO_AUTHORIZE" => "",
								"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
								"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
								"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
								"PATH_TO_PROFILE" => SITE_DIR . "personal/",
								"PATH_TO_REGISTER" => SITE_DIR . "login/",
								"POSITION_FIXED" => "N",
								"SHOW_AUTHOR" => "N",
								"SHOW_DELAY" => "N",
								"SHOW_EMPTY_VALUES" => "Y",
								"SHOW_IMAGE" => "Y",
								"SHOW_NOTAVAIL" => "N",
								"SHOW_NUM_PRODUCTS" => "Y",
								"SHOW_PERSONAL_LINK" => "N",
								"SHOW_PRICE" => "Y",
								"SHOW_PRODUCTS" => "Y",
								"SHOW_SUMMARY" => "Y",
								"SHOW_TOTAL_PRICE" => "Y",
							)
						);
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-menu-popup">
		<div class="container">
			<div class="row">
				<div class="col-15">
					<?
					$APPLICATION->IncludeComponent(
						"webavk:blank.component", "header.wishlist", Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "N",
						)
					);
					?>
				</div>
				<div class="col-30 text-center">
					<div class="header-popup-phones">
						<span class="header-popup-phones__call"></span>
						<span class="header-popup-phones__phone"><a href="tel:+78007771424">+7 800 777 14-24</a></span>
					</div>
				</div>
				<div class="col-15 text-right">
					<a href="javascript:void(0)" class="header-popup__close" title="Закрыть"></a>
				</div>
			</div>
			<hr class="full-width-hr full-width-hr__mb25"/>
			<div class="row">
				<div class="col-60">
					<a href="/personal/" title="Личный кабинет" class="header-user__link header-user__link_personal-popup">Личный
						кабинет</a>
				</div>
			</div>
			<hr class="full-width-hr full-width-hr__mb25"/>
			<div class="row">
				<div class="col-60 menu-mobile-top-col">
					<? CWebavkTmplProTools::IncludeBlockFromDir("mobile.topmenu"); ?>
				</div>
			</div>
		</div>
	</div>
	<div id="header-search-form-area" class="header-search-popup"></div>
</header>
<div class="header-search-popup__bg"></div>
<hr class="full-width-hr full-width-hr__mb25 d-none d-lg-block"/>
<?
/*if ($USER->IsAdmin() && $USER->GetID()==1) {
	?>
	<div class="d-block d-sm-none">XS</div>
	<div class="d-none d-sm-block d-md-none">SM</div>
	<div class="d-none d-md-block d-lg-none">MD</div>
	<div class="d-none d-lg-block d-xl-none">LG</div>
	<div class="d-none d-xl-block">XL</div>
<? }*/