<?
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/fonts.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/material-design.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/bootstrap.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/common.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/header.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/footer.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/menu.top.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/menu.mainpage.left.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/search.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/popular.area.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/tabs.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/owl.carousel.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/owl.theme.default.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/product.card.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/mainpage.section.banners.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/brand.card.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/mainpage.banners.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/navchain.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/catalog.pagenav.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/catsort.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/product.filter.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/recomended.content.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/prodcard.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/tabsblock.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/basket.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/articles.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/places.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/contacts.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/delivery.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/order.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/jquery.ui.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/lk.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/mobile.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/404.css", INCLUDE_MINIFY_WEB_CONTENT);
/*
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/gfonts.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/animate.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/jquery-ui.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/meanmenu.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/nivo-slider.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/preview.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/slick.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/lightbox.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/material-design-iconic-font.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/themefisher.font.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/fontello.font.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/default.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/style.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/shortcode.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/responsive.css", INCLUDE_MINIFY_WEB_CONTENT);


CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/styles.css", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddCSSFile(SITE_TEMPLATE_PATH . "/css/colors.css", INCLUDE_MINIFY_WEB_CONTENT);
*/
