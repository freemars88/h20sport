function declOfNum(number, titles) {
	cases = [2, 0, 1, 1, 1, 2];
	return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

function numeric_format(val, thSep, dcSep) {

	// Проверка указания разделителя разрядов
	if (!thSep)
		thSep = ' ';

	// Проверка указания десятичного разделителя
	if (!dcSep)
		dcSep = ',';

	var res = val.toString();
	var lZero = (val < 0); // Признак отрицательного числа

	// Определение длины форматируемой части
	var fLen = res.lastIndexOf('.'); // До десятичной точки
	fLen = (fLen > -1) ? fLen : res.length;

	// Выделение временного буфера
	var tmpRes = res.substring(fLen);
	var cnt = -1;
	for (var ind = fLen; ind > 0; ind--) {
		// Формируем временный буфер
		cnt++;
		if (((cnt % 3) === 0) && (ind !== fLen) && (!lZero || (ind > 1))) {
			tmpRes = thSep + tmpRes;
		}
		tmpRes = res.charAt(ind - 1) + tmpRes;
	}

	return tmpRes.replace('.', dcSep);

}

function intval(mixed_var, base) {
	var tmp;

	if (typeof (mixed_var) == 'string') {
		tmp = parseInt(mixed_var);
		if (isNaN(tmp)) {
			return 0;
		} else {
			return tmp.toString(base || 10);
		}
	} else if (typeof (mixed_var) == 'number') {
		return Math.floor(mixed_var);
	} else {
		return 0;
	}
}

$(document).ready(function () {
	$(".tabs").each(function () {
		var oTabs = $(this);
		var oTabsHeader = oTabs.find(".tabs__header");
		var oTabsContent = oTabs.find(".tabs__content");
		oTabsHeader.on("click", ".tabs-menu__item-link", function () {
			oTabsHeader.find(".tabs-menu__item-link_active").removeClass("tabs-menu__item-link_active");
			$(this).addClass("tabs-menu__item-link_active");
			oTabsContent.find(".tabs__item_active").removeClass("tabs__item_active");
			oTabsContent.find(".tabs__item[data-content='" + $(this).data("ident") + "']").addClass("tabs__item_active");
		});
	});
	$("#oneclick-phone").mask("+7 (999) 999-99-99");
	$('.product-card-carousel').owlCarousel({
		loop: true,
		margin: 0,
		nav: false,
		responsive: {
			0: {
				loop: true,
				stagePadding: 40,
				dots: false,
				items: 1
			},
			576: {
				items: 2
			},
			768: {
				items: 2
			},
			992: {
				items: 3
			},
			1500: {
				items: 4
			}
		}
	});

	var bannersMainpage = $('.mainpage-banners-carousel').owlCarousel({
		items: 1,
		loop: true,
		margin: 20,
		nav: false,
		autoplay: true,
		autoplayTimeout: 10000,
		autoplayHoverPause: false,
		dots: false
	});
	var bannersMainpageTitle = $(".mainpage-banners-carousel-title").owlCarousel({
		loop: false,
		margin: 0,
		nav: false,
		dots: false,
		responsive: {
			0: {
				items: 4
			},
			576: {
				items: 4
			},
			768: {
				items: 4
			},
			992: {
				items: 4
			},
			1500: {
				items: 4
			}
		}
	});
	bannersMainpage.on("translated.owl.carousel", function (event) {
		var index = $(event.target).find(".owl-item.active .mainpage-banners__link").data("index");
		bannersMainpageTitle.trigger("to.owl.carousel", [index, 300]);
		bannersMainpageTitle.find(".mainpage-banners__title_active").removeClass("mainpage-banners__title_active");
		bannersMainpageTitle.find(".mainpage-banners__title[data-index='" + index + "']").addClass("mainpage-banners__title_active");
		$(".mainpage-banners-carousel-title-small").html(bannersMainpageTitle.find(".mainpage-banners__title[data-index='" + index + "']").find("span").html());
	});
	bannersMainpageTitle.on("click", ".mainpage-banners__title", function () {
		bannersMainpage.trigger("to.owl.carousel", [$(this).data("index"), 300]);
		$(this).parents(".mainpage-banners-carousel-title").find(".mainpage-banners__title_active").removeClass("mainpage-banners__title_active");
		$(this).addClass("mainpage-banners__title_active");
	});

	var productCardMainPhotos = $('.prodcard-photos-carousel').owlCarousel({
		items: 1,
		loop: false,
		margin: 10,
		nav: false,
		autoplay: false,
		autoplayHoverPause: false,
		dots: false
	});
	var productCardSmallPhotos = $('.prodcard-photos-carousel-title').owlCarousel({
		items: 3,
		loop: false,
		margin: 15,
		nav: false,
		autoplay: false,
		autoplayHoverPause: false,
		dots: false
	});
	productCardMainPhotos.on("translated.owl.carousel", function (event) {
		var index = $(event.target).find(".owl-item.active .prodcard-photos__link").data("index");
		productCardSmallPhotos.trigger("to.owl.carousel", [index, 300]);
		productCardSmallPhotos.find(".prodcard-photos__title_active").removeClass("prodcard-photos__title_active");
		productCardSmallPhotos.find(".prodcard-photos__title[data-index='" + index + "']").addClass("prodcard-photos__title_active");
	});
	productCardSmallPhotos.on("click", ".prodcard-photos__title", function () {
		productCardMainPhotos.trigger("to.owl.carousel", [$(this).data("index"), 300]);
		$(this).parents(".prodcard-photos-carousel-title").find(".prodcard-photos__title_active").removeClass("prodcard-photos__title_active");
		$(this).addClass("prodcard-photos__title_active");
	});

	var productCardMobilePhotos = $('.prodcard-photos-mobile-carousel').owlCarousel({
		loop: false,
		margin: 15,
		nav: false,
		autoplay: false,
		autoplayHoverPause: false,
		dots: true,
		responsive: {
			0: {
				items: 1
			},
			576: {
				items: 1
			},
			768: {
				items: 2
			},
			992: {
				items: 2
			},
		}
	});

	var bannersMainpageSections = $(".section-banners-mobile-content").owlCarousel({
		loop: false,
		margin: 20,
		nav: false,
		dots: true,
		autoplay: true,
		autoplayHoverPause: true,
		responsive: {
			0: {
				loop: true,
				stagePadding: 40,
				dots: false,
				items: 1
			},
			576: {
				items: 1
			},
			768: {
				items: 1
			},
			992: {
				items: 2
			},
			1500: {
				items: 3
			}
		}
	});


	$("body").on("click", ".product-card__action-add-favorite", function () {
		var el = this;
		if (parseInt($(el).data("id")) > 0) {
			addToWishlist(el, $(el).data("id"), "product-card__action-add-favorite-active");
		}
	});

	$("body").on("click", ".product-card__action-add-basket", function () {
		var el = this;
		if (parseInt($(el).data("element-id")) > 0) {
			addToBasket($(el).data("element-id"), el, 1, 'product-card__action-add-basket-active', "");
		}
	});
	$("body").on("click", ".prodcard__buy", function () {
		var el = this;
		if (parseInt($(el).data("offer-id")) > 0) {
			addToBasket($(el).data("offer-id"), el, 1, 'prodcard__buy-active', "В корзине");
		}
	});
	$("body").on("click", ".add-to-basket", function () {
		var el = this;
		if (parseInt($(el).data("element-id")) > 0) {
			addToBasket($(el).data("element-id"), el, 1, 'add-to-basket-active', "");
		}
	});
	$("body").on("click", ".catalog-showmore", function () {
		$(this).parents(".catalog-pagenav__seealso-area").hide();
		$(".catalog-pagenav__seealso-loader").show();
		doShowMoreClick($(this));
	});
	$("body").on("mouseenter", ".menu-top__item", function () {
		if ($(window).width() < 992) {
			return false;
		}
		$(this).find(".menu-top-popup").show();
		$(this).find(".menu-top-popup").data("mousein", "Y");

		var mainPopup = $(this).find(".menu-top-popup");
		$(".popupmenu-lev1__link_active").removeClass("popupmenu-lev1__link_active");
		mainPopup.find(".popupmenu-lev2:visible").hide();
		mainPopup.find(".popupmenu-lev3:visible").hide();
		mainPopup.find(".popupmenu-lev4:visible").hide();
		var selEl = mainPopup.find(".popupmenu-lev1__link_selected");
		selEl.addClass("popupmenu-lev1__link_active");
		var lev2 = mainPopup.find(".popupmenu-lev2[data-sid='" + selEl.data("sid") + "']");
		if (lev2.length > 0) {
			mainPopup.find(".menu-top-popup-content__level2").show();
			lev2.show();

			var selEl2 = lev2.find(".popupmenu-lev2__link_selected");
			$(".popupmenu-lev2__link_active").removeClass("popupmenu-lev2__link_active");
			selEl2.addClass("popupmenu-lev2__link_active");
			var lev3 = mainPopup.find(".popupmenu-lev3[data-sid='" + selEl2.data("sid") + "']");
			if (lev3.length > 0) {
				mainPopup.find(".menu-top-popup-content__level3").show();
				lev3.show();

				var selEl3 = lev3.find(".popupmenu-lev3__link_selected");
				$(".popupmenu-lev3__link_active").removeClass("popupmenu-lev3__link_active");
				selEl3.addClass("popupmenu-lev3__link_active");
				var lev4 = mainPopup.find(".popupmenu-lev4[data-sid='" + selEl3.data("sid") + "']");
				if (lev4.length > 0) {
					mainPopup.find(".menu-top-popup-content__level4").show();
					lev4.show();
				} else {
					mainPopup.find(".menu-top-popup-content__level4").hide();
				}

			} else {
				mainPopup.find(".menu-top-popup-content__level3").hide();
			}
		} else {
			mainPopup.find(".menu-top-popup-content__level2").hide();
		}
	});
	$("body").on("mouseleave", ".menu-top__item", function () {
		if ($(window).width() < 992) {
			return false;
		}
		$(this).find(".menu-top-popup").data("mousein", "N");
		doStartPopupMenuLeave($(this).find(".menu-top-popup"));
	});
	$("body").on("mouseenter", ".menu-top-popup", function () {
		$(this).data("mousein", "Y");
	});
	$("body").on("mouseleave", ".menu-top-popup", function () {
		$(this).data("mousein", "N");
		doStartPopupMenuLeave($(this));
	});
	$("body").on("mouseenter", ".popupmenu-lev1__link", function () {
		var mainPopup = $(this).parents(".menu-top-popup:first");
		$(".popupmenu-lev1__link_active").removeClass("popupmenu-lev1__link_active");
		mainPopup.find(".popupmenu-lev2:visible").hide();
		mainPopup.find(".popupmenu-lev3:visible").hide();
		mainPopup.find(".popupmenu-lev4:visible").hide();
		$(this).addClass("popupmenu-lev1__link_active");
		var lev2 = mainPopup.find(".popupmenu-lev2[data-sid='" + $(this).data("sid") + "']");
		if (lev2.length > 0) {
			mainPopup.find(".menu-top-popup-content__level2").show();
			lev2.show();
		} else {
			mainPopup.find(".menu-top-popup-content__level2").hide();
		}
	});
	$("body").on("mouseenter", ".popupmenu-lev2__link", function () {
		var mainPopup = $(this).parents(".menu-top-popup:first");
		$(".popupmenu-lev2__link_active").removeClass("popupmenu-lev2__link_active");
		mainPopup.find(".popupmenu-lev3:visible").hide();
		mainPopup.find(".popupmenu-lev4:visible").hide();
		$(this).addClass("popupmenu-lev2__link_active");
		var lev3 = mainPopup.find(".popupmenu-lev3[data-sid='" + $(this).data("sid") + "']");
		if (lev3.length > 0) {
			mainPopup.find(".menu-top-popup-content__level3").show();
			lev3.show();
		} else {
			mainPopup.find(".menu-top-popup-content__level3").hide();
		}
	});
	$("body").on("mouseenter", ".popupmenu-lev3__link", function () {
		var mainPopup = $(this).parents(".menu-top-popup:first");
		$(".popupmenu-lev3__link_active").removeClass("popupmenu-lev3__link_active");
		mainPopup.find(".popupmenu-lev4:visible").hide();
		$(this).addClass("popupmenu-lev3__link_active");
		var lev4 = mainPopup.find(".popupmenu-lev4[data-sid='" + $(this).data("sid") + "']");
		if (lev4.length > 0) {
			mainPopup.find(".menu-top-popup-content__level4").show();
			lev4.show();
		} else {
			mainPopup.find(".menu-top-popup-content__level4").hide();
		}
	});
	$("body").on("click", ".catsort-field__link", function () {
		window.location = $(this).data("link");
	});

	$("body").on("click", ".header-menu__button", function () {
		$(".header-menu-popup").slideDown(100);
	});
	$("body").on("click", ".header-popup__close", function () {
		$(".header-menu-popup").hide();
	});
	$("body").on("click", ".menu-mobile-top__item-link", function () {
		var mainMobile = $(this).parents(".menu-mobile-top__item:first").find(".menu-mobile-top-mobile");
		if (mainMobile.length > 0) {
			mainMobile.toggle();
			return false;
		}
		return true;
	});
	$("body").on("click", ".mobilemenu-lev1__link_parent", function () {
		var subLevel = $(this).parents(".mobilemenu-lev1__item:first").find(".mobilemenu-lev2");
		if (subLevel.length > 0) {
			subLevel.toggle();
			return false;
		}
		return true;
	});
	$("body").on("click", ".mobilemenu-lev2__link_parent", function () {
		var subLevel = $(this).parents(".mobilemenu-lev2__item:first").find(".mobilemenu-lev3");
		if (subLevel.length > 0) {
			subLevel.toggle();
			return false;
		}
		return true;
	});
	$("body").on("click", ".mobilemenu-lev3__link_parent", function () {
		var subLevel = $(this).parents(".mobilemenu-lev3__item:first").find(".mobilemenu-lev4");
		if (subLevel.length > 0) {
			subLevel.toggle();
			return false;
		}
		return true;
	});
	$("body").on("click", ".header-search__button", function () {
		$(".header-search-popup").addClass("header-search-popup_active");
		$(".header-search-popup__bg").addClass("header-search-popup__bg_active");
	});
	$("body").on("click", ".header-search-popup__bg", function () {
		$(".header-search-popup").removeClass("header-search-popup_active");
		$(".header-search-popup__bg").removeClass("header-search-popup__bg_active");
	});

	$(".tabsblock").each(function () {
		var el = $(this);
		var title = el.find(".tabsblock__title");
		var content = el.find(".tabsblock__content");
		var i = 1;
		title.find(".tabsblock__title-item").each(function () {
			$(this).data("tabsindex", i);
			i++;
		});
		i = 1;
		content.find(".tabsblock__content-item").each(function () {
			$(this).data("tabsindex", i);
			i++;
		});
		title.find(".tabsblock__title-item").click(function () {
			title.find(".tabsblock__title-item_active").removeClass("tabsblock__title-item_active");
			content.find(".tabsblock__content-item_active").removeClass("tabsblock__content-item_active");
			$(this).addClass("tabsblock__title-item_active");
			var curindex = $(this).data("tabsindex");
			content.find(".tabsblock__content-item").each(function () {
				if ($(this).data("tabsindex") == curindex) {
					$(this).addClass("tabsblock__content-item_active");
				}
			});
		});
	});

	$("body").on("click", ".detail-place", function () {
		$(".places-detail").hide();
		$(".places-detail[data-id='" + $(this).data("id") + "']").show();
		return false;
	});

	$("#alphaDalliPickpont .alpha-list a").click(function () {
		if (!$(this).is(".disabled")) {
			$(".alpha-list a").removeClass("selected");
			$(this).addClass("selected");
			$(".alpha-cities").hide();
			$(".alpha-cities[data-symbol='" + $(this).data("symbol") + "']").show();
		}
	});

	$("body").on("click", ".b-pickup-link", function () {
		var self = this;
		$("#b-deliveryall__area-btn").trigger("click");
		$(".b-pickup-list-item").removeClass("active");
		$(".b-pickup-list-item[data-id='" + $(this).data("id") + "']").addClass("active");
		if ($(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").length != 0) {
			$('html, body').animate({scrollTop: $(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").offset().top - 80}, 500, function () {
				if ($(window).scrollTop() != $(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").offset().top - 80) {
					$('html, body').animate({scrollTop: $(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").offset().top - 80}, 500);
				}
			});
		}
	});

	$("body").on("click", "#b-deliveryall__area-btn", function () {
		var self = this;
		$("#b-deliveryall__area-button").hide();
		$("#b-deliveryall__area-content").show();
	});

	$("body").on("change", ".basket_qnt", function (el, ev) {
		if (parseInt($(this).val()) <= 0) {
			$(this).val(1)
		}
		doUpdateBasketData();
	});

	$(".del-from-basket").click(function () {
		var tbl = $("#basket_form");
		$.ajax("/ajax/actions.php", {
			data: "id=" + $(this).data("id") + "&ajaxAction=delBasket",
			context: $(this).parents("tr:first"),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok) {
					$(this).fadeOut(300, function () {
						$(this).remove();
						if (tbl.find(".basket-table tbody tr").length <= 0) {
							window.location.reload();
						}
					});
					//$(self.root).find(".checkout").html(data.checkout_text);
					//$(self.root).find(".b-pane__price span").html(data.allSum_FORMATED + ' <i class="fa fa-rub"></i>');
				}
				doUpdateBasketCnt();
			}
		});
	});

	$("#apply-coupon").click(function () {
		var tbl = $("#basket_form");
		BX.showWait(tbl, "");
		$.ajax("/ajax/actions.php", {
			data: {
				ajaxAction: "updateBasketCoupon",
				BasketRefresh: "Y",
				COUPON: $('#coupon').val()
			},
			dataType: "html",
			method: "POST",
			success: function (s) {
				doUpdateBasketData();
			},
			complete: function () {
				BX.closeWait();
			}
		});
		return false;
	});

	$("#checkout").click(function () {
		var tbl = $("#basket_form");
		tbl.submit();
	});
	$(".basket-table__body-minus").click(function () {

	});
	$(".basket-table__body-plus").click(function () {
		var qnt = $(this).parents(".basket-table__body-plusminus:first").find(".basket_qnt");
		var value = parseInt(qnt.val()) + 1;
		if (value > qnt.data("max")) {
			value = qnt.data("max");
		}
		if (value <= 0) {
			value = 1;
		}
		qnt.val(value);
		doUpdateBasketData();
		return false;
	});
	$(".basket-table__body-minus").click(function () {
		var qnt = $(this).parents(".basket-table__body-plusminus:first").find(".basket_qnt");
		var value = parseInt(qnt.val()) - 1;
		if (value <= 0) {
			value = 1;
		}
		qnt.val(value);
		doUpdateBasketData();
		return false;
	});

	$("body").on("change", "#offerconfirm", function () {
		if ($(this).is(":checked")) {
			$(".btn-order-save").removeClass("btn-disabled");
		} else {
			$(".btn-order-save").addClass("btn-disabled");
		}
	});
	$(window).resize(function () {
		doCheckResizeWindow();
	});
	doCheckResizeWindow();
	$(".prodcard-sel-size .prodcard-sel-size__item").click(function () {
		var link = $(this).find(".prodcard-sel-size__link");
		$(this).parents(".prodcard-sel-size__list:first").find(".prodcard-sel-size__item_active").removeClass("prodcard-sel-size__item_active");
		$(this).addClass("prodcard-sel-size__item_active");
		$(".prodcard-sel-size__title-current").html(link.html());
		$(".prodcard__buy").data("offer-id", link.data("offer-id"));
		$(".prodcard__oneclickbuy").data("offer-id", link.data("offer-id"));
		$(".prodcard__price").html(link.data("price"));
		$(".prodcard__price-old").html(link.data("old-price"));
		if (link.data("show-discount") == "Y") {
			$(".prodcard__price-old").show();
		} else {
			$(".prodcard__price-old").hide();
		}
	});
	$(".b-color-size-select.prodcard-sel-color .prodcard-sel-color__item").click(function () {
		var link = $(this).find(".prodcard-sel-color__link");
		$(this).parents(".prodcard-sel-color__list:first").find(".prodcard-sel-color__item_active").removeClass("prodcard-sel-color__item_active");
		$(this).addClass("prodcard-sel-color__item_active");
		$(".prodcard-sel-color__title-current").html(link.data("title"));
		var sizearea = $(".prodcard-sel-size");
		sizearea.find(".prodcard-sel-size__item").hide();
		sizearea.find(".prodcard-sel-size__item[data-color-id=" + link.data("color-id") + "]").show();
		sizearea.find(".prodcard-sel-size__item:visible:first").trigger("click");
	});
	$(".b-only-color-select.prodcard-sel-color .prodcard-sel-color__item").click(function () {
		var link = $(this).find(".prodcard-sel-color__link");
		$(this).parents(".prodcard-sel-color__list:first").find(".prodcard-sel-color__item_active").removeClass("prodcard-sel-color__item_active");
		$(this).addClass("prodcard-sel-color__item_active");
		$(".prodcard-sel-color__title-current").html(link.data("title"));
		$(".prodcard__buy").data("offer-id", link.data("offer-id"));
		$(".prodcard__oneclickbuy").data("offer-id", link.data("offer-id"));
		$(".prodcard__price").html(link.data("price"));
		$(".prodcard__price-old").html(link.data("old-price"));
		if (link.data("show-discount") == "Y") {
			$(".prodcard__price-old").show();
		} else {
			$(".prodcard__price-old").hide();
		}
	});

	$("body").on("click", "#b-onelicklink", function () {
		//$("#oneclickbuy").html($("#popup-element-oneclick").html());
		$("#oneclickbuy .product-oneclick-buy-button").data("id", $(".prodcard__buy").data("offer-id"));
	});
	$("body").on("click", ".product-oneclick-buy-button", function () {
		//var self = controlObj;
		var el = this;
		if ($(el).is(".disabled")) {
			return false;
		} else {
			if (parseInt($(el).data("id")) > 0 || true) {
				var error = false;
				var errorText = "";
				/*
				 if ($("#onclickbuy-wrapper #popup-element-oneclick-form .product-oneclick-buy-button").data("id").length < 1)
				 {
				 $("#onclickbuy-wrapper #popup-element-oneclick-form .product-size-content").addClass("error");
				 errorText = errorText + "Выберите размер<br>";
				 var error = true;
				 } else {
				 $("#onclickbuy-wrapper #popup-element-oneclick-form .product-size-content").removeClass("error");
				 }*/
				if ($("#oneclick-phone").val().length < 1) {
					$("#oneclick-phone").parents(".form-group:first").addClass("error");
					errorText = errorText + "Введите свой телефон<br>";
					var error = true;
				} else {
					$("#oneclick-phone").parents(".form-group:first").removeClass("error");
				}
				if ($("#oneclick-fio").val().length < 1) {
					$("#oneclick-fio").parents(".form-group:first").addClass("error");
					errorText = errorText + "Укажите свои ФИО";
					var error = true;
				} else {
					$("#oneclick-fio").parents(".form-group:first").removeClass("error");
				}
				if (!error) {
					$(".error-text").html("");
					$(".error-text").hide();
					//$.fancybox.close(true);
					$(el).attr("disabled", "disabled");
					$.ajax("/ajax/actions.php", {
						data: {
							id: $(el).data("id"),
							phone: $("#oneclick-phone").val(),
							fio: $("#oneclick-fio").val(),
							ajaxAction: 'oneClickBuy'
						},
						context: $(el),
						dataType: "json",
						method: "POST",
						success: function (data) {
							$(el).removeAttr("disabled");
							if (data.is_ok) {
								$("#oneclickbuy").html(data.title);
								//$("#onclickbuy .modal-product").html(data.title);
								//self.doShowModal("addbasketFormOk", data.title, data.message);
								//self.doUpdateBasketCnt();
								//self.doPopupBasketAdd(elementId);
								//$(el).removeClass("-is-active");
								//$(el).attr("href", "/personal/cart/");
							} else {
								$("#oneclickbuy").html(data.title);
							}
						}
					});
					return false;
				} else {
					$("#oneclickbuy .error-text").html(errorText);
					$("#oneclickbuy .error-text").show();
				}
			}
		}
	});
	$("body").on("click", ".del-favorite", function () {
		if (parseInt($(this).data("element-id")) > 0) {
			deleteFromWishlist($(this), $(this).data("element-id"));
		}
	});

	$("body").on("mouseenter", ".pro-rating a", function () {
		var proRating = $(this).parents(".pro-rating");
		var iIndex = parseInt($(this).data("id"));
		if (proRating.is(".actived")) {
			proRating.find("a").each(function (i, el) {
				var iStart = $(el).find("i");
				iStart.data("oldclass", iStart.attr("class"));
				if (parseInt($(el).data("id")) <= iIndex) {
					iStart.attr("class", "zmdi zmdi-star");
				} else {
					iStart.attr("class", "zmdi zmdi-star-outline");
				}
			});

		}
	});
	$("body").on("mouseleave", ".pro-rating a", function () {
		var proRating = $(this).parents(".pro-rating");
		if (proRating.is(".actived")) {
			proRating.find("a").each(function (i, el) {
				var iStart = $(el).find("i");
				iStart.attr("class", iStart.data("oldclass"));
				iStart.data("oldclass", "");
			});
		}
	});
	$("body").on("click", ".pro-rating a", function () {
		var proRating = $(this).parents(".pro-rating");
		if (proRating.is(".actived")) {
			$.ajax("/ajax/actions.php", {
				data: {
					"vote_id": proRating.data("id"),
					"vote": "Y",
					"rating": $(this).data("id"),
					"ajaxAction": "voteItem",
					"extclass": proRating.data("extclass"),
					"AJAX_CALL": "Y"
				},
				context: proRating,
				dataType: "json",
				method: "POST",
				success: function (data) {
					proRating.removeClass("actived");
					if (data.is_ok) {
						$(data.html).insertAfter(proRating);
						proRating.remove();
					}
				}
			});
		}
	});
});

function doCheckResizeWindow() {
	if ($(window).outerWidth() >= 992) {
		if ($("#title-search").parents("#search-form-area").length <= 0) {
			$("#search-form-area").append($("#title-search"));
		}
	} else {
		if ($("#title-search").parents("#search-form-area").length > 0) {
			$("#header-search-form-area").append($("#title-search"));
		}
	}
}

function addToWishlist(el, elementId, addClassActive) {
	var self = this;
	$.ajax("/ajax/actions.php", {
		data: "id=" + elementId + "&ajaxAction=addToWishlist",
		context: $(el),
		dataType: "json",
		method: "GET",
		success: function (data) {
			if (data.is_ok) {
				$(this).addClass(addClassActive);
				$.fancybox.open(data.message);
				//self.doUpdateWishListCnt();
			}
		}
	});
	return false;
};

function addToBasket(elementId, el, cnt, addClassActive, inBasketText) {

	$.ajax("/ajax/actions.php", {
		data: "id=" + elementId + "&cnt=" + cnt + "&ajaxAction=addToBasket",
		context: $(el),
		dataType: "json",
		method: "GET",
		success: function (data) {
			if (data.is_ok) {
				$(this).addClass(addClassActive);
				if (inBasketText != "") {
					$(this).html(inBasketText);
				}
				$.fancybox.open(data.message);
				if (typeof productOffersECommerce != 'undefined') {
					window.dataLayer.push({
						"ecommerce": {
							"currencyCode": "RUB",
							"add": {
								"products": [productOffersECommerce[elementId]]
							}
						}
					});
				}
				doUpdateBasketCnt();
			}
		}
	});
	return false;
};

function doUpdateBasketCnt() {
	$.ajax("/ajax/actions.php", {
		data: "ajaxAction=updateBasketCnt",
		dataType: "json",
		method: "GET",
		success: function (data) {
			$("#header-cart-content").html(data.content);
			//$(".basket-mobile-area").html(data.mobile);
			//tmpEl.before($(data));
			//tmpEl.remove();
		}
	});
	return false;
};

function doShowMoreClick(el) {
	var self = this;
	$.ajax($(el).data("href"), {
		data: "ajaxmore=Y",
		dataType: "html",
		method: "POST",
		success: function (data) {
			$("body").append('<div id="catalog-answer-content" style="display:none;"></div>');
			$("#catalog-answer-content").append(data);
			$("#catalog-section-items").append($("#catalog-answer-content").find("#catalog-section-items").html());
			$("#catalog-section-navs").html($("#catalog-answer-content").find("#catalog-section-navs:last").html());
			$("#catalog-answer-content").remove();
			$(".catalog-pagenav__seealso-loader").hide();
			$(".catalog-pagenav__seealso-area").show();
		}
	});
};

function doStartPopupMenuLeave(el) {
	var curPopup = el;
	window.setTimeout(function () {
		if (curPopup.data("mousein") != "Y") {
			curPopup.hide();
		}
	}, 300);
};

function doUpdateBasketData() {
	var tbl = $("#basket_form");
	var dataRequest = tbl.serialize() + "&ajaxAction=recheckBasket";
	BX.showWait(tbl, "");
	$.ajax("/ajax/actions.php", {
		data: dataRequest,
		context: tbl,
		dataType: "json",
		method: "GET",
		success: function (data) {
			if (data.is_ok) {
				$.each(data.items, function (k, row) {
					var rowTable = tbl.find("tr[data-id='" + k + "']");
					if (row.DISCOUNT_PRICE != 0) {
						rowTable.find(".basket-table__body-price-val").html('<span class="oldprice">' + row.FULL_PRICE_FORMATED + '</span>' + row.PRICE_FORMATED);
					} else {
						rowTable.find(".basket-table__body-price-val").html(row.PRICE_FORMATED);
					}
					//rowTable.find(".b-basket-col-name__sum").html(row.TOTAL_SUMM_FORMATED);
					if (rowTable.find(".basket_qnt").val() != row.QUANTITY) {
						rowTable.find(".basket_qnt").val(row.QUANTITY);
					}
				});
				doUpdateBasketCnt();
				//$(self.root).find(".checkout").html(data.checkout_text);
				tbl.find("#allSum_FORMATED").html(data.allSum_FORMATED);
				/*tbl.find("#PRICE_WITHOUT_DISCOUNT").html(data.PRICE_WITHOUT_DISCOUNT);
				tbl.find("#DISCOUNT_PRICE_ALL").html(data.DISCOUNT_PRICE_ALL_FORMATED);
				if (parseInt(data.DISCOUNT_PRICE_ALL) > 0) {
					$(".b-cart__totaldiscount").show();
				} else {
					$(".b-cart__totaldiscount").hide();
				}*/
				/*if (data.BASKET_CNT <= 4)
				{
					$("#checkout").removeClass("hide");
					$("#checkout-text").addClass("hide");
				} else {
					$("#checkout").addClass("hide");
					$("#checkout-text").removeClass("hide");
				}*/
			}
		},
		complete: function () {
			BX.closeWait();
		}
	});
};

function deleteFromWishlist(el, elementId) {

	$(el).attr("disabled", "disabled");
	$.ajax("/ajax/actions.php", {
		data: "id=" + elementId + "&ajaxAction=deleteFromWishlist",
		context: $(el),
		dataType: "json",
		method: "GET",
		success: function (data) {
			$(this).removeAttr("disabled");
			if (data.is_ok) {
				$(this).parents("tr").fadeOut(500, function () {
					$(this).parents("tr").remove();
				});
				//self.doUpdateWishListCnt();
			}
		}
	});
	return false;
};

