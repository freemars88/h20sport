<p class="footer-title-contacts">Контакты</p>
<ul class="footer-contacts">
	<li class="footer-contacts__item">
		<span class="footer-contacts__item-img footer-contacts__item-img_location"></span>
		<div>Москва, ул. Шолохова, 2Ас1</div>
	</li>
	<li class="footer-contacts__item">
		<span class="footer-contacts__item-img footer-contacts__item-img_phone"></span>
		<div>8 (800) 777-14-24, <br/>8 (977) 702-52-17</div>
	</li>
	<li class="footer-contacts__item">
		<span class="footer-contacts__item-img footer-contacts__item-img_mail"></span>
		<div>shop@h2osport.ru</div>
	</li>
</ul>
