<ul class="footer-sonet">
	<li class="footer-sonet__item">
		<a href="https://vk.com/h2osportshop" class="footer-sonet__item-link footer-sonet__item-link_vk" target="_blank" title="VKontakte"></a>
	</li>
	<li class="footer-sonet__item">
		<a href="https://www.facebook.com/h2osport.ru/" class="footer-sonet__item-link footer-sonet__item-link_fb" target="_blank" title="Facebook"></a>
	</li>
	<li class="footer-sonet__item">
		<a href="https://www.instagram.com/h2osport.ru" class="footer-sonet__item-link footer-sonet__item-link_ig" target="_blank" title="Instagram"></a>
	</li>
</ul>