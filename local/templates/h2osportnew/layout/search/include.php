<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!doctype html>
<html>
<head>
	<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
</head>
<body>
<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
<section class="top-menu">
	<div class="container">
		<div class="row">
			<div class="col-12" id="search-form-area">
				<? CWebavkTmplProTools::IncludeBlockFromDir("top.search"); ?>
			</div>
			<div class="col-3"></div>
			<div class="col-45">
				<? CWebavkTmplProTools::IncludeBlockFromDir("mainpage.topmenu"); ?>
			</div>
		</div>
	</div>
</section>
<hr class="full-width-hr full-width-hr__mt25 d-none d-sm-block">
<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="col-60">
				<?
				$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
					"START_FROM" => "0",
					"PATH" => "",
					"SITE_ID" => "-"
				), false, Array('HIDE_ICONS' => 'Y')
				);
				?>
			</div>
			<div class="col-12">
				<? CWebavkTmplProTools::IncludeBlockFromDir("leftmenu"); ?>
			</div>
			<div class="col-3"></div>
			<div class="col-45">
				<?
				if ($layoutArea == "header")
					goto TEMPLATE_END;
				FOOTER:
				?>

			</div>
		</div>
	</div>
</section>

<? CWebavkTmplProTools::IncludeBlockFromDir("brands.top"); ?>
<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
</body>
</html>
<?
TEMPLATE_END:


/*
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!doctype html>
<html class="no-js" lang="ru">
	<head>
		<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
	</head>
	<body id="body">
		<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
		<div class="wrapper bg-dark-white">
			<? CWebavkTmplProTools::IncludeBlockFromDir("header.top"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("content.search"); ?>
			<div class="heading-banner-area overlay-bg" style="background-image:url(<? $APPLICATION->ShowProperty("headingbanner") ?>);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="heading-banner">
								<div class="heading-banner-title">
									<h1><? $APPLICATION->ShowTitle(false) ?></h1>
								</div>
								<div class="breadcumbs pb-15">
									<?
									$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
										"START_FROM" => "0",
										"PATH" => "",
										"SITE_ID" => "-"
											), false, Array('HIDE_ICONS' => 'Y')
									);
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.category"); ?>
			<?
			if ($layoutArea == "header")
				goto TEMPLATE_END;
			FOOTER:
			?>
			<div class="brand-logo-area pt-80">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="brand">
								<div class="brand-slider">
									<?
									$GLOBALS['arrFilterBarnd'] = array(
										"PROPERTY_SHOW_IN_MAINPAGE" => "Y"
									);
									$APPLICATION->IncludeComponent("bitrix:news.line", "mainpage.brands", Array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
										"CACHE_GROUPS" => "Y", // Учитывать права доступа
										"CACHE_TIME" => "300", // Время кеширования (сек.)
										"CACHE_TYPE" => "A", // Тип кеширования
										"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
										"FIELD_CODE" => array(// Поля
											0 => "NAME",
											1 => "PREVIEW_TEXT",
											2 => "PREVIEW_PICTURE",
											3 => "",
										),
										"IBLOCKS" => array(// Код информационного блока
											0 => "20",
										),
										"IBLOCK_TYPE" => "references", // Тип информационного блока
										"NEWS_COUNT" => "5", // Количество новостей на странице
										"SORT_BY1" => "RAND", // Поле для первой сортировки новостей
										"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
										"SORT_ORDER1" => "ASC", // Направление для первой сортировки новостей
										"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
										"FILTER_NAME" => "arrFilterBarnd"
									), false
									);
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.subscribe"); ?>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.instagram"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("quick.view"); ?>
		</div>
	</body>
</html>
<?
TEMPLATE_END:
*/