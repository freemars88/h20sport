<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!doctype html>
<html>
<head>
	<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
</head>
<body>
<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
<section class="">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 d-none d-lg-block">
				<div id="search-form-area">
					<? CWebavkTmplProTools::IncludeBlockFromDir("top.search"); ?>
				</div>
				<? CWebavkTmplProTools::IncludeBlockFromDir("leftmenu"); ?>
			</div>
			<div class="col-3 d-none d-lg-block"></div>
			<div class="col-60 col-lg-45">
				<div class="row">
					<div class="col-60 d-none d-lg-block">
						<? CWebavkTmplProTools::IncludeBlockFromDir("mainpage.topmenu"); ?>
					</div>
					<div class="col-60">
						<? CWebavkTmplProTools::IncludeBlockFromDir("mainpage.banners"); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<? CWebavkTmplProTools::IncludeBlockFromDir("popular.tabs"); ?>
<? CWebavkTmplProTools::IncludeBlockFromDir("mainpage.sections.banners"); ?>
<? CWebavkTmplProTools::IncludeBlockFromDir("brands.top"); ?>
<?
if ($layoutArea == "header")
	goto TEMPLATE_END;
FOOTER:
?>
<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
</body>
</html>
<?
TEMPLATE_END:
