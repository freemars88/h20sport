<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!DOCTYPE html>
<html class="wide wow-animation smoothscroll" lang="ru">
	<head>
		<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
	</head>
	<body>
		<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
		<!-- Page-->
		<div class="page">
			<div id="page-loader">
				<div class="cssload-container">
					<div class="cssload-speeding-wheel"></div>
				</div>
			</div>
			<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
			<section class="breadcrumbs-custom">
				<div class="shell">
					<div class="breadcrumbs-custom__inner">
						<div class="breadcrumbs-custom__title"><h1><?= $APPLICATION->ShowTitle(false); ?></h1></div>
						<?
						$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
							"START_FROM" => "0",
							"PATH" => "",
							"SITE_ID" => "-"
								), false, Array('HIDE_ICONS' => 'Y')
						);
						?>
					</div>
				</div>
			</section>
			<section class="section-lg bg-white section-limit">
				<div class="shell">
			<?
			if ($layoutArea == "header")
				goto TEMPLATE_END;
			FOOTER:
			?>
				</div>
			</section>
			<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
		</div>
		<? CWebavkTmplProTools::IncludeBlockFromDir("global"); ?>
	</body>
</html>
<?
TEMPLATE_END:
