<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
if (!empty($arResult['BANNER']))
{
	?>
	<aside class="widget widget-banner hidden-sm">
		<div class="widget-info widget-banner-img">
			<a href="<?= $arResult['BANNER']['PROPERTIES']['LINK']['VALUE'] ?>"><img src="<?= CFile::GetPath($arResult['BANNER']['PREVIEW_PICTURE']) ?>" alt="" /></a>
		</div>
	</aside>
	<?
}