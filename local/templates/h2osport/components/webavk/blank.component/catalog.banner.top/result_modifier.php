<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$filterFields = array(
	'IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
	'IBLOCK_ACTIVE' => 'Y',
	'ACTIVE' => 'Y',
	'GLOBAL_ACTIVE' => 'Y',
);

if ($arParams['SECTION_ID'] > 0)
{
	$filterFields['ID'] = $arParams['SECTION_ID'];
	$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
	$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
	$sectionResult = $sectionIterator->GetNext();
} elseif (strlen($arParams['SECTION_CODE_PATH']) > 0)
{
	$sectionId = CIBlockFindTools::GetSectionIDByCodePath($arParams['CATALOG_IBLOCK_ID'], $arParams['SECTION_CODE_PATH']);
	if ($sectionId)
	{
		$filterFields['ID'] = $sectionId;
		$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
		$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
		$sectionResult = $sectionIterator->GetNext();
	}
} elseif (strlen($arParams['SECTION_CODE']) > 0)
{
	$filterFields['=CODE'] = $arParams['SECTION_CODE'];
	$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
	$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
	$sectionResult = $sectionIterator->GetNext();
} else
{
	$sectionResult = array(
		'ID' => 0,
		'IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
	);
}

$SECTION_ID = $sectionResult['ID'];
$ELEMENT_ID = false;
if ($arParams['ELEMENT_ID'] > 0)
{
	$ELEMENT_ID = $arParams['ELEMENT_ID'];
} elseif (strlen($arParams['ELEMENT_CODE']) > 0)
{
	$arElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams['CATALOG_IBLOCK_ID'], "ACTIVE" => "Y", "CODE" => $arParams['~ELEMENT_CODE'], "INCLUDE_SUBSECTIONS" => "Y", "SECTION_ID" => $SECTION_ID), false, false, array("ID"))->Fetch();
	if ($arElement)
	{
		$ELEMENT_ID = $arElement['ID'];
	}
}
if ($ELEMENT_ID > 0)
{
	$rBanner = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_SHOW_ELEMENTS" => $ELEMENT_ID, "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_CATALOG_TOP), false, false, array("ID"));
	if ($arBanner = $rBanner->Fetch())
	{
		$arResult['BANNER'] = getElementData($arBanner['ID']);
	}
}

if (empty($arResult['BANNER']))
{
	$rBanner = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_SHOW_CATEGORIES" => $SECTION_ID, "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_CATALOG_TOP), false, false, array("ID"));
	if ($arBanner = $rBanner->Fetch())
	{
		$arResult['BANNER'] = getElementData($arBanner['ID']);
	}
}
if (empty($arResult['BANNER']))
{
	$rNavChain = CIBlockSection::GetNavChain($arParams['CATALOG_IBLOCK_ID'], $SECTION_ID);
	while ($arNavChain = $rNavChain->Fetch())
	{
		$rBanner = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_SHOW_CATEGORIES" => $arNavChain['ID'], "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_CATALOG_TOP), false, false, array("ID"));
		if ($arBanner = $rBanner->Fetch())
		{
			$arResult['BANNER'] = getElementData($arBanner['ID']);
		}
	}
}
if (empty($arResult['BANNER']))
{
	$rBanner = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_CATALOG_TOP, "CODE" => $APPLICATION->GetCurPage(true)), false, false, array("ID"));
	if ($arBanner = $rBanner->Fetch())
	{
		$arResult['BANNER'] = getElementData($arBanner['ID']);
	}
}
if (empty($arResult['BANNER']))
{
	$rBanner = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_CATALOG_TOP, "CODE" => "default"), false, false, array("ID"));
	if ($arBanner = $rBanner->Fetch())
	{
		$arResult['BANNER'] = getElementData($arBanner['ID']);
	}
}