<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ID" => $arParams['ID']), false, false, array("ID", "NAME", "PROPERTY_SET_ELEMENTS"));
if ($arElement = $rElements->Fetch())
{
	$arProdCnt = array();
	foreach ($arElement['PROPERTY_SET_ELEMENTS_VALUE'] as $k => $v)
	{
		$arProdCnt[$v] = $arElement['PROPERTY_SET_ELEMENTS_DESCRIPTION'][$k];
	}
	ksort($arProdCnt, SORT_NUMERIC);
	$arElement['PRODUCTS_SET'] = $arProdCnt;
	$arResult = $arElement;
}
