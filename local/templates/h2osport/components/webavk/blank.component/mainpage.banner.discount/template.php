<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="discount-product-area">
	<div class="container">
		<div class="row">
			<div class="discount-product-slider dots-bottom-right">
				<?
				foreach ($arResult['BANNERS'] as $arBanner)
				{
					?>
					<div class="col-lg-12">
						<div class="discount-product">
							<img src="<?= CFile::GetPath($arBanner['DETAIL_PICTURE']) ?>" alt=""  />
							<?= str_replace("#LINK#", $arBanner['PROPERTY_LINK_VALUE'], $arBanner['DETAIL_TEXT']) ?>
						</div>
					</div>
					<?
				}
				?>
			</div>
		</div>
	</div>
</div>
