<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");
$rElements = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "ACTIVE_DATE" => "Y", "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_PRODUCT1), false, false, array("ID", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_LINK"));
if ($arElement = $rElements->Fetch())
{
	$arResult['BANNERS']['PRODUCT_1'] = $arElement;
}
$rElements = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "ACTIVE_DATE" => "Y", "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_PRODUCT2), false, false, array("ID", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_LINK"));
if ($arElement = $rElements->Fetch())
{
	$arResult['BANNERS']['PRODUCT_2'] = $arElement;
}
