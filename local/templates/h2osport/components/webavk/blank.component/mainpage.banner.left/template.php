<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="slider-banner">
	<?
	$i = 1;
	foreach ($arResult['BANNERS'] as $arBanner)
	{
		?>
		<div class="single-banner banner-<?= $i ?>">
			<a class="banner-thumb" href="<?= $arBanner['PROPERTY_LINK_VALUE'] ?>"><img src="<?= CFile::GetPath($arBanner['DETAIL_PICTURE']) ?>" alt="" /></a>
				<?= str_replace("#LINK#", $arBanner['PROPERTY_LINK_VALUE'], $arBanner['DETAIL_TEXT']) ?>
		</div>
		<?
		$i++;
	}
	?>
</div>
