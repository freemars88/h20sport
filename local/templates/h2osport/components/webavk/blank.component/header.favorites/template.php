<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<a href="/personal/wishlist/" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="tf-ion-ios-heart"></i><span>Избранное</span><?= ($arResult['PRODUCTS_CNT'] > 0 ? " (" . $arResult['PRODUCTS_CNT'] . ")" : "") ?></a>
<?
if (count($arResult['PRODUCTS']) > 0)
{
	?>
	<div class="dropdown-menu fav-dropdown">
		<?
		foreach ($arResult["PRODUCTS"] as $arItem)
		{
			$strPhoto = CFile::GetPath($arItem['PREVIEW_PICTURE']);
			?>
			<div class="media">
				<a class="pull-left" href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><img  class="media-object" src="<?= $strPhoto ?>" alt="<?= $arItem['NAME'] ?>" /></a>
				<div class="media-body">
					<h4 class="media-heading"><a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['NAME'] ?></a></h4>
				</div>
				<a href="javascript:void(0)" class="remove header-fav-remove" data-id="<?= $arItem['ID'] ?>" title="Удалить"><i class="tf-ion-close"></i></a>
			</div>	
			<?
		}
		?>
		<ul class="text-center cart-buttons">
			<li><a href="/personal/wishlist/" class="btn btn-small">Просмотреть все</a></li>
		</ul>
	</div>
<?
}