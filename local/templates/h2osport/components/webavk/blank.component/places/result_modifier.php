<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arResult = array(
	"SPORT_TYPE" => array(),
	"REGIONS" => array(),
	"PLACES" => array(),
	"PLACE_DETAIL" => false
);
CModule::IncludeModule("iblock");
$arFilterPlaces = array(
	"IBLOCK_ID" => $arParams['IBLOCK_ID'],
	"ACTIVE" => "Y"
);
$rElements = CIBlockElement::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID_SPORT'], "ACTIVE" => "Y"), false, false, array("ID", "NAME"));
while ($arElement = $rElements->GetNext())
{
	if ($arElement['ID'] == $arParams['SPORT_TYPE_ID'])
	{
		$arElement['SELECTED'] = true;
		$arFilterPlaces['PROPERTY_SPORT_TYPE'] = $arElement['ID'];
	}
	$arResult['SPORT_TYPE'][] = $arElement;
}

$arTreeLinks = array();
$rTree = CIBlockSection::GetList(array("left_margin" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y"));
while ($arTree = $rTree->GetNext())
{
	if ($arTree['ID'] == $arParams['REGION_ID'])
	{
		$arTree['SELECTED'] = true;
		$arFilterPlaces['SECTION_ID'] = $arTree['ID'];
		$arFilterPlaces['INCLUDE_SUBSECTIONS'] = "Y";
	}
	if ($arTree['IBLOCK_SECTION_ID'] > 0)
	{
		$arTreeLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']] = $arTree;
		$arTreeLinks[$arTree['ID']] = &$arTreeLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']];
		if ($arTree['SELECTED'])
		{
			$arTreeLinks[$arTree['IBLOCK_SECTION_ID']]["SELECTED"] = true;
		}
	} else
	{
		$arResult['REGIONS'][$arTree['ID']] = $arTree;
		$arTreeLinks[$arTree['ID']] = &$arResult['REGIONS'][$arTree['ID']];
	}
}
if (strlen($arParams['PLACE']) > 0)
{
	$arFilterPlaces = array(
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"ACTIVE" => "Y",
		"CODE" => $arParams['PLACE']
	);
	$rPlaces = CIBlockElement::GetList(array("SORT" => "ASC", "NAME" => "ASC"), $arFilterPlaces, false, false, array("ID", "NAME", "IBLOCK_ID", "DETAIL_PICTURE", "DETAIL_TEXT", "PROPERTY_SPORT_TYPE", "PROPERTY_PLACE", "DETAIL_PAGE_URL", "PROPERTY_SPORT_TYPE", "PROPERTY_PLACE"));
	if ($arPlace = $rPlaces->GetNext())
	{
		$ipropValues = new Bitrix\Iblock\InheritedProperty\ElementValues($arPlace['IBLOCK_ID'], $arPlace['ID']);
		$arPlace['IPROPERTY_VALUES'] = $ipropValues->getValues();
		
		$arResult['PLACE_DETAIL'] = $arPlace;
	}
	foreach ($arResult['SPORT_TYPE'] as $k => $v)
	{
		if (in_array($v['ID'], $arResult['PLACE_DETAIL']['PROPERTY_SPORT_TYPE_VALUE']))
		{
			$arResult['SPORT_TYPE'][$k]['SELECTED'] = true;
		}
	}
	//myPrint($arResult['PLACE_DETAIL']);
} else
{
	$rPlaces = CIBlockElement::GetList(array("SORT" => "ASC", "NAME" => "ASC"), $arFilterPlaces, false, false, array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_TEXT", "PROPERTY_SPORT_TYPE", "PROPERTY_PLACE", "DETAIL_PAGE_URL"));
	while ($arPlace = $rPlaces->GetNext())
	{
		$arResult['PLACES'][$arPlace['ID']] = $arPlace;
	}
}