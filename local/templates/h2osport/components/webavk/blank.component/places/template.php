<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
global $APPLICATION;
$regionLevel = 0;
?>
<div class="product-area single-pro-area pt-80 pb-80 product-style-2">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-12 col-xs-12 mt-tab-30">
				<aside class="widget widget-categories mb-30">
					<div class="widget-title">
						<div class="h4">Виды спорта</div>
					</div>
					<div id="cat-treeview-sport"  class="widget-info product-cat">
						<ul>
							<li><a href="/places/<?= (strlen($_REQUEST['region']) > 0 ? "?region=" . $_REQUEST['region'] : "") ?>">Все</a></li>
							<?
							foreach ($arResult['SPORT_TYPE'] as $arSection)
							{
								?>
								<li><a href="/places/?sport=<?= $arSection['ID'] ?>"<?= ($arSection['SELECTED'] ? ' class="selected"' : '') ?>><?= $arSection['NAME'] ?></a></li>
								<?
							}
							?>
						</ul>
					</div>
				</aside>
				<aside class="widget widget-categories mb-30">
					<div class="widget-title">
						<div class="h4">Регионы</div>
					</div>
					<div id="cat-treeview"  class="widget-info product-cat">
						<ul>
							<li><a href="/places/<?= (strlen($_REQUEST['sport']) > 0 ? "?sport=" . $_REQUEST['sport'] : "") ?>">Все</a></li>
							<?
							foreach ($arResult['REGIONS'] as $arRegion)
							{
								if ($arRegion['SELECTED'])
								{
									$regionLevel = 1;
									?>
									<li class="open"><a href="/places/?region=<?= $arRegion['ID'] ?>" class="selected"><?= $arRegion['NAME'] ?></a>
										<?
										if (isset($arRegion['ITEMS']))
										{
											?>
											<ul>
												<?
												foreach ($arRegion['ITEMS'] as $arCity)
												{
													if ($arCity['SELECTED'])
													{
														$regionLevel = 2;
													}
													?>
													<li><a href="?region=<?= $arCity['ID'] ?>"<?= ($arCity['SELECTED'] ? ' class="selected"' : '') ?>><?= $arCity['NAME'] ?></a></li>
													<?
												}
												?>
											</ul>
											<?
										}
										?>
									</li>
									<?
								} else
								{
									?>
									<li><a href="/places/?region=<?= $arRegion['ID'] ?>"><?= $arRegion['NAME'] ?></a></li>
									<?
								}
							}
							?>
						</ul>

					</div>
				</aside>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<?
				$fLon = 0;
				$fLat = 0;
				if (!empty($arResult['PLACE_DETAIL']))
				{
					$arMaps = explode(",", $arResult['PLACE_DETAIL']['PROPERTY_PLACE_VALUE']);
					$lat = $arMaps[0];
					$lon = $arMaps[1];
					$fLon += $lon;
					$fLat += $lat;
					$arPlacemark[] = array(
						"LON" => $lon,
						"LAT" => $lat,
						"TEXT" => $arResult['PLACE_DETAIL']['~NAME'] . '<br/><a href="' . $arResult['PLACE_DETAIL']['DETAIL_PAGE_URL'] . '" class="detail-place" data-id="' . $arResult['PLACE_DETAIL']['ID'] . '">Подробнее...</a>'
					);
				} else
				{
					foreach ($arResult['PLACES'] as $arItem)
					{
						$arMaps = explode(",", $arItem['PROPERTY_PLACE_VALUE']);
						$lat = $arMaps[0];
						$lon = $arMaps[1];
						$fLon += $lon;
						$fLat += $lat;
						$arPlacemark[] = array(
							"LON" => $lon,
							"LAT" => $lat,
							"TEXT" => $arItem['~NAME'] . '<br/><a href="' . $arItem['DETAIL_PAGE_URL'] . '" class="detail-place" data-id="' . $arItem['ID'] . '">Подробнее...</a>'
						);
					}
					$fLon = $fLon / count($arResult['PLACES']);
					$fLat = $fLat / count($arResult['PLACES']);
				}
				?>
				<?
				$APPLICATION->IncludeComponent(
						"bitrix:map.google.view", "", Array(
					"API_KEY" => "AIzaSyBJzmozyXXaTQQhqRIB2tIf7eYJOF-4jjE",
					"CONTROLS" => array("SMALL_ZOOM_CONTROL", "TYPECONTROL", "SCALELINE"),
					"INIT_MAP_TYPE" => "ROADMAP",
					"MAP_DATA" => serialize(array(
						"google_lat" => $fLat,
						"google_lon" => $fLon,
						"google_scale" => ($regionLevel == 2 ? 10 : ($regionLevel == 1 ? 5 : 3)),
						"PLACEMARKS" => $arPlacemark
					)),
					"MAP_HEIGHT" => "500",
					"MAP_ID" => "map1",
					"MAP_WIDTH" => "100%",
					"OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING", "ENABLE_KEYBOARD")
						)
				);
				?>
				<div class="clearfix"></div>
				<?
				if (!empty($arResult['PLACE_DETAIL']))
				{
					$arPlace = $arResult['PLACE_DETAIL'];
					?>
					<div class="detail-place-area" data-id="<?= $arPlace['ID'] ?>" style="display:block;">
						<a name="item<?= $arPlace['ID'] ?>"></a>
						<div class="row">
							<?
							if ($arPlace['DETAIL_PICTURE'] > 0)
							{
								$strPath = CFile::GetPath($arPlace['DETAIL_PICTURE']);
								?>
								<div class="col-md-4">
									<img src="<?= $strPath ?>" style="max-width:100%;"/>
								</div>
								<div class="col-md-8">
									<h1 class="tab-title title-border mb-30"><?= (strlen($arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) > 0 ? $arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arPlace['~NAME']) ?></h1>
									<?= $arPlace['~DETAIL_TEXT'] ?>	
								</div>
								<?
							} else
							{
								?>
								<div class="col-md-12">
									<h1 class="tab-title title-border mb-30"><?= (strlen($arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) > 0 ? $arPlace['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arPlace['~NAME']) ?></h1>
									<?= $arPlace['~DETAIL_TEXT'] ?>	
								</div>
								<?
							}
							?>
						</div>
					</div>
					<?
				} else
				{
					foreach ($arResult['PLACES'] as $arPlace)
					{
						?>
						<div class="detail-place-area" data-id="<?= $arPlace['ID'] ?>" style="<?= (count($arResult['PLACES']) == 1 ? 'display:block;' : '') ?>">
							<a name="item<?= $arPlace['ID'] ?>"></a>
							<div class="row">
								<?
								if ($arPlace['DETAIL_PICTURE'] > 0)
								{
									$strPath = CFile::GetPath($arPlace['DETAIL_PICTURE']);
									?>
									<div class="col-md-4">
										<img src="<?= $strPath ?>" style="max-width:100%;"/>
									</div>
									<div class="col-md-8">
										<h3 class="tab-title title-border mb-30"><?= $arPlace['~NAME'] ?></h3>
										<?= $arPlace['~DETAIL_TEXT'] ?>	
									</div>
									<?
								} else
								{
									?>
									<div class="col-md-12">
										<h3 class="tab-title title-border mb-30"><?= $arPlace['~NAME'] ?></h3>
										<?= $arPlace['~DETAIL_TEXT'] ?>	
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
					}
				}
				?>
			</div>
		</div>
	</div>
</div>
