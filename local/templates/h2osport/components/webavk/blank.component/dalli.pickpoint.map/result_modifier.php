<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();


CModule::IncludeModule("webavk.dalli");

$arResult = CDeliveryServiceDalliDriver::doGetAllPVZList();
foreach ($arResult as $k => $v)
{
	if ($v['PARTNERID'] != "BOXBERRY")
	{
		unset($arResult[$k]);
		continue;
	}
	$ar = array();
	if (strlen($v['TOWN']) > 0 && strpos($v['ADDRESS'], $v['TOWN']) === false)
	{
		$ar[] = $v['TOWN'];
	}
	$ar[] = $v['ADDRESS'];
	$arResult[$k]['ADDRESS_FULL'] = implode(", ", $ar);
}

