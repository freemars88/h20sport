<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="slider-area">
	<div class="bend niceties preview-2">
		<div id="ensign-nivoslider" class="slides">
			<?
			$i = 1;
			foreach ($arResult['BANNERS'] as $arBanner)
			{
				?>
				<a href="<?= $arBanner['PROPERTY_LINK_VALUE'] ?>" style="z-index:100;">
					<img src="<?= CFile::GetPath($arBanner['DETAIL_PICTURE']) ?>" alt="" title="#slider-direction-<?= $i ?>"  />
				</a>
				<?
				$i++;
			}
			?>
		</div>
		<?
		$i = 1;
		foreach ($arResult['BANNERS'] as $arBanner)
		{
			?>
			<div id="slider-direction-<?= $i ?>" class="t-cn slider-direction">
				<div class="slider-progress"></div>
				<div class="slider-content t-lfl s-tb slider-1">
					<div class="title-container s-tb-c title-compress">
						<?= str_replace("#LINK#", $arBanner['PROPERTY_LINK_VALUE'], $arBanner['DETAIL_TEXT']) ?>
					</div>
				</div>	
			</div>
			<?
			$i++;
		}
		?>
	</div>
</section>
