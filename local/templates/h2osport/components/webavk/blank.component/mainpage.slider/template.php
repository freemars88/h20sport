<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? /*
  <section class="main-slider">
  <div style="max-width: 1140px; margin: 0 auto;">
  <div id="home_slider_wrapper" class="rev_slider_wrapper fullscreen-container" >
  <div id="home_slider" class="rev_slider fullscreenbanner tiny_bullet_slider" data-version="5.4.1">
  <ul>
  <?
  foreach ($arResult['BANNERS'] as $arBanner)
  {
  $strCode = $arBanner['CODE'];
  $strCode = str_replace("#ID#", $arBanner['ID'], $strCode);
  $strCode = str_replace("#IMG#", $arBanner['IMAGE_PATH'], $strCode);
  $strCode = str_replace("#LINK#", $arBanner['LINK'], $strCode);
  echo $strCode;
  }
  ?>
  </ul>
  <div class="tp-bannertimer" style="height: 10px; background: rgba(0, 0, 0, 0.15);"></div>
  </div>
  </div>
  </div>
  </section> */ ?>

<section class="slider-area slider-style-2">
	<div class="bend niceties preview-2">
		<div id="ensign-nivoslider" class="slides">
			<?
			$i = 1;
			foreach ($arResult['BANNERS'] as $arBanner)
			{
				?>
				<a href="<?= $arBanner['PROPERTY_LINK_VALUE'] ?>">
					<img src="<?= CFile::GetPath($arBanner['DETAIL_PICTURE']) ?>" alt="" title="#slider-direction-<?= $i ?>"  />
				</a>
				<?
				$i++;
			}
			?>
		</div>
		<?
		$i = 1;
		foreach ($arResult['BANNERS'] as $arBanner)
		{
			?>
			<div id="slider-direction-<?= $i ?>" class="t-cn slider-direction">
				<div class="slider-progress"></div>
				<div class="slider-content t-lfl s-tb slider-1">
					<div class="title-container s-tb-c title-compress">
						<?= str_replace("#LINK#", $arBanner['PROPERTY_LINK_VALUE'], $arBanner['DETAIL_TEXT']) ?>
					</div>
				</div>
			</div>
			<?
			$i++;
		}
		?>
	</div>
</section>