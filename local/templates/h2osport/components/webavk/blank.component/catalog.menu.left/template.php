<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!function_exists(""))
{

	function doShowCatalogLeftMenuTree(&$arSections)
	{
		?>
		<ul>
			<?
			foreach ($arSections as $arSection)
			{
				?>
				<li class="<?= ($arSection['IS_SELECTED'] ? "selected" : "") ?>"><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a>
					<?
					if (isset($arSection['ITEMS']) && !empty($arSection['ITEMS']) && $arSection['IS_SELECTED'])
					{
						doShowCatalogLeftMenuTree($arSection['ITEMS']);
					}
					?>
				</li>
				<?
			}
			?>
		</ul>
		<?
	}

}
doShowCatalogLeftMenuTree($arResult['SECTIONS']);

