<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$filterFields = array(
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'IBLOCK_ACTIVE' => 'Y',
	'ACTIVE' => 'Y',
	'GLOBAL_ACTIVE' => 'Y',
);

if ($arParams['SECTION_ID'] > 0)
{
	$filterFields['ID'] = $arParams['SECTION_ID'];
	$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
	$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
	$sectionResult = $sectionIterator->GetNext();
} elseif (strlen($arParams['SECTION_CODE']) > 0)
{
	$filterFields['=CODE'] = $arParams['SECTION_CODE'];
	$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
	$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
	$sectionResult = $sectionIterator->GetNext();
} elseif (strlen($arParams['SECTION_CODE_PATH']) > 0)
{
	$sectionId = CIBlockFindTools::GetSectionIDByCodePath($arParams['IBLOCK_ID'], $arParams['SECTION_CODE_PATH']);
	if ($sectionId)
	{
		$filterFields['ID'] = $sectionId;
		$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
		$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
		$sectionResult = $sectionIterator->GetNext();
	}
} else
{
	$sectionResult = array(
		'ID' => 0,
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	);
}
$SECTION_ID = $sectionResult['ID'];

$arNewSections = array();
$arLinks = array();
$rTree = CIBlockSection::GetTreeList(array("IBLOCK_ID" => $arParams['IBLOCK_ID']));
while ($arTree = $rTree->GetNext())
{
	if (!isset($arLinks[$arTree['IBLOCK_SECTION_ID']]))
	{
		$arNewSections[$arTree['ID']] = $arTree;
		$arLinks[$arTree['ID']] = &$arNewSections[$arTree['ID']];
	} else
	{
		$arLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']] = $arTree;
		$arLinks[$arTree['ID']] = &$arLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']];
	}
}

if ($SECTION_ID <= 0)
{
	$fromSection = 0;
} elseif (isset($arLinks[$SECTION_ID]['ITEMS']))
{
	$fromSection = $arLinks[$SECTION_ID]['IBLOCK_SECTION_ID'];
} else
{
	$fromSection = $arLinks[$SECTION_ID]['IBLOCK_SECTION_ID'];
	if ($fromSection > 0)
	{
		$fromSection = $arLinks[$fromSection]['IBLOCK_SECTION_ID'];
	}
}

if ($fromSection > 0)
{
	$arData = $arLinks[$fromSection]['ITEMS'];
} else
{
	$arData = $arNewSections;
}

foreach ($arData as $arSection)
{
	if ($arSection['ID'] == $SECTION_ID)
	{
		$arSection['IS_SELECTED'] = true;
	}
	$arResult["SECTIONS"][$arSection['ID']] = $arSection;
	if (isset($arResult["SECTIONS"][$arSection['ID']]['ITEMS']))
	{
		foreach ($arResult["SECTIONS"][$arSection['ID']]['ITEMS'] as $k => $arChildSection)
		{
			if ($arChildSection['ID'] == $SECTION_ID)
			{
				$arResult["SECTIONS"][$arSection['ID']]['ITEMS'][$k]['IS_SELECTED'] = true;
			}
			unset($arChildSection['ITEMS']);
		}
	}
}

