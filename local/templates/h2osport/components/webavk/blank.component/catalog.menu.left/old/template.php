<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<ul>
	<?
	foreach ($arResult['SECTIONS'] as $arSection)
	{
		?>
		<li class="<?= ($arSection['IS_SELECTED'] ? "selected" : "") ?>"><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a>
			<?
			if (isset($arSection['ITEMS']))
			{
				?>
				<ul>
					<?
					foreach ($arSection['ITEMS'] as $arChildSection)
					{
						?>
						<li class="<?= ($arChildSection['IS_SELECTED'] ? "selected" : "") ?>"><a href="<?= $arChildSection['SECTION_PAGE_URL'] ?>"><?= $arChildSection['NAME'] ?></a></li>
						<?
					}
					?>
				</ul>
				<?
			}
			?>
		</li>
		<?
	}
	?>
</ul>
