<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$filterFields = array(
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'IBLOCK_ACTIVE' => 'Y',
	'ACTIVE' => 'Y',
	'GLOBAL_ACTIVE' => 'Y',
);

if ($arParams['SECTION_ID'] > 0)
{
	$filterFields['ID'] = $arParams['SECTION_ID'];
	$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
	$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
	$sectionResult = $sectionIterator->GetNext();
} elseif (strlen($arParams['SECTION_CODE_PATH']) > 0)
{
	$sectionId = CIBlockFindTools::GetSectionIDByCodePath($arParams['IBLOCK_ID'], $arParams['SECTION_CODE_PATH']);
	if ($sectionId)
	{
		$filterFields['ID'] = $sectionId;
		$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
		$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
		$sectionResult = $sectionIterator->GetNext();
	}
} elseif (strlen($arParams['SECTION_CODE']) > 0)
{
	$filterFields['=CODE'] = $arParams['SECTION_CODE'];
	$sectionIterator = CIBlockSection::GetList(array(), $filterFields, false, $selectFields);
	$sectionIterator->SetUrlTemplates('', $arParams['SECTION_URL']);
	$sectionResult = $sectionIterator->GetNext();
} else
{
	$sectionResult = array(
		'ID' => 0,
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	);
}
$SECTION_ID = $sectionResult['ID'];

$arNewSections = array();
$arLinks = array();
/*
  if ($arParams['IS_SALE'] == "Y")
  {
  $rTree = CIBlockSection::GetTreeList(array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", 'IBLOCK_ACTIVE' => 'Y', "UF_IS_SALE" => "1"));
  $rTree->SetUrlTemplates("", "/sale/?s=#ID#");
  } else {
  $rTree = CIBlockSection::GetTreeList(array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", 'IBLOCK_ACTIVE' => 'Y'));
  }
 * 
 */
$rTree = CIBlockSection::GetTreeList(array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", 'IBLOCK_ACTIVE' => 'Y'));
while ($arTree = $rTree->GetNext())
{
	if (!isset($arLinks[$arTree['IBLOCK_SECTION_ID']]))
	{
		$arNewSections[$arTree['ID']] = $arTree;
		$arLinks[$arTree['ID']] = &$arNewSections[$arTree['ID']];
	} else
	{
		$arLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']] = $arTree;
		$arLinks[$arTree['ID']] = &$arLinks[$arTree['IBLOCK_SECTION_ID']]['ITEMS'][$arTree['ID']];
	}
}

if ($SECTION_ID > 0)
{
	$arLinks[$SECTION_ID]['IS_SELECTED'] = true;
}
$iSection = $arLinks[$SECTION_ID]['IBLOCK_SECTION_ID'];
while ($iSection > 0)
{
	$arLinks[$iSection]['IS_SELECTED'] = true;
	$iSection = $arLinks[$iSection]['IBLOCK_SECTION_ID'];
}
$arResult["SECTIONS"] = $arNewSections;
if ($arParams['IS_CATALOG'] == "Y")
{
	unset($arResult["SECTIONS"][SALE_SECTION_ID]);
} else
{
	$arResult["SECTIONS"] = $arResult["SECTIONS"][SALE_SECTION_ID]['ITEMS'];
}