<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['ITEMS']))
{
	?>
	<div class="product-option mb-30 clearfix">
		<!-- Nav tabs -->
		<ul class="shop-tab">
			<li class="active"><a href="#grid-view" data-toggle="tab"><i class="zmdi zmdi-view-module"></i></a></li>
			<li><a href="#list-view"  data-toggle="tab"><i class="zmdi zmdi-view-list"></i></a></li>
		</ul>
		<div class="showing text-right hidden-xs">
			<p class="mb-0">Показаны <?= count($arResult['ITEMS']) ?> из <?= $arResult['NAV_RESULT']->NavRecordCount ?> <?= GetPadez($arResult['NAV_RESULT']->NavRecordCount, "товара", "товаров", "товаров") ?></p>
		</div>
	</div>
	<!-- Tab panes -->
	<div class="tab-content" id="catalog-section-items">
		<div class="tab-pane active" id="grid-view">							
			<div class="row">
				<?
				foreach ($arResult['ITEMS'] as $arItem)
				{
					$arPhoto = false;
					if ($arItem['PREVIEW_PICTURE']['ID'] > 0)
					{
						$arPhoto = $arItem['PREVIEW_PICTURE'];
					}
					if ($arPhoto)
					{
						$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
					} else
					{
						$arPhoto = array(
							"src" => SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg"
						);
					}
					//myPrint($arItem['PROPERTIES']['SYS_SIZE_AVAILABLE']['VALUE']);
					if (!empty($arItem['OFFERS']))
					{
						$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
					} else
					{
						$actualItem = $arItem;
					}
					$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
					$discount = 0;
					$oldPrice = false;
					if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
					{
						$oldPrice = $arItem['PRICES']['OLDPRICE'];
						$price = doModifyItemPrice($price, $oldPrice);
					}
					if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
					{
						$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
						$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
					}
					?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="single-product">
							<div class="product-img">
								<?
								if ($arItem['PROPERTIES']['LABEL_NEW']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label new-label">new</span>
									<?
								} else if ($arItem['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label sale-label">sale</span>
									<?
								} elseif ($arItem['PROPERTIES']['LABEL_POPULAR']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label popular-label">top</span>
									<?
								} elseif ($arItem['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label black-label">black friday</span>
								<? } ?>
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
									<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" />
								</a>
								<div class="product-action clearfix">
									<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в список желаний" data-id="<?= $arItem['ID'] ?>" class="add-favorite"><i class="zmdi zmdi-favorite-outline"></i></a>
									<a href="javascript:void(0)" data-toggle="modal"  data-target="#productModal" title="Быстрый просмотр" class="quick-view-button" data-id="<?= $arItem['ID'] ?>"><i class="zmdi zmdi-zoom-in"></i></a>
									<? /* <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Compare"><i class="zmdi zmdi-refresh"></i></a> */ ?>
									<?
									if (count($arItem['OFFERS']) == 1)
									{
										?>
										<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" class="add-to-basket" data-element-id="<?= $arItem['OFFERS'][0]['ID'] ?>"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
										<?
									} else
									{
										?>
										<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-toggle="tooltip" data-placement="top" title="Купить"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
										<?
									}
									?>
								</div>
							</div>
							<div class="product-info clearfix">
								<div class="fix">
									<div class="h4 post-title floatleft"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
								</div>
								<div class="fix">
									<span class="pro-price floatleft"><?
										if ($price['PERCENT'] > 0)
										{
											?><i><?= $price['PRINT_RATIO_BASE_PRICE'] ?></i><?
										}
										?><?= $price['PRINT_RATIO_PRICE'] ?></span>
									#VOTE_<?= $arItem['ID'] ?>#
								</div>
							</div>
						</div>
					</div>
					<?
				}
				?>
			</div>
		</div>
		<div class="tab-pane" id="list-view">							
			<div class="row shop-list">
				<?
				foreach ($arResult['ITEMS'] as $arItem)
				{
					$arPhoto = false;
					if ($arItem['PREVIEW_PICTURE']['ID'] > 0)
					{
						$arPhoto = $arItem['PREVIEW_PICTURE'];
					}
					if ($arPhoto)
					{
						$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
					} else
					{
						$arPhoto = array(
							"src" => SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg"
						);
					}
					//myPrint($arItem['PROPERTIES']['SYS_SIZE_AVAILABLE']['VALUE']);
					if (!empty($arItem['OFFERS']))
					{
						$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
					} else
					{
						$actualItem = $arItem;
					}
					$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
					$discount = 0;
					$oldPrice = false;
					if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
					{
						$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
						$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
					}
					?>

					<div class="col-lg-12">
						<div class="single-product clearfix">
							<div class="product-img">
								<?
								if ($arItem['PROPERTIES']['LABEL_NEW']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label new-label">new</span>
									<?
								} else if ($arItem['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label sale-label">sale</span>
									<?
								} elseif ($arItem['PROPERTIES']['LABEL_POPULAR']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label popular-label">top</span>
									<?
								} elseif ($arItem['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] == "Y")
								{
									?>
									<span class="pro-label black-label">black friday</span>
								<? } ?>
								<span class="pro-price-2"><?= $price['PRINT_RATIO_PRICE'] ?></span>
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
									<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" />
								</a>
							</div>
							<div class="product-info">
								<div class="fix pro-rating-floatright">
									<div class="h4 post-title floatleft"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
									#VOTE_<?= $arItem['ID'] ?>#
								</div>
								<div class="fix mb-20">
									<span class="pro-price"><?= $price['PRINT_RATIO_PRICE'] ?></span>
								</div>
								<div class="product-description">
									<?= (strlen($arItem['PREVIEW_TEXT']) > 0 ? $arItem['PREVIEW_TEXT'] : $arItem['DETAIL_TEXT']) ?>
								</div>
								<div class="clearfix">
									<? /*
									  <div class="cart-plus-minus">
									  <input type="text" value="02" name="qtybutton" class="cart-plus-minus-box">
									  </div> */ ?>
									<div class="product-action clearfix">
										<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в список желаний"><i class="zmdi zmdi-favorite-outline"></i></a>
										<a href="javascript:void(0)" data-toggle="modal"  data-target="#productModal" title="Быстрый просмотр" class="quick-view-button" data-id="<?= $arItem['ID'] ?>"><i class="zmdi zmdi-zoom-in"></i></a>
										<? /* <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Compare"><i class="zmdi zmdi-refresh"></i></a> */ ?>
										<?
										if (count($arItem['OFFERS']) == 1)
										{
											?>
											<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" class="add-to-basket" data-element-id="<?= $arItem['OFFERS'][0]['ID'] ?>"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
											<?
										} else
										{
											?>
											<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-toggle="tooltip" data-placement="top" title="Купить"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
												<?
											}
											?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?
				}
				?>
			</div>
		</div>
	</div>
	<?
	if (strlen($arResult['NAV_STRING']) > 0)
	{
		echo $arResult['NAV_STRING'];
	}
}
