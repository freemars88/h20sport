<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$arNewSections = array();
$arLinks = array();
foreach ($arResult['SECTIONS'] as $arItem)
{
	if (!isset($arLinks[$arItem['IBLOCK_SECTION_ID']]))
	{
		$arNewSections[$arItem['ID']] = $arItem;
		$arLinks[$arItem['ID']] = &$arNewSections[$arItem['ID']];
	} else
	{
		$arLinks[$arItem['IBLOCK_SECTION_ID']]['ITEMS'][$arItem['ID']] = $arItem;
		$arLinks[$arItem['ID']] = &$arLinks[$arItem['IBLOCK_SECTION_ID']]['ITEMS'][$arItem['ID']];
	}
}
$arResult["SECTIONS"]=$arNewSections;