<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$arBanners = array();
$rBanners = CIBlockElement::GetList(array("rand" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_BANNER_TYPE" => BANNER_TYPE_VALUE_PAGE_TOP), false, false, array("ID", "CODE"));
while ($arBanner = $rBanners->Fetch())
{
	if (strpos($arParams['PAGE'], $arBanner["CODE"]) === 0)
	{
		$arBanners[$arBanner['CODE']][] = $arBanner;
	}
}
ksort($arBanners, SORT_DESC);
$ak = array_keys($arBanners);
$arBanners = $arBanners[$ak[0]];
shuffle($arBanners);
reset($arBanners);
$arBanner = current($arBanners);
$arResult['BANNER'] = getElementData($arBanner['ID']);
