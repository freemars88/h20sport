<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
if ($arResult['BANNER']['PREVIEW_PICTURE'] > 0)
{
	$APPLICATION->SetPageProperty("headingbanner_style", ' style="background-image:url(' . CFile::GetPath($arResult['BANNER']['PREVIEW_PICTURE']) . ');"');
}
