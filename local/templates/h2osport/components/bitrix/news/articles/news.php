<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="blog-area blog-2 pt-80 pb-80">
	<div class="container">	
		<div class="blog">
			<div class="row">
				<div class="col-md-3">
					<aside class="widget widget-search mb-30">
						<form action="/search/">
							<input type="text" placeholder="Поиск..." name="q" />
							<button type="submit" name="search">
								<i class="zmdi zmdi-search"></i>
							</button>
						</form>
					</aside>
					<aside class="widget widget-categories  mb-30">
						<div class="widget-title">
							<div class="h4">Разделы</div>
						</div>
						<div id="cat-treeview" class="widget-info product-cat">
							<?
							$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.menu.left", Array(
								"CACHE_TIME" => "3600", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"IBLOCK_ID" => 16,
								"SECTION_ID" => "",
								"SECTION_CODE" => "",
								"SECTION_CODE_PATH" => "",
								"IS_CATALOG"=>"Y"
									), false
							);
							?>
						</div>
					</aside>
					<aside class="widget widget-product mb-30">
						<div class="widget-title">
							<div class="h4">Последние новости</div>
						</div>
						<div class="widget-info sidebar-product clearfix">
							<?
							$APPLICATION->IncludeComponent("bitrix:news.line", "sidebar", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
								"CACHE_GROUPS" => "Y", // Учитывать права доступа
								"CACHE_TIME" => "300", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
								"FIELD_CODE" => array(// Поля
									0 => "NAME",
									1 => "PREVIEW_TEXT",
									2 => "PREVIEW_PICTURE",
									3 => "",
								),
								"IBLOCKS" => array(// Код информационного блока
									0 => "28",
								),
								"IBLOCK_TYPE" => "news", // Тип информационного блока
								"NEWS_COUNT" => "5", // Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM", // Поле для первой сортировки новостей
								"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
								"SORT_ORDER1" => "DESC", // Направление для первой сортировки новостей
								"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
									), false
							);
							?>
						</div>
					</aside>
					<?
					$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.banner", Array(
						"CACHE_TIME" => "3600", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"CATALOG_IBLOCK_ID" => 16,
						"IBLOCK_ID" => "26",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_CODE_PATH" => "",
						"ELEMENT_ID" => false
							), false
					);
					?>
				</div>
				<div class="col-md-9">
					<?
					$APPLICATION->IncludeComponent(
							"bitrix:news.list", "", Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"NEWS_COUNT" => $arParams["NEWS_COUNT"],
						"SORT_BY1" => $arParams["SORT_BY1"],
						"SORT_ORDER1" => $arParams["SORT_ORDER1"],
						"SORT_BY2" => $arParams["SORT_BY2"],
						"SORT_ORDER2" => $arParams["SORT_ORDER2"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"CHECK_DATES" => $arParams["CHECK_DATES"],
						"IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
						"SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
						"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
						"SET_TITLE" => $arParams["SET_TITLE"],
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
						"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
						"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
						"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
						"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
						"USE_RATING" => $arParams["USE_RATING"],
						"DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
						"MAX_VOTE" => $arParams["MAX_VOTE"],
						"VOTE_NAMES" => $arParams["VOTE_NAMES"],
						"USE_SHARE" => $arParams["LIST_USE_SHARE"],
						"SHARE_HIDE" => $arParams["SHARE_HIDE"],
						"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
						"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
						"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
							), $component
					);
					?>
				</div>
			</div>
		</div>
	</div>
</div>
