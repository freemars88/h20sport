<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="blog-area blog-2 blog-details-area pt-80 pb-80">
	<div class="container">
		<div class="blog">
			<div class="row">
				<div class="col-md-3">
					<aside class="widget widget-search mb-30">
						<form action="/search/">
							<input type="text" placeholder="Поиск..." name="q"/>
							<button type="submit" name="search">
								<i class="zmdi zmdi-search"></i>
							</button>
						</form>
					</aside>
					<aside class="widget widget-categories  mb-30">
						<div class="widget-title">
							<div class="h4">Разделы</div>
						</div>
						<div id="cat-treeview" class="widget-info product-cat">
							<?
							$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.menu.left", Array(
								"CACHE_TIME" => "3600", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"IBLOCK_ID" => 16,
								"SECTION_ID" => "",
								"SECTION_CODE" => "",
								"SECTION_CODE_PATH" => "",
								"IS_CATALOG"=>"Y"
							), false
							);
							?>
						</div>
					</aside>
					<aside class="widget widget-product mb-30">
						<div class="widget-title">
							<div class="h4">Последние новости</div>
						</div>
						<div class="widget-info sidebar-product clearfix">
							<?
							$APPLICATION->IncludeComponent("bitrix:news.line", "sidebar", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
								"CACHE_GROUPS" => "Y", // Учитывать права доступа
								"CACHE_TIME" => "300", // Время кеширования (сек.)
								"CACHE_TYPE" => "A", // Тип кеширования
								"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
								"FIELD_CODE" => array(// Поля
									0 => "NAME",
									1 => "PREVIEW_TEXT",
									2 => "PREVIEW_PICTURE",
									3 => "",
								),
								"IBLOCKS" => array(// Код информационного блока
									0 => "28",
								),
								"IBLOCK_TYPE" => "news", // Тип информационного блока
								"NEWS_COUNT" => "5", // Количество новостей на странице
								"SORT_BY1" => "ACTIVE_FROM", // Поле для первой сортировки новостей
								"SORT_BY2" => "SORT", // Поле для второй сортировки новостей
								"SORT_ORDER1" => "DESC", // Направление для первой сортировки новостей
								"SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
							), false
							);
							?>
						</div>
					</aside>
					<?
					$APPLICATION->IncludeComponent("webavk:blank.component", "catalog.banner", Array(
						"CACHE_TIME" => "3600", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"CATALOG_IBLOCK_ID" => 16,
						"IBLOCK_ID" => "26",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_CODE_PATH" => "",
						"ELEMENT_ID" => false,
					), false
					);
					?>
				</div>
				<div class="col-md-9">
					<?
					$ElementID = $APPLICATION->IncludeComponent(
						"bitrix:news.detail", "", Array(
						"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
						"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
						"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
						"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
						"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"META_KEYWORDS" => $arParams["META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
						"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
						"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
						"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
						"SET_TITLE" => "Y",
						"MESSAGE_404" => $arParams["MESSAGE_404"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"SHOW_404" => $arParams["SHOW_404"],
						"FILE_404" => $arParams["FILE_404"],
						"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
						"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
						"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
						"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
						"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
						"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
						"CHECK_DATES" => $arParams["CHECK_DATES"],
						"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
						"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
						"SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],
						"USE_SHARE" => $arParams["USE_SHARE"],
						"SHARE_HIDE" => $arParams["SHARE_HIDE"],
						"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
						"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
						"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
						"USE_RATING" => $arParams["USE_RATING"],
						"MAX_VOTE" => $arParams["MAX_VOTE"],
						"VOTE_NAMES" => $arParams["VOTE_NAMES"],
						"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
						"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
						"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
					), $component
					);
					if ($ElementID > 0) {
						?>
						<div class="clearfix"></div>
						<?
						global $arrFilter;
						$arrFilter['PROPERTY_BRAND'] = $ElementID;
						$APPLICATION->IncludeComponent(
							"webavk:blank.component", "catalog.section.brand", array(
							"BASE_TITLE_PATH" => $APPLICATION->GetCurPage(),
							"ACTION_VARIABLE" => "action",
							"ADD_ELEMENT_CHAIN" => "Y",
							"ADD_PICT_PROP" => "-",
							"ADD_PROPERTIES_TO_BASKET" => "Y",
							"ADD_SECTIONS_CHAIN" => "Y",
							"ADD_SECTION_CHAIN" => "Y",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"ALSO_BUY_ELEMENT_COUNT" => "4",
							"ALSO_BUY_MIN_BUYES" => "1",
							"BASKET_URL" => "/personal/cart/",
							"BIG_DATA_RCM_TYPE" => "personal",
							"CACHE_FILTER" => "Y",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "N",
							"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
							"COMMON_SHOW_CLOSE_POPUP" => "N",
							"COMPATIBLE_MODE" => "Y",
							"CONVERT_CURRENCY" => "N",
							"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
							"DETAIL_ADD_TO_BASKET_ACTION" => array(
								0 => "BUY",
							),
							"DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => array(
								0 => "BUY",
							),
							"DETAIL_BACKGROUND_IMAGE" => "-",
							"DETAIL_BLOG_EMAIL_NOTIFY" => "N",
							"DETAIL_BLOG_URL" => "catalog_comments",
							"DETAIL_BLOG_USE" => "Y",
							"DETAIL_BRAND_PROP_CODE" => array(
								0 => "",
								1 => "BRAND_REF",
								2 => "",
							),
							"DETAIL_BRAND_USE" => "Y",
							"DETAIL_BROWSER_TITLE" => "-",
							"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
							"DETAIL_DETAIL_PICTURE_MODE" => array(
								0 => "POPUP",
								1 => "MAGNIFIER",
							),
							"DETAIL_DISPLAY_NAME" => "N",
							"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
							"DETAIL_FB_APP_ID" => "",
							"DETAIL_FB_USE" => "Y",
							"DETAIL_IMAGE_RESOLUTION" => "16by9",
							"DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(),
							"DETAIL_MAIN_BLOCK_PROPERTY_CODE" => array(),
							"DETAIL_META_DESCRIPTION" => "-",
							"DETAIL_META_KEYWORDS" => "-",
							"DETAIL_OFFERS_FIELD_CODE" => array(
								0 => "NAME",
								1 => "",
							),
							"DETAIL_OFFERS_PROPERTY_CODE" => array(
								0 => "COLOR",
								1 => "SIZE",
								2 => "",
							),
							"DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
							"DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
							"DETAIL_PROPERTY_CODE" => array(
								0 => "CML2_ARTICLE",
								1 => "MANUFACTURE",
								2 => "BRAND",
								3 => "YEAR",
								4 => "COUNTRY",
								5 => "",
								6 => "",
								7 => "",
								8 => "",
							),
							"DETAIL_SET_CANONICAL_URL" => "Y",
							"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
							"DETAIL_SHOW_POPULAR" => "Y",
							"DETAIL_SHOW_SLIDER" => "N",
							"DETAIL_SHOW_VIEWED" => "Y",
							"DETAIL_STRICT_SECTION_CHECK" => "Y",
							"DETAIL_USE_COMMENTS" => "Y",
							"DETAIL_USE_VOTE_RATING" => "Y",
							"DETAIL_VK_USE" => "N",
							"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
							"DISABLE_INIT_JS_IN_COMPONENT" => "N",
							"DISCOUNT_PERCENT_POSITION" => "bottom-right",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"ELEMENT_SORT_FIELD" => "sort",
							"ELEMENT_SORT_FIELD2" => "shows",
							"ELEMENT_SORT_ORDER" => "asc",
							"ELEMENT_SORT_ORDER2" => "desc",
							"FIELDS" => array(
								0 => "SCHEDULE",
								1 => "STORE",
								2 => "",
							),
							"FILTER_FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"FILTER_HIDE_ON_MOBILE" => "N",
							"FILTER_NAME" => "arrFilter",
							"FILTER_OFFERS_FIELD_CODE" => array(
								0 => "PREVIEW_PICTURE",
								1 => "DETAIL_PICTURE",
								2 => "",
							),
							"FILTER_OFFERS_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"FILTER_PRICE_CODE" => array(
								0 => "BASE",
							),
							"FILTER_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"FILTER_VIEW_MODE" => "VERTICAL",
							"FORUM_ID" => "",
							"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
							"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
							"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
							"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
							"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
							"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
							"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
							"GIFTS_MESS_BTN_BUY" => "Выбрать",
							"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
							"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
							"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
							"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
							"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
							"GIFTS_SHOW_IMAGE" => "Y",
							"GIFTS_SHOW_NAME" => "Y",
							"GIFTS_SHOW_OLD_PRICE" => "Y",
							"HIDE_NOT_AVAILABLE" => "Y",
							"HIDE_NOT_AVAILABLE_OFFERS" => "N",
							"IBLOCK_ID" => "16",
							"IBLOCK_TYPE" => "catalog",
							"INCLUDE_SUBSECTIONS" => "Y",
							"INSTANT_RELOAD" => "N",
							"LABEL_PROP" => array(),
							"LABEL_PROP_MOBILE" => "",
							"LABEL_PROP_POSITION" => "top-left",
							"LAZY_LOAD" => "N",
							"LINE_ELEMENT_COUNT" => "3",
							"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
							"LINK_IBLOCK_ID" => "",
							"LINK_IBLOCK_TYPE" => "",
							"LINK_PROPERTY_SID" => "",
							"LIST_BROWSER_TITLE" => "-",
							"LIST_ENLARGE_PRODUCT" => "STRICT",
							"LIST_META_DESCRIPTION" => "-",
							"LIST_META_KEYWORDS" => "-",
							"LIST_OFFERS_FIELD_CODE" => array(
								0 => "NAME",
								1 => "PREVIEW_PICTURE",
								2 => "DETAIL_PICTURE",
								3 => "",
							),
							"LIST_OFFERS_LIMIT" => "0",
							"LIST_OFFERS_PROPERTY_CODE" => array(
								0 => "",
								1 => "SIZES_SHOES",
								2 => "SIZES_CLOTHES",
								3 => "COLOR_REF",
								4 => "MORE_PHOTO",
								5 => "ARTNUMBER",
								6 => "",
							),
							"LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
							"LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
							"LIST_PROPERTY_CODE" => array(
								0 => "CML2_ARTICLE",
								1 => "BRAND",
								2 => "NEWPRODUCT",
								3 => "SALELEADER",
								4 => "SPECIALOFFER",
								5 => "",
							),
							"LIST_PROPERTY_CODE_MOBILE" => array(),
							"LIST_SHOW_SLIDER" => "Y",
							"LIST_SLIDER_INTERVAL" => "3000",
							"LIST_SLIDER_PROGRESS" => "N",
							"LOAD_ON_SCROLL" => "N",
							"MAIN_TITLE" => "Наличие на складах",
							"MESSAGES_PER_PAGE" => "10",
							"MESSAGE_404" => "",
							"MESS_BTN_ADD_TO_BASKET" => "В корзину",
							"MESS_BTN_BUY" => "Купить",
							"MESS_BTN_COMPARE" => "Сравнение",
							"MESS_BTN_DETAIL" => "Подробнее",
							"MESS_BTN_SUBSCRIBE" => "Подписаться",
							"MESS_COMMENTS_TAB" => "Комментарии",
							"MESS_DESCRIPTION_TAB" => "Описание",
							"MESS_NOT_AVAILABLE" => "Нет в наличии",
							"MESS_PRICE_RANGES_TITLE" => "Цены",
							"MESS_PROPERTIES_TAB" => "Характеристики",
							"MIN_AMOUNT" => "10",
							"OFFERS_CART_PROPERTIES" => array(),
							"OFFERS_SORT_FIELD" => "sort",
							"OFFERS_SORT_FIELD2" => "id",
							"OFFERS_SORT_ORDER" => "desc",
							"OFFERS_SORT_ORDER2" => "desc",
							"OFFER_ADD_PICT_PROP" => "-",
							"OFFER_TREE_PROPS" => array(),
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => "catalog",
							"PAGER_TITLE" => "Товары",
							"PAGE_ELEMENT_COUNT" => "21",
							"PARTIAL_PRODUCT_PROPERTIES" => "N",
							"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
							"PRICE_CODE" => array(
								0 => "BASE",
								1 => "OLDPRICE",
							),
							"PRICE_VAT_INCLUDE" => "Y",
							"PRICE_VAT_SHOW_VALUE" => "N",
							"PRODUCT_DISPLAY_MODE" => "Y",
							"PRODUCT_ID_VARIABLE" => "id",
							"PRODUCT_PROPERTIES" => array(),
							"PRODUCT_PROPS_VARIABLE" => "prop",
							"PRODUCT_QUANTITY_VARIABLE" => "quantity",
							"PRODUCT_SUBSCRIPTION" => "Y",
							"QUANTITY_FLOAT" => "N",
							"REVIEW_AJAX_POST" => "Y",
							"SEARCH_CHECK_DATES" => "Y",
							"SEARCH_NO_WORD_LOGIC" => "Y",
							"SEARCH_PAGE_RESULT_COUNT" => "50",
							"SEARCH_RESTART" => "N",
							"SEARCH_USE_LANGUAGE_GUESS" => "N",
							"SECTIONS_HIDE_SECTION_NAME" => "N",
							"SECTIONS_SHOW_PARENT_NAME" => "N",
							"SECTIONS_VIEW_MODE" => "TILE",
							"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
							"SECTION_BACKGROUND_IMAGE" => "-",
							"SECTION_COUNT_ELEMENTS" => "N",
							"SECTION_ID_VARIABLE" => "SECTION_ID",
							"SECTION_TOP_DEPTH" => "2",
							"SEF_FOLDER" => "/catalog/",
							"SEF_MODE" => "Y",
							"SET_LAST_MODIFIED" => "N",
							"SET_STATUS_404" => "Y",
							"SET_TITLE" => "Y",
							"SHOW_404" => "N",
							"SHOW_DEACTIVATED" => "N",
							"SHOW_DISCOUNT_PERCENT" => "Y",
							"SHOW_EMPTY_STORE" => "Y",
							"SHOW_GENERAL_STORE_INFORMATION" => "N",
							"SHOW_LINK_TO_FORUM" => "Y",
							"SHOW_MAX_QUANTITY" => "N",
							"SHOW_OLD_PRICE" => "Y",
							"SHOW_PRICE_COUNT" => "1",
							"SHOW_TOP_ELEMENTS" => "N",
							"SIDEBAR_DETAIL_SHOW" => "Y",
							"SIDEBAR_PATH" => "/catalog/sidebar.php",
							"SIDEBAR_SECTION_SHOW" => "Y",
							"STORES" => array(
								0 => "",
								1 => "",
							),
							"STORE_PATH" => "/store/#store_id#",
							"TEMPLATE_THEME" => "site",
							"TOP_ADD_TO_BASKET_ACTION" => "ADD",
							"URL_TEMPLATES_READ" => "",
							"USER_CONSENT" => "N",
							"USER_CONSENT_ID" => "0",
							"USER_CONSENT_IS_CHECKED" => "Y",
							"USER_CONSENT_IS_LOADED" => "N",
							"USER_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"USE_ALSO_BUY" => "Y",
							"USE_BIG_DATA" => "Y",
							"USE_CAPTCHA" => "Y",
							"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
							"USE_COMPARE" => "N",
							"USE_ELEMENT_COUNTER" => "Y",
							"USE_ENHANCED_ECOMMERCE" => "N",
							"USE_FILTER" => "Y",
							"USE_GIFTS_DETAIL" => "Y",
							"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
							"USE_GIFTS_SECTION" => "Y",
							"USE_MAIN_ELEMENT_SECTION" => "Y",
							"USE_MIN_AMOUNT" => "N",
							"USE_PRICE_COUNT" => "N",
							"USE_PRODUCT_QUANTITY" => "Y",
							"USE_REVIEW" => "Y",
							"USE_SALE_BESTSELLERS" => "Y",
							"USE_STORE" => "Y",
							"COMPONENT_TEMPLATE" => ".default",
							"SEF_URL_TEMPLATES" => array(
								"sections" => "",
								"section" => "#SECTION_CODE_PATH#/",
								"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#.html",
								"compare" => "compare/",
								"smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
							),
						), false
						);
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>


