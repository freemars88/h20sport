<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arPhoto = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width' => 1090, 'height' => 450), BX_RESIZE_IMAGE_EXACT, true);
$strTS = MakeTimeStamp($arResult['ACTIVE_FROM'], CSite::GetDateFormat());
$arMonths = array(
	1 => "Января",
	2 => "Февраля",
	3 => "Марта",
	4 => "Апреля",
	5 => "Мая",
	6 => "Июня",
	7 => "Июля",
	8 => "Августа",
	9 => "Сентября",
	10 => "Октября",
	11 => "Ноября",
	12 => "Декабря"
);
?>
<div class="single-blog mb-30" id="<? echo $this->GetEditAreaId($arResult['ID']) ?>">
	<div class="blog-photo">
		<img src="<?= $arPhoto['src'] ?>" alt="" />
		<? /*
		  <div class="like-share fix">
		  <a href="#"><i class="zmdi zmdi-account"></i><span>James</span></a>
		  <a href="#"><i class="zmdi zmdi-favorite"></i><span>89 Like</span></a>
		  <a href="#"><i class="zmdi zmdi-comments"></i><span>59 Comments</span></a>
		  <a href="#"><i class="zmdi zmdi-share"></i><span>29 Share</span></a>
		  </div> */ ?>
		<div class="post-date post-date-2">
			<span class="text-dark-red"><?= date("d", $strTS); ?></span>
			<span class="text-dark-red text-uppercase"><?= $arMonths[intval(date("m", $strTS))] ?></span>
		</div>
	</div>
	<div class="blog-info blog-details-info">
		<h4 class="post-title post-title-2"><a href="javascript:void(0)"><?= $arResult['NAME'] ?></a></h4>
			<?= $arResult['DETAIL_TEXT'] ?>
			<? /*
			  <div class="post-share-tag clearfix mt-40">
			  <div class="post-share floatleft">
			  <span class="text-uppercase"><strong>Share</strong></span>
			  <a href="#"><i class="zmdi zmdi-facebook"></i></a>
			  <a href="#"><i class="zmdi zmdi-twitter"></i></a>
			  <a href="#"><i class="zmdi zmdi-linkedin"></i></a>
			  <a href="#"><i class="zmdi zmdi-vimeo"></i></a>
			  <a href="#"><i class="zmdi zmdi-dribbble"></i></a>
			  <a href="#"><i class="zmdi zmdi-instagram"></i></a>
			  </div>
			  <div class="post-share post-tag floatright">
			  <span class="text-uppercase"><strong>tags</strong></span>
			  <a href="#">Chair</a>
			  <a href="#">Furniture</a>
			  <a href="#">Light</a>
			  <a href="#">Table</a>
			  </div>
			  </div> */ ?>							
	</div>
</div>