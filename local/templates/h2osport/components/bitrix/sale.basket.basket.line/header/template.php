<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$total = 0;
foreach ($arResult["CATEGORIES"]['READY'] as $arItem)
{
	$total += $arItem['QUANTITY'];
}
?>

<a class="cart-icon" href="/personal/cart/">
	<i class="zmdi zmdi-shopping-cart"></i>
	<span><?= intval($total) ?></span>
</a>
<?
if (!empty($arResult["CATEGORIES"]['READY']))
{
	?>
	<div class="mini-cart-brief text-left">
		<? /* <div class="cart-items">
		  <p class="mb-0">You have <span>03 items</span> in your shopping bag</p>
		  </div> */ ?>
		<div class="all-cart-product clearfix">
			<?
			foreach ($arResult["CATEGORIES"]['READY'] as $arItem)
			{
				?>
				<div class="single-cart clearfix">
					<div class="cart-photo">
						<a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><img src="<?= $arItem["PICTURE_SRC"] ?>" alt="<?= $arItem['NAME'] ?>" /></a>
					</div>
					<div class="cart-info">
						<h5><a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['NAME'] ?></a></h5>
						<p class="mb-0"><?= $arItem['QUANTITY'] ?> x <?= $arItem['PRICE_FMT'] ?></p>
						<span class="cart-delete"><a href="javascript:void(0)" data-id="<?= $arItem['ID'] ?>" title="Удалить" class="header-basket-remove"><i class="zmdi zmdi-close"></i></a></span>
					</div>
				</div>
				<?
			}
			?>
		</div>
		<div class="cart-totals">
			<h5 class="mb-0">Итого <span class="floatright"><?= $arResult['TOTAL_PRICE'] ?></span></h5>
		</div>
		<div class="cart-bottom  clearfix">
			<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="button-one floatleft text-uppercase" data-text="В корзину">В корзину</a>
			<a href="<?= $arParams['PATH_TO_ORDER'] ?>" class="button-one floatright text-uppercase" data-text="Оформить заказ">Оформить заказ</a>
		</div>
	</div>
	<?
}