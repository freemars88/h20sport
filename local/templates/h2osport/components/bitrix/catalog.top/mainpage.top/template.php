<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS']))
{
	$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
	$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
	$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));
	?>
	<div class="row">
		<?
		foreach ($arResult['ITEMS'] as $arItem)
		{
			$arPhoto = false;
			if ($arItem['PREVIEW_PICTURE']['ID'] > 0)
			{
				$arPhoto = $arItem['PREVIEW_PICTURE'];
			}
			if ($arPhoto)
			{
				$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
			} else
			{
				$arPhoto = array(
					"src" => SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg"
				);
			}
			if (!empty($arItem['OFFERS']))
			{
				$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
			} else
			{
				$actualItem = $arItem;
			}
			$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
			$discount = 0;
			$oldPrice = false;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE'];
				$price = doModifyItemPrice($price, $oldPrice);
			}
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{

				$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
				$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
			}
			?>
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<div class="single-product">
					<div class="product-img">
						<?
						if ($arItem['PROPERTIES']['LABEL_NEW']['VALUE'] == "Y")
						{
							?>
							<span class="pro-label new-label">new</span>
							<?
						} else if ($arItem['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
						{
							?>
							<span class="pro-label sale-label">sale</span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_POPULAR']['VALUE'] == "Y")
						{
							?>
							<span class="pro-label popular-label">top</span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] == "Y")
						{
							?>
							<span class="pro-label black-label">black friday</span>
						<? } ?>
						<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
							<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" />
						</a>
						<div class="product-action clearfix">
							<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в список желаний" data-id="<?= $arItem['ID'] ?>" class="add-favorite"><i class="zmdi zmdi-favorite-outline"></i></a>
							<a href="javascript:void(0)" data-toggle="modal"  data-target="#productModal" title="Быстрый просмотр" class="quick-view-button" data-id="<?= $arItem['ID'] ?>"><i class="zmdi zmdi-zoom-in"></i></a>
							<? /* <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Compare"><i class="zmdi zmdi-refresh"></i></a> */ ?>
							<?
							if (count($arItem['OFFERS']) == 1)
							{
								?>
								<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" class="add-to-basket" data-element-id="<?= $arItem['OFFERS'][0]['ID'] ?>"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
								<?
							} else
							{
								?>
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-toggle="tooltip" data-placement="top" title="Купить"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
								<?
							}
							?>
						</div>
					</div>
					<div class="product-info clearfix">
						<div class="fix">
							<div class="h4 post-title floatleft"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
						</div>
						<div class="fix">
							<span class="pro-price floatleft"><?
								if ($price['PERCENT'] > 0)
								{
									?><i><?= $price['PRINT_RATIO_BASE_PRICE'] ?></i>
									<?= $price['PRINT_RATIO_PRICE'] ?>
									<?
								} else
								{
									?>
									<?= $price['PRINT_RATIO_PRICE'] ?>
									<i>&nbsp;</i>
									<?
								}
								?></span>
							<?
							$APPLICATION->IncludeComponent("bitrix:iblock.vote", "single", Array(
								"EXTCLASS" => "floatright",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"DISPLAY_AS_RATING" => "rating",
								"ELEMENT_CODE" => "",
								"ELEMENT_ID" => $arItem['ID'],
								"IBLOCK_ID" => $arParams['IBLOCK_ID'],
								"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
								"MAX_VOTE" => "5",
								"MESSAGE_404" => "",
								"SET_STATUS_404" => "N",
								"SHOW_RATING" => "N",
								"VOTE_NAMES" => array(
									0 => "1",
									1 => "2",
									2 => "3",
									3 => "4",
									4 => "5",
									5 => "",
								)
									), false
							);
							?>
						</div>
					</div>
				</div>
			</div>
			<?
		}
		?>					
	</div>
	<?
}
