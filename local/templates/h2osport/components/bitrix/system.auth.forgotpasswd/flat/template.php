<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>
<div class="login-area  pt-80 pb-80">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				<div class="customer-login text-left">
					<h4 class="title-1 title-border text-uppercase mb-30">Восстановление пароля</h4>
					<form class="text-left form_auth clearfix" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
						<?
						if (!empty($arParams["~AUTH_RESULT"]))
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert <?= ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "alert-success" : "alert-danger") ?>"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						?>
						<? if ($arResult["BACKURL"] <> ''): ?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
						<? endif ?>
						<input type="hidden" name="AUTH_FORM" value="Y">
						<input type="hidden" name="TYPE" value="SEND_PWD">
						<p class="text-gray"><?= GetMessage("AUTH_FORGOT_PASSWORD_1") ?></p>
						<input type="text" id="exampleInputEmail1" placeholder="EMail адрес" name="USER_EMAIL" />
						<input type="hidden" name="send_account_info" value="Y" />
						<button type="submit" class="button-one submit-button mt-15" data-text="Отправить">Отправить</button>
					</form>
					<p class="mt-20"><noindex><a href="<?= $arResult["AUTH_AUTH_URL"] ?>" rel="nofollow">Авторизоваться</a></noindex></p>
				</div>					
			</div>
		</div>
	</div>
</div>
