<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
//CJSCore::Init();

if ($arResult["FORM_TYPE"] == "login")
{
	?>
	<div class="customer-login text-left">
		<h4 class="title-1 title-border text-uppercase mb-30">Вход в личный кабинет</h4>
		<?
		if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
		{
			ShowMessage($arResult['ERROR_MESSAGE']);
		}
		?>
		<form class="text-left clearfix" name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
			<?
			if ($arResult["BACKURL"] <> '')
			{
				?>
				<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
				<?
			}
			foreach ($arResult["POST"] as $key => $value)
			{
				?>
				<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
				<?
			}
			?>
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<input type="text" placeholder="Логин" name="USER_LOGIN" />
			<script>
				<!--
				BX.ready(function () {
					var loginCookie = BX.getCookie("<?= CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"]) ?>");
					if (loginCookie)
					{
						var form = document.forms["system_auth_form<?= $arResult["RND"] ?>"];
						var loginInput = form.elements["USER_LOGIN"];
						loginInput.value = loginCookie;
					}
				});
	-->
			</script>
			<input type="password" placeholder="Пароль" name="USER_PASSWORD" autocomplete="off" />
			<?
			if ($arResult["STORE_PASSWORD"] == "Y")
			{
				?>
				<p class="mb-0">
					<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /> <label>Запомнить меня</label>
				</p>
				<?
			}
			if ($arResult["CAPTCHA_CODE"])
			{
				?>
				<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
				<input type="text" name="captcha_word" maxlength="50" value="" />
				<?
			}
			?>
			<input type="hidden" name="Login" value="yes" />
			<button type="submit" class="button-one submit-button mt-15" data-text="Вход">Вход</button>
		</form>
		<p class="mt-20">Забыли пароль ?<noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow"> Восстановить</a></noindex></p>
	<p class="mt-20">Впервые на сайте ?<noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow"> Создать аккаунт</a></noindex></p>

	<?
} 
