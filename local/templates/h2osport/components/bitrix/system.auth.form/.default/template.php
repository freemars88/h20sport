<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
//CJSCore::Init();
?>
<div class="signin-page account">
	<?
	if ($arResult["FORM_TYPE"] == "login")
	{
		?>
		<div class="block text-center">
			<p class="h2 text-center">Вход в личный кабинет</p>
			<?
			if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
			{
				ShowMessage($arResult['ERROR_MESSAGE']);
			}
			?>
			<form class="text-left clearfix" name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
				<?
				if ($arResult["BACKURL"] <> '')
				{
					?>
					<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
					<?
				}
				foreach ($arResult["POST"] as $key => $value)
				{
					?>
					<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
					<?
				}
				?>
				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Логин" name="USER_LOGIN" />
					<script>
						<!--
						BX.ready(function () {
							var loginCookie = BX.getCookie("<?= CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"]) ?>");
							if (loginCookie)
							{
								var form = document.forms["system_auth_form<?= $arResult["RND"] ?>"];
								var loginInput = form.elements["USER_LOGIN"];
								loginInput.value = loginCookie;
							}
						});
	-->
					</script>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" placeholder="Пароль" name="USER_PASSWORD" autocomplete="off" />
				</div>
				<?
				if ($arResult["STORE_PASSWORD"] == "Y")
				{
					?>
					<div class="form-group">
						<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /> <label>Запомнить меня</label>
					</div>
					<?
				}
				if ($arResult["CAPTCHA_CODE"])
				{
					?>
					<div class="form-group">
						<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
						<input type="text" name="captcha_word" maxlength="50" value="" />
					</div>
					<?
				}
				?>
				<div class="text-center">
					<input type="hidden" name="Login" value="yes" />
					<button type="submit" class="btn btn-main text-center">Вход</button>
				</div>
			</form>
			<p class="mt-20">Забыли пароль ?<noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow"> Восстановить</a></noindex></p>
			<p class="mt-20">Впервые на сайте ?<noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow"> Создать аккаунт</a></noindex></p>
		</div>
		<? /*
		  if ($arResult["AUTH_SERVICES"])
		  {
		  ?>
		  <tr>
		  <td colspan="2">
		  <div class="bx-auth-lbl"><?= GetMessage("socserv_as_user_form") ?></div>
		  <?
		  $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", array(
		  "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
		  "SUFFIX" => "form",
		  ), $component, array("HIDE_ICONS" => "Y")
		  );
		  ?>
		  </td>
		  </tr>
		  <?
		  }
		 * 
		 */
		?>
	</form>

	<?
	/*
	  if ($arResult["AUTH_SERVICES"])
	  {
	  $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", array(
	  "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
	  "AUTH_URL" => $arResult["AUTH_URL"],
	  "POST" => $arResult["POST"],
	  "POPUP" => "Y",
	  "SUFFIX" => "form",
	  ), $component, array("HIDE_ICONS" => "Y")
	  );
	  }
	 * 
	 */
} else
{
	?>
	<div class="block text-center">
		<h2 class="text-center">Добро пожаловать</h2>
		<form class="text-left clearfix" action="<?= $arResult["AUTH_URL"] ?>">
			<div class="form-group text-center">
				<?= $arResult["USER_NAME"] ?> [<?= $arResult["USER_LOGIN"] ?>]
			</div>
			<div class="form-group text-center">
				<a href="<?= $arResult["PROFILE_URL"] ?>">Личный кабинет</a>
			</div>
			<div class="text-center">
				<?
				foreach ($arResult["GET"] as $key => $value)
				{
					?>
					<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
					<?
				}
				?>
				<input type="hidden" name="logout" value="yes" />
				<input type="submit" name="logout_butt" class="btn btn-main text-center" value="Выйти" />
			</div>
		</form>
	</div>
<? }
?>
</div>
