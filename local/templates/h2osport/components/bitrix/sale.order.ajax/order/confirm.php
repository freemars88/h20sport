<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!empty($arResult["ORDER"])) {
	?>
	<div class="thank-recieve bg-white mb-30">
		<p>Ваш заказ создан</p>
	</div>
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:sale.personal.order.detail", "order.confirm", Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ALLOW_INNER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "A",
			"CUSTOM_SELECT_PROPS" => array(""),
			"ID" => $arResult['ORDER']['ID'],
			"ONLY_INNER_FULL" => "N",
			"PATH_TO_CANCEL" => "",
			"PATH_TO_COPY" => "",
			"PATH_TO_LIST" => "",
			"PATH_TO_PAYMENT" => "payment.php",
			"PICTURE_HEIGHT" => "110",
			"PICTURE_RESAMPLE_TYPE" => "1",
			"PICTURE_WIDTH" => "110",
			"PROP_1" => array(""),
			"REFRESH_PRICES" => "N",
			"RESTRICT_CHANGE_PAYSYSTEM" => array("0"),
			"SET_TITLE" => "Y",
		)
	);
	if ($APPLICATION->get_cookie("LAST_ORDER_ID") != $arResult['ORDER']['ID']) {
		$oOrder = \Bitrix\Sale\Order::load($arResult['ORDER']['ID']);
		$arOrderFields = $oOrder->getFieldValues();
		$oBasket = $oOrder->getBasket();
		$arItems = $oBasket->getBasketItems();
		$arJSData = array();
		foreach ($arItems as $arItem) {
			$arItemFields = $arItem->getFieldValues();
			$arProduct = getElementData($arItemFields['PRODUCT_ID']);
			$arVariant = array();
			if ($arProduct['PROPERTIES']['COLOR']['VALUE'] > 0) {
				$arColor = getElementData($arProduct['PROPERTIES']['COLOR']['VALUE']);
				$arVariant[] = "Цвет " . $arColor['NAME'];
			}
			if ($arProduct['PROPERTIES']['SIZE']['VALUE'] > 0) {
				$arSize = getElementData($arProduct['PROPERTIES']['SIZE']['VALUE']);
				$arVariant[] = "Размер " . $arSize['NAME'];
			}

			if ($arProduct['PROPERTIES']['CML2_LINK']['VALUE'] > 0) {
				$arMainProduct = getElementData($arProduct['PROPERTIES']['CML2_LINK']['VALUE']);
				$arSectionPath = array();
				if ($arMainProduct['IBLOCK_SECTION_ID'] > 0) {
					$rSection = CIBlockSection::GetNavChain($arMainProduct['IBLOCK_ID'], $arMainProduct['IBLOCK_SECTION_ID']);
					while ($arSection = $rSection->Fetch()) {
						$arSectionPath[] = $arSection['NAME'];
					}
				}
				$arBrand = getElementData($arMainProduct['PROPERTIES']['BRAND']['VALUE']);
				$arJSFields = array(
					"id" => $arMainProduct['PROPERTIES']['CML2_ARTICLE']['VALUE'],
					"name" => $arMainProduct['NAME'],
					"price" => $arItemFields['PRICE'],
					"brand" => $arBrand['NAME'],
					"quantity" => $arItemFields['QUANTITY'],
					"category" => implode("/", $arSectionPath),
					"variant" => implode(", ", $arVariant),
				);
			} else {
				$arSectionPath = array();
				if ($arProduct['IBLOCK_SECTION_ID'] > 0) {
					$rSection = CIBlockSection::GetNavChain($arProduct['IBLOCK_ID'], $arProduct['IBLOCK_SECTION_ID']);
					while ($arSection = $rSection->Fetch()) {
						$arSectionPath[] = $arSection['NAME'];
					}
				}
				$arBrand = getElementData($arProduct['PROPERTIES']['BRAND']['VALUE']);
				$arJSFields = array(
					"id" => $arProduct['PROPERTIES']['CML2_ARTICLE']['VALUE'],
					"name" => $arProduct['NAME'],
					"price" => $arItemFields['PRICE'],
					"brand" => $arBrand['NAME'],
					"quantity" => $arItemFields['QUANTITY'],
					"category" => implode("/", $arSectionPath),
					"variant" => implode(", ", $arVariant),
				);
			}
			$arJSData[] = CUtil::PhpToJSObject($arJSFields);
		}
		?>
		<script type="text/javascript">
			<!--
			window.dataLayer.push({
				"ecommerce": {
					"purchase": {
						"actionField": {
							"id": "<?=$arOrderFields['ACCOUNT_NUMBER']?>"
						},
						"products": [
							<?=implode(",\n", $arJSData)?>
						]
					}
				}
			});
			-->
		</script>
		<?
		$APPLICATION->set_cookie("LAST_ORDER_ID", $arResult['ORDER']['ID']);
	}
	?>
	<?
} else {
	?>
	<div class="order-info bg-white text-center clearfix mb-30">
		<div class="single-order-info">
			<h4 class="title-1 text-uppercase text-light-black mb-0">><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></h4>
			<p class="text-uppercase text-light-black mb-0">
				<?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"])) ?>
				<?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
			</p>
		</div>
	</div>
	<?
}
