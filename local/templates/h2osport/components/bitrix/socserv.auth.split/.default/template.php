<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="b-form b-form_order">
	<div class="b-form__title">
		<h3>социальные сети</h3>
	</div>
	<div class="b-settings b-settings_social">
		<?
		if ($arResult['ERROR_MESSAGE']) {
			ShowMessage($arResult['ERROR_MESSAGE']);
		}
		?>
		<?
		$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "personal", array(
			"AUTH_SERVICES" => $arResult["AUTH_SERVICES_ICONS"],
			"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
			"AUTH_URL" => $arResult['CURRENTURL'],
			"POST" => $arResult["POST"],
			"SHOW_TITLES" => 'N',
			"FOR_SPLIT" => 'Y',
			"AUTH_LINE" => 'N',
			"MAIN_RESULT" => $arResult
				), $component, array("HIDE_ICONS" => "Y")
		);
		?>
	</div>
</div>
