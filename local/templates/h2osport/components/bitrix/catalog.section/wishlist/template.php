<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['ITEMS']))
{
	?>

	<table>
		<thead>
			<tr>
				<th class="product-thumbnail">Товар</th>
				<th class="product-price">Цена</th>
				<th class="product-add-cart">Добавить в корзину</th>
				<th class="product-remove">Удалить</th>
			</tr>
		</thead>
		<tbody>
			<?
			foreach ($arResult['ITEMS'] as $arItem)
			{
				$arPhoto = false;
				if ($arItem['PREVIEW_PICTURE']['ID'] > 0)
				{
					$arPhoto = $arItem['PREVIEW_PICTURE'];
				}
				if ($arPhoto)
				{
					$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 270, 'height' => 270), BX_RESIZE_IMAGE_EXACT, true);
				} else
				{
					$arPhoto = array(
						"src" => SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg"
					);
				}
				//myPrint($arItem['PROPERTIES']['SYS_SIZE_AVAILABLE']['VALUE']);
				if (!empty($arItem['OFFERS']))
				{
					$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
				} else
				{
					$actualItem = $arItem;
				}
				$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
				$discount = 0;
				$oldPrice = false;
				if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
				{
					$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
					$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
				}
				?>
				<tr>
					<td class="product-thumbnail  text-left">
						<!-- Single-product start -->
						<div class="single-product">
							<div class="product-img">
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
									<img src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" />
								</a>
							</div>
							<div class="product-info">
								<div class="h4 post-title"><a class="text-light-black" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
								<p class="mb-0">Артикул: <?= $arItem['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?></p>
							</div>
						</div>
						<!-- Single-product end -->				
					</td>
					<td class="product-price"><?
						if ($price['PERCENT'] > 0)
						{
							?><i><?= $price['PRINT_RATIO_BASE_PRICE'] ?></i>
							<?= $price['PRINT_RATIO_PRICE'] ?>
							<?
						} else
						{
							?>
							<?= $price['PRINT_RATIO_PRICE'] ?>
							<i>&nbsp;</i>
							<?
						}
						?></td>
					<td class="product-add-cart">
						<?
						if (count($arItem['OFFERS']) == 1)
						{
							?>
							<a class="text-light-black add-to-basket" href="javascript:void(0)" data-placement="top" title="Добавить в корзину" data-element-id="<?= $arItem['OFFERS'][0]['ID'] ?>" data-toggle="tooltip"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
							<?
						} else
						{
							?>
							<a class="text-light-black" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-toggle="tooltip" data-placement="top" title="Купить"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
							<?
						}
						?>
					</td>
					<td class="product-remove">
						<a href="javascript:void(0)" class="del-favorite" data-element-id="<?= $arItem['ID'] ?>"><i class="zmdi zmdi-close"></i></a>
					</td>
				</tr>
				<?
			}
			?>
		</tbody>
	</table>
	<?
	if (strlen($arResult['NAV_STRING']) > 0)
	{
		echo $arResult['NAV_STRING'];
	}
}
