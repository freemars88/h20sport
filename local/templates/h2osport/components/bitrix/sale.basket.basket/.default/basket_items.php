<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */

/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0)
{
	?>
	<div class="shop-cart-table">
		<div class="table-content table-responsive product-list">
			<table>
				<thead>
					<tr>
						<th class="product-thumbnail">Наименование</th>
						<th class="product-price">Цена</th>
						<th class="product-quantity">Количество</th>
						<th class="product-subtotal">Стоимость</th>
						<th class="product-remove">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?
					foreach ($arResult["GRID"]["ROWS"] as $k => $arItem)
					{
						if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
						{
							?>
							<tr data-id="<?= $arItem["ID"] ?>" data-item-name="<?= $arItem["NAME"] ?>" data-item-brand="<?= $arItem[$arParams['BRAND_PROPERTY'] . "_VALUE"] ?>" data-item-price="<?= $arItem["PRICE"] ?>" data-item-currency="<?= $arItem["CURRENCY"] ?>">
								<td class="product-thumbnail text-left b-basket-col-name">
									<input type="hidden" class="b-basket-qnt__send" name="QUANTITY_INPUT[<?= $arItem["ID"] ?>]" value="<?= $arItem["QUANTITY"] ?>" />
									<div class="single-product">
										<div class="product-img">
											<a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>">
												<?
												//myPrint($arItem);
												if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0)
												{
													$url = $arItem["PREVIEW_PICTURE_SRC"];
												} elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0)
												{
													$url = $arItem["DETAIL_PICTURE_SRC"];
												} else
												{
													$url = SITE_TEMPLATE_PATH . "/img/empty.270.270.jpg";
												}

												if (strlen($arItem["DETAIL_PAGE_URL"]) > 0)
												{
													?>
													<img width="270" height="270" src="<?= $url ?>" alt="" />
													<?
												} else
												{
													?>
													<img width="270" height="270" src="<?= $url ?>" alt="" />
													<?
												}
												?>
											</a>
										</div>
										<div class="product-info">
											<h4 class="post-title"><a class="text-light-black" href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['NAME'] ?></a></h4>
											<p class="mb-0">Артикул: <?= $arItem['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'] ?></p>
											<p class="mb-0">Цвет: <?= $arItem['ELEMENT']['PROPERTY_COLOR_NAME'] ?></p>
											<p class="mb-0">Размер: <?= $arItem['ELEMENT']['PROPERTY_SIZE_NAME'] ?></p>
										</div>
									</div>											
								</td>
								<td class="product-price b-basket-col-name__price">
									<?
									if ($arItem['DISCOUNT_PRICE'] != 0)
									{
										?>
										<span class="oldprice"><?= $arItem['FULL_PRICE_FORMATED'] ?></span>
										<?= $arItem["PRICE_FORMATED"] ?>
										<?
									} else
									{
										echo $arItem["PRICE_FORMATED"];
									}
									?>
								</td>
								<td class="product-quantity b-basket-col-name__quantity">
									<?
									$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
									$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
									$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
									?>
									<div class="cart-plus-minus">
										<input type="text" size="3" maxlength="18" value="<?= $arItem["QUANTITY"] ?>" data-max="<?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?>" class="basket_qnt cart-plus-minus-box" data-title="Доступно для покупки - <?= $arItem['ELEMENT']['CATALOG_QUANTITY'] ?> шт." name="QUANTITY_INPUT[<?= $arItem["ID"] ?>]" />
									</div>
								</td>
								<td class="product-subtotal b-basket-col-name__sum"><?= $arItem["SUM"] ?></td>
								<td class="product-remove b-basket-col-name__actions">
									<a href="javascript:void(0)" class="del-from-basket" data-id="<?= $arItem["ID"] ?>"><i class="zmdi zmdi-close"></i></a>
								</td>
							</tr>
							<?
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="customer-login mt-30">
				<?
				if ($arParams["HIDE_COUPON"] != "Y")
				{
					?>
					<h4 class="title-1 title-border text-uppercase">Купон на скидку</h4>
					<p class="text-gray">Введите купон на скидку в это поле</p>
					<input type="text" id="coupon" name="COUPON" value="<?= $arResult['COUPON_LIST'][0]['COUPON'] ?>" placeholder="Купон на скидку" class="form-control" />
					<button type="button" id="apply-coupon" title="Применить скидочную карту" data-text="Применить" class="button-one submit-button mt-15">Применить</button>
					<?
				}
				?>

			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="customer-login payment-details mt-30">
				<h4 class="title-1 title-border text-uppercase">Итог</h4>
				<table>
					<tbody>
						<?
						$showTotalPrice = (float)$arResult["DISCOUNT_PRICE_ALL"] > 0;
						?>
						<tr>
							<td class="text-left">Стоимость товаров:</td>
							<td class="text-right"><span id="PRICE_WITHOUT_DISCOUNT"><?=($showTotalPrice ? $arResult["PRICE_WITHOUT_DISCOUNT"] : ''); ?></span></td>
						</tr>
						<tr class="b-cart__totaldiscount"<?=($showTotalPrice?'':' style="display:none;"')?>>
							<td class="text-left">Скидка:</td>
							<td class="text-right"><span id="DISCOUNT_PRICE_ALL"><?= $arResult['DISCOUNT_PRICE_ALL_FORMATED'] ?></span></td>
						</tr>
						<tr>
							<td class="text-left">Итоговая стоимость:</td>
							<td class="text-right"><span id="allSum_FORMATED"><?= $arResult['allSum_FORMATED'] ?></span></td>
						</tr>
					</tbody>
				</table>
				<button class="button-one submit-button mt-15" id="checkout" data-text="Оформить заказ" type="button">Оформить заказ</button>
			</div>
		</div>
	</div>

	<?
	/*
	  <div id="basket_items_list">
	  <input type="hidden" id="column_headers" value="<?= htmlspecialcharsbx(implode($arHeaders, ",")) ?>" />
	  <input type="hidden" id="offers_props" value="<?= htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ",")) ?>" />
	  <input type="hidden" id="action_var" value="<?= htmlspecialcharsbx($arParams["ACTION_VARIABLE"]) ?>" />
	  <input type="hidden" id="quantity_float" value="<?= ($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="price_vat_show_value" value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>" />
	  <input type="hidden" id="auto_calculation" value="<?= ($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y" ?>" />

	  <div class="bx_ordercart_order_pay">

	  <div class="bx_ordercart_order_pay_left" id="coupons_block">
	  <?
	  if ($arParams["HIDE_COUPON"] != "Y")
	  {
	  ?>
	  <div class="bx_ordercart_coupon">
	  <span><?= GetMessage("STB_COUPON_PROMT") ?></span><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">&nbsp;<a class="bx_bt_button bx_big" href="javascript:void(0)" onclick="enterCoupon();" title="<?= GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?= GetMessage('SALE_COUPON_APPLY'); ?></a>
	  </div><?
	  if (!empty($arResult['COUPON_LIST']))
	  {
	  foreach ($arResult['COUPON_LIST'] as $oneCoupon)
	  {
	  $couponClass = 'disabled';
	  switch ($oneCoupon['STATUS'])
	  {
	  case DiscountCouponsManager::STATUS_NOT_FOUND:
	  case DiscountCouponsManager::STATUS_FREEZE:
	  $couponClass = 'bad';
	  break;
	  case DiscountCouponsManager::STATUS_APPLYED:
	  $couponClass = 'good';
	  break;
	  }
	  ?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?= htmlspecialcharsbx($oneCoupon['COUPON']); ?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
	  if (isset($oneCoupon['CHECK_CODE_TEXT']))
	  {
	  echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
	  }
	  ?></div></div><?
	  }
	  unset($couponClass, $oneCoupon);
	  }
	  } else
	  {
	  ?>&nbsp;<?
	  }
	  ?>
	  </div>
	  <div class="bx_ordercart_order_pay_right">
	  <table class="bx_ordercart_order_sum">
	  <? if ($bWeightColumn && floatval($arResult['allWeight']) > 0): ?>
	  <tr>
	  <td class="custom_t1"><?= GetMessage("SALE_TOTAL_WEIGHT") ?></td>
	  <td class="custom_t2" id="allWeight_FORMATED"><?= $arResult["allWeight_FORMATED"] ?>
	  </td>
	  </tr>
	  <? endif; ?>
	  <? if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"): ?>
	  <tr>
	  <td><? echo GetMessage('SALE_VAT_EXCLUDED') ?></td>
	  <td id="allSum_wVAT_FORMATED"><?= $arResult["allSum_wVAT_FORMATED"] ?></td>
	  </tr>
	  <?
	  $showTotalPrice = (float) $arResult["DISCOUNT_PRICE_ALL"] > 0;
	  ?>
	  <tr style="display: <?= ($showTotalPrice ? 'table-row' : 'none'); ?>;">
	  <td class="custom_t1"></td>
	  <td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
	  <?= ($showTotalPrice ? $arResult["PRICE_WITHOUT_DISCOUNT"] : ''); ?>
	  </td>
	  </tr>
	  <?
	  if (floatval($arResult['allVATSum']) > 0):
	  ?>
	  <tr>
	  <td><? echo GetMessage('SALE_VAT') ?></td>
	  <td id="allVATSum_FORMATED"><?= $arResult["allVATSum_FORMATED"] ?></td>
	  </tr>
	  <?
	  endif;
	  ?>
	  <? endif; ?>
	  <tr>
	  <td class="fwb"><?= GetMessage("SALE_TOTAL") ?></td>
	  <td class="fwb" id="allSum_FORMATED"><?= str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"]) ?></td>
	  </tr>


	  </table>
	  <div style="clear:both;"></div>
	  </div>
	  <div style="clear:both;"></div>
	  <div class="bx_ordercart_order_pay_center">

	  <? if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0): ?>
	  <?= $arResult["PREPAY_BUTTON"] ?>
	  <span><?= GetMessage("SALE_OR") ?></span>
	  <? endif; ?>
	  <?
	  if ($arParams["AUTO_CALCULATION"] != "Y")
	  {
	  ?>
	  <a href="javascript:void(0)" onclick="updateBasket();" class="checkout refresh"><?= GetMessage("SALE_REFRESH") ?></a>
	  <?
	  }
	  ?>
	  <a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?= GetMessage("SALE_ORDER") ?></a>
	  </div>
	  </div>
	  </div>
	  <?
	 */
} else
{
	?>
	<div id="basket_items_list">
		<table>
			<tbody>
				<tr>
					<td style="text-align:center">
						<div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?
}