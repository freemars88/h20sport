<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */

/** @var array $arResult */
use Bitrix\Main;

foreach ($arResult["GRID"]["ROWS"] as $k => $arItem)
{
	if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
	{
		$arItemProduct = CIBlockElement::GetList(array(), array("ID" => $arItem['PRODUCT_ID'], "IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "NAME", "PROPERTY_SIZE", "PROPERTY_SIZE.NAME", "PROPERTY_COLOR", "PROPERTY_COLOR.NAME", "PROPERTY_CML2_LINK", "CATALOG_QUANTITY"))->Fetch();
		if ($arItemProduct)
		{
			$arItemProduct['MAIN'] = CIBlockElement::GetList(array(), array("ID" => $arItemProduct['PROPERTY_CML2_LINK_VALUE'], "IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "PROPERTY_CML2_ARTICLE", "PROPERTY_SYS_NAME_GENERATED","PROPERTY_BRAND"))->Fetch();
		}
		
		$arResult["GRID"]["ROWS"][$k]['ELEMENT'] = $arItemProduct;
	}
}