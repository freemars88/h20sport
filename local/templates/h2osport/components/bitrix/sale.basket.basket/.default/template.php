<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
?>

<div class="shopping-cart-area  pt-80 pb-80">
	<div class="container">	
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="shopping-cart">
					<?
					if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
					{
						?>
						<div id="warning_message">
							<?
							if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
							{
								foreach ($arResult["WARNING_MESSAGE"] as $v)
									ShowError($v);
							}
							?>
						</div>
						<?
						$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
						$normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';

						$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
						$delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';

						$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
						$subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';

						$naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
						$naHidden = ($naCount == 0) ? 'style="display:none;"' : '';

						foreach (array_keys($arResult['GRID']['HEADERS']) as $id)
						{
							$data = $arResult['GRID']['HEADERS'][$id];
							$headerName = (isset($data['name']) ? (string) $data['name'] : '');
							if ($headerName == '')
								$arResult['GRID']['HEADERS'][$id]['name'] = GetMessage('SALE_' . $data['id']);
							unset($headerName, $data);
						}
						unset($id);
						?>
						<form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="basket_form" id="basket_form">
							<?
							include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items.php");
							?>
							<input type="hidden" name="BasketOrder" value="BasketOrder" />
							<!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
						</form>
						<?
					} else
					{
						ShowError($arResult["ERROR_MESSAGE"]);
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

						<?/*
<script type="text/javascript">
	dataLayer.push({
	'event': 'ecom',
			'eventCategory': 'ECommerce',
			'eventAction': 'View Cart',
			'eventLabel': '',
			'eventNonInteraction': 'False',
			'ecommerce': {
			'currencyCode': 'RUB',
					'checkout': {
					'actionField': {'step': 1},
							'products': [<?
		$i = 0;
		foreach ($arResult["GRID"]["ROWS"] as $k => $arItem)
		{
			if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
			{
				if ($i > 0)
				{
					echo ", ";
				}
				?>{
									'name': '<?= htmlspecialchars($arItem['NAME']) ?>',
											'id': '<?= $arItem['PRODUCT_ID'] ?>',
											'sku': '<?= GAProductSku($arItem['PRODUCT_ID']) ?>',
											'price': '<?= $arItem['PRICE'] ?>',
											'brand': '<?= GAProductBrand($arItem['PRODUCT_ID']) ?>',
											'category': '<?= GAProductCategoryPath($arItem['PRODUCT_ID']) ?>',
											'variant': '<?= GAProductVariant($arItem['PRODUCT_ID']) ?>',
											'quantity': '<?= $arItem['QUANTITY'] ?>'
									}<?
				$i++;
			}
		}
		?>]
					}
			}
	});</script>

*/