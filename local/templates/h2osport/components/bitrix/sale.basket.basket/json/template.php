<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
$curPage = $APPLICATION->GetCurPage() . '?' . $arParams["ACTION_VARIABLE"] . '=';
$arUrls = array(
	"delete" => $curPage . "delete&id=#ID#",
	"delay" => $curPage . "delay&id=#ID#",
	"add" => $curPage . "add&id=#ID#",
);
unset($curPage);

$arBasketJSParams = array(
	'SALE_DELETE' => GetMessage("SALE_DELETE"),
	'SALE_DELAY' => GetMessage("SALE_DELAY"),
	'SALE_TYPE' => GetMessage("SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"]
);

if (strlen($arResult["ERROR_MESSAGE"]) <= 0) {

	$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
	$normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';

	$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
	$delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';

	$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
	$subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';

	$naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
	$naHidden = ($naCount == 0) ? 'style="display:none;"' : '';
	$bIsShowNote = false;
	if (count($arResult["ITEMS"]["AnDelCanBuy"]) > 0) {
		foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems) {
			$product = $arResult['PRODUCTS'][$arBasketItems['PRODUCT_ID']];
			if ($product['QUANTITY'] == 1) {
				$bIsShowNote = true;
				break;
			}
		}
	}

	include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items.php");
} else {
	$arData = array();
	$arData['is_reload'] = true;
	$arData['is_ok'] = true;
	echo json_encode($arData);
}
