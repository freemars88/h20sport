<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$isExists = false;
if (!empty($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $arItem)
	{
		if (!empty($arItem['VALUES']))
		{
			$isExists = true;
			break;
		}
	}
}
if ($isExists)
{
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 product-filter-area mb-30">
				<div class="container">
					<div class="row">
						<div class="bx-filter bx-filter-horizontal">
							<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">
								<?
								foreach ($arResult["HIDDEN"] as $arItem)
								{
									?>
									<input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>" />
									<?
								}
								?>
								<div class="bx-filter-horizontal-props">
									<?
									/*
									  foreach ($arResult["ITEMS"] as $key => $arItem)//prices
									  {
									  $arItem["DISPLAY_EXPANDED"] = "Y";
									  $key = $arItem["ENCODED_ID"];
									  if (isset($arItem["PRICE"]))
									  {
									  if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
									  {
									  continue;
									  }
									  $step_num = 4;
									  $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
									  $prices = array();
									  if (Bitrix\Main\Loader::includeModule("currency"))
									  {
									  for ($i = 0; $i < $step_num; $i++)
									  {
									  $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
									  }
									  $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
									  } else
									  {
									  $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
									  for ($i = 0; $i < $step_num; $i++)
									  {
									  $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
									  }
									  $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
									  }
									  ?>
									  <aside class="panel panel-widget widget  shop-filter mb-30">
									  <div class="panel-heading widget-title" role="tab" id="headingflt-<?= $arItem['ID'] ?>">
									  <div class="h4">
									  <a class="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "" : "collapsed") ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#flt-<?= $arItem['ID'] ?>" aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>" aria-controls="flt-<?= $arItem['ID'] ?>">
									  <?= $arItem["NAME"] ?>
									  </a>
									  </div>
									  </div>
									  <div id="flt-<?= $arItem['ID'] ?>" class="widget-info panel-collapse bx-filter-parameters-box<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? " collapse in" : " collapse") ?>" role="tabpanel" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
									  <div class="panel-body bx-filter-parameters-box bx-active">
									  <span class="bx-filter-container-modef"></span>
									  <div class="bx-filter-block" data-role="bx_filter_block">
									  <div class="row bx-filter-parameters-box-container">
									  <input
									  class="min-price"
									  type="hidden"
									  name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
									  id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
									  value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
									  size="5"
									  onkeyup="smartFilter.keyup(this)"
									  />
									  <input
									  class="max-price"
									  type="hidden"
									  name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
									  id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
									  value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
									  size="5"
									  onkeyup="smartFilter.keyup(this)"
									  />
									  <div class="price_filter">
									  <div class="price_slider_amount">
									  <input type="button" value="Цена:"/>
									  <input type="text" id="amount" name="price"  placeholder="Выберите цену" />
									  </div>
									  <div id="slider-range"></div>
									  </div>
									  <div class="col-xs-12 bx-ui-slider-track-container">
									  <div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
									  <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?= $key ?>"></div>
									  <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?= $key ?>"></div>
									  <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?= $key ?>"></div>
									  <div class="bx-ui-slider-range" id="drag_tracker_<?= $key ?>"  style="left: 0%; right: 0%;">
									  <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?= $key ?>"></a>
									  <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?= $key ?>"></a>
									  </div>
									  </div>
									  </div>
									  </div>
									  </div>
									  <?
									  $arJsParams = array(
									  "leftSlider" => 'left_slider_' . $key,
									  "rightSlider" => 'right_slider_' . $key,
									  "tracker" => "drag_tracker_" . $key,
									  "trackerWrap" => "drag_track_" . $key,
									  "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
									  "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
									  "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
									  "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
									  "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
									  "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
									  "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
									  "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
									  "precision" => $precision,
									  "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
									  "colorAvailableActive" => 'colorAvailableActive_' . $key,
									  "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
									  );
									  ?>
									  <script type="text/javascript">
									  BX.ready(function () {
									  window['trackBar<?= $key ?>'] = new BX.Iblock.SmartFilter(<?= CUtil::PhpToJSObject($arJsParams) ?>);
									  });
									  </script>

									  </div>
									  </div>
									  </aside>
									  <?
									  }
									  }
									 */
									//not prices
									foreach ($arResult["ITEMS"] as $key => $arItem)
									{
										if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
										{
											continue;
										}
										if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
										{
											continue;
										}
										$cnt = 0;
										$nonDis = 0;
										foreach ($arItem["VALUES"] as $val => $ar)
										{
											if ($ar['CHECKED'])
											{
												$cnt++;
											}
											if (!$ar['DISABLED'])
											{
												$nonDis++;
											}
										}
										?>
										<div class="shop-filter<?
										if ($arItem['CODE'] == "SIZE")
										{
											echo " shop-filter-size";
										} elseif ($arItem['CODE'] == "COLOR")
										{
											echo " shop-filter-color";
										}
										if ($nonDis <= 0)
										{
											echo " disabled";
										}
										?>" data-role="mainarea_<?= $arItem['ID'] ?>">
											<div class="shop-filter-title" id="headingflt-<?= $arItem['ID'] ?>">
												<div class="shop-filter-title-name">
													<?= $arItem["NAME"] ?><?= ($cnt > 0 ? ' (' . $cnt . ')' : '') ?>
												</div>
											</div>
											<div id="flt-<?= $arItem['ID'] ?>" class="shop-filter-info<?
											if ($arItem['CODE'] == "SIZE")
											{
												echo " size-filter";
											} elseif ($arItem['CODE'] == "COLOR")
											{
												echo " color-filter";
											}
											?> bx-filter-parameters-box" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
												<div class="shop-filter-info-body bx-filter-parameters-box">
													<ul>
														<?
														$arCur = current($arItem["VALUES"]);
														switch ($arItem["DISPLAY_TYPE"])
														{
															default://CHECKBOXES
																if ($arItem['CODE'] == "SIZE")
																{
																	foreach ($arItem["VALUES"] as $val => $ar)
																	{
																		?>
																		<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
																			<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
																				<span class="bx-filter-input-checkbox">
																					<input
																						type="checkbox"
																						value="<? echo $ar["HTML_VALUE"] ?>"
																						name="<? echo $ar["CONTROL_NAME"] ?>"
																						id="<? echo $ar["CONTROL_ID"] ?>"
																						<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																						onclick="smartFilter.click(this)"
																						/>
																				</span>
																			</label>
																			<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
																		</li>
																		<?
																	}
																	?>
																	<li style="float:none;" class="clearfix"></li>
																	<?
																} elseif ($arItem['CODE'] == "COLOR")
																{
																	foreach ($arItem["VALUES"] as $val => $ar)
																	{
																		$arColor = getElementData($val);
																		$strStyle = "";
																		if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0)
																		{
																			$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
																		} elseif ($arColor['PREVIEW_PICTURE'] > 0)
																		{
																			$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
																			$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
																		}
																		?>
																		<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
																			<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
																				<span class="bx-filter-input-checkbox">
																					<input
																						type="checkbox"
																						value="<? echo $ar["HTML_VALUE"] ?>"
																						name="<? echo $ar["CONTROL_NAME"] ?>"
																						id="<? echo $ar["CONTROL_ID"] ?>"
																						<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																						onclick="smartFilter.click(this)"
																						/>
																				</span>
																			</label>
																			<a href="<?= $ar['URL'] ?>"><span class="color color-img" style="<?= $strStyle ?>"></span><?= $ar["VALUE"]; ?></a>
																			<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
																		</li>
																		<?
																	}
																	?>
																	<li style="float:none;" class="clearfix"></li>
																	<?
																} else
																{
																	foreach ($arItem["VALUES"] as $val => $ar)
																	{
																		?>
																		<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?>">
																			<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
																				<span class="bx-filter-input-checkbox">
																					<input
																						type="checkbox"
																						value="<? echo $ar["HTML_VALUE"] ?>"
																						name="<? echo $ar["CONTROL_NAME"] ?>"
																						id="<? echo $ar["CONTROL_ID"] ?>"
																						<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																						onclick="smartFilter.click(this)"
																						/>
																				</span>
																			</label>
																			<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
																			<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
																		</li>
																		<?
																	}
																}
														}
														?>
													</ul>
													<div class="clearfix"></div>
													<span class="bx-filter-container-modef"></span>
												</div>
											</div>
										</div>
										<?
									}
									?>
									<input class="btn btn-link" type="submit" id="del_filter" name="del_filter" value="Сбросить" /> 
								</div>
								<div class="hide">
									<input class="btn btn-default" type="submit" id="set_filter" name="set_filter" value="Y" />
								</div>

								<div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>" id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?> style="display: inline-block;">
									<? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
									<span class="arrow"></span>
									<br/>
									<a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var smartFilter = new JCSmartFilter('<? echo CUtil::JSEscape($arResult["FORM_ACTION"]) ?>', '<?= CUtil::JSEscape($arParams["FILTER_VIEW_MODE"]) ?>', <?= CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"]) ?>);
	</script>
	<?
}
