<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$isExists = false;
if (!empty($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $arItem)
	{
		if (!empty($arItem['VALUES']))
		{
			$isExists = true;
			break;
		}
	}
}
if ($isExists)
{
	?>
	<div class="bx-filter">
		<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">
			<?
			foreach ($arResult["HIDDEN"] as $arItem)
			{
				?>
				<input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>" />
				<?
			}
			foreach ($arResult["ITEMS"] as $key => $arItem)
			{
				if ($arItem['CODE'] != "BRAND")
				{
					continue;
				}
				if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
				{
					continue;
				}
				if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
				{
					continue;
				}
				?>
				<aside class="panel panel-widget widget shop-filter mb-30<?
				if ($arItem['CODE'] == "SIZE")
				{
					echo " widget-size";
				} elseif ($arItem['CODE'] == "COLOR")
				{
					echo " widget-color";
				}
				?>">
					<div class="panel-heading widget-title" role="tab" id="headingflt-<?= $arItem['ID'] ?>">
						<div class="h4">
							<a class="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "" : "collapsed") ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#flt-<?= $arItem['ID'] ?>" aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>" aria-controls="flt-<?= $arItem['ID'] ?>">
								<?= $arItem["NAME"] ?>
							</a>
						</div>
					</div>
					<div id="flt-<?= $arItem['ID'] ?>" class="widget-info<?
					if ($arItem['CODE'] == "SIZE")
					{
						echo " size-filter";
					} elseif ($arItem['CODE'] == "COLOR")
					{
						echo " color-filter";
					}
					?> panel-collapse bx-filter-parameters-box<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? " collapse in" : " collapse") ?>" role="tabpanel" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
						<div class="panel-body bx-filter-parameters-box bx-active">
							<span class="bx-filter-container-modef"></span>
							<ul>
								<?
								$arCur = current($arItem["VALUES"]);
								switch ($arItem["DISPLAY_TYPE"])
								{
									default://CHECKBOXES
										if ($arItem['CODE'] == "SIZE")
										{
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
														<span class="bx-filter-input-checkbox">
															<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																/>
														</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
												</li>
												<?
											}
										} elseif ($arItem['CODE'] == "COLOR")
										{
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												$arColor = getElementData($val);
												$strStyle = "";
												if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0)
												{
													$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
												} elseif ($arColor['PREVIEW_PICTURE'] > 0)
												{
													$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
													$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
												}
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
														<span class="bx-filter-input-checkbox">
															<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																/>
														</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><span class="color color-img" style="<?= $strStyle ?>"></span><?= $ar["VALUE"]; ?></a>
													<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
												</li>
												<?
											}
											?>
											<?
										} else
										{
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
														<span class="bx-filter-input-checkbox">
															<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																/>
														</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
													<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
												</li>
												<?
												/*
												  <div class="checkbox">
												  <label data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled' : '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
												  <span class="bx-filter-input-checkbox">
												  <input
												  type="checkbox"
												  value="<? echo $ar["HTML_VALUE"] ?>"
												  name="<? echo $ar["CONTROL_NAME"] ?>"
												  id="<? echo $ar["CONTROL_ID"] ?>"
												  <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
												  onclick="smartFilter.click(this)"
												  />
												  <a href="<?= $ar['URL'] ?>" class="bx-filter-param-text" title="<?= $ar["VALUE"]; ?>"><?= $ar["VALUE"]; ?><?
												  if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
												  ?>&nbsp;(<span data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<? endif;
												  ?></a>
												  </span>
												  </label>
												  </div>
												  <?
												 */
											}
										}
								}
								?>
								<? /*
								  <li><a href="#"><span class="color color-1"></span>LightSalmon<span class="count">12</span></a></li>
								  <li><a href="#"><span class="color color-2"></span>Dark Salmon<span class="count">20</span></a></li>
								  <li><a href="#"><span class="color color-3"></span>Tomato<span class="count">59</span></a></li>
								  <li class="active"><a href="#"><span class="color color-4"></span>Deep Sky Blue<span class="count">45</span></a></li>
								  <li><a href="#"><span class="color color-5"></span>Electric Purple<span class="count">78</span></a></li>
								  <li><a href="#"><span class="color color-6"></span>Atlantis<span class="count">10</span></a></li>
								  <li><a href="#"><span class="color color-7"></span>Deep Lilac<span class="count">15</span></a></li>
								 * 
								 */ ?>
							</ul>
						</div>
					</div>
				</aside>

				<?
				/*
				  <div class="panel panel-default">
				  <div class="panel-heading" role="tab" id="headingflt-<?= $arItem['ID'] ?>">
				  <h4 class="panel-title">
				  <a class="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "" : "collapsed") ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#flt-<?= $arItem['ID'] ?>" aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>" aria-controls="flt-<?= $arItem['ID'] ?>">
				  <?= $arItem["NAME"] ?>
				  </a>
				  </h4>
				  </div>
				  <div id="flt-<?= $arItem['ID'] ?>" class="panel-collapse bx-filter-parameters-box<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? " collapse in" : " collapse") ?>" role="tabpanel" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
				  <div class="panel-body">
				  <span class="bx-filter-container-modef"></span>

				  </div>
				  </div>
				  </div>
				  <? */
			}
			foreach ($arResult["ITEMS"] as $key => $arItem)//prices
			{
				$arItem["DISPLAY_EXPANDED"] = "Y";
				$key = $arItem["ENCODED_ID"];
				if (isset($arItem["PRICE"]))
				{
					if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
					{
						continue;
					}
					$step_num = 4;
					$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
					$prices = array();
					if (Bitrix\Main\Loader::includeModule("currency"))
					{
						for ($i = 0; $i < $step_num; $i++)
						{
							$prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
						}
						$prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
					} else
					{
						$precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
						for ($i = 0; $i < $step_num; $i++)
						{
							$prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
						}
						$prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
					}
					/*
					  ?>
					  <aside>
					  <div class="widget-title">
					  <h4><?= $arItem["NAME"] ?></h4>
					  </div>
					  <div class="widget-info color-filter clearfix">
					  <ul>
					  <li><a href="#"><span class="color color-1"></span>LightSalmon<span class="count">12</span></a></li>
					  <li><a href="#"><span class="color color-2"></span>Dark Salmon<span class="count">20</span></a></li>
					  <li><a href="#"><span class="color color-3"></span>Tomato<span class="count">59</span></a></li>
					  <li class="active"><a href="#"><span class="color color-4"></span>Deep Sky Blue<span class="count">45</span></a></li>
					  <li><a href="#"><span class="color color-5"></span>Electric Purple<span class="count">78</span></a></li>
					  <li><a href="#"><span class="color color-6"></span>Atlantis<span class="count">10</span></a></li>
					  <li><a href="#"><span class="color color-7"></span>Deep Lilac<span class="count">15</span></a></li>
					  </ul>
					  </div>
					  </aside>
					 */
					?>
					<aside class="panel panel-widget widget  shop-filter mb-30">
						<div class="panel-heading widget-title" role="tab" id="headingflt-<?= $arItem['ID'] ?>">
							<div class="h4">
								<a class="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "" : "collapsed") ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#flt-<?= $arItem['ID'] ?>" aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>" aria-controls="flt-<?= $arItem['ID'] ?>">
									<?= $arItem["NAME"] ?>
								</a>
							</div>
						</div>
						<div id="flt-<?= $arItem['ID'] ?>" class="widget-info panel-collapse bx-filter-parameters-box<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? " collapse in" : " collapse") ?>" role="tabpanel" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
							<div class="panel-body bx-filter-parameters-box bx-active">
								<span class="bx-filter-container-modef"></span>
								<div class="bx-filter-block" data-role="bx_filter_block">
									<div class="row bx-filter-parameters-box-container">
										<input
											class="min-price"
											type="hidden"
											name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
											id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
											value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
											size="5"
											onkeyup="smartFilter.keyup(this)"
											/>
										<input
											class="max-price"
											type="hidden"
											name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
											id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
											value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
											size="5"
											onkeyup="smartFilter.keyup(this)"
											/>
										<div class="price_filter">
											<div class="price_slider_amount">
												<input type="button" value="Цена:"/> 
												<input type="text" id="amount" name="price"  placeholder="Выберите цену" /> 
											</div>
											<div id="slider-range"></div>
										</div>
										<div class="col-xs-12 bx-ui-slider-track-container">
											<div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
												<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?= $key ?>"></div>
												<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?= $key ?>"></div>
												<div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?= $key ?>"></div>
												<div class="bx-ui-slider-range" id="drag_tracker_<?= $key ?>"  style="left: 0%; right: 0%;">
													<a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?= $key ?>"></a>
													<a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?= $key ?>"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?
								$arJsParams = array(
									"leftSlider" => 'left_slider_' . $key,
									"rightSlider" => 'right_slider_' . $key,
									"tracker" => "drag_tracker_" . $key,
									"trackerWrap" => "drag_track_" . $key,
									"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
									"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
									"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
									"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
									"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
									"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
									"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
									"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
									"precision" => $precision,
									"colorUnavailableActive" => 'colorUnavailableActive_' . $key,
									"colorAvailableActive" => 'colorAvailableActive_' . $key,
									"colorAvailableInactive" => 'colorAvailableInactive_' . $key,
								);
								?>
								<script type="text/javascript">
									BX.ready(function () {
										window['trackBar<?= $key ?>'] = new BX.Iblock.SmartFilter(<?= CUtil::PhpToJSObject($arJsParams) ?>);
									});
								</script>

							</div>
						</div>
					</aside>
					<?
					/*
					  ?>
					  <div class="<? if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL"): ?>col-sm-6 col-md-4<? else: ?>col-lg-12<? endif ?> bx-filter-parameters-box bx-active">
					  <span class="bx-filter-container-modef"></span>
					  <div class="bx-filter-parameters-box-title" onclick="smartFilter.hideFilterProps(this)"><span><?= $arItem["NAME"] ?> <i data-role="prop_angle" class="fa fa-angle-<? if ($arItem["DISPLAY_EXPANDED"] == "Y"): ?>up<? else: ?>down<? endif ?>"></i></span></div>
					  <div class="bx-filter-block" data-role="bx_filter_block">
					  <div class="row bx-filter-parameters-box-container">
					  <div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
					  <i class="bx-ft-sub"><?= GetMessage("CT_BCSF_FILTER_FROM") ?></i>
					  <div class="bx-filter-input-container">
					  <input
					  class="min-price"
					  type="text"
					  name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
					  id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
					  value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
					  size="5"
					  onkeyup="smartFilter.keyup(this)"
					  />
					  </div>
					  </div>
					  <div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
					  <i class="bx-ft-sub"><?= GetMessage("CT_BCSF_FILTER_TO") ?></i>
					  <div class="bx-filter-input-container">
					  <input
					  class="max-price"
					  type="text"
					  name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
					  id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
					  value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
					  size="5"
					  onkeyup="smartFilter.keyup(this)"
					  />
					  </div>
					  </div>

					  <div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
					  <div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
					  <? for ($i = 0; $i <= $step_num; $i++): ?>
					  <div class="bx-ui-slider-part p<?= $i + 1 ?>"><span><?= $prices[$i] ?></span></div>
					  <? endfor; ?>

					  <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?= $key ?>"></div>
					  <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?= $key ?>"></div>
					  <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?= $key ?>"></div>
					  <div class="bx-ui-slider-range" id="drag_tracker_<?= $key ?>"  style="left: 0%; right: 0%;">
					  <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?= $key ?>"></a>
					  <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?= $key ?>"></a>
					  </div>
					  </div>
					  </div>
					  </div>
					  </div>
					  </div>
					  <?
					  $arJsParams = array(
					  "leftSlider" => 'left_slider_' . $key,
					  "rightSlider" => 'right_slider_' . $key,
					  "tracker" => "drag_tracker_" . $key,
					  "trackerWrap" => "drag_track_" . $key,
					  "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
					  "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
					  "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
					  "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
					  "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
					  "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
					  "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
					  "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
					  "precision" => $precision,
					  "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
					  "colorAvailableActive" => 'colorAvailableActive_' . $key,
					  "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
					  );
					  ?>
					  <script type="text/javascript">
					  BX.ready(function () {
					  window['trackBar<?= $key ?>'] = new BX.Iblock.SmartFilter(<?= CUtil::PhpToJSObject($arJsParams) ?>);
					  });
					  </script>
					  <?
					 * 
					 */
				}
			}
			//not prices
			foreach ($arResult["ITEMS"] as $key => $arItem)
			{
				if ($arItem['CODE'] == "BRAND")
				{
					continue;
				}
				if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
				{
					continue;
				}
				if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
				{
					continue;
				}
				?>
				<aside class="panel panel-widget widget shop-filter mb-30<?
				if ($arItem['CODE'] == "SIZE")
				{
					echo " widget-size";
				} elseif ($arItem['CODE'] == "COLOR")
				{
					echo " widget-color";
				}
				?>">
					<div class="panel-heading widget-title" role="tab" id="headingflt-<?= $arItem['ID'] ?>">
						<div class="h4">
							<a class="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "" : "collapsed") ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#flt-<?= $arItem['ID'] ?>" aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>" aria-controls="flt-<?= $arItem['ID'] ?>">
								<?= $arItem["NAME"] ?>
							</a>
						</div>
					</div>
					<div id="flt-<?= $arItem['ID'] ?>" class="widget-info<?
					if ($arItem['CODE'] == "SIZE")
					{
						echo " size-filter";
					} elseif ($arItem['CODE'] == "COLOR")
					{
						echo " color-filter";
					}
					?> panel-collapse bx-filter-parameters-box<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? " collapse in" : " collapse") ?>" role="tabpanel" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
						<div class="panel-body bx-filter-parameters-box bx-active">
							<span class="bx-filter-container-modef"></span>
							<ul>
								<?
								$arCur = current($arItem["VALUES"]);
								switch ($arItem["DISPLAY_TYPE"])
								{
									default://CHECKBOXES
										if ($arItem['CODE'] == "SIZE")
										{
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
														<span class="bx-filter-input-checkbox">
															<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																/>
														</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
												</li>
												<?
											}
										} elseif ($arItem['CODE'] == "COLOR")
										{
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												$arColor = getElementData($val);
												$strStyle = "";
												if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0)
												{
													$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
												} elseif ($arColor['PREVIEW_PICTURE'] > 0)
												{
													$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
													$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
												}
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
														<span class="bx-filter-input-checkbox">
															<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																/>
														</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><span class="color color-img" style="<?= $strStyle ?>"></span><?= $ar["VALUE"]; ?></a>
													<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
												</li>
												<?
											}
											?>
											<?
										} else
										{
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
														<span class="bx-filter-input-checkbox">
															<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																/>
														</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
													<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
												</li>
												<?
												/*
												  <div class="checkbox">
												  <label data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled' : '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
												  <span class="bx-filter-input-checkbox">
												  <input
												  type="checkbox"
												  value="<? echo $ar["HTML_VALUE"] ?>"
												  name="<? echo $ar["CONTROL_NAME"] ?>"
												  id="<? echo $ar["CONTROL_ID"] ?>"
												  <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
												  onclick="smartFilter.click(this)"
												  />
												  <a href="<?= $ar['URL'] ?>" class="bx-filter-param-text" title="<?= $ar["VALUE"]; ?>"><?= $ar["VALUE"]; ?><?
												  if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
												  ?>&nbsp;(<span data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<? endif;
												  ?></a>
												  </span>
												  </label>
												  </div>
												  <?
												 */
											}
										}
								}
								?>
								<? /*
								  <li><a href="#"><span class="color color-1"></span>LightSalmon<span class="count">12</span></a></li>
								  <li><a href="#"><span class="color color-2"></span>Dark Salmon<span class="count">20</span></a></li>
								  <li><a href="#"><span class="color color-3"></span>Tomato<span class="count">59</span></a></li>
								  <li class="active"><a href="#"><span class="color color-4"></span>Deep Sky Blue<span class="count">45</span></a></li>
								  <li><a href="#"><span class="color color-5"></span>Electric Purple<span class="count">78</span></a></li>
								  <li><a href="#"><span class="color color-6"></span>Atlantis<span class="count">10</span></a></li>
								  <li><a href="#"><span class="color color-7"></span>Deep Lilac<span class="count">15</span></a></li>
								 * 
								 */ ?>
							</ul>
						</div>
					</div>
				</aside>

				<?
				/*
				  <div class="panel panel-default">
				  <div class="panel-heading" role="tab" id="headingflt-<?= $arItem['ID'] ?>">
				  <h4 class="panel-title">
				  <a class="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "" : "collapsed") ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#flt-<?= $arItem['ID'] ?>" aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>" aria-controls="flt-<?= $arItem['ID'] ?>">
				  <?= $arItem["NAME"] ?>
				  </a>
				  </h4>
				  </div>
				  <div id="flt-<?= $arItem['ID'] ?>" class="panel-collapse bx-filter-parameters-box<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? " collapse in" : " collapse") ?>" role="tabpanel" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
				  <div class="panel-body">
				  <span class="bx-filter-container-modef"></span>

				  </div>
				  </div>
				  </div>
				  <? */
			}
			?>
			<div class="hide"
				 <input class="btn btn-default" type="submit" id="set_filter" name="set_filter" value="Y" />
			</div>
			<input class="btn btn-default" type="submit" id="del_filter" name="del_filter" value="Сбросить" />
			<div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>" id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?> style="display: inline-block;">
				<? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
				<span class="arrow"></span>
				<br/>
				<a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
			</div>
			<? /*
			  ?>
			  <div class="row mt-20">
			  <div class="col-xs-12 bx-filter-button-box">
			  <div class="bx-filter-block">
			  <div class="bx-filter-parameters-box-container">
			  <input
			  class="btn btn-default"
			  type="submit"
			  id="set_filter"
			  name="set_filter"
			  value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
			  />
			  <input
			  class="btn btn-default"
			  type="submit"
			  id="del_filter"
			  name="del_filter"
			  value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
			  />
			  <div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>" id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?> style="display: inline-block;">
			  <? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
			  <span class="arrow"></span>
			  <br/>
			  <a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
			  </div>
			  </div>
			  </div>
			  </div>
			  </div>
			 * 
			 */ ?>
		</form>
	</div>
	<script type="text/javascript">
		var smartFilter = new JCSmartFilter('<? echo CUtil::JSEscape($arResult["FORM_ACTION"]) ?>', '<?= CUtil::JSEscape($arParams["FILTER_VIEW_MODE"]) ?>', <?= CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"]) ?>);
	</script>
	<?
}
?>