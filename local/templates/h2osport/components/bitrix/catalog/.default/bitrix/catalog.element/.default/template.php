<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES'])) {
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS'],
	),
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId . '_dsc_pict',
	'STICKER_ID' => $mainId . '_sticker',
	'BIG_SLIDER_ID' => $mainId . '_big_slider',
	'BIG_IMG_CONT_ID' => $mainId . '_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId . '_slider_cont',
	'OLD_PRICE_ID' => $mainId . '_old_price',
	'PRICE_ID' => $mainId . '_price',
	'DISCOUNT_PRICE_ID' => $mainId . '_price_discount',
	'PRICE_TOTAL' => $mainId . '_price_total',
	'SLIDER_CONT_OF_ID' => $mainId . '_slider_cont_',
	'QUANTITY_ID' => $mainId . '_quantity',
	'QUANTITY_DOWN_ID' => $mainId . '_quant_down',
	'QUANTITY_UP_ID' => $mainId . '_quant_up',
	'QUANTITY_MEASURE' => $mainId . '_quant_measure',
	'QUANTITY_LIMIT' => $mainId . '_quant_limit',
	'BUY_LINK' => $mainId . '_buy_link',
	'ADD_BASKET_LINK' => $mainId . '_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId . '_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId . '_not_avail',
	'COMPARE_LINK' => $mainId . '_compare_link',
	'TREE_ID' => $mainId . '_skudiv',
	'DISPLAY_PROP_DIV' => $mainId . '_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
	'OFFER_GROUP' => $mainId . '_set_group_',
	'BASKET_PROP_DIV' => $mainId . '_basket_prop',
	'SUBSCRIBE_LINK' => $mainId . '_subscribe',
	'TABS_ID' => $mainId . '_tabs',
	'TAB_CONTAINERS_ID' => $mainId . '_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId . '_small_card_panel',
	'TABS_PANEL_ID' => $mainId . '_tabs_panel',
);
$obName = $templateData['JS_OBJ'] = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'] : $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'] : $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]) ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']] : reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer) {
		if ($offer['MORE_PHOTO_COUNT'] > 1) {
			$showSliderControls = true;
			break;
		}
	}
} else {
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$oldPrice = false;
if (isset($arResult['PRICES']['OLDPRICE']) && $arResult['PRICES']['OLDPRICE']['VALUE'] >= 0) {
	$oldPrice = $arResult['PRICES']['OLDPRICE'];
	$price = doModifyItemPrice($price, $oldPrice);
}
$showDiscount = $price['PERCENT'] > 0;
foreach ($arResult['OFFERS'] as $k => $arOffer) {
	$priceOffer = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];
	$oldPriceOffer = false;
	if (isset($arOffer['PRICES']['OLDPRICE']) && $arOffer['PRICES']['OLDPRICE']['VALUE'] >= 0) {
		$oldPriceOffer = $arOffer['PRICES']['OLDPRICE'];
		$priceOffer = doModifyItemPrice($priceOffer, $oldPriceOffer);
	}
	$showDiscountOffer = $priceOffer['PERCENT'] > 0;
	$arResult['OFFERS_PRICES'][$arOffer['ID']] = array(
		"price" => $priceOffer,
		"oldPrice" => $oldPriceOffer,
		"showDiscount" => $showDiscountOffer,
		"CAN_BUY" => $arOffer['CAN_BUY'],
		"PRICE_TEXT" => ($arOffer['CAN_BUY'] ? $priceOffer['PRINT_RATIO_PRICE'] : "Товар продан"),
		"OLD_PRICE_TEXT" => ($arOffer['CAN_BUY'] ? $priceOffer['PRINT_RATIO_BASE_PRICE'] : ""),
		"SHOW_DISCOUNT" => $showDiscountOffer ? "Y" : "N",
	);
}
$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top',
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION'])) {
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos) {
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION'])) {
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos) {
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
	}
}
$arOffersVariants = array();
?>
	<div itemscope itemtype="http://schema.org/Product">
		<div class="row shop-list single-pro-info" id="b-product-detail">
			<!-- Single-product start -->
			<div class="col-lg-12">
				<div class="single-product clearfix">
					<!-- Single-pro-slider Big-photo start -->
					<div class="single-pro-slider single-big-photo view-lightbox slider-for">
						<?
						foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $arPhoto) {
							$strOriginalPhoto = CFile::GetPath($arPhoto);
							$arPhoto = CFile::ResizeImageGet($arPhoto, array('width' => 370, 'height' => 450), BX_RESIZE_IMAGE_EXACT_ALT, true);
							?>
							<div>
								<?
								if ($arResult['PROPERTIES']['LABEL_NEW']['VALUE'] == "Y") {
									?>
									<span class="pro-label new-label">new</span>
									<?
								} elseif ($arResult['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y") {
									?>
									<span class="pro-label sale-label">sale</span>
									<?
								} elseif ($arResult['PROPERTIES']['LABEL_POPULAR']['VALUE'] == "Y") {
									?>
									<span class="pro-label popular-label">top</span>
									<?
								} elseif ($arResult['PROPERTIES']['LABEL_BLACK_FRIDAY']['VALUE'] == "Y") {
									?>
									<span class="pro-label black-label">black friday</span>
								<? } ?>
								<img src="<?= $arPhoto['src'] ?>" alt="" itemprop="image"/>
								<a class="view-full-screen" href="<?= $strOriginalPhoto ?>" data-lightbox="roadtrip" data-title="<?= $arResult['NAME'] ?>">
									<i class="zmdi zmdi-zoom-in"></i>
								</a>
							</div>
							<?
						}
						?>
					</div>
					<!-- Single-pro-slider Big-photo end -->
					<div class="product-info">
						<div class="fix pro-rating-floatright">
							<h1 class="post-title floatleft" itemprop="name"><?= $arResult['NAME'] ?></h1>
							<?
							$APPLICATION->IncludeComponent("bitrix:iblock.vote", "single", Array(
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"DISPLAY_AS_RATING" => "vote_avg",
								"ELEMENT_CODE" => "",
								"ELEMENT_ID" => $arResult['ID'],
								"IBLOCK_ID" => $arParams['IBLOCK_ID'],
								"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
								"MAX_VOTE" => "5",
								"MESSAGE_404" => "",
								"SET_STATUS_404" => "N",
								"SHOW_RATING" => "N",
								"VOTE_NAMES" => array(
									0 => "1",
									1 => "2",
									2 => "3",
									3 => "4",
									4 => "5",
									5 => "",
								),
							), false
							);
							?>
						</div>
						<div class="fix mb-20" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<?
							if ($arResult['CAN_BUY']) {
								//if ($arParams['SHOW_OLD_PRICE'] === 'Y')
								//{
								?>
								<label>Цена: </label>
								<span class="pro-price old-price" style="<?= ($showDiscount ? '' : 'display: none;') ?>" id="b-old-price-product">
								<?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?>
							</span>
								<?
								//}
								?>
								<span class="pro-price" id="b-price-product" itemprop="price" content="<?= $price['RATIO_PRICE'] ?>">
								<?= $price['PRINT_RATIO_PRICE'] ?>
							</span>
								<meta itemprop="priceCurrency" content="RUB"/>
								<?
								if ($arResult['PROPERTIES']['IS_PRESALE']['VALUE'] == "Y" || $arResult['IS_STORE_OTHER']) {
									if ($arResult['PROPERTIES']['SET_IS_SET']['VALUE'] != "Y") {
										$iDefaultOffer = false;
										if ($arResult['IS_EXISTS_COLOR'] && $arResult['IS_EXISTS_SIZE']) {
											$i = 1;
											foreach ($arResult['TREE'] as $iColor => $arSizes) {
												$j = 1;
												foreach ($arSizes as $iSize => $iOffer) {
													if ($i == 1 && $j == 1) {
														$iDefaultOffer = $iOffer;
														break(2);
													}
													$j++;
												}
												$i++;
											}
										} elseif ($arResult['IS_EXISTS_COLOR']) {
											$i = 1;
											foreach ($arResult['TREE'] as $iSize => $arOffer) {
												$iOffer = $arOffer['NOTREF'];
												if ($i == 1) {
													$iDefaultOffer = $iOffer;
													break;
												}
											}
										} elseif ($arResult['IS_EXISTS_SIZE']) {
											$i = 1;
											foreach ($arResult['TREE']['NOTREF'] as $iSize => $iOffer) {
												if ($i == 1) {
													$iDefaultOffer = $iOffer;
													break;
												}
											}
										}
									}

									$strHide = "";
									if (($arResult['PROPERTIES']['IS_PRESALE']['VALUE'] == "Y" && $arResult['OFFERS'][$iDefaultOffer]['CATALOG_QUANTITY'] > 0) || ($arResult['IS_STORE_OTHER'] && !$arResult['OFFERS'][$iDefaultOffer]['IS_STORE_OTHER'])) {
										$strHide = ' style="display:none;"';
									}
									if (!$arResult['IS_STORE_OTHER']) {
										?>
										<div class="presale-note"<?= $strHide ?>>
											Товар под заказ
										</div>
										<?
									}
									if ($arResult['PROPERTIES']['TEXT_PRESALE']['VALUE'] > 0) {
										$arElementText = CIBlockElement::GetList(array(), array("ID" => $arResult['PROPERTIES']['TEXT_PRESALE']['VALUE']))->Fetch();
										if ($arElementText) {
											?>
											<div class="presale-text"<?= $strHide ?>>
												<?= $arElementText['PREVIEW_TEXT'] ?>
											</div>
											<?
										}
									} elseif ($arResult['IS_STORE_OTHER']) {
										?>
										<div class="presale-text"<?= $strHide ?>>
											Срок поставки от 3-х дней
										</div>
										<?
									}
								}
							} else {
								?>
								<span class="pro-price old-price" style="display: none;" id="b-old-price-product"></span>
								<div class="pro-price" id="b-price-product">
									Товар продан
								</div>
								<?
							}
							?>
						</div>
						<div class="product-description">
							<label>Артикул:</label> <?= $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?>
						</div>
						<?
						if ($arResult['PROPERTIES']['SIZE_TABLE']['VALUE'] > 0) {
							$arSizeTable = getElementDataFull($arResult['PROPERTIES']['SIZE_TABLE']['VALUE']);
							?>
							<div class="product-description">
								<a href="javascript:void(0)" id="b-showsizetable" data-toggle="modal" data-target="#productModal">Таблица
									размеров</a>
								<div class="product-size-table" id="b-showsizetable-content">
									<?
									if ($arSizeTable['PREVIEW_PICTURE'] > 0) {
										?>
										<img src="<?= CFile::GetPath($arSizeTable['PREVIEW_PICTURE']) ?>" style="margin-top:40px;"/>
										<?
									} else {
										echo $arSizeTable['~PREVIEW_TEXT'];
									}
									?>
								</div>
							</div>
							<?
						}
						?>
						<div class="product-description">
							<?= $arResult['PREVIEW_TEXT'] ?>
						</div>
						<?
						$arVariantStat = array();
						if ($arResult['PROPERTIES']['SET_IS_SET']['VALUE'] != "Y") {
							$iDefaultOffer = false;
							if ($arResult['IS_EXISTS_COLOR'] && $arResult['IS_EXISTS_SIZE']) {
								?>
								<div class="color-filter single-pro-color mb-20 b-color-select clearfix">
									<ul>
										<li><span class="color-title text-capitalize">Цвет: </span></li>
										<?
										$i = 1;
										foreach ($arResult['TREE'] as $iColor => $arSize) {
											$arColor = getElementData($iColor);
											$strStyle = "";
											if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0) {
												$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
											} elseif ($arColor['PREVIEW_PICTURE'] > 0) {
												$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
												$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
											}
											if ($i == 1) {
												$arVariantStat[] = "Цвет " . $arColor['NAME'];
											}
											?>
											<li<?= ($i == 1 ? ' class="active"' : "") ?>>
												<a href="javascript:void(0)" data-color-id="<?= $iColor ?>" title="<?= $arColor['NAME'] ?>"><span class="color color-img" style="<?= $strStyle ?>"></span></a>
											</li>
											<?
											$i++;
										}
										?>
									</ul>
								</div>
								<div class="size-filter single-pro-size mb-20 b-color-size-select clearfix">
									<ul>
										<li><span class="color-title text-capitalize">Размер: </span></li>
										<?
										$i = 1;
										foreach ($arResult['TREE'] as $iColor => $arSizes) {
											$j = 1;
											foreach ($arSizes as $iSize => $iOffer) {
												$arSize = getElementData($iSize);
												if ($i == 1 && $j == 1) {
													$iDefaultOffer = $iOffer;
												}
												if ($i == 1 && $j == 1) {
													$arVariantStat[] = "Размер " . $arSize['NAME'];
												}
												?>
												<li<?= (($i == 1 && $j == 1) ? ' class="active"' : "") ?> data-isoffer="1" data-color-id="<?= $iColor ?>"<?= ($i != 1 ? ' style="display:none;"' : "") ?>>
													<a href="javascript:void(0)" data-offer-id="<?= $iOffer ?>" data-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['PRICE_TEXT'] ?>" data-old-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['OLD_PRICE_TEXT'] ?>" data-show-discount="<?= $arResult['OFFERS_PRICES'][$iOffer]['SHOW_DISCOUNT'] ?>" data-offer-quantity="<?= $arResult['OFFERS'][$iOffer]['CATALOG_QUANTITY'] ?>"><?= $arSize['NAME'] ?></a>
												</li>
												<?
												$j++;
											}
											$i++;
										}
										?>
									</ul>
								</div>
								<?
							} elseif ($arResult['IS_EXISTS_COLOR']) {
								?>
								<div class="color-filter single-pro-color mb-20 b-only-color-select clearfix">
									<ul>
										<li><span class="color-title text-capitalize">Цвет: </span></li>
										<?
										$i = 1;
										foreach ($arResult['TREE'] as $iSize => $arOffer) {
											$iOffer = $arOffer['NOTREF'];
											if ($i == 1) {
												$iDefaultOffer = $iOffer;
												break;
											}
											$arColor = getElementData($iColor);
											$strStyle = "";
											if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0) {
												$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
											} elseif ($arColor['PREVIEW_PICTURE'] > 0) {
												$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
												$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
											}
											if ($i == 1) {
												$arVariantStat[] = "Цвет " . $arColor['NAME'];
											}
											?>
											<li<?= ($i == 1 ? ' class="active"' : "") ?> data-isoffer="1" data-color-id="<?= $iColor ?>">
												<a href="javascript:void(0)" data-color-id="<?= $iColor ?>" title="<?= $arColor['NAME'] ?>" data-offer-id="<?= $iOffer ?>" data-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['PRICE_TEXT'] ?>" data-old-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['OLD_PRICE_TEXT'] ?>" data-show-discount="<?= $arResult['OFFERS_PRICES'][$iOffer]['SHOW_DISCOUNT'] ?>" data-offer-quantity="<?= $arResult['OFFERS'][$iOffer]['CATALOG_QUANTITY'] ?>"><span class="color color-img" style="<?= $strStyle ?>"></span></a>
											</li>
											<?
											$i++;
										}
										?>
									</ul>
								</div>
								<?
							} elseif ($arResult['IS_EXISTS_SIZE']) {
								?>
								<div class="size-filter single-pro-size mb-20 b-only-size-select clearfix">
									<ul>
										<li><span class="color-title text-capitalize">Размер: </span></li>
										<?
										$i = 1;
										foreach ($arResult['TREE']['NOTREF'] as $iSize => $iOffer) {
											$arSize = getElementData($iSize);
											if ($i == 1) {
												$iDefaultOffer = $iOffer;
											}
											if ($i == 1) {
												$arVariantStat[] = "Размер " . $arSize['NAME'];
											}
											?>
											<li<?= ($i == 1 ? ' class="active"' : "") ?>>
												<a href="javascript:void(0)" data-offer-id="<?= $iOffer ?>" data-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['PRICE_TEXT'] ?>" data-old-price="<?= $arResult['OFFERS_PRICES'][$iOffer]['OLD_PRICE_TEXT'] ?>" data-show-discount="<?= $arResult['OFFERS_PRICES'][$iOffer]['SHOW_DISCOUNT'] ?>" data-offer-quantity="<?= $arResult['OFFERS'][$iOffer]['CATALOG_QUANTITY'] ?>"><?= $arSize['NAME'] ?></a>
											</li>
											<?
											$i++;
										}
										?>
									</ul>
								</div>
								<?
							} else {

							}
							$ak = array_keys($arResult['OFFERS']);
							?>
							<div class="clearfix mb-35">
								<?
								foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty) {
									if (!in_array($arProperty['CODE'], array("BRAND"))) {
										continue;
									}
									if (!empty($arProperty['DISPLAY_VALUE'])) {
										?>
										<div class="product-description">
											<label><?= $arProperty['NAME'] ?>
												:</label> <?= (is_array($arProperty['DISPLAY_VALUE']) ? implode(' / ', $arProperty['DISPLAY_VALUE']) : $arProperty['DISPLAY_VALUE']) ?>
										</div>
										<?
									}
								}
								?>
							</div>
							<div class="clearfix">
								<div class="cart-plus-minus">
									<input type="text" value="1" name="qtybutton" class="cart-plus-minus-box"/>
								</div>
								<div class="quick-add-to-cart">
									<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" data-offer-id="<?= (count($arResult['OFFERS']) > 1 ? $iDefaultOffer : $arResult['OFFERS'][$ak[0]]['ID']) ?>" class="b-detail-add-basket single_add_to_cart_button" type="button">Добавить
										в корзину</a>
								</div>
								<? /*
							  <div class="product-action clearfix">
							  <a href="#" data-toggle="tooltip" data-placement="top" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
							  <a href="javascript:void(0)" data-toggle="modal"  data-target="#productModal" title="Быстрый просмотр" class="quick-view-button" data-id="<?= $arResult['ID'] ?>"><i class="zmdi zmdi-zoom-in"></i></a>
							  <? /* <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="zmdi zmdi-refresh"></i></a> */ ?>
								<? /*
							  $ak = array_keys($arResult['OFFERS']);
							  ?>
							  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Добавить в корзину" data-offer-id="<?= (count($arResult['OFFERS']) > 0 ? $iDefaultOffer : $arResult['OFFERS'][$ak[0]]['ID']) ?>" class="b-detail-add-basket"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
							 * 
							 *//* ?>
							  </div> */ ?>
							</div>
							<div class="clearfix">
								<div class="quick-add-to-cart">
									<a href="javascript:void(0)" title="Купить в 1 клик" data-offer-id="<?= (count($arResult['OFFERS']) > 0 ? $iDefaultOffer : $arResult['OFFERS'][$ak[0]]['ID']) ?>" class="b-detail-oneclick single_add_to_cart_button" type="button" id="b-onelicklink" data-toggle="modal" data-target="#productOneClickModal">Купить
										в 1 клик</a>
								</div>
							</div>
						<? } ?>
						<div class="single-pro-slider single-sml-photo slider-nav">
							<?
							foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $arPhoto) {
								$strDescription = $arResult['PROPERTIES']['MORE_PHOTOS']['DESCRIPTION'][$k];
								$strOriginalPhoto = CFile::GetPath($arPhoto);
								$arPhoto = CFile::ResizeImageGet($arPhoto, array('width' => 70, 'height' => 83), BX_RESIZE_IMAGE_EXACT_ALT, true);
								?>
								<div>
									<img src="<?= $arPhoto['src'] ?>" alt=""/>
									<?
									if (strlen($strDescription) > 0 || true) {
										?>
										<p><?= $strDescription ?></p>
										<?
									}
									?>
								</div>
								<?
							}
							?>
						</div>
						<div class="clearfix mt-30">
							<?
							$APPLICATION->IncludeComponent("bitrix:main.share", "detail", Array(
								"HANDLERS" => array(// Используемые соц. закладки и сети
									0 => "vk",
									1 => "gplus",
									2 => "twitter",
									3 => "pinterest",
									4 => "facebook",
								),
								"PAGE_TITLE" => $arResult['NAME'], // Заголовок страницы
								"PAGE_URL" => $APPLICATION->GetCurPage(), // URL страницы относительно корня сайта
								"SHORTEN_URL_KEY" => "", // Ключ API для bit.ly
								"SHORTEN_URL_LOGIN" => "", // Логин для bit.ly
							), false
							);
							?>
						</div>
					</div>
				</div>
				<!-- Single-product end -->
			</div>
		</div>
		<?
		if ($arResult['PROPERTIES']['SET_IS_SET']['VALUE'] == "Y") {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="single-product clearfix">
						<h4 class="post-title">Выберите вариант комплекта:</h4>
						<?
						$APPLICATION->IncludeComponent("webavk:blank.component", "element.complect", Array(
							"CACHE_TIME" => "3600", // Время кеширования (сек.)
							"CACHE_TYPE" => "N", // Тип кеширования
							"IBLOCK_ID" => IBLOCK_CATALOG,
							"IBLOCK_COMPLECTS" => IBLOCK_SET_ALL,
							"ID" => $arResult['ID'],
							"MAIN_PRICE" => $price,
						), false
						);
						?>
					</div>
				</div>
			</div>
			<?
		}
		?>
		<!-- single-product-tab start -->
		<div class="single-pro-tab">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="single-pro-tab-menu">
						<!-- Nav tabs -->
						<ul class="">
							<li class="active"><a href="#description" data-toggle="tab">Описание</a></li>
							<li><a href="#pay" data-toggle="tab">Оплата</a></li>
							<li><a href="#delivery" data-toggle="tab">Доставка</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="description">
							<div class="pro-tab-info pro-description" itemprop="description">
								<h3 class="tab-title title-border mb-30"><?= $arResult['NAME'] ?></h3>
								<?
								if (!empty($arResult['DISPLAY_PROPERTIES'])) {
									?>
									<div class="product-item-detail-info-container">
										<?
										if (!empty($arResult['DISPLAY_PROPERTIES'])) {
											?>
											<div class="product-item-detail-properties">
												<?
												foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty) {
													if (in_array($arProperty['CODE'], array("BRAND"))) {
														continue;
													}
													if (!empty($arProperty['DISPLAY_VALUE'])) {
														?>
														<p><label><?= $arProperty['NAME'] ?>
																:</label> <?= strip_tags((is_array($arProperty['DISPLAY_VALUE']) ? implode(' / ', $arProperty['DISPLAY_VALUE']) : $arProperty['DISPLAY_VALUE'])) ?>
														</p>
														<?
													}
												}
												?>
											</div>
											<?
										}
										?>
									</div>
								<? }
								?>
								<?= $arResult['~DETAIL_TEXT'] ?>
							</div>
						</div>
						<div class="tab-pane" id="pay">
							<div class="pro-tab-info pro-information">
								<noindex>
									<h3 class="tab-title title-border mb-30">Оплата</h3>
									<?
									$APPLICATION->IncludeComponent(
										"bitrix:main.include", "", Array(
											"AREA_FILE_SHOW" => "file",
											"AREA_FILE_SUFFIX" => "inc",
											"EDIT_TEMPLATE" => "",
											"PATH" => "/include/element.pay.php",
										)
									);
									?>
								</noindex>
							</div>
						</div>
						<div class="tab-pane" id="delivery">
							<div class="pro-tab-info pro-information">
								<noindex>
									<h3 class="tab-title title-border mb-30">Доставка</h3>
									<?
									$APPLICATION->IncludeComponent(
										"bitrix:main.include", "", Array(
											"AREA_FILE_SHOW" => "file",
											"AREA_FILE_SUFFIX" => "inc",
											"EDIT_TEMPLATE" => "",
											"PATH" => "/include/element.delivery.php",
										)
									);
									?>
								</noindex>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- single-product-tab end -->
	</div>
	<div id="popup-element-oneclick">
		<form action="javascript:void(0)" id="popup-element-oneclick-form">
			<h2><?
				if (!empty($arResult['PARSE_NAME'])) {
					?>
					<?= $arResult['PARSE_NAME']['ARTICLE'] ?> <?= $arResult['PARSE_NAME']['PRODUCT_TYPE'] ?> <?= $arResult['PARSE_NAME']['NAME'] ?> <?= $arResult["COLOR_ITEMS"][$arResult['ID']]['COLOR_NAME'] ?>
					<?
				} else {
					echo $arResult['NAME'];
				}
				?></h2>
			<? /*
		  <div class="form-group b-product__size">
		  <div class="product-size-content product-size-content-detail">
		  <p>Выберите размер</p>
		  <?
		  foreach ($arResult['OFFERS'] as $arOffer)
		  {
		  $strName = $arResult['SIZES_LIST'][$arOffer['PROPERTIES']['SIZE']['VALUE']]['NAME'];
		  $strTitle = $strName;
		  if (strlen($arResult['SIZES_LIST'][$arOffer['PROPERTIES']['SIZE']['VALUE']]['PROPERTY_NAME_FOR_KT_VALUE']) > 0)
		  {
		  $strTitle = $arResult['SIZES_LIST'][$arOffer['PROPERTIES']['SIZE']['VALUE']]['PROPERTY_NAME_FOR_KT_VALUE'];
		  }
		  if ($arOffer['CAN_BUY'])
		  {
		  /*
		  ?>
		  <a href="javascript:void(0)" title="<?= $strTitle ?>" data-toggle="tooltip" data-placement="top" data-id="<?= $arOffer['ID'] ?>" data-product-list="Карточка товара" data-product-name="<?= $arOffer['NAME'] ?>" data-product-id="<?= $arOffer['ID'] ?>" data-product-price="<?= $price['RATIO_PRICE'] ?>" data-product-brand="<?= GAProductBrand($arOffer['ID']) ?>" data-product-category="<?= GAProductCategoryPath($arOffer['ID']) ?>" data-product-variant="<?= GAProductVariant($arOffer['ID']) ?>" data-product-position="<?= $productIndex ?>" data-product-sku="<?= $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?>"><?= $strName ?></a>
		  <? *//*
		  } else
		  {
		  ?>
		  <span class="notavailabe" title="<?= $strTitle ?>" data-toggle="tooltip" data-placement="top"><?= $strName ?></span>
		  <?
		  }
		  }
		  ?>
		  </div>
		  </div>
		 */ ?>
			<div class="form-group">
				<label for="oneclick-phone">Телефон</label>
				<input type="text" class="form-control" id="oneclick-phone" placeholder="Введите телефон">
			</div>
			<div class="form-group">
				<label for="oneclick-fio">ФИО</label>
				<input type="text" class="form-control" id="oneclick-fio" placeholder="Введите ФИО">
			</div>
			<button type="submit" class="btn btn-primary product-oneclick-buy-button" data-id="">
				<span>Отправить заказ</span></button>
			<div class="error-text"></div>
		</form>
	</div>
<?
$ar = array();
foreach ($arResult['SECTION']['PATH'] as $arPath) {
	$ar[] = $arPath['NAME'];
}
$arMainProduct = array(
	"id" => $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'],
	"name" => $arResult['NAME'],
	"price" => $price['PRICE'],
	"brand" => strip_tags($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']),
	"category" => implode("/", $ar),
);
$arOffersVariants = array(
	$arResult['ID'] => $arMainProduct,
);
foreach ($arResult['OFFERS'] as $arOffer) {
	$arVariant = array();
	if ($arOffer['PROPERTIES']['COLOR']['VALUE'] > 0) {
		$arColor = getElementData($arOffer['PROPERTIES']['COLOR']['VALUE']);
		$arVariant[] = "Цвет " . $arColor['NAME'];
	}
	if ($arOffer['PROPERTIES']['SIZE']['VALUE'] > 0) {
		$arSize = getElementData($arOffer['PROPERTIES']['SIZE']['VALUE']);
		$arVariant[] = "Размер " . $arSize['NAME'];
	}
	$arMainProduct['variant'] = implode(", ", $arVariant);
	$arOffersVariants[$arOffer['ID']] = $arMainProduct;
}
?>
	<script type="text/javascript">
		<!--
		var productOffersECommerce =<?=CUtil::PhpToJSObject($arOffersVariants)?>;
		window.dataLayer.push({
			"ecommerce": {
				"currencyCode": "<?=$arResult['MIN_PRICE']['CURRENCY']?>",
				"detail": {
					"products": [
						{
							"id": "<?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?>",
							"name": "<?=$arResult['NAME']?>",
							"price": <?=$price['PRICE']?>,
							"brand": "<?=strip_tags($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE'])?>",
							"category": "<?
								$ar = array();
								foreach ($arResult['SECTION']['PATH'] as $arPath) {
									$ar[] = $arPath['NAME'];
								}
								echo implode("/", $ar);
								?>",
							"variant": "<?=implode(", ", $arVariantStat)?>"
						}
					]
				}
			}
		});
		-->
	</script>

<? /*



<div class="bx-catalog-element bx-<?= $arParams['TEMPLATE_THEME'] ?>" id="<?= $itemIds['ID'] ?>"
	 itemscope itemtype="http://schema.org/Product">
	<div class="container-fluid">
		<?
		if ($arParams['DISPLAY_NAME'] === 'Y')
		{
			?>
			<div class="row">
				<div class="col-xs-12">
					<h1 class="bx-title"><?= $name ?></h1>
				</div>
			</div>
			<?
		}
		?>
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="product-item-detail-slider-container" id="<?= $itemIds['BIG_SLIDER_ID'] ?>">
					<span class="product-item-detail-slider-close" data-entity="close-popup"></span>
					<div class="product-item-detail-slider-block
					<?= ($arParams['IMAGE_RESOLUTION'] === '1by1' ? 'product-item-detail-slider-block-square' : '') ?>"
						 data-entity="images-slider-block">
						<span class="product-item-detail-slider-left" data-entity="slider-control-left" style="display: none;"></span>
						<span class="product-item-detail-slider-right" data-entity="slider-control-right" style="display: none;"></span>
						<div class="product-item-label-text <?= $labelPositionClass ?>" id="<?= $itemIds['STICKER_ID'] ?>"
							 <?= (!$arResult['LABEL'] ? 'style="display: none;"' : '' ) ?>>
								 <?
								 if ($arResult['LABEL'] && !empty($arResult['LABEL_ARRAY_VALUE']))
								 {
									 foreach ($arResult['LABEL_ARRAY_VALUE'] as $code => $value)
									 {
										 ?>
									<div<?= (!isset($arParams['LABEL_PROP_MOBILE'][$code]) ? ' class="hidden-xs"' : '') ?>>
										<span title="<?= $value ?>"><?= $value ?></span>
									</div>
									<?
								}
							}
							?>
						</div>
						<?
						if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
						{
							if ($haveOffers)
							{
								?>
								<div class="product-item-label-ring <?= $discountPositionClass ?>" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"
									 style="display: none;">
								</div>
								<?
							} else
							{
								if ($price['DISCOUNT'] > 0)
								{
									?>
									<div class="product-item-label-ring <?= $discountPositionClass ?>" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"
										 title="<?= -$price['PERCENT'] ?>%">
										<span><?= -$price['PERCENT'] ?>%</span>
									</div>
									<?
								}
							}
						}
						?>
						<div class="product-item-detail-slider-images-container" data-entity="images-container">
							<?
							if (!empty($actualItem['MORE_PHOTO']))
							{
								foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
								{
									?>
									<div class="product-item-detail-slider-image<?= ($key == 0 ? ' active' : '') ?>" data-entity="image" data-id="<?= $photo['ID'] ?>">
										<img src="<?= $photo['SRC'] ?>" alt="<?= $alt ?>" title="<?= $title ?>"<?= ($key == 0 ? ' itemprop="image"' : '') ?>>
									</div>
									<?
								}
							}

							if ($arParams['SLIDER_PROGRESS'] === 'Y')
							{
								?>
								<div class="product-item-detail-slider-progress-bar" data-entity="slider-progress-bar" style="width: 0;"></div>
								<?
							}
							?>
						</div>
					</div>
					<?
					if ($showSliderControls)
					{
						if ($haveOffers)
						{
							foreach ($arResult['OFFERS'] as $keyOffer => $offer)
							{
								if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0)
									continue;

								$strVisible = $arResult['OFFERS_SELECTED'] == $keyOffer ? '' : 'none';
								?>
								<div class="product-item-detail-slider-controls-block" id="<?= $itemIds['SLIDER_CONT_OF_ID'] . $offer['ID'] ?>" style="display: <?= $strVisible ?>;">
									<?
									foreach ($offer['MORE_PHOTO'] as $keyPhoto => $photo)
									{
										?>
										<div class="product-item-detail-slider-controls-image<?= ($keyPhoto == 0 ? ' active' : '') ?>"
											 data-entity="slider-control" data-value="<?= $offer['ID'] . '_' . $photo['ID'] ?>">
											<img src="<?= $photo['SRC'] ?>">
										</div>
										<?
									}
									?>
								</div>
								<?
							}
						} else
						{
							?>
							<div class="product-item-detail-slider-controls-block" id="<?= $itemIds['SLIDER_CONT_ID'] ?>">
								<?
								if (!empty($actualItem['MORE_PHOTO']))
								{
									foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
									{
										?>
										<div class="product-item-detail-slider-controls-image<?= ($key == 0 ? ' active' : '') ?>"
											 data-entity="slider-control" data-value="<?= $photo['ID'] ?>">
											<img src="<?= $photo['SRC'] ?>">
										</div>
										<?
									}
								}
								?>
							</div>
							<?
						}
					}
					?>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="row">
					<div class="col-sm-6">
						<div class="product-item-detail-info-section">
							<?
							foreach ($arParams['PRODUCT_INFO_BLOCK_ORDER'] as $blockName)
							{
								switch ($blockName)
								{
									case 'sku':
										if ($haveOffers && !empty($arResult['OFFERS_PROP']))
										{
											?>
											<div id="<?= $itemIds['TREE_ID'] ?>">
												<?
												foreach ($arResult['SKU_PROPS'] as $skuProperty)
												{
													if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
														continue;

													$propertyId = $skuProperty['ID'];
													$skuProps[] = array(
														'ID' => $propertyId,
														'SHOW_MODE' => $skuProperty['SHOW_MODE'],
														'VALUES' => $skuProperty['VALUES'],
														'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
													);
													?>
													<div class="product-item-detail-info-container" data-entity="sku-line-block">
														<div class="product-item-detail-info-container-title"><?= htmlspecialcharsEx($skuProperty['NAME']) ?></div>
														<div class="product-item-scu-container">
															<div class="product-item-scu-block">
																<div class="product-item-scu-list">
																	<ul class="product-item-scu-item-list">
																		<?
																		foreach ($skuProperty['VALUES'] as &$value)
																		{
																			$value['NAME'] = htmlspecialcharsbx($value['NAME']);

																			if ($skuProperty['SHOW_MODE'] === 'PICT')
																			{
																				?>
																				<li class="product-item-scu-item-color-container" title="<?= $value['NAME'] ?>"
																					data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
																					data-onevalue="<?= $value['ID'] ?>">
																					<div class="product-item-scu-item-color-block">
																						<div class="product-item-scu-item-color" title="<?= $value['NAME'] ?>"
																							 style="background-image: url('<?= $value['PICT']['SRC'] ?>');">
																						</div>
																					</div>
																				</li>
																				<?
																			} else
																			{
																				?>
																				<li class="product-item-scu-item-text-container" title="<?= $value['NAME'] ?>"
																					data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
																					data-onevalue="<?= $value['ID'] ?>">
																					<div class="product-item-scu-item-text-block">
																						<div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
																					</div>
																				</li>
																				<?
																			}
																		}
																		?>
																	</ul>
																	<div style="clear: both;"></div>
																</div>
															</div>
														</div>
													</div>
													<?
												}
												?>
											</div>
											<?
										}

										break;

									case 'props':
										if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
										{
											?>
											<div class="product-item-detail-info-container">
												<?
												if (!empty($arResult['DISPLAY_PROPERTIES']))
												{
													?>
													<dl class="product-item-detail-properties">
														<?
														foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
														{
															if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE'][$property['CODE']]))
															{
																?>
																<dt><?= $property['NAME'] ?></dt>
																<dd><?=
																	(is_array($property['DISPLAY_VALUE']) ? implode(' / ', $property['DISPLAY_VALUE']) : $property['DISPLAY_VALUE'])
																	?>
																</dd>
																<?
															}
														}
														unset($property);
														?>
													</dl>
													<?
												}

												if ($arResult['SHOW_OFFERS_PROPS'])
												{
													?>
													<dl class="product-item-detail-properties" id="<?= $itemIds['DISPLAY_MAIN_PROP_DIV'] ?>"></dl>
													<?
												}
												?>
											</div>
											<?
										}

										break;
								}
							}
							?>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="product-item-detail-pay-block">
							<?
							foreach ($arParams['PRODUCT_PAY_BLOCK_ORDER'] as $blockName)
							{
								switch ($blockName)
								{
									case 'rating':
										if ($arParams['USE_VOTE_RATING'] === 'Y')
										{
											?>
											<div class="product-item-detail-info-container">
												<?
												$APPLICATION->IncludeComponent(
														'bitrix:iblock.vote', 'stars', array(
													'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
													'IBLOCK_ID' => $arParams['IBLOCK_ID'],
													'ELEMENT_ID' => $arResult['ID'],
													'ELEMENT_CODE' => '',
													'MAX_VOTE' => '5',
													'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
													'SET_STATUS_404' => 'N',
													'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
													'CACHE_TYPE' => $arParams['CACHE_TYPE'],
													'CACHE_TIME' => $arParams['CACHE_TIME']
														), $component, array('HIDE_ICONS' => 'Y')
												);
												?>
											</div>
											<?
										}

										break;

									case 'price':
										?>
										<div class="product-item-detail-info-container">
											<?
											if ($arParams['SHOW_OLD_PRICE'] === 'Y')
											{
												?>
												<div class="product-item-detail-price-old" id="<?= $itemIds['OLD_PRICE_ID'] ?>"
													 style="display: <?= ($showDiscount ? '' : 'none') ?>;">
														 <?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?>
												</div>
												<?
											}
											?>
											<div class="product-item-detail-price-current" id="<?= $itemIds['PRICE_ID'] ?>">
												<?= $price['PRINT_RATIO_PRICE'] ?>
											</div>
											<?
											if ($arParams['SHOW_OLD_PRICE'] === 'Y')
											{
												?>
												<div class="item_economy_price" id="<?= $itemIds['DISCOUNT_PRICE_ID'] ?>"
													 style="display: <?= ($showDiscount ? '' : 'none') ?>;">
														 <?
														 if ($showDiscount)
														 {
															 echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
														 }
														 ?>
												</div>
												<?
											}
											?>
										</div>
										<?
										break;

									case 'priceRanges':
										if ($arParams['USE_PRICE_COUNT'])
										{
											$showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
											$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
											?>
											<div class="product-item-detail-info-container"
											<?= $showRanges ? '' : 'style="display: none;"' ?>
												 data-entity="price-ranges-block">
												<div class="product-item-detail-info-container-title">
													<?= $arParams['MESS_PRICE_RANGES_TITLE'] ?>
													<span data-entity="price-ranges-ratio-header">
														(<?=
														(Loc::getMessage(
																'CT_BCE_CATALOG_RATIO_PRICE', array('#RATIO#' => ($useRatio ? $measureRatio : '1') . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
														))
														?>)
													</span>
												</div>
												<dl class="product-item-detail-properties" data-entity="price-ranges-body">
													<?
													if ($showRanges)
													{
														foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range)
														{
															if ($range['HASH'] !== 'ZERO-INF')
															{
																$itemPrice = false;

																foreach ($arResult['ITEM_PRICES'] as $itemPrice)
																{
																	if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
																	{
																		break;
																	}
																}

																if ($itemPrice)
																{
																	?>
																	<dt>
																		<?
																		echo Loc::getMessage(
																				'CT_BCE_CATALOG_RANGE_FROM', array('#FROM#' => $range['SORT_FROM'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
																		) . ' ';

																		if (is_infinite($range['SORT_TO']))
																		{
																			echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
																		} else
																		{
																			echo Loc::getMessage(
																					'CT_BCE_CATALOG_RANGE_TO', array('#TO#' => $range['SORT_TO'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
																			);
																		}
																		?>
																	</dt>
																	<dd><?= ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) ?></dd>
																	<?
																}
															}
														}
													}
													?>
												</dl>
											</div>
											<?
											unset($showRanges, $useRatio, $itemPrice, $range);
										}

										break;

									case 'quantityLimit':
										if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
										{
											if ($haveOffers)
											{
												?>
												<div class="product-item-detail-info-container" id="<?= $itemIds['QUANTITY_LIMIT'] ?>" style="display: none;">
													<div class="product-item-detail-info-container-title">
														<?= $arParams['MESS_SHOW_MAX_QUANTITY'] ?>:
														<span class="product-item-quantity" data-entity="quantity-limit-value"></span>
													</div>
												</div>
												<?
											} else
											{
												if (
														$measureRatio && (float) $actualItem['CATALOG_QUANTITY'] > 0 && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y' && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N'
												)
												{
													?>
													<div class="product-item-detail-info-container" id="<?= $itemIds['QUANTITY_LIMIT'] ?>">
														<div class="product-item-detail-info-container-title">
															<?= $arParams['MESS_SHOW_MAX_QUANTITY'] ?>:
															<span class="product-item-quantity" data-entity="quantity-limit-value">
																<?
																if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
																{
																	if ((float) $actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
																	{
																		echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
																	} else
																	{
																		echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
																	}
																} else
																{
																	echo $actualItem['CATALOG_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
																}
																?>
															</span>
														</div>
													</div>
													<?
												}
											}
										}

										break;

									case 'quantity':
										if ($arParams['USE_PRODUCT_QUANTITY'])
										{
											?>
											<div class="product-item-detail-info-container" style="<?= (!$actualItem['CAN_BUY'] ? 'display: none;' : '') ?>"
												 data-entity="quantity-block">
												<div class="product-item-detail-info-container-title"><?= Loc::getMessage('CATALOG_QUANTITY') ?></div>
												<div class="product-item-amount">
													<div class="product-item-amount-field-container">
														<a class="product-item-amount-field-btn-minus" id="<?= $itemIds['QUANTITY_DOWN_ID'] ?>"
														   href="javascript:void(0)" rel="nofollow">
														</a>
														<input class="product-item-amount-field" id="<?= $itemIds['QUANTITY_ID'] ?>" type="tel"
															   value="<?= $price['MIN_QUANTITY'] ?>">
														<a class="product-item-amount-field-btn-plus" id="<?= $itemIds['QUANTITY_UP_ID'] ?>"
														   href="javascript:void(0)" rel="nofollow">
														</a>
														<span class="product-item-amount-description-container">
															<span id="<?= $itemIds['QUANTITY_MEASURE'] ?>">
																<?= $actualItem['ITEM_MEASURE']['TITLE'] ?>
															</span>
															<span id="<?= $itemIds['PRICE_TOTAL'] ?>"></span>
														</span>
													</div>
												</div>
											</div>
											<?
										}

										break;

									case 'buttons':
										?>
										<div data-entity="main-button-container">
											<div id="<?= $itemIds['BASKET_ACTIONS_ID'] ?>" style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;">
												<?
												if ($showAddBtn)
												{
													?>
													<div class="product-item-detail-info-container">
														<a class="btn <?= $showButtonClassName ?> product-item-detail-buy-button" id="<?= $itemIds['ADD_BASKET_LINK'] ?>"
														   href="javascript:void(0);">
															<span><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></span>
														</a>
													</div>
													<?
												}

												if ($showBuyBtn)
												{
													?>
													<div class="product-item-detail-info-container">
														<a class="btn <?= $buyButtonClassName ?> product-item-detail-buy-button" id="<?= $itemIds['BUY_LINK'] ?>"
														   href="javascript:void(0);">
															<span><?= $arParams['MESS_BTN_BUY'] ?></span>
														</a>
													</div>
													<?
												}
												?>
											</div>
											<?
											if ($showSubscribe)
											{
												?>
												<div class="product-item-detail-info-container">
													<?
													$APPLICATION->IncludeComponent(
															'bitrix:catalog.product.subscribe', '', array(
														'PRODUCT_ID' => $arResult['ID'],
														'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
														'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
														'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
														'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
															), $component, array('HIDE_ICONS' => 'Y')
													);
													?>
												</div>
												<?
											}
											?>
											<div class="product-item-detail-info-container">
												<a class="btn btn-link product-item-detail-buy-button" id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>"
												   href="javascript:void(0)"
												   rel="nofollow" style="display: <?= (!$actualItem['CAN_BUY'] ? '' : 'none') ?>;">
													   <?= $arParams['MESS_NOT_AVAILABLE'] ?>
												</a>
											</div>
										</div>
										<?
										break;
								}
							}

							if ($arParams['DISPLAY_COMPARE'])
							{
								?>
								<div class="product-item-detail-compare-container">
									<div class="product-item-detail-compare">
										<div class="checkbox">
											<label id="<?= $itemIds['COMPARE_LINK'] ?>">
												<input type="checkbox" data-entity="compare-checkbox">
												<span data-entity="compare-title"><?= $arParams['MESS_BTN_COMPARE'] ?></span>
											</label>
										</div>
									</div>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<?
				if ($haveOffers)
				{
					if ($arResult['OFFER_GROUP'])
					{
						foreach ($arResult['OFFER_GROUP_VALUES'] as $offerId)
						{
							?>
							<span id="<?= $itemIds['OFFER_GROUP'] . $offerId ?>" style="display: none;">
								<?
								$APPLICATION->IncludeComponent(
										'bitrix:catalog.set.constructor', '.default', array(
									'IBLOCK_ID' => $arResult['OFFERS_IBLOCK'],
									'ELEMENT_ID' => $offerId,
									'PRICE_CODE' => $arParams['PRICE_CODE'],
									'BASKET_URL' => $arParams['BASKET_URL'],
									'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
									'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									'CACHE_TIME' => $arParams['CACHE_TIME'],
									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
									'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
									'CURRENCY_ID' => $arParams['CURRENCY_ID']
										), $component, array('HIDE_ICONS' => 'Y')
								);
								?>
							</span>
							<?
						}
					}
				} else
				{
					if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
					{
						$APPLICATION->IncludeComponent(
								'bitrix:catalog.set.constructor', '.default', array(
							'IBLOCK_ID' => $arParams['IBLOCK_ID'],
							'ELEMENT_ID' => $arResult['ID'],
							'PRICE_CODE' => $arParams['PRICE_CODE'],
							'BASKET_URL' => $arParams['BASKET_URL'],
							'CACHE_TYPE' => $arParams['CACHE_TYPE'],
							'CACHE_TIME' => $arParams['CACHE_TIME'],
							'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
							'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID']
								), $component, array('HIDE_ICONS' => 'Y')
						);
					}
				}
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8 col-md-9">
				<div class="row" id="<?= $itemIds['TABS_ID'] ?>">
					<div class="col-xs-12">
						<div class="product-item-detail-tabs-container">
							<ul class="product-item-detail-tabs-list">
								<?
								if ($showDescription)
								{
									?>
									<li class="product-item-detail-tab active" data-entity="tab" data-value="description">
										<a href="javascript:void(0);" class="product-item-detail-tab-link">
											<span><?= $arParams['MESS_DESCRIPTION_TAB'] ?></span>
										</a>
									</li>
									<?
								}

								if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
								{
									?>
									<li class="product-item-detail-tab" data-entity="tab" data-value="properties">
										<a href="javascript:void(0);" class="product-item-detail-tab-link">
											<span><?= $arParams['MESS_PROPERTIES_TAB'] ?></span>
										</a>
									</li>
									<?
								}

								if ($arParams['USE_COMMENTS'] === 'Y')
								{
									?>
									<li class="product-item-detail-tab" data-entity="tab" data-value="comments">
										<a href="javascript:void(0);" class="product-item-detail-tab-link">
											<span><?= $arParams['MESS_COMMENTS_TAB'] ?></span>
										</a>
									</li>
									<?
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<div class="row" id="<?= $itemIds['TAB_CONTAINERS_ID'] ?>">
					<div class="col-xs-12">
						<?
						if ($showDescription)
						{
							?>
							<div class="product-item-detail-tab-content active" data-entity="tab-container" data-value="description"
								 itemprop="description">
									 <?
									 if (
											 $arResult['PREVIEW_TEXT'] != '' && (
											 $arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S' || ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
											 )
									 )
									 {
										 echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>' . $arResult['PREVIEW_TEXT'] . '</p>';
									 }

									 if ($arResult['DETAIL_TEXT'] != '')
									 {
										 echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>' . $arResult['DETAIL_TEXT'] . '</p>';
									 }
									 ?>
							</div>
							<?
						}

						if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
						{
							?>
							<div class="product-item-detail-tab-content" data-entity="tab-container" data-value="properties">
								<?
								if (!empty($arResult['DISPLAY_PROPERTIES']))
								{
									?>
									<dl class="product-item-detail-properties">
										<?
										foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
										{
											?>
											<dt><?= $property['NAME'] ?></dt>
											<dd><?=
												(
												is_array($property['DISPLAY_VALUE']) ? implode(' / ', $property['DISPLAY_VALUE']) : $property['DISPLAY_VALUE']
												)
												?>
											</dd>
											<?
										}
										unset($property);
										?>
									</dl>
									<?
								}

								if ($arResult['SHOW_OFFERS_PROPS'])
								{
									?>
									<dl class="product-item-detail-properties" id="<?= $itemIds['DISPLAY_PROP_DIV'] ?>"></dl>
									<?
								}
								?>
							</div>
							<?
						}

						if ($arParams['USE_COMMENTS'] === 'Y')
						{
							?>
							<div class="product-item-detail-tab-content" data-entity="tab-container" data-value="comments" style="display: none;">
								<?
								$componentCommentsParams = array(
									'ELEMENT_ID' => $arResult['ID'],
									'ELEMENT_CODE' => '',
									'IBLOCK_ID' => $arParams['IBLOCK_ID'],
									'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
									'URL_TO_COMMENT' => '',
									'WIDTH' => '',
									'COMMENTS_COUNT' => '5',
									'BLOG_USE' => $arParams['BLOG_USE'],
									'FB_USE' => $arParams['FB_USE'],
									'FB_APP_ID' => $arParams['FB_APP_ID'],
									'VK_USE' => $arParams['VK_USE'],
									'VK_API_ID' => $arParams['VK_API_ID'],
									'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									'CACHE_TIME' => $arParams['CACHE_TIME'],
									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									'BLOG_TITLE' => '',
									'BLOG_URL' => $arParams['BLOG_URL'],
									'PATH_TO_SMILE' => '',
									'EMAIL_NOTIFY' => $arParams['BLOG_EMAIL_NOTIFY'],
									'AJAX_POST' => 'Y',
									'SHOW_SPAM' => 'Y',
									'SHOW_RATING' => 'N',
									'FB_TITLE' => '',
									'FB_USER_ADMIN_ID' => '',
									'FB_COLORSCHEME' => 'light',
									'FB_ORDER_BY' => 'reverse_time',
									'VK_TITLE' => '',
									'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME']
								);
								if (isset($arParams["USER_CONSENT"]))
									$componentCommentsParams["USER_CONSENT"] = $arParams["USER_CONSENT"];
								if (isset($arParams["USER_CONSENT_ID"]))
									$componentCommentsParams["USER_CONSENT_ID"] = $arParams["USER_CONSENT_ID"];
								if (isset($arParams["USER_CONSENT_IS_CHECKED"]))
									$componentCommentsParams["USER_CONSENT_IS_CHECKED"] = $arParams["USER_CONSENT_IS_CHECKED"];
								if (isset($arParams["USER_CONSENT_IS_LOADED"]))
									$componentCommentsParams["USER_CONSENT_IS_LOADED"] = $arParams["USER_CONSENT_IS_LOADED"];
								$APPLICATION->IncludeComponent(
										'bitrix:catalog.comments', '', $componentCommentsParams, $component, array('HIDE_ICONS' => 'Y')
								);
								?>
							</div>
							<?
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-3">
				<div>
					<?
					if ($arParams['BRAND_USE'] === 'Y')
					{
						$APPLICATION->IncludeComponent(
								'bitrix:catalog.brandblock', '.default', array(
							'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
							'IBLOCK_ID' => $arParams['IBLOCK_ID'],
							'ELEMENT_ID' => $arResult['ID'],
							'ELEMENT_CODE' => '',
							'PROP_CODE' => $arParams['BRAND_PROP_CODE'],
							'CACHE_TYPE' => $arParams['CACHE_TYPE'],
							'CACHE_TIME' => $arParams['CACHE_TIME'],
							'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
							'WIDTH' => '',
							'HEIGHT' => ''
								), $component, array('HIDE_ICONS' => 'Y')
						);
					}
					?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<?
				if ($arResult['CATALOG'] && $actualItem['CAN_BUY'] && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
				{
					$APPLICATION->IncludeComponent(
							'bitrix:sale.prediction.product.detail', '.default', array(
						'BUTTON_ID' => $showBuyBtn ? $itemIds['BUY_LINK'] : $itemIds['ADD_BASKET_LINK'],
						'POTENTIAL_PRODUCT_TO_BUY' => array(
							'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
							'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
							'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
							'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
							'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,
							'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][0]['ID']) ? $arResult['OFFERS'][0]['ID'] : null,
							'SECTION' => array(
								'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
								'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
								'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
								'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
							),
						)
							), $component, array('HIDE_ICONS' => 'Y')
					);
				}

				if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
				{
					?>
					<div data-entity="parent-container">
						<?
						if (!isset($arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'] !== 'Y')
						{
							?>
							<div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
								<?= ($arParams['GIFTS_DETAIL_BLOCK_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_GIFT_BLOCK_TITLE_DEFAULT')) ?>
							</div>
							<?
						}

						CBitrixComponent::includeComponentClass('bitrix:sale.products.gift');
						$APPLICATION->IncludeComponent(
								'bitrix:sale.products.gift', '.default', array(
							'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
							'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
							'PRODUCT_ROW_VARIANTS' => "",
							'PAGE_ELEMENT_COUNT' => 0,
							'DEFERRED_PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
									SaleProductsGiftComponent::predictRowVariants(
											$arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'], $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT']
									)
							),
							'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
							'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
							'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
							'PRODUCT_DISPLAY_MODE' => 'Y',
							'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],
							'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
							'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
							'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',
							'TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
							'LABEL_PROP_' . $arParams['IBLOCK_ID'] => array(),
							'LABEL_PROP_MOBILE_' . $arParams['IBLOCK_ID'] => array(),
							'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
							'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
							'MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
							'MESS_BTN_ADD_TO_BASKET' => $arParams['~GIFTS_MESS_BTN_BUY'],
							'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
							'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
							'SHOW_PRODUCTS_' . $arParams['IBLOCK_ID'] => 'Y',
							'PROPERTY_CODE_' . $arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE'],
							'PROPERTY_CODE_MOBILE' . $arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE_MOBILE'],
							'PROPERTY_CODE_' . $arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
							'OFFER_TREE_PROPS_' . $arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
							'CART_PROPERTIES_' . $arResult['OFFERS_IBLOCK'] => $arParams['OFFERS_CART_PROPERTIES'],
							'ADDITIONAL_PICT_PROP_' . $arParams['IBLOCK_ID'] => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
							'ADDITIONAL_PICT_PROP_' . $arResult['OFFERS_IBLOCK'] => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),
							'HIDE_NOT_AVAILABLE' => 'Y',
							'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
							'PRICE_CODE' => $arParams['PRICE_CODE'],
							'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
							'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'BASKET_URL' => $arParams['BASKET_URL'],
							'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
							'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
							'PARTIAL_PRODUCT_PROPERTIES' => $arParams['PARTIAL_PRODUCT_PROPERTIES'],
							'USE_PRODUCT_QUANTITY' => 'N',
							'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
							'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
							'POTENTIAL_PRODUCT_TO_BUY' => array(
								'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
								'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
								'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
								'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
								'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,
								'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']) ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'] : null,
								'SECTION' => array(
									'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
									'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
									'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
									'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
								),
							),
							'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
							'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
							'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
								), $component, array('HIDE_ICONS' => 'Y')
						);
						?>
					</div>
					<?
				}

				if ($arResult['CATALOG'] && $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
				{
					?>
					<div data-entity="parent-container">
						<?
						if (!isset($arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'] !== 'Y')
						{
							?>
							<div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
								<?= ($arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_GIFTS_MAIN_BLOCK_TITLE_DEFAULT')) ?>
							</div>
							<?
						}

						$APPLICATION->IncludeComponent(
								'bitrix:sale.gift.main.products', '.default', array(
							'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
							'LINE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
							'HIDE_BLOCK_TITLE' => 'Y',
							'BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
							'OFFERS_FIELD_CODE' => $arParams['OFFERS_FIELD_CODE'],
							'OFFERS_PROPERTY_CODE' => $arParams['OFFERS_PROPERTY_CODE'],
							'AJAX_MODE' => $arParams['AJAX_MODE'],
							'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
							'IBLOCK_ID' => $arParams['IBLOCK_ID'],
							'ELEMENT_SORT_FIELD' => 'ID',
							'ELEMENT_SORT_ORDER' => 'DESC',
							//'ELEMENT_SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
							//'ELEMENT_SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
							'FILTER_NAME' => 'searchFilter',
							'SECTION_URL' => $arParams['SECTION_URL'],
							'DETAIL_URL' => $arParams['DETAIL_URL'],
							'BASKET_URL' => $arParams['BASKET_URL'],
							'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
							'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
							'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
							'CACHE_TYPE' => $arParams['CACHE_TYPE'],
							'CACHE_TIME' => $arParams['CACHE_TIME'],
							'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
							'SET_TITLE' => $arParams['SET_TITLE'],
							'PROPERTY_CODE' => $arParams['PROPERTY_CODE'],
							'PRICE_CODE' => $arParams['PRICE_CODE'],
							'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
							'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
							'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => 'Y',
							'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],
							'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
							'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
							'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',
							'ADD_PICT_PROP' => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
							'LABEL_PROP' => (isset($arParams['LABEL_PROP']) ? $arParams['LABEL_PROP'] : ''),
							'LABEL_PROP_MOBILE' => (isset($arParams['LABEL_PROP_MOBILE']) ? $arParams['LABEL_PROP_MOBILE'] : ''),
							'LABEL_PROP_POSITION' => (isset($arParams['LABEL_PROP_POSITION']) ? $arParams['LABEL_PROP_POSITION'] : ''),
							'OFFER_ADD_PICT_PROP' => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),
							'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : ''),
							'SHOW_DISCOUNT_PERCENT' => (isset($arParams['SHOW_DISCOUNT_PERCENT']) ? $arParams['SHOW_DISCOUNT_PERCENT'] : ''),
							'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
							'SHOW_OLD_PRICE' => (isset($arParams['SHOW_OLD_PRICE']) ? $arParams['SHOW_OLD_PRICE'] : ''),
							'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
							'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
							'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
							'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
							'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
							'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
							'DISPLAY_COMPARE' => (isset($arParams['DISPLAY_COMPARE']) ? $arParams['DISPLAY_COMPARE'] : ''),
							'COMPARE_PATH' => (isset($arParams['COMPARE_PATH']) ? $arParams['COMPARE_PATH'] : ''),
								) + array(
							'OFFER_ID' => empty($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']) ? $arResult['ID'] : $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'],
							'SECTION_ID' => $arResult['SECTION']['ID'],
							'ELEMENT_ID' => $arResult['ID'],
							'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
							'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
							'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
								), $component, array('HIDE_ICONS' => 'Y')
						);
						?>
					</div>
					<?
				}
				?>
			</div>
		</div>
	</div>
	<!--Small Card-->
	<div class="product-item-detail-short-card-fixed hidden-xs" id="<?= $itemIds['SMALL_CARD_PANEL_ID'] ?>">
		<div class="product-item-detail-short-card-content-container">
			<table>
				<tr>
					<td rowspan="2" class="product-item-detail-short-card-image">
						<img src="" style="height: 65px;" data-entity="panel-picture">
					</td>
					<td class="product-item-detail-short-title-container" data-entity="panel-title">
						<span class="product-item-detail-short-title-text"><?= $name ?></span>
					</td>
					<td rowspan="2" class="product-item-detail-short-card-price">
						<?
						if ($arParams['SHOW_OLD_PRICE'] === 'Y')
						{
							?>
							<div class="product-item-detail-price-old" style="display: <?= ($showDiscount ? '' : 'none') ?>;"
								 data-entity="panel-old-price">
									 <?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?>
							</div>
							<?
						}
						?>
						<div class="product-item-detail-price-current" data-entity="panel-price">
							<?= $price['PRINT_RATIO_PRICE'] ?>
						</div>
					</td>
					<?
					if ($showAddBtn)
					{
						?>
						<td rowspan="2" class="product-item-detail-short-card-btn"
							style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;"
							data-entity="panel-add-button">
							<a class="btn <?= $showButtonClassName ?> product-item-detail-buy-button"
							   id="<?= $itemIds['ADD_BASKET_LINK'] ?>"
							   href="javascript:void(0);">
								<span><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></span>
							</a>
						</td>
						<?
					}

					if ($showBuyBtn)
					{
						?>
						<td rowspan="2" class="product-item-detail-short-card-btn"
							style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;"
							data-entity="panel-buy-button">
							<a class="btn <?= $buyButtonClassName ?> product-item-detail-buy-button" id="<?= $itemIds['BUY_LINK'] ?>"
							   href="javascript:void(0);">
								<span><?= $arParams['MESS_BTN_BUY'] ?></span>
							</a>
						</td>
						<?
					}
					?>
					<td rowspan="2" class="product-item-detail-short-card-btn"
						style="display: <?= (!$actualItem['CAN_BUY'] ? '' : 'none') ?>;"
						data-entity="panel-not-available-button">
						<a class="btn btn-link product-item-detail-buy-button" href="javascript:void(0)"
						   rel="nofollow">
							   <?= $arParams['MESS_NOT_AVAILABLE'] ?>
						</a>
					</td>
				</tr>
				<?
				if ($haveOffers)
				{
					?>
					<tr>
						<td>
							<div class="product-item-selected-scu-container" data-entity="panel-sku-container">
								<?
								$i = 0;

								foreach ($arResult['SKU_PROPS'] as $skuProperty)
								{
									if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
									{
										continue;
									}

									$propertyId = $skuProperty['ID'];

									foreach ($skuProperty['VALUES'] as $value)
									{
										$value['NAME'] = htmlspecialcharsbx($value['NAME']);
										if ($skuProperty['SHOW_MODE'] === 'PICT')
										{
											?>
											<div class="product-item-selected-scu product-item-selected-scu-color selected"
												 title="<?= $value['NAME'] ?>"
												 style="background-image: url('<?= $value['PICT']['SRC'] ?>'); display: none;"
												 data-sku-line="<?= $i ?>"
												 data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
												 data-onevalue="<?= $value['ID'] ?>">
											</div>
											<?
										} else
										{
											?>
											<div class="product-item-selected-scu product-item-selected-scu-text selected"
												 title="<?= $value['NAME'] ?>"
												 style="display: none;"
												 data-sku-line="<?= $i ?>"
												 data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
												 data-onevalue="<?= $value['ID'] ?>">
													 <?= $value['NAME'] ?>
											</div>
											<?
										}
									}

									$i++;
								}
								?>
							</div>
						</td>
					</tr>
					<?
				}
				?>
			</table>
		</div>
	</div>
	<!--Top tabs-->
	<div class="product-item-detail-tabs-container-fixed hidden-xs" id="<?= $itemIds['TABS_PANEL_ID'] ?>">
		<ul class="product-item-detail-tabs-list">
			<?
			if ($showDescription)
			{
				?>
				<li class="product-item-detail-tab active" data-entity="tab" data-value="description">
					<a href="javascript:void(0);" class="product-item-detail-tab-link">
						<span><?= $arParams['MESS_DESCRIPTION_TAB'] ?></span>
					</a>
				</li>
				<?
			}

			if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
			{
				?>
				<li class="product-item-detail-tab" data-entity="tab" data-value="properties">
					<a href="javascript:void(0);" class="product-item-detail-tab-link">
						<span><?= $arParams['MESS_PROPERTIES_TAB'] ?></span>
					</a>
				</li>
				<?
			}

			if ($arParams['USE_COMMENTS'] === 'Y')
			{
				?>
				<li class="product-item-detail-tab" data-entity="tab" data-value="comments">
					<a href="javascript:void(0);" class="product-item-detail-tab-link">
						<span><?= $arParams['MESS_COMMENTS_TAB'] ?></span>
					</a>
				</li>
				<?
			}
			?>
		</ul>
	</div>

	<meta itemprop="name" content="<?= $name ?>" />
	<meta itemprop="category" content="<?= $arResult['CATEGORY_PATH'] ?>" />
	<?
	if ($haveOffers)
	{
		foreach ($arResult['JS_OFFERS'] as $offer)
		{
			$currentOffersList = array();

			if (!empty($offer['TREE']) && is_array($offer['TREE']))
			{
				foreach ($offer['TREE'] as $propName => $skuId)
				{
					$propId = (int) substr($propName, 5);

					foreach ($skuProps as $prop)
					{
						if ($prop['ID'] == $propId)
						{
							foreach ($prop['VALUES'] as $propId => $propValue)
							{
								if ($propId == $skuId)
								{
									$currentOffersList[] = $propValue['NAME'];
									break;
								}
							}
						}
					}
				}
			}

			$offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
			?>
			<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="<?= htmlspecialcharsbx(implode('/', $currentOffersList)) ?>" />
				<meta itemprop="price" content="<?= $offerPrice['RATIO_PRICE'] ?>" />
				<meta itemprop="priceCurrency" content="<?= $offerPrice['CURRENCY'] ?>" />
				<link itemprop="availability" href="http://schema.org/<?= ($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock') ?>" />
			</span>
			<?
		}

		unset($offerPrice, $currentOffersList);
	} else
	{
		?>
		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?= $price['RATIO_PRICE'] ?>" />
			<meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>" />
			<link itemprop="availability" href="http://schema.org/<?= ($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock') ?>" />
		</span>
		<?
	}
	?>
</div>
<?
if ($haveOffers)
{
	$offerIds = array();
	$offerCodes = array();

	$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

	foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
	{
		$offerIds[] = (int) $jsOffer['ID'];
		$offerCodes[] = $jsOffer['CODE'];

		$fullOffer = $arResult['OFFERS'][$ind];
		$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

		$strAllProps = '';
		$strMainProps = '';
		$strPriceRangesRatio = '';
		$strPriceRanges = '';

		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($jsOffer['DISPLAY_PROPERTIES']))
			{
				foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
				{
					$current = '<dt>' . $property['NAME'] . '</dt><dd>' . (
							is_array($property['VALUE']) ? implode(' / ', $property['VALUE']) : $property['VALUE']
							) . '</dd>';
					$strAllProps .= $current;

					if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
					{
						$strMainProps .= $current;
					}
				}

				unset($current);
			}
		}

		if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
		{
			$strPriceRangesRatio = '(' . Loc::getMessage(
							'CT_BCE_CATALOG_RATIO_PRICE', array('#RATIO#' => ($useRatio ? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'] : '1'
						) . ' ' . $measureName)
					) . ')';

			foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
			{
				if ($range['HASH'] !== 'ZERO-INF')
				{
					$itemPrice = false;

					foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
					{
						if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
						{
							break;
						}
					}

					if ($itemPrice)
					{
						$strPriceRanges .= '<dt>' . Loc::getMessage(
										'CT_BCE_CATALOG_RANGE_FROM', array('#FROM#' => $range['SORT_FROM'] . ' ' . $measureName)
								) . ' ';

						if (is_infinite($range['SORT_TO']))
						{
							$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
						} else
						{
							$strPriceRanges .= Loc::getMessage(
											'CT_BCE_CATALOG_RANGE_TO', array('#TO#' => $range['SORT_TO'] . ' ' . $measureName)
							);
						}

						$strPriceRanges .= '</dt><dd>' . ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) . '</dd>';
					}
				}
			}

			unset($range, $itemPrice);
		}

		$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
		$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
		$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
		$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
	}

	$templateData['OFFER_IDS'] = $offerIds;
	$templateData['OFFER_CODES'] = $offerCodes;
	unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]) ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE'] : null
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => $itemIds,
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'NAME' => $arResult['~NAME'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $skuProps
	);
} else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
	{
		?>
		<div id="<?= $itemIds['BASKET_PROP_DIV'] ?>" style="display: none;">
			<?
			if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
			{
				foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
				{
					?>
					<input type="hidden" name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]" value="<?= htmlspecialcharsbx($propInfo['ID']) ?>">
					<?
					unset($arResult['PRODUCT_PROPERTIES'][$propId]);
				}
			}

			$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
			if (!$emptyProductProperties)
			{
				?>
				<table>
					<?
					foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
					{
						?>
						<tr>
							<td><?= $arResult['PROPERTIES'][$propId]['NAME'] ?></td>
							<td>
								<?
								if (
										$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L' && $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
								)
								{
									foreach ($propInfo['VALUES'] as $valueId => $value)
									{
										?>
										<label>
											<input type="radio" name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]"
												   value="<?= $valueId ?>" <?= ($valueId == $propInfo['SELECTED'] ? '"checked"' : '') ?>>
												   <?= $value ?>
										</label>
										<br>
										<?
									}
								} else
								{
									?>
									<select name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]">
										<?
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
											<option value="<?= $valueId ?>" <?= ($valueId == $propInfo['SELECTED'] ? '"selected"' : '') ?>>
												<?= $value ?>
											</option>
											<?
										}
										?>
									</select>
									<?
								}
								?>
							</td>
						</tr>
						<?
					}
					?>
				</table>
				<?
			}
			?>
		</div>
		<?
	}

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]) ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE'] : null
		),
		'VISUAL' => $itemIds,
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'PICT' => reset($arResult['MORE_PHOTO']),
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
			'ITEM_PRICES' => $arResult['ITEM_PRICES'],
			'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
			'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
			'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
			'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
			'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	unset($emptyProductProperties);
}

if ($arParams['DISPLAY_COMPARE'])
{
	$jsParams['COMPARE'] = array(
		'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
		'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
		'COMPARE_PATH' => $arParams['COMPARE_PATH']
	);
}
?>
<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?= GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2') ?>',
		TITLE_ERROR: '<?= GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
		TITLE_BASKET_PROPS: '<?= GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
		BASKET_UNKNOWN_ERROR: '<?= GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
		BTN_SEND_PROPS: '<?= GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS') ?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?= GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
		BTN_MESSAGE_CLOSE: '<?= GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE') ?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?= GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP') ?>',
		TITLE_SUCCESSFUL: '<?= GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK') ?>',
		COMPARE_MESSAGE_OK: '<?= GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
		COMPARE_UNKNOWN_ERROR: '<?= GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
		COMPARE_TITLE: '<?= GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?= GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
		PRODUCT_GIFT_LABEL: '<?= GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
		PRICE_TOTAL_PREFIX: '<?= GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX') ?>',
		RELATIVE_QUANTITY_MANY: '<?= CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY']) ?>',
		RELATIVE_QUANTITY_FEW: '<?= CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW']) ?>',
		SITE_ID: '<?= SITE_ID ?>'
	});

	var <?= $obName ?> = new JCCatalogElement(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
</script>
<?
unset($actualItem, $itemIds, $jsParams);
*/