<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach ($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
} else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach ($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	/*
	  if (!count($arResult['ORDERS']))
	  {
	  if ($_REQUEST["filter_history"] == 'Y')
	  {
	  if ($_REQUEST["show_canceled"] == 'Y')
	  {
	  ?>
	  <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
	  <?
	  } else
	  {
	  ?>
	  <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
	  <?
	  }
	  } else
	  {
	  ?>
	  <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
	  <?
	  }
	  }
	 * 
	 */
	?>
	<div class="shop-cart-table">
		<div class="table-content table-responsive">
			<?
			if ($_REQUEST["filter_history"] !== 'Y')
			{
				$paymentChangeData = array();
				$orderHeaderStatus = null;
				?>
				<table>
					<thead>
						<tr>
							<th class="order-number">Дата и номер заказа</th>
							<th class="order-status">Статус</th>
							<th class="order-price">Стоимость заказа</th>
							<th class="order-pay">Оплата</th>
							<th class="order-operations">Операции</th>
						</tr>
					</thead>
					<tbody>
						<?
						foreach ($arResult['ORDERS'] as $key => $order)
						{
							?>
							<tr>
								<td class="order-number">
									№<?= $order['ORDER']['ACCOUNT_NUMBER'] ?> от <?= $order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT']) ?>
								</td>
								<td class="order-status">
									<?= htmlspecialcharsbx($arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]['NAME']) ?>
								</td>
								<td class="order-price">
									<?= $order['ORDER']['FORMATED_PRICE'] ?>
								</td>
								<td class="order-pay">
									<?
									$showDelimeter = false;
									foreach ($order['PAYMENT'] as $payment)
									{
										if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
										{
											$paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
												"order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
												"payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
												"allow_inner" => $arParams['ALLOW_INNER'],
												"refresh_prices" => $arParams['REFRESH_PRICES'],
												"path_to_payment" => $arParams['PATH_TO_PAYMENT'],
												"only_inner_full" => $arParams['ONLY_INNER_FULL']
											);
										}
										?>
										<?
										$paymentSubTitle = Loc::getMessage('SPOL_TPL_BILL') . " " . Loc::getMessage('SPOL_TPL_NUMBER_SIGN') . htmlspecialcharsbx($payment['ACCOUNT_NUMBER']);
										if (isset($payment['DATE_BILL']))
										{
											$paymentSubTitle .= " " . Loc::getMessage('SPOL_TPL_FROM_DATE') . " " . $payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']);
										}
										$paymentSubTitle .= ",";
										echo $paymentSubTitle;
										?>
										<span class="sale-order-list-payment-title-element"><?= $payment['PAY_SYSTEM_NAME'] ?></span>
										<?
										if ($payment['PAID'] === 'Y')
										{
											?>
											<span class="sale-order-list-status-success"><?= Loc::getMessage('SPOL_TPL_PAID') ?></span>
											<?
										} elseif ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
										{
											?>
											<span class="sale-order-list-status-restricted"><?= Loc::getMessage('SPOL_TPL_RESTRICTED_PAID') ?></span>
											<?
										} else
										{
											?>
											<span class="sale-order-list-status-alert"><?= Loc::getMessage('SPOL_TPL_NOTPAID') ?></span>
											<?
										}
										?>
										</div>
										<div class="sale-order-list-payment-price">
											<span class="sale-order-list-payment-element"><?= Loc::getMessage('SPOL_TPL_SUM_TO_PAID') ?>:</span>

											<span class="sale-order-list-payment-number"><?= $payment['FORMATED_SUM'] ?></span>
										</div>
										<?
										if ($order['ORDER']['IS_ALLOW_PAY'] == 'N' && $payment['PAID'] !== 'Y')
										{
											?>
											<div class="sale-order-list-status-restricted-message-block">
												<span class="sale-order-list-status-restricted-message"><?= Loc::getMessage('SOPL_TPL_RESTRICTED_PAID_MESSAGE') ?></span>
											</div>
											<?
										}
										if ($payment['PAID'] === 'N' && $payment['IS_CASH'] !== 'Y')
										{
											if ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
											{
												?>
												<div class="col-md-3 col-sm-4 col-xs-12 sale-order-list-button-container">
													<a class="sale-order-list-button inactive-button">
														<?= Loc::getMessage('SPOL_TPL_PAY') ?>
													</a>
												</div>
												<?
											} elseif ($payment['NEW_WINDOW'] === 'Y')
											{
												?>
												<div class="col-md-3 col-sm-4 col-xs-12 sale-order-list-button-container">
													<a class="sale-order-list-button" target="_blank" href="<?= htmlspecialcharsbx($payment['PSA_ACTION_FILE']) ?>">
														<?= Loc::getMessage('SPOL_TPL_PAY') ?>
													</a>
												</div>
												<?
											} else
											{
												?>
												<div class="col-md-3 col-sm-4 col-xs-12 sale-order-list-button-container">
													<a class="sale-order-list-button ajax_reload" href="<?= htmlspecialcharsbx($payment['PSA_ACTION_FILE']) ?>">
														<?= Loc::getMessage('SPOL_TPL_PAY') ?>
													</a>
												</div>
												<?
											}
										}
									}
									?>
								</td>
								<td class="order-operations">
									<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>">Подробнее</a>
								</td>
							</tr>
							<?
						}
						?>
					</tbody>
				</table>
				<?
			} else
			{
				$orderHeaderStatus = null;

				if ($_REQUEST["show_canceled"] === 'Y' && count($arResult['ORDERS']))
				{
					?>
					<h1 class="sale-order-title">
						<?= Loc::getMessage('SPOL_TPL_ORDERS_CANCELED_HEADER') ?>
					</h1>
					<?
				}

				foreach ($arResult['ORDERS'] as $key => $order)
				{
					if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $_REQUEST["show_canceled"] !== 'Y')
					{
						$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
						?>
						<h1 class="sale-order-title">
							<?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?> &laquo;<?= htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME']) ?>&raquo;
						</h1>
						<?
					}
					?>
					<div class="col-md-12 col-sm-12 sale-order-list-container">
						<div class="row">
							<div class="col-md-12 col-sm-12 sale-order-list-accomplished-title-container">
								<div class="row">
									<div class="col-md-8 col-sm-12 sale-order-list-accomplished-title-container">
										<h2 class="sale-order-list-accomplished-title">
											<?= Loc::getMessage('SPOL_TPL_ORDER') ?>
											<?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
											<?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']) ?>
											<?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
											<?= $order['ORDER']['DATE_INSERT'] ?>,
											<?= count($order['BASKET_ITEMS']); ?>
											<?
											$count = substr(count($order['BASKET_ITEMS']), -1);
											if ($count == '1')
											{
												echo Loc::getMessage('SPOL_TPL_GOOD');
											} elseif ($count >= '2' || $count <= '4')
											{
												echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
											} else
											{
												echo Loc::getMessage('SPOL_TPL_GOODS');
											}
											?>
											<?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
											<?= $order['ORDER']['FORMATED_PRICE'] ?>
										</h2>
									</div>
									<div class="col-md-4 col-sm-12 sale-order-list-accomplished-date-container">
										<?
										if ($_REQUEST["show_canceled"] !== 'Y')
										{
											?>
											<span class="sale-order-list-accomplished-date">
												<?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED') ?>
											</span>
											<?
										} else
										{
											?>
											<span class="sale-order-list-accomplished-date canceled-order">
												<?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED') ?>
											</span>
											<?
										}
										?>
										<span class="sale-order-list-accomplished-date-number"><?= $order['ORDER']['DATE_STATUS_FORMATED'] ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 sale-order-list-inner-accomplished">
								<div class="row sale-order-list-inner-row">
									<div class="col-md-3 col-sm-12 sale-order-list-about-accomplished">
										<a class="sale-order-list-about-link" href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>">
											<?= Loc::getMessage('SPOL_TPL_MORE_ON_ORDER') ?>
										</a>
									</div>
									<div class="col-md-3 col-md-offset-6 col-sm-12 sale-order-list-repeat-accomplished">
										<a class="sale-order-list-repeat-link sale-order-link-accomplished" href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"]) ?>">
											<?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?
				}
			}
			?>
		</div>
	</div>
	<?
	/*
	  <div class="row col-md-12 col-sm-12">
	  <?
	  $nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
	  $clearFromLink = array("filter_history", "filter_status", "show_all", "show_canceled");

	  if ($nothing || $_REQUEST["filter_history"] == 'N')
	  {
	  ?>
	  <a class="sale-order-history-link" href="<?= $APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false) ?>">
	  <? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY") ?>
	  </a>
	  <?
	  }
	  if ($_REQUEST["filter_history"] == 'Y')
	  {
	  ?>
	  <a class="sale-order-history-link" href="<?= $APPLICATION->GetCurPageParam("", $clearFromLink, false) ?>">
	  <? echo Loc::getMessage("SPOL_TPL_CUR_ORDERS") ?>
	  </a>
	  <?
	  if ($_REQUEST["show_canceled"] == 'Y')
	  {
	  ?>
	  <a class="sale-order-history-link" href="<?= $APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false) ?>">
	  <? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY") ?>
	  </a>
	  <?
	  } else
	  {
	  ?>
	  <a class="sale-order-history-link" href="<?= $APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false) ?>">
	  <? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_CANCELED") ?>
	  </a>
	  <?
	  }
	  }
	  ?>
	  </div>
	  <?
	 * 
	 *//*
	  if (!count($arResult['ORDERS']))
	  {
	  ?>
	  <div class="row col-md-12 col-sm-12">
	  <a href="<?= htmlspecialcharsbx($arParams['PATH_TO_CATALOG']) ?>" class="sale-order-history-link">
	  <?= Loc::getMessage('SPOL_TPL_LINK_TO_CATALOG') ?>
	  </a>
	  </div>
	  <?
	  }
	 */

	echo $arResult["NAV_STRING"];
}
