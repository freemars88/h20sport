<?
$isExistsPresaleProducts = false;
foreach ($arResult['BASKET'] as $arBasketItem) {
	$arElement = getElementData($arBasketItem['PRODUCT_ID']);
	$arElCatalog = CIBlockElement::GetList(array(), array("ID" => $arBasketItem['PRODUCT_ID']), false, false, array("ID", "CATALOG_QUANTITY"))->Fetch();
	if ($arElement['PROPERTIES']['CML2_LINK']['VALUE'] > 0) {
		$arElement = getElementData($arElement['PROPERTIES']['CML2_LINK']['VALUE']);
	}
	if ($arElement['PROPERTIES']['IS_PRESALE']['VALUE'] == "Y" && $arElCatalog['CATALOG_QUANTITY'] <= 0) {
		$isExistsPresaleProducts = true;
		break;
	}
}
$arResult['IS_PRESALE_PRODUCTS'] = $isExistsPresaleProducts;

$arResult['ALLOW_PAYMENT'] = true;
if ($arResult['PAY_SYSTEM_ID'] == PAYSYSTEM_CARD_IN_SITE_ID && $isExistsPresaleProducts) {
	$arResult['ALLOW_PAYMENT'] = false;
}