<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

if (!empty($arResult['ERRORS']['FATAL'])) {
	foreach ($arResult['ERRORS']['FATAL'] as $error) {
		ShowError($error);
	}

	$component = $this->__component;

	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
} else {
	if (!empty($arResult['ERRORS']['NONFATAL'])) {
		foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
			ShowError($error);
		}
	}
	?>
	<div class="order-info bg-white text-center clearfix mb-30">
		<div class="single-order-info">
			<h4 class="title-1 text-uppercase text-light-black mb-0">Номер заказа</h4>
			<p class="text-uppercase text-light-black mb-0"><strong><?= $arResult["ACCOUNT_NUMBER"] ?></strong></p>
		</div>
		<div class="single-order-info">
			<h4 class="title-1 text-uppercase text-light-black mb-0">Дата заказа</h4>
			<p class="text-uppercase text-light-black mb-0"><strong><?= $arResult["DATE_INSERT_FORMATED"] ?></strong>
			</p>
		</div>
		<div class="single-order-info">
			<h4 class="title-1 text-uppercase text-light-black mb-0">Стоимость заказа</h4>
			<p class="text-uppercase text-light-black mb-0"><strong><?= $arResult["PRICE_FORMATED"] ?></strong></p>
		</div>
		<div class="single-order-info">
			<h4 class="title-1 text-uppercase text-light-black mb-0">Метод оплаты</h4>
			<p class="text-uppercase text-light-black mb-0">
				<?
				foreach ($arResult['PAYMENT'] as $payment) {
					?>
					<?= $payment['PAY_SYSTEM_NAME'] ?>
					<?
				}
				?>
			</p>
		</div>
	</div>

	<div class="shop-cart-table check-out-wrap">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-sm-12">
				<div class="our-order payment-details pr-20">
					<h4 class="title-1 title-border text-uppercase mb-30">Параметры заказа</h4>
					<?
					if (isset($arResult["ORDER_PROPS"])) {
						foreach ($arResult["ORDER_PROPS"] as $property) {
							?>
							<label class="b-order_paramname"><?= htmlspecialcharsbx($property['NAME']) ?>:</label>
							<span class="b-order_paramvalue">
								<?
								if ($property["TYPE"] == "Y/N") {
									echo Loc::getMessage('SPOD_' . ($property["VALUE"] == "Y" ? 'YES' : 'NO'));
								} else {
									if ($property['MULTIPLE'] == 'Y' && $property['TYPE'] !== 'FILE' && $property['TYPE'] !== 'LOCATION') {
										$propertyList = unserialize($property["VALUE"]);
										foreach ($propertyList as $propertyElement) {
											echo $propertyElement . '</br>';
										}
									} elseif ($property['TYPE'] == 'FILE') {
										echo $property["VALUE"];
									} else {
										echo htmlspecialcharsbx($property["VALUE"]);
									}
								}
								?>
							</span>
							<?
						}
					}
					?>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-sm-12 mt-xs-30">
				<div class="our-order payment-details pl-20">
					<h4 class="title-1 title-border text-uppercase mb-30">Товары заказа</h4>
					<table>
						<thead>
						<tr>
							<th><strong>Товары</strong></th>
							<th class="text-right"><strong>Итого</strong></th>
						</tr>
						</thead>
						<tbody>
						<?
						foreach ($arResult['BASKET'] as $arData) {
							?>
							<tr>
								<td><a href="<?= $arData["DETAIL_PAGE_URL"] ?>"><?= $arData["NAME"] ?></a>
									x <?= $arData["QUANTITY"] ?></td>
								<td class="text-right"><?= FormatCurrency($arData["BASE_PRICE"] * $arData["QUANTITY"], $arData['CURRENCY']) ?></td>
							</tr>
							<?
						}
						?>
						<tr>
							<td>Стоимость товаров</td>
							<td class="text-right"><?= $arResult['PRODUCT_SUM_FORMATED'] ?></td>
						</tr>
						<?
						if ($arResult['DISCOUNT_PRICE'] > 0) {
							?>
							<tr>
								<td>Скидка</td>
								<td class="text-right"><?= $arResult['DISCOUNT_PRICE_FORMATED'] ?></td>
							</tr>
							<?
						}
						?>
						<tr>
							<td>Доставка</td>
							<td class="text-right"><?= ($arResult['PRICE_DELIVERY'] > 0 ? $arResult['PRICE_DELIVERY_FORMATED'] : "Бесплатно") ?></td>
						</tr>
						<tr>
							<td>Стоимость заказа</td>
							<td class="text-right"><?= $arResult['PRICE_FORMATED'] ?></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="payment-method mt-60 pr-20">
					<h4 class="title-1 title-border text-uppercase mb-30">Способ доставки</h4>
					<?
					foreach ($arResult['SHIPMENT'] as $shipment) {
						?>
						<?= htmlspecialcharsbx($shipment["DELIVERY_NAME"]) ?>
						<?
					}
					?>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="our-order payment-details mt-60 pl-20">
					<h4 class="title-1 title-border text-uppercase mb-30">Способ оплаты</h4>
					<?
					foreach ($arResult['PAYMENT'] as $payment) {
						?>
						<?= $payment['PAY_SYSTEM_NAME'] ?>
						<?
						if ($arResult['ALLOW_PAYMENT']) {
							if ($payment['PAY_SYSTEM']["IS_CASH"] !== "Y") {
								if ($payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] === 'Y' && $arResult["IS_ALLOW_PAY"] !== "N") {
									?>
									<a href="<?= htmlspecialcharsbx($payment['PAY_SYSTEM']['PSA_ACTION_FILE']) ?>" target="_blank" class="button-one submit-button mt-15" data-text="Оформить заказ">Оплатить</a>
									<?
								}
							}
							if ($payment["PAID"] !== "Y" && $payment['PAY_SYSTEM']["IS_CASH"] !== "Y" && $payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] !== 'Y' && $arResult['CANCELED'] !== 'Y' && $arResult["IS_ALLOW_PAY"] !== "N") {
								?>
								<?= $payment['BUFFERED_OUTPUT'] ?>
								<?
							}
						}
					}
					?>
					<? /* <a href="javascript:void(0)" class="button-one submit-button mt-15 btn-order-save btn-disabled" data-text="Оформить заказ" onclick="submitForm('Y');
					  return false;">Оформить заказ</a> */ ?>
				</div>
			</div>
		</div>
	</div>
	<?
}
