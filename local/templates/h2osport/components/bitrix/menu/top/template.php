<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult))
{
	return;
}
?>

<ul class="nav navbar-nav">
	<?
	$iIndex = 0;
	foreach ($arResult['ITEMS'] as $arItem)
	{
		$iIndex++;
		if (empty($arItem['ITEMS']))
		{
			$arClass = array();
			if ($arItem['PARAMS']['IS_RED'] == "Y")
			{
				$arClass[] = "b-isred";
			}
			?>
			<li class="dropdown">
				<a href="<?= $arItem["LINK"] ?>" class="<?= implode(" ", $arClass) ?>"><?= $arItem["TEXT"] ?></a>
			</li>
			<?
		} else
		{
			$arClass = array();
			if ($arItem['PARAMS']['IS_RED'] == "Y")
			{
				$arClass[] = "b-isred";
			}
			?>
			<li class="dropdown dropdown-slide">
				<a href="<?= $arItem["LINK"] ?>" class="dropdown-toggle <?= implode(" ", $arClass) ?>" data-toggle="dropdown" data-hover="dropdown" data-delay="350" role="button" aria-haspopup="true" aria-expanded="false"><?= $arItem["TEXT"] ?> <span class="zmdi zmdi-chevron-down"></span></a>
				<div class="dropdown-menu">
					<div class="row">
						<?
						$isChildExists = false;
						//	myPrint($arItem['ITEMS']);
						foreach ($arItem['ITEMS'] as $arChildItem)
						{
							if (isset($arChildItem['ITEMS']) && count($arChildItem['ITEMS']) > 0)
							{
								$isChildExists = true;
							}
						}
						if ($isChildExists)
						{
							$cntRowChild = intval(count($arItem['ITEMS']));
							if ($cntRowChild <= 0)
							{
								$cntRowChild = 1;
							}
							if ($cntRowChild >= 3)
							{
								$cntRowChild = 3;
							}
							if (in_array($iIndex, $arResult['EXISTS_BANNERS']))
							{
								$cntRowChild++;
							}
							?>
							<div class="col-sm-12">
								<div class="row">
									<?
									$iCheckCol = 1;
									foreach ($arItem['ITEMS'] as $arChildItem)
									{
										?>
										<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
											<ul>
												<li class="dropdown-header"><a href="<?= $arChildItem["LINK"] ?>"><?= $arChildItem["TEXT"] ?></a></li>
												<li role="separator" class="divider"></li>
												<?
												foreach ($arChildItem['ITEMS'] as $arChildItem2)
												{
													?>
													<li>
														<a href="<?= $arChildItem2["LINK"] ?>"><?= $arChildItem2["TEXT"] ?></a>
													</li>
													<?
												}
												?>
												<li>
													<a href="<?= $arChildItem["LINK"] ?>">Все разделы...</a>
												</li>
											</ul>
										</div>
										<?
										$iCheckCol++;
										if ((($iCheckCol - 1) % 6) == 0)
										{
											?>
											<div class="clearfix visible-lg"></div>	
											<?
										}
										if ((($iCheckCol - 1) % 4) == 0)
										{
											?>
											<div class="clearfix visible-md"></div>	
											<?
										}
										if ((($iCheckCol - 1) % 3) == 0)
										{
											?>
											<div class="clearfix visible-sm"></div>	
											<?
										}
										/* if ($iCheckCol >= 4)
										  {
										  ?>
										  <div class="clearfix"></div>
										  <?
										  $iCheckCol = 1;
										  }
										 * 
										 */
									}
									?>
								</div>
							</div>
						<? }
						?>
					</div>
				</div>
			</li>
			<?
		}
	}
	?>
</ul>