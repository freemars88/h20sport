<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @global CMain $APPLICATION
 */
global $APPLICATION;

//delayed function must return a string
unset($arResult[0]);
$arResult = array_values($arResult);
if (empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<ul itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize - 2 && $arResult[$index + 1]["LINK"] <> "" ? ' itemref="breadcrumb_' . ($index + 1) . '"' : '');
	$child = ($index > 0 ? ' itemprop="child"' : '');

	if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1)
	{
		$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $arResult[$index]["LINK"] . '" itemprop="item" title="' . $title . '"><span itemprop="item">' . $title . '</span></a></li>';
	} else
	{
		$strReturn .= '<li title="' . $title . '">' . $title . '</li>';
	}
}

$strReturn .= '</ul>';

return $strReturn;
