<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<form action="/search/">
	<input type="text" placeholder="Поиск..." name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>" />
	<button type="submit" name="search">
		<i class="zmdi zmdi-search"></i>
	</button>
</form>