<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */
?>
<div class="login-area  pt-80 pb-80">
	<div class="container">
		<form class="text-left clearfix" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
					<div class="customer-login text-left">
						<h4 class="title-1 title-border text-uppercase mb-30">Вход для зарегистрированных пользователей</h4>
						<?
						if (!empty($arParams["~AUTH_RESULT"]))
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert alert-danger"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						if ($arResult['ERROR_MESSAGE'] <> '')
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
							?>
							<div class="alert alert-danger"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						?>
						<input type="hidden" name="AUTH_FORM" value="Y" />
						<input type="hidden" name="TYPE" value="AUTH" />
						<?
						if (strlen($arResult["BACKURL"]) > 0)
						{
							?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
							<?
						}
						foreach ($arResult["POST"] as $key => $value)
						{
							?>
							<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
							<?
						}
						?>
						<input type="text" name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>" placeholder="Логин" id="USER_LOGIN" />
						<input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="Пароль" id="USER_PASSWORD" />
						<?
						if ($arResult["STORE_PASSWORD"] == "Y")
						{
							?>
							<p class="mb-0">
								<label><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /> Запомнить меня</label>
							</p>
							<?
						}
						if ($arResult["CAPTCHA_CODE"])
						{
							?>
							<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
							<input type="text" name="captcha_word" maxlength="50" value="" />
							<?
						}
						?>
						<input type="hidden" name="Login" value="yes" />
						<button type="submit" data-text="Вход" class="button-one submit-button mt-15">Вход</button>
						<p class="mt-20">Забыли пароль ?<noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow" class="text-gray"> Восстановить</a></noindex></p>
						<?
						if ($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y")
						{
							?>
							<p class="mt-20">Впервые на сайте? 
							<noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow" class="text-gray">Зарегистрироваться</a></noindex>
							</p>
							<?
						}
						?>
					</div>					
				</div>
			</div>
		</form>
	</div>
</div>
