<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!doctype html>
<html class="no-js" lang="ru">
	<head>
		<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
	</head>
	<body id="body">
		<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
		<div class="wrapper bg-dark-white">
			<? CWebavkTmplProTools::IncludeBlockFromDir("header.top"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("content.search"); ?>
			<div class="heading-banner-area overlay-bg" style="background-image:url(<? $APPLICATION->ShowProperty("headingbanner") ?>);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="heading-banner">
								<div class="heading-banner-title">
									<h2></h2>
									<?/*<h2><? $APPLICATION->ShowTitle(false) ?></h2>*/?>
								</div>
								<div class="breadcumbs pb-15">
									<?
									$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
										"START_FROM" => "0",
										"PATH" => "",
										"SITE_ID" => "-"
											), false, Array('HIDE_ICONS' => 'Y')
									);
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.category"); ?>
			<?
			if ($layoutArea == "header")
				goto TEMPLATE_END;
			FOOTER:
			?>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.subscribe"); ?>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.instagram"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("quick.view"); ?>
		</div>
	</body>
</html>
<?
TEMPLATE_END:
