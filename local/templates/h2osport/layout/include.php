<?

CModule::IncludeModule("webavk.tmplpro");
global $layoutArea;
if ($layoutArea == "header")
{
	include(__DIR__ . "/../lib/include.php");
	include(__DIR__ . "/../css/include.php");
	include(__DIR__ . "/../js/include.php");
}
$arPath = pathinfo($APPLICATION->GetCurPage(true));
/* if (defined("IS_PERSONAL_PAGE") && IS_PERSONAL_PAGE===true)
  {
  include(__DIR__ . "/personal/include.php");
  } else */
if ($APPLICATION->GetCurPage(true) == "/index.php" || (defined("MAINPAGE_TEMPLATE") && MAINPAGE_TEMPLATE === true))
{
	include(__DIR__ . "/mainpage/include.php");
} elseif (defined("ERROR_404") && ERROR_404 == "Y")
{
	include(__DIR__ . "/.default/include.php");
} else/* if (strpos($APPLICATION->GetCurPage(true), "/about/") === 0 || strpos($APPLICATION->GetCurPage(true), "/delivery/") === 0 || strpos($APPLICATION->GetCurPage(true), "/contacts/") === 0)
  {
  include(__DIR__ . "/text/include.php");
  } else */ if (strpos($APPLICATION->GetCurPage(true), "/ajax/") === 0 || $_REQUEST['ajaxRequest'] == "Y" || $_SERVER['HTTP_X_AJAX'] == "Y")
{
	include(__DIR__ . "/ajax/include.php");
} elseif (strpos($APPLICATION->GetCurPage(true), "/catalog/") === 0)
{
	if ($arPath['basename'] == "index.php")
	{
		include(__DIR__ . "/catalog/include.php");
	} else
	{
		include(__DIR__ . "/catalog.element/include.php");
	}
} else
{
	include(__DIR__ . "/.default/include.php");
}
