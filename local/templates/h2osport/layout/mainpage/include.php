<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!doctype html>
<html class="no-js" lang="ru">
	<head>
		<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
	</head>
	<body>
		<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
		<div class="wrapper">
			<? CWebavkTmplProTools::IncludeBlockFromDir("header.top"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
			<section class="slider-banner-area clearfix">
				<? CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.social"); ?>
				<div class="banner-left floatleft">
					<? CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.banner.left"); ?>
				</div>
				<div class="slider-right floatleft">
					<? CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.slider2"); ?>
				</div>
				<? CWebavkTmplProTools::IncludeBlockFromDir("content.sidebar.right"); ?>
			</section>
			<? CWebavkTmplProTools::IncludeBlockFromDir("content.search"); ?>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.category"); ?>
			<?
			if ($layoutArea == "header")
				goto TEMPLATE_END;
			FOOTER:
			?>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.subscribe"); ?>
			<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.instagram"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
			<? CWebavkTmplProTools::IncludeBlockFromDir("quick.view"); ?>
		</div>
	</body>
</html>
<?
TEMPLATE_END:
