<div class="sidebar-account hidden-xs">
	<div class="table">
		<div class="table-cell">
			<ul>
				<li><a class="search-open" href="/search/" title="Поиск"><i class="zmdi zmdi-search"></i></a></li>
				<?
				if (!$USER->IsAuthorized())
				{
					?>
					<li><a href="/auth/" title="Авторизация и регистрация"><i class="zmdi zmdi-lock"></i></a>
						<?
						$APPLICATION->IncludeComponent(
								"bitrix:system.auth.form", "sidebar.right", Array(
							"FORGOT_PASSWORD_URL" => "/auth/",
							"PROFILE_URL" => "",
							"REGISTER_URL" => "/auth/",
							"SHOW_ERRORS" => "N"
								)
						);
						?>
					</li>
					<?
				}
				?>
				<li><a href="/personal/" title="Личный кабинет"><i class="zmdi zmdi-account"></i></a></li>
				<li><a href="/personal/wishlist/" title="Список желаний"><i class="zmdi zmdi-favorite"></i></a></li>
			</ul>
		</div>
	</div>
</div>