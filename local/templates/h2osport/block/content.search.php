<div class="sidebar-search animated slideOutUp" style="display:none;">
	<div class="table">
		<div class="table-cell">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 p-0">
						<div class="search-form-wrap">
							<button class="close-search"><i class="zmdi zmdi-close"></i></button>
							<form action="/search/">
								<input type="text" placeholder="Введите запрос..." name="q" />
								<button class="search-button" type="submit" name="search">
									<i class="zmdi zmdi-search"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>