<div class="subscribe-area pt-80">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="subscribe">
					<form action="#">
						<input type="text" placeholder="Enter your email address"/>
						<button class="submit-button submit-btn-2 button-one" data-text="subscribe" type="submit" >subscribe</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="footer-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Контакты</h3>
						<? CWebavkTmplProTools::IncludeBlockFromDir("footer.contacts", SITE_TEMPLATE_PATH . "/include/"); ?>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Мой кабинет</h3>
						<?
						$APPLICATION->IncludeComponent(
								"bitrix:menu", "bottom", Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_USE_GROUPS" => "N",
							"ROOT_MENU_TYPE" => "bottom1",
							"USE_EXT" => "Y"
								)
						);
						?>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Магазин</h3>
						<?
						$APPLICATION->IncludeComponent(
								"bitrix:menu", "bottom", Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_USE_GROUPS" => "N",
							"ROOT_MENU_TYPE" => "bottom2",
							"USE_EXT" => "Y"
								)
						);
						?>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 hidden-sm col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Рекомендованные товары</h3>
						<div class="footer-product">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<div class="footer-thumb">
										<a href="#"><img src="img/footer/1.jpg" alt="" /></a>
										<div class="footer-thumb-info">
											<p><a href="#">Furniture Product<br>Name</a></p>
											<h4 class="price-3">$ 60.00</h4>
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-xs-12">
									<div class="footer-thumb">
										<a href="#"><img src="img/footer/1.jpg" alt="" /></a>
										<div class="footer-thumb-info">
											<p><a href="#">Furniture Product<br>Name</a></p>
											<h4 class="price-3">$ 60.00</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div class="copyright">
						<p class="mb-0">&copy; <a href="/" target="_blank">H2OSport </a> <?= date("Y") ?>. Все права защищены.</p>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="payment  text-right">
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/1.png" alt="" /></a>
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/2.png" alt="" /></a>
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/3.png" alt="" /></a>
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/4.png" alt="" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>