<?
if (IsShowAdminSitePanel())
{
	?>
	<div class="front-panel<?= ($APPLICATION->get_cookie("FRONT_PANEL") == "OPEN" ? " open" : "") ?>">
		<div class="row front-panel-name">
			<?= $_SESSION['SESS_AUTH']['NAME'] ?>
		</div>
		<? /*
		  <div class="row" style="padding: 5px 0;">
		  Заказы: <?
		  $cnt = getInSiteOrdersCnt();
		  if ($cnt <= 0) {
		  echo '0';
		  } else {
		  ?>
		  <a href="/front/utl/site-orders/" class="btn btn-xs btn-info" title="Заказы в статусе Оставлен на сайте"><?= $cnt ?></a>
		  <?
		  }
		  ?>
		  </div>
		 * 
		 */ ?>
		<div class="row browser-nav">
			<a href="javascript:history.go(-1)" title="Назад" class="glyphicon glyphicon-arrow-left"></a>
			<a href="javascript:window.location.replace(window.location)" title="Обновить" class="glyphicon glyphicon-refresh"></a>
			<? /* <a href="javascript:history.go(1)" title="Вперед" class="glyphicon glyphicon-arrow-right"></a> */ ?>
		</div>
		<div class="row" id="change-filter-descriptions">
			<? $APPLICATION->ShowProperty("LINK_TO_ADMIN_CHANGE_FILTER_DESCRIPTIONS"); ?>
		</div>
		<div class="row" id="change-filter-url">
			<? $APPLICATION->ShowProperty("LINK_TO_ADMIN_CHANGE_FILTER_URL"); ?>
		</div>
		<div class="row text-right">
			<a href="javascript:void(0)" class="btn btn-default btn-xs" title="Показать/скрыть дополнительные элементы управления" id="front-panel-show"><?= ($APPLICATION->get_cookie("FRONT_PANEL") == "OPEN" ? "Скрыть" : "Ещё") ?></a>
		</div>
		<?/*
		<div class="row ext-info">
			<?
			if (in_array(strtolower($_SERVER['HTTP_HOST']), array("francesco.ru", "www.francesco.ru")))
			{
				?>
				<ul class="nav nav-admin-site">
					<li><a href="http://dev.francesco-donni.com<?= $APPLICATION->GetCurPageParam() ?>">dev.francesco-donni.com</a></li>
				</ul>
				<?
			} else
			{
				?>
				<ul class="nav nav-admin-site">
					<li><a href="http://www.francesco.ru<?= $APPLICATION->GetCurPageParam() ?>">www.francesco.ru</a></li>
				</ul>
				<?
			}
			?>
			<?
			$APPLICATION->IncludeComponent(
					"bitrix:menu", "admin-site", array(
				"ROOT_MENU_TYPE" => "adminsite",
				"MENU_CACHE_TYPE" => "Y",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"USE_EXT" => "Y",
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N"
					), false
			);
			?>
		</div>
		 * 
		 */?>
	</div>
	<?
}