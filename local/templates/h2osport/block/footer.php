<footer>
	<div class="footer-area footer-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Контакты</h3>
						<? CWebavkTmplProTools::IncludeBlockFromDir("footer.contacts", SITE_TEMPLATE_PATH . "/include/"); ?>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Мой кабинет</h3>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:menu", "bottom", Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_USE_GROUPS" => "N",
								"ROOT_MENU_TYPE" => "bottom1",
								"USE_EXT" => "Y",
							)
						);
						?>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 hidden-sm col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Магазин</h3>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:menu", "bottom", Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_USE_GROUPS" => "N",
								"ROOT_MENU_TYPE" => "bottom2",
								"USE_EXT" => "Y",
							)
						);
						?>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="single-footer">
						<h3 class="footer-title  title-border">Подписаться на новости</h3>
						<div class="footer-subscribe">
							<?
							$APPLICATION->IncludeComponent(
								"bitrix:sender.subscribe", "", Array(
									"AJAX_MODE" => "Y",
									"AJAX_OPTION_ADDITIONAL" => "",
									"AJAX_OPTION_HISTORY" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"CACHE_TIME" => "3600",
									"CACHE_TYPE" => "A",
									"CONFIRMATION" => "Y",
									"HIDE_MAILINGS" => "Y",
									"SET_TITLE" => "N",
									"SHOW_HIDDEN" => "N",
									"USER_CONSENT" => "N",
									"USER_CONSENT_ID" => "0",
									"USER_CONSENT_IS_CHECKED" => "Y",
									"USER_CONSENT_IS_LOADED" => "N",
									"USE_PERSONALIZATION" => "N",
								)
							);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright-area copyright-2">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div class="copyright">
						<p class="mb-0">&copy; <a href="/" target="_blank">H2OSport </a> <?= date("Y") ?>. Все права
							защищены.</p>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="payment  text-right">
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/1.png" alt=""/></a>
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/2.png" alt=""/></a>
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/3.png" alt=""/></a>
						<a href="javascript:void(0)"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment/4.png" alt=""/></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--
<p>Сайт разработан <a href="https://www.ammina.ru/">Веб-студия Ammina</a></p>
-->
<? /*
  <script type='text/javascript'>
  (function(){ var widget_id = 'xFYdvp8Pti';
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
 */ ?>
<?
if (IS_DEV_SITE !== true) {
	/*
	?>
	<script src="//unpkg.com/@textback/notification-widget@latest/build/index.js"></script>
	<tb-notification-widget widget-id="b4a5e72d-eb37-0ecb-e78e-0166c531b574">
	</tb-notification-widget>
	<script type="text/javascript" encoding="utf-8">
		var tbEmbedArgs = tbEmbedArgs || [];
		(function () {
			var u = "https://widget.textback.io/widget";
			_tbEmbedArgs.push(["widgetId", "119dfce3-4976-4026-9372-a23ccc8c3bb0"]);
			_tbEmbedArgs.push(["baseUrl", u]);

			var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
			g.type = "text/javascript";
			g.charset = "utf-8";
			g.defer = true;
			g.async = true;
			g.src = u + "/widget.js";
			s.parentNode.insertBefore(g, s);
		})();

	</script>
<?*/
	//ob_start();
	?>
	<!-- Facebook Pixel Code -->
	<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq)
				return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq)
				f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '266043570816392');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=266043570816392&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->
	<?/*
	$cont = ob_get_contents();
	ob_end_clean();
	?>
	<script type="text/javascript">
		$(window).load(function () {
			window.setTimeout(function(){
				var arCont =<?=CUtil::PhpToJSObject(array("text" => $cont))?>;
				$("body").append($(arCont.text));
			}, 1500);
		});
	</script>

	<?*/
}
?>