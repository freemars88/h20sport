<div class="mobile-header-top hidden-lg hidden-md hidden-sm">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- header-search-mobile start -->
				<div class="header-search-mobile">
					<div class="table">
						<div class="table-cell">
							<ul>
								<li><a class="search-open" href="/search/"><i class="zmdi zmdi-search"></i></a></li>
								<li><a href="/auth/"><i class="zmdi zmdi-lock"></i></a></li>
								<li><a href="/personal/"><i class="zmdi zmdi-account"></i></a></li>
								<li><a href="/personal/wishlist/"><i class="zmdi zmdi-favorite"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- header-search-mobile start -->
			</div>
		</div>
	</div>
</div>