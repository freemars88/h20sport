<noscript><img src="https://vk.com/rtrg?p=VK-RTRG-288931-23aoJ" style="position:fixed; left:-999px;" alt=""/></noscript>
<!-- HEADER-AREA START -->
<header id="sticky-menu" class="header header-2">
	<div class="header-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-xs-4">
					<div class="phones text-left">
						<?
						$APPLICATION->IncludeComponent(
								"bitrix:main.include", "", Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => "/include/header.phones.php"
								)
						);
						?>
					</div>
				</div>
				<div class="col-sm-4 col-xs-6">
					<div class="logo text-center">
						<a href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="" /></a>
					</div>
				</div>
				<div class="col-sm-4 col-xs-2">
					<div class="mini-cart text-right">
						<ul>
							<li id="header-cart-content">
								<?
								$APPLICATION->IncludeComponent(
										"bitrix:sale.basket.basket.line", "header", Array(
									"HIDE_ON_BASKET_PAGES" => "N",
									"PATH_TO_AUTHORIZE" => "",
									"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
									"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
									"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
									"PATH_TO_PROFILE" => SITE_DIR . "personal/",
									"PATH_TO_REGISTER" => SITE_DIR . "login/",
									"POSITION_FIXED" => "N",
									"SHOW_AUTHOR" => "N",
									"SHOW_DELAY" => "N",
									"SHOW_EMPTY_VALUES" => "Y",
									"SHOW_IMAGE" => "Y",
									"SHOW_NOTAVAIL" => "N",
									"SHOW_NUM_PRODUCTS" => "Y",
									"SHOW_PERSONAL_LINK" => "N",
									"SHOW_PRICE" => "Y",
									"SHOW_PRODUCTS" => "Y",
									"SHOW_SUMMARY" => "Y",
									"SHOW_TOTAL_PRICE" => "Y"
										)
								);
								?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<? /*
	  <div class="menu-toggle menu-toggle-2 hamburger hamburger--emphatic hidden-xs">
	  <div class="hamburger-box">
	  <div class="hamburger-inner"></div>
	  </div>
	  </div>
	  <div class="main-menu hidden-xs">
	  <nav>
	  <ul>
	  <li>
	  <a href="/">Главная</a>
	  </li>
	  <?
	  $APPLICATION->IncludeComponent(
	  "bitrix:menu", "top", Array(
	  "ALLOW_MULTI_SELECT" => "N",
	  "CHILD_MENU_TYPE" => "left",
	  "DELAY" => "N",
	  "MAX_LEVEL" => "2",
	  "MENU_CACHE_GET_VARS" => array(""),
	  "MENU_CACHE_TIME" => "3600",
	  "MENU_CACHE_TYPE" => "A",
	  "MENU_CACHE_USE_GROUPS" => "N",
	  "ROOT_MENU_TYPE" => "left",
	  "USE_EXT" => "Y",
	  "CNT_ITEMS_COL" => 5
	  )
	  );
	  /*
	  ?>
	  <li>
	  <a href="shop.html">products</a>
	  <div class="mega-menu menu-scroll">
	  <div class="table">
	  <div class="table-cell">
	  <div class="half-width">
	  <ul>
	  <li class="menu-title">best brands</li>
	  <li><a href="#">henning koppel</a></li>
	  <li><a href="#">jehs + Laub</a></li>
	  <li><a href="#">vicke lindstrand</a></li>
	  <li><a href="#">don chadwick</a></li>
	  <li><a href="#">akiko kuwahata</a></li>
	  <li><a href="#">barbro berlin</a></li>
	  <li><a href="#">cecilia hall</a></li>
	  <li><a href="#">don chadwick</a></li>
	  </ul>
	  </div>
	  <div class="half-width">
	  <ul>
	  <li class="menu-title">popular brands</li>
	  <li><a href="#">akiko kuwahata</a></li>
	  <li><a href="#">barbro berlin</a></li>
	  <li><a href="#">cecilia hall</a></li>
	  <li><a href="#">don chadwick</a></li>
	  <li><a href="#">henning koppel</a></li>
	  <li><a href="#">jehs + Laub</a></li>
	  <li><a href="#">vicke lindstrand</a></li>
	  <li><a href="#">don chadwick</a></li>
	  </ul>
	  </div>
	  <div class="full-width">
	  <div class="mega-menu-img">
	  <a href="single-product.html"><img src="<?= SITE_TEMPLATE_PATH ?>/img/megamenu/1.jpg" alt="" /></a>
	  </div>
	  </div>
	  <div class="pb-80"></div>
	  </div>
	  </div>
	  </div>
	  </li>
	  <li><a href="#">Shortcodes</a>
	  <div class="sub-menu menu-scroll">
	  <ul>
	  <li class="menu-title">Shortcodes</li>
	  <li><a href="elements-accordions.html">Accordion</a></li>
	  <li><a href="elements-toggles.html">Toggles</a></li>
	  <li><a href="elements-tab.html">Tab</a></li>
	  <li><a href="elements-product-tab.html">Product Tab</a></li>
	  <li><a href="elements-product-tab-2.html">Product Tab 2</a></li>
	  <li><a href="elements-carousel.html">product carousel</a></li>
	  <li><a href="elements-carousel-2.html">product carousel 2</a></li>
	  <li><a href="elements-featured-product.html">Featured Product</a></li>
	  <li><a href="elements-featured-product-2.html">Featured Product 2</a></li>
	  <li><a href="elements-button.html">Button</a></li>
	  <li><a href="elements-table.html">Table</a></li>
	  <li><a href="elements-progress-bars.html">Progress Bar</a></li>
	  <li><a href="elements-blog.html">Blog</a></li>
	  <li><a href="elements-blog-2.html">Blog - 2</a></li>
	  <li><a href="elements-team.html">Team</a></li>
	  <li><a href="elements-footer.html">Footer</a></li>
	  <li><a href="elements-footer-2.html">Footer 2</a></li>
	  <li><a href="elements-map.html">Map</a></li>
	  </ul>
	  </div>
	  </li>
	  <li><a href="shop-sidebar.html">accesories</a></li>
	  <li><a href="shop-list.html">lookbook</a></li>
	  <li><a href="blog.html">blog</a></li>
	  <li><a href="#">pages</a>
	  <div class="sub-menu menu-scroll">
	  <ul>
	  <li class="menu-title">Page's</li>
	  <li><a href="shop.html">Shop</a></li>
	  <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
	  <li><a href="shop-grid-right-sidebar.html">Shop Right Sidebar</a></li>
	  <li><a href="shop-list.html">Shop List</a></li>
	  <li><a href="shop-list-right-sidebar.html">Shop List right sidebar</a></li>
	  <li><a href="single-product.html">Single Product</a></li>
	  <li><a href="single-product-sidebar.html">Single Product Sidebar</a></li>
	  <li><a href="cart.html">Shopping Cart</a></li>
	  <li><a href="wishlist.html">Wishlist</a></li>
	  <li><a href="checkout.html">Checkout</a></li>
	  <li><a href="order.html">Order</a></li>
	  <li><a href="login.html">login / Registration</a></li>
	  <li><a href="my-account.html">My Account</a></li>
	  <li><a href="404.html">404</a></li>
	  <li><a href="blog.html">Blog</a></li>
	  <li><a href="single-blog.html">Single Blog</a></li>
	  <li><a href="single-blog-sidebar.html">Single Blog Sidebar</a></li>
	  <li><a href="about.html">About Us</a></li>
	  <li><a href="contact.html">Contact</a></li>
	  </ul>
	  </div>
	  </li>
	  <li><a href="about.html">about us</a></li>
	  <li><a href="contact.html">contact</a></li>
	 *//* ?>
	  </ul>
	  </nav>
	  </div>
	 * 
	 */ ?>
</header>
<section class="menu">
	<nav class="navbar navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<h2 class="menu-title">Главное меню</h2>
			</div>
			<div id="navbar" class="navbar-collapse collapse text-center">
				<?
				$APPLICATION->IncludeComponent(
						"bitrix:menu", "top", Array(
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "left",
					"DELAY" => "N",
					"MAX_LEVEL" => "2",
					"MENU_CACHE_GET_VARS" => array(""),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_USE_GROUPS" => "N",
					"ROOT_MENU_TYPE" => "top",
					"USE_EXT" => "Y",
					"CNT_ITEMS_COL" => 5
						)
				);
				?>
			</div>
		</div>
	</nav>
</section>
<? /*
  <div class="mobile-menu-area">
  <div class="container-fluid">
  <div class="row">
  <div class="col-xs-12 hidden-lg hidden-md hidden-sm">
  <div class="mobile-menu">
  <nav id="dropdown">
  <ul>
  <li><a href="/">Главная</a>
  <ul>
  <li><a href="index.html">Home Version 1</a></li>
  <li><a href="index-2.html">Home Version 2</a></li>
  </ul>
  </li>
  <li><a href="shop.html">products</a></li>
  <li><a href="shop-sidebar.html">accesories</a></li>
  <li><a href="shop-list.html">lookbook</a></li>
  <li><a href="blog.html">blog</a></li>
  <li><a href="#">pages</a>
  <ul>
  <li><a href="shop.html">Shop</a></li>
  <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
  <li><a href="shop-list.html">Shop List</a></li>
  <li><a href="single-product.html">Single Product</a></li>
  <li><a href="single-product-sidebar.html">Single Product Sidebar</a></li>
  <li><a href="cart.html">Shopping Cart</a></li>
  <li><a href="wishlist.html">Wishlist</a></li>
  <li><a href="checkout.html">Checkout</a></li>
  <li><a href="order.html">Order</a></li>
  <li><a href="login.html">login / Registration</a></li>
  <li><a href="my-account.html">My Account</a></li>
  <li><a href="404.html">404</a></li>
  <li><a href="blog.html">Blog</a></li>
  <li><a href="single-blog.html">Single Blog</a></li>
  <li><a href="single-blog-sidebar.html">Single Blog Sidebar</a></li>
  <li><a href="about.html">About Us</a></li>
  <li><a href="contact.html">Contact</a></li>
  </ul>
  </li>
  <li><a href="about.html">about us</a></li>
  <li><a href="contact.html">contact</a></li>
  </ul>
  </nav>
  </div>
  </div>
  </div>
  </div>
  </div>
  <!-- Mobile-menu end -->
 */ ?>
<? /*
  <section class="top-header">
  <div class="container">
  <div class="row">
  <div class="col-md-4 col-sm-12">
  <div class="contact-number">
  <i class="tf-ion-ios-telephone"></i>
  <?
  $arRegion = json_decode($APPLICATION->get_cookie("REASPEKT_GEOBASE"));
  ?>
  <span><?
  if ($arRegion->CITY == "Москва")
  {
  ?>
  +7 (495) 215-04-49
  <?
  } else
  {
  ?>
  +7 (800) 707-58-27
  <?
  }
  ?></span>
  </div>
  <div class="header-location">
  <?
  $APPLICATION->IncludeComponent(
  "reaspekt:reaspekt.geoip", "", Array(
  "CHANGE_CITY_MANUAL" => "Y"
  )
  );
  ?>
  </div>
  </div>
  <div class="col-md-4 col-sm-12">
  <div class="logo text-center">
  <a href="/">
  <img src="/local/templates/francesco/img/logo_fd.png" alt="Francesco Donni" />
  </a>
  </div>
  </div>
  <div class="col-md-4 col-sm-12">
  <ul class="top-menu text-right list-inline">
  <li class="dropdown cart-nav dropdown-slide" id="header-cart-content">
  <?
  $APPLICATION->IncludeComponent(
  "bitrix:sale.basket.basket.line", "header", Array(
  "HIDE_ON_BASKET_PAGES" => "N",
  "PATH_TO_AUTHORIZE" => "",
  "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
  "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
  "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
  "PATH_TO_PROFILE" => SITE_DIR . "personal/",
  "PATH_TO_REGISTER" => SITE_DIR . "login/",
  "POSITION_FIXED" => "N",
  "SHOW_AUTHOR" => "N",
  "SHOW_DELAY" => "N",
  "SHOW_EMPTY_VALUES" => "Y",
  "SHOW_IMAGE" => "Y",
  "SHOW_NOTAVAIL" => "N",
  "SHOW_NUM_PRODUCTS" => "Y",
  "SHOW_PERSONAL_LINK" => "N",
  "SHOW_PRICE" => "Y",
  "SHOW_PRODUCTS" => "Y",
  "SHOW_SUMMARY" => "Y",
  "SHOW_TOTAL_PRICE" => "Y"
  )
  );
  ?>
  </li>
  <li class="dropdown fav-nav dropdown-slide header-hide-title" id="header-fav-content">
  <?
  $APPLICATION->IncludeComponent(
  "webavk:blank.component", "header.favorites", Array(
  "CACHE_TIME" => "3600",
  "CACHE_TYPE" => "N"
  )
  );
  ?>
  </li>
  <li class="dropdown search dropdown-slide header-hide-title">
  <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="tf-ion-ios-search-strong"></i> <span>Поиск</span></a>
  <ul class="dropdown-menu search-dropdown">
  <li>
  <?
  $APPLICATION->IncludeComponent(
  "bitrix:search.form", "", Array(
  "PAGE" => "/search/index.php",
  "USE_SUGGEST" => "N"
  )
  );
  ?>
  </li>
  </ul>
  </li>
  <li class="dropdown login-nav dropdown-slide header-hide-title" id="header-login-content">
  <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="tf-ion-android-person"></i><span>Вход</span></a>
  <div class="dropdown-menu login-dropdown">
  <?
  $APPLICATION->IncludeComponent(
  "bitrix:system.auth.form", "", Array(
  "FORGOT_PASSWORD_URL" => "",
  "PROFILE_URL" => "/personal/account/",
  "REGISTER_URL" => "/login/",
  "SHOW_ERRORS" => "Y"
  )
  );
  ?>
  </div>
  </li>
  </ul>
  </div>
  </div>
  </div>
  </section>

  <section class="menu">
  <nav class="navbar navigation">
  <div class="container">
  <div class="navbar-header">
  <h2 class="menu-title">Главное меню</h2>
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
  </div>
  <div id="navbar" class="navbar-collapse collapse text-center">
  <?
  $APPLICATION->IncludeComponent(
  "bitrix:menu", "top", Array(
  "ALLOW_MULTI_SELECT" => "N",
  "CHILD_MENU_TYPE" => "left",
  "DELAY" => "N",
  "MAX_LEVEL" => "2",
  "MENU_CACHE_GET_VARS" => array(""),
  "MENU_CACHE_TIME" => "3600",
  "MENU_CACHE_TYPE" => "A",
  "MENU_CACHE_USE_GROUPS" => "N",
  "ROOT_MENU_TYPE" => "top",
  "USE_EXT" => "Y",
  "CNT_ITEMS_COL" => 5
  )
  );
  ?>
  </div>
  </div>
  </nav>
  </section>


  <? /*
  <header class="page-header">
  <!-- RD Navbar-->
  <div class="rd-navbar-wrap">
  <nav class="rd-navbar rd-navbar_inverse" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-static" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-stick-up-clone="false" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true" data-md-stick-up-offset="120px" data-lg-stick-up-offset="35px" data-body-class="">
  <div class="rd-navbar-inner rd-navbar-search-wrap">
  <!-- RD Navbar Panel-->
  <div class="rd-navbar-panel rd-navbar-search_collapsable">
  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
  <!-- RD Navbar Brand-->
  <div class="rd-navbar-brand"><a class="brand-name" href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/images/logo-108x47.png" alt="" width="108" height="47"/></a></div>
  </div>
  <!-- RD Navbar Nav-->
  <div class="rd-navbar-nav-wrap rd-navbar-search_not-collapsable">
  <ul class="rd-navbar-items-list rd-navbar-search_collapsable">
  <li>
  8 918 984 08 28<br/>
  Ежедневно: с 9 до 21
  </li>
  <li>
  <button class="rd-navbar-search__toggle rd-navbar-fixed--hidden" data-rd-navbar-toggle=".rd-navbar-search-wrap"></button>
  </li>
  <li class="rd-navbar-nav-wrap__shop"><a class="icon icon-md linear-icon-cart link-primary" href="/personal/cart/"></a></li>
  </ul>
  <!-- RD Search-->
  <div class="rd-navbar-search rd-navbar-search_toggled rd-navbar-search_not-collapsable">
  <form class="rd-search" action="/search/" method="GET" data-search-live="rd-search-results-live">
  <div class="form-wrap">
  <input class="form-input" id="rd-navbar-search-form-input" type="text" name="q" autocomplete="off">
  <label class="form-label" for="rd-navbar-search-form-input">Поиск</label>
  <div class="rd-search-results-live" id="rd-search-results-live"></div>
  </div>
  <button class="rd-search__submit" type="submit"></button>
  </form>
  <div class="rd-navbar-fixed--hidden">
  <button class="rd-navbar-search__toggle" data-custom-toggle=".rd-navbar-search-wrap" data-custom-toggle-disable-on-blur="true"></button>
  </div>
  </div>
  <div class="rd-navbar-search_collapsable">
  <?
  $APPLICATION->IncludeComponent(
  "bitrix:menu", "top", Array(
  "ALLOW_MULTI_SELECT" => "N",
  "CHILD_MENU_TYPE" => "left",
  "DELAY" => "N",
  "MAX_LEVEL" => "2",
  "MENU_CACHE_GET_VARS" => array(""),
  "MENU_CACHE_TIME" => "3600",
  "MENU_CACHE_TYPE" => "A",
  "MENU_CACHE_USE_GROUPS" => "N",
  "ROOT_MENU_TYPE" => "top",
  "USE_EXT" => "Y"
  )
  );
  ?>
  </div>
  </div>
  </div>
  </nav>
  </div>
  </header>
 */ ?>
<?/*

<header class="b-header">
	<div class="container">
		<div class="b-header__top row row_str">
			<div class="col col_left">
				<div class="b-header__logo"><a href="/" class="b-icon b-icon_svg -logo"></a></div>
			</div>
			<div class="col col_center last">
				<div class="b-header-bar__mobile">
					<div class="b-bar-mobile">
						<ul>
							<li class="b-bar-mobile__item">
								<?
								$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mobile", Array(
									"HIDE_ON_BASKET_PAGES" => "N", // Не показывать на страницах корзины и оформления заказа
									"PATH_TO_BASKET" => SITE_DIR . "personal/cart/", // Страница корзины
									"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/", // Страница оформления заказа
									"PATH_TO_PERSONAL" => SITE_DIR . "personal/", // Страница персонального раздела
									"PATH_TO_PROFILE" => SITE_DIR . "personal/", // Страница профиля
									"PATH_TO_REGISTER" => SITE_DIR . "login/", // Страница регистрации
									"POSITION_FIXED" => "N", // Отображать корзину поверх шаблона
									"SHOW_AUTHOR" => "N", // Добавить возможность авторизации
									"SHOW_EMPTY_VALUES" => "Y", // Выводить нулевые значения в пустой корзине
									"SHOW_NUM_PRODUCTS" => "Y", // Показывать количество товаров
									"SHOW_PERSONAL_LINK" => "N", // Отображать персональный раздел
									"SHOW_PRODUCTS" => "Y", // Показывать список товаров
									"SHOW_TOTAL_PRICE" => "Y", // Показывать общую сумму по товарам
										), false
								);
								?>
							</li>
							<li class="b-bar-mobile__item">
								<div class="b-bar-mobile-item">
									<i class="b-icon b-icon_svg -search"></i>
								</div>
							</li>
							<li class="b-bar-mobile__item">
								<div class="b-bar-mobile-item">
									<i class="b-icon b-icon_svg -hamburger"></i>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col col_right last">
				<div class="b-header__nav-main row">
					<div class="col b-nav b-nav_inline -uppercase">
						<?
						$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
							"ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "top", // Тип меню для остальных уровней
							"DELAY" => "N", // Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1", // Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A", // Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
							"ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
							"USE_EXT" => "Y", // Подключать файлы с именами вида .тип_меню.menu_ext.php
								), false
						);
						?>
					</div>
					<? CWebavkTmplProTools::IncludeBlockFromDir("header-glossary") ?>
				</div>
			</div>
		</div>
		<div class="b-header__bottom row">
			<div class="col_left"></div>
			<div class="col_right">
				<div class="b-header__bar row">
					<div class="b-header-bar__menu row">
						<div class="col">
							<a href="/catalog/" class="b-button js-dropdown-button -icon -right -gray-bg" data-target="drop1">
								<div class="b-button__inner"><i class="b-icon b-icon_svg -list"></i> <span>каталог</span></div>
								<i class="b-icon b-button__arrow"></i>
							</a>
						</div>
						<div class="col last">
							<a href="/brands/" class="b-button js-dropdown-button -icon -right -gray-bg" data-target="drop2">
								<div class="b-button__inner"><i class="b-icon b-icon_svg -brush"></i> <span>бренды</span></div>
								<i class="b-icon b-button__arrow"></i>
							</a>
						</div>
					</div>
					<div class="b-header-bar__search">
						<div class="b-search -is-active js-search">
							<div class="b-search__input">
								<input class="js-search-input" type="text" placeholder="Что вы ищете?">
							</div>
							<div class="b-search__button">
								<div class="b-button js-search-button"><i class="b-icon b-icon_svg -search"></i></div>
							</div>
						</div>
					</div>
					<div class="b-header-bar__info">
						<?
						$APPLICATION->IncludeComponent(
								"webavk:blank.component", "header.favorites", Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "N"
								)
						);
						?>
						<?
						$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", Array(
							"HIDE_ON_BASKET_PAGES" => "N", // Не показывать на страницах корзины и оформления заказа
							"PATH_TO_BASKET" => SITE_DIR . "personal/cart/", // Страница корзины
							"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/", // Страница оформления заказа
							"PATH_TO_PERSONAL" => SITE_DIR . "personal/", // Страница персонального раздела
							"PATH_TO_PROFILE" => SITE_DIR . "personal/", // Страница профиля
							"PATH_TO_REGISTER" => SITE_DIR . "login/", // Страница регистрации
							"POSITION_FIXED" => "N", // Отображать корзину поверх шаблона
							"SHOW_AUTHOR" => "N", // Добавить возможность авторизации
							"SHOW_EMPTY_VALUES" => "Y", // Выводить нулевые значения в пустой корзине
							"SHOW_NUM_PRODUCTS" => "Y", // Показывать количество товаров
							"SHOW_PERSONAL_LINK" => "N", // Отображать персональный раздел
							"SHOW_PRODUCTS" => "Y", // Показывать список товаров
							"SHOW_TOTAL_PRICE" => "Y", // Показывать общую сумму по товарам
								), false
						);
						?>
					</div>
				</div>
				<?
				ob_start();
				$APPLICATION->IncludeComponent(
						"webavk:blank.component", "catalog.popup", Array(
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"IBLOCK_ID" => 3,
					"DESTINATION_IBLOCK_ID" => 27,
					"DESTINATION_SECTION_PROPERTY" => "UF_DESTINATION",
					"DESTINATION_SECTION_NAME_PROPERTY" => "UF_DESTINATION_NAME",
					"IS_ACTION_PROPERTY" => "UF_SYS_IS_ACTION",
					"IS_POPULAR_PROPERTY" => "UF_SYS_IS_POPULAR"
						)
				);
				$content = ob_get_contents();
				ob_end_clean();
				ob_start();
				$APPLICATION->IncludeComponent(
						"bitrix:advertising.banner", "", Array(
					"CACHE_TIME" => "0",
					"CACHE_TYPE" => "A",
					"NOINDEX" => "N",
					"QUANTITY" => "1",
					"TYPE" => "MENU_CATALOG_POPUP"
						)
				);
				$banner = ob_get_contents();
				ob_end_clean();
				echo str_replace("#BANNER#", $banner, $content);
				?>
				<?
				$APPLICATION->IncludeComponent(
						"webavk:blank.component", "brands.popup", Array(
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"IBLOCK_ID" => 6
						)
				);
				?>
			</div>
		</div>
	</div>
</header>
 * 
 */