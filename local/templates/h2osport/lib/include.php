<?
CJSCore::Init(array("jquery2"));
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-vendor/modernizr-2.8.3.js", INCLUDE_MINIFY_WEB_CONTENT);
//CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-vendor/jquery-1.12.0.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-vendor/jquery-ui.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-vendor/datepicker-ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/bootstrap.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/jquery.meanmenu.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/slick.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/jquery.treeview.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/lightbox.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/jquery-ui.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/jquery.nivo.slider.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/jquery.nicescroll.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/countdon.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/wow.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-template/plugins.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/03-canjs/can.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.validation/jquery.validate.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.validation/additional-methods.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.validation/localization/messages_ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-jquery.maskedinput/jquery.maskedinput.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-kladr/jquery.kladr.js", INCLUDE_MINIFY_WEB_CONTENT);
