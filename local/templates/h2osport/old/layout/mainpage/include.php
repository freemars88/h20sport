<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
	<head>
		<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
	</head>
	<body id="body">
		<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.slider"); ?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.category"); ?>
		<?
		if ($layoutArea == "header")
			goto TEMPLATE_END;
		FOOTER:
		?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.subscribe"); ?>
		<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.instagram"); ?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
	</body>
</html>
<?
TEMPLATE_END:
