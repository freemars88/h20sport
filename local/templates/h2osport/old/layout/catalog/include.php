<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
	<head>
		<? CWebavkTmplProTools::IncludeBlockFromDir("head"); ?>
	</head>
	<body id="body">
		<? CWebavkTmplProTools::IncludeBlockFromDir("admin.panel"); ?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("header"); ?>
		<section class="page-header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="content">
							<h1 class="page-name"><? $APPLICATION->ShowTitle(false) ?></h1>
							<?
							$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
								"START_FROM" => "0",
								"PATH" => "",
								"SITE_ID" => "-"
									), false, Array('HIDE_ICONS' => 'Y')
							);
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?
		if ($layoutArea == "header")
			goto TEMPLATE_END;
		FOOTER:
		?>
		<? //CWebavkTmplProTools::IncludeBlockFromDir("content.mainpage.instagram"); ?>
		<? CWebavkTmplProTools::IncludeBlockFromDir("footer"); ?>
	</body>
</html>
<?
TEMPLATE_END:
