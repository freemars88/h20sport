<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogTopComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$rSizes = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_REFERENCE_SIZE, "ACTIVE" => "Y"), false, false, array("ID", "NAME"));
while ($arSize = $rSizes->Fetch())
{
	$arResult['SIZES'][$arSize['ID']] = $arSize;
}
