<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */
/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>


<section class="signin-page account">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="block text-center">
					<h2 class="text-center">Регистрация на сайте</h2>
					<?
					if (!empty($arParams["~AUTH_RESULT"]))
					{
						$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
						?>
						<div class="alert <?= ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "alert-success" : "alert-danger") ?>"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
						<?
					}
					if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) && $arParams["AUTH_RESULT"]["TYPE"] === "OK")
					{
						?>
						<div class="alert alert-success"><? echo GetMessage("AUTH_EMAIL_SENT") ?></div>
						<?
					} else
					{
						?>
						<form class="text-left clearfix" method="post" action="<?= $arResult["AUTH_URL"] ?>" name="bform">
							<?
							if ($arResult["BACKURL"] <> '')
							{
								?>
								<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
							<? } ?>
							<input type="hidden" name="AUTH_FORM" value="Y" />
							<input type="hidden" name="TYPE" value="REGISTRATION" />
							<div class="form-group">
								<input type="text" name="USER_LAST_NAME" maxlength="255" value="<?= $arResult["USER_LAST_NAME"] ?>" placeholder="Фамилия" class="form-control" />
							</div>
							<div class="form-group">
								<input type="text" name="USER_NAME" maxlength="255" value="<?= $arResult["USER_NAME"] ?>" placeholder="Имя" class="form-control" />
							</div>
							<div class="form-group">
								<input type="text" name="USER_PERSONAL_PHONE" maxlength="255" value="<?= $arResult["USER_PERSONAL_PHONE"] ?>" placeholder="Телефон" class="form-control" />
							</div>
							<div class="form-group">
								<input type="text" name="USER_EMAIL" maxlength="255" value="<?= $arResult["USER_EMAIL"] ?>" placeholder="E-mail" class="form-control" />
							</div>
							<div class="form-group">
								<input type="password" name="USER_PASSWORD" maxlength="255" value="<?= $arResult["USER_PASSWORD"] ?>" autocomplete="off" placeholder="Пароль" class="form-control" />
							</div>
							<div class="form-group">
								<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off" placeholder="Подтверждение пароля" class="form-control" />
							</div>
							<div class="text-center">
								<input type="hidden" name="Register" value="yes" />
								<button type="submit" class="btn btn-main text-center">Зарегистрироваться</button>
							</div>
						</form>
						<p class="mt-20">Уже есть аккаунт ?<noindex><a href="<?= $arResult["AUTH_AUTH_URL"] ?>" rel="nofollow"> Авторизоваться</a></noindex></p>
							<?
						}
						?>
				</div>
			</div>
		</div>
	</div>
</section>

