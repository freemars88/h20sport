<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="partners-links-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Уважаемые партнёры!</h2>
				<p>В данном разделе представлены основные макеты рекламных и POS материалов, необходимые для выделения торговой марки Francesco Donni в рамках торговой точки. Все макеты выполненны по слоям, с качественным графическим разрешением, что позволит Вам их модифицировать под персональные потребности.</p>
			</div>
		</div>
		<div class="row">
			<?
			foreach ($arResult["ITEMS"] as $arItem)
			{
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="col-md-12">
					<div class="panel panel-default" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-10">
									<a name="#<?= $arItem['CODE'] ?>"></a><?= $arItem['NAME'] ?>
								</div>
								<div class="col-md-2 text-right">
									<a href="<?= $arItem['PROPERTY_LINK_DOWNLOAD_VALUE'] ?>" target="_blank">Скачать <i class="pe-7s-cloud-download"></i></a>
								</div>
							</div>
						</div>
						<div class="panel-body text-center">
							<a href="<?= $arItem['PROPERTY_LINK_DOWNLOAD_VALUE'] ?>" target="_blank"><img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" /></a>
						</div>
					</div>
				</div>
				<?
			}
			?>
		</div>
	</div>
</section>
