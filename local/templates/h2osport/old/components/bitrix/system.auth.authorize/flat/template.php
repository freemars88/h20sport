<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */
?>
<section class="signin-page account">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="block text-center">
					<h2 class="text-center">Вход для зарегистрированных пользователей</h2>
					<form class="text-left clearfix" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
						<?
						if (!empty($arParams["~AUTH_RESULT"]))
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert alert-danger"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						if ($arResult['ERROR_MESSAGE'] <> '')
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
							?>
							<div class="alert alert-danger"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						?>
						<input type="hidden" name="AUTH_FORM" value="Y" />
						<input type="hidden" name="TYPE" value="AUTH" />
						<?
						if (strlen($arResult["BACKURL"]) > 0)
						{
							?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
							<?
						}
						foreach ($arResult["POST"] as $key => $value)
						{
							?>
							<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
							<?
						}
						?>
						<div class="form-group">
							<input type="text" class="form-control" name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>" placeholder="Логин" id="USER_LOGIN" />
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="Пароль" id="USER_PASSWORD" />
						</div>
						<?
						if ($arResult["STORE_PASSWORD"] == "Y")
						{
							?>
							<div class="form-group">
								<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /> <label>Запомнить меня</label>
							</div>
							<?
						}
						if ($arResult["CAPTCHA_CODE"])
						{
							?>
							<div class="form-group">
								<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
								<img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
								<input type="text" name="captcha_word" maxlength="50" value="" />
							</div>
							<?
						}
						?>
						<div class="text-center">
							<input type="hidden" name="Login" value="yes" />
							<button type="submit" class="btn btn-main text-center">Вход</button>
						</div>
					</form>
					<p class="mt-20">Забыли пароль ?<noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow"> Восстановить</a></noindex></p>
					<?
					if ($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y")
					{
						?>
						<p class="mt-20">Впервые на сайте? 
						<noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow">Зарегистрироваться</a></noindex>
						</p>
						<?
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<?
/*
  <section class="b-section b-section_lk">
  <div class="container">
  <div class="b-form b-form_order">
  <?
  if ($arResult["AUTH_SERVICES"])
  {
  $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", array(
  "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
  "AUTH_URL" => $arResult["AUTH_URL"],
  "POST" => $arResult["POST"],
  ), $component, array("HIDE_ICONS" => "Y")
  );
  }
  ?>
  </div>
  </div>
  </section>

  <script type="text/javascript">
  <?
  if (strlen($arResult["LAST_LOGIN"]) > 0)
  {
  ?>
  try {
  document.form_auth.USER_PASSWORD.focus();
  } catch (e) {
  }
  <?
  } else
  {
  ?>
  try {
  document.form_auth.USER_LOGIN.focus();
  } catch (e) {
  }
  <? } ?>
  </script>

 */?>