<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>
<section class="forget-password-page account">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="block text-center">
					<h2 class="text-center">Восстановление пароля</h2>
					<form class="text-left form_auth clearfix" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
						<?
						if (!empty($arParams["~AUTH_RESULT"]))
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert <?= ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "alert-success" : "alert-danger") ?>"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						?>
						<? if ($arResult["BACKURL"] <> ''): ?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
						<? endif ?>
						<input type="hidden" name="AUTH_FORM" value="Y">
						<input type="hidden" name="TYPE" value="SEND_PWD">
						<p><?= GetMessage("AUTH_FORGOT_PASSWORD_1") ?></p>
						<div class="form-group">
							<input type="text" class="form-control" id="exampleInputEmail1" placeholder="EMail адрес" name="USER_EMAIL" />
						</div>
						<div class="text-center">
							<input type="hidden" name="send_account_info" value="Y" />
							<button type="submit" class="btn btn-main text-center">Отправить</button>
						</div>
					</form>
					<p class="mt-20"><noindex><a href="<?= $arResult["AUTH_AUTH_URL"] ?>" rel="nofollow">Авторизоваться</a></noindex></p>
				</div>
			</div>
		</div>
	</div>
</section>
