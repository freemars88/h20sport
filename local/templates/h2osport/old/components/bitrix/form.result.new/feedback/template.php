<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
//myPrint($arResult);
?>
<section class="feedback-page account">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="block text-center">
					<div style="display: none;"><div id="errorMessages"></div></div>
					<?
					if ($_REQUEST['formresult'] == "addok")
					{
						?>
						<h2 class="text-center">
							Спасибо! Ваше сообщение отправлено.
						</h2>
						<?
					} else
					{
						if ($arResult["isFormErrors"] == "Y")
						{
							echo $arResult["FORM_ERRORS_TEXT"];
						}
						?>
						<?= $arResult["FORM_HEADER"] ?>
						<?
						foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
						{
							if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
							{
								echo $arQuestion["HTML_CODE"];
							}
						}
						$FORM_FIELD_FIO = "SIMPLE_QUESTION_926";
						$FORM_FIELD_PHONE = "SIMPLE_QUESTION_165";
						$FORM_FIELD_EMAIL = "SIMPLE_QUESTION_103";
						$FORM_FIELD_MESSAGE = "SIMPLE_QUESTION_197";
						?>
						<div class="text-left clearfix">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="<?= $arResult["QUESTIONS"][$FORM_FIELD_FIO]['CAPTION'] ?>" value="<?= $arResult["arrVALUES"]['form_text_' . $arResult["QUESTIONS"][$FORM_FIELD_FIO]['STRUCTURE'][0]['ID']] ?>" name="form_text_<?= $arResult["QUESTIONS"][$FORM_FIELD_FIO]['STRUCTURE'][0]['ID'] ?>" id="form_text_<?= $arResult["QUESTIONS"][$FORM_FIELD_FIO]['STRUCTURE'][0]['ID'] ?>" />
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="<?= $arResult["QUESTIONS"][$FORM_FIELD_PHONE]['CAPTION'] ?>" value="<?= $arResult["arrVALUES"]['form_text_' . $arResult["QUESTIONS"][$FORM_FIELD_PHONE]['STRUCTURE'][0]['ID']] ?>" name="form_text_<?= $arResult["QUESTIONS"][$FORM_FIELD_PHONE]['STRUCTURE'][0]['ID'] ?>" id="form_text_<?= $arResult["QUESTIONS"][$FORM_FIELD_PHONE]['STRUCTURE'][0]['ID'] ?>" />
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="<?= $arResult["QUESTIONS"][$FORM_FIELD_EMAIL]['CAPTION'] ?>" value="<?= $arResult["arrVALUES"]['form_email_' . $arResult["QUESTIONS"][$FORM_FIELD_EMAIL]['STRUCTURE'][0]['ID']] ?>" name="form_email_<?= $arResult["QUESTIONS"][$FORM_FIELD_EMAIL]['STRUCTURE'][0]['ID'] ?>" id="form_email_<?= $arResult["QUESTIONS"][$FORM_FIELD_EMAIL]['STRUCTURE'][0]['ID'] ?>" />
							</div>
							<div class="form-group">
								<textarea rows="10" cols="40" name="form_textarea_<?= $arResult["QUESTIONS"][$FORM_FIELD_MESSAGE]['STRUCTURE'][0]['ID'] ?>" id="form_textarea_<?= $arResult["QUESTIONS"][$FORM_FIELD_MESSAGE]['STRUCTURE'][0]['ID'] ?>" placeholder="<?= $arResult["QUESTIONS"][$FORM_FIELD_MESSAGE]['CAPTION'] ?>" class="form-control"><?= $arResult["arrVALUES"]['form_textarea_' . $arResult["QUESTIONS"][$FORM_FIELD_MESSAGE]['STRUCTURE'][0]['ID']] ?></textarea>
							</div>
							<div class="text-center">
								<button type="submit" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> class="btn btn-main text-center"><?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?></button>
								<input type="hidden" name="web_form_submit" value="Y" />
							</div>
						</div>
						<?= $arResult["FORM_FOOTER"] ?>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</section>
