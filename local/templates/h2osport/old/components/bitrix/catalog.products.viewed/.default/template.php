<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;

if (!empty($arResult['ITEMS']))
{
	//myPrint($arResult);
	?>
	<div class="row">
		<?
		foreach ($arResult['ITEMS'] as $arItem)
		{
			if (!empty($arItem['OFFERS']))
			{
				$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]) ? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']] : reset($arItem['OFFERS']);
			} else
			{
				$actualItem = $arItem;
			}
			$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
			$discount = 0;
			$oldPrice = false;
			if (isset($arItem['PRICES']['OLDPRICE']) && $arItem['PRICES']['OLDPRICE']['VALUE'] >= 0)
			{
				$oldPrice = $arItem['PRICES']['OLDPRICE']['PRINT_VALUE'];
				$discount = round((($arItem['PRICES']['OLDPRICE']['VALUE'] - $price['PRICE']) / $arItem['PRICES']['OLDPRICE']['VALUE']) * 100, 0);
			}
			?>
			<div class="col-md-3">
				<div class="product-item">
					<div class="product-thumb">
						<?
						if ($arItem['PROPERTIES']['LABEL_SALE']['VALUE'] == "Y")
						{
							?>
							<span class="bage sale">Sale<?= ($discount > 0 ? ' ' . $discount . '%' : "") ?></span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_POPULAR']['VALUE'] == "Y")
						{
							?>
							<span class="bage">Популярное</span>
							<?
						} elseif ($arItem['PROPERTIES']['LABEL_RECOMENDED']['VALUE'] == "Y")
						{
							?>
							<span class="bage">Рекомендуем</span>
							<?
						}
						$arPhoto = false;
						if ($arItem['PREVIEW_PICTURE']['ID'] > 0)
						{
							$arPhoto = $arItem['PREVIEW_PICTURE'];
						}
						if ($arPhoto)
						{
							$arPhoto = CFile::ResizeImageGet($arPhoto['ID'], array('width' => 500, 'height' => 600), BX_RESIZE_IMAGE_EXACT, true);
						}
						?>
						<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="imglink">
							<img class="img-responsive" src="<?= $arPhoto['src'] ?>" alt="<?= $arItem['NAME'] ?>" />
						</a>
						<div class="preview-meta">
							<ul>
								<? /*
								  <li>
								  <span  data-toggle="modal" data-target="#product-modal">
								  <i class="tf-ion-ios-search"></i>
								  </span>
								  </li> */ ?>
								<li>
									<a href="javascript:void(0)" class="add-favorite" data-id="<?= $arItem['ID'] ?>"><i class="tf-ion-ios-heart"></i></a>
								</li>
								<li>
									<?
									if (empty($arItem['OFFERS']))
									{
										?>
										<a href="javascript:void(0)" class="add-to-basket" data-id="<?= $arItem['ID'] ?>"><i class="tf-ion-android-cart"></i></a>
										<?
									} else
									{
										?>
										<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><i class="tf-ion-android-cart"></i></a>
										<?
									}
									?>
								</li>
							</ul>
						</div>
					</div>
					<div class="product-content">
						<h4><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h4>
						<p class="price"><?
							if (strlen($oldPrice) > 0)
							{
								?>
								<span class="product-oldprice"><?= $oldPrice ?></span> 
								<?
							}
							?><?= $price['PRINT_RATIO_PRICE'] ?></p>
					</div>
				</div>
			</div>
			<?
		}
		?>
		<div class="clearfix"></div>
	</div>
	<?
}
