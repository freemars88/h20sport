<?

foreach ($arResult["PAY_SYSTEM"] as $k => $arPaySystem)
{
	if ($arPaySystem['ID'] == PAYSYSTEM_SBERBANK_ID)
	{
		unset($arResult["PAY_SYSTEM"][$k]);
	}
	if ($arPaySystem['ID'] == PAYSYSTEM_SBERBANK_PRE_ID && !$USER->IsAdmin())
	{
		unset($arResult["PAY_SYSTEM"][$k]);
	}
}

foreach ($arResult["ORDER_PROP"] as $arProp)
{
	foreach ($arProp as $arProperty)
	{
		$arResult['PROP_BY_CODE'][$arProperty['CODE']] = $arProperty['VALUE'];
	}
}

if ($arResult['PROP_BY_CODE']['LOCATION'] > 0)
{
	$arLocation = CSaleLocation::GetByID($arResult['PROP_BY_CODE']['LOCATION'], LANGUAGE_ID);
	$arResult['PROP_BY_CODE']['LOCATION_NAME'] = $arLocation['CITY_NAME_LANG'];
}

if (!empty($arResult["DELIVERY"]))
{
	foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
	{
		if (in_array($delivery_id,array(3)))
		{
			unset($arResult["DELIVERY"][$delivery_id]);
			continue;
		}
		if ($delivery_id !== 0 && intval($delivery_id) <= 0)
		{
			$rRes = CSaleDeliveryHandler::GetBySID($delivery_id);
			if ($arRes = $rRes->Fetch())
			{
				$arResult["DELIVERY"][$delivery_id]['SORT'] = $arRes['SORT'];
			}
		}
	}
	uasort($arResult["DELIVERY"], function($a, $b) {
		if ($a['SORT'] == $b['SORT'])
		{
			return 0;
		}
		return ($a['SORT'] < $b['SORT']) ? -1 : 1;
	});

	$isChecked = false;
	foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
	{
		if ($delivery_id !== 0 && intval($delivery_id) <= 0 && isset($arDelivery['PROFILES']))
		{
			foreach ($arDelivery['PROFILES'] as $profile_id => $arProfile)
			{
				if ($arProfile['CHECKED'] == "Y")
				{
					$arResult['DELIVERY_ACTIVE'] = $arDelivery['SID'] . ":" . $arProfile['SID'];
					$arResult['DELIVERY_ACTIVE_ID'] = $arProfile['ID'];
					$arResult["DELIVERY"][$delivery_id]['CHECKED'] = 'Y';
					$isChecked = true;
					break(2);
				}
			}
		} elseif (intval($delivery_id) > 0)
		{
			if ($arDelivery['CHECKED'] == "Y")
			{
				$arResult['DELIVERY_ACTIVE'] = $arDelivery['ID'];
				$arResult['DELIVERY_ACTIVE_ID'] = $arDelivery['ID'];
				$isChecked = true;
				break;
			}
		}
	}

	if (!$isChecked)
	{
		foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
		{
			if ($delivery_id !== 0 && intval($delivery_id) <= 0 && isset($arDelivery['PROFILES']))
			{
				foreach ($arDelivery['PROFILES'] as $profile_id => $arProfile)
				{
					$arResult["DELIVERY"][$delivery_id]['PROFILES'][$profile_id]['CHECKED'] = "Y";
					$arResult["DELIVERY"][$delivery_id]['CHECKED'] = 'Y';
					$arResult['DELIVERY_ACTIVE'] = $arDelivery['SID'] . ":" . $arProfile['SID'];
					$arResult['DELIVERY_ACTIVE_ID'] = $arProfile['ID'];
					break(2);
				}
			} elseif (intval($delivery_id) > 0)
			{
				$arResult["DELIVERY"][$delivery_id]['CHECKED'] = "Y";
				$arResult['DELIVERY_ACTIVE'] = $arDelivery['ID'];
				$arResult['DELIVERY_ACTIVE_ID'] = $arDelivery['ID'];
				break;
			}
		}
	}
}

$arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY_SHOW'] = "N";
if ($arResult['DELIVERY_ACTIVE_ID'] == DELIVERY_DALLI_COURIER_MSK_ID && $arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE'] > 0 && $arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE'] >= (FREE_DELIVERY_MSK_SUM * 2 / 3))
{
	$arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY_VALUE'] = FREE_DELIVERY_MSK_SUM - $arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE'];
	$arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY'] = FormatCurrency($arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY_VALUE'], "RUB");
	$arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY_SHOW'] = "Y";
}

/*
if ($arResult['DELIVERY_ACTIVE'] == 2)
{
	foreach ($arResult["PAY_SYSTEM"] as $k => $arPaySystem)
	{
		if (in_array($arPaySystem['ID'], array(RBS_PAY_SYSTEM_ID, RBS_PREPAY_SYSTEM_ID)))
		{
			$arResult['PAY_SYSTEM'][$k]['ORDER_TOTAL_PRICE_FORMATED'] = $arResult["PRICE_WITHOUT_DISCOUNT_VALUE"] * 0.93;
			if (round($arPaySystem['RESULT']['DISCOUNT_PERCENT']) == 7)
			{
				$arResult['PAY_SYSTEM'][$k]['IS_PICKUP_AND_RBS'] = true;
			}
		} else
		{
			$arResult['PAY_SYSTEM'][$k]['ORDER_TOTAL_PRICE_FORMATED'] = $arResult['ORDER_TOTAL_PRICE_FORMATED'];
		}
	}
} else
{
	foreach ($arResult["PAY_SYSTEM"] as $k => $arPaySystem)
	{
		$arResult['PAY_SYSTEM'][$k]['ORDER_TOTAL_PRICE_FORMATED'] = $arResult['ORDER_TOTAL_PRICE_FORMATED'];
	}
}

if ($arResult['DELIVERY_ACTIVE'] != DELIVERY_ID_EUROSET && $_REQUEST['delivery-old'] == DELIVERY_ID_EUROSET)
{
	foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
	{
		if ($delivery_id == DELIVERY_ID_EUROSET)
		{
			$arResult["DELIVERY"][$delivery_id]['office-euroset'] = '<a href="javascript:void(0)" class="selectEurosetButton" style="color: #AA0000; text-decoration: none;">Выбрать салон</a>';
		}
	}
	foreach ($arResult['ORDER_PROP']['USER_PROPS_Y'] as $k => $arProp)
	{
		if ($arProp['CODE'] == "ADDRESS")
		{
			$arResult['ORDER_PROP']['USER_PROPS_Y'][$k]['VALUE'] = "";
			$arResult['ORDER_PROP']['USER_PROPS_Y'][$k]['~VALUE'] = "";
			$propId = $arProp['ID'];
		}
	}
	foreach ($arResult['ORDER_PROP']['USER_PROPS_N'] as $k => $arProp)
	{
		if ($arProp['CODE'] == "ADDRESS")
		{
			$arResult['ORDER_PROP']['USER_PROPS_Y'][$k]['VALUE'] = "";
			$arResult['ORDER_PROP']['USER_PROPS_Y'][$k]['~VALUE'] = "";
			$propId = $arProp['ID'];
		}
	}
	if (strlen($arUserResult['ORDER_PROP'][$propId]) > 0)
	{
		$arUserResult['ORDER_PROP'][$propId] = "";
	}
}

if (!empty($arResult['PAY_SYSTEM']))
{
	foreach ($arResult['PAY_SYSTEM'] as $k => $arPaySystem)
	{
		if ($arPaySystem['ID'] == RBS_PAY_SYSTEM_ID)
		{
			unset($arResult['PAY_SYSTEM'][$k]);
		}
	}
}
*/