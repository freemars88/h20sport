<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
if (!empty($arResult["ORDER_PROP"]["USER_PROFILES"]))
{
	$ak = $arResult["ORDER_PROP"]["USER_PROFILES"];
	$arUserProfiles = $arResult["ORDER_PROP"]["USER_PROFILES"][$ak[0]];
	?>
	<input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?= $arUserProfiles["ID"] ?>" />
	<?
}
$curPerson = false;
foreach ($arResult["PERSON_TYPE"] as $arPerson)
{
	if ($curPerson === false || $arPerson['CHECKED'] == "Y")
	{
		$curPerson = $arPerson;
	}
}

if ($curPerson['ID'] == ORDER_PERSON_TYPE_FIZ)
{
	?>
	<div class="form-group">
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "FIO", "Ваше ФИО"); ?>
	</div>
	<div class="form-group">
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "PHONE", "Телефон"); ?>
	</div>
	<div class="form-group">
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "EMAIL", "Эл. почта"); ?>
	</div>
	<div class="form-group">
		<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "LOCATION", "Регион"); ?>
	</div>
	<?
	if (!in_array($arResult['USER_VALS']['DELIVERY_ID'], array(DELIVERY_DALLI_PVZ_DALLI_ID, DELIVERY_DALLI_PVZ_BOXBERRY_ID, DELIVERY_DALLI_PVZ_SDEK_ID, DELIVERY_DALLI_PVZ_PICKUP_ID, DELIVERY_PICKUP_ID)))
	{
		?>
		<div class="form-group">
			<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "STREET", "Улица"); ?>
		</div>
		<div class="form-group">
			<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "HOUSE", "Дом"); ?>
		</div>
		<div class="form-group">
			<? PrintPropsFormProperty($arResult["ORDER_PROP"], $arParams["TEMPLATE_LOCATION"], "FLAT", "Квартира"); ?>
		</div>
		<?
	}
}

foreach ($arResult["ORDER_PROP"] as $v)
{
	if (!in_array($arResult['USER_VALS']['DELIVERY_ID'], array(DELIVERY_DALLI_PVZ_DALLI_ID, DELIVERY_DALLI_PVZ_BOXBERRY_ID, DELIVERY_DALLI_PVZ_SDEK_ID, DELIVERY_DALLI_PVZ_PICKUP_ID, DELIVERY_PICKUP_ID)))
	{
		PrintPropsFormOther($v);
	} else
	{
		PrintPropsFormOther($v, array("STREET", "HOUSE", "FLAT"));
	}
}
if (!CSaleLocation::isLocationProEnabled())
{
	?>
	<div style="display:none;">

		<?
		$APPLICATION->IncludeComponent(
				"bitrix:sale.ajax.locations", $arParams["TEMPLATE_LOCATION"], array(
			"AJAX_CALL" => "N",
			"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
			"REGION_INPUT_NAME" => "REGION_tmp",
			"CITY_INPUT_NAME" => "tmp",
			"CITY_OUT_LOCATION" => "Y",
			"LOCATION_VALUE" => "",
			"ONCITYCHANGE" => "submitForm()",
				), null, array('HIDE_ICONS' => 'Y')
		);
		?>

	</div>
	<?
}
