<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
//==================== edost НАЧАЛО
/* if (isset($arResult['edost']['javascript']))
  echo $arResult['edost']['javascript'];
 * 
 */
if (isset($arResult['edost']['warning']))
	echo $arResult['edost']['warning'] . '<br>';

//==================== edost КОНЕЦ
?>
<script type="text/javascript">
	function fShowStore(id, showImages, formWidth, siteId)
	{
		var strUrl = '<?= $templateFolder ?>' + '/map.php';
		var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

		var storeForm = new BX.CDialog({
			'title': '<?= GetMessage('SOA_ORDER_GIVE') ?>',
			head: '',
			'content_url': strUrl,
			'content_post': strUrlPost,
			'width': formWidth,
			'height': 450,
			'resizable': false,
			'draggable': false
		});

		var button = [
			{
				title: '<?= GetMessage('SOA_POPUP_SAVE') ?>',
				id: 'crmOk',
				'action': function ()
				{
					GetBuyerStore();
					BX.WindowManager.Get().Close();
				}
			},
			BX.CDialog.btnCancel
		];
		storeForm.ClearButtons();
		storeForm.SetButtons(button);
		storeForm.Show();
	}

	function GetBuyerStore()
	{
		BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
		//BX('ORDER_DESCRIPTION').value = '<?= GetMessage("SOA_ORDER_GIVE_TITLE") ?>: '+BX('POPUP_STORE_NAME').value;
		BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
		BX.show(BX('select_store'));
	}

	function showExtraParamsDialog(deliveryId)
	{
		var strUrl = '<?= $templateFolder ?>' + '/delivery_extra_params.php';
		var formName = 'extra_params_form';
		var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

		if (window.BX.SaleDeliveryExtraParams)
		{
			for (var i in window.BX.SaleDeliveryExtraParams)
			{
				strUrlPost += '&' + encodeURI(i) + '=' + encodeURI(window.BX.SaleDeliveryExtraParams[i]);
			}
		}

		var paramsDialog = new BX.CDialog({
			'title': '<?= GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS') ?>',
			head: '',
			'content_url': strUrl,
			'content_post': strUrlPost,
			'width': 500,
			'height': 200,
			'resizable': true,
			'draggable': false
		});

		var button = [
			{
				title: '<?= GetMessage('SOA_POPUP_SAVE') ?>',
				id: 'saleDeliveryExtraParamsOk',
				'action': function ()
				{
					insertParamsToForm(deliveryId, formName);
					BX.WindowManager.Get().Close();
				}
			},
			BX.CDialog.btnCancel
		];

		paramsDialog.ClearButtons();
		paramsDialog.SetButtons(button);
		//paramsDialog.adjustSizeEx();
		paramsDialog.Show();
	}

	function insertParamsToForm(deliveryId, paramsFormName)
	{
		var orderForm = BX("ORDER_FORM"),
				paramsForm = BX(paramsFormName);
		wrapDivId = deliveryId + "_extra_params";

		var wrapDiv = BX(wrapDivId);
		window.BX.SaleDeliveryExtraParams = {};

		if (wrapDiv)
			wrapDiv.parentNode.removeChild(wrapDiv);

		wrapDiv = BX.create('div', {props: {id: wrapDivId}});

		for (var i = paramsForm.elements.length - 1; i >= 0; i--)
		{
			var input = BX.create('input', {
				props: {
					type: 'hidden',
					name: 'DELIVERY_EXTRA[' + deliveryId + '][' + paramsForm.elements[i].name + ']',
					value: paramsForm.elements[i].value
				}
			}
			);

			window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

			wrapDiv.appendChild(input);
		}

		orderForm.appendChild(wrapDiv);

		BX.onCustomEvent('onSaleDeliveryGetExtraParams', [window.BX.SaleDeliveryExtraParams]);
	}

	if (typeof submitForm === 'function')
		BX.addCustomEvent('onDeliveryExtraServiceValueChange', function () {
			submitForm();
		});
</script>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>" />
<?
if (!empty($arResult["DELIVERY"]))
{
	$width = ($arParams["SHOW_STORES_IMAGES"] == "Y") ? 850 : 700;
	?>
	<input type="hidden" value="ORDER_PROP_<?= ORDER_PROP_FIZ_ADDRESS ?>" id="address_input">
	<input type = "hidden" value = "" id = "edost_office" name = "edost_office">
	<script type = "text/javascript">
		function edost_OpenMap(n) {
			var E = document.getElementById("edost_office_" + n);
			if (E)
				window.open("http://www.edost.ru/office.php?c=" + E.value, "_blank");
		}

		function edost_SetOffice(n) {
			var E = document.getElementById("edost_office_" + n);
			if (E) {
				var E2 = document.getElementById("edost_office");
				if (E2)
					E2.value = E.value;
				if (document.getElementById("ID_DELIVERY_edost_" + n).checked)
					submitForm();
			}
		}
	</script>
	<input type="hidden" value="" id="edost_submit_active" />
	<script type="text/javascript">
		function edost_SubmitActive(n) {
			var E = document.getElementById("edost_submit_active");
			if (E) {
				if (n == "set")
					E.value = "Y";
				else
					return (E.value == "Y" ? true : false);
			}
		}

		function EdostPickPoint(rz) {
			if (edost_SubmitActive("get"))
				return false;

			var s = (rz['name'].substr(0, 3) == "ПВЗ" ? "Пункт выдачи" : "Постамат") + " PickPoint: ";

			var i = rz['address'].indexOf("Российская Федерация");
			if (i > 0)
				rz['address'] = rz['address'].substr(i + 22);
			var s2 = rz['name'];
			var i = s2.indexOf(":");
			if (i > 0)
				s2 = s2.substr(i + 1).replace(/^\s+/g, "");
			s2 = s2.trim();
			if (s2 != "")
				rz['address'] += " (" + s2 + ")";

			rz['id'] = ", код филиала: " + rz['id'];

			document.getElementById(document.getElementById("address_input").value).value = s + rz['address'] + rz['id'];

			var E = document.getElementById("EdostPickPointRef");
			if (E)
				E.innerHTML = "обработка...";

			var E = document.getElementById("edost_office");
			if (E)
				E.value = "pickpoint";

			var E = document.getElementById("ID_DELIVERY_edost_57");
			if (E && !E.checked)
				E.checked = true;

			submitForm();
		}

	</script>
	<?
	$ind = 0;

	foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
	{
		/*
		  if ($arDelivery['SID'] == "maxipost" || $arDelivery['ID'] == 12)
		  {
		  continue;
		  }
		 * 
		 */
		if ($arDelivery["ISNEEDEXTRAINFO"] == "Y")
			$extraParams = "showExtraParamsDialog('" . $delivery_id . "');";
		else
			$extraParams = "";

		if (count($arDelivery["STORE"]) > 0)
			$clickHandler = "onClick = \"fShowStore('" . $arDelivery["ID"] . "','" . $arParams["SHOW_STORES_IMAGES"] . "','" . $width . "','" . SITE_ID . "')\";";
		else
			$clickHandler = "onClick = \"BX('ID_DELIVERY_ID_" . $arDelivery["ID"] . "').checked=true;" . $extraParams . "submitForm();\"";
		?>
		<div class="form-check<?
		if ($arDelivery["CHECKED"] == "Y")
		{
			echo " selected";
		}
		?>">
			<input type="radio" id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" name="<?= htmlspecialcharsbx($arDelivery["FIELD_NAME"]) ?>" value="<?= $arDelivery["ID"] ?>"<? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?> onchange="submitForm();" />
			<label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"><?= htmlspecialcharsbx($arDelivery["OWN_NAME"]) ?> 
				<?
				if (isset($arDelivery["PRICE"]))
				{
					?>
					<span><?= (strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : number_format($arDelivery["PRICE"], 2, ',', ' ')) ?> <i class="fa fa-rub"></i></span>
					<?
				} elseif (isset($arDelivery["CALCULATE_ERRORS"]))
				{
					ShowError($arDelivery["CALCULATE_ERRORS"]);
				} else
				{
					$APPLICATION->IncludeComponent('bitrix:sale.ajax.delivery.calculator', '', array(
						"NO_AJAX" => $arParams["DELIVERY_NO_AJAX"],
						"DELIVERY_ID" => $delivery_id,
						"ORDER_WEIGHT" => $arResult["ORDER_WEIGHT"],
						"ORDER_PRICE" => $arResult["ORDER_PRICE"],
						"LOCATION_TO" => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
						"LOCATION_ZIP" => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
						"CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
						"ITEMS" => $arResult["BASKET_ITEMS"],
						"EXTRA_PARAMS_CALLBACK" => $extraParams,
						"ORDER_DATA" => $arResult['ORDER_DATA']
							), null, array('HIDE_ICONS' => 'Y'));
				}
				/* if (strlen($arDelivery['PERIOD_TEXT']) > 0) {
				  ?> <?= $arDelivery['PERIOD_TEXT'] ?><?
				  } */
				?>
			</label>
			<?
			if ($arDelivery["CHECKED"] == "Y")
			{
				if (strlen($arDelivery['office']) > 0)
				{
					?>
					<div class="clearfix"></div>
					<div class="form-check__prop">
						<?= $arDelivery['office'] ?>
					</div>
					<?
				}
				if (strlen($arDelivery['office-euroset']) > 0)
				{
					?>
					<div class="clearfix"></div>
					<div class="form-check__prop">
						<?= $arDelivery['office-euroset'] ?>
					</div>
					<?
				}
				if (in_array($arDelivery['ID'], array(DELIVERY_DALLI_PVZ_BOXBERRY_ID, DELIVERY_DALLI_PVZ_SDEK_ID, DELIVERY_DALLI_PVZ_PICKUP_ID, DELIVERY_DALLI_PVZ_DALLI_ID)))
				{
					$strAddress = "";
					$propId = false;
					foreach ($arResult['ORDER_PROP']['USER_PROPS_Y'] as $arProp)
					{
						if ($arProp['CODE'] == "ADDRESS")
						{
							$strAddress = $arProp['VALUE'];
							$propId = $arProp['ID'];
						}
					}
					foreach ($arResult['ORDER_PROP']['USER_PROPS_N'] as $arProp)
					{
						if ($arProp['CODE'] == "ADDRESS")
						{
							$strAddress = $arProp['VALUE'];
							$propId = $arProp['ID'];
						}
					}
					if (strlen($arUserResult['ORDER_PROP'][$propId]) > 0)
					{
						$strAddress = $arUserResult['ORDER_PROP'][$propId];
					}
				}
				if ($arDelivery['ID'] == DELIVERY_DALLI_PVZ_BOXBERRY_ID)
				{
					?>
					<div class="clearfix"></div>
					<div class="form-check__prop">
						<?= CDeliveryServiceDalliDriver::GetLinkPVZBoxberry($arResult["USER_VALS"]["DELIVERY_LOCATION"], $strAddress) ?>
					</div>
					<?
				} elseif ($arDelivery['ID'] == DELIVERY_DALLI_PVZ_SDEK_ID)
				{
					?>
					<div class="clearfix"></div>
					<div class="form-check__prop">
						<?= CDeliveryServiceDalliDriver::GetLinkPVZSdek($arResult["USER_VALS"]["DELIVERY_LOCATION"], $strAddress) ?>
					</div>
					<?
				} elseif ($arDelivery['ID'] == DELIVERY_DALLI_PVZ_PICKUP_ID)
				{
					?>
					<div class="clearfix"></div>
					<div class="form-check__prop">
						<?= CDeliveryServiceDalliDriver::GetLinkPVZPickup($arResult["USER_VALS"]["DELIVERY_LOCATION"], $strAddress) ?>
					</div>
					<?
				} elseif ($arDelivery['ID'] == DELIVERY_DALLI_PVZ_DALLI_ID)
				{
					?>
					<div class="clearfix"></div>
					<div class="form-check__prop">
						<?= CDeliveryServiceDalliDriver::GetLinkPVZDalli($arResult["USER_VALS"]["DELIVERY_LOCATION"], $strAddress) ?>
					</div>
					<?
				}
				?>
				<div class="clearfix"></div>
				<?
			}
			if (strlen(trim(strip_tags($arDelivery["DESCRIPTION"]))) > 0)
			{
				?>
				<div class="form-check__prop">
					<?= $arDelivery["DESCRIPTION"] ?>
				</div>
				<?
			}
			if ($arDelivery["CHECKED"] == "Y")
			{
				?>
				<div class="clearfix"></div>
				<?
				if (in_array($arDelivery['ID'], array(DELIVERY_DALLI_COURIER_MSK_ID, DELIVERY_DALLI_COURIER_MO_ID, DELIVERY_DALLI_COURIER_SDEK_ID, DELIVERY_DALLI_COURIER_SPB_ID, DELIVERY_ICOURIER1_ID, DELIVERY_ICOURIER2_ID)))
				{
					include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery_date_dalli.php");
				} else
				{
					include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery_date_pvz.php");
				}
			}
			?>
		</div>
		<?
		/*
		  <div class="row row-delivery<?= ($ind == 0 ? " row-delivery-first" : "") ?>">
		  <div class="col-md-6 col-md-offset-8">
		  <input type="radio" id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" name="<?= htmlspecialcharsbx($arDelivery["FIELD_NAME"]) ?>" value="<?= $arDelivery["ID"] ?>"<? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?> class="styler" onchange="submitForm();" />
		  </div>
		  <div class="col-md-100 col-md-offset-1">
		  <label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>">
		  <div class="bx_description">
		  <div class="name" <?= $clickHandler ?>><?= htmlspecialcharsbx($arDelivery["OWN_NAME"]) ?></div>
		  <span class="bx_result_price">
		  <?
		  if (isset($arDelivery["PRICE"])) {
		  ?>
		  Стоимость: <?= (strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : number_format($arDelivery["PRICE"], 2, ',', ' ')) ?>
		  <?
		  } elseif (isset($arDelivery["CALCULATE_ERRORS"])) {
		  ShowError($arDelivery["CALCULATE_ERRORS"]);
		  } else {
		  $APPLICATION->IncludeComponent('bitrix:sale.ajax.delivery.calculator', '', array(
		  "NO_AJAX" => $arParams["DELIVERY_NO_AJAX"],
		  "DELIVERY_ID" => $delivery_id,
		  "ORDER_WEIGHT" => $arResult["ORDER_WEIGHT"],
		  "ORDER_PRICE" => $arResult["ORDER_PRICE"],
		  "LOCATION_TO" => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
		  "LOCATION_ZIP" => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
		  "CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
		  "ITEMS" => $arResult["BASKET_ITEMS"],
		  "EXTRA_PARAMS_CALLBACK" => $extraParams,
		  "ORDER_DATA" => $arResult['ORDER_DATA']
		  ), null, array('HIDE_ICONS' => 'Y'));
		  }
		  if (strlen($arDelivery['PERIOD_TEXT']) > 0) {
		  ?> <?= $arDelivery['PERIOD_TEXT'] ?><?
		  }
		  ?>
		  </span>

		  <? /*
		  <div <?= $clickHandler ?> class="description">
		  <?
		  if (strlen($arDelivery["DESCRIPTION"]) > 0)
		  echo htmlspecialchars_decode($arDelivery["DESCRIPTION"]);
		  ?>
		  </div> *//* ?>
		  </div>
		  </label>
		  </div>
		  <div class="clearfix"></div>
		  </div>
		  <div class="clearfix"></div>
		  <?
		 */
		$ind++;
	}
}