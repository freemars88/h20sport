<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
global $SHOW_ORDER_PROPERTIES;
$SHOW_ORDER_PROPERTIES = array();
$APPLICATION->AddHeadScript("//pickpoint.ru/select/postamat.js");
$APPLICATION->AddHeadScript("//api-maps.yandex.ru/2.1/?lang=ru_RU");
$APPLICATION->AddHeadScript("https://pd.softclub.ru/api/deliverypoints");
if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if (strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href = '<?= CUtil::JSEscape($arResult["REDIRECT_URL"]) ?>';
			</script>
			<?
			die();
		}
	}
}
?>
<a name="order_form"></a>

<div id="order_form_div" class="order-checkout">
	<NOSCRIPT>
	<div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
	</NOSCRIPT>
	<div style="display: none;"><div id="errorMessages"></div></div>
	<div class="bx_order_make">
		<?
		if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
		{
			if (!empty($arResult["ERROR"]))
			{
				foreach ($arResult["ERROR"] as $v)
					echo ShowError($v);
			} elseif (!empty($arResult["OK_MESSAGE"]))
			{
				foreach ($arResult["OK_MESSAGE"] as $v)
					echo ShowNote($v);
			}

			include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
		} else
		{
			if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
			{
				if (strlen($arResult["REDIRECT_URL"]) == 0)
				{
					include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
				}
			} else
			{
				?>
				<script type="text/javascript">

		<?
		if (CSaleLocation::isLocationProEnabled())
		{
// spike: for children of cities we place this prompt
			$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
			?>

						BX.saleOrderAjax.init(<?=
			CUtil::PhpToJSObject(array(
				'source' => $this->__component->getPath() . '/get.php',
				'cityTypeId' => intval($city['ID']),
				'messages' => array(
					'otherLocation' => '--- ' . GetMessage('SOA_OTHER_LOCATION'),
					'moreInfoLocation' => '--- ' . GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
					'notFoundPrompt' => '<div class="-bx-popup-special-prompt">' . GetMessage('SOA_LOCATION_NOT_FOUND') . '.<br />' . GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
						'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
						'#ANCHOR_END#' => '</a>'
					)) . '</div>'
				)
			))
			?>);

		<? } ?>

					var BXFormPosting = false;
					var doUpdateAjaxEvents = false;
					var map = null, map_created = false;
					var isInitKladr = false;
		<? /* var eurosetPointsLoc =<?= CUtil::PhpToJSObject(CWebavkEuroset::GetPointsToLocations()) ?>; */ ?>

					function submitForm(val)
					{
						$("#ORDER_FORM").validate().destroy();
						if (BXFormPosting === true)
							return true;
						if (val == 'Y')
						{
							if ($(".btn-order-save").is(".btn-disabled"))
							{
								return false;
							}
							if ($("#ID_DELIVERY_ID_22").is(":checked"))
							{
								if ($("input[data-code='ADDRESS']").val() == "")
								{
									$("#seladdr-boxberry").trigger("click");
									return false;
								}
							}
							if ($("#ID_DELIVERY_ID_18").is(":checked"))
							{
								if ($("input[data-code='ADDRESS']").val() == "")
								{
									$("#seladdr-ds").trigger("click");
									return false;
								}
							}
							if ($("#ID_DELIVERY_ID_19").is(":checked"))
							{
								if ($("input[data-code='ADDRESS']").val() == "")
								{
									$("#seladdr-sdek").trigger("click");
									return false;
								}
							}
							if ($("#ID_DELIVERY_ID_30").is(":checked"))
							{
								if ($("input[data-code='ADDRESS']").val() == "")
								{
									$("#seladdr-pickup").trigger("click");
									return false;
								}
							}
						}
						map = null;
						map_created = false;
						BXFormPosting = true;
						if (val != 'Y')
							BX('confirmorder').value = 'N';

						if (val == "Y")
						{
		<? /* if (isPickPoint() || isDeliveryPickup())
		  {
		  $("#ORDER_FORM").validate({
		  errorLabelContainer: "#errorMessages",
		  focusInvalid: true,
		  rules: {
		  "ORDER_PROP_1": {
		  required: true
		  },
		  "ORDER_PROP_20": {
		  required: true
		  },
		  "ORDER_PROP_3": {
		  required: true
		  },
		  "ORDER_PROP_2": {
		  required: true,
		  email: true
		  }
		  },
		  invalidHandler: function (event, validator) {

		  var errors = validator.numberOfInvalids();
		  if (errors) {
		  $("body:not(:animated)").animate({scrollTop: $(".row-header").offset().top}, 1000);
		  $("html").animate({scrollTop: $(".row-header").offset().top}, 1000);
		  }
		  }
		  });
		  } else { */ ?>

		<? /* } */
		?>
							if (!$("#ORDER_FORM").valid())
							{
								BXFormPosting = false;
								return false;
							}
						} else {
							$("#ORDER_FORM").validate().destroy();
						}
						var orderForm = BX('ORDER_FORM');
						//$("#ORDER_FORM").css("opacity", 0.5);
						BX.showWait(orderForm);

		<?
		if (CSaleLocation::isLocationProEnabled())
		{
			?>
							BX.saleOrderAjax.cleanUp();
		<? } ?>
						doUpdateAjaxEvents = true;
						isInitKladr = false;
						BX.ajax.submit(orderForm, ajaxResult);

						return true;
					}

					function ajaxResult(res)
					{
						var orderForm = BX('ORDER_FORM');
						try
						{
							// if json came, it obviously a successfull order submit

							var json = JSON.parse(res);
							$("#ORDER_FORM").css("opacity", 1);
							//BX.closeWait();

							if (json.error)
							{
								BXFormPosting = false;
								return;
							} else if (json.redirect)
							{
								window.top.location.href = json.redirect;
							}
						} catch (e)
						{
							// json parse failed, so it is a simple chunk of html

							BXFormPosting = false;
							BX('order_form_content').innerHTML = res;

		<? if (CSaleLocation::isLocationProEnabled()): ?>
								BX.saleOrderAjax.initDeferredControl();
		<? endif ?>
						}

						//BX.closeWait();
						$("#ORDER_FORM").css("opacity", 1);
						BX.onCustomEvent(orderForm, 'onAjaxSuccess');
					}

					function SetContact(profileId)
					{
						BX("profile_change").value = "Y";
						submitForm();
					}
					BX.addCustomEvent('onAjaxSuccess', function () {
						BX.closeWait();
						if (doUpdateAjaxEvents)
						{
							doInitOrderForm();
							doInitKladr();
							doUpdateAjaxEvents = false;
						}
					});
		<? /*
		  function doCheckEurosetDelivery()
		  {
		  if ($("#ID_DELIVERY_ID_152").is(":checked"))
		  {
		  $("#address_input, input[data-code='ADDRESS']").val("");
		  $(".selectEurosetButton").html("Выбрать салон");
		  }
		  } */ ?>
					function doInitOrderForm()
					{
						$("#ORDER_FORM").validate({
							errorLabelContainer: "#errorMessages",
							focusInvalid: true,
							rules: {
								"ORDER_PROP_<?= ORDER_PROP_FIZ_FIO ?>": {
									required: true
								},
								"ORDER_PROP_<?= ORDER_PROP_FIZ_PHONE ?>": {
									required: true
								},
								"ORDER_PROP_<?= ORDER_PROP_FIZ_EMAIL ?>": {
									required: true,
									email: true
								}<? /* ,
		  "ORDER_PROP_21": {
		  required: true
		  },
		  "ORDER_PROP_22": {
		  required: true
		  } */ ?>
							},
							invalidHandler: function (event, validator) {
								var errors = validator.numberOfInvalids();
								if (errors)
								{
									$("body:not(:animated)").animate({
										scrollTop: $(".row-header").offset().top}, 1000);
									$("html").animate({
										scrollTop: $(".row-header").offset().top}, 1000);
								}
							}
						});
						$("#ORDER_FORM input").blur(function () {
							$("#ORDER_FORM").valid()
						});
						/*$("#ORDER_FORM .styler").styler({
						 selectPlaceholder: "(выберите)"
						 });
						 */
						/*
						 $("#ORDER_PROP_3").keydown(function (event) {
						 var objPress = $("input[rel='phone-press']");
						 if (event.key != undefined)
						 {
						 objPress.val(objPress.val() + '[' + event.key + ']');
						 } else if (event.keyCode) {
						 objPress.val(objPress.val() + '[' + String.fromCharCode(event.keyCode) + ']');
						 } else if (event.which)
						 {
						 objPress.val(objPress.val() + '[' + String.fromCharCode(event.which) + ']');
						 }
						 while (objPress.val().length > 255)
						 {
						 objPress.val(objPress.val().substr(objPress.val().indexOf("]") + 1));
						 }
						 });
						 */
						$("#ORDER_PROP_<?= ORDER_PROP_FIZ_PHONE ?>").mask("+7 (999) 999-99-99");
						if ($("input[name='DELIVERY_ID']:checked").length <= 0)
						{
							$("input[name='DELIVERY_ID']:first").attr("checked", "checked");
							$("input[name='DELIVERY_ID']:first").trigger("click");
						}

						if ($("#ID_DELIVERY_ID_22").is(":checked"))
						{
							if ($("#seladdr-boxberry").data("issel") != "Y")
							{
								$("input[data-code='ADDRESS']").val("");
							}
						}
						if ($("#ID_DELIVERY_ID_18").is(":checked"))
						{
							if ($("#seladdr-ds").data("issel") != "Y")
							{
								$("input[data-code='ADDRESS']").val("");
							}
						}
						if ($("#ID_DELIVERY_ID_19").is(":checked"))
						{
							if ($("#seladdr-sdek").data("issel") != "Y")
							{
								$("input[data-code='ADDRESS']").val("");
							}
						}
						if ($("#ID_DELIVERY_ID_30").is(":checked"))
						{
							if ($("#seladdr-pickup").data("issel") != "Y")
							{
								$("input[data-code='ADDRESS']").val("");
							}
						}

						/*
						 $("#seladdr-boxberry").click(function () {
						 console.log(arBoxberryPoints);
						 //showWidgetPVZDalli($(this).data("pvz"), "BOXBERRY", "N");
						 return false;
						 });
						 */
		<? /*
		  $("a.selectEurosetButton").click(function () {
		  $("#button-euroset").trigger("click");
		  //$('.shadowBlock').fadeIn(300);
		  //$(".selectEuroset.modalBox").fadeIn(300);
		  //$("#selectEurosetMap").height("auto");
		  return false;
		  }); */ ?>
						doInitDeliveryDate();
					}

					function doInitDeliveryDate()
					{
						if ($("select#F_DELIVERY_DATE").length > 0)
						{
							$("#div_F_DELIVERY_DATE").datepicker({
								showOtherMonths: true,
								beforeShowDay: function (checkDay) {
									if ($("select#F_DELIVERY_DATE option[value='" + $.datepicker.formatDate('dd.mm.yy', checkDay) + "']").length > 0)
									{
										return [true, ''];
									}
									return [false, ''];
								},
								onSelect: function (dateText) {
									$("select#F_DELIVERY_DATE").val(dateText);
									submitForm();
								},
								defaultDate: $.datepicker.parseDate("dd.mm.yy", $("select#F_DELIVERY_DATE").val())
							});
							$("#div_F_DELIVERY_DATE").datepicker("option", $.datepicker.regional["ru"]);
						}
					}

					function doInitKladr() {
						var $region = $('[name="sys-location-region"]'),
								$city = $('[name="sys-location-city"]'),
								$street = $('[data-code="STREET"]'),
								$building = $('[data-code="HOUSE"]');
						var bInitedCity = true;
						var bInitedStreet = false;
						var bInitedBuilding = false;
						$.kladr.setDefault({
							token: '<?= KLADR_API_TOKEN ?>',
							parentInput: '#ORDER_FORM',
							verify: true,
							labelFormat: function (obj, query) {
								var label = '';

								var name = obj.name.toLowerCase();
								query = query.name.toLowerCase();

								var start = name.indexOf(query);
								start = start > 0 ? start : 0;

								if (obj.typeShort) {
									label += obj.typeShort + '. ';
								}

								if (query.length < obj.name.length) {
									label += obj.name.substr(0, start);
									label += '<strong>' + obj.name.substr(start, query.length) + '</strong>';
									label += obj.name.substr(start + query.length, obj.name.length - query.length - start);
								} else {
									label += '<strong>' + obj.name + '</strong>';
								}

								if (obj.parents) {
									for (var k = obj.parents.length - 1; k > -1; k--) {
										var parent = obj.parents[k];
										if (parent.name) {
											if (label)
												label += '<small>, </small>';
											label += '<small>' + parent.name + ' ' + parent.typeShort + '.</small>';
										}
									}
								}

								return label;
							},
							change: function (obj) {
								if (obj) {
									setLabel($(this), obj.type);
								}

								if (!bInitedCity && $('[name="sys-location-city"]').val() != "")
								{
									$('[name="sys-location-city"]').trigger("change");
									bInitedCity = true;
									return;
								} else if (!bInitedCity)
								{
									bInitedCity = true;
									return;
								}
								if (!bInitedStreet && $('[data-code="STREET"]').val() != "")
								{
									$('[data-code="STREET"]').trigger("change");
									bInitedStreet = true;
									return;
								} else if (!bInitedStreet)
								{
									bInitedStreet = true;
									return;
								}
								if (obj && obj.zip != undefined && obj.zip != "")
								{
									$("input[data-code='ZIP']").val(obj.zip);
								}
								addressUpdate();
								mapUpdate();
							},
							checkBefore: function () {
								var $input = $(this);

								if (!$.trim($input.val())) {
									addressUpdate();
									mapUpdate();
									return false;
								}
							}
						});

						$region.kladr('type', $.kladr.type.region);
						//$district.kladr('type', $.kladr.type.district);
						$city.kladr('type', $.kladr.type.city);
						$street.kladr('type', $.kladr.type.street);
						$building.kladr('type', $.kladr.type.building);

						// Включаем получение родительских объектов для населённых пунктов
						$city.kladr('withParents', true);
						$street.kladr('withParents', true);

						// Отключаем проверку введённых данных для строений
						$building.kladr('verify', false);
						ymaps.ready(function () {
							if (map_created)
								return;
							map_created = true;

							map = new ymaps.Map('map', {
								center: [55.76, 37.64],
								zoom: 12,
								controls: []
							});

							map.controls.add('zoomControl', {
								position: {
									right: 10,
									top: 10
								}
							});
						});
						if ($('[name="sys-location-region"]').val() != "")
						{
							$('[name="sys-location-region"]').trigger("change");
							bInitedCity = false;
						} else if ($('[name="sys-location-city"]').val() != "")
						{
							$('[name="sys-location-city"]').trigger("change");
						}
					}

					function addressUpdate() {
						var address = $.kladr.getAddress('#ORDER_FORM');
						var flat = $("[data-code='FLAT']").val();
						if (flat.length > 0)
						{
							address += ", кв. " + flat;
						}
						if (!$("#ID_DELIVERY_ID_22").is(":checked") && !$("#ID_DELIVERY_ID_18").is(":checked") && !$("#ID_DELIVERY_ID_19").is(":checked") && !$("#ID_DELIVERY_ID_30").is(":checked"))
						{
							$('[name="' + $('#address_input').val() + '"]').val(address);
						}
					}

					function setLabel($input, text) {
						text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
						$input.parent().find('label').text(text);
					}

					function mapUpdate() {
						var zoom = 4;

						var address = $.kladr.getAddress('#ORDER_FORM', function (objs) {
							var result = '';

							$.each(objs, function (i, obj) {
								var name = '',
										type = '';

								if ($.type(obj) === 'object') {
									name = obj.name;
									type = ' ' + obj.type;

									switch (obj.contentType) {
										case $.kladr.type.region:
											zoom = 4;
											break;

										case $.kladr.type.district:
											zoom = 7;
											break;

										case $.kladr.type.city:
											zoom = 10;
											break;

										case $.kladr.type.street:
											zoom = 13;
											break;

										case $.kladr.type.building:
											zoom = 16;
											break;
									}
								} else {
									name = obj;
								}

								if (result)
									result += ', ';
								result += type + name;
							});

							return result;
						});

						if (address && map_created) {
							var geocode = ymaps.geocode(address);
							geocode.then(function (res) {
								map.geoObjects.each(function (geoObject) {
									map.geoObjects.remove(geoObject);
								});

								var position = res.geoObjects.get(0).geometry.getCoordinates(),
										placemark = new ymaps.Placemark(position, {}, {});

								map.geoObjects.add(placemark);
								map.setCenter(position, zoom);
							});
						}
					}
		<? /*
		  function isPickPoint()
		  {
		  return ($("#ID_DELIVERY_ID_68:checked").length > 0);
		  }

		  function isDeliveryPickup() {
		  return ($("#ID_DELIVERY_ID_2:checked").length > 0);
		  }

		  window.onEurosetPointSelected = function (deliveryPoint) {
		  console.log(deliveryPoint);
		  var strAddress = "Салон " + deliveryPoint.deliveryOperator + ": [" + deliveryPoint.shopId + "] " + deliveryPoint.region + ", " + deliveryPoint.city + ", " + deliveryPoint.address;
		  $("#address_input, input[data-code='ADDRESS']").val(strAddress);
		  $(".selectEurosetButton").html(strAddress);
		  //$('.shadowBlock').hide();
		  //$(".selectEuroset.modalBox").hide();
		  $("#modal-selectEuroset .js-modal-close").trigger("click");
		  if (eurosetPointsLoc[deliveryPoint.shopId] != undefined && eurosetPointsLoc[deliveryPoint.shopId] != $("#ORDER_PROP_6").val())
		  {
		  $("#ORDER_PROP_6").val(eurosetPointsLoc[deliveryPoint.shopId]);
		  submitForm("N");
		  }
		  }
		 */ ?>
					$(document).ready(function () {
						//submitForm();
						doInitOrderForm();
						doInitKladr();
		<? /*
		  window.mapWidget = new MapWidget(<?= EUROSET_PARTNERID ?>, "selectEurosetMap", onEurosetPointSelected, {height: 530, size: 'xs'});
		  window.mapWidget.init();
		 * 
		 */ ?>
					});
					function showWidgetPVZDalli(pvzObj, partner, showSuccessPvzWrite) {
						if (partner == 'DS')
							partner = 'DALLI-SERVICE';
						var partnersblock = '<div class="partnerblock"><div class="partner_img"><img src="/bitrix/images/dalliservicecom.delivery/' + partner + '.png"></div><div class="partner_delivery_info">';
						/*if (typeof pvzObj.deliveryPrice != 'undefined' && pvzObj.deliveryPrice.length > 0)
						 partnersblock += '<div class="delivery_info_row"><div class="vidget_icon vidget_cost"></div><div class="ds_price">' + pvzObj.deliveryPrice + '</div></div>';
						 if (typeof pvzObj.deliveryPeriod != 'undefined' && pvzObj.deliveryPeriod.length > 0)
						 partnersblock += '<div class="delivery_info_row"><div class="vidget_icon vidget_delivery"></div><div class="ds_date">' + pvzObj.deliveryPeriod + '</div></div>';*/
						partnersblock = partnersblock + '</div></div><div id="partners_info"></div>';

						vidgetContent = '<div id="ds_vidget_sidebar"><div class="sidebar_wrapper">' + partnersblock + '</div><button disabled id="confirmChooze" onclick="confirmChooze(showSuccessPvzWrite_)">' + BX.message('CHOOZE') + '</button></div><div id="ds_vidget_map"></div>';

						DsPvzVidget = new BX.PopupWindow("ds_pvz_popup", null, {
							content: vidgetContent,
							closeIcon: {right: "-5px", top: "-5px"},
							zIndex: 0,
							autoHide: true,
							offsetLeft: 0,
							lightShadow: true,
							closeByEsc: true,
							offsetTop: 0,
							draggable: {restrict: false},
							overlay: {backgroundColor: 'grey', opacity: '30'},
							events: {
								onPopupClose: function () {
									DsPvzVidget.destroy();
								},
								onAfterPopupShow: function () {
									showSuccessPvzWrite_ = showSuccessPvzWrite;
								}

							}
						});

						// var myArray = [ 3, 5, 7, 9, 4, 8, 2, 1, 6 ];
						//myArray.forEach( function(item){ alert(item); } );

						ymaps.ready(init);
						function init() {
							center =
									myMap = new ymaps.Map("ds_vidget_map", {
										center: [50, 50],
										zoom: 15,
										controls: ['zoomControl', 'typeSelector', 'routeEditor', 'trafficControl']
									});
							if (typeof pvzObj != 'undefined') {
								myCollection = new ymaps.GeoObjectCollection();
								$.each(pvzObj, function (index, value) {
									var balloonContent = '';
									if (typeof value.ADDRESS != 'undefined' && value.ADDRESS.length > 0) {
										balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>' + value.ADDRESS + '</div>';
									}
									if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0) {
										balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div><div>' + value.PHONE + '</div></div>';
									}
									if (typeof value.WORK_SHEDULE != 'undefined' && value.WORK_SHEDULE.length > 0) {
										balloonContent = balloonContent + '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>' + value.WORK_SHEDULE + '</div>';
									}
									if (typeof value.GPS != 'undefined') {
										arrCoords = value.GPS.split(',')
									}

									var PVZPoint = new ymaps.Placemark([arrCoords['0'], arrCoords['1']], {
										hintContent: value.address,
										balloonContent: balloonContent
									}, {
										iconLayout: 'default#image',
									});
									/*PVZPoint.events.add('balloonclose', function(e) {
									 document.getElementById('confirmChooze').setAttribute("disabled", "disabled");
									 $('#confirmChooze').fadeTo(0, 0.5);
									 partners_info.innerHTML = '';
									 });*/
									PVZPoint.events.add('click', function (e) {
										if (typeof value.ADDRESS != 'undefined' && value.ADDRESS.length > 0)
											address = '<div class="vidget_desc_row"><div class="vidget_icon vidget_address"></div>' + value.ADDRESS + '</div>';
										else
											address = '';
										if (typeof value.PHONE != 'undefined' && value.PHONE.length > 0)
											phone = '<div class="vidget_desc_row"><div class="vidget_icon vidget_phone"></div>' + value.PHONE + '</div>';
										else
											phone = '';
										if (typeof value.WORK_SHEDULE != 'undefined' && value.WORK_SHEDULE.length > 0)
											shedule = '<div class="vidget_desc_row"><div class="vidget_icon vidget_workshedule"></div>' + value.WORK_SHEDULE + '</div>';
										else
											shedule = '';
										if (typeof value.DESCRIPTION != 'undefined' && value.DESCRIPTION.length > 0)
											description = '<div class="vidget_desc_row"><div class="vidget_icon vidget_description"></div>' + value.DESCRIPTION + '</div>';
										else
											description = '';
										if (typeof value.PARTNER != 'undefined' && value.PARTNER.length > 0)
											partnername = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_partnername"></div>' + value.PARTNER + '</div>';
										else
											partnername = '';
										if (typeof value.PARTNERID != 'undefined' && value.PARTNERID.length > 0)
											partnerid = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_partnerid"></div>' + value.PARTNERID + '</div>';
										else
											partnerid = '';
										if (typeof value.ID != 'undefined' && value.ID.length > 0)
											id = '<div class="vidget_desc_row" style="display:none;"><div class="vidget_icon vidget_id"></div>' + value.ID + '</div>';
										else
											id = '';
										content = address + phone + shedule + description + partnername + id;
										partners_info.innerHTML = content;
										document.getElementById('confirmChooze').removeAttribute("disabled");
										$('#confirmChooze').fadeTo(0, 1);
									});

									myCollection.add(PVZPoint);
								});
								myMap.geoObjects.add(myCollection);
								myMap.setBounds(myCollection.getBounds(), {checkZoomRange: true}).then(function () {
									if (myMap.getZoom() > 14)
										myMap.setZoom(13);
								});
							}
						}
						DsPvzVidget.show();
					}

					function confirmChooze(showSuccessPvzWrite_) {
						address = strip_tags(address);
						partnername = strip_tags(partnername);
						partnerid = strip_tags(partnerid);
						id = strip_tags(id);
						zip = "";
						zipPos = address.indexOf(",");
						if (zipPos >= 0)
						{
							zip = address.substr(0, zipPos);
						}
						$("input[data-code='ZIP']").val(zip);
						fullAddress = partnername + ": [" + id + "] " + address;
						DsPvzVidget.destroy();
						$("#seladdr-boxberry").data("issel", "N");
						$("#seladdr-sdek").data("issel", "N");
						$("#seladdr-pickup").data("issel", "N");
						$("#seladdr-ds").data("issel", "N");
						if (partnerid == "BOXBERRY")
						{
							$("#seladdr-boxberry").html(fullAddress);
							$("#seladdr-boxberry").data("issel", "Y");
							$("input[name='ORDER_PROP_<?= ORDER_PROP_FIZ_ADDRESS ?>']").val(fullAddress);
						} else if (partnerid == "SDEK")
						{
							$("#seladdr-sdek").html(fullAddress);
							$("#seladdr-sdek").data("issel", "Y");
							$("input[name='ORDER_PROP_<?= ORDER_PROP_FIZ_ADDRESS ?>']").val(fullAddress);
						} else if (partnerid == "PICKUP")
						{
							$("#seladdr-pickup").html(fullAddress);
							$("#seladdr-pickup").data("issel", "Y");
							$("input[name='ORDER_PROP_<?= ORDER_PROP_FIZ_ADDRESS ?>']").val(fullAddress);
						} else if (partnerid == "DS")
						{
							$("#seladdr-ds").html(fullAddress);
							$("#seladdr-ds").data("issel", "Y");
							$("input[name='ORDER_PROP_<?= ORDER_PROP_FIZ_ADDRESS ?>']").val(fullAddress);
						}
						return false;
					}

				</script>
				<?
				if ($_POST["is_ajax_post"] != "Y")
				{
					?>
					<form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" class="checkout-form" enctype="multipart/form-data">
						<?= bitrix_sessid_post() ?>
						<div id="order_form_content">
							<?
						} else
						{
							$APPLICATION->RestartBuffer();
						}

						if ($_REQUEST['PERMANENT_MODE_STEPS'] == 1)
						{
							?>
							<input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
							<?
						}

						if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
						{
							foreach ($arResult["ERROR"] as $v)
								echo ShowError($v);
							?>
							<script type="text/javascript">
								top.BX.scrollToNode(top.BX('ORDER_FORM'));
							</script>
							<?
						}
						?>

						<div class="row">
							<div class="col-md-8">
								<div class="block billing-details">
									<h4 class="widget-title">ИНФОРМАЦИЯ О ВАС</h4>
									<? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php"); ?>
									<? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props.php"); ?>
								</div>
								<div class="block">
									<h4 class="widget-title">Способ доставки</h4>
									<? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php"); ?>
								</div>
								<div class="block">
									<h4 class="widget-title">Способ оплаты</h4>
									<? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php"); ?>
									<?
									if (getCountProductsInBasket() <= 4)
									{
										?>
										<label class="b-offer-confirm"><input type="checkbox" name="" id="offerconfirm" value="Y" />&nbsp;&nbsp;Согласен(на) с условиями <a href="/offer.php" target="_blank">Пользовательского соглашения</a></label>
										<div class="clearfix"></div>
										<a href="javascript:void(0)" class="btn btn-main mt-20 btn-order-save btn-disabled" onclick="submitForm('Y');
															return false;">Оформить заказ</a>
										   <?
									   } else
									   {
										   ?>
										<p id="checkout-text">В одном заказе может быть не более 4 товаров</p>
										<?
									}
									?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="product-checkout-details">
									<div class="block">
										<h4 class="widget-title">Проверка данных</h4>
										<? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/summary.php"); ?>
										<ul class="summary-prices">
											<li>
												<span>Стоимость товаров:</span>
												<span class="price"><?= $arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT'] ?></span>
											</li>
											<li>
												<span>Доставка:</span>
												<span><?= ($arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE'] > 0 ? $arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE_FORMATED'] : "Бесплатно") ?></span>
											</li>
											<?
											if ($arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY_SHOW'] == "Y")
											{
												?>
												<li>
													<span>До бесплатной доставки осталось:</span>
													<span><?= $arResult['JS_DATA']['TOTAL']['PRICE_FOR_FREE_DELIVERY'] ?></span>
												</li>
											<? } ?>
										</ul>
										<div class="summary-total">
											<span>Стоимость заказа</span>
											<span><?= $arResult['ORDER_TOTAL_PRICE_FORMATED'] ?></span>
										</div>
										<div id="map" class="panel-map"></div>
										<? /*
										  <div class="b-pane__text">
										  <ul>
										  <li><span><?= $arResult['PROP_BY_CODE']['FAMALY'] ?> <?= $arResult['PROP_BY_CODE']['NAME'] ?></span></li>
										  <li><span><?= $arResult['PROP_BY_CODE']['LOCATION_NAME'] ?>, <?= ($arResult['ORDER_DATA']['PERSON_TYPE_ID'] == 1 ? "розничный покупатель" : "юридическое лицо") ?></span></li>
										  <li><span><?= $arResult['PROP_BY_CODE']['PHONE'] ?></span></li>
										  <li><span><?= $arResult['PROP_BY_CODE']['EMAIL'] ?></span></li>
										  <li><span><?= count($arResult['BASKET_ITEMS']) ?> <?= GetPadez(count($arResult['BASKET_ITEMS']), "товар", "товара", "товаров") ?></span></li>
										  </ul>
										  </div>
										  <div class="b-pane__content">

										  <div class="b-pane__price">
										  <span><?= $arResult['ORDER_TOTAL_PRICE_FORMATED'] ?> <i class="fa fa-rub"></i></span>
										  </div>
										  <div class="b-pane__controls">
										  <div class="b-pane__submit">
										  <input type="button" class="b-button" value="заказать" onclick="submitForm('Y');
										  return false;" />
										  </div>
										  </div>
										  </div>
										 */ ?>
									</div>
								</div>
							</div>
						</div>
						<?
						/*
						  <div id="bx-soa-order">
						  <div class="bx-soa">
						  <div class="bx-soa-section bx-soa-section-nobb">
						  <div class="row row-header">
						  <div class="col-md-110 col-md-offset-4">
						  <h2>Информация о вас</h2>
						  </div>
						  </div>
						  <? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php"); ?>
						  <div class="row">
						  <div class="col-md-59">
						  <? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props.php"); ?>
						  <div class="bx-soa-section">
						  <div class="row">
						  <div class="col-md-110 col-md-offset-8">
						  <h3>Доставка</h3>
						  </div>
						  </div>
						  <? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php"); ?>
						  </div>
						  </div>
						  <div class="col-md-59 col-md-offset-2">
						  <div class="row row-prop">
						  <div class="col-md-120">
						  <div id="map" class="panel-map"></div>
						  </div>
						  </div>
						  </div>
						  </div>
						  </div>
						  </div>
						  <div class="bx-soa">
						  <div class="bx-soa-section">
						  <div class="row">
						  <div class="col-md-110 col-md-offset-4">
						  <h3>Оплата</h3>
						  </div>
						  </div>
						  <? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php"); ?>
						  </div>
						  </div>
						  <div class="bx-soa-total">
						  <div class="bx-soa-cart-total">
						  <a href="javascript:void(0)" class="btn btn-default btn-lg btn-order-save" onclick="submitForm('Y'); return false;">Оформить заказ на сумму - <?= $arResult['ORDER_TOTAL_PRICE_FORMATED'] ?></a>
						  </div>
						  </div>
						  </div>
						  <? */
						//include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/summary.php");
						if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
							echo $arResult["PREPAY_ADIT_FIELDS"];
						?>

						<?
						if ($_POST["is_ajax_post"] != "Y")
						{
							?>
						</div>
						<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
						<input type="hidden" name="profile_change" id="profile_change" value="N">
						<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
						<input type="hidden" name="json" value="Y">

						<? /* <div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="checkout"><?= GetMessage("SOA_TEMPL_BUTTON") ?></a></div> */ ?>
					</form>
					<?
					if ($arParams["DELIVERY_NO_AJAX"] == "N")
					{
						?>
						<div style="display:none;"><? $APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
						<?
					}
				} else
				{
					?>
					<script type="text/javascript">
						top.BX('confirmorder').value = 'Y';
						top.BX('profile_change').value = 'N';
					</script>
					<?
					die();
				}
			}
		}
		?>
	</div>
</div>

<?
if (CSaleLocation::isLocationProEnabled())
{
	?>

	<div style="display: none">
		<? // we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it  ?>
		<?
		$APPLICATION->IncludeComponent(
				"bitrix:sale.location.selector.steps", ".default", array(
				), false
		);
		?>
		<?
		$APPLICATION->IncludeComponent(
				"bitrix:sale.location.selector.search", ".default", array(
				), false
		);
		?>
	</div>

	<?
}?>