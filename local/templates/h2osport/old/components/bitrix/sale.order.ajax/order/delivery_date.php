<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
CModule::IncludeModule("webavk.deliverydate");
$arElementsID = array();
foreach ($arResult["BASKET_ITEMS"] as $arBasketItem) {
	$arElementsID[$arBasketItem['PRODUCT_ID']] = $arBasketItem['QUANTITY'];
}
/*if ($arResult['ORDER_PRICE'] >= 5000) {
	$arDatesAllowDelivery = CWebavkDeliveryDateTools::GetAllowDeliveryDatesForOrder(6, 30, $arElementsID);
} else {
 * 
 */
	$arDatesAllowDelivery = array();//CWebavkDeliveryDateTools::GetAllowDeliveryDatesForOrder($arResult['USER_VALS']['DELIVERY_ID'], 30, $arElementsID);
//}

$currentDeliveryDate = '';
foreach ($arResult["ORDER_PROP"] as $arOrderProperties) {
	foreach ($arOrderProperties as $arProperties) {
		if ($arProperties["CODE"] == "F_DELIVERY_DATE") {
			foreach ($arDatesAllowDelivery as $curDate) {
				if ($arProperties["VALUE"] == $curDate)
					$currentDeliveryDate = $curDate;
			}
		}
	}
}
$arTimeIntervalsDelivery = array();
if ($currentDeliveryDate == '')
	$currentDeliveryDate = $arDatesAllowDelivery[0];

if (strlen($currentDeliveryDate) > 0) {
	/*if ($arResult['ORDER_PRICE'] >= 5000) {
		$arTimeIntervalsDelivery = CWebavkDeliveryDateTools::GetTimeIntervalsForOrder(6, MakeTimeStamp($currentDeliveryDate), $arElementsID);
	} else {*/
		$arTimeIntervalsDelivery = array();//CWebavkDeliveryDateTools::GetTimeIntervalsForOrder($arResult['USER_VALS']['DELIVERY_ID'], MakeTimeStamp($currentDeliveryDate), $arElementsID);
	//}
}
?>
<script type="text/javascript">
	<!--
	var webavkDeliveryAllowDates =<?= CUtil::PhpToJSObject($arDatesAllowDelivery) ?>;
-->
</script>
<?
if (!empty($arDatesAllowDelivery)) {
	$arPropertyDeliveryDate = false;
	$arPropertyDeliveryTime = false;
	foreach ($arResult["ORDER_PROP"] as $k => $v) {
		foreach ($v as $arProp) {
			if ($arProp["CODE"] == "F_DELIVERY_DATE") {
				$arPropertyDeliveryDate = $arProp;
			} elseif ($arProp["CODE"] == "F_DELIVERY_TIME") {
				$arPropertyDeliveryTime = $arProp;
			}
		}
	}

	$isSel = false;
	foreach ($arDatesAllowDelivery as $curDate) {
		if (trim($arPropertyDeliveryDate["VALUE"]) == trim($curDate)) {
			$isSel = true;
		}
	}
	if (!$isSel) {
		foreach ($arDatesAllowDelivery as $k => $curDate) {
			$arPropertyDeliveryDate["VALUE"] = trim($curDate);
			break;
		}
	}

	$isSel = false;
	foreach ($arTimeIntervalsDelivery as $k => $curTime) {
		if (trim($arPropertyDeliveryTime["VALUE"]) == trim($curTime)) {
			$isSel = true;
		}
	}
	if (!$isSel) {
		foreach ($arTimeIntervalsDelivery as $k => $curTime) {
			$arPropertyDeliveryTime["VALUE"] = trim($curTime);
			break;
		}
	}
	$ar = array();
	if (strlen($arPropertyDeliveryDate['VALUE']) > 0) {
		$ar[] = $arPropertyDeliveryDate['VALUE'];
	}
	if (strlen($arPropertyDeliveryTime['VALUE']) > 0) {
		$ar[] = $arPropertyDeliveryTime['VALUE'];
	}
	?>
	<div class="row delivery-date-row">
		<div class="col-md-5"></div>
		<div class="col-md-114 col-md-offset-1">
			<strong>Желаемая дата и время доставки *:</strong> <?= (count($ar) > 0 ? implode(", ", $ar) : "") ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row delivery-date-row">
		<div class="col-md-5"></div>
		<div class="col-md-45 col-md-offset-1">
			<select name="<?= $arPropertyDeliveryDate["FIELD_NAME"] ?>" id="F_DELIVERY_DATE">
				<?
				foreach ($arDatesAllowDelivery as $curDate) {
					?>
					<option value="<?= $curDate ?>"<? if ($arPropertyDeliveryDate["VALUE"] == $curDate) { ?> selected="selected"<? } ?>><?= $curDate ?></option>
					<?
				}
				?>
			</select>
			<div id="div_F_DELIVERY_DATE" class="order-calendar"></div>
		</div>
		<div class="col-md-45 col-md-offset-5">
			<?
			foreach ($arTimeIntervalsDelivery as $k => $curTime) {
				?>
				<div class="mb10">
					<input type="radio" name="<?= $arPropertyDeliveryTime["FIELD_NAME"] ?>" class="styler deliveryTime" value="<?= trim($curTime) ?>"<? if (trim($arPropertyDeliveryTime["VALUE"]) == trim($curTime)) { ?> checked="checked"<? } ?> id="<?= $arPropertyDeliveryTime["FIELD_NAME"] ?>_<?= intval($k) ?>" /> <label for="<?= $arPropertyDeliveryTime["FIELD_NAME"] ?>_<?= intval($k) ?>"><?= $curTime ?></label>
					<div class="clearfix"></div>
				</div>
				<?
			}
			?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?
}?>