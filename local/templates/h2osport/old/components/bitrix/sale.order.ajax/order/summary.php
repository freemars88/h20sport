<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
foreach ($arResult["GRID"]["ROWS"] as $k => $arData)
{
	if (strlen($arData["data"]["PREVIEW_PICTURE_SRC"]) > 0)
	{
		$url = $arData["data"]["PREVIEW_PICTURE_SRC"];
	} elseif (strlen($arData["data"]["DETAIL_PICTURE_SRC"]) > 0)
	{
		$url = $arData["data"]["DETAIL_PICTURE_SRC"];
	} else
	{
		$url = $templateFolder . "/images/no_photo.png";
	}
	?>
	<div class="media product-card">
		<a class="pull-left" href="<?= $arData["data"]["DETAIL_PAGE_URL"] ?>">
			<img class="media-object" src="<?= $url ?>" alt="<?= $arData["data"]["NAME"] ?>" />
		</a>
		<div class="media-body">
			<h4 class="media-heading"><a href="<?= $arData["data"]["DETAIL_PAGE_URL"] ?>"><?= $arData["data"]["NAME"] ?></a></h4>
			<p class="price"><?= $arData["data"]["QUANTITY"] ?> x <?= $arData["data"]["PRICE_FORMATED"] ?></p>
			<? /* <span class="remove" >Remove</span> */ ?>
		</div>
	</div>
	<?
}
