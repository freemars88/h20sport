<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;

$arData = array();

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0)
{
	$i = 0;
	foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $arItem)
	{
		$arData['items'][$arItem['ID']] = array(
			"DISCOUNT_PRICE" => $arItem['DISCOUNT_PRICE'],
			"PRICE_FORMATED" => $arItem['PRICE_FORMATED'],
			"FULL_PRICE_FORMATED" => $arItem['FULL_PRICE_FORMATED'],
			"QUANTITY" => $arItem['QUANTITY'],
			"TOTAL_SUMM_OLD_FORMATED" => $arItem['TOTAL_SUMM_OLD_FORMATED'],
			"TOTAL_SUMM_FORMATED" => $arItem['TOTAL_SUMM_FORMATED']
		);
	}
} else
{
	$arData['is_reload'] = true;
}
$arData['is_ok'] = true;
$arData['checkout_text'] = "Оформить заказ на сумму - " . str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"]);
$arData['allSum_FORMATED'] = $arResult['allSum_FORMATED'];
$arData['BASKET_CNT'] = $arResult['BASKET_CNT'];
echo json_encode($arData);
