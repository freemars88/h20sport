<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult))
{
	return;
}
?>

<ul class="nav navbar-nav">
	<?
	$iIndex = 0;
	foreach ($arResult['ITEMS'] as $arItem)
	{
		$iIndex++;
		if (empty($arItem['ITEMS']))
		{
			?>
			<li class="dropdown">
				<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
			</li>
			<?
		} else
		{
			?>
			<li class="dropdown dropdown-slide">
				<a href="<?= $arItem["LINK"] ?>" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350" role="button" aria-haspopup="true" aria-expanded="false"><?= $arItem["TEXT"] ?> <span class="tf-ion-ios-arrow-down"></span></a>
				<div class="dropdown-menu">
					<div class="row">
						<?
						$isChildExists = false;
						//	myPrint($arItem['ITEMS']);
						foreach ($arItem['ITEMS'] as $arChildItem)
						{
							if (isset($arChildItem['ITEMS']) && count($arChildItem['ITEMS']) > 0)
							{
								$isChildExists = true;
							}
						}
						if ($isChildExists)
						{
							$cntRowChild = intval(count($arItem['ITEMS']));
							if ($cntRowChild <= 0)
							{
								$cntRowChild = 1;
							}
							if ($cntRowChild >= 3)
							{
								$cntRowChild = 3;
							}
							if (in_array($iIndex, $arResult['EXISTS_BANNERS']))
							{
								$cntRowChild++;
							}
							foreach ($arItem['ITEMS'] as $arChildItem)
							{
								?>
								<div class="col-sm-<?= (12 / $cntRowChild) ?> col-xs-12">
									<ul>
										<li class="dropdown-header"><?= $arChildItem["TEXT"] ?></li>
										<li role="separator" class="divider"></li>
										<?
										foreach ($arChildItem['ITEMS'] as $arChildItem2)
										{
											?>
											<li>
												<a href="<?= $arChildItem2["LINK"] ?>"><?= $arChildItem2["TEXT"] ?></a>
											</li>
											<?
										}
										?>
									</ul>
								</div>
								<?
							}
							if (in_array($iIndex, $arResult['EXISTS_BANNERS']))
							{
								?>
								<div class="col-sm-<?= (12 / $cntRowChild) ?> col-xs-12">
									<?
									$APPLICATION->IncludeComponent(
											"bitrix:advertising.banner", "topmenu", Array(
										"CACHE_TIME" => "0",
										"CACHE_TYPE" => "A",
										"NOINDEX" => "N",
										"QUANTITY" => "1",
										"TYPE" => "TOP_MENU_BANNER_" . $iIndex
											)
									);
									?>
								</div>
								<?
							}
						} else
						{
							$cntRowChild = intval(count($arItem['ITEMS']) / $arParams['CNT_ITEMS_COL']);
							if ($cntRowChild <= 0)
							{
								$cntRowChild = 1;
							}
							if ($cntRowChild >= 3)
							{
								$cntRowChild = 3;
							}
							$cntPerPage = count($arItem['ITEMS']) / $cntRowChild;
							if (in_array($iIndex, $arResult['EXISTS_BANNERS']))
							{
								$cntRowChild++;
							}
							$arItems = array();
							$iCntRowMenu = (in_array($iIndex, $arResult['EXISTS_BANNERS']) ? $cntRowChild - 1 : $cntRowChild);
							$iItemColIndex = 1;
							foreach ($arItem['ITEMS'] as $arChildItem)
							{
								if (!isset($arItems[$iItemColIndex]))
								{
									$arItems[$iItemColIndex] = array();
								}
								if (count($arItems[$iItemColIndex]) < $cntPerPage)
								{
									$arItems[$iItemColIndex][] = $arChildItem;
								} else if ($iItemColIndex < $iCntRowMenu)
								{
									$iItemColIndex++;
									$arItems[$iItemColIndex][] = $arChildItem;
								} else
								{
									$arItems[$iItemColIndex][] = $arChildItem;
								}
							}
							for ($i = 1; $i <= $cntRowChild; $i++)
							{
								if (($i == $cntRowChild) && in_array($iIndex, $arResult['EXISTS_BANNERS']))
								{
									?>
									<div class="col-sm-<?= (12 / $cntRowChild) ?> col-xs-12">
										<?
										$APPLICATION->IncludeComponent(
												"bitrix:advertising.banner", "topmenu", Array(
											"CACHE_TIME" => "0",
											"CACHE_TYPE" => "A",
											"NOINDEX" => "N",
											"QUANTITY" => "1",
											"TYPE" => "TOP_MENU_BANNER_" . $iIndex
												)
										);
										?>
									</div>
									<?
								} else
								{
									?>
									<div class="col-sm-<?= (12 / $cntRowChild) ?> col-xs-12">
										<ul>
											<?
											foreach ($arItems[$i] as $arChildItem)
											{
												?>
												<li>
													<a href="<?= $arChildItem["LINK"] ?>"><?= $arChildItem["TEXT"] ?></a>
												</li>
												<?
											}
											?>
										</ul>
									</div>
									<?
								}
							}
						}
						?>
					</div>
				</div>
			</li>
			<?
		}
	}
	?>
</ul>