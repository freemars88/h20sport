<?

$arNewResult = array();
$arLinks = array();
foreach ($arResult as $arItem)
{
	if ($arItem['DEPTH_LEVEL'] == 1)
	{
		$arNewResult[] = $arItem;
		$arLinks = array(1 => &$arNewResult[count($arNewResult) - 1]);
	} else
	{
		$arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS'][] = $arItem;
		$arLinks[$arItem['DEPTH_LEVEL']] = &$arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS'][count($arLinks[$arItem['DEPTH_LEVEL'] - 1]['ITEMS']) - 1];
	}
}
$arResult = array("ITEMS" => $arNewResult);

CModule::IncludeModule("advertising");
$by = "s_weight";
$order = "asc";
$is_filtered = false;
$arFilter = array(
	"LAMP" => "green",
	"TYPE_SID" => "TOP_MENU_BANNER_%",
	"SITE" => SITE_ID
);
$rBanners = CAdvBanner::GetList($by, $order, $arFilter, $is_filtered, "N");
while ($arBanner = $rBanners->Fetch())
{
	$arResult["EXISTS_BANNERS"][] = substr($arBanner['TYPE_SID'], 16);
}
