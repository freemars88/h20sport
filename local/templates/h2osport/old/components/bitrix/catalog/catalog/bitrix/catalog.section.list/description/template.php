<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="col-md-12 b_catalog__description" id="section_description">
	<?
	if (strlen($arParams['~DETAIL_TEXT']) > 0)
	{
		echo $arParams['~DETAIL_TEXT'];
	} else
	{
		echo $arResult['SECTION']['DESCRIPTION'];
	}
	?>
</div>