<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if ($arResult['DISPLAY_PROPERTIES']['SHOES_VOLUME']['VALUE'] > 0 && $arResult['DISPLAY_PROPERTIES']['SHOES_HEIGHT']['VALUE'] > 0)
{
	$arResult['DISPLAY_PROPERTIES']['SHOES_VOLUME']['NAME'] = "Объем/высота";
	$arResult['DISPLAY_PROPERTIES']['SHOES_VOLUME']['DISPLAY_VALUE'] .= " / " . $arResult['DISPLAY_PROPERTIES']['SHOES_HEIGHT']['DISPLAY_VALUE'] . " см";
	unset($arResult['DISPLAY_PROPERTIES']['SHOES_HEIGHT']);
}

if ($arResult["PROPERTIES"]['SIZE_TABLE']['VALUE'] > 0)
{
	$arResult["SIZE_TABLE"] = $arResult["PROPERTIES"]['SIZE_TABLE']['VALUE'];
} else
{
	if (isset($arResult['SECTION']['PATH']))
	{
		foreach ($arResult['SECTION']['PATH'] as $arSection)
		{
			$arSectionInfo = CIBlockSection::GetList(array(), array("ID" => $arSection['ID'], "IBLOCK_ID" => $arSection['IBLOCK_ID']), false, array("ID", "NAME", "UF_*"))->Fetch();
			if ($arSectionInfo['UF_SIZE_TABLE'] > 0)
			{
				$arResult["SIZE_TABLE"] = $arSectionInfo['UF_SIZE_TABLE'];
			}
		}
	}
}

if ($arResult["SIZE_TABLE"] > 0)
{
	$arResult["SIZE_TABLE"] = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_REFERENCE_SIZE_TABLE, "ID" => $arResult["SIZE_TABLE"]), false, false, array("ID", "NAME", "PREVIEW_TEXT"))->Fetch();
}

$arMaterials = array();
if (!is_array($arResult['PROPERTIES']['MATERIAL_LINING']['VALUE']))
{
	$arResult['PROPERTIES']['MATERIAL_LINING']['VALUE'] = array($arResult['PROPERTIES']['MATERIAL_LINING']['VALUE']);
}
foreach ($arResult['PROPERTIES']['MATERIAL_LINING']['VALUE'] as $val)
{
	if ($val > 0)
	{
		$arMaterials[$val] = $val;
	}
}
if (!is_array($arResult['PROPERTIES']['MATERIAL_TOP']['VALUE']))
{
	$arResult['PROPERTIES']['MATERIAL_TOP']['VALUE'] = array($arResult['PROPERTIES']['MATERIAL_TOP']['VALUE']);
}
foreach ($arResult['PROPERTIES']['MATERIAL_TOP']['VALUE'] as $val)
{
	if ($val > 0)
	{
		$arMaterials[$val] = $val;
	}
}
if (!empty($arMaterials))
{
	$rElements = CIBlockElement::GetList(array(), array("ID" => $arMaterials), false, false, array("ID", "NAME", "PREVIEW_TEXT"));
	while ($arElement = $rElements->Fetch())
	{
		$arResult['MATERIALS'][$arElement['ID']] = $arElement;
	}
}


foreach ($arResult['SECTION']['PATH'] as $arSection)
{
	$arSectionInfo = CIBlockSection::GetList(array(), array("ID" => $arSection['ID'], "IBLOCK_ID" => $arSection['IBLOCK_ID']), false, array("ID", "UF_*"))->Fetch();
	if ($arSectionInfo['UF_RECOMENDED'] > 0)
	{
		$arSectionBrand = CIBlockSection::GetList(array(), array("ID" => $arSectionInfo['UF_RECOMENDED'], "ACTIVE" => "Y"))->Fetch();
		if ($arSectionBrand)
		{
			$arResult["RECOMENDED_FROM_SECTION"] = $arSectionBrand['ID'];
		}
	}
}

$arResult['OFFERS_AMOUNT'] = array();
$arResult['STORE_LIST'] = array();
$rStore = CCatalogStore::GetList(array("TITLE" => "ASC"));
while ($arStore = $rStore->Fetch())
{
	if (in_array($arStore['ID'], array(STORE_ID_IGNORED)))
	{
		continue;
	}
	$arResult['STORE_LIST'][$arStore['ID']] = $arStore;
}
$arResult['OFFERS_AMOUNT'][$arResult['ID']] = array();
foreach ($arResult['OFFERS'] as $arOffer)
{
	$arResult['OFFERS_AMOUNT'][$arOffer['ID']] = array();
}
$rStoreAmount = CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => array_keys($arResult['OFFERS_AMOUNT'])));
while ($arStoreAmount = $rStoreAmount->Fetch())
{
	$arResult['OFFERS_AMOUNT'][$arStoreAmount['PRODUCT_ID']][$arStoreAmount['STORE_ID']] = $arStoreAmount['AMOUNT'];
}

$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 24, "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PROPERTY_STORE", "PROPERTY_MAPS", "PROPERTY_METRO", "PROPERTY_ADDRESS", "PROPERTY_PHONE", "PROPERTY_WORKTIME"));
while ($arElement = $rElements->Fetch())
{
	if (strlen($arElement['PROPERTY_STORE_VALUE']) > 0 && $arElement['PROPERTY_STORE_ENUM_ID'] > 0)
	{
		$arEnum = CIBlockPropertyEnum::GetByID($arElement['PROPERTY_STORE_ENUM_ID']);
		$arElement['STORE_XML_ID'] = $arEnum['XML_ID'];
		$arResult['SHOP'][$arElement['STORE_XML_ID']] = $arElement;
	}
}

$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 23, "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PROPERTY_X", "PROPERTY_Y"));
while ($arElement = $rElements->Fetch())
{
	$arResult['METRO'][$arElement['ID']] = $arElement;
}

$this->__component->setResultCacheKeys(array(
	"RECOMENDED_FROM_SECTION",
	"STORE_LIST",
	"OFFERS_AMOUNT",
	"SHOP",
	"METRO"
));
