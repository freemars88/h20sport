<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
$GLOBALS['ELEMENT_SECTION_ID'] = $arResult['SECTION']['ID'];
$GLOBALS['RECOMENDED_SECTION_ID'] = $arResult["RECOMENDED_FROM_SECTION"];
