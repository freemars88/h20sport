<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>
<section class="forget-password-page account">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="block text-center">
					<h2 class="text-center">Смена пароля</h2>
					<form class="text-left form_auth clearfix" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
						<?
						if (!empty($arParams["~AUTH_RESULT"]))
						{
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert <?= ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "alert-success" : "alert-danger") ?>"><p><?= nl2br(htmlspecialcharsbx($text)) ?></p></div>
							<?
						}
						if (strlen($arResult["BACKURL"]) > 0)
						{
							?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
							<?
						}
						?>
						<input type="hidden" name="AUTH_FORM" value="Y">
						<input type="hidden" name="TYPE" value="CHANGE_PWD">

						<div class="form-group">
							<input type="text" name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>"  placeholder="Логин" class="form-control" />
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="USER_CHECKWORD" maxlength="255" value="<?= $arResult["USER_CHECKWORD"] ?>" placeholder="Контрольная строка" class="form-control" />
						</div>
						<div class="form-group">
							<input type="password" name="USER_PASSWORD" maxlength="255" value="<?= $arResult["USER_PASSWORD"] ?>" autocomplete="off" placeholder="Пароль" class="form-control" />
						</div>
						<div class="form-group">
							<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off" placeholder="Подтверждение пароля" class="form-control" />
						</div>
						<div class="text-center">
							<input type="hidden" name="change_pwd" value="Y" />
							<button type="submit" class="btn btn-main text-center">Изменить пароль</button>
						</div>
					</form>
					<p class="mt-20"><noindex><a href="<?= $arResult["AUTH_AUTH_URL"] ?>" rel="nofollow">Авторизоваться</a></noindex></p>
				</div>
			</div>
		</div>
	</div>
</section>
