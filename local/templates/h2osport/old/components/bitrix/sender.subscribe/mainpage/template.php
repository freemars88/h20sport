<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<form role="form" method="post" action="javascript:void(0)" id="sender-subscribe-form" class="input-group subscription-form">
	<?= bitrix_sessid_post() ?>
	<input type="hidden" name="sender_subscription" value="add" />
	<? foreach ($arResult["RUBRICS"] as $itemID => $itemValue)
	{ ?>
		<input type="hidden" name="SENDER_SUBSCRIBE_RUB_ID[]" id="SENDER_SUBSCRIBE_RUB_ID_<?= $itemValue["ID"] ?>" value="<?= $itemValue["ID"] ?>" />
<? } ?>
	<input type="text" class="form-control" placeholder="Введите ваш EMail" name="SENDER_SUBSCRIBE_EMAIL" value="" />
	<span class="input-group-btn">
		<button class="btn btn-main sender-subscribe-button-submit" type="button">Подписаться!</button>
	</span>
</form>
