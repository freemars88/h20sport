<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$arRes = array(
	"is_ok" => false
);

if (isset($arResult['MESSAGE'])) {
	ob_start();
	?>
	<table>
		<tr>
			<td style="padding-right: 40px; padding-bottom: 0px;"><img src="<?= ($this->GetFolder() . '/images/' . ($arResult['MESSAGE']['TYPE'] == 'ERROR' ? 'icon-alert.png' : 'icon-ok.png')) ?>" alt=""></td>
			<td>
				<div style="font-size: 16px;"><?= htmlspecialcharsbx($arResult['MESSAGE']['TEXT']) ?></div>
			</td>
		</tr>
	</table>
	<?
	$arRes['is_ok'] = true;
	$arRes['message'] = ob_get_contents();
	$arRes['title'] = GetMessage('subscr_form_response_' . $arResult['MESSAGE']['TYPE']);
	ob_end_clean();
}
echo json_encode($arRes);
