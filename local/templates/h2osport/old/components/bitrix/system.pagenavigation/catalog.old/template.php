<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<ul class="pagination">
	<li class="pagination__prev pagination__prev_10">
		<?
		if ($arResult["NavPageNomer"] > 1)
		{
			if ($arResult["bSavePage"])
			{
				?>
				<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"></a>
				<?
			} else
			{
				?>
				<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
				<?
			}
		} else
		{
			?>
			<a href="javascript:void(0)" class="nav-current-page disabled"></a>
		<? }
		?>
	</li>
	<li class="pagination__prev">

		<?
		if ($arResult["NavPageNomer"] > 1)
		{
			if ($arResult["bSavePage"])
			{
				?>
				<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
				<?
			} else
			{
				?>
				<?
				if ($arResult["NavPageNomer"] > 2)
				{
					?>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
					<?
				} else
				{
					?>
					<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
					<?
				}
			}
		} else
		{
			?>
			<a href="javascript:void(0)" class="disabled"></a>
		<? }
		?>
	</li>
	<?
	while ($arResult["nStartPage"] <= $arResult["nEndPage"])
	{
		if ($arResult["nStartPage"] == $arResult["NavPageNomer"])
		{
			?>
			<li class="active">
				<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
			</li>
			<?
		} elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false)
		{
			?>
			<li>
				<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
			</li>
			<?
		} else
		{
			?>
			<li>
				<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
			</li>
			<?
		}
		$arResult["nStartPage"] ++;
	}
	?>
	<li class="pagination__next">
		<?
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
		{
			?>
			<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
			<?
		} else
		{
			?>
			<a href="javascript:void(0)" class="disabled"></a>
		<? }
		?>
	</li>
	<li class="pagination__next pagination__next_10">
		<?
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
		{
			?>
			<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"></a>
			<?
		} else
		{
			?>
			<a href="javascript:void(0)" class="nav-current-page disabled"></a>
			<?
		}
		?>
	</li>
</ul>
