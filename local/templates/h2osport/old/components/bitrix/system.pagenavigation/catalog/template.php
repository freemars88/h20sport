<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<div class="text-center">
	<ul class="pagination post-pagination">
		<?
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
		{
			?>
			<div class="pagination-also justify-content-center">
				<a href="javascript:void(0)" data-href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>" class="catalog-showmore">Показать больше</a>
			</div>
			<div class="pagination-also-loader justify-content-center">
				<img src="<?= SITE_TEMPLATE_PATH ?>/img/30.gif" />
			</div>
			<?
		}
		if ($arResult["NavPageNomer"] > 1)
		{
			if ($arResult["bSavePage"])
			{
				?>
				<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">Назад</a></li>
				<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a></li>
				<?
			} else
			{
				if ($arResult["NavPageNomer"] > 2)
				{
					?>
					<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">Назад</a></li>
					<?
				} else
				{
					?>
					<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">Назад</a></li>
				<? } ?>
				<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
			<? } ?>
			<?
			if ($arResult["NavPageCount"] > 5 && $arResult["NavPageNomer"] > 3)
			{
				?>
				<li><span>...</span></li>
				<?
			}
		} else
		{
			?>
			<li class="active"><a href="javascript:void(0)">Назад</a></li>
			<li class="active"><a href="javascript:void(0)">1</a></li>
			<?
		}

		$arResult["nStartPage"] ++;
		while ($arResult["nStartPage"] <= $arResult["nEndPage"] - 1)
		{
			if ($arResult["nStartPage"] == $arResult["NavPageNomer"])
			{
				?>
				<li class="active"><a href="javascript:void(0)"><?= $arResult["nStartPage"] ?></a></li>
				<?
			} else
			{
				?>
				<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a></li>
				<?
			}
			$arResult["nStartPage"] ++;
		}
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
		{
			if ($arResult["NavPageCount"] > 5 && $arResult["NavPageNomer"] < ($arResult["NavPageCount"] - 2))
			{
				?>
				<li><span>...</span></li>
				<?
			}
			if ($arResult["NavPageCount"] > 1)
			{
				?>
				<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a></li>
			<? } ?>
			<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">Вперед</a></li>
			<?
		} else
		{
			if ($arResult["NavPageCount"] > 1)
			{
				?>
				<li class="active"><a href="javascript:void(0)"><?= $arResult["NavPageCount"] ?></a></li>
			<? }
			?>
			<li class="active"><a href="javascript:void(0)">Вперед</a></li>
			<? } ?>
	</ul>
</div>