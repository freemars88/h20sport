<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

if ($arResult["NavPageNomer"] > 2) {
	?>
	<div class="b-button__container -show-more b-news-show-more-prev">
		<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>" class="b-button -gray -light" id="btn-get-news-prev"><?= $arResult['NavTitle'] ?></a>
	</div>
	<?
} else if ($arResult["NavPageNomer"] > 1) {
	?>
	<div class="b-button__container -show-more b-news-show-more-prev">
		<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="b-button -gray -light" id="btn-get-news-prev"><?= $arResult['NavTitle'] ?></a>
	</div>
	<?
}
