<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
	<?
	foreach ($arResult["ITEMS"] as $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="col-md-6" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
			<div class="post">
				<div class="post-thumb text-center">
					<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
						<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="" style="max-width:100%;" />
					</a>
				</div>
				<h2 class="post-title"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h2>
				<?/*<div class="post-meta">
					<ul>
						<li>
							<i class="tf-ion-ios-calendar"></i> <?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
						</li>
					</ul>
				</div>
				 * 
				 */?>
				<div class="post-content">
					<p><?= $arItem['PREVIEW_TEXT'] ?></p>
					<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="btn btn-main">Далее...</a>
				</div>
			</div>
		</div>
		<?
	}
	?>

</div>
<div class="text-center">
	<?= $arResult["NAV_STRING"] ?>
</div>
