<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="post-thumb text-center">
	<img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" alt="">
</div>
<h2 class="post-title"><?= $arResult['NAME'] ?></h2>
<?/*
<div class="post-meta">
	<ul>
		<li>
			<i class="tf-ion-ios-calendar"></i> <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>
		</li>
	</ul>
</div>
 * 
 */?>
<div class="post-content post-excerpt">
	<?= $arResult['DETAIL_TEXT'] ?>
</div>

<div class="post-social-share">
	<noindex>
		<h3 class="post-sub-heading">Поделиться</h3>
		<div class="social-media-icons">
			<ul>
				<?
				$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
						), $component, array("HIDE_ICONS" => "Y")
				);
				?>
				<?/*
				<li><a class="facebook" href=""><i class="tf-ion-social-facebook"></i></a></li>
				<li><a class="twitter" href=""><i class="tf-ion-social-twitter"></i></a></li>
				<li><a class="dribbble" href=""><i class="tf-ion-social-dribbble-outline"></i></a></li>
				<li><a class="instagram" href=""><i class="tf-ion-social-instagram"></i></a></li>
				<li><a class="googleplus" href=""><i class="tf-ion-social-googleplus"></i></a></li>*/?>
			</ul>
		</div>
	</noindex>
</div>
