<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/**
 * @global CMain $APPLICATION
 */
global $APPLICATION;

//delayed function must return a string
if (empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize - 2 && $arResult[$index + 1]["LINK"] <> "" ? ' itemref="breadcrumb_' . ($index + 1) . '"' : '');
	$child = ($index > 0 ? ' itemprop="child"' : '');

	if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1)
	{
		$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $arResult[$index]["LINK"] . '" itemprop="item"><span itemprop="item">' . $title . '</span></a></li>';
	} else
	{
		$strReturn .= '<li class="active">' . $title . '</li>';
	}
}

$strReturn .= '</ol>';

return $strReturn;
