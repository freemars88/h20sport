<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$total = 0;
foreach ($arResult["CATEGORIES"]['READY'] as $arItem)
{
	$total += $arItem['QUANTITY'];
}
?>
<a href="/personal/cart/" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="tf-ion-android-cart"></i>Корзина<?
	if ($total > 0)
	{
		echo " (" . $total . ")";
	}
	?></a>
<?
if (!empty($arResult["CATEGORIES"]['READY']))
{
	?>
	<div class="dropdown-menu cart-dropdown">
		<?
		foreach ($arResult["CATEGORIES"]['READY'] as $arItem)
		{
			?>
			<div class="media">
				<a class="pull-left" href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><img  class="media-object" src="<?= $arItem["PICTURE_SRC"] ?>" alt="<?= $arItem['NAME'] ?>" /></a>
				<div class="media-body">
					<h4 class="media-heading"><a href="<?= (strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)") ?>"><?= $arItem['NAME'] ?></a></h4>
					<div class="cart-price">
						<span><?= $arItem['QUANTITY'] ?> x</span>
						<span><?= $arItem['PRICE_FMT'] ?></span>
					</div>
				</div>
				<a href="javascript:void(0)" class="remove header-basket-remove" data-id="<?= $arItem['ID'] ?>" title="Удалить"><i class="tf-ion-close"></i></a>
			</div>	
			<?
		}
		?>
		<div class="cart-summary">
			<span>Итого</span>
			<span class="total-price"><?= $arResult['TOTAL_PRICE'] ?></span>
		</div>
		<ul class="text-center cart-buttons">
			<li><a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="btn btn-small">В корзину</a></li>
			<li><a href="<?= $arParams['PATH_TO_ORDER'] ?>" class="btn btn-small btn-solid-border">Оформить заказ</a></li>
		</ul>
	</div>
	<?
}

