<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @var array $arParams
 */
CUtil::InitJSCore(array("popup"));

$arAuthServices = $arPost = array();
if (is_array($arParams["~AUTH_SERVICES"])) {
	$arAuthServices = $arParams["~AUTH_SERVICES"];
}
if (is_array($arParams["~POST"])) {
	$arPost = $arParams["~POST"];
}

$hiddens = "";
foreach ($arPost as $key => $value) {
	if (!preg_match("|OPENID_IDENTITY|", $key)) {
		$hiddens .= '<input type="hidden" name="' . $key . '" value="' . $value . '" />' . "\n";
	}
}
?>
<script type="text/javascript">
	function BxSocServPopup(id)
	{
		var content = BX("bx_socserv_form_" + id);
		if (content)
		{
			var popup = BX.PopupWindowManager.create("socServPopup" + id, BX("bx_socserv_icon_" + id), {
				autoHide: true,
				closeByEsc: true,
				angle: {offset: 24},
				content: content,
				offsetTop: 3
			});

			popup.show();

			var input = BX.findChild(content, {'tag': 'input', 'attribute': {'type': 'text'}}, true);
			if (input)
			{
				input.focus();
			}

			var button = BX.findChild(content, {'tag': 'input', 'attribute': {'type': 'submit'}}, true);
			if (button)
			{
				button.className = 'btn btn-primary';
			}
		}
	}
</script>

<?
$arSocialIcons = array(
	"VKontakte" => array(
		"gray" => "-social-vk_gray",
		"active" => "-social-vk_violet"
	),
	"Facebook" => array(
		"gray" => "-social-fb_gray",
		"active" => "-social-fb_violet"
	),
);
foreach ($arAuthServices as $service) {
	$onclick = ($service["ONCLICK"] <> '' ? $service["ONCLICK"] : "BxSocServPopup('" . $service["ID"] . "')");
	$isLink = false;
	foreach ($arParams['~MAIN_RESULT']['DB_SOCSERV_USER'] as $arSocserv) {
		if ($arSocserv['EXTERNAL_AUTH_ID'] == $service['ID']) {
			$isLink = true;
			$arSocservUser = $arSocserv;
		}
	}
	if ($isLink) {
		if (!$icon = htmlspecialcharsbx($arParams['~MAIN_RESULT']["AUTH_SERVICES_ICONS"][$arSocservUser["EXTERNAL_AUTH_ID"]]["ICON"]))
		{
			$icon = 'openid';
		}
		$authID = ($arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"]) ? $arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"] : $arSocservUser["EXTERNAL_AUTH_ID"];
		?>
		<div class="b-settings__item">
			<div class="b-settings-item b-settings-item_done">
				<div class="b-settings-item__user">
					<div class="b-settings-item__icon"><i class="b-icon b-icon_svg <?= $arSocialIcons[$service["ID"]]['active'] ?>"></i></div>
					<?/*<img src="/dist/img/lk/avatar01.jpg" alt="">*/?>
				</div>
				<div class="b-settings-item__name">
					<span><?= $arSocservUser["VIEW_NAME"] ?></span>
				</div>
				<? if (in_array($arSocservUser["ID"], $arParams['~MAIN_RESULT']["ALLOW_DELETE_ID"])) { ?>
					<div class="b-settings-item__button"><a href="<?= htmlspecialcharsbx($arSocservUser["DELETE_LINK"]) ?>" class="b-button -green" onclick="return confirm('<?= GetMessage("SS_PROFILE_DELETE_CONFIRM") ?>')">Отвязать аккаунт</a></div>
				<? } ?>
			</div>
		</div>
<?/*
		<tr class="soc-serv-personal">
			<td class="bx-ss-icons">
				<i class="bx-ss-icon <?= $icon ?>">&nbsp;</i>
				<? if ($arUser["PERSONAL_LINK"] != ''): ?>
					<a class="soc-serv-link" target="_blank" href="<?= $arUser["PERSONAL_LINK"] ?>">
					<? endif; ?>
					<?= $authID ?>
					<? if ($arUser["PERSONAL_LINK"] != ''): ?>
					</a>
				<? endif; ?>
			</td>
		</tr>
		<?*/
	} else {
		?>
		<div class="b-settings__item">
			<div class="b-settings-item">
				<div class="b-settings-item__icon"><a href="javascript:void(0)" class="b-icon b-icon_svg <?= $arSocialIcons[$service["ID"]]['gray'] ?>" onclick="<?= htmlspecialcharsbx($onclick) ?>"></a></div>
				<div class="b-settings-item__button"><a href="javascript:void(0)" class="b-button -gray" onclick="<?= htmlspecialcharsbx($onclick) ?>">привязать аккаунт</a></div>
				<? if ($service["ONCLICK"] == '' && $service["FORM_HTML"] <> '') { ?>
					<div id="bx_socserv_form_<?= $service["ID"] ?>" class="bx-authform-social-popup">
						<form action="<?= $arParams["AUTH_URL"] ?>" method="post">
							<?= $service["FORM_HTML"] ?>
							<?= $hiddens ?>
							<input type="hidden" name="auth_service_id" value="<?= $service["ID"] ?>" />
						</form>
					</div>
				<? } ?>
			</div>
		</div>
		<?
	}
}
