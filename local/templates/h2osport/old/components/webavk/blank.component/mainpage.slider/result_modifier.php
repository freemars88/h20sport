<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("advertising");
$by = "s_weight";
$order = "asc";
$is_filtered = false;
$arFilter = array(
	"LAMP" => "green",
	"TYPE_SID" => $arParams['BANNER_TYPE'],
	"SITE" => SITE_ID
);
$rBanners = CAdvBanner::GetList($by, $order, $arFilter, $is_filtered, "N");
while ($arBanner = $rBanners->Fetch()) {
	$arImage = CFile::GetFileArray($arBanner["IMAGE_ID"]);
	$strReturn = "";
	if ($arImage) {
		$file_type = GetFileType($arImage["FILE_NAME"]);
		$path = $arImage["SRC"];
		$arBanner['IMAGE_PATH'] = $path;
		switch ($file_type) {
			default:
				$alt = CAdvBanner::PrepareHTML(trim($arBanner["IMAGE_ALT"]), $arBanner);
				$strImage = "<img alt=\"" . htmlspecialcharsEx($alt) . "\" title=\"" . htmlspecialcharsEx($alt) . "\" src=\"" . $path . "\" width=\"" . $arImage["WIDTH"] . "\" height=\"" . $arImage["HEIGHT"] . "\" style=\"border:0;\" />";
				if (strlen(trim($arBanner["URL"])) > 0) {
					$url = $arBanner["URL"];
					$url = CAdvBanner::PrepareHTML($url, $arBanner);
					$url = CAdvBanner::GetRedirectURL($url, $arBanner);
					$target = (strlen(trim($arBanner["URL_TARGET"])) > 0) ? " target=\"" . $arBanner["URL_TARGET"] . "\" " : "";
					$strReturn = "<a href=\"" . $url . "\"" . $target . ">" . $strImage . "</a>";
					$arBanner['LINK'] = $url;
				} else {
					$strReturn .= $strImage;
				}
				break;
		}
	}
	$arBanner['HTML'] = $strReturn;
	$arResult['BANNERS'][] = $arBanner;
}
