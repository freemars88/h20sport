<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<section class="main-slider">
	<div style="max-width: 1140px; margin: 0 auto;">
		<div id="home_slider_wrapper" class="rev_slider_wrapper fullscreen-container" >
			<div id="home_slider" class="rev_slider fullscreenbanner tiny_bullet_slider" data-version="5.4.1">
				<ul>
					<?
					foreach ($arResult['BANNERS'] as $arBanner)
					{
						$strCode = $arBanner['CODE'];
						$strCode = str_replace("#ID#", $arBanner['ID'], $strCode);
						$strCode = str_replace("#IMG#", $arBanner['IMAGE_PATH'], $strCode);
						$strCode = str_replace("#LINK#", $arBanner['LINK'], $strCode);
						echo $strCode;
					}
					?>
				</ul>
				<div class="tp-bannertimer" style="height: 10px; background: rgba(0, 0, 0, 0.15);"></div>
			</div>
		</div>
	</div>
</section>
