<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//Грузим все станции метро
$arSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams['METRO_IBLOCK_ID'], "ID" => $arParams['METRO_SECTION_ID']), false, array("UF_*"))->Fetch();
$arResult['METRO_SECTION'] = $arSection;

$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams['METRO_IBLOCK_ID'], "SECTION_ID" => $arParams['METRO_SECTION_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PROPERTY_X", "PROPERTY_Y", "PROPERTY_METRO_LINE"));
while ($arElement = $rElements->Fetch())
{
	$arResult['METRO'][$arElement['ID']] = $arElement;
}

$rElements = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['METROLINE_IBLOCK_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PROPERTY_NUMBER", "PROPERTY_COLOR", "PROPERTY_COLOR_TEXT"));
while ($arElement = $rElements->Fetch())
{
	$arResult['METRO_LINE'][$arElement['ID']] = $arElement;
}

//Грузим магазины
$arSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams['SHOP_IBLOCK_ID'], "ID" => $arParams['SHOP_SECTION_ID']))->Fetch();
$arResult['SHOP_SECTION'] = $arSection;

$rElements = CIBlockElement::GetList(array("PROPERTY_METRO.NAME" => "ASC", "SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $arParams['SHOP_IBLOCK_ID'], "SECTION_ID" => $arParams['SHOP_SECTION_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PROPERTY_STORE", "PROPERTY_MAPS", "PROPERTY_METRO", "PROPERTY_ADDRESS", "PROPERTY_PHONE", "PROPERTY_WORKTIME"));
while ($arElement = $rElements->Fetch())
{
	$arResult['SHOP'][$arElement['ID']] = $arElement;
}

//Грузим города
$rSections = CIBlockSection::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $arParams['SHOP_IBLOCK_ID']));
while ($arSection = $rSections->Fetch())
{
	$arResult['ALLSHOP'][$arSection['ID']] = $arSection;
}

//Грузим магазины
$rElements = CIBlockElement::GetList(array("PROPERTY_METRO.NAME"=>"ASC", "SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $arParams['SHOP_IBLOCK_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_STORE", "PROPERTY_MAPS", "PROPERTY_METRO", "PROPERTY_METRO.NAME", "PROPERTY_ADDRESS", "PROPERTY_PHONE", "PROPERTY_WORKTIME"));
while ($arElement = $rElements->Fetch())
{
	$arResult['ALLSHOP'][$arElement['IBLOCK_SECTION_ID']]['ITEMS'][$arElement['ID']] = $arElement;
}

//myPrint($arResult['SHOP']);
