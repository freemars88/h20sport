<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['METRO_SECTION']))
{
	if ($arResult['METRO_SECTION']['PICTURE'] > 1)
	{
		$sMapPath = CFile::GetPath($arResult['METRO_SECTION']['PICTURE']);
		$arFile = CFile::GetImageSize($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arResult['METRO_SECTION']['PICTURE']));
		$iMapWidth = $arFile[0];
		$iMapHeight = $arFile[1];
	} else
	{
		$sMapPath = CFile::GetPath($arResult['METRO_SECTION']['UF_MAP']);
		$iMapWidth = $arResult['METRO_SECTION']['UF_MAP_WIDTH'];
		$iMapHeight = $arResult['METRO_SECTION']['UF_MAP_HEIGHT'];
	}
	$arIgnoredShop = array();
	?>
	<div class="row">
		<div class="contact-details">
			<div class="row">
				<div class="col-sm-6 col-sm-push-6">
					<div class="metromap" id="metro<?= $arResult['METRO_SECTION']['ID'] ?>">
						<?
						foreach ($arResult['SHOP'] as $arShop)
						{
							if ($arShop['PROPERTY_METRO_VALUE'] > 0 && isset($arResult['METRO'][$arShop['PROPERTY_METRO_VALUE']]))
							{
								$arIgnoredShop[] = $arShop['ID'];
								$arMetro = $arResult['METRO'][$arShop['PROPERTY_METRO_VALUE']];
								?>
								<a href="javascript:void(0)" class="metroitem" data-x="<?= $arMetro['PROPERTY_X_VALUE'] ?>" data-y="<?= $arMetro['PROPERTY_Y_VALUE'] ?>" data-basewidth="30" data-baseheight="22" title="<?= $arShop['NAME'] ?>" data-clicklink="#shopitem-<?= $arShop['ID'] ?>"></a>
								<div class="metroshopinfo" id="shopitem-<?= $arShop['ID'] ?>">
									<h2><?= $arShop['NAME'] ?></h2>
									<?
									$arMaps = explode(",", $arShop['PROPERTY_MAPS_VALUE']);
									$lat = $arMaps[0];
									$lon = $arMaps[1];
									$APPLICATION->IncludeComponent(
											"bitrix:map.yandex.view", ".default", array(
										"COMPONENT_TEMPLATE" => ".default",
										"CONTROLS" => array(
											0 => "ZOOM",
											1 => "MINIMAP",
											2 => "TYPECONTROL",
											3 => "SCALELINE",
										),
										"INIT_MAP_TYPE" => "MAP",
										"MAP_DATA" => serialize(array(
											"yandex_lat" => $lat,
											"yandex_lon" => $lon,
											"yandex_scale" => 10,
											"PLACEMARKS" => array(
												array(
													"LON" => $lon,
													"LAT" => $lat,
													"TEXT" => $arShop['NAME'] . "###RN###м." . $arMetro['NAME'] . "###RN###" . $arShop['PROPERTY_ADDRESS_VALUE'] . "###RN###Телефон: " . $arShop['PROPERTY_PHONE_VALUE']
												)
											)
										)),
										"MAP_HEIGHT" => "400",
										"MAP_ID" => "map" . $arShop['ID'],
										"MAP_WIDTH" => "100%",
										"OPTIONS" => array(
											0 => "ENABLE_SCROLL_ZOOM",
											1 => "ENABLE_DBLCLICK_ZOOM",
											2 => "ENABLE_DRAGGING",
										)
											), false
									);
									?>
									<div class="contact-details col-md-12">
										<ul class="contact-short-info">
											<li>
												<i class="tf-ion-ios-home"></i>
												м. <?= $arMetro['NAME'] ?>, <?= $arShop['PROPERTY_ADDRESS_VALUE'] ?>
											</li>
											<li>
												<i class="tf-ion-android-phone-portrait"></i>
												Телефон: <?= $arShop['PROPERTY_PHONE_VALUE'] ?></li>
											<li>
												<i class="pe-7s-clock"></i>
												Время работы: <?= $arShop['PROPERTY_WORKTIME_VALUE'] ?></li>
										</ul>
									</div>
								</div>
								<?
							}
						}
						?>
						<div class="metromapcontent" style="background-image: url(<?= $sMapPath ?>);" data-imagewidth="<?= $iMapWidth ?>" data-imageheight="<?= $iMapHeight ?>"></div>
					</div>
					<div class="hidden-xs">
						<ul class="shoplist">
							<?
							foreach ($arResult['ALLSHOP'] as $arCity)
							{
								foreach ($arCity['ITEMS'] as $arShop)
								{
									if (in_array($arShop['ID'], $arIgnoredShop))
									{
										continue;
									}
									?>
									<li>
										<a href="javascript:void(0);" class="metroitem" data-clicklink="#shopitem-<?= $arShop['ID'] ?>"><span>г. <?= $arCity['NAME'] ?></span>, <?= $arShop['PROPERTY_ADDRESS_VALUE'] ?>, тел.: <?= $arShop['PROPERTY_PHONE_VALUE'] ?>, <?= $arShop['PROPERTY_WORKTIME_VALUE'] ?></a>
										<div class="metroshopinfo" id="shopitem-<?= $arShop['ID'] ?>">
											<h2><?= $arShop['NAME'] ?></h2>
											<?
											$arMaps = explode(",", $arShop['PROPERTY_MAPS_VALUE']);
											$lat = $arMaps[0];
											$lon = $arMaps[1];
											$APPLICATION->IncludeComponent(
													"bitrix:map.yandex.view", ".default", array(
												"COMPONENT_TEMPLATE" => ".default",
												"CONTROLS" => array(
													0 => "ZOOM",
													1 => "MINIMAP",
													2 => "TYPECONTROL",
													3 => "SCALELINE",
												),
												"INIT_MAP_TYPE" => "MAP",
												"MAP_DATA" => serialize(array(
													"yandex_lat" => $lat,
													"yandex_lon" => $lon,
													"yandex_scale" => 10,
													"PLACEMARKS" => array(
														array(
															"LON" => $lon,
															"LAT" => $lat,
															"TEXT" => $arShop['NAME'] . "###RN###" . $arShop['PROPERTY_ADDRESS_VALUE'] . "###RN###Телефон: " . $arShop['PROPERTY_PHONE_VALUE']
														)
													)
												)),
												"MAP_HEIGHT" => "400",
												"MAP_ID" => "map" . $arShop['ID'],
												"MAP_WIDTH" => "100%",
												"OPTIONS" => array(
													0 => "ENABLE_SCROLL_ZOOM",
													1 => "ENABLE_DBLCLICK_ZOOM",
													2 => "ENABLE_DRAGGING",
												)
													), false
											);
											?>
											<div class="contact-details col-md-12">
												<ul class="contact-short-info">
													<li>
														<i class="tf-ion-ios-home"></i>
														<?= $arShop['PROPERTY_ADDRESS_VALUE'] ?>
													</li>
													<li>
														<i class="tf-ion-android-phone-portrait"></i>
														Телефон: <?= $arShop['PROPERTY_PHONE_VALUE'] ?></li>
													<li>
														<i class="pe-7s-clock"></i>
														Время работы: <?= $arShop['PROPERTY_WORKTIME_VALUE'] ?></li>
												</ul>
											</div>
										</div>
									</li>
									<?
								}
							}
							?>
						</ul>
					</div>

				</div>
				<div class="col-sm-6 col-sm-pull-6">
					<h2>Москва</h2>
					<ul class="shoplist">
						<?
						$i = 1;
						foreach ($arResult['METRO_LINE'] as $arMetroLine)
						{
							foreach ($arResult['SHOP'] as $arShop)
							{
								if ($arResult['METRO'][$arShop['PROPERTY_METRO_VALUE']]['PROPERTY_METRO_LINE_VALUE'] == $arMetroLine['ID'])
								{
									$arMetro = $arResult['METRO'][$arShop['PROPERTY_METRO_VALUE']];
									?>
									<li style="color:#<?= $arMetroLine['PROPERTY_COLOR_VALUE'] ?>;">
										<span class="shop-number" style="background-color: #<?= $arMetroLine['PROPERTY_COLOR_VALUE'] ?>;"><?= $i ?></span>
										<a href="javascript:void(0);" class="metroitem" data-clicklink="#shopitem-<?= $arShop['ID'] ?>" style="color:#<?= $arMetroLine['PROPERTY_COLOR_VALUE'] ?>;"><span>м. <?= $arMetro['NAME'] ?></span>, <?= $arShop['PROPERTY_ADDRESS_VALUE'] ?>, тел.: <?= $arShop['PROPERTY_PHONE_VALUE'] ?>, <?= $arShop['PROPERTY_WORKTIME_VALUE'] ?></a>
									</li>
									<?
									$i++;
								}
							}
						}

						foreach ($arResult['ALLSHOP'] as $arCity)
						{
							foreach ($arCity['ITEMS'] as $arShop)
							{
								if (in_array($arShop['ID'], $arIgnoredShop))
								{
									continue;
								}
								?>
								<li class="visible-xs-block">
									<a href="javascript:void(0);" class="metroitem" data-clicklink="#shopitem-<?= $arShop['ID'] ?>"><span>г. <?= $arCity['NAME'] ?></span>, <?= $arShop['PROPERTY_ADDRESS_VALUE'] ?>, тел.: <?= $arShop['PROPERTY_PHONE_VALUE'] ?>, <?= $arShop['PROPERTY_WORKTIME_VALUE'] ?></a>
								</li>
								<?
							}
						}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?
}