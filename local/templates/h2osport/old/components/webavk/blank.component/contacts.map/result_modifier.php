<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//Грузим города
$rSections = CIBlockSection::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $arParams['SHOP_IBLOCK_ID']));
while ($arSection = $rSections->Fetch())
{
	$arResult['SHOP'][$arSection['ID']] = $arSection;
}

//Грузим магазины
$rElements = CIBlockElement::GetList(array("PROPERTY_METRO.NAME"=>"ASC", "SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $arParams['SHOP_IBLOCK_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_STORE", "PROPERTY_MAPS", "PROPERTY_METRO", "PROPERTY_METRO.NAME", "PROPERTY_ADDRESS", "PROPERTY_PHONE", "PROPERTY_WORKTIME"));
while ($arElement = $rElements->Fetch())
{
	$arResult['SHOP'][$arElement['IBLOCK_SECTION_ID']]['ITEMS'][$arElement['ID']] = $arElement;
}

