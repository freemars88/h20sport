<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$arPlacemark = array();
foreach ($arResult['SHOP'] as $arCity)
{
	foreach ($arCity['ITEMS'] as $arItem)
	{
		$arMaps = explode(",", $arItem['PROPERTY_MAPS_VALUE']);
		$lat = $arMaps[0];
		$lon = $arMaps[1];
		$arPlacemark[] = array(
			"LON" => $lon,
			"LAT" => $lat,
			"TEXT" => $arItem['NAME'] . (strlen($arItem['PROPERTY_METRO_NAME']) > 0 ? "###RN###м." . $arItem['PROPERTY_METRO_NAME'] : "") . "###RN###" . $arShop['PROPERTY_ADDRESS_VALUE'] . "###RN###Телефон: " . $arItem['PROPERTY_PHONE_VALUE']
		);
	}
}
?>
<div class="row">
	<div class="contact-details col-md-12">
		<div class="yandex-map">
			<?
			$APPLICATION->IncludeComponent(
					"bitrix:map.yandex.view", ".default", array(
				"COMPONENT_TEMPLATE" => ".default",
				"CONTROLS" => array(
					0 => "ZOOM",
					1 => "MINIMAP",
					2 => "TYPECONTROL",
					3 => "SCALELINE",
				),
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA" => serialize(array(
					"yandex_lat" => 55.73829999984372,
					"yandex_lon" => 37.59459999999997,
					"yandex_scale" => 10,
					"PLACEMARKS" => $arPlacemark
				)),
				"MAP_HEIGHT" => "500",
				"MAP_ID" => "",
				"MAP_WIDTH" => "100%",
				"OPTIONS" => array(
					0 => "ENABLE_SCROLL_ZOOM",
					1 => "ENABLE_DBLCLICK_ZOOM",
					2 => "ENABLE_DRAGGING",
				)
					), false
			);
			?>
		</div>
	</div>
</div>
<div class="contact-detail-address-list">
	<?
	foreach ($arResult['SHOP'] as $arCity)
	{
		?>
		<div class="row">
			<h2><?= $arCity['NAME'] ?></h2>
			<?
			foreach ($arCity['ITEMS'] as $arItem)
			{
				?>
				<div class="contact-details col-md-4">
					<ul class="contact-short-info">
						<li>
							<i class="tf-ion-ios-home"></i>
							<?= (strlen($arItem['PROPERTY_METRO_NAME']) > 0 ? "м." . $arItem['PROPERTY_METRO_NAME'] . ", " : "") ?><?= $arItem['PROPERTY_ADDRESS_VALUE'] ?>
						</li>
						<li>
							<i class="tf-ion-android-phone-portrait"></i>
							Телефон: <?= $arItem['PROPERTY_PHONE_VALUE'] ?>
						</li>
						<li>
							<i class="pe-7s-clock"></i>
							Время работы: <?= $arItem['PROPERTY_WORKTIME_VALUE'] ?>
						</li>
					</ul>
				</div>
				<?
			}
			?>
		</div>
		<?
	}
	?>
</div>
	<?

/*
if (!empty($arResult['METRO_SECTION']))
{
	if ($arResult['METRO_SECTION']['PICTURE'] > 1)
	{
		$sMapPath = CFile::GetPath($arResult['METRO_SECTION']['PICTURE']);
		$arFile = CFile::GetImageSize($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arResult['METRO_SECTION']['PICTURE']));
		$iMapWidth = $arFile[0];
		$iMapHeight = $arFile[1];
	} else
	{
		$sMapPath = CFile::GetPath($arResult['METRO_SECTION']['UF_MAP']);
		$iMapWidth = $arResult['METRO_SECTION']['UF_MAP_WIDTH'];
		$iMapHeight = $arResult['METRO_SECTION']['UF_MAP_HEIGHT'];
	}
	?>
	<div class="metromap" id="metro<?= $arResult['METRO_SECTION']['ID'] ?>">
		<?
		foreach ($arResult['SHOP'] as $arShop)
		{
			if ($arShop['PROPERTY_METRO_VALUE'] > 0 && isset($arResult['METRO'][$arShop['PROPERTY_METRO_VALUE']]))
			{
				$arMetro = $arResult['METRO'][$arShop['PROPERTY_METRO_VALUE']];
				?>
				<a href="javascript:void(0)" class="metroitem" data-x="<?= $arMetro['PROPERTY_X_VALUE'] ?>" data-y="<?= $arMetro['PROPERTY_Y_VALUE'] ?>" data-basewidth="30" data-baseheight="22" title="<?= $arShop['NAME'] ?>" data-clicklink="#shopitem-<?= $arShop['ID'] ?>"></a>
				<div class="metroshopinfo" id="shopitem-<?= $arShop['ID'] ?>">
					<h2><?= $arShop['NAME'] ?></h2>
					<?
					$arMaps = explode(",", $arShop['PROPERTY_MAPS_VALUE']);
					$lat = $arMaps[0];
					$lon = $arMaps[1];
					$APPLICATION->IncludeComponent(
							"bitrix:map.yandex.view", ".default", array(
						"COMPONENT_TEMPLATE" => ".default",
						"CONTROLS" => array(
							0 => "ZOOM",
							1 => "MINIMAP",
							2 => "TYPECONTROL",
							3 => "SCALELINE",
						),
						"INIT_MAP_TYPE" => "MAP",
						"MAP_DATA" => serialize(array(
							"yandex_lat" => $lat,
							"yandex_lon" => $lon,
							"yandex_scale" => 10,
							"PLACEMARKS" => array(
								array(
									"LON" => $lon,
									"LAT" => $lat,
									"TEXT" => $arShop['NAME'] . "###RN###м." . $arMetro['NAME'] . "###RN###" . $arShop['PROPERTY_ADDRESS_VALUE'] . "###RN###Телефон: " . $arShop['PROPERTY_PHONE_VALUE']
								)
							)
						)),
						"MAP_HEIGHT" => "400",
						"MAP_ID" => "map" . $arShop['ID'],
						"MAP_WIDTH" => "100%",
						"OPTIONS" => array(
							0 => "ENABLE_SCROLL_ZOOM",
							1 => "ENABLE_DBLCLICK_ZOOM",
							2 => "ENABLE_DRAGGING",
						)
							), false
					);
					?>
					<div class="contact-details col-md-12">
						<ul class="contact-short-info">
							<li>
								<i class="tf-ion-ios-home"></i>
								м. <?= $arMetro['NAME'] ?>, <?= $arShop['PROPERTY_ADDRESS_VALUE'] ?>
							</li>
							<li>
								<i class="tf-ion-android-phone-portrait"></i>
								Телефон: <?= $arShop['PROPERTY_PHONE_VALUE'] ?></li>
							<li>
								<i class="pe-7s-clock"></i>
								Время работы: <?= $arShop['PROPERTY_WORKTIME_VALUE'] ?></li>
						</ul>
					</div>
				</div>
				<?
			}
		}
		?>
		<div class="metromapcontent" style="background-image: url(<?= $sMapPath ?>);" data-imagewidth="<?= $iMapWidth ?>" data-imageheight="<?= $iMapHeight ?>"></div>
	</div>
	<?
}

*/