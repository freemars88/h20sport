var BasketFormControl = can.Control.extend({
	defaults: {}
}, {
	init: function (el, options) {
		var self = this;
		self.root = el;
		$(".basket_qnt").TouchSpin({});
		$(".basket_qnt").on("touchspin.on.stopspin", function (el, ev) {
			if (parseInt($(this).val()) <= 0)
			{
				$(this).val(1)
			}
			self.doUpdateBasketData();
			//$("#product-buy-form input[name='quantity']").val($("#product-quantity").val());
		});
	},
	".basket_qnt change": function (el, ev) {
		var self = this;
		/*if (parseInt($(el).val()) > $(el).data("max"))
		 {
		 $(el).val($(el).data("max"))
		 }
		 */
		if (parseInt($(el).val()) <= 0)
		{
			$(el).val(1)
		}
		self.doUpdateBasketData();
	},
	/*".b-counter .plus click": function (el, ev) {
	 var qnt = $(el).parents(".b-counter:first").find(".basket-quantity");
	 var value = parseInt(qnt.val()) + 1;
	 if (value > qnt.data("max"))
	 {
	 value = qnt.data("max");
	 }
	 if (value <= 0)
	 {
	 value = 1;
	 }
	 qnt.val(value);
	 this.doUpdateBasketData();
	 return false;
	 },
	 ".b-counter .minus click": function (el, ev) {
	 var qnt = $(el).parents(".b-counter:first").find(".basket-quantity");
	 var value = parseInt(qnt.val()) - 1;
	 if (value <= 0)
	 {
	 value = 1;
	 }
	 qnt.val(value);
	 this.doUpdateBasketData();
	 return false;
	 },
	 */
	".del-from-basket click": function (el, ev) {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "id=" + $(el).data("id") + "&ajaxAction=delBasket",
			context: $(el).parents("tr:first"),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok)
				{
					$(this).fadeOut(300, function () {
						$(this).remove();
						if ($(self.root).find(".product-list table tbody tr").length <= 0)
						{
							window.location.reload();
						}
					});
					//$(self.root).find(".checkout").html(data.checkout_text);
					//$(self.root).find(".b-pane__price span").html(data.allSum_FORMATED + ' <i class="fa fa-rub"></i>');
				}
				self.doUpdateBasketCnt();
			}
		});
	},
	doUpdateBasketCnt: function () {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "ajaxAction=updateBasketCnt",
			dataType: "json",
			method: "GET",
			success: function (data) {
				$("#header-cart-content").html(data.content);
				//$(".basket-mobile-area").html(data.mobile);
				//tmpEl.before($(data));
				//tmpEl.remove();
			}
		});
		return false;
	},
	doUpdateBasketData: function () {
		var self = this;
		var dataRequest = $(this.root).serialize() + "&ajaxAction=recheckBasket";
		BX.showWait(this.root, "");
		$.ajax("/ajax/actions.php", {
			data: dataRequest,
			context: $(this.root),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok)
				{
					$.each(data.items, function (k, row) {
						var rowTable = $(self.root).find("tr[data-id='" + k + "']");
						if (row.DISCOUNT_PRICE != 0)
						{
							rowTable.find(".price-cell").html('<span class="oldprice">' + row.FULL_PRICE_FORMATED + '</span>' + row.PRICE_FORMATED);
						} else {
							rowTable.find(".price-cell").html(row.PRICE_FORMATED);
						}
						rowTable.find(".priceTotalItem").html(row.TOTAL_SUMM_FORMATED);
						if (rowTable.find(".basket_qnt").val() != row.QUANTITY)
						{
							rowTable.find(".basket_qnt").val(row.QUANTITY);
						}
					});
					self.doUpdateBasketCnt();
					//$(self.root).find(".checkout").html(data.checkout_text);
					// $(self.root).find(".b-pane__price span").html(data.allSum_FORMATED + ' <i class="fa fa-rub"></i>');
					if (data.BASKET_CNT <= 4)
					{
						$("#checkout").removeClass("hide");
						$("#checkout-text").addClass("hide");
					} else {
						$("#checkout").addClass("hide");
						$("#checkout-text").removeClass("hide");
					}
				}
			},
			complete: function () {
				BX.closeWait();
			}
		});
	},
	"#checkout click": function (el, ev) {
		$(this.root).submit();
	},
	"#apply-coupon click": function (el, ev) {
		var self = this;
		BX.showWait(this.root, "");
		$.ajax("/ajax/actions.php", {
			data: {
				ajaxAction: "updateBasketCoupon",
				BasketRefresh: "Y",
				COUPON: $('#coupon').val()
			},
			dataType: "html",
			method: "POST",
			success: function (s) {
				self.doUpdateBasketData();
			},
			complete: function () {
				BX.closeWait();
			}
		});
		return false;
	}
	/*
	 "#coupon change": function (el, ev) {
	 var self = this;
	 BX.showWait(this.root, "");
	 $.ajax("/ajax/actions.php", {
	 data: {
	 ajaxAction: "updateBasketCoupon",
	 BasketRefresh: "Y",
	 COUPON: $(el).val()
	 },
	 dataType: "html",
	 method: "POST",
	 success: function (s) {
	 self.doUpdateBasketData();
	 },
	 complete: function () {
	 BX.closeWait();
	 }
	 });
	 return false;
	 }
	 */
});
