function declOfNum(number, titles)
{
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
}

function numeric_format(val, thSep, dcSep) {

    // Проверка указания разделителя разрядов
    if (!thSep)
        thSep = ' ';

    // Проверка указания десятичного разделителя
    if (!dcSep)
        dcSep = ',';

    var res = val.toString();
    var lZero = (val < 0); // Признак отрицательного числа

    // Определение длины форматируемой части
    var fLen = res.lastIndexOf('.'); // До десятичной точки
    fLen = (fLen > -1) ? fLen : res.length;

    // Выделение временного буфера
    var tmpRes = res.substring(fLen);
    var cnt = -1;
    for (var ind = fLen; ind > 0; ind--) {
        // Формируем временный буфер
        cnt++;
        if (((cnt % 3) === 0) && (ind !== fLen) && (!lZero || (ind > 1))) {
            tmpRes = thSep + tmpRes;
        }
        tmpRes = res.charAt(ind - 1) + tmpRes;
    }

    return tmpRes.replace('.', dcSep);

}

function intval(mixed_var, base) {
    var tmp;

    if (typeof (mixed_var) == 'string') {
        tmp = parseInt(mixed_var);
        if (isNaN(tmp)) {
            return 0;
        } else {
            return tmp.toString(base || 10);
        }
    } else if (typeof (mixed_var) == 'number') {
        return Math.floor(mixed_var);
    } else {
        return 0;
    }
}

$(document).ready(function () {
    new MainControl('html', {});
    if ($('#basket_form').length > 0)
    {
        new BasketFormControl("#basket_form", {});
    }
    /*	if ($('#main-catalog-menu').length > 0)
     {
     new MainCatalogMenuControl('#main-catalog-menu', {});
     }
     */
});

