<footer class="footer section text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<noindex>
				<ul class="social-media">
					<li>
						<a href="https://vk.com/francescodonnimoscow" target="_blank" rel="nofollow">
							<i class="tff-ion-social-vk"></i>
						</a>
					</li>
					<li>
						<a href="https://www.facebook.com/francescodonnimoscow/" target="_blank" rel="nofollow">
							<i class="tf-ion-social-facebook"></i>
						</a>
					</li>
					<li>
						<a href="https://www.ok.ru/group/55092172881933" target="_blank" rel="nofollow">
							<i class="tff-ion-social-ok"></i>
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com/francesco_donni/" target="_blank" rel="nofollow">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
				</ul>
				</noindex>
				<?
				$APPLICATION->IncludeComponent(
						"bitrix:menu", "bottom", Array(
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "left",
					"DELAY" => "N",
					"MAX_LEVEL" => "1",
					"MENU_CACHE_GET_VARS" => array(""),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_USE_GROUPS" => "N",
					"ROOT_MENU_TYPE" => "bottom",
					"USE_EXT" => "Y"
						)
				);
				?>
				<p class="copyright-text">&copy; Francesco-Donni, <?= date("Y") ?></p>
			</div>
		</div>
	</div>
</footer>
<div class="modal product-modal fade" id="product-modal" style="display: none;">
	<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
		<i class="tf-ion-close"></i>
	</button>
	<div class="modal-dialog" role="dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="modal-content-text">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?
CWebavkTmplProTools::IncludeBlockFromDir("admin.site.panel");
$APPLICATION->ShowCSS(true, $bXhtmlStyle);