<!-- Site Title-->
<title><? $APPLICATION->ShowTitle(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

<!--[if lt IE 10]>
<div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?= SITE_TEMPLATE_PATH ?>images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/html5shiv.min.js"></script>
<![endif]-->

<?
$bXhtmlStyle = true;
echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . ($bXhtmlStyle ? ' /' : '') . '>' . "\n";
$APPLICATION->ShowMeta("robots", false, $bXhtmlStyle);
$APPLICATION->ShowMeta("description", false, $bXhtmlStyle);
$APPLICATION->ShowLink("canonical", null, $bXhtmlStyle);
//$APPLICATION->ShowCSS(true, $bXhtmlStyle);
?>
<style type="text/css">
	<!--
	body{display:none;}
	-->
</style>
	<?
$APPLICATION->ShowHeadStrings();
$APPLICATION->ShowHeadScripts();
?>

<? /*
  <script type="text/javascript">
  <!--
  function doEventReachGoal(eventName, eventPrams) {
  var intervalID = setInterval(function () {
  if (window.yaCounter<?= doLoadAscaronSettings("UF_YANDEX_COUNTER") ?> !== undefined)
  {
  clearInterval(intervalID);
  window.yaCounter<?= doLoadAscaronSettings("UF_YANDEX_COUNTER") ?>.reachGoal(eventName, eventPrams);
  }
  }, 500);
  }
  -->
  </script>
 */ ?>
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46791003 = new Ya.Metrika({ id:46791003, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch (e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script> <!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46791003 = new Ya.Metrika({ id:11108464, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch (e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script> <!-- /Yandex.Metrika counter -->
<meta name="yandex-verification" content="e36d5784763a62bc" />
<meta name="google-site-verification" content="PZrywZGMarjkVCmiZ0lUFOHhhSMNM8d_xof-oVyVoPg" />
<meta name="yandex-verification" content="1c6cdfe5828c4913" />
<!— Global site tag (gtag.js) - Google Analytics —>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114249628-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114249628-1');
</script>