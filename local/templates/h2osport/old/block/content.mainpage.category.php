<section class="product-category section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title text-center">
					<h2>Популярные разделы</h2>
				</div>
			</div>
			<div class="col-md-6">
				<div class="category-box category-box-2">
					<?
					$APPLICATION->IncludeComponent(
							"bitrix:advertising.banner", "", Array(
						"CACHE_TIME" => "0",
						"CACHE_TYPE" => "A",
						"NOINDEX" => "N",
						"QUANTITY" => "1",
						"TYPE" => "MAINPAGE_CATEGORY_3"
							)
					);
					?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="category-box">
					<?
					$APPLICATION->IncludeComponent(
							"bitrix:advertising.banner", "", Array(
						"CACHE_TIME" => "0",
						"CACHE_TYPE" => "A",
						"NOINDEX" => "N",
						"QUANTITY" => "1",
						"TYPE" => "MAINPAGE_CATEGORY_1"
							)
					);
					?>	
				</div>
				<div class="category-box">
					<?
					$APPLICATION->IncludeComponent(
							"bitrix:advertising.banner", "", Array(
						"CACHE_TIME" => "0",
						"CACHE_TYPE" => "A",
						"NOINDEX" => "N",
						"QUANTITY" => "1",
						"TYPE" => "MAINPAGE_CATEGORY_2"
							)
					);
					?>
				</div>
			</div>
		</div>
	</div>
</section>