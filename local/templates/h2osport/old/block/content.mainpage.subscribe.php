<section class="call-to-action bg-gray section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="title">
					<h2>ПОДПИСКА НА НОВОСТИ</h2>
					<p>Введите свой EMail адрес для того, чтобы получать обновления на сайте</p>
				</div>
				<div class="col-lg-6 col-md-offset-3">
					<?
					$APPLICATION->IncludeComponent(
							"bitrix:sender.subscribe", "mainpage", Array(
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"CONFIRMATION" => "Y",
						"SET_TITLE" => "N",
						"SHOW_HIDDEN" => "N",
						"USE_PERSONALIZATION" => "N"
							)
					);
					?>
					<div class="modal subscribe-modal fade" id="subscribe-modal" style="display: none;">
						<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
							<i class="tf-ion-close"></i>
						</button>
						<div class="modal-dialog" role="dialog">
							<div class="modal-content">
								<div class="modal-body">
									<div class="row">
										<div class="col-md-12">
											<div class="modal-content-text">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
