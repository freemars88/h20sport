<?

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-jquery/jquery-3.2.1.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/01-jquery/jquery-migrate-1.4.1.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-js.cookie/js.cookie.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/03-jquery.mousewhell/jquery.mousewheel.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-canjs/can.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/transition.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/alert.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/button.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/carousel.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/collapse.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/dropdown.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/modal.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/tooltip.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/popover.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/scrollspy.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/tab.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-bootstrap/affix.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/jquery.bootstrap-touchspin.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/instafeed.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/ekko-lightbox.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/jquery.countdown.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/jquery.themepunch.tools.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/jquery.themepunch.revolution.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.actions.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.carousel.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.kenburn.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.layeranimation.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.migration.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.navigation.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.parallax.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.slideanims.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/revolution.extension.video.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-template-lib/warning.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/07-cz/cz.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/08-jquery.maskedinput/jquery.maskedinput.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/09-jquery.validation/jquery.validate.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/09-jquery.validation/additional-methods.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/09-jquery.validation/localization/messages_ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/10-kladr/jquery.kladr.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/11-uitotop/jquery.ui.totop.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/12-jquery.ui/jquery-ui.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/12-jquery.ui/datepicker-ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/13-fancyapps/jquery.fancybox.js", INCLUDE_MINIFY_WEB_CONTENT);

/*
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/02-device.js/device.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/03-jquery.resize/jquery.ba-resize.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/04-jquery.easing/jquery.easing.1.3.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/05-jquery.touch.swipe/jquery.touchSwipe.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/06-fvf/fvf.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/07-jquery.form/jquery.form.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/08-wow/wow.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/09-owl.carousel2/owl.carousel.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/10-isotope/isotope.pkgd.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/11-photo.swipe/photoswipe.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/11-photo.swipe/photoswipe-ui-default.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/12-rd.navbar/jquery.rd-navbar.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/13-ui.to.top/jquery.ui.totop.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/14-bootstrap/bootstrap.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/15-rd.input.label/rd.input.label.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/16-rd.google.map/rd.google.map.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/17-rd.parallax/jquery.rd-parallax.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/18-swiper/swiper.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/19-jquery.count.to/jquery.countTo.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/20-jquery.circle.progress/circle-progress.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/21-time.circle/TimeCircles.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/22-rd.flickfeed/jquery.rd-flickr-gallery.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/23-select2/select2.full.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/23-select2/ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/24-rd.audio/rd.audio.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/25-rd.video.player/rd.video.player.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/26-maginfic-popup/jquery.magnific-popup.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/27-rd.video/jquery.rd-video.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/28-smooth.scroll/smooth.scroll.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/29-slick/slick.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/30-jquery.countdown/jquery.plugin.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/30-jquery.countdown/jquery.countdown.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/30-jquery.countdown/jquery.countdown-ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/32-moment/moment.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/32-moment/ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/33-bootstrap-material-dtp/bootstrap-material-datetimepicker.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/33-bootstrap-material-dtp/bootstrap-material-datetimepicker.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/34-rd.range/rd.range.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/35-stepper/stepper.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/36-jplayer/jplayer.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/36-jplayer/playlist.js", INCLUDE_MINIFY_WEB_CONTENT);


CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/100-canjs/can.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/101-jquery.validation/jquery.validate.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/101-jquery.validation/additional-methods.js", INCLUDE_MINIFY_WEB_CONTENT);
CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/101-jquery.validation/localization/messages_ru.js", INCLUDE_MINIFY_WEB_CONTENT);

CWebavkTmplProTools::doAddJSFile(SITE_TEMPLATE_PATH . "/lib/102-jquery.maskedinput/jquery.maskedinput.js", INCLUDE_MINIFY_WEB_CONTENT);

*/