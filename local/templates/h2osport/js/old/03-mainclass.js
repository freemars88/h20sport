var MainControl = can.Control.extend({
	defaults: {}
}, {
	init: function (el, options) {
		var self = this;
		self.root = el;
		self.oldwidth = 0;
		var controlObj = this;
		CloudZoom.quickStart();
		$("#product-quantity").TouchSpin({});
		$("#product-quantity").on("touchspin.on.stopspin", function (el, ev) {
			$("#product-buy-form input[name='quantity']").val($("#product-quantity").val());
		});
		$("input[name='USER_PERSONAL_PHONE']").mask("+7 (999) 999-99-99");
		$("input[name='form_text_2']").mask("+7 (999) 999-99-99");
		$(".feedback-page form").validate({
			errorLabelContainer: "#errorMessages",
			focusInvalid: true,
			rules: {
				"form_text_1": {
					required: true
				},
				"form_text_2": {
					required: true
				},
				"form_email_3": {
					required: true,
					email: true
				},
				"form_textarea_4": {
					required: true
				}
			}
		});
		self.doCheckOrderInfoScroll();
		$(window).scroll(function () {
			self.doCheckOrderInfoScroll();
		});
		/*
		$(".popup-material").click(function () {
			$.fancybox.open('<div style="max-width:400px;">' + arElementMaterials[$(this).data("material-id")].PREVIEW_TEXT + '</div>');
		});*/
		alert(2);
		$("body").on("change", "#offerconfirm", function () {
			alert(1);
			if ($(this).is(":checked"))
			{
				$(".btn-order-save").removeClass("btn-disabled");
			} else {
				$(".btn-order-save").addClass("btn-disabled");
			}
		});
		$(self.root).on("click", ".catalog-showmore", function () {
			$(this).hide();
			$(".pagination-also-loader").show();
			self.doShowMoreClick($(self.root).find(".catalog-showmore"));
		});
		$(self.root).on("mouseenter", ".product-item", function () {
			var stub = $(this).parents(".product-item-area").find(".product-item-stub");
			stub.height($(this).parents(".product-item-area").outerHeight());
			stub.show();
			$(this).addClass("hover");
		});
		$(self.root).on("mouseleave", ".product-item", function () {
			var stub = $(this).parents(".product-item-area").find(".product-item-stub");
			stub.height("auto");
			stub.hide();
			$(this).removeClass("hover");
		});
		$("#oneclick-phone").mask("+7 (999) 999-99-99");
		$("body").on("click",".btn-select-shop-oneclick",function(){
			var el=$(this);
			$(".popup-element-oneclick-form input[name='storeid']").val($(el).data("storeid"));
			$(".popup-element-oneclick-form input[name='storexmlid']").val($(el).data("storexmlid"));
			$(".popup-element-oneclick-form input[name='storename']").val($(el).data("storetitle"));
			$(".popup-element-oneclick-form #oneclick-shop").val($(el).data("shopname"));
			$(".popup-element-oneclick-form input[name='id']").val($(".product-size-content .selected").data("id"));
			$(".popup-element-oneclick-form").show();
		});
		
		$("#popup-element-oneclick #oneclick-phone, #popup-element-oneclick #oneclick-fio").change(function () {
			if ($(this).val().length < 1)
			{
				$(this).parents(".form-group:first").addClass("error");
			} else {
				$(this).parents(".form-group:first").removeClass("error");
			}
		});
		$(".product-oneclick-buy-button").click(function () {
			var self = controlObj;
			var el = this;
			if ($(el).is(".disabled"))
			{
				return false;
			} else {
				if (parseInt($(el).data("id")) > 0 || true)
				{
					var error = false;
					var errorText = "";
					if ($("#popup-element-oneclick #oneclick-phone").val().length < 1)
					{
						$("#popup-element-oneclick #oneclick-phone").parents(".form-group:first").addClass("error");
						errorText = errorText + "Введите свой телефон<br>";
						var error = true;
					} else {
						$("#popup-element-oneclick #oneclick-phone").parents(".form-group:first").removeClass("error");
					}
					if ($("#popup-element-oneclick #oneclick-fio").val().length < 1)
					{
						$("#popup-element-oneclick #oneclick-fio").parents(".form-group:first").addClass("error");
						errorText = errorText + "Укажите свои ФИО";
						var error = true;
					} else {
						$("#popup-element-oneclick #oneclick-fio").parents(".form-group:first").removeClass("error");
					}

					if (!error)
					{
						$("#popup-element-oneclick .error-text").html("");
						$("#popup-element-oneclick .error-text").hide();
						$.fancybox.close(true);
						$(el).attr("disabled", "disabled");
						$.ajax("/ajax/actions.php", {
							data: {
								id: $("#popup-element-oneclick input[name='id']").val(),
								storeid: $("#popup-element-oneclick input[name='storeid']").val(),
								storexmlid: $("#popup-element-oneclick input[name='storexmlid']").val(),
								storename: $("#popup-element-oneclick input[name='storename']").val(),
								phone: $("#popup-element-oneclick #oneclick-phone").val(),
								fio: $("#popup-element-oneclick #oneclick-fio").val(),
								ajaxAction: 'oneClickBuy'
							},
							context: $(el),
							dataType: "json",
							method: "POST",
							success: function (data) {
								$(el).removeAttr("disabled");
								if (data.is_ok)
								{
									$.fancybox.open(data.title);
									//self.doShowModal("addbasketFormOk", data.title, data.message);
									//self.doUpdateBasketCnt();
									//self.doPopupBasketAdd(elementId);
									//$(el).removeClass("-is-active");
									//$(el).attr("href", "/personal/cart/");
								} else {
									$.fancybox.open(data.title);
								}
							}
						});
						return false;
					} else {
						$("#popup-element-oneclick .error-text").html(errorText);
						$("#popup-element-oneclick .error-text").show();
					}
				}
			}
		});
		$(window).resize(function () {
			self.doCheckResize();
		});
		self.doCheckResize();
		/*
		 self.overlay = $('.js-overlay');
		 $().UItoTop({easingType: 'easeOutQuart', containerClass: 'ui-to-top'});
		 $(".fancybox-catalog").fancybox({});
		 $(".fancybox-inlife").fancybox({});
		 $("#btn-get-news-next").on("click", function () {
		 self.doBtnGetNewsNext($(this));
		 return false;
		 });
		 $(self.root).on("click", "#btn-get-news-prev", function () {
		 self.doBtnGetNewsPrev($(this));
		 return false;
		 });
		 if ($("table.endtime").length > 0)
		 {
		 $("body").everyTime(1000, function () {
		 self.doCheckEndtimeTable();
		 });
		 }
		 $("#modal-review").appendTo(".js-modal-tpl");
		 $("#modal-send").appendTo(".js-modal-tpl");
		 $("#modal-send").removeClass("hidden");
		 $("#modal-send form").validate({
		 //errorLabelContainer: "#errorMessages",
		 rules: {
		 "FRIEND_NAME": {
		 required: true
		 },
		 "FRIEND_EMAIL": {
		 required: true,
		 email: true
		 }
		 },
		 submitHandler: function (form) {
		 $.ajax("/ajax/actions.php", {
		 data: $(form).serialize() + "&ajaxAction=sendProduct",
		 context: $(form),
		 dataType: "html",
		 method: "POST",
		 success: function (data) {
		 if (data != 'fail')
		 {
		 $("#modal-send .b-modal__content").html(data);
		 } else {
		 
		 }
		 }
		 });
		 return false;
		 }
		 
		 });
		 $("#select-student-school").parents('.js-select').each(function () {
		 var $selectContainer = $(this);
		 $(this).find('select').select2({
		 width: 'auto',
		 minimumResultsForSearch: -1,
		 dropdownAutoWidth: 'true',
		 dropdownParent: $selectContainer
		 });
		 $(this).on('select2:open', function () {
		 $selectContainer.addClass('-is-active')
		 });
		 $(this).on('select2:close', function () {
		 $selectContainer.removeClass('-is-active')
		 });
		 $(this).on('select2:select', function (ev) {
		 self.doChangeStudentSchool();
		 });
		 });
		 $("#select-student-course").parents('.js-select').each(function () {
		 var $selectContainer = $(this);
		 $(this).find('select').select2({
		 width: 'auto',
		 minimumResultsForSearch: -1,
		 dropdownAutoWidth: 'true',
		 dropdownParent: $selectContainer
		 });
		 $(this).on('select2:open', function () {
		 $selectContainer.addClass('-is-active')
		 });
		 $(this).on('select2:close', function () {
		 $selectContainer.removeClass('-is-active')
		 });
		 $(this).on('select2:select', function (ev) {
		 self.doChangeStudentCourse();
		 });
		 });
		 */
	},
	/*"form .fn-button-submit click": function (el, ev) {
	 $(el).parents("form").trigger("submit");
	 return false;
	 },*/
	".sender-subscribe-button-submit click": function (el, ev) {
		var self = this;
		//$(el).find(".subscribe-submit").attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: $(el).parents("form").serialize() + "&ajaxAction=subscribe",
			context: $(el).parents("form"),
			dataType: "json",
			method: "POST",
			success: function (data) {
				if (data.is_ok)
				{
					$("#subscribe-modal .modal-content-text").html(data.message);
					$('#subscribe-modal').modal('show');
				} else {
					$("#subscribe-modal .modal-content-text").html(data.message);
					$('#subscribe-modal').modal('show');
				}
			}
		});
		return false;
	},
	".add-basket click": function (el, ev) {
		var self = this;
		if ($(el).is(".-is-active"))
		{
			self.addToBasket($(el).data("element-id"), el, 1);
		}
	},
	".add-basket-product click": function (el, ev) {
		var self = this;
		self.addToBasket($(el).data("element-id"), el, 1);
	},
	".add-to-basket-offer click": function (el, ev) {
		var self = this;
		var offerId = $(el).parents("form").find("input[name='id']").val();
		var offerCnt = $(el).parents("form").find("input[name='quantity']").val();
		if (!$(self).is(".disabled"))
		{
			self.addToBasketOffer(offerId, $(el), offerCnt);
		}
	},
	"#product-quantity change": function (el, ev) {
		$("#product-buy-form input[name='quantity']").val($(el).val());
	},
	/*
	 ".add-to-basket-offers click": function (el, ev) {
	 var self = this;
	 var elements = [];
	 var cnt = [];
	 var i = 0;
	 $(el).parents(".sku-list-area").find("tr.offer-line").each(function (k, line) {
	 if (parseInt($(line).find(".offer-cnt").val()) > 0)
	 {
	 elements[i] = $(line).find(".offer-cnt").data("id");
	 cnt[i] = $(line).find(".offer-cnt").val();
	 i++;
	 }
	 });
	 if (i > 0)
	 {
	 self.addToBasketOffers(elements, cnt, el);
	 }
	 return false;
	 },*/
	addToBasket: function (elementId, el, cnt) {
		var self = this;
		$(el).attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&cnt=" + cnt + "&ajaxAction=addToBasket",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				$(el).removeAttr("disabled");
				if (data.is_ok)
				{
					//self.doShowModal("addbasketFormOk", data.title, data.message);
					self.doUpdateBasketCnt();
					//$(el).removeClass("-is-active");
					$(el).attr("href", "/personal/cart/");
				}
			}
		});
		return false;
	},
	/*addToBasketOffers: function (elementsId, cntList, el) {
	 var self = this;
	 var request = {
	 ajaxAction: "addToBasket",
	 id: elementsId,
	 cnt: cntList
	 };
	 $(el).attr("disabled", "disabled");
	 $.ajax("/ajax/actions.php", {
	 data: request,
	 context: $(el),
	 dataType: "json",
	 method: "GET",
	 success: function (data) {
	 $(el).removeAttr("disabled");
	 if (data.is_ok)
	 {
	 //$(this).parents(".sku-list-area").find("tr.offer-line .offer-cnt").val("0");
	 //$(this).parents(".sku-list-area").find("tr.offer-line .basket .add-to-basket-offer").data("cnt", "0");
	 //$.each(elementsId, function (k, v) {
	 //$(el).parents(".sku-list-area").find("tr.offer-line .offer-cnt[data-id=" + v + "]").val("0");
	 //$(el).parents(".sku-list-area").find("tr.offer-line .offer-cnt[data-id=" + v + "]").parents("tr.offer-line").addClass("-is-added");
	 //});
	 //$(this).parents(".sku-list-area").find(".cnt-button-area .buy-button").html("Р’С‹Р±РµСЂРёС‚Рµ С‚РѕРІР°СЂС‹");
	 //$("[data-id='" + elementId + "']").val("0");
	 //self.doShowModal("addbasketFormOk", data.title, data.message);
	 self.doUpdateBasketCnt();
	 }
	 }
	 });
	 return false;
	 },*/
	addToBasketOffer: function (elementId, el, cnt) {
		var self = this;
		$(el).attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&cnt=" + cnt + "&ajaxAction=addToBasket",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				$(el).removeAttr("disabled");
				if (data.is_ok)
				{
					if (data.is_ok)
					{
						$("#product-modal .modal-content-text").html(data.message);
						$('#product-modal').modal('show');
					} else {
						$("#product-modal .modal-content-text").html(data.message);
						$('#product-modal').modal('show');
					}
					//$(this).parents("tr").find(".offer-cnt").val("0");
					//$(this).parents("tr").addClass("-is-added");
					//self.doShowModal("addbasketFormOk", data.title, data.message);
					self.doUpdateBasketCnt();
				}
			}
		});
		return false;
	},
	doUpdateBasketCnt: function () {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "ajaxAction=updateBasketCnt",
			dataType: "json",
			method: "GET",
			success: function (data) {
				$("#header-cart-content").html(data.content);
				//$(".basket-mobile-area").html(data.mobile);
				//tmpEl.before($(data));
				//tmpEl.remove();
			}
		});
		return false;
	},
	/*
	 '.js-counter-button click': function (el, ev) {
	 var $button = $(el);
	 var $counterInput = $button.parents(".js-counter").find('.js-counter-input');
	 if ($counterInput.val() >= 0) {
	 var value = +$counterInput.val();
	 $counterInput.val(value + $button.data('counter-action'));
	 }
	 if ($counterInput.val() <= 0) {
	 $counterInput.val(0);
	 }
	 },
	 */
	".add-favorite click": function (el, ev) {
		var self = this;
		if (parseInt($(el).data("id")) > 0)
		{
			self.addToWishlist(el, $(el).data("id"));
		}
	},
	addToWishlist: function (el, elementId) {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&ajaxAction=addToWishlist",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok)
				{
					self.doUpdateWishListCnt();
				}
			}
		});
		return false;
	},
	".del-favorite click": function (el, ev) {
		var self = this;
		if (parseInt($(el).data("element-id")) > 0)
		{
			self.deleteFromWishlist(el, $(el).data("element-id"));
		}
	},
	deleteFromWishlist: function (el, elementId) {
		var self = this;
		$(el).attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&ajaxAction=deleteFromWishlist",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				$(this).removeAttr("disabled");
				if (data.is_ok)
				{
					/*$(this).parents(".b-catalogue__item").fadeOut(500, function () {
					 self.remove();
					 });
					 */
					self.doUpdateWishListCnt();
				}
			}
		});
		return false;
	},
	doUpdateWishListCnt: function () {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "ajaxAction=updateWishlistCnt",
			dataType: "json",
			method: "GET",
			success: function (data) {
				$("#header-fav-content").html(data.content);
				//tmpEl.before($(data));
				//tmpEl.remove();
			}
		});
		return false;
	},
	".product-size-content a click": function (el, ev) {
		$(".product-size-content a.selected").removeClass("selected");
		$(el).addClass("selected");
		$("#product-buy-form input[name='id']").val($(el).data("id"));
		if (parseInt($("#product-buy-form input[name='id']").val()) > 0)
		{
			$("#product-buy-button").removeClass("disabled");
			$(".note-buy-text").hide();
		} else {
			$("#product-buy-button").addClass("disabled");
			$(".note-buy-text").show();
		}
		var max = $(el).data("quantity");
		$("#product-quantity").trigger("touchspin.updatesettings", {max: max});
		if (parseInt($("#product-quantity").val()) < 1)
		{
			$("#product-quantity").val(1);
		}
		$("#product-reserve-button").removeClass("disabled");
		$(".popup-element-oneclick-map").hide();
		$(".popup-element-oneclick-map[data-offerid='" + $(el).data("id") + "']").show();

		/*
		 $(".product-store").show();
		 $(".product-store .table").hide();
		 $(".product-store .table[data-offerid='" + $(el).data("id") + "']").show();
		 */
	},
	".header-basket-remove click": function (el, ev) {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "id=" + $(el).data("id") + "&ajaxAction=deleteFromHeaderBasket",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok)
				{
					self.doUpdateBasketCnt();
				}
			}
		});
		return false;
	},
	".header-fav-remove click": function (el, ev) {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "id=" + $(el).data("id") + "&ajaxAction=deleteFromWishlist",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok)
				{
					self.doUpdateWishListCnt();
				}
			}
		});
		return false;
	},
	".top-menu .dropdown-toggle mouseenter": function (el, ev) {
		$(el).find("span").fadeIn();
	},
	".top-menu .dropdown-toggle mouseleave": function (el, ev) {
		$(el).find("span").hide();
	},
	"#sort-field change": function (el, ev) {
		$(el).parents("form").submit();
	},
	doCheckOrderInfoScroll: function () {
		var blockScroll = $(".order-checkout .product-checkout-details");
		if (blockScroll.length > 0)
		{
			var offset = $(".order-checkout:first").offset();
			var winScroll = $(window).scrollTop();
			if (winScroll > offset.top)
			{
				blockScroll.css("top", (winScroll - offset.top) + "px");//animate({"top":(winScroll-offset.top)+"px"}, "fast");
			} else {
				blockScroll.css("top", 0);
			}
		}
	},
	".front-panel #front-panel-show click": function (el, ev) {
		if ($(".front-panel").is(".open"))
		{
			$(".front-panel").removeClass("open");
			Cookies.set('BITRIX_SM_FRONT_PANEL', 'CLOSE', {path: '/'});
			$("#front-panel-show").html("Ещё");
		} else {
			$(".front-panel").addClass("open");
			Cookies.set('BITRIX_SM_FRONT_PANEL', 'OPEN', {path: '/'});
			$("#front-panel-show").html("Скрыть");
		}
		return false;
	},
	doShowMoreClick: function (el)
	{
		var self = this;
		$.ajax($(el).data("href"), {
			data: "ajaxmore=Y",
			dataType: "html",
			method: "POST",
			success: function (data) {
				$("body").append('<div id="catalog-answer-content" style="display:none;"></div>');
				$("#catalog-answer-content").append(data);
				$(self.root).find("#catalog-section-items").append($("#catalog-answer-content").find("#catalog-section-items").html());
				$(self.root).find(".catalog-section-navs").html($("#catalog-answer-content").find(".catalog-section-navs:last").html());
				$("#catalog-answer-content").remove();
			}
		});
	},
	".b-oneclick-pickup click": function (el, ev) {
		var self = this;
		if ($(el).is(".disabled"))
		{
			return false;
		} else {
			$("#popup-element-oneclick input[name='storeid']").val($(el).data("storeid"));
			$("#popup-element-oneclick input[name='storexmlid']").val($(el).data("storexmlid"));
			$("#popup-element-oneclick input[name='storename']").val($(el).data("storetitle"));
			$("#popup-element-oneclick input[name='id']").val($(".product-size-content .selected").data("id"));
			$.fancybox.open($("#popup-element-oneclick"), {
				caption: "Заказ в 1 клик"
			});
		}
	},
	doCheckResize: function () {
		var self = this;
		if ($(".metromap").length > 0)
		{
			$(".metromap").each(function () {
				self.doCheckResizeMapMetro($(this));
			});
		}
		if (self.oldwidth == 0)
		{
			self.oldwidth = $(window).width();
			if ($(window).width() < 768)
			{
				self.doHideFilterExpand();
			}
		}
		if ($(window).width() < 768)
		{
			if (self.oldwidth >= 768)
			{
				self.doHideFilterExpand();
			}
		}
		self.oldwidth = $(window).width();
	},
	doCheckResizeMapMetro: function (el) {
		var cont = el.find(".metromapcontent");
		var coeff = parseInt(el.width())*1.0 / parseInt(cont.data("imagewidth"))*1.0;
		var height = parseInt(parseInt(cont.data("imageheight")) * coeff);
		cont.height(height);
		cont.css("background-size", parseInt(el.width()) + "px " + height + "px");
		el.find(".metroitem").each(function () {
			var item = $(this);
			item.width(parseInt(parseInt(item.data("basewidth")) * coeff));
			item.height(parseInt(parseInt(item.data("baseheight")) * coeff));
			item.css("left", parseInt(parseInt(item.data("x")) * coeff));
			item.css("top", parseInt(parseInt(item.data("y")) * coeff));
		});
	},
	".metroitem click": function (el, ev) {
		$.fancybox.open($($(el).data("clicklink")), {
			touch: {
				vertical: false,
				momentum: false
			},
		});
	},
	"#product-reserve-button click": function (el, ev) {
		$.fancybox.open($("#popup-element-oneclick"), {
			touch: {
				vertical: false,
				momentum: false
			},
		});
	},
	doHideFilterExpand: function(){
		$(".smartfilter .panel-title a[aria-expanded='true']").trigger("click");
	}
	/*
	 "#button-feedback click": function (el, ev) {
	 var self = this;
	 $.ajax("/ajax/actions.php", {
	 data: "ajaxAction=getFeedback",
	 context: $(el),
	 dataType: "html",
	 method: "GET",
	 success: function (data) {
	 $("#modal-feedback .b-modal__content").html(data);
	 self.doInitValidateFeedback();
	 }
	 });
	 return false;
	 },
	 */
	/*doInitValidateFeedback: function () {
	 $("#modal-feedback form").validate({
	 //errorLabelContainer: "#errorMessages",
	 rules: {
	 "form_text_7": {
	 required: true
	 },
	 "form_email_8": {
	 required: true,
	 email: true
	 },
	 "form_textarea_9": {
	 required: true
	 }
	 },
	 submitHandler: function (form) {
	 $(form).find(".send-button").attr("disabled", "disabled");
	 $.ajax($(form).attr("action"), {
	 data: $(form).serialize() + "&ajaxAction=getFeedback",
	 context: $(form),
	 dataType: "html",
	 method: "POST",
	 success: function (data) {
	 $(form).find(".send-button").removeAttr("disabled");
	 $("#modal-feedback .b-modal__content").html(data);
	 }
	 });
	 return false;
	 }
	 
	 });
	 $("#modal-feedback form .js-select").each(function () {
	 if (!$(this).is(".nodefault"))
	 {
	 var $selectContainer = $(this);
	 $(this).find('select').select2({
	 width: 'auto',
	 minimumResultsForSearch: -1,
	 dropdownAutoWidth: 'true',
	 dropdownParent: $selectContainer
	 });
	 $(this).on('select2:open', function () {
	 $selectContainer.addClass('-is-active')
	 });
	 $(this).on('select2:close', function () {
	 $selectContainer.removeClass('-is-active')
	 });
	 $(this).on('select2:select', function (ev) {
	 $selectContainer.find('select').trigger("change");
	 if ($selectContainer.find('select').val() == "10" || $selectContainer.find('select').val() == "11")
	 {
	 $("#form-feedback-note").show();
	 } else {
	 $("#form-feedback-note").hide();
	 }
	 });
	 }
	 });
	 },
	 */
});
