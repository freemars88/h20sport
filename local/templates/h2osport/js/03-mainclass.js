var MainControl = can.Control.extend({
	defaults: {}
}, {
	init: function (el, options) {
		var self = this;
		self.root = el;
		$("body").on("click", ".detail-place", function () {
			$(".detail-place-area").hide();
			$(".detail-place-area[data-id='" + $(this).data("id") + "']").show();
			return true;
		});
		$("body").on("mouseenter", ".pro-rating a", function () {
			var proRating = $(this).parents(".pro-rating");
			var iIndex = parseInt($(this).data("id"));
			if (proRating.is(".actived")) {
				proRating.find("a").each(function (i, el) {
					var iStart = $(el).find("i");
					iStart.data("oldclass", iStart.attr("class"));
					if (parseInt($(el).data("id")) <= iIndex) {
						iStart.attr("class", "zmdi zmdi-star");
					} else {
						iStart.attr("class", "zmdi zmdi-star-outline");
					}
				});

			}
		});
		$("body").on("mouseleave", ".pro-rating a", function () {
			var proRating = $(this).parents(".pro-rating");
			if (proRating.is(".actived")) {
				proRating.find("a").each(function (i, el) {
					var iStart = $(el).find("i");
					iStart.attr("class", iStart.data("oldclass"));
					iStart.data("oldclass", "");
				});
			}
		});
		$("body").on("click", ".pro-rating a", function () {
			var proRating = $(this).parents(".pro-rating");
			if (proRating.is(".actived")) {
				$.ajax("/ajax/actions.php", {
					data: {
						"vote_id": proRating.data("id"),
						"vote": "Y",
						"rating": $(this).data("id"),
						"ajaxAction": "voteItem",
						"extclass": proRating.data("extclass"),
						"AJAX_CALL": "Y"
					},
					context: proRating,
					dataType: "json",
					method: "POST",
					success: function (data) {
						proRating.removeClass("actived");
						if (data.is_ok) {
							$(data.html).insertAfter(proRating);
							proRating.remove();
						}
					}
				});
			}
		});
		$("body").on("click", ".quick-view-button", function () {
			var ModalContent = $("#productModal .modal-product");
			ModalContent.html("");
			$.ajax("/ajax/actions.php", {
				data: {
					"id": $(this).data("id"),
					"ajaxAction": "quickView"
				},
				context: ModalContent,
				dataType: "json",
				method: "GET",
				success: function (data) {
					if (data.is_ok) {
						ModalContent.html(data.html);
					}
				}
			});
		});
		$("body").on("click", "#b-showsizetable", function () {
			$("#quickview-wrapper .modal-product").html($("#b-showsizetable-content").html());
		});
		$("body").on("click", "#b-onelicklink", function () {
			$("#onclickbuy-wrapper .modal-product").html($("#popup-element-oneclick").html());
			$("#onclickbuy-wrapper .modal-product .product-oneclick-buy-button").data("id", $(".b-detail-add-basket").data("offer-id"));
		});
		$("body").on("click", "form.cart .single_add_to_cart_button", function () {
			self.addToBasket($(this).data("element-id"), $(this), $("form.cart #french-hens").val());
			return false;
		});
		$("body").on("change", "#offerconfirm", function () {
			if ($(this).is(":checked")) {
				$(".btn-order-save").removeClass("btn-disabled");
			} else {
				$(".btn-order-save").addClass("btn-disabled");
			}
		});
		if ($("#b-product-detail").length > 0) {
			if (window.location.hash.substr(1).length > 0) {
				var hash = window.location.hash.substr(1);
				if ($(".b-color-size-select").length > 0) {
					var el = $(".b-color-size-select a[data-offer-id='" + hash + "']");
					var areaSelect = el.parents(".b-color-size-select");
					areaSelect.find("li.active").removeClass("active");
					el.parents("li:first").addClass("active");
					$(".b-detail-add-basket").data("offer-id", el.data("offer-id"));
					$(".b-detail-oneclick").data("offer-id", el.data("offer-id"));
					$("#b-price-product").html(el.data("price"));
					$("#b-old-price-product").html(el.data("old-price"));
					if (el.data("show-discount") == "Y") {
						$("#b-old-price-product").show();
					} else {
						$("#b-old-price-product").hide();
					}
					var colorId = el.parents("li:first").data("color-id");
					var areaColorSelect = $(".b-color-select");
					areaColorSelect.find("li.active").removeClass("active");
					areaColorSelect.find("[data-color-id='" + colorId + "']").parents("li:first").addClass("active");
					//$(".b-color-size-select li.active").removeClass("active");
					$(".b-color-size-select li[data-isoffer='1']").hide();
					$(".b-color-size-select li[data-color-id='" + colorId + "']").show();

				} else if ($(".b-only-size-select").length > 0) {
					var el = $(".b-only-size-select a[data-offer-id='" + hash + "']");
					var areaSelect = el.parents(".b-only-size-select");
					areaSelect.find("li.active").removeClass("active");
					el.parents("li:first").addClass("active");
					$(".b-detail-add-basket").data("offer-id", el.data("offer-id"));
					$(".b-detail-oneclick").data("offer-id", el.data("offer-id"));
					$("#b-price-product").html(el.data("price"));
					$("#b-old-price-product").html(el.data("old-price"));
					if (el.data("show-discount") == "Y") {
						$("#b-old-price-product").show();
					} else {
						$("#b-old-price-product").hide();
					}
				}
			}
		}
		$("body").on("click", ".product-oneclick-buy-button", function () {
			//var self = controlObj;
			var el = this;
			if ($(el).is(".disabled")) {
				return false;
			} else {
				if (parseInt($(el).data("id")) > 0 || true) {
					var error = false;
					var errorText = "";
					/*
					 if ($("#onclickbuy-wrapper #popup-element-oneclick-form .product-oneclick-buy-button").data("id").length < 1)
					 {
					 $("#onclickbuy-wrapper #popup-element-oneclick-form .product-size-content").addClass("error");
					 errorText = errorText + "Выберите размер<br>";
					 var error = true;
					 } else {
					 $("#onclickbuy-wrapper #popup-element-oneclick-form .product-size-content").removeClass("error");
					 }*/
					if ($("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-phone").val().length < 1) {
						$("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-phone").parents(".form-group:first").addClass("error");
						errorText = errorText + "Введите свой телефон<br>";
						var error = true;
					} else {
						$("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-phone").parents(".form-group:first").removeClass("error");
					}
					if ($("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-fio").val().length < 1) {
						$("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-fio").parents(".form-group:first").addClass("error");
						errorText = errorText + "Укажите свои ФИО";
						var error = true;
					} else {
						$("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-fio").parents(".form-group:first").removeClass("error");
					}
					if (!error) {
						$("#onclickbuy-wrapper #popup-element-oneclick-form .error-text").html("");
						$("#onclickbuy-wrapper #popup-element-oneclick-form .error-text").hide();
						//$.fancybox.close(true);
						$(el).attr("disabled", "disabled");
						$.ajax("/ajax/actions.php", {
							data: {
								id: $(el).data("id"),
								phone: $("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-phone").val(),
								fio: $("#onclickbuy-wrapper #popup-element-oneclick-form #oneclick-fio").val(),
								ajaxAction: 'oneClickBuy'
							},
							context: $(el),
							dataType: "json",
							method: "POST",
							success: function (data) {
								$(el).removeAttr("disabled");
								if (data.is_ok) {
									$("#onclickbuy-wrapper .modal-product").html(data.title);
									//self.doShowModal("addbasketFormOk", data.title, data.message);
									//self.doUpdateBasketCnt();
									//self.doPopupBasketAdd(elementId);
									//$(el).removeClass("-is-active");
									//$(el).attr("href", "/personal/cart/");
								} else {
									$("#onclickbuy-wrapper .modal-product").html(data.title);
								}
							}
						});
						return false;
					} else {
						$("#onclickbuy-wrapper #popup-element-oneclick-form .error-text").html(errorText);
						$("#onclickbuy-wrapper #popup-element-oneclick-form .error-text").show();
					}
				}
			}
		});
		$("body").on("click", ".b-pickup-link", function () {
			var self = this;
			$("#b-deliveryall__area-btn").trigger("click");
			$(".b-pickup-list-item").removeClass("active");
			$(".b-pickup-list-item[data-id='" + $(this).data("id") + "']").addClass("active");
			if ($(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").length != 0) {
				$('html, body').animate({scrollTop: $(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").offset().top - 80}, 500, function () {
					if ($(window).scrollTop() != $(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").offset().top - 80) {
						$('html, body').animate({scrollTop: $(".b-pickup-list-item[data-id='" + $(self).data("id") + "']").offset().top - 80}, 500);
					}
				});
			}
		});
		$("body").on("click", "#b-deliveryall__area-btn", function () {
			var self = this;
			$("#b-deliveryall__area-button").hide();
			$("#b-deliveryall__area-content").show();
		});
		$(self.root).on("click", ".catalog-showmore", function () {
			$(this).parents(".pagination-also").hide();
			$(".pagination-also-loader").show();
			self.doShowMoreClick($(self.root).find(".catalog-showmore"));
		});
	},
	".add-to-basket click": function (el, ev) {
		var self = this;
		self.addToBasket($(el).data("element-id"), el, 1);
	},
	addToBasket: function (elementId, el, cnt) {
		var self = this;
		//$(el).attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&cnt=" + cnt + "&ajaxAction=addToBasket",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				//$(el).removeAttr("disabled");
				if (data.is_ok) {
					if (typeof productOffersECommerce != 'undefined') {
						window.dataLayer.push({
							"ecommerce": {
								"currencyCode": "RUB",
								"add": {
									"products": [productOffersECommerce[elementId]]
								}
							}
						});
					}
					//self.doShowModal("addbasketFormOk", data.title, data.message);
					self.doUpdateBasketCnt();
					//$(el).removeClass("-is-active");
					/*$(el).attr("href", "/personal/cart/");*/
				}
			}
		});
		return false;
	},
	doUpdateBasketCnt: function () {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "ajaxAction=updateBasketCnt",
			dataType: "json",
			method: "GET",
			success: function (data) {
				$("#header-cart-content").html(data.content);
				//$(".basket-mobile-area").html(data.mobile);
				//tmpEl.before($(data));
				//tmpEl.remove();
			}
		});
		return false;
	},
	".b-only-size-select a click": function (el, ev) {
		var areaSelect = $(el).parents(".b-only-size-select");
		areaSelect.find("li.active").removeClass("active");
		$(el).parents("li:first").addClass("active");
		$(".b-detail-add-basket").data("offer-id", $(el).data("offer-id"));
		$(".b-detail-oneclick").data("offer-id", $(el).data("offer-id"));
		$("#b-price-product").html($(el).data("price"));
		$("#b-old-price-product").html($(el).data("old-price"));
		if ($(el).data("show-discount") == "Y") {
			$("#b-old-price-product").show();
		} else {
			$("#b-old-price-product").hide();
		}
		if (parseInt($(el).data("offer-quantity")) > 0) {
			$(".presale-note").hide();
			$(".presale-text").hide();
		} else {
			$(".presale-note").show();
			$(".presale-text").show();
		}
	},
	".b-color-select a click": function (el, ev) {
		var areaSelect = $(el).parents(".b-color-select");
		areaSelect.find("li.active").removeClass("active");
		$(el).parents("li:first").addClass("active");
		$(".b-color-size-select li.active").removeClass("active");
		$(".b-color-size-select li[data-isoffer='1']").hide();
		$(".b-color-size-select li[data-color-id='" + $(el).data("color-id") + "']").show();
		$(".b-detail-add-basket").data("offer-id", "");
		$(".b-detail-oneclick").data("offer-id", "");
		var areaSizeSelect = $(el).parents(".b-color-size-select");
		areaSizeSelect.find("li.active").removeClass("active");
		var elSize = $(".b-color-size-select li[data-isoffer='1']:visible:first a");
		$(elSize).parents("li:first").addClass("active");
		$(".b-detail-add-basket").data("offer-id", $(elSize).data("offer-id"));
		$(".b-detail-oneclick").data("offer-id", $(elSize).data("offer-id"));
		$("#b-price-product").html($(elSize).data("price"));
		$("#b-old-price-product").html($(elSize).data("old-price"));
		if ($(elSize).data("show-discount") == "Y") {
			$("#b-old-price-product").show();
		} else {
			$("#b-old-price-product").hide();
		}
		if (parseInt($(elSize).data("offer-quantity")) > 0) {
			$(".presale-note").hide();
			$(".presale-text").hide();
		} else {
			$(".presale-note").show();
			$(".presale-text").show();
		}
	},
	".b-color-size-select a click": function (el, ev) {
		var areaSelect = $(el).parents(".b-color-size-select");
		areaSelect.find("li.active").removeClass("active");
		$(el).parents("li:first").addClass("active");
		$(".b-detail-add-basket").data("offer-id", $(el).data("offer-id"));
		$(".b-detail-oneclick").data("offer-id", $(el).data("offer-id"));
		$("#b-price-product").html($(el).data("price"));
		$("#b-old-price-product").html($(el).data("old-price"));
		if ($(el).data("show-discount") == "Y") {
			$("#b-old-price-product").show();
		} else {
			$("#b-old-price-product").hide();
		}
		if (parseInt($(el).data("offer-quantity")) > 0) {
			$(".presale-note").hide();
			$(".presale-text").hide();
		} else {
			$(".presale-note").show();
			$(".presale-text").show();
		}
	},
	".b-only-color-select a click": function (el, ev) {
		var areaSelect = $(el).parents(".b-only-color-select");
		areaSelect.find("li.active").removeClass("active");
		$(el).parents("li:first").addClass("active");
		$(".b-detail-add-basket").data("offer-id", $(el).data("offer-id"));
		$(".b-detail-oneclick").data("offer-id", $(el).data("offer-id"));
		$("#b-price-product").html($(el).data("price"));
		$("#b-old-price-product").html($(el).data("old-price"));
		if ($(el).data("show-discount") == "Y") {
			$("#b-old-price-product").show();
		} else {
			$("#b-old-price-product").hide();
		}
		if (parseInt($(el).data("offer-quantity")) > 0) {
			$(".presale-note").hide();
			$(".presale-text").hide();
		} else {
			$(".presale-note").show();
			$(".presale-text").show();
		}
	},
	".b-detail-add-basket click": function (el, ev) {
		var self = this;
		if (parseInt($(el).data("offer-id")) > 0) {
			self.addToBasket($(el).data("offer-id"), $(el), $("input[name='qtybutton']").val());
		}
		return false;
	},
	".b-detail-oneclick click": function (el, ev) {
		var self = this;
		if (parseInt($(el).data("offer-id")) > 0) {
			//self.addToBasket($(el).data("offer-id"), $(el), $("input[name='qtybutton']").val());
		}
		return false;
	},
	".add-favorite click": function (el, ev) {
		var self = this;
		if (parseInt($(el).data("id")) > 0) {
			self.addToWishlist(el, $(el).data("id"));
		}
	},
	addToWishlist: function (el, elementId) {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&ajaxAction=addToWishlist",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok) {
					//self.doUpdateWishListCnt();
				}
			}
		});
		return false;
	},
	".del-favorite click": function (el, ev) {
		var self = this;
		if (parseInt($(el).data("element-id")) > 0) {
			self.deleteFromWishlist(el, $(el).data("element-id"));
		}
	},
	deleteFromWishlist: function (el, elementId) {
		var self = this;
		$(el).attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: "id=" + elementId + "&ajaxAction=deleteFromWishlist",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				$(this).removeAttr("disabled");
				if (data.is_ok) {
					$(this).parents("tr").fadeOut(500, function () {
						$(this).parents("tr").remove();
					});
					//self.doUpdateWishListCnt();
				}
			}
		});
		return false;
	},
	".header-basket-remove click": function (el, ev) {
		var self = this;
		$.ajax("/ajax/actions.php", {
			data: "id=" + $(el).data("id") + "&ajaxAction=deleteFromHeaderBasket",
			context: $(el),
			dataType: "json",
			method: "GET",
			success: function (data) {
				if (data.is_ok) {
					self.doUpdateBasketCnt();
				}
			}
		});
		return false;
	},
	"#alphaDalliPickpont .alpha-list a click": function (el, ev) {
		if (!$(el).is(".disabled")) {
			$(".alpha-list a").removeClass("selected");
			$(el).addClass("selected");
			$(".alpha-cities").hide();
			$(".alpha-cities[data-symbol='" + $(el).data("symbol") + "']").show();
		}
	},
	doShowMoreClick: function (el) {
		var self = this;
		$.ajax($(el).data("href"), {
			data: "ajaxmore=Y",
			dataType: "html",
			method: "POST",
			success: function (data) {
				$("body").append('<div id="catalog-answer-content" style="display:none;"></div>');
				$("#catalog-answer-content").append(data);
				$(self.root).find("#catalog-section-items").append($("#catalog-answer-content").find("#catalog-section-items").html());
				$(self.root).find(".catalog-section-navs").html($("#catalog-answer-content").find(".catalog-section-navs:last").html());
				$("#catalog-answer-content").remove();
			}
		});
	},
	".row-set-product input change": function (el, ev) {
		$(el).parents(".row-set-product").find(".row-set-product-sku").removeClass("selected");
		$(el).parents(".row-set-product-sku").addClass("selected");
	},
	".b-detail-add-basket-set click": function (el, ev) {
		var self = this;
		self.addToBasketComplect();
		/*
		 if (parseInt($(el).data("offer-id")) > 0)
		 {
		 self.addToBasket($(el).data("offer-id"), $(el), $("input[name='qtybutton']").val());
		 }
		 */
		return false;
	},
	addToBasketComplect: function () {
		var self = this;
		var data = $("#set-product-form").serialize();
		//$(el).attr("disabled", "disabled");
		$.ajax("/ajax/actions.php", {
			data: data + "&ajaxAction=addToBasketComplect",
			context: $("#set-product-form"),
			dataType: "json",
			method: "GET",
			success: function (data) {
				//$(el).removeAttr("disabled");
				if (data.is_ok) {
					//self.doShowModal("addbasketFormOk", data.title, data.message);
					self.doUpdateBasketCnt();
					//$(el).removeClass("-is-active");
					/*$(el).attr("href", "/personal/cart/");*/
				}
			}
		});
		return false;
	},
	/*
	 doUpdateWishListCnt: function () {
	 var self = this;
	 $.ajax("/ajax/actions.php", {
	 data: "ajaxAction=updateWishlistCnt",
	 dataType: "json",
	 method: "GET",
	 success: function (data) {
	 $("#header-fav-content").html(data.content);
	 //tmpEl.before($(data));
	 //tmpEl.remove();
	 }
	 });
	 return false;
	 },*/
});
