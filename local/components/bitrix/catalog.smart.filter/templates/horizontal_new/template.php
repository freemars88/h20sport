<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$isExists = false;
if (!empty($arResult["ITEMS"])) {
	foreach ($arResult["ITEMS"] as $arItem) {
		if (!empty($arItem['VALUES'])) {
			$isExists = true;
			break;
		}
	}
}
if ($isExists) {
	?>
	<div class="product-filter bx-filter">
		<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">
			<?
			foreach ($arResult["HIDDEN"] as $arItem) {
				?>
				<input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>"/>
				<?
			}
			?>
			<div class="product-filter__props">
				<?
				//not prices
				foreach ($arResult["ITEMS"] as $key => $arItem) {
					if (empty($arItem["VALUES"]) || isset($arItem["PRICE"])) {
						continue;
					}
					if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)) {
						continue;
					}
					$cnt = 0;
					$arCurrent = array();
					$nonDis = 0;
					foreach ($arItem["VALUES"] as $val => $ar) {
						if ($ar['CHECKED']) {
							$cnt++;
							$arCurrent[] = $ar["VALUE"];
						}
						if (!$ar['DISABLED']) {
							$nonDis++;
						}
					}
					?>
					<div class="product-filter__item<?
					if ($arItem['CODE'] == "SIZE") {
						echo " product-filter__item-size";
					} elseif ($arItem['CODE'] == "COLOR") {
						echo " product-filter__item-color";
					}
					if ($nonDis <= 0) {
						echo " disabled";
					}
					$strCurrentText = "Все";
					if ($cnt > 0) {
						$strCurrentText = implode(", ", $arCurrent);
						if (strlen($strCurrentText) > 15) {
							$strCurrentText = substr($strCurrentText, 0, 12) . "...";
						}
					}
					?>" data-role="mainarea_<?= $arItem['ID'] ?>">
						<div class="product-filter__item-title" id="headingflt-<?= $arItem['ID'] ?>">
							<?/*if ($arItem['CODE'] == "INFLATABLE") {?>
								Надувной:
							<?} else */{?>
							<?= $arItem["NAME"] ?>:
							<?}?>
						</div>
						<?//if ($arItem['CODE'] != "INFLATABLE") {?>
						<div class="product-filter__item-current" id="headingflt-<?= $arItem['ID'] ?>">
							<?= $strCurrentText ?>
						</div>
						<?//}?>
						<div id="flt-<?= $arItem['ID'] ?>" class="product-filter__item-popup<?
						/*if ($arItem['CODE'] == "SIZE") {
							echo " size-filter";
						} elseif ($arItem['CODE'] == "COLOR") {
							echo " color-filter";
						}*/
						/*?> bx-filter-parameters-box*/ ?>" aria-labelledby="headingflt-<?= $arItem['ID'] ?>">
							<ul class="product-filter__param-list">
								<?
								$arCur = current($arItem["VALUES"]);
								switch ($arItem["DISPLAY_TYPE"]) {
									default://CHECKBOXES
										/*if ($arItem['CODE'] == "SIZE") {
											foreach ($arItem["VALUES"] as $val => $ar) {
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="product-filter__param-item <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
																				<span class="bx-filter-input-checkbox">
																					<input
																							type="checkbox"
																							value="<? echo $ar["HTML_VALUE"] ?>"
																							name="<? echo $ar["CONTROL_NAME"] ?>"
																							id="<? echo $ar["CONTROL_ID"] ?>"
																						<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																						onclick="smartFilter.click(this)"
																					/>
																				</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><?= $ar["VALUE"]; ?></a>
												</li>
												<?
											}
										} elseif ($arItem['CODE'] == "COLOR") {
											foreach ($arItem["VALUES"] as $val => $ar) {
												$arColor = getElementData($val);
												$strStyle = "";
												if (strlen($arColor['PROPERTIES']['HEX_COLOR']['VALUE']) > 0) {
													$strStyle = 'background-color:#' . $arColor['PROPERTIES']['HEX_COLOR']['VALUE'] . ";";
												} elseif ($arColor['PREVIEW_PICTURE'] > 0) {
													$strPath = CFile::GetPath($arColor['PREVIEW_PICTURE']);
													$strStyle = 'background-image: url(' . $strPath . ');background-position:center center;background-repeat:no-repeat;background-size: cover;';
												}
												?>
												<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-line <? echo $ar["DISABLED"] ? 'disabled' : '' ?><?= ($ar["CHECKED"] ? " active" : "") ?>">
													<label class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
																				<span class="bx-filter-input-checkbox">
																					<input
																							type="checkbox"
																							value="<? echo $ar["HTML_VALUE"] ?>"
																							name="<? echo $ar["CONTROL_NAME"] ?>"
																							id="<? echo $ar["CONTROL_ID"] ?>"
																						<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																						onclick="smartFilter.click(this)"
																					/>
																				</span>
													</label>
													<a href="<?= $ar['URL'] ?>"><span class="color color-img" style="<?= $strStyle ?>"></span><?= $ar["VALUE"]; ?>
													</a>
													<span class="count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
												</li>
												<?
											}*/


										//} else {
										foreach ($arItem["VALUES"] as $val => $ar) {
											?>
											<li data-role="label_<?= $ar["CONTROL_ID"] ?>" class="product-filter__param-item<? //echo $ar["DISABLED"] ? ' product-filter__param-label_disabled' : '' ?>">
												<label class="product-filter__param-label" for="<? echo $ar["CONTROL_ID"] ?>">
																				<span class="product-filter__param-checkbox">
																					<input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" id="<? echo $ar["CONTROL_ID"] ?>"<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> onclick="smartFilter.click(this)"/>
																				</span>
												</label>
												<a href="<?= $ar['URL'] ?>" class="product-filter__param-link"><?= $ar["VALUE"]; ?></a>
												<span class="product-filter__param-count" data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>
											</li>
											<?
										}?>

										<?
									//}
								}
								?>
							</ul>
							<span class="product-filter__modef bx-filter-container-modef"></span>
						</div>
					</div>
					<?
				}
				?>
			</div>
			<div class="product-filter__hide">
				<input type="submit" id="set_filter" name="set_filter" value="Y"/>
				<input class="btn btn-link" type="submit" id="del_filter" name="del_filter" value="Сбросить"/>
			</div>

			<div class="product-filter__popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>" id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?> style="display: inline-block;">
				<? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
				<span class="arrow"></span>
				<br/>
				<a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		var smartFilter = new JCSmartFilter('<? echo CUtil::JSEscape($arResult["FORM_ACTION"]) ?>', '<?= CUtil::JSEscape($arParams["FILTER_VIEW_MODE"]) ?>', <?= CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"]) ?>);
	</script>
	<?
}
