<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);
if ($arParams["DEPTH_LEVEL"] <= 0)
	$arParams["DEPTH_LEVEL"] = 1;

$arResult["SECTIONS"] = array();
$arResult["ELEMENT_LINKS"] = array();

if ($this->StartResultCache())
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
	} else
	{
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"GLOBAL_ACTIVE" => "Y",
			"IBLOCK_ACTIVE" => "Y",
			"<=" . "DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
		);
		$arOrder = array(
			"left_margin" => "asc",
		);
		/*
		  $arAllChildSections = array();
		  $arF = $arFilter;
		  unset($arF['<' . '=DEPTH_LEVEL']);
		  $arF['UF_TOPMENU'] = 1;
		  $arF['>=' . 'DEPTH_LEVEL'] = $arParams['DEPTH_LEVEL'] + 1;
		  $rSect = CIBlockSection::GetList($arOrder, $arF, false, array(
		  "ID",
		  "DEPTH_LEVEL",
		  "NAME",
		  "SECTION_PAGE_URL",
		  "UF_TOPMENU",
		  "LEFT_MARGIN",
		  "RIGHT_MARGIN"
		  ));
		  while ($arSect = $rSect->GetNext())
		  {
		  if ($arSect['DEPTH_LEVEL'] > $arParams['DEPTH_LEVEL'])
		  {
		  $arAllChildSections[$arSect['ID']] = $arSect;
		  }
		  }
		  if ($arParams['IS_ADMIN'])
		  {
		  myPrint($arAllChildSections);
		  } */
		$rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
					"ID",
					"DEPTH_LEVEL",
					"NAME",
					"SECTION_PAGE_URL",
					"UF_TOPMENU",
					"LEFT_MARGIN",
					"RIGHT_MARGIN"
		));
		if ($arParams["IS_SEF"] !== "Y")
			$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		else
			$rsSections->SetUrlTemplates("", $arParams["SEF_BASE_URL"] . $arParams["SECTION_PAGE_URL"]);
		while ($arSection = $rsSections->GetNext())
		{
			$arResult["SECTIONS"][] = array(
				"ID" => $arSection["ID"],
				"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
				"~NAME" => $arSection["~NAME"],
				"~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
				"UF_TOPMENU" => $arSection["UF_TOPMENU"],
				"LEFT_MARGIN" => $arSection["LEFT_MARGIN"],
				"RIGHT_MARGIN" => $arSection["RIGHT_MARGIN"],
			);
			if ($arParams['IS_ADMIN'])
			{
				if ($arSection['DEPTH_LEVEL'] >= 2)
				{
					$arF = $arFilter;
					unset($arF['<' . '=DEPTH_LEVEL']);
					$arF['UF_TOPMENU'] = 1;
					$arF['>LEFT_MARGIN'] = $arSection['LEFT_MARGIN'];
					$arF['<' . 'RIGHT_MARGIN'] = $arSection['RIGHT_MARGIN'];
					$arF[">=" . "DEPTH_LEVEL"] = $arParams["DEPTH_LEVEL"] + 1;
					$rSect = CIBlockSection::GetList($arOrder, $arF, false, array(
								"ID",
								"DEPTH_LEVEL",
								"NAME",
								"SECTION_PAGE_URL",
								"UF_TOPMENU",
								"LEFT_MARGIN",
								"RIGHT_MARGIN"
					));
					while ($arSect = $rSect->GetNext())
					{
						$arResult["SECTIONS"][] = array(
							"ID" => $arSect["ID"],
							"DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
							"~NAME" => $arSect["~NAME"],
							"~SECTION_PAGE_URL" => $arSect["~SECTION_PAGE_URL"],
							"UF_TOPMENU" => $arSect["UF_TOPMENU"],
							"LEFT_MARGIN" => $arSect["LEFT_MARGIN"],
							"RIGHT_MARGIN" => $arSect["RIGHT_MARGIN"],
						);
					}
				}
			}
			$arResult["ELEMENT_LINKS"][$arSection["ID"]] = array();
		}
		$this->EndResultCache();
	}
}

//In "SEF" mode we'll try to parse URL and get ELEMENT_ID from it
if ($arParams["IS_SEF"] === "Y")
{
	$engine = new CComponentEngine($this);
	if (CModule::IncludeModule('iblock'))
	{
		$engine->addGreedyPart("#SECTION_CODE_PATH#");
		$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
	}
	$componentPage = $engine->guessComponentPath(
			$arParams["SEF_BASE_URL"], array(
		"section" => $arParams["SECTION_PAGE_URL"],
		"detail" => $arParams["DETAIL_PAGE_URL"],
			), $arVariables
	);
	if ($componentPage === "detail")
	{
		CComponentEngine::InitComponentVariables(
				$componentPage, array("SECTION_ID", "ELEMENT_ID"), array(
			"section" => array("SECTION_ID" => "SECTION_ID"),
			"detail" => array("SECTION_ID" => "SECTION_ID", "ELEMENT_ID" => "ELEMENT_ID"),
				), $arVariables
		);
		$arParams["ID"] = intval($arVariables["ELEMENT_ID"]);
	}
}

if (($arParams["ID"] > 0) && (intval($arVariables["SECTION_ID"]) <= 0) && CModule::IncludeModule("iblock"))
{
	$arSelect = array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID");
	$arFilter = array(
		"ID" => $arParams["ID"],
		"ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);
	$rsElements = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if (($arParams["IS_SEF"] === "Y") && (strlen($arParams["DETAIL_PAGE_URL"]) > 0))
		$rsElements->SetUrlTemplates($arParams["SEF_BASE_URL"] . $arParams["DETAIL_PAGE_URL"]);
	while ($arElement = $rsElements->GetNext())
	{
		$arResult["ELEMENT_LINKS"][$arElement["IBLOCK_SECTION_ID"]][] = $arElement["~DETAIL_PAGE_URL"];
	}
}

$aMenuLinksNew = array();
$menuIndex = 0;
$previousDepthLevel = 1;
foreach ($arResult["SECTIONS"] as $arSection)
{
	/*if (stristr($APPLICATION->GetCurPage(), '/pervenstvo/') !== false) {
		$sections = GetIBlockSectionList(
			16,
			false,
			Array(),
			false,
			["LEFT_MARGIN" => $arSection['LEFT_MARGIN'],
			"RIGHT_MARGIN" => $arSection['RIGHT_MARGIN']]);
		$arSections = [];
		while ($section = $sections->Fetch()) {
			$arSections[] = $section['ID'];
		}

		$arFilterPervenstvo = [
			'>CATALOG_STORE_AMOUNT_42' => 0,
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => 16,
			'SECTION_ID' => $arSections
		];

		$arCount = CIBlockElement::GetList([], $arFilterPervenstvo, []);

		if (!$arCount) {
			continue;
		}
	}*/

	if ($menuIndex > 0)
		$aMenuLinksNew[$menuIndex - 1][3]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
	$previousDepthLevel = $arSection["DEPTH_LEVEL"];

	$arResult["ELEMENT_LINKS"][$arSection["ID"]][] = urldecode($arSection["~SECTION_PAGE_URL"]);
	$aMenuLinksNew[$menuIndex++] = array(
		htmlspecialcharsbx($arSection["~NAME"]),
		$arSection["~SECTION_PAGE_URL"],
		$arResult["ELEMENT_LINKS"][$arSection["ID"]],
		array(
			"FROM_IBLOCK" => true,
			"IS_PARENT" => false,
			"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
			"SECTION_ID" => $arSection['ID'],
			"UF_TOPMENU" => $arSection['UF_TOPMENU'],
		),
	);
}
return $aMenuLinksNew;
