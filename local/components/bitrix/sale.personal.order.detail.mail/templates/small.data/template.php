<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
if (strlen($arResult["ERROR_MESSAGE"]))
{
	?>
	<?= ShowError($arResult["ERROR_MESSAGE"]); ?>
	<?
} else
{
	?>
	<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
		<?
		if ($arParams["SHOW_ORDER_BUYER"] == 'Y')
		{
			foreach ($arResult["ORDER_PROPS"] as $prop)
			{

				if ($prop["SHOW_GROUP_NAME"] == "Y")
				{
					?>
					<tr>
						<td align="center" colspan="2" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;background-color:#f0f0f0;">
							<?= $prop["GROUP_NAME"] ?>
						</td>
					</tr>
				<? } ?>

				<tr>
					<td width="50%" align="right" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;"><?= $prop['NAME'] ?>:</td>
					<td width="50%" align="left" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;">
						<?
						if ($prop["TYPE"] == "Y/N")
						{
							?>
							<?= GetMessage('SPOD_' . ($prop["VALUE"] == "Y" ? 'YES' : 'NO')) ?>
							<?
						} elseif ($prop["TYPE"] == "FILE")
						{
							?>
							<?= $prop["VALUE"] ?>
							<?
						} else
						{
							?>
							<?= htmlspecialcharsbx($prop["VALUE"]) ?>
						<? } ?>
					</td>
				</tr>

				<?
			}
			if (!empty($arResult["USER_DESCRIPTION"]))
			{
				?>
				<tr>
					<td width="50%" align="right" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;"><?= GetMessage('SPOD_ORDER_USER_COMMENT') ?>:</td>
					<td width="50%" align="left" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;"><?= $arResult["USER_DESCRIPTION"] ?></td>
				</tr>

				<?
			}
		}
		?>
	</table>
	<?
}
