<?

foreach ($arResult['BASKET'] as $k => $arItem)
{
	$arItemProduct = CIBlockElement::GetList(array(), array("ID" => $arItem['PRODUCT_ID'], "IBLOCK_ID" => IBLOCK_CATALOG_OFFERS), false, false, array("ID", "NAME", "PROPERTY_SIZE", "PROPERTY_SIZE.NAME","PROPERTY_COLOR", "PROPERTY_COLOR.NAME", "PROPERTY_CML2_LINK", "CATALOG_QUANTITY"))->Fetch();
	if ($arItemProduct)
	{
		$arItemProduct['MAIN'] = CIBlockElement::GetList(array(), array("ID" => $arItemProduct['PROPERTY_CML2_LINK_VALUE'], "IBLOCK_ID" => IBLOCK_CATALOG), false, false, array("ID", "NAME", "PROPERTY_CML2_ARTICLE", "PROPERTY_BRAND", "PROPERTY_BRAND.NAME"))->Fetch();
	}
	$arResult['BASKET'][$k]['ELEMENT'] = $arItemProduct;
}
