<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
if (strlen($arResult["ERROR_MESSAGE"]))
{
	?>
	<?= ShowError($arResult["ERROR_MESSAGE"]); ?>
	<?
} else
{
	if ($arParams["SHOW_ORDER_BASKET"] == 'Y')
	{
		?>
		<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
			<?
			foreach ($arResult["BASKET"] as $arProd)
			{
				$hasLink = !empty($arProd["DETAIL_PAGE_URL"]);
				$actuallyHasProps = is_array($arProd["PROPS"]) && !empty($arProd["PROPS"]);
				?>
				<tr>
					<td width="50%" align="left" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;">
						<?
						if ($hasLink)
						{
							?><a href="<?= $arProd["DETAIL_PAGE_URL"] ?>" target="_blank"><?
							}
							if ($arProd['PICTURE']['SRC'])
							{
								?>
								<img src="<?= $arProd['PICTURE']['SRC'] ?>" width="<?= $arProd['PICTURE']['WIDTH'] ?>" height="<?= $arProd['PICTURE']['HEIGHT'] ?>" alt="<?= $arProd['NAME'] ?>" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" />
								<?
							}
							if ($hasLink)
							{
								?>
							</a>
						<? }
						?>
					</td>
					<td width="50%" align="left" valign="top" style="border-bottom: 1px solid #333333;padding: 14px 0;">
						<p style="margin:0; padding:0; margin-bottom: 10px;font-size:18px;color:#333333;"><a href="<?= $arProd["DETAIL_PAGE_URL"] ?>" style="color:#333333; text-decoration:none;font-size:18px;font-weight:bold;"><?= $arProd['ELEMENT']['MAIN']['NAME'] ?></a></p>
						<p style="margin:0; padding:0; margin-bottom: 0;font-size:12px;color:#333333;">Артикул: <strong style="font-weight:bold;"><?= $arProd['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'] ?></strong></p>
						<?
						if (strlen($arProd['ELEMENT']['PROPERTY_COLOR_NAME']) > 0)
						{
							?>
							<p style="margin:0; padding:0; margin-bottom: 0;font-size:12px;color:#333333;">Цвет: <strong style="font-weight:bold;"><?= $arProd['ELEMENT']['PROPERTY_COLOR_NAME'] ?></strong></p>
							<?
						}
						if ($arProd['ELEMENT']['PROPERTY_SIZE_NAME'])
						{
							?>
							<p style="margin:0; padding:0; margin-bottom: 0;font-size:12px;color:#333333;">Размер: <strong style="font-weight:bold;"><?= $arProd['ELEMENT']['PROPERTY_SIZE_NAME'] ?></strong></p>
						<? } ?>
						<p style="margin:0; padding:0; margin-bottom: 0;margin-top:10px;font-size:14px;color:#333333;font-weight:bold;"><?= $arProd['QUANTITY'] ?> x <?= SaleFormatCurrency($arProd['PRICE'], $arProd['CURRENCY']) ?></p>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		<?
		if ($arParams["SHOW_ORDER_SUM"] == 'Y')
		{
			?>
			<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
				<tr>
					<td width="70%" align="right" valign="top" style="padding: 3px 10px;text-align:right;">
						Стоимость товаров:
					</td>
					<td width="30%" align="left" valign="top" style="padding: 3px 0;">
						<?= $arResult['PRODUCT_SUM_FORMATED'] ?>
					</td>
				</tr>

				<?
				if (strlen($arResult["PRICE_DELIVERY_FORMATED"]))
				{
					?>
					<tr>
						<td width="70%" align="right" valign="top" style="padding: 3px 10px;text-align:right;">
							Стоимость доставки:
						</td>
						<td width="30%" align="left" valign="top" style="padding: 3px 0;">
							<?= $arResult['PRICE_DELIVERY_FORMATED'] ?>
						</td>
					</tr>
					<?
				} else
				{
					?>
					<tr>
						<td width="70%" align="right" valign="top" style="padding: 3px 10px;text-align:right;">
							Стоимость доставки:
						</td>
						<td width="30%" align="left" valign="top" style="padding: 3px 0;">
							бесплатно
						</td>
					</tr>
					<?
				}
				if (floatval($arResult["DISCOUNT_VALUE"]))
				{
					?>
					<tr>
						<td width="70%" align="right" valign="top" style="padding: 3px 10px;text-align:right;">
							Скидка:
						</td>
						<td width="30%" align="left" valign="top" style="padding: 3px 0;">
							<?= $arResult['DISCOUNT_VALUE_FORMATED'] ?>
						</td>
					</tr>
				<? } ?>
				<tr>
					<td width="70%" align="right" valign="top" style="border-bottom: 1px solid #333333;padding: 3px 10px;text-align:right;">
						ИТОГО К ОПЛАТЕ:
					</td>
					<td width="30%" align="left" valign="top" style="border-bottom: 1px solid #333333;padding: 3px 0;">
						<?= $arResult['PRICE_FORMATED'] ?>
					</td>
				</tr>
			</table>
			<?
		}
	}
}
