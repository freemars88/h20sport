<?

class CSaleExportExt extends CSaleExport
{

	public static function normalizeExternalCode($code)
	{
		$xml_id = $code;
		list($productXmlId, $offerXmlId) = explode("#", $xml_id, 2);
		if (strlen($offerXmlId) > 0)
		{
			$xml_id = $offerXmlId;
		}
		/*
		  if ($productXmlId === $offerXmlId)
		  $xml_id = $productXmlId;
		 */
		return $xml_id;
	}

}
