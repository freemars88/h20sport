<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->InitComponentTemplate();
$additionalCacheID = false;
$cachePath = false;
$RETURN_RESULT = false;
$RETURN_RESULT_KEY_AFTER_TEMPLATE = false;
$RESULT_KEY_CACHE = false;
$post_path = $_SERVER["DOCUMENT_ROOT"] . $this->__template->__folder . "/before_cache.php";

if (file_exists($post_path))
{
	include($post_path);
}
if ($RETURN_RESULT)
{
	return $arResult;
}

// including cache area (result_modifier.php)
if ($this->StartResultCache(false, $additionalCacheID, $cachePath))
{
	if ($RESULT_KEY_CACHE !== false)
	{
		if (!is_array($RESULT_KEY_CACHE))
			$RESULT_KEY_CACHE = array();
		$this->SetResultCacheKeys($RESULT_KEY_CACHE);
	}
	$this->IncludeComponentTemplate();
}
if ($RETURN_RESULT_KEY_AFTER_TEMPLATE !== false)
{
	if (file_exists($_SERVER["DOCUMENT_ROOT"] . $this->__template->__folder . "/component_epilog.php"))
	{
		include($_SERVER["DOCUMENT_ROOT"] . $this->__template->__folder . "/component_epilog.php");
	}
	return $arResult[$RETURN_RESULT_KEY_AFTER_TEMPLATE];
}