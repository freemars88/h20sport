<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("WEBAVK_TOOLS_COMPONENT_BLANK_NAME"),
	"DESCRIPTION" => GetMessage("WEBAVK_TOOLS_COMPONENT_BLANK_DESC"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "webavk"
	),
);
?>