<?
define("DOMAIN", "http://www.h2osport.ru");
define("MAILPATH", DOMAIN . "/local/mailtemplate/mail/");

function doShowProduct($iIndex)
{
	$arProducts = array(
		0 => array(
			"URL" => '/catalog/gidroodezhda/zhilet/muzh/jobe-19-segmented-jet-vest-backsupport-men-s.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/53c/270_270_2/53ca223fedd3174b89a6e7f6449e32e3.jpg',
			"NAME" => 'JOBE 19 Segmented Jet Vest Backsupport Men',
			"PRICE" => '8 750 р.',
			"OLDPRICE" => '9 950',
			"DISCOUNT" => '',
		),
		1 => array(
			"URL" => '/catalog/vodnye-lyzhi/vodnye-lyzhi/kompl/jobe-19-allegre-67-combo-skis-lime-green-package-67.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/7e2/270_270_2/7e2cb96ac4b3e6c248a85e752903d991.jpg',
			"NAME" => 'JOBE 19 Allegre 67" Combo Skis Lime Green Package 67"',
			"PRICE" => '24 950 р.',
			"OLDPRICE" => '28 360',
			"DISCOUNT" => '',
		),
		2 => array(
			"URL" => '/catalog/veykbording/veykbord/kompl/jobe-19-vanity-wakeboard-141-maze-bindings-package-std.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/237/270_270_2/237a17d3568231b219a55a89c73d7621.jpg',
			"NAME" => 'JOBE 19 Vanity Wakeboard 141 & Maze Bindings Package',
			"PRICE" => '34 950 р.',
			"OLDPRICE" => '39 720',
			"DISCOUNT" => '',
		),
		3 => array(
			"URL" => '/catalog/gidroodezhda/kurtka/kurtka-neopr/uniseks/kurtka-neopr-jobe-17-neoprene-jacket.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/87d/270_270_2/87ddc3b462b00802e8736ef0b987897d.jpg',
			"NAME" => 'Куртка неопр. JOBE 17 Neoprene Jacket',
			"PRICE" => '12 450 р.',
			"OLDPRICE" => '14 150',
			"DISCOUNT" => '',
		),
	);
	$arProduct = $arProducts[$iIndex];
	?>
	<table width="280" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="center" valign="top" style="padding: 10px 10px 0 10px;">
				<a href="<?= DOMAIN ?><?= $arProduct['URL'] ?>" target="_blank">
					<img src="<?= $arProduct['IMG'] ?>" width="264" height="264" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
				</a>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
				<a href="<?= DOMAIN ?><?= $arProduct['URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arProduct['NAME'] ?></a>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
				<table width="280" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
							<?= $arProduct['PRICE'] ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?
}

?>
<html>
<head>
	<meta http-equiv="Content-Type">
	<meta name="viewport">
	<meta http-equiv="X-UA-Compatible">

	<title>Не нашли товар? теперь можно заказать!</title>

	<style>

		/* Outlines the grids, remove when sending */
		table td {
			border: 0 none;
		}

		table th {
			border: 0 none;
			border-bottom: 1px solid #ffffff;
			padding: 14px 0 !important;
			background-color: #fcfcfc;
		}

		/* CLIENT-SPECIFIC STYLES */
		body, table, td, a {
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
		}

		table, td {
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			-ms-interpolation-mode: bicubic;
		}

		/* RESET STYLES */
		img {
			border: 0;
			outline: none;
			text-decoration: none;
		}

		table {
			border-collapse: collapse !important;
		}

		body {
			margin: 0 !important;
			padding: 0 !important;
			width: 100% !important;
		}

		/* iOS BLUE LINKS */
		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		/* ANDROID CENTER FIX */
		div[style*="margin: 16px 0;"] {
			margin: 0 !important;
		}

		/* MEDIA QUERIES */
		@media all and (max-width: 639px) {
			.wrapper {
				width: 320px !important;
				padding: 0 !important;
			}

			.container {
				width: 300px !important;
				padding: 0 !important;
			}

			.mobile {
				width: 300px !important;
				display: block !important;
				padding: 0 !important;
			}

			.mobilefont {
				font-size: 12px !important;
			}

			.mobilecenter {
				text-align: center !important;
			}

			.img {
				width: 100% !important;
				height: auto !important;
			}

			.img-nofull {
				max-width: 100% !important;
				height: auto !important;
			}

			*[class="mobileOff"] {
				width: 0px !important;
				display: none !important;
			}

			*[class*="mobileOn"] {
				display: block !important;
				max-height: none !important;
			}

			table th.mobile {
				border: 0 none;
				border-bottom: 1px solid #ffffff;
				padding: 14px 0 !important;
				background-color: #fcfcfc;
				font-weight: normal;
			}

			table th a {
				display: block;
			}
		}

	</style>
</head>
<body style="margin:0; padding:0; background-color:#ffffff;">

<span style="display: block; width: 640px !important; max-width: 640px; height: 1px" class="mobileOff"></span>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" valign="top">

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="200" class="mobile mobilecenter" align="left" valign="top">

									<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">+7
										965 1192310<br>+7 495 6618784</p>

								</td>
								<td width="200" class="mobile" align="center" valign="top">

									<a href="<?= DOMAIN ?>/" target="_blank">
										<img src="<?= DOMAIN ?>/local/mailtemplate/mail/logo.jpg" width="220" height="70" style="margin:0; padding:0; border:none; display:block;max-width:100%;" class="img-nofull" alt="H2OSport">
									</a>

								</td>
								<td width="200" class="mobileOff" align="right" valign="top">

									<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;"></p>

								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/catalog/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
								</td>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/brands/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Бренды</a>
								</td>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/actions/" class="mobilefont" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Акции</a>
								</td>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/contacts/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>
			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;
					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">
									<h1>Уважаемые клиенты!</h1>
									<p>Не нашли подходящий товар или размер, товара нет в наличии?</p>
									<p>Спешим Вас обрадовать, что теперь вы можете любой интересующий вас товар приобрести "под заказ".</p>
									<p>Специально для Вас мы закажем на заводе производителя именно то, что вас интересует.</p>
									<p>Срок поставки от 3-х недель, более точно Вам сможет подсказать менеджер.</p>
									<p>Звоните и наши менеджеры проконсультируют по всем интересующим Вас вопросам!</p>
									<p>Мы расширяем свои горизонты для Вас!</p>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>


			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="300" class="mobile" align="center" valign="top">
									<? doShowProduct(0) ?>
								</td>
								<td width="300" class="mobile" align="center" valign="top">
									<? doShowProduct(1) ?>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="300" class="mobile" align="center" valign="top">
									<? doShowProduct(2) ?>
								</td>
								<td width="300" class="mobile" align="center" valign="top">
									<? doShowProduct(3) ?>
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper mobileOn" style="display:none;">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/catalog/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
								</th>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/brands/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Бренды</a>
								</th>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/actions/" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Акции</a>
								</th>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/contacts/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
								</th>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;
					</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">
									<p style="margin:0; padding:0;font-size:12px;color:#808080;">Это письмо было
										отправлено на email:
										<a href="mailto:#MAILTO#" style="color:#333333; text-decoration:underline;font-size:12px;">#EMAIL#</a>
									</p>
									<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#808080;">
										Чтобы не пропустить ни одного письма от нас, добавьте адрес
										<a href="mailto:info@h2osport.ru" style="color:#333333; text-decoration:underline;font-size:12px;">info@h2osport.ru</a>
										в адресную книгу.</p>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">

									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td>
												<a href="https://www.facebook.com/h2osport.ru/" target="_blank"><img src="<?= DOMAIN ?>/local/mailtemplate/mail/fb.gif" style="display:block;" height="26"></a>
											</td>
											<td width="40">
												<br>
											</td>
											<td>
												<a href="https://www.instagram.com/h2osport.ru" target="_blank"><img src="<?= DOMAIN ?>/local/mailtemplate/mail/in.gif" style="display:block;" height="26"></a>
											</td>
											<td width="40">
												<br>
											</td>
											<td>
												<a href="https://vk.com/club167358802" target="_blank"><img src="<?= DOMAIN ?>/local/mailtemplate/mail/vk.gif" style="display:block;height:26px;" height="26"></a>
											</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

		</td>
	</tr>
</table>

</body>
</html>
