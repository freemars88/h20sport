<?
define("DOMAIN", "http://www.h2osport.ru");
define("MAILPATH", DOMAIN . "/local/mailtemplate/eshop/");

function doShowProduct($iIndex)
{
	$arProducts = array(
		0 => array(
			"URL" => '/catalog/odri-mio/zhenshchinam/palto/18310108-odri-mio-palto-biscay-bay-biryuzovyy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/3b3/264_396_2/3b3ab68beaf78bf0dd092af7008b8eba.jpg',
			"NAME" => '18310108 Пальто ODRI MIO ',
			"PRICE" => '19 635 р.',
			"OLDPRICE" => '23 100',
			"DISCOUNT" => '-15%'
		),
		1 => array(
			"URL" => '/catalog/odri-mio/zhenshchinam/palto/18310121-odri-mio-palto-deep-forest-zelenyy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/719/264_396_2/7196d325fb1a752cec38fe259066838a.jpg',
			"NAME" => '18310121 Пальто ODRI MIO',
			"PRICE" => '12 410 р.',
			"OLDPRICE" => '14 600',
			"DISCOUNT" => '-15%'
		),
		2 => array(
			"URL" => '/catalog/odri-mio/zhenshchinam/palto/18310127-odri-mio-palto-desert-sage-zelenyy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/813/264_396_2/813a4962a4283b2eeb34f049caa4a27f.jpg',
			"NAME" => '18310127 Пальто ODRI MIO',
			"PRICE" => '30 200 р.',
			"OLDPRICE" => '16 065',
			"DISCOUNT" => '-15%'
		),
		3 => array(
			"URL" => '/catalog/odri-mio/zhenshchinam/palto/18310138-odri-mio-palto-pearl-seryy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/d8f/264_396_2/d8f924cc9e69e3b4df77c0264a516423.jpg',
			"NAME" => '18310138 Пальто ODRI MIO',
			"PRICE" => '16 830 р.',
			"OLDPRICE" => '19 800',
			"DISCOUNT" => '-15%'
		),
		4 => array(
			"URL" => '/catalog/odri/zhenshchinam/palto/18210104-odri-palto-black-chernyy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/d66/264_396_2/d66b329cffdbb5e08f1fe8daaae52fbc.jpg',
			"NAME" => '18210104 Пальто ODRI',
			"PRICE" => '54 995 р.',
			"OLDPRICE" => '64 700',
			"DISCOUNT" => '-15%'
		),
		5 => array(
			"URL" => '/catalog/odri/zhenshchinam/palto/18210136-odri-palto-etro-zelenyy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/3aa/264_396_2/3aac95ae0f8da6d12ac21f7eadf6244c.jpg',
			"NAME" => '18210136 Пальто ODRI',
			"PRICE" => '61 285 р.',
			"OLDPRICE" => '72 100',
			"DISCOUNT" => '-15%'
		),
		6 => array(
			"URL" => '/catalog/odri/zhenshchinam/kombinezony/18210401-odri-kombinezon-lumi-lime-zheltyy.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/9df/264_396_2/9dfcb79567353ac71e1a8a583dd576db.jpg',
			"NAME" => '18210401 Комбинезон ODRI',
			"PRICE" => '68 255 р.',
			"OLDPRICE" => '80 300',
			"DISCOUNT" => '-15%'
		),
		7 => array(
			"URL" => '/catalog/odri/zhenshchinam/kombinezony/18210403-odri-kombinezon-multicolor.html',
			"IMG" => DOMAIN . '/upload/resize_cache/iblock/55c/264_396_2/55c317af77285882040ef9c67662303b.jpg',
			"NAME" => '18210403 Комбинезон ODRI',
			"PRICE" => '67 915 р.',
			"OLDPRICE" => '79 900',
			"DISCOUNT" => '-15%'
		),
	);
	$arProduct = $arProducts[$iIndex];
	?>
	<table width="280" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="center" valign="top" style="padding: 10px 10px 0 10px;">
				<a href="<?= DOMAIN ?><?= $arProduct['URL'] ?>" target="_blank">
					<img src="<?= $arProduct['IMG'] ?>" width="264" height="396" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="">
				</a>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
				<a href="<?= DOMAIN ?><?= $arProduct['URL'] ?>" target="_blank" style="color:#333333; text-decoration:none;font-size:14px;"><?= $arProduct['NAME'] ?></a>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" style="padding: 5px 10px 10px 10px;">
				<table width="280" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="left" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;font-weight:bold;">
							<?= $arProduct['PRICE'] ?>
						</td>
						<td align="center" valign="top" style="padding: 10px 10px 0 10px; font-size:16px;color:#888888;text-decoration: line-through;">
							<?= $arProduct['OLDPRICE'] ?>
						</td>
						<td align="right" valign="top" style="padding: 10px 10px 0 10px;color:#ff0000; font-size:16px;">
							<?= $arProduct['DISCOUNT'] ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?
}
?><html>
<head>
	<meta http-equiv="Content-Type">
	<meta name="viewport">
	<meta http-equiv="X-UA-Compatible">

	<title>Тема письма</title>

	<style>

		/* Outlines the grids, remove when sending */
		table td { border: 0 none; }
		table th { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;}

		/* CLIENT-SPECIFIC STYLES */
		body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
		table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
		img { -ms-interpolation-mode: bicubic; }

		/* RESET STYLES */
		img { border: 0; outline: none; text-decoration: none; }
		table { border-collapse: collapse !important; }
		body { margin: 0 !important; padding: 0 !important; width: 100% !important; }

		/* iOS BLUE LINKS */
		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		/* ANDROID CENTER FIX */
		div[style*="margin: 16px 0;"] { margin: 0 !important; }

		/* MEDIA QUERIES */
		@media all and (max-width:639px){
			.wrapper{ width:320px!important; padding: 0 !important; }
			.container{ width:300px!important;  padding: 0 !important; }
			.mobile{ width:300px!important; display:block!important; padding: 0 !important; }
			.mobilefont{font-size:12px !important;}
			.mobilecenter{text-align:center !important;}
			.img{ width:100% !important; height:auto !important; }
			.img-nofull{ max-width:100% !important; height:auto !important; }
			*[class="mobileOff"] { width: 0px !important; display: none !important; }
			*[class*="mobileOn"] { display: block !important; max-height:none !important; }
			table th.mobile { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;font-weight:normal;}
			table th a{display:block;}
		}

	</style>
</head>
<body style="margin:0; padding:0; background-color:#ffffff;">

<span style="display: block; width: 640px !important; max-width: 640px; height: 1px" class="mobileOff"></span>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" valign="top">

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="200" class="mobile mobilecenter" align="left" valign="top">

									<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">+7 965 1192310<br>+7 495 6618784</p>

								</td>
								<td width="200" class="mobile" align="center" valign="top">

									<a href="<?= DOMAIN ?>/" target="_blank">
										<img src="<?= DOMAIN ?>/local/mailtemplate/mail/logo.jpg" width="220" height="70" style="margin:0; padding:0; border:none; display:block;max-width:100%;" class="img-nofull" alt="H2OSport">
									</a>

								</td>
								<td width="200" class="mobileOff" align="right" valign="top">

									<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;"></p>

								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/catalog/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
								</td>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/brands/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Бренды</a>
								</td>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/actions/" class="mobilefont" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Акции</a>
								</td>
								<td width="25%" align="center" valign="top">
									<a href="<?= DOMAIN ?>/contacts/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>
			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">
									<a href="<?= DOMAIN ?>/">
										<img src="<?= DOMAIN ?>/upload/medialibrary/daf/daf0d9243b4dd70f66ed14842d889059.jpg" width="600" height="393">
									</a>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">
									<h1>Заголовок текста</h1>
									<p>Сам текст строка 1</p>
									<p>Сам текст строка 2</p>
									<p>Сам текст строка 3</p>
									<p>Сам текст строка 4</p>
									<p>Сам текст строка 5</p>
									<p>Сам текст строка 6</p>
									<p>По состоянию на #DATE# на вашем бонусном счете: #BONUS#</p>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>


			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_1#
								</td>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_2#
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_3#
								</td>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_4#
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_5#
								</td>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_6#
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_7#
								</td>
								<td width="300" class="mobile" align="center" valign="top">
									#PRODUCT_8#
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper mobileOn" style="display:none;">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/catalog/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
								</th>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/brands/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Бренды</a>
								</th>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/actions/" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Акции</a>
								</th>
								<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
									<a href="<?= DOMAIN ?>/contacts/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
								</th>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">
									<p style="margin:0; padding:0;font-size:12px;color:#808080;">Это письмо было отправлено на email: <a href="mailto:#MAILTO#" style="color:#333333; text-decoration:underline;font-size:12px;">#EMAIL#</a></p>
									<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#808080;">Чтобы не пропустить ни одного письма от нас, добавьте адрес <a href="mailto:info@h2osport.ru" style="color:#333333; text-decoration:underline;font-size:12px;">info@h2osport.ru</a> в адресную книгу.</p>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

			<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" valign="top">

						<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
							<tr>
								<td align="center" valign="top">

									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td>
												<a href="https://www.facebook.com/" target="_blank"><img src="<?= DOMAIN ?>/local/mailtemplate/mail/fb.gif" style="display:block;" height="26"></a>
											</td>
											<td width="40">
												<br>
											</td>
											<td>
												<a href="https://www.instagram.com/" target="_blank"><img src="<?= DOMAIN ?>/local/mailtemplate/mail/in.gif" style="display:block;" height="26"></a>
											</td>
											<td width="40">
												<br>
											</td>
											<td>
												<a href="https://www.vk.com/" target="_blank"><img src="<?= DOMAIN ?>/local/mailtemplate/mail/vk.gif" style="display:block;height:26px;" height="26"></a>
											</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
				</tr>
			</table>

		</td>
	</tr>
</table>

</body>
</html>
