<html>
	<head>
		<meta http-equiv="Content-Type">
		<meta name="viewport">
		<meta http-equiv="X-UA-Compatible">

		<title>Ликвидация склада товаров для водных видов спорта</title>

		<style>

			/* Outlines the grids, remove when sending */
			table td { border: 0 none; }
			table th { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;}

			/* CLIENT-SPECIFIC STYLES */
			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
			img { -ms-interpolation-mode: bicubic; }

			/* RESET STYLES */
			img { border: 0; outline: none; text-decoration: none; }
			table { border-collapse: collapse !important; }
			body { margin: 0 !important; padding: 0 !important; width: 100% !important; }

			/* iOS BLUE LINKS */
			a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}

			/* ANDROID CENTER FIX */
			div[style*="margin: 16px 0;"] { margin: 0 !important; }

			/* MEDIA QUERIES */
			@media all and (max-width:639px){ 
				.wrapper{ width:320px!important; padding: 0 !important; }
				.container{ width:300px!important;  padding: 0 !important; }
				.mobile{ width:300px!important; display:block!important; padding: 0 !important; }
				.mobilefont{font-size:12px !important;}
				.mobilecenter{text-align:center !important;}
				.img{ width:100% !important; height:auto !important; }
				.img-nofull{ max-width:100% !important; height:auto !important; }
				*[class="mobileOff"] { width: 0px !important; display: none !important; }
				*[class*="mobileOn"] { display: block !important; max-height:none !important; }
				table th.mobile { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;font-weight:normal;}
				table th a{display:block;}
			}

		</style>    
	</head>
	<body style="margin:0; padding:0; background-color:#ffffff;">

		<span style="display: block; width: 640px !important; max-width: 640px; height: 1px" class="mobileOff"></span>


		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center" valign="top">

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="200" class="mobile mobilecenter" align="left" valign="top">

											<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">+7 977 7025217<br>+7 800 7771424</p>

										</td>
										<td width="200" class="mobile" align="center" valign="top">

											<a href="http://www.h2osport.ru/" target="_blank">
												<img src="http://www.h2osport.ru/local/mailtemplate/mail/logo.jpg" width="220" height="70" style="margin:0; padding:0; border:none; display:block;max-width:100%;" class="img-nofull" alt="H2OSport">
											</a>

										</td>
										<td width="200" class="mobileOff" align="right" valign="top">

											<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;"></p>

										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="25%" align="center" valign="top">
											<a href="http://www.h2osport.ru/catalog/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
										</td>
										<td width="25%" align="center" valign="top">
											<a href="http://www.h2osport.ru/catalog/sale/" class="mobilefont" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Распродажа</a>
										</td>
										<td width="25%" align="center" valign="top">
											<a href="http://www.h2osport.ru/places/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Где кататься</a>
										</td>
										<td width="25%" align="center" valign="top">
											<a href="http://www.h2osport.ru/contacts/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>
					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">
											<a href="http://www.h2osport.ru/catalog/sale/">
												<img src="http://www.h2osport.ru/upload/medialibrary/daf/daf0d9243b4dd70f66ed14842d889059.jpg" width="600" height="393">
											</a>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">
											<h1>Ликвидация склада</h1>
											<p>По состоянию на #DATE# на вашем бонусном счете: #BONUS#</p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>


					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_1#
										</td>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_2#
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_3#
										</td>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_4#
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_5#
										</td>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_6#
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_7#
										</td>
										<td width="300" class="mobile" align="center" valign="top">
											#PRODUCT_8#
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper mobileOn" style="display:none;">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="http://www.h2osport.ru/catalog/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
										</th>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="http://www.h2osport.ru/catalog/sale/" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Распродажа</a>
										</th>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="http://www.h2osport.ru/places/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Где кататься</a>
										</th>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="http://www.h2osport.ru/contacts/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
										</th>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">
											<p style="margin:0; padding:0;font-size:12px;color:#808080;">Это письмо было отправлено на email: <a href="mailto:#MAILTO#" style="color:#333333; text-decoration:underline;font-size:12px;">#EMAIL#</a></p>
											<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#808080;">Чтобы не пропустить ни одного письма от нас, добавьте адрес <a href="mailto:info@h2osport.ru" style="color:#333333; text-decoration:underline;font-size:12px;">info@h2osport.ru</a> в адресную книгу.</p>
											<p style="margin:0; padding:0;font-size:12px;color:#808080;">Для того чтобы не получать данные письма: <a href="http://www.h2osport.ru/personal/unsb.php?id=#USER_ID#" style="color:#333333; text-decoration:underline;font-size:12px;">Отписаться</a></p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">

											<table cellspacing="0" cellpadding="0" border="0">
												<tr>
													<td>
														<a href="https://www.facebook.com/h2osport.ru/" target="_blank"><img src="http://www.h2osport.ru/local/mailtemplate/mail/fb.gif" style="display:block;" height="26"></a>
													</td>
													<td width="40">
														<br>
													</td>
													<td>
														<a href="https://www.instagram.com/h2osport.ru" target="_blank"><img src="http://www.h2osport.ru/local/mailtemplate/mail/in.gif" style="display:block;" height="26"></a>
													</td>
													<td width="40">
														<br>
													</td>
													<td>
														<a href="https://vk.com/club167358802" target="_blank"><img src="http://www.h2osport.ru/local/mailtemplate/mail/vk.gif" style="display:block;height:26px;" height="26"></a>
													</td>
												</tr>
											</table>

										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

				</td>
			</tr>
		</table>

	</body>
</html>
