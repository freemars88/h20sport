<?
define("DOMAIN", "https://eshop.odrifashion.com");
define("MAILPATH", DOMAIN . "/local/mailtemplate/eshop/");
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>#THEME#</title>

		<style type="text/css">

			/* Outlines the grids, remove when sending */
			table td { border: 0 none; }
			table th { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;}

			/* CLIENT-SPECIFIC STYLES */
			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
			img { -ms-interpolation-mode: bicubic; }

			/* RESET STYLES */
			img { border: 0; outline: none; text-decoration: none; }
			table { border-collapse: collapse !important; }
			body { margin: 0 !important; padding: 0 !important; width: 100% !important; }

			/* iOS BLUE LINKS */
			a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}

			/* ANDROID CENTER FIX */
			div[style*="margin: 16px 0;"] { margin: 0 !important; }

			/* MEDIA QUERIES */
			@media all and (max-width:639px){ 
				.wrapper{ width:320px!important; padding: 0 !important; }
				.container{ width:300px!important;  padding: 0 !important; }
				.mobile{ width:300px!important; display:block!important; padding: 0 !important; }
				.mobilefont{font-size:12px !important;}
				.mobilecenter{text-align:center !important;}
				.img{ width:100% !important; height:auto !important; }
				.img-nofull{ max-width:100% !important; height:auto !important; }
				*[class="mobileOff"] { width: 0px !important; display: none !important; }
				*[class*="mobileOn"] { display: block !important; max-height:none !important; }
				table th.mobile { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;font-weight:normal;}
				table th a{display:block;}
			}

		</style>    
	</head>
	<body style="margin:0; padding:0; background-color:#ffffff;">

		<span style="display: block; width: 640px !important; max-width: 640px; height: 1px" class="mobileOff"></span>

		<center>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
				<tr>
					<td align="center" valign="top">

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td width="200" class="mobile mobilecenter" align="left" valign="top">

												<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">+7 495 241-29-19</p>

											</td>
											<td width="200" class="mobile" align="center" valign="top">

												<a href="<?= DOMAIN ?>" target="_blank">
													<img src="<?= MAILPATH ?>logo.jpg" width="107" height="41" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="Odri">
												</a>

											</td>
											<td width="200" class="mobileOff" align="right" valign="top">

												<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">с 10:00 до 19:00<br/>Выходной: СБ-ВС </p>

											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td width="25%" align="center" valign="top">
												<a href="<?= DOMAIN ?>/catalog/zhenshchinam/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Женщинам</a>
											</td>
											<td width="25%" align="center" valign="top">
												<a href="<?= DOMAIN ?>/catalog/muzhchinam/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Мужчинам</a>
											</td>
											<td width="25%" align="center" valign="top">
												<a href="<?= DOMAIN ?>/catalog/devochkam/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Девочкам</a>
											</td>
											<td width="25%" align="center" valign="top">
												<a href="<?= DOMAIN ?>/catalog/sale/" class="mobilefont" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Распродажа</a>
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>
						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="2" bgcolor="#333333" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td align="center" valign="top">
												#WORK_AREA#
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>
						
						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper mobileOn" bgcolor="#FFFFFF" style="display:none;">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="<?= DOMAIN ?>/catalog/zhenshchinam/" target="_blank" style="color:#333333; text-decoration:none;font-size:12px;display:block;font-weight:normal;">ЖЕНЩИНАМ</a>
											</th>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="<?= DOMAIN ?>/catalog/muzhchinam/" target="_blank" style="color:#333333; text-decoration:none;font-size:12px;display:block;font-weight:normal;">МУЖЧИНАМ</a>
											</th>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="<?= DOMAIN ?>/catalog/devochkam/" target="_blank" style="color:#333333; text-decoration:none;font-size:12px;display:block;font-weight:normal;">ДЕВОЧКАМ</a>
											</th>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="<?= DOMAIN ?>/catalog/sale/" target="_blank" style="color:#ff0000; text-decoration:none;font-size:12px;display:block;font-weight:normal;">РАСПРОДАЖА</a>
											</th>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>
						
						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td height="2" bgcolor="#333333" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
							</tr>
						</table>
						
						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td align="center" valign="top">
												<p style="margin:0; padding:0;font-size:12px;color:#808080;">Это письмо было отправлено на email: <a href="mailto:#MAILTO#" style="color:#333333; text-decoration:underline;font-size:12px;">#MAILTO#</a></p>
												<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#808080;">Чтобы не пропустить ни одного письма от нас, добавьте адрес <a href="mailto:shop@odrifashion.com" style="color:#333333; text-decoration:underline;font-size:12px;">shop@odrifashion.com</a> в адресную книгу.</p>
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>
						
						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td align="center" valign="top">
												
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td>
															<a href="https://www.facebook.com/odri.ru/" target="_blank"><img src="<?=MAILPATH?>fb.gif" style="display:block;" border="0" height="26"></a>
														</td>
														<td width="40">
															<br/>
														</td>
														<td>
															<a href="https://www.instagram.com/odri_fashion/" target="_blank"><img src="<?=MAILPATH?>in.gif" style="display:block;" border="0" height="26"></a>
														</td>
														<td width="40">
															<br/>
														</td>
														<td>
															<a href="https://www.youtube.com/channel/UCsdYRvSCQpy5YoCDQ1M_qBQ/featured?view_as=subscriber" target="_blank"><img src="<?=MAILPATH?>yt.gif" style="display:block;height:26px;" border="0" height="26"></a>
														</td>
													</tr>
												</table>

											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>
						
					</td>
				</tr>
			</table>
		</center>
	</body>
</html>
