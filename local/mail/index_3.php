<html>
	<head>
		<meta http-equiv="Content-Type">
		<meta name="viewport">
		<meta http-equiv="X-UA-Compatible">

		<title>Бонусная программа от H2OSport</title>

		<style>

			/* Outlines the grids, remove when sending */
			table td { border: 0 none; }
			table th { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;}

			/* CLIENT-SPECIFIC STYLES */
			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
			img { -ms-interpolation-mode: bicubic; }

			/* RESET STYLES */
			img { border: 0; outline: none; text-decoration: none; }
			table { border-collapse: collapse !important; }
			body { margin: 0 !important; padding: 0 !important; width: 100% !important; }

			/* iOS BLUE LINKS */
			a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}

			/* ANDROID CENTER FIX */
			div[style*="margin: 16px 0;"] { margin: 0 !important; }

			/* MEDIA QUERIES */
			@media all and (max-width:639px){ 
				.wrapper{ width:320px!important; padding: 0 !important; }
				.container{ width:300px!important;  padding: 0 !important; }
				.mobile{ width:300px!important; display:block!important; padding: 0 !important; }
				.mobilefont{font-size:12px !important;}
				.mobilecenter{text-align:center !important;}
				.img{ width:100% !important; height:auto !important; }
				.img-nofull{ max-width:100% !important; height:auto !important; }
				*[class="mobileOff"] { width: 0px !important; display: none !important; }
				*[class*="mobileOn"] { display: block !important; max-height:none !important; }
				table th.mobile { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;font-weight:normal;}
				table th a{display:block;}
			}

		</style>    
	</head>
	<body style="margin:0; padding:0; background-color:#ffffff;">

		<span style="display: block; width: 640px !important; max-width: 640px; height: 1px" class="mobileOff"></span>


		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center" valign="top">

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="200" class="mobile mobilecenter" align="left" valign="top">

											<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">+7 977 7025217<br>+7 800 7771424</p>

										</td>
										<td width="200" class="mobile" align="center" valign="top">

											<a href="/" target="_blank">
												<img src="/local/mailtemplate/mail/logo.jpg" width="220" height="70" style="margin:0; padding:0; border:none; display:block;max-width:100%;" class="img-nofull" alt="H2OSport">
											</a>

										</td>
										<td width="200" class="mobileOff" align="right" valign="top">

											<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;"></p>

										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td width="25%" align="center" valign="top">
											<a href="/catalog/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
										</td>
										<td width="25%" align="center" valign="top">
											<a href="/catalog/sale/" class="mobilefont" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Распродажа</a>
										</td>
										<td width="25%" align="center" valign="top">
											<a href="/places/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Где кататься</a>
										</td>
										<td width="25%" align="center" valign="top">
											<a href="/contacts/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>
					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">
											<a href="/catalog/">
												<img src="/upload/medialibrary/b53/b5383d877c68205e60533ab4c2eaf860.jpg" width="600" height="393">
											</a>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">
											<h1>Бонусная программа от H2OSport</h1>
											<p style="text-align:justify;">Люди, увлеченные водными видами спорта, знают, что снаряжения и спортивной экипировки не бывает много. Поэтому наш интернет-магазин H2O Sport разработал и внедрил выгодную бонусную программу лояльности для клиентов. Уже хотите узнать, как покупать больше снаряжения для водных видов спорта больше, а тратить меньше денег?</p>
											<h2>Все просто и понятно</h2>
											<p style="text-align:justify;">Бонусная программа доступна для всех клиентов интернет-магазина, вне зависимости от суммы заказа. Просто делайте покупки, а мы начислим на бонусный счет бонусы. Покупатель получает 10% бонусов от суммы каждой покупки, при этом баллы, полученные за каждую покупку, суммируются. В пересчете 1 бонусный балл равен рублю. Программа лояльности распространяется на все товары. Нужна экипировка для яхтинга, серфинга, вейкбординга или гидроодежда? Оформляйте заказы, а на вашем персональном счете появятся бонусы в количестве 10% от суммы рублей, которые вы потратили. Балы начисляются даже на покупки акционных товаров, что позволяет совершать покупки на самых выгодных условиях.</p>
											<h2>Как пользоваться бонусами</h2>
											<p style="text-align:justify;">По своей сути наши бонусы являются деньгами, которые можно потратить на любую покупку. При оформлении заказа нужно указать, какую часть чека вы хотите оплатить бонусами. При оплате одной покупки можно использовать столько бонусов, чтобы их количество не превышало 30% от общей суммы заказа. В остальном, интернет-магазин совершенно не ограничивает покупателей. Бонусы моно использовать даже для частичной оплаты снаряжения и экипировки, которая продается на акционных условиях.</p>
											<h2>Кому нужна бонусная программа</h2>
											<p style="text-align:justify;">Мы постарались сделать такую программу лояльности, которая бы помогала экономить всем без исключения клиентам. Бонусы не имеют ограничений по сроку давности или сумме покупки. Поэтому покупать больше, а платить меньше могут как начинающие любители активностей на воде, так и профессионалы водного спорта.</p>
											<p style="text-align:justify;">Теперь наши покупатели могут отказаться от экономии на различных полезных мелочах, на которые часто жалко денег. Бонусы дают возможность заказывать более дорогие товары при одном бюджете на хобби и развлечения. Внедрение бонусной системы совершенно не сказалось на качестве товаров в магазине H2O Sport - мы по-прежнему продает только самые качественные брендовые продукты.</p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper mobileOn" style="display:none;">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="/catalog/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
										</th>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="/catalog/sale/" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Распродажа</a>
										</th>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="/places/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Где кататься</a>
										</th>
										<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
											<a href="/contacts/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
										</th>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td height="2" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">
											<p style="margin:0; padding:0;font-size:12px;color:#808080;">Вы получили это письмо, т.к. являетесь клиентом нашей компании</p>
											<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#808080;">Чтобы не пропустить ни одного письма от нас, добавьте адрес <a href="mailto:info@h2osport.ru" style="color:#333333; text-decoration:underline;font-size:12px;">info@h2osport.ru</a> в адресную книгу.</p>
											<p style="margin:0; padding:0;font-size:12px;color:#808080;">Для того чтобы не получать данные письма: <a href="#UNSUBSCRIBE_LINK#" style="color:#333333; text-decoration:underline;font-size:12px;">Отписаться</a></p>
										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

					<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper">
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="top">

								<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
									<tr>
										<td align="center" valign="top">

											<table cellspacing="0" cellpadding="0" border="0">
												<tr>
													<td>
														<a href="https://www.facebook.com/h2osport.ru/" target="_blank"><img src="/local/mailtemplate/mail/fb.gif" style="display:block;" height="26"></a>
													</td>
													<td width="40">
														<br>
													</td>
													<td>
														<a href="https://www.instagram.com/h2osport.ru" target="_blank"><img src="/local/mailtemplate/mail/in.gif" style="display:block;" height="26"></a>
													</td>
													<td width="40">
														<br>
													</td>
													<td>
														<a href="https://vk.com/club167358802" target="_blank"><img src="/local/mailtemplate/mail/vk.gif" style="display:block;height:26px;" height="26"></a>
													</td>
												</tr>
											</table>

										</td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
						</tr>
					</table>

				</td>
			</tr>
		</table>

	</body>
</html>
