<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule('webavk.icourier');
CModule::IncludeModule('sale');
IncludeModuleLangFile(__FILE__);

$modulePermissions = $APPLICATION->GetGroupRight("webavk.icourier");
if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

CJSCore::Init(array('jquery'));
ClearVars();
$APPLICATION->SetTitle("Заказы для ICourier");

$sTableID = "t_webavk_icourier_orders";
$oSort = new CAdminSorting($sTableID, "ORDER_ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_order_id",
);
$lAdmin->InitFilter($arFilterFields);
$arFilter = array();

if (!empty($filter_order_id))
	$arFilter["ORDER_ID"] = $filter_order_id;

/*
  if ($lAdmin->EditAction() && $modulePermissions >= "W") {
  foreach ($FIELDS as $ID => $arFields) {
  if (!$lAdmin->IsUpdated($ID))
  continue;
  foreach ($arFields as $k => $v) {
  if (in_array($k, array("DELIVERY_DATE", "WEEK_DAY", "MONTH_DAY", "PERIOD_START", "PERIOD_END"))) {
  if ($v === "0" || strlen($v) < 1)
  $arFields[$k] = false;
  }
  if (in_array($k, array("DELIVERY_DATE", "PERIOD_START", "PERIOD_END"))) {
  if ($arFields[$k] !== false)
  $arFields[$k] = new Bitrix\Main\Type\Date($arFields[$k]);
  }
  }
  $DB->StartTransaction();
  $ID = IntVal($ID);
  $ob = new WebAVK\DeliveryDate\Days\DaysTable();

  if (!$ob->update($ID, $arFields)) {
  $lAdmin->AddUpdateError(GetMessage("webavk.deliverydate_POST_SAVE_ERROR") . $ID . ": " . $ob->LAST_ERROR, $ID);
  $DB->Rollback();
  }
  $DB->Commit();
  }
  }

  if (($arID = $lAdmin->GroupAction()) && $modulePermissions >= "W") {
  if ($_REQUEST['action_target'] == 'selected') {
  $arID = Array();
  $dbResultList = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
  "select" => array("ID"),
  "filter" => $arFilter,
  "order" => array($by => $order)
  ));
  while ($arResult = $dbResultList->Fetch())
  $arID[] = $arResult['ID'];
  }
  $ob = new WebAVK\DeliveryDate\Days\DaysTable();

  foreach ($arID as $ID) {
  if (strlen($ID) <= 0)
  continue;

  switch ($_REQUEST['action']) {
  case "delete":
  @set_time_limit(0);

  $DB->StartTransaction();

  if (!$ob->delete($ID)) {
  $DB->Rollback();

  if ($ex = $APPLICATION->GetException())
  $lAdmin->AddGroupError($ex->GetString(), $ID);
  else
  $lAdmin->AddGroupError(GetMessage("webavk.deliverydate_ERROR_DELETE"), $ID);
  }

  $DB->Commit();

  break;
  case "activate":
  case "deactivate":
  $arFields = array(
  "ACTIVE" => (($_REQUEST['action'] == "activate") ? "Y" : "N")
  );
  if (!$ob->update($ID, $arFields)) {
  if ($ex = $APPLICATION->GetException())
  $lAdmin->AddGroupError($ex->GetString(), $ID);
  else
  $lAdmin->AddGroupError(GetMessage("webavk.deliverydate_ERROR_ACTIVATE"), $ID);
  }
  break;
  }
  }
  }
 * 
 */
$arHeaders = array(
	array("id" => "ID", "content" => "ID", "sort" => "ID", "default" => true),
	array("id" => "ORDER_ID", "content" => "ID заказа", "sort" => "ORDER_ID", "default" => true),
	array("id" => "REMOTE_ID", "content" => "Удаленный код", "sort" => "REMOTE_ID", "default" => true),
	array("id" => "IS_SEND", "content" => "Отправлено в ICourier", "sort" => "IS_SEND", "default" => true),
	array("id" => "DATE_SEND", "content" => "Дата отправки", "sort" => "DATE_SEND", "default" => true),
	array("id" => "ORDER_DATA", "content" => "Данные отправки", "sort" => "ORDER_DATA", "default" => false),
	array("id" => "BARCODE", "content" => "Штрих-код заказа", "sort" => "BARCODE", "default" => true),
	array("id" => "DATE_CHECK", "content" => "Дата проверки статуса", "sort" => "DATE_CHECK", "default" => true),
	array("id" => "ERROR_CODE", "content" => "Код ошибки", "sort" => "ERROR_CODE", "default" => true),
	array("id" => "ERROR_MSG", "content" => "Сообщение ошибки", "sort" => "ERROR_MSG", "default" => true),
	array("id" => "STATUS", "content" => "Статус", "sort" => "STATUS", "default" => true),
	array("id" => "STATUS_DATA", "content" => "Данные статуса", "sort" => "STATUS_DATA", "default" => false),
	array("id" => "ALLOW_AUTO_SEND", "content" => "Разрешена автоотправка заказа", "sort" => "ALLOW_AUTO_SEND", "default" => false),
);

$lAdmin->AddHeaders($arHeaders);
$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();
$arSelectColumns = $arVisibleColumns;
$rData = WebAVK\ICourierService\OrdersTable::getList(array(
			"select" => $arSelectColumns,
			"filter" => $arFilter,
			"order" => array($by => $order)
		));
$rData = new CAdminResult($rData, $sTableID);
$rData->NavStart();
$lAdmin->NavText($rData->GetNavPrint("Заказы"));
while ($arData = $rData->GetNext()) {
	$row = & $lAdmin->AddRow($arData["ID"], $arData, "webavk.icourier.order.detail.php?ORDER_ID=" . $arData["ORDER_ID"] . "&lang=" . LANGUAGE_ID, "Заказ");

	/*
	  $row->AddCheckField("ACTIVE");
	  $row->AddInputField("SORT");
	  $row->AddInputField("NAME");
	 */


	$arActions = array();
	$arActions[] = array("ICON" => "edit", "TEXT" => "Данные отправки заказа", "ACTION" => $lAdmin->ActionRedirect("webavk.icourier.order.detail.php?ORDER_ID=" . $arData["ORDER_ID"] . "&lang=" . LANGUAGE_ID), "DEFAULT" => true);
	$row->AddActions($arActions);
}

$arFooterArray = array(
	array(
		"title" => GetMessage('MAIN_ADMIN_LIST_SELECTED'),
		"value" => $rData->SelectedRowsCount()
	)
);
$lAdmin->AddFooter($arFooterArray);

if ($modulePermissions >= "W") {
	$lAdmin->AddGroupActionTable(Array()
	);
}

if ($modulePermissions >= "W") {
	$aContext = array();
	$lAdmin->AddAdminContextMenu($aContext);
}
$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?">
	<?
	$oFilter = new CAdminFilter(
			$sTableID . "_filters", array(
		"filter_ORDER_ID" => "ID заказа",
			)
	);
	$oFilter->Begin();
	?>
	<tr>
		<td>ID заказа:</td>
		<td>
			<input type="text" name="filter_ORDER_ID" id="filter_ORDER_ID" value="<? echo (IntVal($filter_ORDER_ID) > 0) ? IntVal($filter_ORDER_ID) : "" ?>" size="20">
		</td>
	</tr>
	<?
	$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form"
			)
	);
	$oFilter->End();
	?>
</form>

<?
$lAdmin->DisplayList();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
