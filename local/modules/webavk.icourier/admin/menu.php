<?

IncludeModuleLangFile(__FILE__);

if ($APPLICATION->GetGroupRight("webavk.icourier") != "D") {
	$aMenu = array(
		"parent_menu" => "global_menu_store",
		"section" => "webavk.icourier",
		"sort" => 100,
		"module_id" => "webavk.icourier",
		"text" => GetMessage("WEBAVK_ICOURIER_MENU_MAIN"),
		"title" => GetMessage("WEBAVK_ICOURIER_MENU_MAIN_TITLE"),
		"icon" => "webavk_icourier_menu_icon",
		"page_icon" => "webavk_icourierpage_icon",
		"items_id" => "menu_webavk_icourier",
		"items" => array(
			array(
				"text" => GetMessage("WEBAVK_ICOURIER_MENU_ORDER_LIST"),
				"url" => "webavk.icourier.order.list.php?lang=" . LANGUAGE_ID,
				"title" => GetMessage("WEBAVK_ICOURIER_MENU_ORDER_LIST_ALT"),
				"more_url" => Array("webavk.icourier.order.detail.php")
			),
		)
	);
	return $aMenu;
}
return false;
