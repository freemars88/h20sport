<?

IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("sale");

class CDeliveryICourier
{

	public static $_config;

	function Init()
	{
		return array(
			// общее описание
			"SID" => "courier",
			"NAME" => GetMessage('WEBAVK_ICOURIER_DELIVERY_TITLE'),
			"DESCRIPTION" => GetMessage('WEBAVK_ICOURIER_DELIVERY_DESCRIPTION'),
			//"DESCRIPTION_INNER" => GetMessage('WEBAVK_ICOURIER_DELIVERY_DESCRIPTION_INNER'),
			"BASE_CURRENCY" => COption::GetOptionString("sale", "default_currency", "RUB"),
			"HANDLER" => __FILE__,
			// методы-обработчики
			"DBGETSETTINGS" => array(__CLASS__, "GetSettings"),
			"DBSETSETTINGS" => array(__CLASS__, "SetSettings"),
			"GETCONFIG" => array(__CLASS__, "GetConfig"),
			"COMPABILITY" => array(__CLASS__, "Compability"),
			"CALCULATOR" => array(__CLASS__, "Calculate"),
			// Список профилей
			"PROFILES" => array(
				"simple" => array(
					"TITLE" => 'courier',
					"DESCRIPTION" => GetMessage('WEBAVK_ICOURIER_DELIVERY_DESCRIPTION'),
					"RESTRICTIONS_WEIGHT" => array(0),
					"RESTRICTIONS_SUM" => array(0),
				),
			)
		);
	}

	function GetConfig()
	{
		$arConfig = array(
			"CONFIG_GROUPS" => array(
			//"delivery" => GetMessage('SALE_DH_DHL_USA_CONFIG_DELIVERY_TITLE'),
			),
			"CONFIG" => array(
			/*
			  "package_type" => array(
			  "TYPE" => "DROPDOWN",
			  "DEFAULT" => DELIVERY_DHL_USA_PACKAGE_TYPE_DEFAULT,
			  "TITLE" => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE'),
			  "GROUP" => "delivery",
			  "VALUES" => array(
			  'EE' => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_EE'),
			  'OD' => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_OD'),
			  'CP' => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_CP'),
			  ),
			  ), */
			),
		);

		return $arConfig;
	}

	function GetSettings($strSettings)
	{
		return unserialize($strSettings);
	}

	function SetSettings($arSettings)
	{
		return serialize($arSettings);
	}

	function __GetLocation($location_id)
	{
		$arLocation = CSaleLocation::GetByID($location_id, 'ru');
		return $arLocation;
	}

	/**
	 * Проверка соответствия профиля доставки заказу.
	 */
	function Compability($arOrder, $arConfig)
	{
		return array("simple");
	}

	/**
	 * Расчет стоимости доставки.
	 */
	static function Calculate($profile, $arConfig, $arOrder = false, $step = false, $temp = false)
	{
		$arLocationTo = CDeliveryDalli::__GetLocation($arOrder['LOCATION_TO']);
		if ($arLocationTo['REGION_NAME_ORIG'] == "Московская область")
		{
			return array(
				"RESULT" => "OK",
				"VALUE" => 350,
				"TRANSIT" => "",
				"IS_FROM" => true
			);
		} elseif ($arLocationTo['CITY_NAME_ORIG'] == 'Москва')
		{
			return array(
				"RESULT" => "OK",
				"VALUE" => 300,
				"TRANSIT" => "",
				"IS_FROM" => true
			);
		}
		// определение городов отправления и доставки
		$tf = CSaleLocation::GetByID($arOrder['LOCATION_FROM']);
		$townfrom = $tf['CITY_NAME'] ? $tf['CITY_NAME'] : 'Москва';
		$tt = CSaleLocation::GetByID($arOrder['LOCATION_TO']);
		$townto = $tt['CITY_NAME'] ? $tt['CITY_NAME'] : $_COOKIE['MS_CITY_NAME'];

		$cache_id = "icourier" . "|" . $arConfig["category"]['VALUE'] . "|" . serialize($profile) . "|" . $arOrder["LOCATION_TO"] . "|" . intval($arOrder['WEIGHT']) . "|" . $townfrom . "|" . $townto;
		$obCache = new CPHPCache();
		if ($obCache->InitCache(DELIVERY_ICOURIER_CACHE_LIFETIME, $cache_id, "/icourier/"))
		{
			// cache found
			$vars = $obCache->GetVars();
			$result = $vars["RESULT"];
			$transit_time = $vars["TRANSIT"];

			return array(
				"RESULT" => "OK",
				"VALUE" => $result,
				"TRANSIT" => $transit_time
			);
		}
		// пытаемся отправить запрос расчета стоимости доставки
		$measoft = new Measoft(COption::GetOptionString("webavk.icourier", "login"), COption::GetOptionString("webavk.icourier", "password"), COption::GetOptionString("webavk.icourier", "extra"));
		$price = $measoft->calculatorRequest(array(
			'townfrom' => $townfrom,
			'townto' => $townto,
			'mass' => $arOrder['WEIGHT'] / 1000
		));
		$orderPrice = $arOrder['PRICE'];
		$transit_time = false;
		/*
		  // расчет стоимости доставки с учетом ценовой политики магазина
		  if ($price != false)
		  {
		  for ($i = 1; $i <= 3; $i++)
		  {
		  $min = is_numeric(self::configValue('PRICE_IF_' . $i . '_MIN')) ? (int) self::configValue('PRICE_IF_' . $i . '_MIN') : null;
		  $max = is_numeric(self::configValue('PRICE_IF_' . $i . '_MAX')) ? (int) self::configValue('PRICE_IF_' . $i . '_MAX') : null;
		  $type = self::configValue('PRICE_IF_' . $i . '_TYPE');
		  $aomunt = self::configValue('PRICE_IF_' . $i . '_AOMUNT');

		  if (($min != null && $max != null && $orderPrice > $min && $orderPrice < $max) || ($min != null && $max == null && $orderPrice > $min) || ($min == null && $max != null && $orderPrice < $max)
		  )
		  {
		  switch ($type)
		  {
		  case 1:
		  $price = $aomunt ? $aomunt : $price;
		  break;
		  case 2:
		  $price = 0;
		  break;
		  case 3:
		  $price = round($price - $price * $aomunt / 100);
		  break;
		  case 4:
		  $price = ($price > $aomunt) ? round($price - $aomunt) : 0;
		  break;
		  }
		  }
		  }
		  }
		 */
		if ($measoft->errors)
		{
			$measoft->errors = implode(';<br>', $measoft->errors);
			return array("RESULT" => "ERROR", 'TEXT' => $measoft->errors);
		} else
		{
			$obCache->StartDataCache();
			$price = strval($price)+50;
			$obCache->EndDataCache(
					array(
						"RESULT" => $price,
						"TRANSIT" => $transit_time,
					)
			);
			return array("RESULT" => "OK", 'VALUE' => $price);
		}
	}

	/*
	  function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
	  {
	  $arLocationTo = CDeliveryDalli::__GetLocation($arOrder['LOCATION_TO']);
	  if ($profile == "msk")
	  {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 300,
	  "TRANSIT" => "",
	  "IS_FROM" => true
	  );
	  } elseif ($profile == "spb")
	  {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 350,
	  "TRANSIT" => "",
	  "IS_FROM" => true
	  );
	  }
	  /* if ($arLocationTo['CITY_NAME'] == "Москва" || $arLocationTo['REGION_NAME_ORIG'] == "Московская область") {
	  if ($arOrder['PRICE'] >= 5000) {
	  if ($arLocationTo['REGION_NAME_ORIG'] == "Московская область") {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 300,
	  "TRANSIT" => "",
	  "IS_FROM" => true
	  );
	  } else {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 0,
	  "TRANSIT" => "",
	  );
	  }
	  } else {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 300,
	  "TRANSIT" => "",
	  );
	  }
	  } elseif (($arLocationTo['CITY_NAME'] == "Санкт-Петербург" || $arLocationTo['REGION_NAME_ORIG'] == "Ленинградская область")) {
	  if ($arLocationTo['REGION_NAME_ORIG'] == "Ленинградская область") {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 300,
	  "TRANSIT" => "",
	  "IS_FROM" => true
	  );
	  } else {
	  if ($arOrder['PRICE'] >= 5000) {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 0,
	  "TRANSIT" => "",
	  );
	  } else {
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => 280,
	  "TRANSIT" => "",
	  );
	  }
	  }
	  } else { *//*
	  if ($arLocationTo['ID'] > 0 && strlen($arLocationTo['CITY_NAME']) > 0)
	  {
	  $arOrder["WEIGHT"] = CSaleMeasure::Convert($arOrder["WEIGHT"], "G", "KGM");
	  if ($arOrder["WEIGHT"] <= 0)
	  $arOrder["WEIGHT"] = 0.1;
	  $cache_id = "dalli" . "|" . $arConfig["category"]['VALUE'] . "|" . serialize($profile) . "|" . $arOrder["LOCATION_TO"] . "|" . intval($arOrder['WEIGHT']);
	  $obCache = new CPHPCache();
	  if ($obCache->InitCache(DELIVERY_DALLI_CACHE_LIFETIME, $cache_id, "/"))
	  {
	  // cache found
	  $vars = $obCache->GetVars();
	  $result = $vars["RESULT"];
	  $transit_time = $vars["TRANSIT"];

	  return array(
	  "RESULT" => "OK",
	  "VALUE" => $result,
	  "TRANSIT" => $transit_time
	  );
	  }
	  $partner = "SDEK";
	  $typedelivery = "KUR";
	  switch ($profile)
	  {
	  case "msk":
	  $partner = "DS";
	  $typedelivery = "KUR";
	  break;
	  case "pvz_dalli":
	  $partner = "DS";
	  $typedelivery = "PVZ";
	  break;
	  case "pvz_sdek":
	  $partner = "SDEK";
	  $typedelivery = "PVZ";
	  break;
	  case "sdek_courier":
	  $partner = "SDEK";
	  $typedelivery = "KUR";
	  break;
	  case "spb":
	  $partner = "DS";
	  $typedelivery = "KUR";
	  break;
	  case "pvz_boxberry":
	  $partner = "BOXBERRY";
	  $typedelivery = "PVZ";
	  break;
	  case "pvz_pickup":
	  $partner = "PICKUP";
	  $typedelivery = "PVZ";
	  break;
	  }
	  $arData = CDeliveryServiceDalliDriver::doGetDeliveryCost($partner, $arLocationTo['CITY_NAME'], $arOrder["WEIGHT"], $arOrder["PRICE"], $arOrder["PRICE"], COption::GetOptionString("webavk.dalli", "length", 10), COption::GetOptionString("webavk.dalli", "width", 10), COption::GetOptionString("webavk.dalli", "height", 10), $typedelivery);
	  if ($arData['error'] <= 0)
	  {
	  $obCache->StartDataCache();
	  $result = doubleval($arData['price']);
	  if ($partner == "DS" && $typedelivery == "KUR")
	  {
	  $result = 300;
	  }
	  $transit_time = "";
	  if (strlen($arData['delivery_period']) > 0)
	  {
	  $transit_time = $arData['delivery_period'];
	  }
	  if (strlen($arData['msg']) > 0)
	  {
	  $transit_time .= " (" . $arData['msg'] . ")";
	  }
	  $transit_time = trim($transit_time);
	  $obCache->EndDataCache(
	  array(
	  "RESULT" => $result,
	  "TRANSIT" => $transit_time,
	  )
	  );
	  return array(
	  "RESULT" => "OK",
	  "VALUE" => $result,
	  "TRANSIT" => $transit_time,
	  );
	  } else
	  {
	  return array(
	  "RESULT" => "ERROR",
	  "TEXT" => $arData['errormsg']
	  );
	  }


	  return array(
	  "RESULT" => "OK",
	  "VALUE" => $result,
	  "TRANSIT" => $transit_time,
	  );
	  }
	  //}
	  return array(
	  "RESULT" => "ERROR",
	  "TEXT" => GetMessage('SALE_DH_DHL_USA_ERROR_RESPONSE'),
	  );
	  }

	  function Compability($arOrder)
	  {
	  $arLocationTo = CDeliveryDalli::__GetLocation($arOrder['LOCATION_TO']);
	  $arResult = array('pvz_dalli', 'pvz_sdek', 'pvz_boxberry', "pvz_pickup");
	  if ($arLocationTo['CITY_NAME'] == "Москва" || $arLocationTo['REGION_NAME_ORIG'] == "Московская область")
	  {
	  $arResult[] = "msk";
	  } elseif ($arLocationTo['CITY_NAME'] == "Санкт-Петербург" || $arLocationTo['REGION_NAME_ORIG'] == "Ленинградская область")
	  {
	  $arResult[] = "spb";
	  //$arResult[] = "sdek_courier";
	  } else
	  {
	  $arResult[] = "sdek_courier";
	  }
	  foreach ($arResult as $k => $profile)
	  {
	  $arCalc = self::Calculate($profile, self::GetConfig(), $arOrder);
	  if ($arCalc['RESULT'] == "ERROR")
	  {
	  unset($arResult[$k]);
	  }
	  }
	  return $arResult;
	  }
	 */
}
