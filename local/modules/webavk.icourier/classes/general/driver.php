<?

class CDeliveryServiceICourierDriver
{
	/*
	  function doGetDeliveryCost($strPartner, $strTownto, $fWeight, $fPrice, $fInshprice, $iLength, $iWidth, $iHeight, $strTypeDelivery)
	  {
	  $strRequest = '<?xml version="1.0" encoding="UTF-8"?>
	  <deliverycost>
	  <auth token="' . COption::GetOptionString("webavk.dalli", "authtoken", "") . '"></auth>
	  <partner>' . $strPartner . '</partner>
	  <townto>' . $strTownto . '</townto>
	  <weight>' . $fWeight . '</weight>
	  <price>' . $fPrice . '</price>
	  <inshprice>' . $fInshprice . '</inshprice>
	  <length>' . ($iLength > 0 ? $iLength : COption::GetOptionString("webavk.dalli", "length", "")) . '</length>
	  <width>' . ($iWidth > 0 ? $iWidth : COption::GetOptionString("webavk.dalli", "width", "")) . '</width>
	  <height>' . ($iHeight > 0 ? $iHeight : COption::GetOptionString("webavk.dalli", "height", "")) . '</height>
	  <typedelivery>' . $strTypeDelivery . '</typedelivery>
	  </deliverycost>';
	  CDeliveryServiceDalliDriver::__Write2Log($strRequest);
	  $data = QueryGetData(DELIVERY_DALLI_SERVER, DELIVERY_DALLI_SERVER_PORT, DELIVERY_DALLI_SERVER_PAGE, $strRequest, $error_number = 0, $error_text = "", DELIVERY_DALLI_SERVER_METHOD, DELIVERY_DALLI_SERVER_PROTO);
	  $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
	  $json = json_encode($xml);
	  $array = json_decode($json, TRUE);
	  CDeliveryServiceDalliDriver::__Write2Log($data);
	  return $array['@attributes'];
	  }
	 */

	function doGetDeliveryType()
	{
		$cache_id = "icourier_delivery_type";
		$obCache = new CPHPCache();
		if ($obCache->InitCache(DELIVERY_ICOURIER_CACHE_LIFETIME, $cache_id, "/"))
		{
			$vars = $obCache->GetVars();
			$result = $vars["RESULT"];
			return $result;
		}
		$obCache->StartDataCache();
		$measoft = new Measoft(COption::GetOptionString("webavk.icourier", "login"), COption::GetOptionString("webavk.icourier", "password"), COption::GetOptionString("webavk.icourier", "extra"));
		$level = 0;
		$xml = $measoft->startxml();
		$xml .= $measoft->makexmlnode('services', '', $level, '', 1);
		$level++;
		$xml .= $measoft->makexmlNode('auth', '', $level, 'extra="' . $measoft->extra . '" login="' . $measoft->login . '" pass="' . $measoft->password . '"');
		$level--;
		$xml .= $measoft->makexmlnode('services', '', $level, '', 2);
		$response = simplexml_load_string($measoft->sendRequest($xml));
		$json = json_encode($response);
		$array = json_decode($json, TRUE);
		$arResult = array();
		foreach ($array['service'] as $k => $v)
		{
			$arResult[$v['code']] = $v['name'];
		}
		//$arResult=$measoft->getDeliveryTypes();
		/*
		  $strRequest = '<?xml version="1.0" encoding="UTF-8"?>
		  <services>
		  <auth token="' . COption::GetOptionString("webavk.dalli", "authtoken", "") . '"></auth>
		  </services>';
		  CDeliveryServiceDalliDriver::__Write2Log($strRequest);
		  $data = QueryGetData(DELIVERY_DALLI_SERVER, DELIVERY_DALLI_SERVER_PORT, DELIVERY_DALLI_SERVER_PAGE, $strRequest, $error_number = 0, $error_text = "", DELIVERY_DALLI_SERVER_METHOD, DELIVERY_DALLI_SERVER_PROTO);
		  $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
		  $json = json_encode($xml);
		  $array = json_decode($json, TRUE);
		  CDeliveryServiceDalliDriver::__Write2Log($data);
		 * $arResult = array();
		  foreach ($array['service'] as $k => $v)
		  {
		  $arResult[$v['code']] = $v['name'];
		  }
		 */
		$obCache->EndDataCache(
				array(
					"RESULT" => $arResult
				)
		);
		return $arResult;
	}

	function doSendOrder($RECORD_ID)
	{
		$measoft = new Measoft(COption::GetOptionString("webavk.icourier", "login"), COption::GetOptionString("webavk.icourier", "password"), COption::GetOptionString("webavk.icourier", "extra"));
		$dbData = \WebAVK\ICourierService\OrdersTable::getList(array(
					"filter" => array("ID" => $RECORD_ID)
		));
		$arData = $dbData->Fetch();
		if ($arData && $arData['IS_SEND'] != "Y")
		{
			$arData['ORDER_DATA'] = unserialize($arData['ORDER_DATA']);
			$arData['STATUS_DATA'] = unserialize($arData['STATUS_DATA']);
			$arData['REMOTE_ID'] = $arData['ORDER_ID'] . COption::GetOptionString("webavk.dalli", "order_postfix", "");
			\WebAVK\ICourierService\OrdersTable::update($arData['ID'], array("REMOTE_ID" => $arData['REMOTE_ID']));
			$strRequest = '<?xml version="1.0" encoding="UTF-8"?>
<neworder>
 <auth extra="' . $measoft->extra . '" login="' . $measoft->login . '" pass="' . $measoft->password . '" />
 <order orderno="' . htmlspecialchars($arData['REMOTE_ID'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '">
   <receiver>
     <town>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['town'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</town>
     <address>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['address'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</address>
     <zipcode>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['zipcode'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</zipcode>
     <person>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['person'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</person>
     <phone>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['phone'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . ' / ' . htmlspecialchars($arData['ORDER_DATA']['receiver']['email'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</phone>
     <date>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['date'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</date>
     <time_min>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['time_min'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</time_min>
     <time_max>' . htmlspecialchars($arData['ORDER_DATA']['receiver']['time_max'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</time_max>
   </receiver>
   <service>' . htmlspecialchars($arData['ORDER_DATA']['service'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</service>
   <weight>' . htmlspecialchars($arData['ORDER_DATA']['weight'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</weight>
   <quantity>' . htmlspecialchars($arData['ORDER_DATA']['quantity'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</quantity>
   <paytype>' . htmlspecialchars($arData['ORDER_DATA']['paytype'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</paytype>
   <price>' . htmlspecialchars($arData['ORDER_DATA']['price'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</price>
   <inshprice>' . htmlspecialchars($arData['ORDER_DATA']['inshprice'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</inshprice>
   <deliveryprice>' . htmlspecialchars($arData['ORDER_DATA']['priced'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</deliveryprice>
   <instruction>' . htmlspecialchars($arData['ORDER_DATA']['instruction'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</instruction>
   <items>';
			foreach ($arData['ORDER_DATA']['items'] as $arItem)
			{
				$strRequest .= '
      <item quantity="' . floatval($arItem['quantity']) . '" mass="' . floatval($arItem['mass']) . '" retprice="' . floatval($arItem['retprice']) . '" article="">' . htmlspecialchars($arItem['name'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . (strlen($arItem['article']) > 0 ? ' (' . htmlspecialchars($arItem['article'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . ')' : '') . '</item>';
				/* $strRequest .= '
				  <item quantity="' . floatval($arItem['quantity']) . '" mass="' . floatval($arItem['mass']) . '" retprice="' . floatval($arItem['retprice']) . '" barcode="' . htmlspecialchars($arItem['barcode'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '" article="' . htmlspecialchars($arItem['article'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '">' . htmlspecialchars($arItem['name'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</item>'; */
			}
			$strRequest .= '
   </items>
 </order>
</neworder>';
			CDeliveryServiceICourierDriver::__Write2Log($strRequest);
			$strResponse = $measoft->sendRequest($strRequest);
			$data = simplexml_load_string($strResponse);
			$json = json_encode($data);
			$array = json_decode($json, TRUE);
			/*
			  $data = QueryGetData(DELIVERY_DALLI_SERVER, DELIVERY_DALLI_SERVER_PORT, DELIVERY_DALLI_SERVER_PAGE, $strRequest, $error_number = 0, $error_text = "", DELIVERY_DALLI_SERVER_METHOD, DELIVERY_DALLI_SERVER_PROTO);
			  $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
			  $json = json_encode($xml);
			  $array = json_decode($json, TRUE); */
			CDeliveryServiceICourierDriver::__Write2Log($strResponse);
			$arResult = $array['createorder']['@attributes'];
			if ($arResult['error'] == 0)
			{
				\WebAVK\ICourierService\OrdersTable::update($arData['ID'], array(
					"SEND_ANSWER_DATA" => serialize($arResult),
					"IS_SEND" => "Y",
					"DATE_SEND" => new \Bitrix\Main\Type\DateTime(),
					"ERROR_CODE" => $arResult['error'],
					"ERROR_MSG" => $arResult['errormsg'],
					"BARCODE" => $arResult['barcode'],
				));
			} else
			{
				\WebAVK\ICourierService\OrdersTable::update($arData['ID'], array(
					"SEND_ANSWER_DATA" => serialize($arResult),
					"DATE_SEND" => new \Bitrix\Main\Type\DateTime(),
					"ERROR_CODE" => $arResult['error'],
					"ERROR_MSG" => $arResult['errormsg'],
				));
			}
			return $arResult;
		}
		return false;
	}

	function doCheckStatus($RECORD_ID)
	{
		static $arConstants;
		$measoft = new Measoft(COption::GetOptionString("webavk.icourier", "login"), COption::GetOptionString("webavk.icourier", "password"), COption::GetOptionString("webavk.icourier", "extra"));
		if (empty($arConstants))
		{
			$arConstants = get_defined_constants();
		}
		$dbData = \WebAVK\ICourierService\OrdersTable::getList(array(
					"filter" => array("ID" => $RECORD_ID)
		));
		$arData = $dbData->Fetch();
		if ($arData && $arData['IS_SEND'] == "Y")
		{
			$arData['ORDER_DATA'] = unserialize($arData['ORDER_DATA']);
			$arData['STATUS_DATA'] = unserialize($arData['STATUS_DATA']);
			$strRequest = '<?xml version="1.0" encoding="UTF-8"?>
<statusreq>
 <auth extra="' . $measoft->extra . '" login="' . $measoft->login . '" pass="' . $measoft->password . '" />
  <orderno>' . htmlspecialchars($arData['REMOTE_ID'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . '</orderno>
  <quickstatus>NO</quickstatus>
</statusreq>';
			CDeliveryServiceICourierDriver::__Write2Log($strRequest);
			$strResponse = $measoft->sendRequest($strRequest);
			$data = simplexml_load_string($strResponse);
			$json = json_encode($data);
			$array = json_decode($json, TRUE);
			/*
			  $data = QueryGetData(DELIVERY_DALLI_SERVER, DELIVERY_DALLI_SERVER_PORT, DELIVERY_DALLI_SERVER_PAGE, $strRequest, $error_number = 0, $error_text = "", DELIVERY_DALLI_SERVER_METHOD, DELIVERY_DALLI_SERVER_PROTO);
			  $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
			  $json = json_encode($xml);
			  $array = json_decode($json, TRUE); */
			CDeliveryServiceICourierDriver::__Write2Log($data);
			$arResult = $array['order'];
			\WebAVK\ICourierService\OrdersTable::update($arData['ID'], array(
				"STATUS_DATA" => serialize($arResult),
				"DATE_CHECK" => new \Bitrix\Main\Type\DateTime(),
				"STATUS" => $arResult['status'],
			));
			/*
			  $newDeliveryStatus = $arConstants["DALLI_STATUS_" . $arResult['status']];
			  if (strlen($newDeliveryStatus) > 0)
			  {
			  $oOrder = \Bitrix\Sale\Order::load($arData['ORDER_ID']);
			  $oShipmentCollection = $oOrder->loadShipmentCollection();
			  $iActiveIndex = false;
			  foreach ($oShipmentCollection as $index => &$oShipment)
			  {
			  $arFields = $oShipment->getFieldValues();
			  if ($arFields['SYSTEM'] != "Y")
			  {
			  $iActiveIndex = $index;
			  }
			  }
			  if ($iActiveIndex !== false)
			  {
			  $oShipment = $oShipmentCollection->getItemByIndex($iActiveIndex);
			  $arFields = $oShipment->getFieldValues();
			  if ($arFields['STATUS_ID'] != $newDeliveryStatus)
			  {
			  $oShipment->SetField("STATUS_ID", $newDeliveryStatus);
			  $oShipment->save();
			  }
			  }
			  }
			 * 
			 */
			return $arResult;
		}
		return false;
	}

	function __Write2Log($data)
	{
		if (defined('DELIVERY_ICOURIER_WRITE_LOG') && DELIVERY_ICOURIER_WRITE_LOG === 1)
		{
			$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/bitrix/icourier.log", "a");
			fwrite($fp, "\r\n==========================================\r\n");
			fwrite($fp, $data);
			fclose($fp);
		}
	}

	/*
	  function doGetPVZList($strType, $strPartnerName, $iLocation)
	  {
	  $cache_id = "dalli" . "|" . $strType . "|" . $iLocation;
	  $obCache = new CPHPCache();
	  if ($obCache->InitCache(DELIVERY_DALLI_CACHE_LIFETIME, $cache_id, "/"))
	  {
	  // cache found
	  $vars = $obCache->GetVars();
	  $result = $vars;
	  return $result;
	  }
	  $obCache->StartDataCache();
	  $arData = array();
	  $arLocationTo = CDeliveryDalli::__GetLocation($iLocation);
	  if ($arLocationTo['ID'] > 0 && strlen($arLocationTo['CITY_NAME']) > 0)
	  {
	  $strRequest = '<?xml version="1.0" encoding="UTF-8"?>
	  <pointsInfo>
	  <auth token="' . COption::GetOptionString("webavk.dalli", "authtoken", "") . '"></auth>
	  <town>' . $arLocationTo['CITY_NAME'] . '</town>
	  </pointsInfo>';
	  CDeliveryServiceDalliDriver::__Write2Log($strRequest);
	  $data = QueryGetData(DELIVERY_DALLI_SERVER, DELIVERY_DALLI_SERVER_PORT, DELIVERY_DALLI_SERVER_PAGE, $strRequest, $error_number = 0, $error_text = "", DELIVERY_DALLI_SERVER_METHOD, DELIVERY_DALLI_SERVER_PROTO);
	  $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
	  $json = json_encode($xml);
	  $array = json_decode($json, TRUE);
	  foreach ($array['point'] as $arPoint)
	  {
	  if ($arPoint['partner'] == $strType)
	  {
	  $arData[] = array(
	  "ID" => $arPoint['@attributes']['code'],
	  "NAME" => empty($arPoint['name']) ? "" : $arPoint['name'],
	  "ADDRESS" => empty($arPoint['address']) ? "" : $arPoint['address'],
	  "ADDRESS_REDUCE" => empty($arPoint['addressReduce']) ? "" : $arPoint['addressReduce'],
	  "DESCRIPTION" => empty($arPoint['description']) ? "" : $arPoint['description'],
	  "ONLY_PREPAID" => empty($arPoint['onlyPrepaid']) ? "" : $arPoint['onlyPrepaid'],
	  "WORK_SHEDULE" => empty($arPoint['workShedule']) ? "" : $arPoint['workShedule'],
	  "GPS" => empty($arPoint['GPS']) ? "" : $arPoint['GPS'],
	  "PHONE" => empty($arPoint['phone']) ? "" : $arPoint['phone'],
	  "WEIGHT_LIMIT" => empty($arPoint['weightLimit']) ? "" : $arPoint['weightLimit'],
	  "PARTNER" => $strPartnerName,
	  "PARTNERID" => $arPoint['partner']
	  );
	  }
	  }
	  }
	  $obCache->EndDataCache($arData);
	  return $arData;
	  }

	  function GetLinkPVZBoxberry($iLocation, $strAddress)
	  {
	  $arPVZ = self::doGetPVZList("BOXBERRY", "BoxBerry", $iLocation);
	  if (substr($strAddress, 0, 10) == "BoxBerry: ")
	  {
	  $iPoint = substr($strAddress, 11, strpos($strAddress, ']') - 11);
	  foreach ($arPVZ as $k => $v)
	  {
	  if ($v['ID'] == $iPoint)
	  {
	  $arPVZ[$k]['SELECTED'] = "Y";
	  } else
	  {
	  $arPVZ[$k]['SELECTED'] = "N";
	  }
	  }
	  ?>
	  <a id="seladdr-boxberry" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'BOXBERRY', 'N');return false;" data-issel="Y"><?= $strAddress ?></a>
	  <?
	  } else
	  {
	  ?>
	  <a id="seladdr-boxberry" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'BOXBERRY', 'N');return false;" data-issel="N">Выберите пункт самовывоза</a>
	  <?
	  }
	  }

	  function GetLinkPVZSdek($iLocation, $strAddress)
	  {
	  $arPVZ = self::doGetPVZList("SDEK", "СДЭК", $iLocation);
	  if (substr($strAddress, 0, 6) == "СДЭК: ")
	  {
	  $iPoint = substr($strAddress, 7, strpos($strAddress, ']') - 7);
	  foreach ($arPVZ as $k => $v)
	  {
	  if ($v['ID'] == $iPoint)
	  {
	  $arPVZ[$k]['SELECTED'] = "Y";
	  } else
	  {
	  $arPVZ[$k]['SELECTED'] = "N";
	  }
	  }
	  ?>
	  <a id="seladdr-sdek" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'SDEK', 'N');return false;" data-issel="Y"><?= $strAddress ?></a>
	  <?
	  } else
	  {
	  ?>
	  <a id="seladdr-sdek" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'SDEK', 'N');return false;" data-issel="N">Выберите пункт самовывоза</a>
	  <?
	  }
	  }

	  function GetLinkPVZPickup($iLocation, $strAddress)
	  {
	  $arPVZ = self::doGetPVZList("PICKUP", "PickUp", $iLocation);
	  if (substr($strAddress, 0, 8) == "PickUp: ")
	  {
	  $iPoint = substr($strAddress, 9, strpos($strAddress, ']') - 9);
	  foreach ($arPVZ as $k => $v)
	  {
	  if ($v['ID'] == $iPoint)
	  {
	  $arPVZ[$k]['SELECTED'] = "Y";
	  } else
	  {
	  $arPVZ[$k]['SELECTED'] = "N";
	  }
	  }
	  ?>
	  <a id="seladdr-pickup" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'PICKUP', 'N');return false;" data-issel="Y"><?= $strAddress ?></a>
	  <?
	  } else
	  {
	  ?>
	  <a id="seladdr-pickup" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'PICKUP', 'N');return false;" data-issel="N">Выберите пункт самовывоза</a>
	  <?
	  }
	  }

	  function GetLinkPVZDalli($iLocation, $strAddress)
	  {
	  $arPVZ = self::doGetPVZList("DS", "DalliService", $iLocation);
	  if (substr($strAddress, 0, 14) == "DalliService: ")
	  {
	  $iPoint = substr($strAddress, 15, strpos($strAddress, ']') - 15);
	  foreach ($arPVZ as $k => $v)
	  {
	  if ($v['ID'] == $iPoint)
	  {
	  $arPVZ[$k]['SELECTED'] = "Y";
	  } else
	  {
	  $arPVZ[$k]['SELECTED'] = "N";
	  }
	  }
	  ?>
	  <a id="seladdr-ds" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'DS', 'N');return false;" data-issel="Y"><?= $strAddress ?></a>
	  <?
	  } else
	  {
	  ?>
	  <a id="seladdr-ds" class="seladdrpvz" href="javascript:void(0)" onclick="showWidgetPVZDalli(<?= htmlspecialchars(CUtil::PhpToJSObject($arPVZ)) ?>, 'DS', 'N');return false;" data-issel="N">Выберите пункт самовывоза</a>
	  <?
	  }
	  }

	  function doGetAllPVZList()
	  {
	  $cache_id = "dalli|pvzlist";
	  $obCache = new CPHPCache();
	  if ($obCache->InitCache(DELIVERY_DALLI_CACHE_LIFETIME, $cache_id, "/"))
	  {
	  // cache found
	  $vars = $obCache->GetVars();
	  $result = $vars;
	  return $result;
	  }
	  $obCache->StartDataCache();
	  $arData = array();
	  $arPartnersName = array(
	  "BOXBERRY" => "BoxBerry",
	  "DS" => "Dalli Service",
	  "PICKUP" => "PickUp",
	  "SDEK" => "СДЭК"
	  );
	  $strRequest = '<?xml version="1.0" encoding="UTF-8"?>
	  <pointsInfo>
	  <auth token="' . COption::GetOptionString("webavk.dalli", "authtoken", "") . '"></auth>
	  </pointsInfo>';
	  CDeliveryServiceDalliDriver::__Write2Log($strRequest);
	  $data = QueryGetData(DELIVERY_DALLI_SERVER, DELIVERY_DALLI_SERVER_PORT, DELIVERY_DALLI_SERVER_PAGE, $strRequest, $error_number = 0, $error_text = "", DELIVERY_DALLI_SERVER_METHOD, DELIVERY_DALLI_SERVER_PROTO);
	  $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
	  $json = json_encode($xml);
	  $array = json_decode($json, TRUE);
	  foreach ($array['point'] as $arPoint)
	  {
	  $arData[] = array(
	  "ID" => $arPoint['@attributes']['code'],
	  "NAME" => empty($arPoint['name']) ? "" : $arPoint['name'],
	  "TOWN" => empty($arPoint['town']) ? "" : $arPoint['town'],
	  "ADDRESS" => empty($arPoint['address']) ? "" : $arPoint['address'],
	  "ADDRESS_REDUCE" => empty($arPoint['addressReduce']) ? "" : $arPoint['addressReduce'],
	  "DESCRIPTION" => empty($arPoint['description']) ? "" : $arPoint['description'],
	  "ONLY_PREPAID" => empty($arPoint['onlyPrepaid']) ? "" : $arPoint['onlyPrepaid'],
	  "WORK_SHEDULE" => empty($arPoint['workShedule']) ? "" : $arPoint['workShedule'],
	  "GPS" => empty($arPoint['GPS']) ? "" : $arPoint['GPS'],
	  "PHONE" => empty($arPoint['phone']) ? "" : $arPoint['phone'],
	  "WEIGHT_LIMIT" => empty($arPoint['weightLimit']) ? "" : $arPoint['weightLimit'],
	  "PARTNERID" => $arPoint['partner'],
	  "PARTNER" => $arPartnersName[$arPoint['partner']]
	  );
	  }
	  $obCache->EndDataCache($arData);
	  return $arData;
	  }

	  /*
	  if (substr($strAddress, 0, 10) == "Евросеть: ")
	  {
	  $arProfile['office-euroset'] = '<a href="javascript:void(0)" class="selectEurosetButton" style="color: #AA0000; text-decoration: none;">' . $strAddress . '</a>';
	  } else
	  {
	  $arProfile['office-euroset'] = '<a href="javascript:void(0)" class="selectEurosetButton" style="color: #AA0000; text-decoration: none;">Выбрать салон для получения заказа</a>';
	  }
	 *  */
}
