<?

IncludeModuleLangFile(__FILE__);
\CModule::IncludeModule("iblock");
\CModule::IncludeModule("measoft.courier");

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Main;

define('DELIVERY_ICOURIER_CACHE_LIFETIME', 60 * 60 * 24 * 30); // cache lifetime - 30 days (60*60*24*30)
/*
  define('DELIVERY_DALLI_SERVER', 'api.dalli-service.com'); // server name to send data
  define('DELIVERY_DALLI_SERVER_PORT', 443); // server port
  define('DELIVERY_DALLI_SERVER_PAGE', '/v1/'); // server page url
  define('DELIVERY_DALLI_SERVER_METHOD', 'POST'); // data send method
  define('DELIVERY_DALLI_SERVER_PROTO', 'ssl://'); // data send method
 */
define('DELIVERY_ICOURIER_WRITE_LOG', 1);

Class CWebavkICourier
{

	function DoAdminContextMenuShow(&$items)
	{
		global $APPLICATION;
		if (($APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_detail.php' || $APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_view.php') && $_REQUEST['ID'] > 0)
		{
			$orderId = intval($_REQUEST['ID']);
			$oOrder = \Bitrix\Sale\Order::load($orderId);
			if (in_array($oOrder->getField("DELIVERY_ID"), array(DELIVERY_ICOURIER1_ID, DELIVERY_ICOURIER2_ID)))
			{
				$items[] = array(
					'TEXT' => GetMessage("webavk.icourier_ORDER_SEND_ICOURIER"),
					'TITLE' => GetMessage('webavk.icourier_ORDER_SEND_ICOURIER_TITLE'),
					'LINK' => '/bitrix/admin/webavk.icourier.order.detail.php?ORDER_ID=' . $orderId,
					'ICON' => 'btn_order_send_icourier',
					'LINK_PARAM' => 'target="_blank"'
				);
			}
		}
	}

}

CModule::AddAutoloadClasses(
		"webavk.icourier", array(
	"CDeliveryServiceICourierDriver" => "classes/general/driver.php",
	"CDeliveryICourier" => "classes/general/delivery_icourier.php",
	"WebAVK\\ICourierService\\OrdersTable" => "lib/orders.php",
		)
);

