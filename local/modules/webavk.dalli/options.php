<?
$module_id = "webavk.dalli";
$modulePermissions = $APPLICATION->GetGroupRight($module_id);
if ($modulePermissions >= "R") {

	global $MESS;
	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/main/lang/", "/options.php"));
	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/webavk.dalli/lang/", "/options.php"));

	CModule::IncludeModule("sale");
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("webavk.dalli");

	$bWasUpdated = false;

	if ($REQUEST_METHOD == "GET" && strlen($RestoreDefaults) > 0 && $modulePermissions >= "W" && check_bitrix_sessid()) {
		$bWasUpdated = true;
		COption::RemoveOption("webavk.dalli");
		$z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
		while ($zr = $z->Fetch())
			$APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
	}

	$arAllOptions = Array(
		Array("authtoken", GetMessage("webavk.dalli_OPTION_authtoken"), "", Array("text", 50)),
		Array("order_postfix", GetMessage("webavk.dalli_OPTION_order_postfix"), "_TEST", Array("text", 50)),
		Array("length", GetMessage("webavk.dalli_OPTION_length"), "", Array("text", 50)),
		Array("width", GetMessage("webavk.dalli_OPTION_width"), "", Array("text", 50)),
		Array("height", GetMessage("webavk.dalli_OPTION_height"), "", Array("text", 50)),
			//Array("maxdeliveryorders", GetMessage("webavk.krasniykarandash_OPTION_maxdeliveryorders"), "", Array("text", 10)),
	);
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "sale_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
		array("DIV" => "edit4", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => "sale_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS"))
			//array("DIV" => "edit7", "TAB" => GetMessage("SALE_TAB_WEIGHT"), "ICON" => "sale_settings", "TITLE" => GetMessage("SALE_TAB_WEIGHT_TITLE")),
			//array("DIV" => "edit5", "TAB" => GetMessage("SALE_TAB_ADDRESS"), "ICON" => "sale_settings", "TITLE" => GetMessage("SALE_TAB_ADDRESS_TITLE"))
	);
	$tabControl = new CAdminTabControl("tabControl", $aTabs);

	$strWarning = "";
	if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $modulePermissions >= "W" && check_bitrix_sessid()) {
		$bWasUpdated = true;
		for ($i = 0; $i < count($arAllOptions); $i++) {
			$name = $arAllOptions[$i][0];
			$val = $$name;
			if ($arAllOptions[$i][3][0] == "checkbox" && $val != "Y")
				$val = "N";
			COption::SetOptionString($module_id, $name, $val, $arAllOptions[$i][1]);
		}
		//LocalRedirect($_SERVER["REQUEST_URI"]);
	}
	if (strlen($strWarning) > 0)
		CAdminMessage::ShowMessage($strWarning);
	$tabControl->Begin();
	?><form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANGUAGE_ID ?>" name="opt_form">
		<?= bitrix_sessid_post() ?>
		<?
		$tabControl->BeginNextTab();
		for ($i = 0; $i < count($arAllOptions); $i++) {
			$Option = $arAllOptions[$i];
			$val = COption::GetOptionString($module_id, $Option[0], $Option[2]);
			$type = $Option[3];
			?>
			<tr>
				<td width="40%">
					<?
					if ($type[0] == "checkbox")
						echo "<label for=\"" . htmlspecialcharsbx($Option[0]) . "\">" . $Option[1] . "</label>";
					else
						echo $Option[1];
					?>
				</td>
				<td width="60%">

					<? if ($type[0] == "checkbox"): ?>
						<input type="checkbox" name="<? echo htmlspecialcharsbx($Option[0]) ?>" id="<? echo htmlspecialcharsbx($Option[0]) ?>" value="Y"<? if ($val == "Y") echo" checked"; ?>>
					<? elseif ($type[0] == "text"): ?>
						<input type="text" size="<? echo $type[1] ?>" value="<? echo htmlspecialcharsbx($val) ?>" name="<? echo htmlspecialcharsbx($Option[0]) ?>">
					<? elseif ($type[0] == "textarea"): ?>
						<textarea rows="<? echo $type[1] ?>" cols="<? echo $type[2] ?>" name="<? echo htmlspecialcharsbx($Option[0]) ?>"><? echo htmlspecialcharsbx($val) ?></textarea>
					<? endif ?>
				</td>
			</tr>
			<?
		}
		$tabControl->BeginNextTab(); ?>
		<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?>
		<? $tabControl->Buttons(); ?>
		<script type="text/javascript">
			function RestoreDefaults()
			{
				if (confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>'))
					window.location = "<? echo $APPLICATION->GetCurPage() ?>?RestoreDefaults=Y&lang=<? echo LANG ?>&mid=<? echo urlencode($mid) ?>";
						}
		</script>
		<input type="submit" <? if ($modulePermissions < "W") echo "disabled" ?> name="Update" value="<? echo GetMessage("MAIN_SAVE") ?>" class="adm-btn-save">
		<input type="hidden" name="Update" value="Y">
		<? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
			<input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>" onclick="window.location = '<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
			<input type="hidden" name="back_url_settings" value="<?
			if (strlen($_REQUEST["back_url_settings"]) > 0) {
				echo htmlspecialchars($_REQUEST["back_url_settings"]);
			}
			?>">
			   <? endif; ?>
		<input type="button" <? if ($modulePermissions < "W") echo "disabled" ?> title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="RestoreDefaults();" value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
		<? $tabControl->End(); ?>
	</form>
<? } ?>
