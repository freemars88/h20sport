<?php

namespace WebAVK\DalliService;

class OrdersTable extends \Bitrix\Main\Entity\DataManager
{

	public static function getTableName()
	{
		return 'wadl_orders';
	}

	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getMap()
	{
		$fieldsMap = array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true
			),
			"ORDER_ID" => array(
				"data_type" => "integer"
			),
			"REMOTE_ID" => array(
				"data_type" => "string"
			),
			"IS_SEND" => array(
				"data_type" => "boolean",
				"values" => array("N", "Y")
			),
			"DATE_SEND" => array(
				"data_type" => 'datetime'
			),
			"ORDER_DATA" => array(
				"data_type" => 'string'
			),
			"SEND_ANSWER_DATA" => array(
				"data_type" => 'string'
			),
			"DATE_CHECK" => array(
				"data_type" => "datetime"
			),
			"ERROR_CODE" => array(
				"data_type" => "string"
			),
			"ERROR_MSG" => array(
				"data_type" => "string"
			),
			"STATUS" => array(
				"data_type" => "enum",
				"values" => array("UN", "NEW", "ACCEPTED", "INVENTORY", "DEPARTURING", "DEPARTURE", "DELIVERY", "COURIERDELIVERED", "COMPLETE", "PARTIALLY", "COURIERRETURN", "CANCELED", "RETURNING", "RETURNED", "CONFIRM", "DATECHANGE", "NEWPICKUP", "UNCONFIRM", "PICKUPREADY")
			),
			"STATUS_DATA" => array(
				"data_type" => "string"
			),
			"ALLOW_AUTO_SEND" => array(
				"data_type" => "boolean",
				"values" => array("N", "Y")
			),
			"BARCODE" => array(
				"data_type" => "string"
			)
		);
		return $fieldsMap;
	}

}
