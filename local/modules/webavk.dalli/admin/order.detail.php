<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule('webavk.dalli');
CModule::IncludeModule('sale');
IncludeModuleLangFile(__FILE__);
$modulePermissions = $APPLICATION->GetGroupRight("webavk.dalli");
if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

//CJSCore::Init(array('jquery'));
ClearVars();

$errorMessage = "";
$bVarsFromForm = false;

$ORDER_ID = IntVal($ORDER_ID);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => "Данные ордера", "ICON" => "webavk.dalli", "TITLE" => "Данные ордера"),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$arErrorCodes = array(
	"0" => "успешно",
	"1" => "ошибка авторизации (неверный или пустой токен)",
	"2" => "отправлен пустой xml или некорректный xml документ",
	"3" => "заказ с таким номером уже существует",
	"4" => "не заполнен person!",
	"5" => "не заполнен phone!",
	"6" => "не заполнен town!",
	"7" => "не заполнен address!",
	"8" => "не заполнен date!",
	"9" => "не корректное значение time_min!",
	"10" => "не корректное значение time_max!",
	"11" => "не заполнен service!",
	"12" => "не корректное значение zipcode!",
	"13" => "не корректное значение service!",
	"14" => "не корректное значение weight!",
	"15" => "не заполнен orderno!",
	"16" => "минимальный интервал 4 часа",
	"17" => "не корректное значение town!",
	"18" => "не корректное значение orderno!",
	"19" => "не корректное значение quantity!",
	"20" => "не корректное значение price!",
	"21" => "не корректное значение inshprice!",
	"21" => "Заказ с таким номером уже существует в корзине!",
	"22" => "Некорректно указана дата доставки!"
);
$arStatusCodes = array(
	"UN" => "Неизвестно",
	"NEW" => "Новый",
	"ACCEPTED" => "Получен складом",
	"INVENTORY" => "Инвентаризация",
	"DEPARTURING" => "Планируется отправка",
	"DEPARTURE" => "Отправлено со склада",
	"DELIVERY" => "Выдан курьеру на доставку",
	"COURIERDELIVERED" => "Доставлен (предварительно)",
	"COMPLETE" => "Доставлен",
	"PARTIALLY" => "Доставлен частично",
	"COURIERRETURN" => "Возвращено курьером",
	"CANCELED" => "Не доставлен (Возврат/Отмена)",
	"RETURNING" => "Планируется возврат",
	"RETURNED" => "Возвращен",
	"CONFIRM" => "Согласована доставка",
	"DATECHANGE" => "Перенос",
	"NEWPICKUP" => "Создан забор",
	"UNCONFIRM" => "Не удалось согласовать доставку",
	"PICKUPREADY" => "Готов к выдаче"
);

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $modulePermissions >= "W" && check_bitrix_sessid())
{
	$dbData = \WebAVK\DalliService\OrdersTable::getList(array(
				"filter" => array("ORDER_ID" => $ORDER_ID)
	));
	$arData = $dbData->Fetch();
	if (!$arData)
	{
		$arOrder = CSaleOrder::GetByID($ORDER_ID);
		if ($arOrder['ID'] <= 0)
		{
			LocalRedirect("/bitrix/admin/webavk.dalli.order.list.php?lang=" . LANG . GetFilterParams("filter_", false));
		}
		$rProps = CSaleOrderPropsValue::GetOrderProps($ORDER_ID);
		while ($arProp = $rProps->Fetch())
		{
			$arOrder['PROPS'][$arProp['CODE']] = $arProp['VALUE'];
		}
		$rProducts = CSaleBasket::GetList(array(), array("ORDER_ID" => $ORDER_ID));
		while ($arProducts = $rProducts->Fetch())
		{
			$arOrder['WEIGHT'] += $arProducts['WEIGHT'] * $arProducts['QUANTITY'];
			$arItem = CIBlockElement::GetList(array(), array("ID" => $arProducts['PRODUCT_ID']), false, false, array("ID", "IBLOCK_ID"))->Fetch();
			$arProducts['ELEMENT'] = CIBlockElement::GetList(array(), array("ID" => $arProducts['PRODUCT_ID'], "IBLOCK_ID" => $arItem['IBLOCK_ID']), false, false, array("ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE", "PROPERTY_CML2_LINK"))->Fetch();
			if ($arProducts['ELEMENT']['PROPERTY_CML2_LINK_VALUE'] > 0)
			{
				$arItem = CIBlockElement::GetList(array(), array("ID" => $arProducts['ELEMENT']['PROPERTY_CML2_LINK_VALUE']), false, false, array("ID", "IBLOCK_ID"))->Fetch();
				$arProducts['ELEMENT']['MAIN'] = CIBlockElement::GetList(array(), array("ID" => $arProducts['ELEMENT']['PROPERTY_CML2_LINK_VALUE'], "IBLOCK_ID" => $arItem['IBLOCK_ID']), false, false, array("ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE", "PROPERTY_CML2_LINK"))->Fetch();
			}
			$arOrder['BASKET'][] = $arProducts;
		}
		$arOrder["WEIGHT"] = CSaleMeasure::Convert($arOrder["WEIGHT"], "G", "KGM");
		if ($arOrder["WEIGHT"] <= 0)
		{
			$arOrder["WEIGHT"] = 0.1;
		}
		$arOrder['ADDRESS'] = array();
		if (strlen($arOrder['PROPS']['STREET']) > 0)
		{
			$arOrder['ADDRESS'][] = $arOrder['PROPS']['STREET'];
		}
		if (strlen($arOrder['PROPS']['HOUSE']) > 0)
		{
			$arOrder['ADDRESS'][] = "д. " . $arOrder['PROPS']['HOUSE'];
		}
		if (strlen($arOrder['PROPS']['CORPUS']) > 0)
		{
			$arOrder['ADDRESS'][] = "корп. " . $arOrder['PROPS']['CORPUS'];
		}
		if (strlen($arOrder['PROPS']['ROOM']) > 0)
		{
			$arOrder['ADDRESS'][] = "кв. " . $arOrder['PROPS']['ROOM'];
		}
		$arOrder['ADDRESS'] = $arOrder['PROPS']['ADDRESS']; //trim(implode(", ", $arOrder['ADDRESS']));
		$arOrder['SERVICE'] = 1;
		if (substr($arOrder['ADDRESS'], 0, 10) == "BoxBerry: ")
		{
			$arOrder['SERVICE'] = 13;
			$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
		} elseif (substr($arOrder['ADDRESS'], 0, 6) == "СДЭК: ")
		{
			$arOrder['SERVICE'] = 9;
			$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
		} elseif (substr($arOrder['ADDRESS'], 0, 8) == "PickUp: ")
		{
			$arOrder['SERVICE'] = 14;
			$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
		} elseif (substr($arOrder['ADDRESS'], 0, 14) == "DalliService: ")
		{
			$arOrder['SERVICE'] = 4;
			$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
		}

		$arOrder['PRICE'] = round($arOrder['PRICE'], 0);
		$arLocation = CSaleLocation::GetByID($arOrder['PROPS']['LOCATION'], 'ru');
		$arTime = explode("-", trim($arOrder['PROPS']['F_DELIVERY_TIME']));
		$arData = array(
			"ID" => false,
			"ORDER_ID" => $ORDER_ID,
			"REMOTE_ID" => false,
			"IS_SEND" => "N",
			"DATE_SEND" => false,
			"ORDER_DATA" => array(
				"receiver" => array(
					"town" => $arLocation['CITY_NAME'],
					"address" => $arOrder['ADDRESS'],
					"zipcode" => $arOrder['PROPS']['ZIP'],
					"person" => trim($arOrder['PROPS']['FIO']),
					"phone" => $arOrder['PROPS']['PHONE'],
					"email" => $arOrder['PROPS']['EMAIL'],
					"date" => date("Y-m-d", MakeTimeStamp($arOrder['PROPS']['F_DELIVERY_DATE'], "DD.MM.YYYY")),
					"time_min" => $arTime[0],
					"time_max" => $arTime[1],
				),
				"service" => $arOrder['SERVICE'],
				"weight" => $arOrder['WEIGHT'],
				"quantity" => 1,
				"paytype" => ($arOrder['PAY_SYSTEM_ID'] == DALLI_CARD_PAY_SYSTEM_ID ? "CARD" : "CASH"),
				"price" => $arOrder['PRICE'],
				"inshprice" => $arOrder['PRICE'],
				"priced" => $arOrder['PRICE_DELIVERY'],
				"instruction" => "Частичный выкуп возможен, при отказе за доставку брать если сумма заказа меньше 10000 руб", // "Частичный выкуп невозможен, при отказе за доставку не брать", // . $arOrder['PRICE_DELIVERY'],
				"items" => array()
			),
			"DATE_CHECK" => false,
			"ERROR_CODE" => 0,
			"ERROR_MSG" => "",
			"STATUS" => "UN",
			"STATUS_DATA" => array(),
			"ALLOW_AUTO_SEND" => "N",
			"BARCODE" => ""
		);
		$productsPrice = 0;
		foreach ($arOrder['BASKET'] as $arBasket)
		{
			$arData['ORDER_DATA']['items'][] = array(
				"name" => $arBasket['NAME'],
				"quantity" => intval($arBasket['QUANTITY']),
				"mass" => $arProducts['WEIGHT'] / 1000,
				"retprice" => $arBasket['PRICE'],
				"barcode" => (strlen($arBasket['ELEMENT']['PROPERTY_CML2_BAR_CODE_VALUE']) > 0 ? $arBasket['ELEMENT']['PROPERTY_CML2_BAR_CODE_VALUE'] : $arBasket['ELEMENT']['MAIN']['PROPERTY_CML2_BAR_CODE_VALUE']),
				"article" => (strlen($arBasket['ELEMENT']['PROPERTY_CML2_ARTICLE_VALUE']) > 0 ? $arBasket['ELEMENT']['PROPERTY_CML2_ARTICLE_VALUE'] : $arBasket['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'])
			);
			$productsPrice += $arBasket['PRICE'] * intval($arBasket['QUANTITY']);
		}
	} else
	{
		$arData['ORDER_DATA'] = unserialize($arData['ORDER_DATA']);
		$arData['STATUS_DATA'] = unserialize($arData['STATUS_DATA']);
	}
	$arData['ORDER_DATA'] = $_REQUEST['ORDER_DATA'];
	$arData['ALLOW_AUTO_SEND'] = ($_REQUEST['ALLOW_AUTO_SEND'] == "Y" ? "Y" : "N");
	$ob = new WebAVK\DalliService\OrdersTable();
	$arData['ORDER_DATA'] = serialize($arData['ORDER_DATA']);
	$arData['STATUS_DATA'] = serialize($arData['STATUS_DATA']);
	if ($arData['ID'] > 0)
	{
		$oRes = $ob->update($arData['ID'], array("ORDER_DATA" => $arData['ORDER_DATA'], "ALLOW_AUTO_SEND" => $arData['ALLOW_AUTO_SEND']));
		if (!$oRes->isSuccess())
		{
			$errorMessage .= implode("<br>", $oRes->getErrorMessages());
		}
	} else
	{
		$oRes = $ob->add($arData);
		$ID = false;
		if (!$oRes->isSuccess())
		{
			$errorMessage .= implode("<br>", $oRes->getErrorMessages());
		} else
		{
			$ID = intval($oRes->getId());
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
			LocalRedirect("/bitrix/admin/webavk.dalli.order.list.php?lang=" . LANG . GetFilterParams("filter_", false));
		else
			LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
	}
	else
	{
		$bVarsFromForm = true;
	}
} else if ($REQUEST_METHOD == "GET" && $_REQUEST['action'] == "Send" && $ORDER_ID > 0)
{
	$bAllowSend = true;
	if (function_exists("IsAllowSendOrderToDalliService"))
	{
		$bAllowSend = IsAllowSendOrderToDalliService($ORDER_ID);
	}
	if ($bAllowSend)
	{
		$dbData = \WebAVK\DalliService\OrdersTable::getList(array(
					"filter" => array("ORDER_ID" => $ORDER_ID)
		));
		$arData = $dbData->Fetch();
		if ($arData && $arData['IS_SEND'] != "Y")
		{
			$arResult = CDeliveryServiceDalliDriver::doSendOrder($arData['ID']);
			if ($arResult['error'] == 0)
			{
				LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?sendOk=Y&lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
			} else
			{
				LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?sendError=Y&lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
			}
		} else
		{
			LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
		}
	} else
	{
		LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?sendErrorCard=Y&lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
	}
} else if ($REQUEST_METHOD == "GET" && $_REQUEST['action'] == "UpdateStatus" && $ORDER_ID > 0)
{
	$dbData = \WebAVK\DalliService\OrdersTable::getList(array(
				"filter" => array("ORDER_ID" => $ORDER_ID)
	));
	$arData = $dbData->Fetch();
	if ($arData)
	{
		$arResult = CDeliveryServiceDalliDriver::doCheckStatus($arData['ID']);
		if (strlen($arResult['status']) > 0)
		{
			LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?statusOk=Y&lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
		} else
		{
			LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?statusError=Y&lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
		}
	} else
	{
		LocalRedirect("/bitrix/admin/webavk.dalli.order.detail.php?lang=" . LANG . "&ORDER_ID=" . $ORDER_ID . GetFilterParams("filter_", false));
	}
}

if ($ORDER_ID > 0)
{
	$APPLICATION->SetTitle(str_replace("#ORDER_ID#", $ORDER_ID, "Отправка заказа #ORDER_ID#"));
} else
{
	LocalRedirect("/bitrix/admin/webavk.dalli.order.list.php?lang=" . LANG . GetFilterParams("filter_", false));
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arServices = CDeliveryServiceDalliDriver::doGetDeliveryType();
$arServicesRef = array();
foreach ($arServices as $k => $v)
{
	$arServicesRef['reference_id'][] = $k;
	$arServicesRef['reference'][] = $v;
}
$dbData = \WebAVK\DalliService\OrdersTable::getList(array(
			"filter" => array("ORDER_ID" => $ORDER_ID)
		));
$arData = $dbData->fetch();
if (!$arData)
{
	$arOrder = CSaleOrder::GetByID($ORDER_ID);
	if ($arOrder['ID'] <= 0)
	{
		LocalRedirect("/bitrix/admin/webavk.dalli.order.list.php?lang=" . LANG . GetFilterParams("filter_", false));
	}
	$rProps = CSaleOrderPropsValue::GetOrderProps($ORDER_ID);
	while ($arProp = $rProps->Fetch())
	{
		if (!isset($arOrder['PROPS'][$arProp['CODE']]) || (isset($arOrder['PROPS'][$arProp['CODE']]) && strlen($arProp['VALUE']) > 0))
		{
			$arOrder['PROPS'][$arProp['CODE']] = $arProp['VALUE'];
		}
	}
	$rProducts = CSaleBasket::GetList(array(), array("ORDER_ID" => $ORDER_ID));
	while ($arProducts = $rProducts->Fetch())
	{
		$arOrder['WEIGHT'] += $arProducts['WEIGHT'] * $arProducts['QUANTITY'];
		$arItem = CIBlockElement::GetList(array(), array("ID" => $arProducts['PRODUCT_ID']), false, false, array("ID", "IBLOCK_ID"))->Fetch();
		$arProducts['ELEMENT'] = CIBlockElement::GetList(array(), array("ID" => $arProducts['PRODUCT_ID'], "IBLOCK_ID" => $arItem['IBLOCK_ID']), false, false, array("ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE", "PROPERTY_CML2_LINK"))->Fetch();
		if ($arProducts['ELEMENT']['PROPERTY_CML2_LINK_VALUE'] > 0)
		{
			$arItem = CIBlockElement::GetList(array(), array("ID" => $arProducts['ELEMENT']['PROPERTY_CML2_LINK_VALUE']), false, false, array("ID", "IBLOCK_ID"))->Fetch();
			$arProducts['ELEMENT']['MAIN'] = CIBlockElement::GetList(array(), array("ID" => $arProducts['ELEMENT']['PROPERTY_CML2_LINK_VALUE'], "IBLOCK_ID" => $arItem['IBLOCK_ID']), false, false, array("ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE", "PROPERTY_CML2_LINK"))->Fetch();
		}
		$arOrder['BASKET'][] = $arProducts;
	}
	$arOrder["WEIGHT"] = CSaleMeasure::Convert($arOrder["WEIGHT"], "G", "KGM");
	if ($arOrder["WEIGHT"] <= 0)
	{
		$arOrder["WEIGHT"] = 0.1;
	}
	$arOrder['ADDRESS'] = array();
	if (strlen($arOrder['PROPS']['STREET']) > 0)
	{
		$arOrder['ADDRESS'][] = $arOrder['PROPS']['STREET'];
	}
	if (strlen($arOrder['PROPS']['HOUSE']) > 0)
	{
		$arOrder['ADDRESS'][] = "д. " . $arOrder['PROPS']['HOUSE'];
	}
	if (strlen($arOrder['PROPS']['CORPUS']) > 0)
	{
		$arOrder['ADDRESS'][] = "корп. " . $arOrder['PROPS']['CORPUS'];
	}
	if (strlen($arOrder['PROPS']['ROOM']) > 0)
	{
		$arOrder['ADDRESS'][] = "кв. " . $arOrder['PROPS']['ROOM'];
	}
	$arOrder['ADDRESS'] = $arOrder['PROPS']['ADDRESS']; //trim(implode(", ", $arOrder['ADDRESS']));
	$arOrder['SERVICE'] = 1;
	if (substr($arOrder['ADDRESS'], 0, 10) == "BoxBerry: ")
	{
		$arOrder['SERVICE'] = 13;
		$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
	} elseif (substr($arOrder['ADDRESS'], 0, 6) == "СДЭК: ")
	{
		$arOrder['SERVICE'] = 9;
		$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
	} elseif (substr($arOrder['ADDRESS'], 0, 8) == "PickUp: ")
	{
		$arOrder['SERVICE'] = 14;
		$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
	} elseif (substr($arOrder['ADDRESS'], 0, 14) == "DalliService: ")
	{
		$arOrder['SERVICE'] = 4;
		$arOrder['ADDRESS'] = trim(substr($arOrder['ADDRESS'], strpos($arOrder['ADDRESS'], "]") + 1));
	} elseif ($arOrder['DELIVERY_ID'] == "dalli:spb")
	{
		$arOrder['SERVICE'] = 11;
	} elseif ($arOrder['DELIVERY_ID'] == "dalli:msk")
	{
		$arOrder['SERVICE'] = 1;
	} elseif ($arOrder['DELIVERY_ID'] == "dalli:sdek_courier")
	{
		$arOrder['SERVICE'] = 10;
	}
	$arLocation = CSaleLocation::GetByID($arOrder['PROPS']['LOCATION'], 'ru');
	$arTime = explode("-", trim($arOrder['PROPS']['F_DELIVERY_TIME']));
	$arOrder['PRICE'] = round($arOrder['PRICE'], 0);

	$arData = array(
		"ID" => false,
		"ORDER_ID" => $ORDER_ID,
		"REMOTE_ID" => false,
		"IS_SEND" => "N",
		"DATE_SEND" => false,
		"ORDER_DATA" => array(
			"receiver" => array(
				"town" => $arLocation['CITY_NAME'],
				"address" => $arOrder['ADDRESS'],
				"zipcode" => $arOrder['PROPS']['ZIP'],
				"person" => trim($arOrder['PROPS']['FIO']),
				"phone" => $arOrder['PROPS']['PHONE'],
				"email" => $arOrder['PROPS']['EMAIL'],
				"date" => date("Y-m-d", MakeTimeStamp($arOrder['PROPS']['F_DELIVERY_DATE'], "DD.MM.YYYY")),
				"time_min" => $arTime[0],
				"time_max" => $arTime[1],
			),
			"service" => $arOrder['SERVICE'],
			"weight" => $arOrder['WEIGHT'],
			"quantity" => 1,
			"paytype" => ($arOrder['PAY_SYSTEM_ID'] == DALLI_CARD_PAY_SYSTEM_ID ? "CARD" : "CASH"),
			"price" => $arOrder['PRICE'],
			"inshprice" => $arOrder['PRICE'],
			"priced" => $arOrder['PRICE_DELIVERY'],
			"instruction" => "Частичный выкуп возможен, при отказе за доставку брать если сумма заказа меньше 10000 руб", // "Частичный выкуп невозможен, при отказе за доставку не брать", // . $arOrder['PRICE_DELIVERY'],
			"items" => array()
		),
		"DATE_CHECK" => false,
		"ERROR_CODE" => 0,
		"ERROR_MSG" => "",
		"STATUS" => "UN",
		"STATUS_DATA" => array(),
		"ALLOW_AUTO_SEND" => "N",
		"BARCODE" => ""
	);
	$productsPrice = 0;
	foreach ($arOrder['BASKET'] as $arBasket)
	{
		$arData['ORDER_DATA']['items'][] = array(
			"name" => $arBasket['NAME'],
			"quantity" => intval($arBasket['QUANTITY']),
			"mass" => $arProducts['WEIGHT'] / 1000,
			"retprice" => $arBasket['PRICE'],
			"barcode" => (strlen($arBasket['ELEMENT']['PROPERTY_CML2_BAR_CODE_VALUE']) > 0 ? $arBasket['ELEMENT']['PROPERTY_CML2_BAR_CODE_VALUE'] : $arBasket['ELEMENT']['MAIN']['PROPERTY_CML2_BAR_CODE_VALUE']),
			"article" => (strlen($arBasket['ELEMENT']['PROPERTY_CML2_ARTICLE_VALUE']) > 0 ? $arBasket['ELEMENT']['PROPERTY_CML2_ARTICLE_VALUE'] : $arBasket['ELEMENT']['MAIN']['PROPERTY_CML2_ARTICLE_VALUE'])
		);
		$productsPrice += $arBasket['PRICE'] * intval($arBasket['QUANTITY']);
	}
} else
{
	$arData['ORDER_DATA'] = unserialize($arData['ORDER_DATA']);
	$arData['STATUS_DATA'] = unserialize($arData['STATUS_DATA']);
	/*
	  $strPrefix = "str_";
	  foreach ($arDeliveryDateDays as $key => $val) {
	  $varname = $strPrefix . $key;
	  global $$varname;

	  if (!is_array($val) && !is_object($val)) {
	  $$varname = htmlspecialcharsEx($val);
	  } else {
	  $$varname = $val;
	  }
	  }
	 * 
	 */
}

/*
  if (strlen($str_DELIVERY_DATE) > 0) {
  $str_DELIVERY_DATE = ConvertTimeStamp(MakeTimeStamp($str_DELIVERY_DATE, "YYYY-MM-DD"), FORMAT_DATE);
  }
  if (strlen($str_PERIOD_START) > 0) {
  $str_PERIOD_START = ConvertTimeStamp(MakeTimeStamp($str_PERIOD_START, "YYYY-MM-DD"), FORMAT_DATE);
  }
  if (strlen($str_PERIOD_END) > 0) {
  $str_PERIOD_END = ConvertTimeStamp(MakeTimeStamp($str_PERIOD_END, "YYYY-MM-DD"), FORMAT_DATE);
  }
 */
if ($bVarsFromForm)
{
	$DB->InitTableVarsForEdit("wadl_orders", "", "str_");
//	$arDELIVERY_ID = $_REQUEST['DELIVERY'];
}

$aMenu = array(
	array(
		"TEXT" => "Список",
		"LINK" => "/bitrix/admin/webavk.dalli.order.list.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_list"
	)
);

if ($arData['ID'] > 0)
{
	if ($arData['STATUS'] == "UN")
	{
		$aMenu[] = array(
			"TEXT" => "Отправить",
			"LINK" => "/bitrix/admin/webavk.dalli.order.detail.php?ORDER_ID=" . $ORDER_ID . "&action=Send&lang=" . LANG . GetFilterParams("filter_"),
			"ICON" => "btn_send"
		);
	}

	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => "Обновить статус",
		"LINK" => "/bitrix/admin/webavk.dalli.order.detail.php?ORDER_ID=" . $ORDER_ID . "&action=UpdateStatus&lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_status"
	);
}
$context = new CAdminContextMenu($aMenu);
$context->Show();

if ($_REQUEST['sendOk'] == "Y")
{
	echo CAdminMessage::ShowMessage(Array("DETAILS" => "Заказ успешно отправлен", "TYPE" => "OK", "MESSAGE" => "Заказ успешно отправлен", "HTML" => true));
} elseif ($_REQUEST['sendError'] == "Y")
{
	echo CAdminMessage::ShowMessage(Array("DETAILS" => "Ошибка отправки заказа", "TYPE" => "ERROR", "MESSAGE" => "Ошибка отправки заказа", "HTML" => true));
} elseif ($_REQUEST['sendErrorCard'] == "Y")
{
	echo CAdminMessage::ShowMessage(Array("DETAILS" => "Ошибка отправки заказа", "TYPE" => "ERROR", "MESSAGE" => "Отправка заказа в DalliService при оплате картой возможно только после оплаты заказа клиентом", "HTML" => true));
} elseif ($_REQUEST['statusOk'] == "Y")
{
	echo CAdminMessage::ShowMessage(Array("DETAILS" => "Статус успешно обновлен", "TYPE" => "OK", "MESSAGE" => "Статус успешно обновлен", "HTML" => true));
} elseif ($_REQUEST['statusError'] == "Y")
{
	echo CAdminMessage::ShowMessage(Array("DETAILS" => "Ошибка обновления статуса", "TYPE" => "ERROR", "MESSAGE" => "Ошибка обновления статуса", "HTML" => true));
}
?>
<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?" name="form1">
	<? echo GetFilterHiddens("filter_"); ?>
	<input type="hidden" name="Update" value="Y">
	<input type="hidden" name="lang" value="<? echo LANG ?>">
	<input type="hidden" name="ORDER_ID" value="<? echo $ORDER_ID ?>">

	<?= bitrix_sessid_post() ?>
	<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
	?>
	<tr>
		<td width="40%">ID:</td>
		<td width="60%"><?= ($arData['ID'] > 0 ? $arData['ID'] : "Не сохранено") ?></td>
	</tr>
	<tr>
		<td>ID заказа</td>
		<td><?= $ORDER_ID ?></td>
	</tr>
	<tr>
		<td>Удаленный ID</td>
		<td><?= $arData['REMOTE_ID'] ?></td>
	</tr>
	<tr>
		<td>Отправлено в Dalli Service</td>
		<td><?= ($arData['IS_SEND'] == "Y" ? "Да" : "Нет") ?></td>
	</tr>
	<tr>
		<td>Дата отправки</td>
		<td><?= $arData['DATE_SEND'] ?></td>
	</tr>
	<tr>
		<td>Дата проверки статуса</td>
		<td><?= $arData['DATE_CHECK'] ?></td>
	</tr>
	<tr>
		<td>Код ошибки</td>
		<td>[<?= $arData['ERROR_CODE'] ?>] <?= $arErrorCodes[$arData['ERROR_CODE']] ?></td>
	</tr>
	<tr>
		<td>Сообщение ошибки</td>
		<td><?= $arData['ERROR_MSG'] ?></td>
	</tr>
	<tr>
		<td>Статус</td>
		<td>[<?= $arData['STATUS'] ?>] <?= $arStatusCodes[$arData['STATUS']] ?></td>
	</tr>
	<tr>
		<td>Штрих-код заказа</td>
		<td><?= $arData['BARCODE'] ?></td>
	</tr>
	<tr class="heading">
		<td colspan="2">Данные заказа</td>
	</tr>
	<tr>
		<td>Город получателя</td>
		<td><input type="text" name="ORDER_DATA[receiver][town]" value="<?= $arData['ORDER_DATA']['receiver']['town'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Адрес получтеля</td>
		<td><input type="text" name="ORDER_DATA[receiver][address]" value="<?= $arData['ORDER_DATA']['receiver']['address'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Индекс получателя</td>
		<td><input type="text" name="ORDER_DATA[receiver][zipcode]" value="<?= $arData['ORDER_DATA']['receiver']['zipcode'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>ФИО получателя</td>
		<td><input type="text" name="ORDER_DATA[receiver][person]" value="<?= $arData['ORDER_DATA']['receiver']['person'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Телефон получателя</td>
		<td><input type="text" name="ORDER_DATA[receiver][phone]" value="<?= $arData['ORDER_DATA']['receiver']['phone'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Email получателя</td>
		<td><input type="text" name="ORDER_DATA[receiver][email]" value="<?= $arData['ORDER_DATA']['receiver']['email'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Дата доставки (YYYY-MM-DD)</td>
		<td><input type="text" name="ORDER_DATA[receiver][date]" value="<?= $arData['ORDER_DATA']['receiver']['date'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Время доставки (от)</td>
		<td><input type="text" name="ORDER_DATA[receiver][time_min]" value="<?= $arData['ORDER_DATA']['receiver']['time_min'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Время доставки (до)</td>
		<td><input type="text" name="ORDER_DATA[receiver][time_max]" value="<?= $arData['ORDER_DATA']['receiver']['time_max'] ?>" size="50" /></td>
	</tr>
	<tr>
		<td>Режим доставки</td>
		<td>
			<?= SelectBoxFromArray("ORDER_DATA[service]", $arServicesRef, $arData['ORDER_DATA']['service'], "") ?>
		</td>
	</tr>
	<tr>
		<td>Вес заказа (кг)</td>
		<td><input type="text" name="ORDER_DATA[weight]" value="<?= $arData['ORDER_DATA']['weight'] ?>" /></td>
	</tr>
	<tr>
		<td>Количество мест</td>
		<td><input type="text" name="ORDER_DATA[quantity]" value="<?= $arData['ORDER_DATA']['quantity'] ?>" /></td>
	</tr>
	<tr>
		<td>Тип оплаты</td>
		<td>
			<?=
			SelectBoxFromArray("ORDER_DATA[paytype]", array(
				"reference_id" => array("CASH", "CARD", "NO"),
				"reference" => array("наличными, при получении", "картой, при получении", "без оплаты"),
					), $arData['ORDER_DATA']['paytype'], "")
			?>
		</td>
	</tr>
	<tr>
		<td>Стоимость заказа</td>
		<td><input type="text" name="ORDER_DATA[price]" value="<?= $arData['ORDER_DATA']['price'] ?>" /></td>
	</tr>
	<tr>
		<td>Объявленная ценность</td>
		<td><input type="text" name="ORDER_DATA[inshprice]" value="<?= $arData['ORDER_DATA']['inshprice'] ?>" /></td>
	</tr>
	<tr>
		<td>Стоимость доставки</td>
		<td><input type="text" name="ORDER_DATA[priced]" value="<?= $arData['ORDER_DATA']['priced'] ?>" /></td>
	</tr>
	<tr>
		<td>Инструкция, примечание</td>
		<td><input type="text" name="ORDER_DATA[instruction]" value="<?= $arData['ORDER_DATA']['instruction'] ?>" size="100" /></td>
	</tr>
	<tr>
		<td>Разрешить автоотправку после сохранения</td>
		<td><input type="checkbox" name="ALLOW_AUTO_SEND" value="Y"<?
			if ($arData['ALLOW_AUTO_SEND'] == "Y")
			{
				?> checked="checked"<? } ?> /></td>
	</tr>
	<tr class="heading">
		<td colspan="2">Состав заказа</td>
	</tr>
	<tr>
		<td colspan="2" valign="top">
			<table class="internal" cellspacing="1" cellpadding="3" border="0" width="100%">
				<tbody>
					<tr class="heading">
						<td>№</td>
						<td>Название</td>
						<td>Количество</td>
						<td>Вес</td>
						<td>Цена</td>
						<td>Штрих-код</td>
						<td>Артикул</td>
					</tr>
					<?
					$i = 1;
					foreach ($arData['ORDER_DATA']['items'] as $k => $arItem)
					{
						?>
						<tr>
							<td><?= $i ?>.</td>
							<td><input type="text" name="ORDER_DATA[items][<?= $k ?>][name]" maxlength="128" value="<?= $arItem['name'] ?>" /></td>
							<td><input type="text" name="ORDER_DATA[items][<?= $k ?>][quantity]" value="<?= $arItem['quantity'] ?>" /></td>
							<td><input type="text" name="ORDER_DATA[items][<?= $k ?>][mass]" value="<?= $arItem['mass'] ?>" /></td>
							<td><input type="text" name="ORDER_DATA[items][<?= $k ?>][retprice]" value="<?= $arItem['retprice'] ?>" /></td>
							<td><input type="text" name="ORDER_DATA[items][<?= $k ?>][barcode]" value="<?= $arItem['barcode'] ?>" /></td>
							<td><input type="text" name="ORDER_DATA[items][<?= $k ?>][article]" value="<?= $arItem['article'] ?>" /></td>
							<?
							$i++;
						}
						?>
				</tbody>
			</table>
		</td>
	</tr>
	<?
	$tabControl->EndTab();
	?>

	<?
	$tabControl->Buttons(
			array(
				"disabled" => ($modulePermissions < "W" || $arData['STATUS'] != "UN"),
				"back_url" => "/bitrix/admin/webavk.deliverydate.days.php?lang=" . LANG . GetFilterParams("filter_")
			)
	);
	?>

	<?
	$tabControl->End();
	?>

</form>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>