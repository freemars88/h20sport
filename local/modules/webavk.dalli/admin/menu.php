<?

IncludeModuleLangFile(__FILE__);

if ($APPLICATION->GetGroupRight("webavk.dalli") != "D") {
	$aMenu = array(
		"parent_menu" => "global_menu_store",
		"section" => "webavk.dalli",
		"sort" => 100,
		"module_id" => "webavk.dalli",
		"text" => GetMessage("WEBAVK_DALLI_MENU_MAIN"),
		"title" => GetMessage("WEBAVK_DALLI_MENU_MAIN_TITLE"),
		"icon" => "webavk_dalli_menu_icon",
		"page_icon" => "webavk_dallipage_icon",
		"items_id" => "menu_webavk_dalli",
		"items" => array(
			array(
				"text" => GetMessage("WEBAVK_DALLI_MENU_ORDER_LIST"),
				"url" => "webavk.dalli.order.list.php?lang=" . LANGUAGE_ID,
				"title" => GetMessage("WEBAVK_DALLI_MENU_ORDER_LIST_ALT"),
				"more_url" => Array("webavk.dalli.order.detail.php")
			),
		)
	);
	return $aMenu;
}
return false;
