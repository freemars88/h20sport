<?

IncludeModuleLangFile(__FILE__);
\CModule::IncludeModule("iblock");

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Main;

define('DELIVERY_DALLI_CACHE_LIFETIME', 60 * 60 * 24 * 30); // cache lifetime - 30 days (60*60*24*30)

define('DELIVERY_DALLI_SERVER', 'api.dalli-service.com'); // server name to send data
define('DELIVERY_DALLI_SERVER_PORT', 443); // server port
define('DELIVERY_DALLI_SERVER_PAGE', '/v1/'); // server page url
define('DELIVERY_DALLI_SERVER_METHOD', 'POST'); // data send method
define('DELIVERY_DALLI_SERVER_PROTO', 'ssl://'); // data send method

define('DELIVERY_DALLI_WRITE_LOG', 1);

Class CWebavkDalli
{

	function DoAdminContextMenuShow(&$items)
	{
		global $APPLICATION;
		if (($APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_detail.php' || $APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_view.php') && $_REQUEST['ID'] > 0)
		{
			$orderId = intval($_REQUEST['ID']);
			$oOrder = \Bitrix\Sale\Order::load($orderId);
			if (in_array($oOrder->getField("DELIVERY_ID"), array(DELIVERY_DALLI1_ID, DELIVERY_DALLI2_ID, DELIVERY_DALLI3_ID, DELIVERY_DALLI4_ID, DELIVERY_DALLI5_ID, DELIVERY_DALLI6_ID, DELIVERY_DALLI7_ID, DELIVERY_DALLI8_ID, DELIVERY_DALLI9_ID)))
			{
				$items[] = array(
					'TEXT' => GetMessage("webavk.dalli_ORDER_SEND_DALLI"),
					'TITLE' => GetMessage('webavk.dalli_ORDER_SEND_DALLI_TITLE'),
					'LINK' => '/bitrix/admin/webavk.dalli.order.detail.php?ORDER_ID=' . $orderId,
					'ICON' => 'btn_order_send_dalli',
					'LINK_PARAM' => 'target="_blank"'
				);
			}
		}
	}

}

CModule::AddAutoloadClasses(
		"webavk.dalli", array(
	"CDeliveryServiceDalliDriver" => "classes/general/driver.php",
	"CDeliveryDalli" => "classes/general/delivery_dalli.php",
	"WebAVK\\DalliService\\OrdersTable" => "lib/orders.php",
		)
);

