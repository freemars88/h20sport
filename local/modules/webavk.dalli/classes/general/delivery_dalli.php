<?

IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("sale");

class CDeliveryDalli
{

	function Init()
	{
		return array(
			/* Основное описание */
			"SID" => "dalli",
			"NAME" => GetMessage("WEBAVK_DALLI_DELIVERY_TITLE"),
			"DESCRIPTION" => "",
			"DESCRIPTION_INNER" => "", /*
			  "Простой обработчик курьерской доставки. Для функционирования необходимо "
			  . "наличие хотя бы одной группы местоположений. При настройке обработчика указывается "
			  . "фиксированная стоимость доставки для каждой группы местоположений. Для того, чтобы "
			  . "группа не участвовала в обработке, оставьте пустым поле стоимости для этой группы."
			  . "<br />"
			  . "<a href=\"/bitrix/admin/sale_location_group_admin.php?lang=ru\" target=\"_blank\">"
			  . "Редактировать группы местоположений"
			  . "</a>.", */
			"BASE_CURRENCY" => COption::GetOptionString("sale", "default_currency", "RUB"),
			"HANDLER" => __FILE__,
			/* Методы обработчика */
			"DBGETSETTINGS" => array("CDeliveryDalli", "GetSettings"),
			"DBSETSETTINGS" => array("CDeliveryDalli", "SetSettings"),
			"GETCONFIG" => array("CDeliveryDalli", "GetConfig"),
			"COMPABILITY" => array("CDeliveryDalli", "Compability"),
			"CALCULATOR" => array("CDeliveryDalli", "Calculate"),
			/* Список профилей доставки */
			"PROFILES" => array(
				"msk" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_MSK_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_MSK_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
				"pvz_dalli" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_DALLI_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_DALLI_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
				"pvz_sdek" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_SDEK_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_SDEK_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
				"sdek_courier" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_SDEK_COURIER_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_SDEK_COURIER_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
				"spb" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_SPB_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_SPB_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
				"pvz_boxberry" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_BOXBERRY_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_BOXBERRY_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
				"pvz_pickup" => array(
					"TITLE" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_PICKUP_TITLE"),
					"DESCRIPTION" => GetMessage("WEBAVK_DALLI_DELIVERY_PVZ_PICKUP_DESCRIPTION"),
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
				),
			)
		);
	}

	function GetConfig()
	{
		$arConfig = array(
			"CONFIG_GROUPS" => array(
			//"delivery" => GetMessage('SALE_DH_DHL_USA_CONFIG_DELIVERY_TITLE'),
			),
			"CONFIG" => array(
			/*
			  "package_type" => array(
			  "TYPE" => "DROPDOWN",
			  "DEFAULT" => DELIVERY_DHL_USA_PACKAGE_TYPE_DEFAULT,
			  "TITLE" => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE'),
			  "GROUP" => "delivery",
			  "VALUES" => array(
			  'EE' => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_EE'),
			  'OD' => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_OD'),
			  'CP' => GetMessage('SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_CP'),
			  ),
			  ), */
			),
		);

		return $arConfig;
	}

	function GetSettings($strSettings)
	{
		return unserialize($strSettings);
	}

	function SetSettings($arSettings)
	{
		return serialize($arSettings);
	}

	function __GetLocation($location_id)
	{
		$arLocation = CSaleLocation::GetByID($location_id, 'ru');
		return $arLocation;
	}

	function Calculate($profile, $arConfig, $arOrder, $STEP = 1, $TEMP = false)
	{
		$arLocationTo = CDeliveryDalli::__GetLocation($arOrder['LOCATION_TO']);
		if ($profile == "msk")
		{
			return array(
				"RESULT" => "OK",
				"VALUE" => 300,
				"TRANSIT" => "",
				"IS_FROM" => true
			);
		} elseif ($profile == "spb")
		{
			return array(
				"RESULT" => "OK",
				"VALUE" => 350,
				"TRANSIT" => "",
				"IS_FROM" => true
			);
		}
		/* if ($arLocationTo['CITY_NAME'] == "Москва" || $arLocationTo['REGION_NAME_ORIG'] == "Московская область") {
		  if ($arOrder['PRICE'] >= 5000) {
		  if ($arLocationTo['REGION_NAME_ORIG'] == "Московская область") {
		  return array(
		  "RESULT" => "OK",
		  "VALUE" => 300,
		  "TRANSIT" => "",
		  "IS_FROM" => true
		  );
		  } else {
		  return array(
		  "RESULT" => "OK",
		  "VALUE" => 0,
		  "TRANSIT" => "",
		  );
		  }
		  } else {
		  return array(
		  "RESULT" => "OK",
		  "VALUE" => 300,
		  "TRANSIT" => "",
		  );
		  }
		  } elseif (($arLocationTo['CITY_NAME'] == "Санкт-Петербург" || $arLocationTo['REGION_NAME_ORIG'] == "Ленинградская область")) {
		  if ($arLocationTo['REGION_NAME_ORIG'] == "Ленинградская область") {
		  return array(
		  "RESULT" => "OK",
		  "VALUE" => 300,
		  "TRANSIT" => "",
		  "IS_FROM" => true
		  );
		  } else {
		  if ($arOrder['PRICE'] >= 5000) {
		  return array(
		  "RESULT" => "OK",
		  "VALUE" => 0,
		  "TRANSIT" => "",
		  );
		  } else {
		  return array(
		  "RESULT" => "OK",
		  "VALUE" => 280,
		  "TRANSIT" => "",
		  );
		  }
		  }
		  } else { */
		if ($arLocationTo['ID'] > 0 && strlen($arLocationTo['CITY_NAME']) > 0)
		{
			$arOrder["WEIGHT"] = CSaleMeasure::Convert($arOrder["WEIGHT"], "G", "KGM");
			if ($arOrder["WEIGHT"] <= 0)
				$arOrder["WEIGHT"] = 0.1;
			$cache_id = "dalli" . "|" . $arConfig["category"]['VALUE'] . "|" . serialize($profile) . "|" . $arOrder["LOCATION_TO"] . "|" . intval($arOrder['WEIGHT']);
			$obCache = new CPHPCache();
			if ($obCache->InitCache(DELIVERY_DALLI_CACHE_LIFETIME, $cache_id, "/"))
			{
				// cache found
				$vars = $obCache->GetVars();
				$result = $vars["RESULT"];
				$transit_time = $vars["TRANSIT"];

				return array(
					"RESULT" => "OK",
					"VALUE" => $result,
					"TRANSIT" => $transit_time
				);
			}
			$partner = "SDEK";
			$typedelivery = "KUR";
			switch ($profile)
			{
				case "msk":
					$partner = "DS";
					$typedelivery = "KUR";
					break;
				case "pvz_dalli":
					$partner = "DS";
					$typedelivery = "PVZ";
					break;
				case "pvz_sdek":
					$partner = "SDEK";
					$typedelivery = "PVZ";
					break;
				case "sdek_courier":
					$partner = "SDEK";
					$typedelivery = "KUR";
					break;
				case "spb":
					$partner = "DS";
					$typedelivery = "KUR";
					break;
				case "pvz_boxberry":
					$partner = "BOXBERRY";
					$typedelivery = "PVZ";
					break;
				case "pvz_pickup":
					$partner = "PICKPOINT";
					$typedelivery = "PVZ";
                    break;
			}
			$arData = CDeliveryServiceDalliDriver::doGetDeliveryCost($partner, $arLocationTo['CITY_NAME'], $arOrder["WEIGHT"], $arOrder["PRICE"], $arOrder["PRICE"], COption::GetOptionString("webavk.dalli", "length", 10), COption::GetOptionString("webavk.dalli", "width", 10), COption::GetOptionString("webavk.dalli", "height", 10), $typedelivery);

            if ($arData['error'] <= 0)
			{
				$obCache->StartDataCache();
				$result = doubleval($arData['price']);
				if ($partner == "DS" && $typedelivery == "KUR")
				{
					$result = 300;
				}
				$transit_time = "";
				if (strlen($arData['delivery_period']) > 0)
				{
					$transit_time = $arData['delivery_period'];
				}
				if (strlen($arData['msg']) > 0)
				{
					$transit_time .= " (" . $arData['msg'] . ")";
				}
				$transit_time = trim($transit_time);
				$obCache->EndDataCache(
						array(
							"RESULT" => $result,
							"TRANSIT" => $transit_time,
						)
				);
				return array(
					"RESULT" => "OK",
					"VALUE" => $result,
					"TRANSIT" => $transit_time,
				);
			} else
			{
				return array(
					"RESULT" => "ERROR",
					"TEXT" => $arData['errormsg']
				);
			}


			return array(
				"RESULT" => "OK",
				"VALUE" => $result,
				"TRANSIT" => $transit_time,
			);
		}
		//}
		return array(
			"RESULT" => "ERROR",
			"TEXT" => GetMessage('SALE_DH_DHL_USA_ERROR_RESPONSE'),
		);
	}

	function Compability($arOrder)
	{
		$arLocationTo = CDeliveryDalli::__GetLocation($arOrder['LOCATION_TO']);
		$arResult = array('pvz_dalli', 'pvz_sdek', 'pvz_boxberry', "pvz_pickup");
		if ($arLocationTo['CITY_NAME'] == "Москва" || $arLocationTo['REGION_NAME_ORIG'] == "Московская область")
		{
			$arResult[] = "msk";
		} elseif ($arLocationTo['CITY_NAME'] == "Санкт-Петербург" || $arLocationTo['REGION_NAME_ORIG'] == "Ленинградская область")
		{
			$arResult[] = "spb";
			//$arResult[] = "sdek_courier";
		} else
		{
			$arResult[] = "sdek_courier";
		}
		foreach ($arResult as $k => $profile)
		{
			$arCalc = self::Calculate($profile, self::GetConfig(), $arOrder);
			if ($arCalc['RESULT'] == "ERROR")
			{
				unset($arResult[$k]);
			}
		}
		return $arResult;
	}

}
