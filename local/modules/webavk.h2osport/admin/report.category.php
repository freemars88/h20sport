<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule('webavk.h2osport');

$modulePermissions = $APPLICATION->GetGroupRight("webavk.h2osport");
if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
//if ($saleModulePermissions == "D")
//	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if (!CModule::IncludeModule('sale'))
{
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(GetMessage("webavk.h2osport_MODULE_SALE_REQUIRED"));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}
$IBLOCK_ID = 16;
$SKU_IBLOCK_ID = 17;

CJSCore::RegisterExt('report_category_chart', array(
	'js' => array(
		'/bitrix/js/main/amcharts/3.3/amcharts.js',
		'/bitrix/js/main/amcharts/3.3/funnel.js',
		'/bitrix/js/main/amcharts/3.3/serial.js',
		'/bitrix/js/main/amcharts/3.3/themes/light.js',
	),
	'rel' => array('ajax', "date")
));
CJSCore::Init(array("report_category_chart"));

$sTableID = "tbl_webavk_h2osport_report_category";

$oSort = new CAdminSorting($sTableID, "DATE_PERIOD_START", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

// FILTER
$arFilterFields = array(
	"filter_period_type",
	"filter_date_from",
	"filter_date_to",
	"filter_category",
	"filter_include_subsection",
	"filter_report_view"
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array();

if (in_array($filter_period_type, array("W", "M", "A")))
{
	$arFilter['PERIOD_TYPE'] = $filter_period_type;
}
if (strlen($filter_date_from) > 0)
{
	$arFilter["DATE_FROM"] = Trim($filter_date_from);
} else
{
	$arFilter["DATE_FROM"] = ConvertTimeStamp(0, "FULL");
}
if (strlen($filter_date_to) > 0)
{
	if ($arDate = ParseDateTime($filter_date_to, CSite::GetDateFormat("FULL", SITE_ID)))
	{
		if (StrLen($filter_date_to) < 11)
		{
			$arDate["HH"] = 23;
			$arDate["MI"] = 59;
			$arDate["SS"] = 59;
		}

		$filter_date_to = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"], $arDate["YYYY"]));
		$arFilter["DATE_TO"] = $filter_date_to;
	} else
	{
		$filter_date_to = "";
	}
}
if (strlen($arFilter["DATE_TO"]) <= 0)
{
	$arFilter["DATE_TO"] = ConvertTimeStamp(time(), "FULL");
}

$arResult = array();
global $by, $order;

function bxStatSort($a, $b)
{
	global $by, $order;
	$by = toUpper($by);
	$order = toUpper($order);
	if (in_array($by, Array("DATE_PERIOD_START", "DATE_PERIOD_END")))
	{
		if (DoubleVal($a[$by]) == DoubleVal($b[$by]))
			return 0;
		elseif (MakeTimeStamp($a[$by], FORMAT_DATETIME) > MakeTimeStamp($b[$by], FORMAT_DATETIME))
			return ($order == "DESC") ? -1 : 1;
		else
			return ($order == "DESC") ? 1 : -1;
	}
}

if (strlen($arFilter['PERIOD_TYPE']) > 0 && !empty($filter_category))
{
	$obCache = new CPHPCache();
	if ($obCache->InitCache(600, serialize(array($arFilter, $filter_category, $filter_include_subsection, $filter_report_view)) . "_webavk_h2osport_report_category", "/webavk_h2osport_report_category"))
	{
		$arResult = $obCache->GetVars();
	} elseif ($obCache->StartDataCache())
	{

		// START GET DATA
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("sale");
		CModule::IncludeModule("catalog");

		$obElement = new CIBlockElement();
		$obCatalogSKU = new CCatalogSKU();
		$obBasket = new CSaleBasket();
		$obSale = new CSaleOrder();

		$arSectionsStructure = array();
		$arElementsLinks = array();
		//Создаем структуру разделов
		$bIsChild = false;
		$iOldDepthLevel = 0;
		$rSections = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "SECTION_ID", "DEPTH_LEVEL", "NAME"));
		while ($arSection = $rSections->Fetch())
		{
			if ($iOldDepthLevel >= $arSection['DEPTH_LEVEL'])
			{
				$bIsChild = false;
				$iOldDepthLevel = 0;
			}
			if (in_array($arSection['ID'], $filter_category))
			{
				$arSectionsStructure[$arSection['ID']] = $arSection;
				$bIsChild = true;
				if ($iOldDepthLevel <= 0)
				{
					$iOldDepthLevel = $arSection['DEPTH_LEVEL'];
				}
			}
			if ($filter_include_subsection == "Y" && $bIsChild)
			{
				$arSectionsStructure[$arSection['ID']] = $arSection;
			}
		}
		foreach ($arSectionsStructure as $sectionId => $arSection)
		{
			$arSectionParents = array($sectionId);
			$sTmp = $sectionId;
			while ($sTmp > 0)
			{
				$sTmp = $arSectionsStructure[$sTmp]['SECTION_ID'];
				if ($sTmp > 0)
				{
					$arSectionParents[] = $sTmp;
				}
			}
			$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "SECTION_ID" => $sectionId, "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID"));
			while ($arElement = $rElements->Fetch())
			{
				if (!is_array($arElementsLinks[$arElement['ID']]))
				{
					$arElementsLinks[$arElement['ID']] = array();
				}
				$arElementsLinks[$arElement['ID']] = array_merge($arElementsLinks[$arElement['ID']], $arSectionParents);
			}
		}
		if (!empty($arElementsLinks))
		{
			$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $SKU_IBLOCK_ID, "@PROPERTY_CML2_LINK" => array_keys($arElementsLinks)), false, false, array("ID", "PROPERTY_CML2_LINK"));
			while ($arElement = $rElements->Fetch())
			{
				if (isset($arElementsLinks[$arElement['PROPERTY_CML2_LINK_VALUE']]))
				{
					$arElementsLinks[$arElement['ID']] = $arElementsLinks[$arElement['PROPERTY_CML2_LINK_VALUE']];
				}
			}
		}
		//myPrint($arElementsLinks);
		//Формируем массив элементов по разделам
		$arWhere = array("1=1");
		$arWhere[] = "`O`.`DATE_INSERT`>='" . date("Y-m-d H:i:s", MakeTimeStamp($arFilter['DATE_FROM'], FORMAT_DATETIME)) . "'";
		$arWhere[] = "`O`.`DATE_INSERT`<='" . date("Y-m-d H:i:s", MakeTimeStamp($arFilter['DATE_TO'], FORMAT_DATETIME)) . "'";
		$arWhere[] = "`O`.`CANCELED`<>'Y'";
		if (!empty($arElementsLinks))
		{
			$arWhere[] = "`B`.`PRODUCT_ID` IN (" . implode(", ", array_keys($arElementsLinks)) . ")";
		}
		if ($arFilter['PERIOD_TYPE'] == "A")
		{
			$strSql = "SELECT `B`.`PRODUCT_ID` AS `PRODUCT_ID`,
				COUNT(B.ID) AS `CNT`, 
				SUM(`B`.`PRICE`*`B`.`QUANTITY`) AS `TOTAL_PRICE`, 
				`B`.`CURRENCY` AS `CURRENCY`,
				SUM(`B`.`QUANTITY`) AS `QUANTITY`, 
				MIN(`O`.`DATE_INSERT`) AS MIN_DATE_INSERT, 
				MAX(`O`.`DATE_INSERT`) AS MAX_DATE_INSERT, 
				'1' AS `PERIOD_IDENT` 
				FROM `b_sale_order` AS `O` LEFT JOIN `b_sale_basket` AS `B` ON `B`.`ORDER_ID`=`O`.`ID`
				WHERE " . implode(" AND ", $arWhere) . " GROUP BY `B`.`PRODUCT_ID`, `B`.`CURRENCY`;";
		} else if ($arFilter['PERIOD_TYPE'] == "W")
		{
			$strSql = "SELECT `B`.`PRODUCT_ID` AS `PRODUCT_ID`,
				COUNT(B.ID) AS `CNT`, 
				SUM(`B`.`PRICE`*`B`.`QUANTITY`) AS `TOTAL_PRICE`, 
				`B`.`CURRENCY` AS `CURRENCY`,
				SUM(`B`.`QUANTITY`) AS `QUANTITY`, 
				MIN(`O`.`DATE_INSERT`) AS MIN_DATE_INSERT, 
				MAX(`O`.`DATE_INSERT`) AS MAX_DATE_INSERT, 
				DATE_FORMAT(`O`.`DATE_INSERT`,'%v-%Y') AS `PERIOD_IDENT`
				FROM `b_sale_order` AS `O` LEFT JOIN `b_sale_basket` AS `B` ON `B`.`ORDER_ID`=`O`.`ID`
				WHERE " . implode(" AND ", $arWhere) . " GROUP BY `B`.`PRODUCT_ID`, `B`.`CURRENCY`, `PERIOD_IDENT`;";
		} else if ($arFilter['PERIOD_TYPE'] == "M")
		{
			$strSql = "SELECT `B`.`PRODUCT_ID` AS `PRODUCT_ID`,
				COUNT(B.ID) AS `CNT`, 
				SUM(`B`.`PRICE`*`B`.`QUANTITY`) AS `TOTAL_PRICE`, 
				`B`.`CURRENCY` AS `CURRENCY`,
				SUM(`B`.`QUANTITY`) AS `QUANTITY`, 
				MIN(`O`.`DATE_INSERT`) AS MIN_DATE_INSERT, 
				MAX(`O`.`DATE_INSERT`) AS MAX_DATE_INSERT, 
				DATE_FORMAT(`O`.`DATE_INSERT`,'%m-%Y') AS `PERIOD_IDENT`
				FROM `b_sale_order` AS `O` LEFT JOIN `b_sale_basket` AS `B` ON `B`.`ORDER_ID`=`O`.`ID`
				WHERE " . implode(" AND ", $arWhere) . " GROUP BY `B`.`PRODUCT_ID`, `B`.`CURRENCY`, `PERIOD_IDENT`;";
		}
		$rDb = $DB->Query($strSql);
		while ($arDb = $rDb->Fetch())
		{
			if (!isset($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START']))
			{
				if ($arFilter['PERIOD_TYPE'] == "A")
				{
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = $arFilter['DATE_FROM'];
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = $arFilter['DATE_TO'];
				} else if ($arFilter['PERIOD_TYPE'] == "W")
				{
					$checkTS = MakeTimeStamp($arDb['MIN_DATE_INSERT'], "YYYY-MM-DD HH:MI:SS");
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = date("d.m.Y", $checkTS - (date("N", $checkTS) - 1) * 3600 * 24);
					if ($arDate = ParseDateTime($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], CSite::GetDateFormat("FULL", SITE_ID)))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"] + 7, $arDate["YYYY"]) - 1);
					}
				} else if ($arFilter['PERIOD_TYPE'] == "M")
				{
					$checkTS = MakeTimeStamp($arDb['MIN_DATE_INSERT'], "YYYY-MM-DD HH:MI:SS");
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = date("01.m.Y", $checkTS);
					if ($arDate = ParseDateTime($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], CSite::GetDateFormat("FULL", SITE_ID)))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"] + 1, $arDate["DD"], $arDate["YYYY"]) - 1);
					}
				}
				if ($arFilter['PERIOD_TYPE'] == "W" || $arFilter['PERIOD_TYPE'] == "M")
				{
					if (MakeTimeStamp($arFilter['DATE_FROM'], FORMAT_DATETIME) >= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], FORMAT_DATETIME) && MakeTimeStamp($arFilter['DATE_FROM'], FORMAT_DATETIME) <= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'], FORMAT_DATETIME))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = $arFilter['DATE_FROM'];
					}
					if (MakeTimeStamp($arFilter['DATE_TO'], FORMAT_DATETIME) >= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], FORMAT_DATETIME) && MakeTimeStamp($arFilter['DATE_TO'], FORMAT_DATETIME) <= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'], FORMAT_DATETIME))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = $arFilter['DATE_TO'];
					}
				}
			}
			foreach ($arElementsLinks[$arDb['PRODUCT_ID']] as $sectionId)
			{
				$arSectionsStructure[$sectionId]['STAT'][$arDb['PERIOD_IDENT']]['ORDERS_CNT'] += $arDb['CNT'];
				$arSectionsStructure[$sectionId]['STAT'][$arDb['PERIOD_IDENT']]['QUANTITY'] += $arDb['QUANTITY'];
				$arSectionsStructure[$sectionId]['STAT'][$arDb['PERIOD_IDENT']]['TOTAL_PRICE'] += $arDb['TOTAL_PRICE'];
			}
		}

		$arDataForChart = array(
			"DATA" => array(),
			"GRAPHS" => array(),
			"FIRST_GRAPHS" => false
		);

		$arTmpData = array(
			"HEADERS" => array(),
			"DATA" => array()
		);
		$arNewData = $arResult['DATA'];
		foreach ($arSectionsStructure as $sectionId => $arSection)
		{
			$arTmpData['DATA'][$sectionId]['CATEGORY'] = str_repeat(".", $arSection["DEPTH_LEVEL"]) . $arSection['NAME'];
			foreach ($arSection['STAT'] as $period => $arStat)
			{
				$arNewData[$period][$sectionId] = array(
					"VALUE" => $arStat['QUANTITY'],
					"DESC" => $arStat['QUANTITY'] . " / " . SaleFormatCurrency($arStat["TOTAL_PRICE"], "RUB")
				);
			}
		}
		$tmpBy = $by;
		$tmpOrder = $order;
		$by = "DATE_PERIOD_START";
		$order = "ASC";
		uasort($arNewData, "bxStatSort");
		$by = $tmpBy;
		$order = $tmpOrder;
		foreach ($arNewData as $period => $arInfo)
		{
			$arTmpData['HEADERS'][] = array("id" => $period, "content" => $arInfo['DATE_PERIOD_START']);
			foreach ($arInfo as $k => $v)
			{
				if (isset($arTmpData['DATA'][$k]))
				{
					$arTmpData['DATA'][$k][$period] = $v;
				}
			}
		}

		foreach ($arTmpData['DATA'] as $sectionId => $arSectionInfo)
		{
			if ($arDataForChart['FIRST_GRAPHS'] === false)
			{
				$arDataForChart['FIRST_GRAPHS'] = "s" . $sectionId;
			}
			$arDataForChart['GRAPHS'][] = array(
				"id" => "s" . $sectionId,
				"title" => $arSectionInfo['CATEGORY'],
				"balloonText" => "[[title]]: [[category]]<br/><b><span style='font-size:14px;'>[[description]]</span></b>",
				"valueField" => "v" . $sectionId,
				"descriptionField" => "d" . $sectionId
			);
			foreach ($arTmpData['HEADERS'] as $ident => $arHeader)
			{
				$arDataForChart['DATA'][$arHeader['id']]['date'] = date("Y-m-d", MakeTimeStamp($arHeader['content'], "DD.MM.YYYY"));
				if (isset($arSectionInfo[$arHeader['id']]))
				{
					$arDataForChart['DATA'][$arHeader['id']]['v' . $sectionId] = intval($arSectionInfo[$arHeader['id']]['VALUE']);
					$arDataForChart['DATA'][$arHeader['id']]['d' . $sectionId] = $arSectionInfo[$arHeader['id']]['DESC'];
				} else
				{
					$arDataForChart['DATA'][$arHeader['id']]['v' . $sectionId] = 0;
					$arDataForChart['DATA'][$arHeader['id']]['d' . $sectionId] = "-";
				}
			}
		}
		$arDataForChart['DATA'] = array_values($arDataForChart['DATA']);

		if ($filter_report_view != "V")
		{
			$arResult['HEADERS'] = array(
				array("id" => "DATE_PERIOD_START", "content" => "Дата начала периода", "sort" => "DATE_PERIOD_START", "default" => true),
				array("id" => "DATE_PERIOD_END", "content" => "Дата окончания периода", "sort" => "DATE_PERIOD_END", "default" => true),
			);
			foreach ($arSectionsStructure as $sectionId => $arSection)
			{
				$arResult['HEADERS'][] = array("id" => "SECTION_" . $sectionId, "content" => str_repeat("&nbsp;.&nbsp;", $arSection["DEPTH_LEVEL"]) . $arSection['NAME'] . " (кол./сумм.)", "default" => true);
				foreach ($arSection['STAT'] as $period => $arStat)
				{
					$arResult['DATA'][$period]["SECTION_" . $sectionId] = $arStat['QUANTITY'] . " / " . SaleFormatCurrency($arStat["TOTAL_PRICE"], "RUB");
				}
			}
		} else
		{
			$arResult['HEADERS'] = array(
				array("id" => "CATEGORY", "content" => "Категория", "default" => true),
			);
			$arNewData = array();
			foreach ($arSectionsStructure as $sectionId => $arSection)
			{
				$arNewData["SECTION_" . $sectionId]['CATEGORY'] = str_repeat("&nbsp;.&nbsp;", $arSection["DEPTH_LEVEL"]) . $arSection['NAME'];
				foreach ($arSection['STAT'] as $period => $arStat)
				{
					$arResult['DATA'][$period]["SECTION_" . $sectionId] = $arStat['QUANTITY'] . " / " . SaleFormatCurrency($arStat["TOTAL_PRICE"], "RUB");
				}
			}
			$by = "DATE_PERIOD_START";
			$order = "DESC";
			uasort($arResult['DATA'], "bxStatSort");

			foreach ($arResult['DATA'] as $period => $arInfo)
			{
				$arResult['HEADERS'][] = array("id" => "PERIOD_" . $period, "content" => $arInfo['DATE_PERIOD_START'] . " - " . $arInfo['DATE_PERIOD_END'], "default" => true);
				foreach ($arInfo as $k => $v)
				{
					if (isset($arNewData[$k]))
					{
						$arNewData[$k]["PERIOD_" . $period] = $v;
					}
				}
			}
			$arResult['DATA'] = $arNewData;
		}
		$arResult['CHART'] = $arDataForChart;
		$obCache->EndDataCache($arResult);
	}
}

// SORT
if ($filter_report_view != "V")
{
	uasort($arResult['DATA'], "bxStatSort");
}

// HEADERS
$lAdmin->AddHeaders($arResult['HEADERS']);
// BODY
$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

$dbRes = new CDBResult;
$dbRes->InitFromArray($arResult['DATA']);

$dbResult = new CAdminResult($dbRes, $sTableID);
$dbResult->NavStart(20);
$lAdmin->NavText($dbResult->GetNavPrint("Заказы"));

while ($arRow = $dbResult->GetNext())
{
	$row = & $lAdmin->AddRow($arRow["ID"], $arRow);
}

$lAdmin->AddFooter(
		array(
			array(
				"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
				"value" => $dbResult->SelectedRowsCount()
			),
			array(
				"counter" => true,
				"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
				"value" => "0"
			),
		)
);
$lAdmin->AddGroupActionTable(array());
$lAdmin->AddAdminContextMenu(array());

$lAdmin->BeginPrologContent();
?>
<style type="text/css">
	.report-category-graph{
		width:100%;
		height:500px;
	}
</style>
<div id="chartdiv" class="report-category-graph"></div>
<script type="text/javascript">
	BX.ready(function () {
		var chart = AmCharts.makeChart("chartdiv", {
			"type": "serial",
			"theme": "light",
			"pathToImages": "/bitrix/js/main/amcharts/3.3/images/",
			"dataProvider": <?= CUtil::PhpToJSObject($arResult['CHART']['DATA']) ?>,
			"categoryField": "date",
			"valueAxes": [{
					"axisAlpha": 0.2,
					"dashLength": 1,
					"position": "left"
				}],
			"categoryAxis": {
				"parseDates": true,
				"axisColor": "#DADADA",
				"dashLength": 1,
				"minorGridEnabled": false,
				"dataDateFormat": "YYYY-MM-DD"
			},
			"graphs": <?= CUtil::PhpToJSObject($arResult['CHART']['GRAPHS']) ?>,
			"marginRight": 0,
			"autoMarginOffset": 20,
			"marginTop": 7,
			"mouseWheelZoomEnabled": true,
			"chartScrollbar": {
				"autoGridCount": true,
				"graph": "<?= $arResult['CHART']['FIRST_GRAPHS'] ?>",
				"scrollbarHeight": 40
			},
			"chartCursor": {},
			"legend": {
				"useGraphSettings": true,
				"position": "right"
			},
		});
	});
</script>
<?
$lAdmin->EndPrologContent();

$lAdmin->CheckListMode();


/* * ************************************************************************* */
/* * *********  MAIN PAGE  *************************************************** */
/* * ************************************************************************* */
$APPLICATION->SetTitle("Продажи по категориям");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?">
	<?
	$oFilter = new CAdminFilter(
			$sTableID . "_filter", array(
		"Период отчета",
		"Категории в отчете *",
		"Детализировать подкатегории",
		"Вид отображения"
			)
	);

	$oFilter->Begin();
	?>
	<tr>
		<td>Тип периода:</td>
		<td>
			<select name="filter_period_type">
				<option value="">(выберите)</option>
				<option value="A"<?
				if ($filter_period_type == "A")
				{
					?> selected="selected"<? } ?>>Общие данные за период</option>
				<option value="W"<?
				if ($filter_period_type == "W")
				{
					?> selected="selected"<? } ?>>Разбивка по неделям</option>
				<option value="M"<?
				if ($filter_period_type == "M")
				{
					?> selected="selected"<? } ?>>Разбивка по месяцам</option>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top">Период отчета:</td>
		<td>
			<? echo CalendarPeriod("filter_date_from", $filter_date_from, "filter_date_to", $filter_date_to, "find_form", "Y") ?>
		</td>
	</tr>
	<tr>
		<td valign="top">Категории в отчете *:</td>
		<td>
			<select name="filter_category[]" multiple="multiple" size="20">
				<option value="-1">Выберите категории</option>
				<?
				$rSections = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));
				while ($arSection = $rSections->GetNext())
				{
					?><option value="<? echo $arSection["ID"] ?>"<? if (in_array($arSection["ID"], $filter_category)) echo " selected" ?>><? echo str_repeat("&nbsp;.&nbsp;", $arSection["DEPTH_LEVEL"]) ?><? echo $arSection["NAME"] ?></option><?
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top">Детализировать подкатегории:</td>
		<td>
			<input type="checkbox" name="filter_include_subsection" value="Y"<? if ($filter_include_subsection == "Y") echo" checked" ?>> 
		</td>
	</tr>
	<tr>
		<td valign="top">Вид отображения:</td>
		<td>
			<select name="filter_report_view">
				<option value="H"<? if ($filter_report_view == "H") echo " selected" ?>>Горизонтальный</option>
				<option value="V"<? if ($filter_report_view == "V") echo " selected" ?>>Вертикальный</option>
			</select>
		</td>
	</tr>
	<?
	$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form"
			)
	);
	$oFilter->End();
	?>
</form>
<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>