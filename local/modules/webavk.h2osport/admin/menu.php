<?

IncludeModuleLangFile(__FILE__);

if ($APPLICATION->GetGroupRight("webavk.h2osport") != "D") {
	$aMenu = array(
		"parent_menu" => "global_menu_store",
		"section" => "webavk.h2osport",
		"sort" => 1000,
		"module_id" => "webavk.h2osport",
		"text" => GetMessage("WEBAVK_H2OSPORT_MENU_MAIN"),
		"title" => GetMessage("WEBAVK_H2OSPORT_MAIN_TITLE"),
		"icon" => "webavk_h2osport_menu_icon",
		"page_icon" => "webavk_h2osport_page_icon",
		"items_id" => "menu_webavk_h2osport",
		"items" => array(
			array(
				"text" => GetMessage("WEBAVK_H2OSPORT_MENU_REPORT_CATEGORY"),
				"title" => GetMessage("WEBAVK_H2OSPORT_MENU_REPORT_CATEGORY_ALT"),
				"url" => "webavk.h2osport.report.category.php",
				"more_url" => Array(),
			),
			/*
			array(
				"text" => GetMessage("WEBAVK_H2OSPORT_MENU_REPORT_CATEGORY_PRODUCT"),
				"title" => GetMessage("WEBAVK_H2OSPORT_MENU_REPORT_CATEGORY_PRODUCT_ALT"),
				"url" => "webavk.h2osport.report.category.product.php",
				"more_url" => Array(),
			)*/
		)
	);
	/*
	if ($APPLICATION->GetGroupRight("webavk.h2osport") >= "W")
	{
		$aMenu['items'] = array_merge($aMenu['items'], array(
			array(
				"text" => GetMessage("WEBAVK_H2OSPORT_MENU_IMPORT_COUPONS"),
				"url" => "webavk.h2osport.import.coupons.php?lang=" . LANGUAGE_ID,
				"title" => GetMessage("WEBAVK_H2OSPORT_MENU_IMPORT_COUPONS_ALT"),
				"more_url" => Array()
			)
		));
	}
	 * 
	 */
	return $aMenu;
}
return false;
