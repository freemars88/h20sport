<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule('webavk.h2osport');

$modulePermissions = $APPLICATION->GetGroupRight("webavk.h2osport");
if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if ($saleModulePermissions == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if (!CModule::IncludeModule('sale'))
{
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(GetMessage("webavk.h2osport_MODULE_SALE_REQUIRED"));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}
$IBLOCK_ID = 16;
$SKU_IBLOCK_ID = 17;

CJSCore::RegisterExt('report_category_chart', array(
	'js' => array(
		'/bitrix/js/main/amcharts/3.3/amcharts.js',
		'/bitrix/js/main/amcharts/3.3/funnel.js',
		'/bitrix/js/main/amcharts/3.3/serial.js',
		'/bitrix/js/main/amcharts/3.3/themes/light.js',
	),
	'rel' => array('ajax', "date")
));
CJSCore::Init(array("report_category_chart"));

$sTableID = "tbl_webavk_h2osport_report_category";

$oSort = new CAdminSorting($sTableID, "DATE_PERIOD_START", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

// FILTER
$arFilterFields = array(
	"filter_period_type",
	"filter_date_from",
	"filter_date_to",
	"filter_category",
	"filter_report_view"
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array();

if (in_array($filter_period_type, array("W", "M", "A")))
{
	$arFilter['PERIOD_TYPE'] = $filter_period_type;
}
if (strlen($filter_date_from) > 0)
{
	$arFilter["DATE_FROM"] = Trim($filter_date_from);
} else
{
	$arFilter["DATE_FROM"] = ConvertTimeStamp(0, "FULL");
}
if (strlen($filter_date_to) > 0)
{
	if ($arDate = ParseDateTime($filter_date_to, CSite::GetDateFormat("FULL", SITE_ID)))
	{
		if (StrLen($filter_date_to) < 11)
		{
			$arDate["HH"] = 23;
			$arDate["MI"] = 59;
			$arDate["SS"] = 59;
		}

		$filter_date_to = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"], $arDate["YYYY"]));
		$arFilter["DATE_TO"] = $filter_date_to;
	} else
	{
		$filter_date_to = "";
	}
}
if (strlen($arFilter["DATE_TO"]) <= 0)
{
	$arFilter["DATE_TO"] = ConvertTimeStamp(time(), "FULL");
}

$arResult = array();
global $by, $order;

function bxStatSort($a, $b)
{
	global $by, $order;
	$by = toUpper($by);
	$order = toUpper($order);
	if (in_array($by, Array("DATE_PERIOD_START", "DATE_PERIOD_END")))
	{
		if (DoubleVal($a[$by]) == DoubleVal($b[$by]))
			return 0;
		elseif (MakeTimeStamp($a[$by], FORMAT_DATETIME) > MakeTimeStamp($b[$by], FORMAT_DATETIME))
			return ($order == "DESC") ? -1 : 1;
		else
			return ($order == "DESC") ? 1 : -1;
	}
}

function bxSortElements($a, $b)
{
	if (strtolower($a) > strtolower($b))
	{
		return 1;
	} else if (strtolower($a) > strtolower($b))
	{
		return -1;
	} else
	{
		return 0;
	}
}

if (strlen($arFilter['PERIOD_TYPE']) > 0 && !empty($filter_category))
{
	$obCache = new CPHPCache();
	if ($obCache->InitCache(1, serialize(array($arFilter, $filter_category, $filter_report_view)) . "_webavk_h2osport_report_category_product", "/webavk_h2osport_report_category_product"))
	{
		$arResult = $obCache->GetVars();
	} elseif ($obCache->StartDataCache())
	{

		// START GET DATA
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("sale");
		CModule::IncludeModule("catalog");

		$obElement = new CIBlockElement();
		$obCatalogSKU = new CCatalogSKU();
		$obBasket = new CSaleBasket();
		$obSale = new CSaleOrder();

		$arSectionsStructure = array();
		$arElementsLinks = array();
		//Создаем структуру разделов
		$bIsChild = false;
		$iOldDepthLevel = 0;
		$rSections = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "SECTION_ID", "DEPTH_LEVEL", "NAME"));
		while ($arSection = $rSections->Fetch())
		{
			if ($iOldDepthLevel >= $arSection['DEPTH_LEVEL'])
			{
				$bIsChild = false;
				$iOldDepthLevel = 0;
			}
			if ($filter_category == $arSection['ID'])
			{
				$arSectionsStructure[$arSection['ID']] = $arSection;
				$bIsChild = true;
				if ($iOldDepthLevel <= 0)
				{
					$iOldDepthLevel = $arSection['DEPTH_LEVEL'];
				}
			}
			if ($bIsChild)
			{
				$arSectionsStructure[$arSection['ID']] = $arSection;
			}
		}
		$arElementsList = array();
		foreach ($arSectionsStructure as $sectionId => $arSection)
		{
			$rElements = CIBlockElement::GetList(array("NAME" => "ASC"), array("IBLOCK_ID" => $IBLOCK_ID, "SECTION_ID" => $sectionId, "INCLUDE_SUBSECTIONS" => "Y"), false, false, array("ID", "NAME", "IBLOCK_ID", "IBLOCK_TYPE_ID", "IBLOCK_SECTION_ID"));
			while ($arElement = $rElements->Fetch())
			{
				$arElement['EDIT_LINK'] = "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . $arElement['IBLOCK_ID'] . "&type=" . $arElement['IBLOCK_TYPE_ID'] . "&ID=" . $arElement['ID'] . "&lang=" . LANG . "&find_section_section=" . $arElement['IBLOCK_SECTION_ID'] . "&WF=Y";
				$arElement['HTML'] = '[<a href="' . $arElement['EDIT_LINK'] . '">' . $arElement['ID'] . '</a>] ' . $arElement['NAME'];
				$arElementsList[$arElement['ID']] = $arElement;
			}
		}
		if (!empty($arElementsList))
		{
			$rElements = CIBlockElement::GetList(array("NAME" => "ASC"), array("IBLOCK_ID" => $SKU_IBLOCK_ID, "PROPERTY_CML2_LINK" => array_keys($arElementsList)), false, false, array("ID", "NAME", "IBLOCK_ID", "IBLOCK_TYPE_ID"));
			while ($arElement = $rElements->Fetch())
			{
				$arElement['EDIT_LINK'] = "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . $arElement['IBLOCK_ID'] . "&type=" . $arElement['IBLOCK_TYPE_ID'] . "&ID=" . $arElement['ID'] . "&lang=" . LANG . "&find_section_section=-1&WF=Y";
				$arElement['HTML'] = '[<a href="' . $arElement['EDIT_LINK'] . '">' . $arElement['ID'] . '</a>] ' . $arElement['NAME'];
				$arElementsList[$arElement['ID']] = $arElement;
			}
		}
		uasort($arElementsList, "bxSortElements");
		//Формируем массив элементов по разделам
		$arWhere = array("1=1");
		$arWhere[] = "`O`.`DATE_INSERT`>='" . date("Y-m-d H:i:s", MakeTimeStamp($arFilter['DATE_FROM'], FORMAT_DATETIME)) . "'";
		$arWhere[] = "`O`.`DATE_INSERT`<='" . date("Y-m-d H:i:s", MakeTimeStamp($arFilter['DATE_TO'], FORMAT_DATETIME)) . "'";
		$arWhere[] = "`O`.`CANCELED`<>'Y'";
		$arWhere[] = "`B`.`PRODUCT_ID` IN (" . implode(", ", array_merge(array(-1), array_keys($arElementsList))) . ")";
		if ($arFilter['PERIOD_TYPE'] == "A")
		{
			$strSql = "SELECT DATE_FORMAT(`O`.`DATE_INSERT`, '%d.%m.%Y') AS `DATE_INSERT`, `O`.`DELIVERY_ID`, `B`.`PRODUCT_ID` AS `PRODUCT_ID`,
		  COUNT(B.ID) AS `CNT`,
		  SUM(`B`.`PRICE`*`B`.`QUANTITY`) AS `TOTAL_PRICE`,
		  `B`.`CURRENCY` AS `CURRENCY`,
		  SUM(`B`.`QUANTITY`) AS `QUANTITY`,
		  MIN(`O`.`DATE_INSERT`) AS MIN_DATE_INSERT,
		  MAX(`O`.`DATE_INSERT`) AS MAX_DATE_INSERT,
		  '1' AS `PERIOD_IDENT`
		  FROM `b_sale_order` AS `O` LEFT JOIN `b_sale_basket` AS `B` ON `B`.`ORDER_ID`=`O`.`ID`
		  WHERE " . implode(" AND ", $arWhere) . " GROUP BY `O`.`DATE_INSERT`, `O`.`DELIVERY_ID`, `B`.`PRODUCT_ID`, `B`.`CURRENCY` ORDER BY DATE_FORMAT(`O`.`DATE_INSERT`, '%d.%m.%Y') ASC;";
		} else if ($arFilter['PERIOD_TYPE'] == "W")
		{
			$strSql = "SELECT DATE_FORMAT(`O`.`DATE_INSERT`, '%d.%m.%Y') AS `DATE_INSERT`, `O`.`DELIVERY_ID`, `B`.`PRODUCT_ID` AS `PRODUCT_ID`,
		  COUNT(B.ID) AS `CNT`,
		  SUM(`B`.`PRICE`*`B`.`QUANTITY`) AS `TOTAL_PRICE`,
		  `B`.`CURRENCY` AS `CURRENCY`,
		  SUM(`B`.`QUANTITY`) AS `QUANTITY`,
		  MIN(`O`.`DATE_INSERT`) AS MIN_DATE_INSERT,
		  MAX(`O`.`DATE_INSERT`) AS MAX_DATE_INSERT,
		  DATE_FORMAT(`O`.`DATE_INSERT`,'%v-%Y') AS `PERIOD_IDENT`
		  FROM `b_sale_order` AS `O` LEFT JOIN `b_sale_basket` AS `B` ON `B`.`ORDER_ID`=`O`.`ID`
		  WHERE " . implode(" AND ", $arWhere) . " GROUP BY `O`.`DATE_INSERT`, `O`.`DELIVERY_ID`,`B`.`PRODUCT_ID`, `B`.`CURRENCY`, `PERIOD_IDENT` ORDER BY DATE_FORMAT(`O`.`DATE_INSERT`, '%d.%m.%Y') ASC;";
		} else if ($arFilter['PERIOD_TYPE'] == "M")
		{
			$strSql = "SELECT DATE_FORMAT(`O`.`DATE_INSERT`, '%d.%m.%Y') AS `DATE_INSERT`, `O`.`DELIVERY_ID`, `B`.`PRODUCT_ID` AS `PRODUCT_ID`,
		  COUNT(B.ID) AS `CNT`,
		  SUM(`B`.`PRICE`*`B`.`QUANTITY`) AS `TOTAL_PRICE`,
		  `B`.`CURRENCY` AS `CURRENCY`,
		  SUM(`B`.`QUANTITY`) AS `QUANTITY`,
		  MIN(`O`.`DATE_INSERT`) AS MIN_DATE_INSERT,
		  MAX(`O`.`DATE_INSERT`) AS MAX_DATE_INSERT,
		  DATE_FORMAT(`O`.`DATE_INSERT`,'%m-%Y') AS `PERIOD_IDENT`
		  FROM `b_sale_order` AS `O` LEFT JOIN `b_sale_basket` AS `B` ON `B`.`ORDER_ID`=`O`.`ID`
		  WHERE " . implode(" AND ", $arWhere) . " GROUP BY `O`.`DATE_INSERT`, `O`.`DELIVERY_ID`,`B`.`PRODUCT_ID`, `B`.`CURRENCY`, `PERIOD_IDENT` ORDER BY DATE_FORMAT(`O`.`DATE_INSERT`, '%d.%m.%Y') ASC;";
		}

		$rDb = $DB->Query($strSql);
		while ($arDb = $rDb->Fetch())
		{
			if (!isset($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START']))
			{
				if ($arFilter['PERIOD_TYPE'] == "A")
				{
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = $arFilter['DATE_FROM'];
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = $arFilter['DATE_TO'];
				} else if ($arFilter['PERIOD_TYPE'] == "W")
				{
					$checkTS = MakeTimeStamp($arDb['MIN_DATE_INSERT'], "YYYY-MM-DD HH:MI:SS");
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = date("d.m.Y", $checkTS - (date("N", $checkTS) - 1) * 3600 * 24);
					if ($arDate = ParseDateTime($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], CSite::GetDateFormat("FULL", SITE_ID)))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"] + 7, $arDate["YYYY"]) - 1);
					}
				} else if ($arFilter['PERIOD_TYPE'] == "M")
				{
					$checkTS = MakeTimeStamp($arDb['MIN_DATE_INSERT'], "YYYY-MM-DD HH:MI:SS");
					$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = date("01.m.Y", $checkTS);
					if ($arDate = ParseDateTime($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], CSite::GetDateFormat("FULL", SITE_ID)))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"] + 1, $arDate["DD"], $arDate["YYYY"]) - 1);
					}
				}
				if ($arFilter['PERIOD_TYPE'] == "W" || $arFilter['PERIOD_TYPE'] == "M")
				{
					if (MakeTimeStamp($arFilter['DATE_FROM'], FORMAT_DATETIME) >= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], FORMAT_DATETIME) && MakeTimeStamp($arFilter['DATE_FROM'], FORMAT_DATETIME) <= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'], FORMAT_DATETIME))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'] = $arFilter['DATE_FROM'];
					}
					if (MakeTimeStamp($arFilter['DATE_TO'], FORMAT_DATETIME) >= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_START'], FORMAT_DATETIME) && MakeTimeStamp($arFilter['DATE_TO'], FORMAT_DATETIME) <= MakeTimeStamp($arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'], FORMAT_DATETIME))
					{
						$arResult['DATA'][$arDb['PERIOD_IDENT']]['DATE_PERIOD_END'] = $arFilter['DATE_TO'];
					}
				}
			}
			$arElementsList[$arDb['PRODUCT_ID']]['STAT'][$arDb['PERIOD_IDENT']][$arDb['DATE_INSERT']][$arDb['DELIVERY_ID']]['ORDERS_CNT'] += $arDb['CNT'];
			$arElementsList[$arDb['PRODUCT_ID']]['STAT'][$arDb['PERIOD_IDENT']][$arDb['DATE_INSERT']][$arDb['DELIVERY_ID']]['QUANTITY'] += $arDb['QUANTITY'];
			$arElementsList[$arDb['PRODUCT_ID']]['STAT'][$arDb['PERIOD_IDENT']][$arDb['DATE_INSERT']][$arDb['DELIVERY_ID']]['TOTAL_PRICE'] += $arDb['TOTAL_PRICE'];
		}
		foreach ($arElementsList as $k => $v)
		{
			if (!isset($v['STAT']))
			{
				unset($arElementsList[$k]);
			}
		}

		$arTmpData = array(
			"HEADERS" => array(),
			"DATA" => array()
		);
		$arNewData = $arResult['DATA'];
		foreach ($arElementsList as $elementId => $arElement)
		{
			$arTmpData['DATA'][$elementId]['ELEMENT'] = $arElement['HTML'];
			foreach ($arElement['STAT'] as $period => $arStat)
			{
				$arNewData[$period][$elementId] = array(
					"VALUE" => $arStat['QUANTITY'],
					"DESC" => $arStat['QUANTITY'] . " / " . SaleFormatCurrency($arStat["TOTAL_PRICE"], "RUB")
				);
			}
		}
		$tmpBy = $by;
		$tmpOrder = $order;
		$by = "DATE_PERIOD_START";
		$order = "ASC";
		uasort($arNewData, "bxStatSort");
		$by = $tmpBy;
		$order = $tmpOrder;
		foreach ($arNewData as $period => $arInfo)
		{
			$arTmpData['HEADERS'][] = array("id" => $period, "content" => $arInfo['DATE_PERIOD_START']);
			foreach ($arInfo as $k => $v)
			{
				if (isset($arTmpData['DATA'][$k]))
				{
					$arTmpData['DATA'][$k][$period] = $v;
				}
			}
		}
		$rDeliveryList = CSaleDelivery::GetList();
		while ($arDeliveryList = $rDeliveryList->Fetch())
		{
			$arResult['DELIEVRY_LIST'][$arDeliveryList['ID']] = $arDeliveryList['NAME'];
		}
		$rDeliveryList = CSaleDeliveryHandler::GetList();
		while ($arDeliveryList = $rDeliveryList->Fetch())
		{
			$ar = Bitrix\Sale\Internals\DeliveryHandlerTable::getList(array("filter" => array("SID" => $arDeliveryList['SID'])));
			myPrint($ar);
			foreach ($arDeliveryList['PROFILES'] as $id => $arProfile)
			{
				$arResult['DELIEVRY_LIST'][$arDeliveryList['SID'] . ":" . strval($id)] = $arProfile['TITLE'];
			}
		}
		if ($filter_report_view != "V")
		{
			$arResult['HEADERS'] = array(
				array("id" => "DATE_PERIOD_START", "content" => "Дата начала периода", "sort" => "DATE_PERIOD_START", "default" => true),
				array("id" => "DATE_PERIOD_END", "content" => "Дата окончания периода", "sort" => "DATE_PERIOD_END", "default" => true),
			);
			foreach ($arElementsList as $elementId => $arElement)
			{
				$arResult['HEADERS'][] = array("id" => "ELEMENT_" . $elementId, "content" => $arElement['HTML'] . " (кол./сумм.)", "default" => true);
				foreach ($arElement['STAT'] as $period => $arStat)
				{
					$arCellData = array();
					foreach ($arStat as $kDateOrder => $arDataOrders)
					{
						foreach ($arDataOrders as $kDelivery => $arDelivery)
						{
							$arCellData[] = $kDateOrder . " - " . $arResult['DELIEVRY_LIST'][$kDelivery] . ": " . $arDelivery['ORDERS_CNT'] . " / " . $arDelivery['QUANTITY'] . " / " . SaleFormatCurrency($arDelivery["TOTAL_PRICE"], "RUB");
						}
					}
					$arResult['DATA'][$period]["ELEMENT_" . $elementId] = implode("<br/>", $arCellData);
				}
			}
		} else
		{
			$arResult['HEADERS'] = array(
				array("id" => "ELEMENT", "content" => "Товар", "default" => true),
			);
			$arNewData = array();
			foreach ($arElementsList as $elementId => $arElement)
			{
				$arNewData["ELEMENT_" . $elementId]['ELEMENT'] = $arElement['HTML'];
				foreach ($arElement['STAT'] as $period => $arStat)
				{
					$arCellData = array();
					foreach ($arStat as $kDateOrder => $arDataOrders)
					{
						foreach ($arDataOrders as $kDelivery => $arDelivery)
						{
							$arCellData[] = $kDateOrder . " - " . $arResult['DELIEVRY_LIST'][$kDelivery] . ": " . $arDelivery['ORDERS_CNT'] . " / " . $arDelivery['QUANTITY'] . " / " . SaleFormatCurrency($arDelivery["TOTAL_PRICE"], "RUB");
						}
					}
					$arResult['DATA'][$period]["ELEMENT_" . $elementId] = implode("<br/>", $arCellData);
				}
			}
			$by = "DATE_PERIOD_START";
			$order = "DESC";
			uasort($arResult['DATA'], "bxStatSort");

			foreach ($arResult['DATA'] as $period => $arInfo)
			{
				$arResult['HEADERS'][] = array("id" => "PERIOD_" . $period, "content" => $arInfo['DATE_PERIOD_START'] . " - " . $arInfo['DATE_PERIOD_END'], "default" => true);
				foreach ($arInfo as $k => $v)
				{
					if (isset($arNewData[$k]))
					{
						$arNewData[$k]["PERIOD_" . $period] = $v;
					}
				}
			}
			$arResult['DATA'] = $arNewData;
		}
		$obCache->EndDataCache($arResult);
	}
}

// SORT
if ($filter_report_view != "V")
{
	uasort($arResult['DATA'], "bxStatSort");
}

// HEADERS
$lAdmin->AddHeaders($arResult['HEADERS']);
// BODY
$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();

$dbRes = new CDBResult;
$dbRes->InitFromArray($arResult['DATA']);

$dbResult = new CAdminResult($dbRes, $sTableID);
$dbResult->NavStart(20);
$lAdmin->NavText($dbResult->GetNavPrint("Заказы"));

while ($arRow = $dbResult->GetNext())
{
	$row = & $lAdmin->AddRow($arRow["ID"], $arRow);
	if ($filter_report_view == "V")
	{
		foreach ($arRow as $k => $v)
		{
			if ($k == "ELEMENT")
			{
				$row->AddViewField("ELEMENT", $arRow['~ELEMENT']);
			} elseif (strpos($k, "PERIOD_") === 0)
			{
				$row->AddViewField($k, $arRow['~' . $k]);
			}
		}
	}
}

$lAdmin->AddFooter(
		array(
			array(
				"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
				"value" => $dbResult->SelectedRowsCount()
			),
			array(
				"counter" => true,
				"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
				"value" => "0"
			),
		)
);
$lAdmin->AddGroupActionTable(array());
$lAdmin->AddAdminContextMenu(array());

$lAdmin->BeginPrologContent();

$lAdmin->EndPrologContent();

$lAdmin->CheckListMode();


/* * ************************************************************************* */
/* * *********  MAIN PAGE  *************************************************** */
/* * ************************************************************************* */
$APPLICATION->SetTitle("Продажи по товарам в категории");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?">
	<?
	$oFilter = new CAdminFilter(
			$sTableID . "_filter", array(
		"Период отчета",
		"Категория в отчете *",
		"Вид отображения"
			)
	);

	$oFilter->Begin();
	?>
	<tr>
		<td>Тип периода:</td>
		<td>
			<select name="filter_period_type">
				<option value="">(выберите)</option>
				<option value="A"<?
				if ($filter_period_type == "A")
				{
					?> selected="selected"<? } ?>>Общие данные за период</option>
				<option value="W"<?
				if ($filter_period_type == "W")
				{
					?> selected="selected"<? } ?>>Разбивка по неделям</option>
				<option value="M"<?
				if ($filter_period_type == "M")
				{
					?> selected="selected"<? } ?>>Разбивка по месяцам</option>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top">Период отчета:</td>
		<td>
			<? echo CalendarPeriod("filter_date_from", $filter_date_from, "filter_date_to", $filter_date_to, "find_form", "Y") ?>
		</td>
	</tr>
	<tr>
		<td valign="top">Категория в отчете *:</td>
		<td>
			<select name="filter_category">
				<option value="-1">Выберите категорию</option>
				<?
				$rSections = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));
				while ($arSection = $rSections->GetNext())
				{
					?><option value="<? echo $arSection["ID"] ?>"<? if ($filter_category == $arSection["ID"]) echo " selected" ?>><? echo str_repeat("&nbsp;.&nbsp;", $arSection["DEPTH_LEVEL"]) ?><? echo $arSection["NAME"] ?></option><?
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top">Вид отображения:</td>
		<td>
			<select name="filter_report_view">
				<option value="V"<? if ($filter_report_view == "V") echo " selected" ?>>Вертикальный</option>
				<option value="H"<? if ($filter_report_view == "H") echo " selected" ?>>Горизонтальный</option>
			</select>
		</td>
	</tr>
	<?
	$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form"
			)
	);
	$oFilter->End();
	?>
</form>
<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>