<?

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

\CModule::IncludeModule("iblock");

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Main;

class CWebavkH2OSport
{

	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		if ($GLOBALS['APPLICATION']->GetGroupRight("main") < "R")
			return;

		$MODULE_ID = basename(dirname(__FILE__));
		$aMenu = array(
			//"parent_menu" => "global_menu_services",
			"parent_menu" => "global_menu_settings",
			"section" => $MODULE_ID,
			"sort" => 50,
			"text" => $MODULE_ID,
			"title" => '',
//			"url" => "partner_modules.php?module=".$MODULE_ID,
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID . "_items",
			"more_url" => array(),
			"items" => array()
		);

		if (file_exists($path = dirname(__FILE__) . '/admin'))
		{
			if ($dir = opendir($path))
			{
				$arFiles = array();

				while (false !== $item = readdir($dir))
				{
					if (in_array($item, array('.', '..', 'menu.php')))
						continue;

					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/local/admin/' . $MODULE_ID . '_' . $item))
						file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/local/modules/' . $MODULE_ID . '/admin/' . $item . '");?' . '>');

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach ($arFiles as $item)
					$aMenu['items'][] = array(
						'text' => $item,
						'url' => $MODULE_ID . '_' . $item,
						'module_id' => $MODULE_ID,
						"title" => "",
					);
			}
		}
		$aModuleMenu[] = $aMenu;
	}
/*
	function GetFilterData($arFilterData, $iSectionID, $bMakeIfNotExists = false)
	{
		ksort($arFilterData);

		//Шаг 1 - ищем есть ли привязанный раздел
		if (!empty($arFilterData) && $iSectionID > 0)
		{
			$SECTION_ID = false;
			$rSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_DATA, "UF_CATALOG_SECTION" => $iSectionID));
			if ($arSection = $rSection->Fetch())
			{
				$SECTION_ID = $arSection['ID'];
			} else
			{
				if ($bMakeIfNotExists)
				{
					$SECTION_ID = self::MakeSectionFilterData($iSectionID);
				}
			}
			if ($SECTION_ID > 0)
			{
				$ELEMENT_ID = false;
				$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_DATA, "ACTIVE" => "Y", "SECTION_ID" => $SECTION_ID), false, false, array("ID", "NAME", "PROPERTY_FILTER_DATA", "PROPERTY_FULL_MATCH"));
				while ($arElement = $rElements->Fetch())
				{
					$arElement['PROPERTY_FILTER_DATA_VALUE'] = unserialize($arElement['PROPERTY_FILTER_DATA_VALUE']);
					if ($arElement['PROPERTY_FULL_MATCH_VALUE'] == "Y")
					{
						if (self::doCompareArrayFull($arElement['PROPERTY_FILTER_DATA_VALUE'], $arFilterData) === true)
						{
							$ELEMENT_ID = $arElement['ID'];
							break;
						}
					} else
					{
						if (self::doCompareArrayPartial($arElement['PROPERTY_FILTER_DATA_VALUE'], $arFilterData) === true)
						{
							$ELEMENT_ID = $arElement['ID'];
							break;
						}
					}
				}

				if (!$ELEMENT_ID && $bMakeIfNotExists)
				{
					$obElement = new CIBlockElement();
					$ELEMENT_ID = $obElement->Add(array(
						"IBLOCK_ID" => IBLOCK_ID_FILTER_DATA,
						"ACTIVE" => "Y",
						"NAME" => "filter_item",
						"IBLOCK_SECTION_ID" => $SECTION_ID,
						"PROPERTY_VALUES" => array(
							"FILTER_DATA" => serialize($arFilterData),
							"FULL_MATCH" => "Y"
						)
					));
				}
				if ($ELEMENT_ID > 0)
				{
					$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_DATA, "ACTIVE" => "Y", "ID" => $ELEMENT_ID), false, false, array("ID", "NAME", "PROPERTY_FILTER_DATA", "PROPERTY_FULL_MATCH", "PROPERTY_TITLE_PAGE", "PROPERTY_H1_TITLE", "PROPERTY_META_KEYWORDS", "PROPERTY_META_DESCRIPTION", "DETAIL_TEXT", "DETAIL_TEXT_TYPE", "IBLOCK_SECTION_ID"));
					if ($arElements = $rElements->Fetch())
					{
						return $arElements;
					}
				}
			}
		}
		return false;
	}

	function MakeSectionFilterData($iSectionID)
	{
		$obSection = new CIBlockSection();
		$SECTION_ID = false;
		if ($iSectionID > 0)
		{
			$rNavChain = CIBlockSection::GetNavChain(IBLOCK_CATALOG, $iSectionID);
			while ($arNavChain = $rNavChain->Fetch())
			{
				$rSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_DATA, "UF_CATALOG_SECTION" => $arNavChain['ID']));
				if ($arSection = $rSection->Fetch())
				{
					$SECTION_ID = $arSection['ID'];
				} else
				{
					$SECTION_ID = $obSection->Add(array(
						"IBLOCK_ID" => IBLOCK_ID_FILTER_DATA,
						"NAME" => $arNavChain['NAME'],
						"SORT" => $arNavChain['SORT'],
						"IBLOCK_SECTION_ID" => $SECTION_ID,
						"ACTIVE" => "Y",
						"UF_CATALOG_SECTION" => $arNavChain['ID']
					));
				}
			}
		}
		return $SECTION_ID;
	}

	function doCompareArrayFull($ar1, $ar2, $bAssoc = true)
	{
		foreach ($ar1 as $k => $v)
		{
			if ($bAssoc)
			{
				if (!isset($ar2[$k]))
				{
					return false;
				}
				if (is_array($v))
				{
					if (!self::doCompareArrayFull($v, $ar2[$k], false))
					{
						return false;
					}
				} else
				{
					if ($v !== $ar2[$k])
					{
						return false;
					}
				}
				unset($ar1[$k]);
				unset($ar2[$k]);
			} else
			{
				if (!in_array($v, $ar2))
				{
					return false;
				}
				$ar2Key = array_search($v, $ar2);
				if (is_array($v))
				{
					if (!self::doCompareArrayFull($v, $ar2[$ar2Key], false))
					{
						return false;
					}
				} else
				{
					if ($v !== $ar2[$ar2Key])
					{
						return false;
					}
				}
				unset($ar1[$k]);
				unset($ar2[$ar2Key]);
			}
		}
		if (!empty($ar2))
		{
			return false;
		}
		return true;
	}

	function doCompareArrayPartial($arMinimal1, $ar2, $bAssoc = true)
	{
		foreach ($arMinimal1 as $k => $v)
		{
			if ($bAssoc)
			{
				if (!isset($ar2[$k]))
				{
					return false;
				}
				if (is_array($v))
				{
					if (!self::doCompareArrayPartial($v, $ar2[$k], false))
					{
						return false;
					}
				} else
				{
					if ($v !== $ar2[$k])
					{
						return false;
					}
				}
				unset($ar1[$k]);
				unset($ar2[$k]);
			} else
			{
				if (!in_array($v, $ar2))
				{
					return false;
				}
				$ar2Key = array_search($v, $ar2);
				if (is_array($v))
				{
					if (!self::doCompareArrayPartial($v, $ar2[$ar2Key], false))
					{
						return false;
					}
				} else
				{
					if ($v !== $ar2[$ar2Key])
					{
						return false;
					}
				}
				unset($ar1[$k]);
				unset($ar2[$ar2Key]);
			}
		}
		return true;
	}

	function GetFilterUrl($strUrl, $iSectionID, $bMakeIfNotExists = false)
	{
		//Шаг 1 - ищем есть ли привязанный раздел
		if (strlen($strUrl) > 0 && $iSectionID > 0)
		{
			$SECTION_ID = false;
			$rSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_URL, "UF_CATALOG_SECTION" => $iSectionID));
			if ($arSection = $rSection->Fetch())
			{
				$SECTION_ID = $arSection['ID'];
			} else
			{
				if ($bMakeIfNotExists)
				{
					$SECTION_ID = self::MakeSectionFilterUrl($iSectionID);
				}
			}
			if ($SECTION_ID > 0)
			{
				$ELEMENT_ID = false;
				$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_URL, "ACTIVE" => "Y", "SECTION_ID" => $SECTION_ID), false, false, array("ID", "NAME", "PROPERTY_URL_ORIGINAL", "PROPERTY_URL_SITE"));
				while ($arElement = $rElements->Fetch())
				{
					if ($arElement['PROPERTY_URL_ORIGINAL_VALUE'] == $strUrl)
					{
						$ELEMENT_ID = $arElement['ID'];
						break;
					}
				}

				if (!$ELEMENT_ID && $bMakeIfNotExists)
				{
					$obElement = new CIBlockElement();
					$ELEMENT_ID = $obElement->Add(array(
						"IBLOCK_ID" => IBLOCK_ID_FILTER_URL,
						"ACTIVE" => "Y",
						"NAME" => "url_item",
						"IBLOCK_SECTION_ID" => $SECTION_ID,
						"PROPERTY_VALUES" => array(
							"URL_ORIGINAL" => $strUrl
						)
					));
				}
				if ($ELEMENT_ID > 0)
				{
					$rElements = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_URL, "ACTIVE" => "Y", "ID" => $ELEMENT_ID), false, false, array("ID", "NAME", "PROPERTY_URL_ORIGINAL", "PROPERTY_URL_SITE"));
					if ($arElements = $rElements->Fetch())
					{
						return $arElements;
					}
				}
			}
		}
		return false;
	}

	function MakeSectionFilterUrl($iSectionID)
	{
		$obSection = new CIBlockSection();
		$SECTION_ID = false;
		if ($iSectionID > 0)
		{
			$rNavChain = CIBlockSection::GetNavChain(IBLOCK_CATALOG, $iSectionID);
			while ($arNavChain = $rNavChain->Fetch())
			{
				$rSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => IBLOCK_ID_FILTER_URL, "UF_CATALOG_SECTION" => $arNavChain['ID']));
				if ($arSection = $rSection->Fetch())
				{
					$SECTION_ID = $arSection['ID'];
				} else
				{
					$SECTION_ID = $obSection->Add(array(
						"IBLOCK_ID" => IBLOCK_ID_FILTER_URL,
						"NAME" => $arNavChain['NAME'],
						"SORT" => $arNavChain['SORT'],
						"IBLOCK_SECTION_ID" => $SECTION_ID,
						"ACTIVE" => "Y",
						"UF_CATALOG_SECTION" => $arNavChain['ID']
					));
				}
			}
		}
		return $SECTION_ID;
	}

	function DoAdminContextMenuShow(&$items)
	{
		global $APPLICATION, $USER;
		if (($APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_detail.php' || $APPLICATION->GetCurPage() == '/bitrix/admin/sale_order_view.php') && $_REQUEST['ID'] > 0)
		{
			$orderId = intval($_REQUEST['ID']);
			$arOrder = CSaleOrder::GetById($orderId);
			/*
			if ($arOrder['PAY_SYSTEM_ID'] == PAYSYSTEM_SBERBANK_ID && $arOrder['PS_STATUS'] == "Y")
			{
				$rData = WebAVK\KrasniyKarandash\RbsPayTable::GetList(array(
							"order" => array(),
							"filter" => array("ORDER_ID" => $arOrder['ID'])
				));
				if ($arData = $rData->fetch())
				{
					$modRight = doMakeUserRightForRbs(); //$APPLICATION->GetGroupRight("webavk.krasniykarandash",false,"N","N");
					if (date("d.m.Y", $arData['DATE_SEND']->getTimestamp()) == date("d.m.Y", time()) && empty($arData['DATE_SEND_REFUND_REQUEST']) && ($modRight == "T" || $modRight == "U"))
					{
						$items[] = array(
							'TEXT' => GetMessage("webavk.krasniykarandash_ORDER_RBS_CANCEL_REQUEST"),
							'TITLE' => GetMessage('webavk.krasniykarandash_ORDER_RBS_CANCEL_REQUEST_TITLE'),
							'LINK' => '/bitrix/admin/webavk.krasniykarandash.rbs.cancel.request.php?ID=' . $orderId,
							'ICON' => 'btn_rbs_cancel_request',
							'LINK_PARAM' => 'target="_blank"'
						);
					} else
					{
						$items[] = array(
							'TEXT' => GetMessage("webavk.krasniykarandash_ORDER_RBS_REFUND_REQUEST"),
							'TITLE' => GetMessage('webavk.krasniykarandash_ORDER_RBS_REFUND_REQUEST_TITLE'),
							'LINK' => '/bitrix/admin/webavk.krasniykarandash.rbs.refund.request.php?ID=' . $orderId,
							'ICON' => 'btn_rbs_refund_request',
							'LINK_PARAM' => 'target="_blank"'
						);
					}
				}
			}*//*
			if ($arOrder['PAY_SYSTEM_ID'] == PAYSYSTEM_SBERBANK_PRE_ID && $arOrder['PAYED'] != "Y")
			{
				$items[] = array(
					'TEXT' => GetMessage("webavk.h2osport_ORDER_SBERBANK_MAKE_PAY"),
					'TITLE' => GetMessage('webavk.krasniykarandash_ORDER_SBERBANK_MAKE_PAY_TITLE'),
					'LINK' => '/bitrix/admin/webavk.h2osport.sberbank.make.pay.php?ID=' . $orderId,
					'ICON' => 'btn_sberbank_make_pay',
					'LINK_PARAM' => 'target="_blank"'
				);
			}
		}
	}
	*/

}

CModule::AddAutoloadClasses(
		"webavk.h2osport", array(
	"H2O_IBProp_ElemListDescr" => "classes/general/ibprop_elemlistdescr.php",
	"WebAVK\\H2OSport\\UserTable" => "lib/user.php",
	"WebAVK\\H2OSport\\WishlistTable" => "lib/wishlist.php",
		)
);
