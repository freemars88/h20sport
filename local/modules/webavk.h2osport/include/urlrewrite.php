<?

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/filter.url.php"))
{
	$arRules = include($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/filter.url.php");
	foreach ($arRules as $k => $v)
	{
		if (strpos($_SERVER["REQUEST_URI"], $k) === 0)
		{
			$_SERVER["REQUEST_URI"] = $v . substr($_SERVER["REQUEST_URI"], strlen($k));
			$_SERVER['IS_FILTER_URL'] = true;
			break;
		}
	}
}
