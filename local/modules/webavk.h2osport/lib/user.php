<?php

namespace WebAVK\H2OSport;

class UserTable extends \Bitrix\Main\Entity\DataManager
{

	public static function getTableName()
	{
		return 'wahs_user';
	}

	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getMap()
	{
		$fieldsMap = array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true
			),
			"DATE_INSERT" => array(
				"data_type" => "datetime"
			),
			"DATE_UPDATE" => array(
				"data_type" => "datetime"
			),
			"USER_ID" => array(
				"data_type" => "integer",
			),
			"CODE" => array(
				"data_type" => 'string'
			),
			"OLD_IP" => array(
				"data_type" => 'string'
			),
		);
		return $fieldsMap;
	}

	function InitUser()
	{
		global $USER, $APPLICATION;
		$bUpdateProducts = false;
		//unset($_SESSION['WISHLIST_UID']);
		//	unset($_SESSION['WISHLIST_CUID']);
		if ($_SESSION['WISHLIST_OLD_AUTH'] != $USER->IsAuthorized())
		{
			unset($_SESSION['WISHLIST_UID']);
			unset($_SESSION['WISHLIST_CUID']);
		}
		$_SESSION['WISHLIST_OLD_AUTH'] = $USER->IsAuthorized();
		if (!isset($_SESSION['WISHLIST_UID']))
		{
			$bUpdateProducts = true;
			if ($USER->IsAuthorized())
			{
				$rUsers = UserTable::getList(array(
							"filter" => array("USER_ID" => $USER->GetID())
				));
				while ($arUser = $rUsers->fetch())
				{
					if (!in_array($arUser['ID'], $_SESSION['WISHLIST_UID']))
					{
						$_SESSION['WISHLIST_UID'][] = $arUser['ID'];
					}
					UserTable::update($arUser['ID'], array(
						"DATE_UPDATE" => new \Bitrix\Main\Type\DateTime(),
						"OLD_IP" => $_SERVER['REMOTE_ADDR']
					));
				}
			}
			$code = $APPLICATION->get_cookie("wlui");
			if (strlen($code) > 0)
			{
				$rUsers = UserTable::getList(array(
							"filter" => array("CODE" => $code)
				));
				while ($arUser = $rUsers->fetch())
				{
					if (!in_array($arUser['ID'], $_SESSION['WISHLIST_UID']))
					{
						$_SESSION['WISHLIST_UID'][] = $arUser['ID'];
					}
					if ($USER->IsAuthorized() && $arUser['USER_ID'] <= 0)
					{
						UserTable::update($arUser['ID'], array(
							"DATE_UPDATE" => new \Bitrix\Main\Type\DateTime(),
							"OLD_IP" => $_SERVER['REMOTE_ADDR'],
							"USER_ID" => $USER->GetID()
						));
					} else
					{
						UserTable::update($arUser['ID'], array(
							"DATE_UPDATE" => new \Bitrix\Main\Type\DateTime(),
							"OLD_IP" => $_SERVER['REMOTE_ADDR']
						));
					}
				}
			}
			unset($_SESSION['WISHLIST_CUID']);
		}
		if (!isset($_SESSION['WISHLIST_UID']))
		{
			$bUpdateProducts = true;
			$code = md5(microtime() . randString(20));
			$complete = false;
			while (!$complete)
			{
				$rUsers = UserTable::getList(array(
							"filter" => array("CODE" => $code)
				));
				if ($arUser = $rUsers->fetch())
				{
					$code = md5(microtime() . randString(20));
				} else
				{
					$complete = true;
				}
			}
			$oRes = UserTable::add(array(
						"DATE_INSERT" => new \Bitrix\Main\Type\DateTime(),
						"DATE_UPDATE" => new \Bitrix\Main\Type\DateTime(),
						"USER_ID" => $USER->GetID(),
						"CODE" => $code,
						"OLD_IP" => $_SERVER['REMOTE_ADDR']
			));
			if ($oRes->isSuccess())
			{
				$_SESSION['WISHLIST_UID'][] = $oRes->getId();
				$APPLICATION->set_cookie("wlui", $code, time() + 3600 * 24 * 365);
			}
			unset($_SESSION['WISHLIST_CUID']);
		}
		if (!isset($_SESSION['WISHLIST_CUID']))
		{
			$code = $APPLICATION->get_cookie("wlui");
			if (strlen($code) > 0)
			{
				$rUsers = UserTable::getList(array(
							"filter" => array("CODE" => $code)
				));
				while ($arUser = $rUsers->fetch())
				{
					$_SESSION['WISHLIST_CUID'] = $arUser['ID'];
				}
			}
		}
		if ($bUpdateProducts)
		{
			$GLOBALS['NOCALL_WL'] = true;
			UserTable::UpdateCurrentUserProducts();
			$GLOBALS['NOCALL_WL'] = false;
		}
		return $_SESSION['WISHLIST_UID'];
	}

	function UpdateCurrentUserProducts()
	{
		unset($_SESSION['WISHLIST_PID']);
		if (empty($_SESSION['WISHLIST_UID']))
		{
			if (!$GLOBALS['NOCALL_WL'])
			{
				UserTable::InitUser();
			}
		}
		if (!empty($_SESSION['WISHLIST_UID']))
		{
			$rProducts = WishlistTable::getList(array(
						"order" => array("ID" => "DESC"),
						"filter" => array(
							"KUSER_ID" => $_SESSION['WISHLIST_UID']
						)
			));
			while ($arProducts = $rProducts->fetch())
			{
				$_SESSION['WISHLIST_PID'][] = $arProducts['ELEMENT_ID'];
			}
		}
		return $_SESSION['WISHLIST_PID'];
	}

	function GetCurrentUserProducts($bUpdate = false)
	{
		if ($bUpdate)
		{
			UserTable::UpdateCurrentUserProducts();
		}
		return $_SESSION['WISHLIST_PID'];
	}

	function GetCurrentUserProductsCnt($bUpdate = false)
	{
		if ($bUpdate)
		{
			UserTable::UpdateCurrentUserProducts();
		}
		return count($_SESSION['WISHLIST_PID']);
	}

	function AddToWishList($iElementId)
	{
		$bResult = false;
		UserTable::InitUser();
		if ($_SESSION['WISHLIST_CUID'] > 0)
		{
			if (in_array($iElementId, $_SESSION['WISHLIST_PID']))
			{
				$bResult = true;
			} else
			{
				$oRes = WishlistTable::add(array(
							"KUSER_ID" => $_SESSION['WISHLIST_CUID'],
							"DATE_INSERT" => new \Bitrix\Main\Type\DateTime(),
							"ELEMENT_ID" => $iElementId
				));
				if ($oRes->isSuccess())
				{
					$bResult = true;
				}
			}
			unset($_SESSION['WISHLIST_UID']);
			unset($_SESSION['WISHLIST_CUID']);
			UserTable::InitUser();
		}
		return $bResult;
	}

	function DeleteFromWishList($iElementId)
	{
		$bResult = false;
		if ($_SESSION['WISHLIST_CUID'] > 0)
		{
			if (!in_array($iElementId, $_SESSION['WISHLIST_PID']))
			{
				$bResult = true;
			} else
			{
				$oRes = WishlistTable::getList(array(
							"filter" => array(
								"KUSER_ID" => $_SESSION['WISHLIST_CUID'],
								"ELEMENT_ID" => $iElementId
							)
				));
				if ($arRes = $oRes->fetch())
				{
					$oRes = WishlistTable::delete($arRes['ID']);
					if ($oRes->isSuccess())
					{
						$bResult = true;
					}
				}
			}
			unset($_SESSION['WISHLIST_UID']);
			unset($_SESSION['WISHLIST_CUID']);
			UserTable::InitUser();
		}
		return $bResult;
	}

	function GetCurrentUserProductsList($cnt = 0)
	{
		$arResult = array();
		$arProducts = self::GetCurrentUserProducts();
		if (!empty($arProducts))
		{
			foreach ($arProducts as $v)
			{
				$arResult[$v] = $v;
			}
			//Загружаем товары
			$rElements = \CIBlockElement::getList(array(), array("ID" => $arProducts, "ACTIVE" => "Y"));
			while ($rsElement = $rElements->GetNextElement())
			{
				$arFields = $rsElement->GetFields();
				$arFields['PROPERTIES'] = $rsElement->GetProperties();
				$arResult[$arFields['ID']] = $arFields;
			}
			foreach ($arResult as $k => $v)
			{
				if (!is_array($v))
				{
					unset($arResult[$k]);
				}
			}
			if ($cnt > 0)
			{
				$i = 0;
				foreach ($arResult as $k => $v)
				{
					if ($i >= $cnt)
					{
						unset($arResult[$k]);
					}
					$i++;
				}
			}
		}
		return $arResult;
	}

}
