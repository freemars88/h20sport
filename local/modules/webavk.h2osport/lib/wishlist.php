<?php

namespace WebAVK\H2OSport;

class WishlistTable extends \Bitrix\Main\Entity\DataManager
{

	public static function getTableName()
	{
		return 'wahs_wishlist';
	}

	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getMap()
	{
		$fieldsMap = array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true
			),
			"KUSER_ID" => array(
				"data_type" => "integer",
			),
			"DATE_INSERT" => array(
				"data_type" => "datetime"
			),
			"ELEMENT_ID" => array(
				"data_type" => 'integer'
			),
		);
		return $fieldsMap;
	}

}
