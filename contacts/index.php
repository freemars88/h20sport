<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контактная информация магазина H2O. Доставка спортивной экипировки по всей России. Оборудования для водных видов спорта");
$APPLICATION->SetPageProperty("title", "Экипировка для водных видов спорта по всей России. Контакты");
$APPLICATION->SetTitle("Контакты");
?>
	<div class="contacts">
		<div class="row">
			<div class="col-60 col-lg-20">
				<div class="contacts-details">
					<h1>Контакты</h1>
					<ul>
						<li>
							Россия, Москва: ул. Шолохова, 2Ас1
						</li>
						<li><a href="tel:7 (977) 7025217">+7 (977) 702-5217 (будни с
								10 до 18) </a></li>
						<li><a href="mailto: shop@h2osport.ru ">
								shop@h2osport.ru </a></li>
						<li>ИП Зинчин К.А.</li>
						<li>ИНН 772820754186</li>
						<li>ОГРНИП 311774620300293</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-1 d-none d-lg-block">&nbsp;</div>
			<div class="col-60 col-lg-39">
				<div class="map-area">
					<? $APPLICATION->IncludeComponent(
						"bitrix:map.yandex.view",
						"",
						Array(
							"CONTROLS" => array("ZOOM", "MINIMAP", "TYPECONTROL", "SCALELINE"),
							"INIT_MAP_TYPE" => "MAP",
							"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.73829999999371;s:10:\"yandex_lon\";d:37.59459999999997;s:12:\"yandex_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.35674384788513;s:3:\"LAT\";d:55.63572786293272;s:4:\"TEXT\";s:24:\"Шолохова, 2Ас1\";}}}",
							"MAP_HEIGHT" => "600",
							"MAP_ID" => "",
							"MAP_WIDTH" => "100%",
							"OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING"),
						)
					); ?>
				</div>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>