<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
$APPLICATION->AddChainItem("404", "/404.php");
$APPLICATION->SetTitle("Страница не найдена");
?>

<div class="area-404  pt-80 pb-80">
	<div class="container">	
		<div class="row">
			<div class="col-lg-12">
				<div class="error-content text-center">
					<img src="<?= SITE_TEMPLATE_PATH ?>/img/bg/error.png" alt="" />
					<h4 class="text-light-black mt-60">Ooops.... Ошибка 404</h4>
					<h5 class="text-light-black">Извините, но страница, которую вы ищете, не существует</h5>
					<p class="text-uppercase">Или</p>
					<a class="button-one submit-btn-4 go-to-home bg-white text-uppercase text-light-black" href="/" data-text="Перейти на главую страницу">Перейти на главую страницу</a>
				</div>
			</div>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>