<?php
$canonical = [
    "/catalog/vindserfing/zapchasti/adapter/" => "/catalog/vindserfing/adapter/",
    "/catalog/yakhting/vizor/uniseks/" => "/catalog/odezhda/vizor/uniseks/",
    "/catalog/yakhting/gidromayka-avlare/uniseks/" => "/catalog/gidroodezhda/gidromayka/uniseks/",
    "/catalog/yakhting/gidronoski/uniseks/" => "/catalog/gidroodezhda/gidronoski/uniseks/",
    "/catalog/t293/gik/" => "/catalog/vindserfing/gik/",
    "/catalog/kayak/zhilet/uniseks/" => "/catalog/gidroodezhda/zhilet/uniseks/",
    "/catalog/yakhting/zhilet/uniseks/" => "/catalog/gidroodezhda/zhilet/uniseks/",
    "/catalog/t293/zapchasti/" => "/catalog/vindserfing/zapchasti/",
    "/catalog/yakhting/kepka/uniseks/" => "/catalog/odezhda/kepka/uniseks/",
    "/catalog/yakhting/kurtka/zhenskoe/" => "/catalog/odezhda/kurtka/zhenskoe/",
    "/catalog/yakhting/kurtka/muzhskoe/" => "/catalog/odezhda/kurtka/muzh/",
    "/catalog/yakhting/kurtka-neprom/zhenskoe/" => "/catalog/odezhda/kurtka/kurtka-neprom/zhenskoe/",
    "/catalog/yakhting/noski/uniseks/" => "/catalog/odezhda/noski/uniseks/",
    "/catalog/odezhda/polo/zhenskoe/" => "/catalog/yakhting/polo/zhenskoe/",
    "/catalog/odezhda/polo/muzhskoe/" => "/catalog/yakhting/polo/muzhskoe/",
    "/catalog/odezhda/shapka/uniseks/" => "/catalog/gidroodezhda/shapka/uniseks/",
    "/catalog/yakhting/shapka/uniseks/" => "/catalog/gidroodezhda/shapka/uniseks/",
    "/catalog/yakhting/shorty/zhen/" => "/catalog/odezhda/shorty/zhenskoe/",
    "/catalog/yakhting/shorty/muzhskoe/" => "/catalog/odezhda/shorty/muzhskoe/"
];

return $canonical;
