<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Ознакомьтесь с огромных количеством живописных мест для активного отдыха со спортивной экипировкой от нашего магазина H2O");
$APPLICATION->SetPageProperty("title", "Экипировка для водных видов спорта по всей России. Где покататься");
$APPLICATION->SetTitle("Где кататься");
?><?

$APPLICATION->IncludeComponent(
		"webavk:blank.component", "places", Array(
	"CACHE_TIME" => "3600",
	"CACHE_TYPE" => "N",
	"IBLOCK_ID" => 32,
	"IBLOCK_ID_SPORT" => 31,
	"SPORT_TYPE_ID" => $_REQUEST['sport'],
	"REGION_ID" => $_REQUEST['region'],
	"PLACE" => $_REQUEST['place'],
		)
);
?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>