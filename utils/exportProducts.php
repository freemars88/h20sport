<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

function array2csv(array &$array)
{
	if (count($array) == 0) {
		return null;
	}
	ob_start();
	$df = fopen("php://output", 'w');
	//fputcsv($df, $array[0], ';');
	foreach ($array as $key => $row) {
		/*if ($key == 0) {
			continue;
		}*/
		fputcsv($df, $row, ';');
	}
	fclose($df);
	return ob_get_clean();
}

function download_send_headers($filename) {
	// disable caching
	$now = gmdate("D, d M Y H:i:s");
	header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
	header("Last-Modified: {$now} GMT");

	// force download
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");

	// disposition / encoding on response body
	header("Content-Disposition: attachment;filename={$filename}");
	header("Content-Transfer-Encoding: binary");
}

function toWindow($ii){
    return iconv("utf-8", "windows-1251", $ii);
}

$arResult = [];
$arResult[] = [
	'category',
	'brand',
	'name',
	'article',
	'barcode',
	'color',
	'size',
	'country',
	'year',
	'sex',
	'width'
];
$resProducts = CIBlockElement::GetList([], ['IBLOCK_ID' => IBLOCK_CATALOG, 'ACTIVE' => 'Y']);//, false, ['nPageSize' => 10]);
while($obProduct = $resProducts->GetNextElement()) {
	$arFields = $obProduct->GetFields();
	$arProperties = $obProduct->GetProperties();
	//debug($arFields);
	//debug($arProperties);

	$resCategoryChain = CIBlockSection::GetNavChain(false, $arFields['IBLOCK_SECTION_ID']);
	$nameCategory = $resCategoryChain->Fetch()['NAME'];

	$resOffers = CIBlockElement::GetList(
		[],
		['IBLOCK_ID' => IBLOCK_CATALOG_OFFERS, 'ACTIVE' => 'Y', 'PROPERTY_CML2_LINK' => $arFields['ID']]);

	while ($obOffer = $resOffers->GetNextElement()) {
		$arOfferFields = $obOffer->GetFields();
		$arOfferProperties = $obOffer->GetProperties();

		$arResult[] = [
			toWindow($nameCategory),
			toWindow(CIBlockElement::GetByID($arProperties['BRAND']['VALUE'])->Fetch()['NAME']),
			toWindow($arOfferFields['NAME']),
			toWindow($arProperties['CML2_ARTICLE']['VALUE']),
			toWindow($arOfferProperties['CML2_BARCODE']['VALUE']),
			toWindow(CIBlockElement::GetByID($arOfferProperties['COLOR']['VALUE'])->Fetch()['NAME']),
			toWindow(CIBlockElement::GetByID($arOfferProperties['SIZE']['VALUE'])->Fetch()['NAME']),
			toWindow(CIBlockElement::GetByID($arProperties['COUNTRY']['VALUE'])->Fetch()['NAME']),
			toWindow(CIBlockElement::GetByID($arProperties['YEAR']['VALUE'])->Fetch()['NAME']),
			toWindow(CIBlockElement::GetByID($arProperties['SEX']['VALUE'])->Fetch()['NAME']),
			toWindow(CIBlockElement::GetByID($arOfferProperties['WIDTH']['VALUE'])->Fetch()['NAME']),
		];
	}
}

debug($arResult);
$APPLICATION->RestartBuffer();
download_send_headers("data_export_" . date("Y-m-d") . ".csv");
echo array2csv($arResult);
die();
?>