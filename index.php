<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин спортивного снаряжения «Н2О» оформляет заказы круглосуточно без выходных. Ответы на возникшие вопросы вы можете получить у наших консультантов, если зададите их в разделе «Контакты» или свяжитесь с нами по телефону: +7 (965) 119-23-10");
$APPLICATION->SetPageProperty("title", "Спортивное снаряжение для водных видов спорта по всей России. Магазин “H2O”");
$APPLICATION->SetTitle("H2OSport");
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>