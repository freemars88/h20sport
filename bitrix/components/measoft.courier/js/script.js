function measoftCourier() {
    var orderId = getURLVar('ID');
    var curStatus = $('#STATUS_ID option:selected').attr('value');
    var prevent = true;
    var wasAjax = false;

    // передача заказа в курьерскую службу при изменении статуса заказа
    if (curStatus) {
        $('#save_status_button, #editStatusDIV .adm-btn, input[name=save], input[name=apply]').on('click', function(e) {
            var self = $(this);
            var statusId = $('#STATUS_ID option:selected').attr('value');
            if (!wasAjax) {
                wasAjax = true;
                $.ajax({
                    type: 'get',
                    url: '/bitrix/components/measoft.courier/ajax.php?action=saveStatus&orderId='+orderId+'&statusId='+statusId,
                    dataType: 'json',
                    success: function(data, textStatus, jqXHR) {
                        if (data.message) {
                            alert(data.message);
                        }
                        if (data.data && data.data.status) {
                            $('#meafott-order-status').html(data.data.status);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert('Ошибка передачи заказа в курьерскую службу!');
                    },
                    complete: function() {
                        prevent = false;
                        if (self.attr('id') != 'save_status_button') {
                            self.click();
                        }
                    }
                });
            }
            if (prevent && curStatus != statusId) {
                return false;
            }
        });
    }

    // отображение статуса доставки на странице заказа
    var statusContainer = $('#sale-order-edit-block-order-info .adm-bus-orderinfoblock-content .adm-bus-orderinfoblock-content-block-last');
    if (statusContainer.length) {
        $.ajax({
            type: 'get',
            url: '/bitrix/components/measoft.courier/ajax.php?action=checkStatus&orderId=' + orderId,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                if (data.message) {
                    statusContainer.append('Статус доставки: "<span id="meafott-order-status">'+data.message+'</span>"');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Ошибка проверки статуса заказа в службе доставки!');
            }
        });
    }

    // dropdown календарь на странице оформления заказа клиентом
    $(document).on('focus', '#ms_date_putn', function() {
        $(this).datepicker({
            "dateFormat": "dd.mm.yy",
            "dayNamesMin": ["вс","пн","вт","ср","чт","пт","сб"],
            "monthNames": ["январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь"]
        });
    });
}

function toggleForm(){
    var msForm = $('#ms_courier');
    var msButton = $('#ID_DELIVERY_courier_simple');
    if (msButton) {
        if(msButton.is(':checked')) {
            msForm.show();
            return false;
        }
        else msForm.hide();
    }
    return false;
}

// возвращает значение переменной строки запроса
function getURLVar(param) {
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        var hash = hashes[i].split('=');
        if (param == hash[0]) {
            return hash[1];
        }
    }
    return undefined;
}

// динамическая подгрузка jQuery и jQuery UI
if (!window.jQuery) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://code.jquery.com/jquery-latest.min.js';
    document.getElementsByTagName('head')[0].appendChild(script);
    script.addEventListener('load', function(){
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js';
        document.getElementsByTagName('head')[0].appendChild(script);
        $(document).ready(function(){
            measoftCourier();
        });
    }, false);
} else {
    $(document).ready(function(){
        measoftCourier();
    });
}