<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php');
require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/measoft.courier/MeasoftEvents.php');
require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/measoft.courier/classes/Measoft.php');

CModule::IncludeModule("sale");

$action = isset($_GET['action']) ? $_GET['action'] : '';

// данные для ответа
$success = true;
$message = '';
$data = array();

// создание объекта Measoft
$measoft = new Measoft(MeasoftEvents::configValue('LOGIN'), MeasoftEvents::configValue('PASSWORD'), MeasoftEvents::configValue('CODE'));

switch ($action) {
    case 'saveStatus':
        $orderId = isset($_GET['orderId']) ? $_GET['orderId'] : null;
        $statusId = isset($_GET['statusId']) ? $_GET['statusId'] : null;

        $order = CSaleOrder::GetByID($orderId);

        if (isset($order['DELIVERY_ID']) && $order['DELIVERY_ID'] == 'courier:simple' && $order && $statusId == MeasoftEvents::configValue('SEND_STATUS')) {
            $dbItems = CSaleBasket::GetList(array(), array('ORDER_ID' => $orderId), false, false, array());
            $items = array();
            while ($item = $dbItems->Fetch()) {
                $items[] = $item;
            }

            // получаем поля покупателя
            $dbProps = CSaleOrderPropsValue::GetOrderProps($orderId);
            $props = array();
            while ($prop = $dbProps->Fetch()) {
                $props[$prop['CODE']] = $prop['VALUE'];
            }

            $measoftOrder = MeasoftEvents::getOrderArray($order, $props);
            $measoftItems = MeasoftEvents::getItemsArray($items, $order['PRICE_DELIVERY']);

            if (!$measoft->statusRequest($measoftOrder['orderno']) && $measoft->orderRequest($measoftOrder, $measoftItems)) {
                $message = "Заказ №$orderId успешно отправлен в курьерскую службу!";
                $data['status'] = 'Новый';
            } else {
                $message = $measoft->errors;
            }
        }
        break;
    case 'checkStatus':
        $orderId = isset($_GET['orderId']) ? $_GET['orderId'] : null;

        if ($status = $measoft->statusRequest($orderId)) {
            $message = $status;
        } else {
            $message = 'Заказ не отправлен в курьерскую службу!';
        }
        break;
}

echo json_encode(array(
    'success' => $success,
    'message' => $message,
    'data' => $data
));