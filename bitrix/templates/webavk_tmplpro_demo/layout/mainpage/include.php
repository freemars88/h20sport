<?
global $layoutArea;
if ($layoutArea == "footer")
	goto FOOTER;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<? $APPLICATION->ShowHead(); ?>
		<title><? $APPLICATION->ShowTitle(); ?></title>
	</head>

	<body>
		<?
		if ($USER->IsAuthorized()) {
			?>
			<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
			<?
		}
		?>
		<? CWebavkTmplProTools::ShowPropertyTemplate("H1_TITLE", '', '<h1>#VALUE#</h1>'); ?>
		<?
		if ($layoutArea == "header")
			goto TEMPLATE_END;
		FOOTER:
		?>

	</body>
</html>
<?
TEMPLATE_END:
?>