<?

CModule::IncludeModule("webavk.tmplpro");
global $layoutArea;
if ($layoutArea == "header") {
	CWebavkTmplProTools::InitLibFromDir();
	CWebavkTmplProTools::IncludeCSSFromDir(false, SITE_ID);
	CWebavkTmplProTools::IncludeJSFromDir(false, SITE_ID);
}
if ($APPLICATION->GetCurPage(true) == "/index.php" || (defined("MAINPAGE_TEMPLATE") && MAINPAGE_TEMPLATE === true)) {
	include(__DIR__ . "/mainpage/include.php");
} else {
	include(__DIR__ . "/.default/include.php");
}
