
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper mobileOn" bgcolor="#FFFFFF" style="display:none;">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="http://<?=$arParams['SERVER_NAME']?>/catalog/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
											</th>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="http://<?=$arParams['SERVER_NAME']?>/brands/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Бренды</a>
											</th>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="http://<?=$arParams['SERVER_NAME']?>/actions/" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Акции</a>
											</th>
											<th width="25%" align="center" valign="top" class="mobile" style="border-bottom: 1px solid #ffffff;padding: 14px 0;background-color: #f0f0f0;">
												<a href="http://<?=$arParams['SERVER_NAME']?>/contacts/" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
											</th>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td height="2" bgcolor="#333333" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td align="center" valign="top">
												<p style="margin:0; padding:0;font-size:12px;color:#808080;">Это письмо было отправлено на email: <a href="mailto:#MAILTO#" style="color:#333333; text-decoration:underline;font-size:12px;">#MAILTO#</a></p>
												<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#808080;">Чтобы не пропустить ни одного письма от нас, добавьте адрес <a href="mailto:info@h2osport.ru" style="color:#333333; text-decoration:underline;font-size:12px;">info@h2osport.ru</a> в адресную книгу.</p>
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td align="center" valign="top">

												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td>
															<a href="https://www.facebook.com/" target="_blank"><img src="http://<?=$arParams['SERVER_NAME']?>/local/mailtemplate/mail/fb.gif" style="display:block;" border="0" height="26"></a>
														</td>
														<td width="40">
															<br/>
														</td>
														<td>
															<a href="https://www.instagram.com/" target="_blank"><img src="http://<?=$arParams['SERVER_NAME']?>/local/mailtemplate/mail/in.gif" style="display:block;" border="0" height="26"></a>
														</td>
														<td width="40">
															<br/>
														</td>
														<td>
															<a href="https://www.vk.com/" target="_blank"><img src="http://<?=$arParams['SERVER_NAME']?>/local/mailtemplate/mail/vk.gif" style="display:block;height:26px;" border="0" height="26"></a>
														</td>
													</tr>
												</table>

											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>
		</center>
	</body>
</html>
