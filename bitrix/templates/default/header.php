<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>#THEME#</title>

		<style type="text/css">

			/* Outlines the grids, remove when sending */
			table td { border: 0 none; }
			table th { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;}

			/* CLIENT-SPECIFIC STYLES */
			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
			img { -ms-interpolation-mode: bicubic; }

			/* RESET STYLES */
			img { border: 0; outline: none; text-decoration: none; }
			table { border-collapse: collapse !important; }
			body { margin: 0 !important; padding: 0 !important; width: 100% !important; }

			/* iOS BLUE LINKS */
			a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}

			/* ANDROID CENTER FIX */
			div[style*="margin: 16px 0;"] { margin: 0 !important; }

			/* MEDIA QUERIES */
			@media all and (max-width:639px){ 
				.wrapper{ width:320px!important; padding: 0 !important; }
				.container{ width:300px!important;  padding: 0 !important; }
				.mobile{ width:300px!important; display:block!important; padding: 0 !important; }
				.mobilefont{font-size:12px !important;}
				.mobilecenter{text-align:center !important;}
				.img{ width:100% !important; height:auto !important; }
				.img-nofull{ max-width:100% !important; height:auto !important; }
				*[class="mobileOff"] { width: 0px !important; display: none !important; }
				*[class*="mobileOn"] { display: block !important; max-height:none !important; }
				table th.mobile { border: 0 none; border-bottom: 1px solid #ffffff;padding: 14px 0 !important;background-color: #fcfcfc;font-weight:normal;}
				table th a{display:block;}
			}

		</style>    
	</head>
	<body style="margin:0; padding:0; background-color:#ffffff;">

		<span style="display: block; width: 640px !important; max-width: 640px; height: 1px" class="mobileOff"></span>

		<center>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
				<tr>
					<td align="center" valign="top">

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td width="200" class="mobile mobilecenter" align="left" valign="top">

												<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;">+7 965 1192310<br/>+7 495 6618784</p>

											</td>
											<td width="200" class="mobile" align="center" valign="top">

												<a href="http://<?=$arParams['SERVER_NAME']?>" target="_blank">
													<img src="http://<?=$arParams['SERVER_NAME']?>/local/mailtemplate/mail/logo.jpg" width="220" height="70" style="margin:0; padding:0; border:none; display:block;max-width:100%;" border="0" class="img-nofull" alt="H2OSport">
												</a>

											</td>
											<td width="200" class="mobileOff" align="right" valign="top">

												<p style="margin:0; padding:0; margin-bottom:15px;font-size:12px;color:#333333;"></p>

											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td width="25%" align="center" valign="top">
												<a href="http://<?=$arParams['SERVER_NAME']?>/catalog/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Каталог</a>
											</td>
											<td width="25%" align="center" valign="top">
												<a href="http://<?=$arParams['SERVER_NAME']?>/brands/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Бренды</a>
											</td>
											<td width="25%" align="center" valign="top">
												<a href="http://<?=$arParams['SERVER_NAME']?>/actions/" class="mobilefont" target="_blank" style="color:#ff0000; text-decoration:none;font-size:20px;font-weight:bold;">Акции</a>
											</td>
											<td width="25%" align="center" valign="top">
												<a href="http://<?=$arParams['SERVER_NAME']?>/contacts/" class="mobilefont" target="_blank" style="color:#333333; text-decoration:none;font-size:20px;font-weight:bold;">Контакты</a>
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>
						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="2" bgcolor="#333333" style="font-size:1px; line-height:1px;height:1px;background-color:#cccccc;">&nbsp;</td>
							</tr>
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
						</table>

						<table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
							<tr>
								<td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" valign="top">

									<table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
										<tr>
											<td align="center" valign="top">
												