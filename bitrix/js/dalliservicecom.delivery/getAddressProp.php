<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$address_prop = COption::GetOptionString('dalliservicecom.delivery', 'ADDRESS');
$db_props = CSaleOrderProps::GetList(array(),  array("CODE" => $address_prop), false, false, array('ID'));
if ($props = $db_props->Fetch()) {
	echo json_encode(array('addressprop'=>$props['ID']));
}   
?>
