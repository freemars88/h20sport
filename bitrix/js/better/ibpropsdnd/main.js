function trowSortDrop(isGlow) {
    if (!(window.betterDndSortStep > 0))
        var betterDndSortStep = 10;

    var i = 1;
    $('#ib_prop_list td:nth-child(7) input').each(function(){
        t = $(this);
        var oldv = t.val();
        var newv = i*betterDndSortStep;
        if (isGlow) {
            //alert(oldv+' != '+newv);
            if (oldv != newv) {
                //t.parent().parent().fadeTo(150,1,function(){$(this).addClass("betterGlowRow")}).delay(100).fadeTo(150,1,function(){$(this).removeClass("betterGlowRow")});
            }
        }
        t.val(i*betterDndSortStep);
        i++;
    });
}
function trowSortSort($tr) {
    $('#ib_prop_list>tbody>tr').tsort('td:eq(6)>input',{useVal:true});
    $tr.fadeTo(50,1,function(){$tr.addClass("betterGlowRow")}).delay(400).fadeTo(50,1,function(){$tr.removeClass("betterGlowRow")});
    trowSortDrop(true);
    
}
function makeDNDib_prop_listTable(bIfWRow) {
    if (bIfWRow)
        obIBProps.addPropRow();

    trowSortDrop();

    $('#ib_prop_list tbody td:nth-child(1)').each(function(){
        t = $(this);
        t.addClass('dragHandle');
    });
    $('#ib_prop_list').tableDnD({
        onDragClass: 'dragRow',
        onDrop: function () {
            trowSortDrop();
        },
        dragHandle: "dragHandle"
    });

    $('#ib_prop_list tbody td:nth-child(7) input').attr('onchange','trowSortSort($(this).parent().parent());');
}

$(document).ready(function(){
    $('#ib_prop_list tr:first').addClass('nodrop');

    // move first tr to thead
    $('#ib_prop_list').prepend('<thead><tr>'+$('#ib_prop_list tr:first-child').html()+'</tr></thead>');
    $($('#ib_prop_list thead tr').get(0)).attr('class',$($('#ib_prop_list tbody tr').get(0)).attr('class'));
    $('#ib_prop_list tbody tr:first-child').remove();

    makeDNDib_prop_listTable ();
    $('#ib_prop_list').parent().find('div > input').attr('onclick','makeDNDib_prop_listTable(true);');
   
});
