<?php

//Подключение классов
if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog")) {
    exit;
}

global $APPLICATION;
IncludeModuleLangFile(__FILE__);

$APPLICATION->AddHeadScript("/bitrix/components/measoft.courier/js/script.js");

\Bitrix\Main\Loader::registerAutoLoadClasses(
    'measoft.courier',
    array(
        'MeasoftEvents' => 'MeasoftEvents.php',
        'Measoft' => 'classes/Measoft.php',
        'Errors' => 'classes/Errors.php',
    )
);