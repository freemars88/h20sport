<?php

    class MeasoftEvents
    {
        /**
         * Настройки модуля.
         */
        public static $_config;

        /**
         * Инициализация объекта.
         */
        public function Init()
        {
            IncludeModuleLangFile(__FILE__);

            return array(
                // общее описание
                "SID" => "courier",
                "NAME" => GetMessage('MEASOFT_HANDLER_NAME'),
                "DESCRIPTION" => GetMessage('MEASOFT_HANDLER_DESCRIPTION'),
                "DESCRIPTION_INNER" => GetMessage('MEASOFT_HANDLER_DESCRIPTION_INNER'),
                "BASE_CURRENCY" => COption::GetOptionString("sale", "default_currency", "RUB"),

                "HANDLER" => __FILE__,

                // методы-обработчики
                "DBGETSETTINGS" => array(__CLASS__, "GetSettings"),
                "DBSETSETTINGS" => array(__CLASS__, "SetSettings"),
                "GETCONFIG" => array(__CLASS__, "GetConfig"),

                "COMPABILITY" => array(__CLASS__, "Compability"),
                "CALCULATOR" => array(__CLASS__, "Calculate"),

                // Список профилей
                "PROFILES" => array(
                    "simple" => array(
                        "TITLE" => 'courier',
                        "DESCRIPTION" => GetMessage('MEASOFT_HANDLER_DESCRIPTION'),
                        "RESTRICTIONS_WEIGHT" => array(0),
                        "RESTRICTIONS_SUM" => array(0),
                    ),
                )
            );
        }

        /**
         * Запрос конфигурации службы доставки.
         */
        function GetConfig()
        {
            global $APPLICATION;

            // список статусов заказа
            $dbStatusesList = CSaleStatus::GetList(array('SORT' => 'ASC'), array(), false, false, array("ID", "NAME"));
            $sendStatuses = array();
            while ($arResult = $dbStatusesList->Fetch()){
                $sendStatuses[$arResult['ID']] = $arResult['NAME'];
            }

            // список полей клиента
            $dbProps = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("ACTIVE" => 'Y'), false, false, array());
            $props = array();
            while($prop = $dbProps->Fetch()){
                $props[$prop['CODE']] = $prop['NAME'];
            }

            $arConfig = array(
                "CONFIG_GROUPS" => array(
                    'general' => GetMessage('MEASOFT_HANDLER_GROUP_GENERAL'),
                    'price' => GetMessage('MEASOFT_HANDLER_GROUP_DELIVERY_PRICE_POLICY')
                ),
                "CONFIG" => array(
                    "SECTION_AUTH" => array(
                        'TYPE' => 'SECTION',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_SECTION_AUTH'),
                        "GROUP" => "general",
                    ),
                    "LOGIN"=> array(
                        "TYPE" => "STRING",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_CLIENT_LOGIN'),
                        "DEFAULT" => '',
                        "GROUP" => "general",
                    ),
                    "PASSWORD"=> array(
                        "TYPE" => "STRING",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_CLIENT_PASSWORD'),
                        "DEFAULT" => '',
                        "GROUP" => "general",
                    ),
                    "CODE"=> array(
                        "TYPE" => "STRING",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_CLIENT_CODE'),
                        "DEFAULT" => '',
                        "GROUP" => "general",
                    ),

                    "SECTION_PROPS" => array(
                        'TYPE' => 'SECTION',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_SECTION_PROPS'),
                        "GROUP" => "general",
                    ),
                    "SEND_STATUS"=> array(
                        "TYPE" => "DROPDOWN",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_SEND_STATUS'),
                        "VALUES" => $sendStatuses,
                        "DEFAULT" => '',
                        "GROUP" => "general",
                    ),
                    "USE_ARTICLES"=> array(
                        "TYPE" => "CHECKBOX",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_USE_ARTICLES'),
                        "DEFAULT" => '',
                        "GROUP" => "general",
                    ),

                    "SECTION_FIELDS" => array(
                        'TYPE' => 'SECTION',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_SECTION_FIELDS'),
                        "GROUP" => "general",
                    ),
                    "PROP_FIO"=> array(
                        "TYPE" => "DROPDOWN",
                        "DEFAULT" => "FIO",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_PROP_FIO'),
                        "GROUP" => "general",
                        'POST_TEXT' => '',
                        'VALUES' => $props
                    ),
                    "PROP_CITY" => array(
                        "TYPE"=>"DROPDOWN",
                        "DEFAULT" => "CITY",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_PROP_CITY'),
                        "GROUP" => "general",
                        'VALUES' => $props
                    ),
                    "PROP_ADDRESS" => array(
                        "TYPE"=>"DROPDOWN",
                        "DEFAULT" => "ADDRESS",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_PROP_ADDRESS'),
                        "GROUP" => "general",
                        'VALUES' => $props
                    ),
                    "PROP_PHONE" => array(
                        "TYPE" => "DROPDOWN",
                        "DEFAULT" => "PHONE",
                        "TITLE" => GetMessage('MEASOFT_CONFIG_PROP_PHONE'),
                        "GROUP" => "general",
                        'VALUES' => $props
                    ),
                )
            );

            // ценовая политика доставки
            for ($i = 1; $i <= 3; $i++) {
                $arConfig['CONFIG'] += array(
                    'SECTION_PRICE_'.$i => array(
                        'TYPE' => 'SECTION',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_SECTION_PRICE_INTERVAL') . " №$i",
                        "GROUP" => "price",
                    ),
                    'PRICE_IF_'.$i.'_MIN' => array (
                        'TYPE' => 'STRING',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_ORDER_MIN_PRICE'),
                        'SIZE' => 1,
                        'DEFAULT' => '',
                        'GROUP' => 'price',
                        'CHECK_FORMAT' => 'NUMBER',
                    ),
                    'PRICE_IF_'.$i.'_MAX' => array (
                        'TYPE' => 'STRING',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_ORDER_MAX_PRICE'),
                        'SIZE' => 1,
                        'DEFAULT' => '',
                        'GROUP' => 'price',
                        'CHECK_FORMAT' => 'NUMBER',
                    ),
                    'PRICE_IF_'.$i.'_TYPE' => array (
                        'TYPE' => 'DROPDOWN',
                        'TITLE' => 'Тип оплаты',
                        'DEFAULT' => 1,
                        'VALUES' => array(
                            1 => GetMessage('MEASOFT_CONFIG_OPTION_CLIENT_PAY_ALL'),
                            2 => GetMessage('MEASOFT_CONFIG_OPTION_MARKET_PAY_ALL'),
                            3 => GetMessage('MEASOFT_CONFIG_OPTION_MARKET_PAY_PRESENT'),
                            4 => GetMessage('MEASOFT_CONFIG_OPTION_MARKET_PAY_RUB'),
                        ),
                        'GROUP' => 'price',
                    ),
                    'PRICE_IF_'.$i.'_AOMUNT' => array (
                        'TYPE' => 'STRING',
                        'TITLE' => GetMessage('MEASOFT_CONFIG_SUMM'),
                        'SIZE' => 1,
                        'DEFAULT' => '',
                        'GROUP' => 'price',
                        'CHECK_FORMAT' => 'NUMBER',
                    ),
                );
            }

            return $arConfig;
        }

        /**
         * Возвращает значение из конфига.
         * (Дополнительный метод)
         */
        public static function configValue($param = null)
        {
            if (!self::$_config) {
                self::$_config = CSaleDeliveryHandler::GetBySID('courier')->Fetch();
            }

            if ($param) {
                return isset(self::$_config['CONFIG']['CONFIG'][$param]) ? self::$_config['CONFIG']['CONFIG'][$param]['VALUE'] : null;
            }

            return self::$_config;
        }

        /**
         * Проверка настроек
         */
        public function configIsIncorrect($config)
        {
            return false;
        }

        /**
         * Взять настойки.
         */
        function GetSettings($strSettings)
        {
            return unserialize($strSettings);
        }

        /**
         * Установить настройки.
         */
        function SetSettings($arSettings)
        {
            return serialize($arSettings);
        }

        /**
         * Проверка соответствия профиля доставки заказу.
         */
        function Compability($arOrder, $arConfig)
        {
            return array("simple");
        }

        /**
         * Проверка статуса.
         */
        public function statusIsIncorrect($statusID, $config)
        {
            return false;
        }

        /**
         * Расчет стоимости доставки.
         */
        static function Calculate($profile, $arConfig, $arOrder = false, $step = false, $temp = false)
        {
            // определение городов отправления и доставки
            $tf = CSaleLocation::GetByID($arOrder['LOCATION_FROM']);
            $townfrom = $tf['CITY_NAME'] ? $tf['CITY_NAME'] : 'Москва';
            $tt = CSaleLocation::GetByID($arOrder['LOCATION_TO']);
            $townto = $tt['CITY_NAME'] ? $tt['CITY_NAME'] : $_COOKIE['MS_CITY_NAME'];

            // пытаемся отправить запрос расчета стоимости доставки
            $measoft = new Measoft(self::configValue('LOGIN'), self::configValue('PASSWORD'), self::configValue('CODE'));
            $price = $measoft->calculatorRequest(array(
                                                      'townfrom' => $townfrom,
                                                      'townto' => $townto,
                                                      'mass' => $arOrder['WEIGHT']/1000
                                                 ));
            $orderPrice = $arOrder['PRICE'];

            // расчет стоимости доставки с учетом ценовой политики магазина
            if ($price != false) {
                for ($i = 1; $i <= 3; $i++) {
                    $min = is_numeric(self::configValue('PRICE_IF_'.$i.'_MIN')) ? (int) self::configValue('PRICE_IF_'.$i.'_MIN') : null;
                    $max = is_numeric(self::configValue('PRICE_IF_'.$i.'_MAX')) ? (int) self::configValue('PRICE_IF_'.$i.'_MAX') : null;
                    $type = self::configValue('PRICE_IF_'.$i.'_TYPE');
                    $aomunt = self::configValue('PRICE_IF_'.$i.'_AOMUNT');

                    if (($min != null && $max != null && $orderPrice > $min && $orderPrice < $max)
                        || ($min != null && $max == null && $orderPrice > $min)
                        || ($min == null && $max != null && $orderPrice < $max)
                    ) {
                        switch ($type) {
                            case 1:
                                $price = $aomunt ? $aomunt : $price;
                                break;
                            case 2:
                                $price = 0;
                                break;
                            case 3:
                                $price = round($price - $price * $aomunt/100);
                                break;
                            case 4:
                                $price = ($price > $aomunt) ? round($price - $aomunt) : 0;
                                break;
                        }
                    }
                }
            }

            if ($measoft->errors) {
                $measoft->errors = implode(';<br>', $measoft->errors);
                return array( "RESULT" => "ERROR", 'TEXT' => $measoft->errors);
            } else {
                return array("RESULT" => "OK", 'VALUE' => $price);
            }
        }

        /**
         * Событие происходит когда выбирается служба доставки.
         */
        static function OnSaleComponentOrderOneStepDelivery(&$arResult, &$arUserResult, $arParams)
        {
            global $APPLICATION;

            if (empty($arResult["DELIVERY"])) return;

            if (isset($arResult["DELIVERY"]["courier"]) && $arResult["DELIVERY"]["courier"]["PROFILES"]["simple"]["CHECKED"] == 'Y') {
                $arResult["DELIVERY"]["courier"]["PROFILES"]["simple"]["DESCRIPTION"] .= self::getDeliveryDescription();
            } else if (is_array($arResult["DELIVERY"])) {
                foreach ($arResult["DELIVERY"] as $id => $delivery) {
                    if (preg_match('/(courier)/', $delivery['NAME']) && $delivery["CHECKED"] == 'Y') {
                        $arResult["DELIVERY"][$id]['DESCRIPTION'] .= self::getDeliveryDescription();
                    }
                }
            }
        }

        /**
         * Событие происходит после создания заказа и всех его параметров.
         */
        static function OnSaleComponentOrderOneStepComplete($orderId, $order)
        {
            global $APPLICATION;

            // устанавливаем доп свойства заказа
            self::setOrderPropsValue($orderId, 'MEASOFT_DATE_PUTN', $_REQUEST['MS_DATE_PUTN']);
            self::setOrderPropsValue($orderId, 'MEASOFT_TIME_MIN', $_REQUEST['MS_TIME_MIN']);
            self::setOrderPropsValue($orderId, 'MEASOFT_TIME_MAX', $_REQUEST['MS_TIME_MAX']);

            // получение вложений заказа
            $dbItems = CSaleBasket::GetList(array(), array('ORDER_ID' => $orderId), false, false, array());
            $items = array();
            while ($item = $dbItems->Fetch()) {
                $items[] = $item;
            }

            // получаем поля покупателя
            $dbProps = CSaleOrderPropsValue::GetOrderProps($orderId);
            $props = array();
            while ($prop = $dbProps->Fetch()) {
                $props[$prop['CODE']] = $prop['VALUE'];
            }

            // отправка заказа, если статус отправки "Принят, ожидается оплата"
            if (self::configValue('SEND_STATUS') == 'N') {
                $measoft = new Measoft(self::configValue('LOGIN'), self::configValue('PASSWORD'), self::configValue('CODE'));

                if ($measoft->orderRequest(self::getOrderArray($order, $props), self::getItemsArray($items, $order['PRICE_DELIVERY']))) {

                } else {
                    CSaleOrder::CommentsOrder($orderId, $measoft->errors);
                    CSaleOrder::CancelOrder($orderId, 'Y', $measoft->errors);
                    ShowMessage("Внимание! Заказ принят, но не отправлен в выбранную вами службу доставки:<br>".$measoft->errors);
                    exit();
                }
            }
        }

        /**
         * Описание службы доставки.
         * (Дополнительный метод)
         */
        protected static function getDeliveryDescription()
        {
            ob_start();
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/measoft.courier/block.php");
            $content = ob_get_clean();
            return str_replace(array("\n","\r"), array(' ', ''), $content);
        }

        /**
         * Получаем массив заказ.
         * (Дополнительный метод)
         */
        static function getOrderArray($order, $props)
        {
            if (!$order || !$props) {
                return false;
            }

            $propFio = MeasoftEvents::configValue('PROP_FIO');
            $propCity = MeasoftEvents::configValue('PROP_CITY');
            $propAddress = MeasoftEvents::configValue('PROP_ADDRESS');
            $propPhone = MeasoftEvents::configValue('PROP_PHONE');

            if (is_numeric($props[$propCity])) {
                $res = CSaleLocation::GetByID($props[$propCity]);
                $recipientCity = $res['CITY_NAME'];
            } else {
                $recipientCity = (isset($props[$propCity]) && $props[$propCity]) ? $props[$propCity] : $order['CITY'];
            }

            return array(
                'sender' => self::getSender(),
                'orderno' => $order['ID'],
//            'barcode' => $order[''],
//            'company' => $order[''],
                'person' => $props[$propFio],
                'phone' => $props[$propPhone],
                'town' => $recipientCity,
                'address' => $props[$propAddress],
                'date' => ConvertDateTime($props['MEASOFT_DATE_PUTN'], "YYYY-MM-DD"),
                'time_min' => $props['MEASOFT_TIME_MIN'],
                'time_max' => $props['MEASOFT_TIME_MAX'],
//            'weight' => $order[''],
//            'quantity' => $order[''],
                'price' => $order['PRICE'],
                'inshprice' => $order['PRICE'],
                'paytype' => $order['PAYED'] == 'Y' ? 'NO' : 'CASH',
                'enclosure' => '',
                'instruction' => $order['USER_DESCRIPTION'],
            );
        }

        /**
         * Получаем массив вложения.
         * (Дополнительный метод)
         */
        static function getItemsArray($items, $shipping = null)
        {
            if (!$items) {
                return false;
            }

            $result = array();
            foreach ($items as $item) {
                $article = '';
                $product = CCatalogProduct::GetByIDEx($item['PRODUCT_ID']);
                if (isset($product['PROPERTIES']['ARTNUMBER']['VALUE'])) {
                    $article = $product['PROPERTIES']['ARTNUMBER']['VALUE'];
                }

                $result[] = array(
                    'name' => $item['NAME'],
                    'quantity' => $item['QUANTITY'],
                    'mass' => $item['WEIGHT'],
                    'retprice' => $item['PRICE'],
                    'VATrate' => (int)round($item['VAT_RATE'] * 100, 0),
                    'article' => self::configValue('USE_ARTICLES') == 'Y' ? $article : '',
                );
            }

            // включение доставки в список вложений
            if ($shipping) {
                $result[] = array(
                    'name' => 'Доставка',
                    'quantity' => 1,
                    'mass' => 0,
                    'retprice' => $shipping,
                    'VATrate' => '',
                    'article' => '',
                );
            }

            return $result;
        }

        /**
         * Установка значения доп свойства заказа.
         */
        static public function setOrderPropsValue($orderId, $code, $value)
        {
            if ($arProp = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $orderId, 'CODE' => $code))->Fetch()) {
                return CSaleOrderPropsValue::Update($arProp['ID'], array(
                                                                        'VALUE' => $value,
                                                                   ));
            } else {
                $arProp = CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch();
                return CSaleOrderPropsValue::Add(array(
                                                      'NAME' => $arProp['NAME'],
                                                      'CODE' => $arProp['CODE'],
                                                      'ORDER_PROPS_ID' => $arProp['ID'],
                                                      'ORDER_ID' => $orderId,
                                                      'VALUE' => $value,
                                                 ));
            }
        }

        /**
         * Возвращает название CMS и Bitrix версию.
         * (Дополнительный метод)
         */
        protected static function getSender()
        {
            $module = CModule::CreateModuleObject('measoft.courier');

            return array(
                'module' => Measoft::BITRIX,
                'cms_version' => defined('SM_VERSION') ? SM_VERSION : '',
                'module_version' => $module ? $module->MODULE_VERSION : '1.5.5'
            );
        }

        /**
         * Событие происходит перед добавлением заказа.
         * Не работает. На удаление!!!!!!!!!!!!!!!
         */
        /*static function OnBeforeOrderAdd(&$arFields)
        {
            global $APPLICATION;
            //Делаем проверку на службу доставки
            if (isset($_REQUEST['DELIVERY_ID']) && $_REQUEST['DELIVERY_ID'] != 'courier:simple') {
                return true;
            }
            //Получаем настройки
            $dbConfig = CSaleDeliveryHandler::GetBySID('courier')->Fetch();
            $config = array(
                'login'=>$dbConfig['CONFIG']['CONFIG']['LOGIN']['VALUE'],
                'password'=>$dbConfig['CONFIG']['CONFIG']['PASSWORD']['VALUE'],
                'code'=>$dbConfig['CONFIG']['CONFIG']['CODE']['VALUE'],
            );
            // Попытка валидации
            $measoft = new Measoft($config['login'], $config['password'], $config['code']);
            if (!$measoft->orderValidate(array(
                'phone'=>$_REQUEST['ORDER_PROP_3'],
                'town'=>$_REQUEST['ORDER_PROP_6'],
                'address'=>$_REQUEST['ORDER_PROP_7'],
                'date'=>$_REQUEST['MS_DATE_PUTN'],
                'time_min'=>$_REQUEST['MS_TIME_MIN'],
                'time_max'=>$_REQUEST['MS_TIME_MAX'],
            ))) {
                $arFields['CANCELED'] = 'Y';
                $APPLICATION->ThrowException('?????? ?????????? ??????:<br>'.$measoft->errors);
                return false;
            }
        }*/
        /**
         * Событие происходит при изменении статуса заказа.
         * Не работает, поэтому на удаление!!!!!!
         */
        /*static function OnSaleStatusOrder($orderId, $statusId)
        {
            global $APPLICATION;
            //Получаем заказ
            $order = CSaleOrder::GetByID($orderId);
            //Делаем проверку на службу доставки
            if (isset($order['DELIVERY_ID']) && $order['DELIVERY_ID'] != 'courier:simple') {
                return false;
            }
            //Получаем данные заказа
            $dbProps = CSaleOrderPropsValue::GetOrderProps($orderId);
            $props = array();
            while ($prop = $dbProps->Fetch()) {
                $props[$prop['CODE']] = $prop['VALUE'];
            }
            //Получаем настройки
            $dbConfig = CSaleDeliveryHandler::GetBySID('courier')->Fetch();
            $config = array(
                'login'=>$dbConfig['CONFIG']['CONFIG']['LOGIN']['VALUE'],
                'password'=>$dbConfig['CONFIG']['CONFIG']['PASSWORD']['VALUE'],
                'code'=>$dbConfig['CONFIG']['CONFIG']['CODE']['VALUE'],
            );
            //Проверяем настройки
            if (self::configIsIncorrect($config)) {
                return Errors::getError(GetMessage('MEASOFT_ERROR_CONFIG'));
            }
            //Получаем товары
            $dbItems = CSaleBasket::GetList(array(), array('ORDER_ID'=>$orderId), false, false, array());
            $items = array();
            while ($item = $dbItems->Fetch()) {
                $items[] = $item;
            }
            //Пытаемся отправить заказ
            $measoft = new Measoft($config['login'], $config['password'], $config['code']);
            if ($measoft->orderRequest(self::getOrderArray($order, $props), self::getItemsArray($items))) {
            } else {
                CSaleOrder::CommentsOrder($orderId, $measoft->errors);
                CSaleOrder::CancelOrder($orderId, 'Y', $measoft->errors);
                //ShowMessage("????????! ????? ???????.<br>".$measoft->errors);
                exit();
            }
        }*/
    }
//AddEventHandler("sale", "onSaleDeliveryHandlersBuildList", array('MeasoftEvents', 'Init'));
//AddEventHandler("sale", "OnBeforeOrderAdd", array('MeasoftEvents', 'OnBeforeOrderAdd'));