<?php

class measoft_courier extends CModule
{
    public $MODULE_ID = "measoft.courier";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;

    public $MODULE_GROUP_RIGHTS = "N";

    function getMessage($name, $aReplace = false)
    {
        return GetMessage($name, $aReplace);
    }

    function __construct()
    {
        $arModuleVersion = array();
        include("version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        IncludeModuleLangFile(__FILE__);

        $this->PARTNER_NAME = 'Measoft';
        $this->PARTNER_URI = 'http://courierexe.ru/';

        $this->MODULE_NAME = $this->GetMessage('MEASOFT_MODULE_NAME');
        $this->MODULE_DESCRIPTION = $this->GetMessage('MEASOFT_MODULE_DESCRIPTION');
    }

    function DoInstall()
    {
        //Регистрация событий
        RegisterModuleDependences('sale', 'OnSaleDeliveryHandlersBuildList', $this->MODULE_ID, 'MeasoftEvents', 'Init');
        RegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepDelivery', $this->MODULE_ID, 'MeasoftEvents', 'OnSaleComponentOrderOneStepDelivery');
        RegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepComplete', $this->MODULE_ID, 'MeasoftEvents', 'OnSaleComponentOrderOneStepComplete');
        RegisterModuleDependences('sale', 'OnSaleStatusOrder', $this->MODULE_ID, 'MeasoftEvents', 'OnSaleStatusOrder');
        RegisterModuleDependences('sale', 'OnBeforeOrderAdd', $this->MODULE_ID, 'MeasoftEvents', 'OnBeforeOrderAdd');

        //Регистрация модуля
        RegisterModule($this->MODULE_ID);

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/measoft.courier/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);


        // Добавляем свойства в БД
        CModule::IncludeModule("sale");
        if (!CSaleOrderProps::GetList(array(), array('CODE' => 'MEASOFT_DATE_PUTN'))->Fetch()) {
            CSaleOrderProps::Add(array(
                'CODE'=>                 'MEASOFT_DATE_PUTN',
                'NAME'=>                 'Желаемая дата доставки',
                'TYPE'=>                 'TEXT',
                'REQUIED'=>              'N',
                'PROPS_GROUP_ID'=>       '2',
                'DEFAULT_VALUE'=>        '',
                'PERSON_TYPE_ID'=>       '1',
                'SORT'=>                 '1000',
                'USER_PROPS'=>           'N',
                'IS_LOCATION'=>          'N',
                'IS_EMAIL'=>             'N',
                'IS_PROFILE_NAME'=>      'N',
                'IS_PAYER'=>             'N',
                'IS_LOCATION4TAX'=>      'N',
                'IS_ZIP'=>               'N',
                'IS_FILTERED'=>          'N',
                'ACTIVE'=>               'Y',
                'UTIL'=>                 'Y',
                'INPUT_FIELD_LOCATION'=> '0',
            ));
        }
        if (!CSaleOrderProps::GetList(array(), array('CODE' => 'MEASOFT_TIME_MIN'))->Fetch()) {
            CSaleOrderProps::Add(array(
                'CODE'=>                 'MEASOFT_TIME_MIN',
                'NAME'=>                 'Желаемое начальное время доставки',
                'TYPE'=>                 'TEXT',
                'REQUIED'=>              'N',
                'PROPS_GROUP_ID'=>       '2',
                'DEFAULT_VALUE'=>        '',
                'PERSON_TYPE_ID'=>       '1',
                'SORT'=>                 '1100',
                'USER_PROPS'=>           'N',
                'IS_LOCATION'=>          'N',
                'IS_EMAIL'=>             'N',
                'IS_PROFILE_NAME'=>      'N',
                'IS_PAYER'=>             'N',
                'IS_LOCATION4TAX'=>      'N',
                'IS_ZIP'=>               'N',
                'IS_FILTERED'=>          'N',
                'ACTIVE'=>               'Y',
                'UTIL'=>                 'Y',
                'INPUT_FIELD_LOCATION'=> '0',
            ));
        }
        if (!CSaleOrderProps::GetList(array(), array('CODE' => 'MEASOFT_TIME_MAX'))->Fetch()) {
            CSaleOrderProps::Add(array(
                'CODE'=>                 'MEASOFT_TIME_MAX',
                'NAME'=>                 'Желаемое конечное время доставки',
                'TYPE'=>                 'TEXT',
                'REQUIED'=>              'N',
                'PROPS_GROUP_ID'=>       '2',
                'DEFAULT_VALUE'=>        '',
                'PERSON_TYPE_ID'=>       '1',
                'SORT'=>                 '1200',
                'USER_PROPS'=>           'N',
                'IS_LOCATION'=>          'N',
                'IS_EMAIL'=>             'N',
                'IS_PROFILE_NAME'=>      'N',
                'IS_PAYER'=>             'N',
                'IS_LOCATION4TAX'=>      'N',
                'IS_ZIP'=>               'N',
                'IS_FILTERED'=>          'N',
                'ACTIVE'=>               'Y',
                'UTIL'=>                 'Y',
                'INPUT_FIELD_LOCATION'=> '0',
            ));
        }
    }

    function DoUninstall()
    {
        // Разрегистрация событий
        UnRegisterModuleDependences('sale', 'OnSaleDeliveryHandlersBuildList', $this->MODULE_ID, 'MeasoftEvents', 'Init');
        UnRegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepDelivery', $this->MODULE_ID, 'MeasoftEvents', 'OnSaleComponentOrderOneStepDelivery');
        UnRegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepComplete', $this->MODULE_ID, 'MeasoftEvents', 'OnSaleComponentOrderOneStepComplete');
        UnRegisterModuleDependences('sale', 'OnSaleStatusOrder', $this->MODULE_ID, 'MeasoftEvents', 'OnSaleStatusOrder');
        UnRegisterModuleDependences('sale', 'OnBeforeOrderAdd', $this->MODULE_ID, 'MeasoftEvents', 'OnBeforeOrderAdd');

        // Удаление свойств заказа из БД
        CModule::IncludeModule("sale");
        if ($prop = CSaleOrderProps::GetList(array(), array('CODE' => 'MEASOFT_DATE_PUTN'))->Fetch()) {
            CSaleOrderProps::Delete($prop['ID']);
        }
        if ($prop = CSaleOrderProps::GetList(array(), array('CODE' => 'MEASOFT_TIME_MIN'))->Fetch()) {
            CSaleOrderProps::Delete($prop['ID']);
        }
        if ($prop = CSaleOrderProps::GetList(array(), array('CODE' => 'MEASOFT_TIME_MAX'))->Fetch()) {
            CSaleOrderProps::Delete($prop['ID']);
        }

        // Разрегистрация модуля
        UnRegisterModule($this->MODULE_ID);

        // Удаление скопированных в компоненты файлов при установке
        if (is_dir($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/measoft.courier/install/components/measoft.courier")) {
                DeleteDirFilesEx("/bitrix/components/measoft.courier");
        }
    }
}
?>