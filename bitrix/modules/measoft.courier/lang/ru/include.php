<?

    $MESS["MEASOFT_MODULE_NOT_INSTALL"] = "Модуль \"Курьерская служба\" не настроен";
    $MESS["MEASOFT_HANDLER_NAME"] = "\"Курьерская служба\" - сервис доставки";
    $MESS["MEASOFT_HANDLER_DESCRIPTION"] = "Сервис доставки \"Курьерская служба\" позволяет использовать настройки службы доставки";
    $MESS["MEASOFT_HANDLER_DESCRIPTION_INNER"] = "Уважаемые пользователи!<br>Убедительная просьба внимательно проверять настройки интеграции!";

    $MESS["MEASOFT_HANDLER_GROUP_GENERAL"] = "Настройки интеграции";
    $MESS["MEASOFT_HANDLER_GROUP_DELIVERY_PRICE_POLICY"] = 'Ценовая политика доставки';

    $MESS["MEASOFT_CONFIG_SECTION_PRICE_INTERVAL"] = "Ценовой интервал";
    $MESS["MEASOFT_CONFIG_SECTION_AUTH"] = "Настройки доступа";
    $MESS["MEASOFT_CONFIG_SECTION_PROPS"] = 'Основные настройки';
    $MESS["MEASOFT_CONFIG_SECTION_FIELDS"] = 'Соответствующие поля покупателя';
    $MESS["MEASOFT_CONFIG_SECTION_PRICE_INTERVAL"] = 'Ценовой интервал';

    $MESS["MEASOFT_CONFIG_CLIENT_LOGIN"] = "Логин";
    $MESS["MEASOFT_CONFIG_CLIENT_PASSWORD"] = "Пароль";
    $MESS["MEASOFT_CONFIG_CLIENT_CODE"] = "Код клиента";
    $MESS["MEASOFT_CONFIG_USE_ARTICLES"] = "Использовать артикулы";
    $MESS["MEASOFT_CONFIG_SEND_STATUS"] = "Статус отправки заказа";
    $MESS["MEASOFT_CONFIG_RISE_TYPE"] = "Тип надбавки";
    $MESS["MEASOFT_CONFIG_RISE_SUM"] = "Величина надбавки";
    $MESS["MEASOFT_CONFIG_PROP_FIO"] = "Поле ФИО";
    $MESS["MEASOFT_CONFIG_PROP_CITY"] = "Поле города";
    $MESS["MEASOFT_CONFIG_PROP_ADDRESS"] = "Поле адреса";
    $MESS["MEASOFT_CONFIG_PROP_PHONE"] = "Поле телефона";
    $MESS["MEASOFT_CONFIG_ORDER_MIN_PRICE"] = 'Минимальная цена заказа';
    $MESS["MEASOFT_CONFIG_ORDER_MAX_PRICE"] = 'Максимальная цена заказа';
    $MESS["MEASOFT_CONFIG_OPTION_CLIENT_PAY_ALL"] = 'Клиент оплачивает все';
    $MESS["MEASOFT_CONFIG_OPTION_MARKET_PAY_ALL"] = 'Магазин оплачивает все';
    $MESS["MEASOFT_CONFIG_OPTION_MARKET_PAY_PRESENT"] = 'Магазин оплачивает % от стоимости доставки';
    $MESS["MEASOFT_CONFIG_OPTION_MARKET_PAY_RUB"] = 'Магазин оплачивает руб. от стоимости доставки';
    $MESS["MEASOFT_CONFIG_SUMM"] = 'Сумма';

    $MESS['MEASOFT_FIELDS_DATE_PUTN'] = "Желаемая дата доставки в формате ДД.ММ.ГГГГ";
    $MESS['MEASOFT_FIELDS_TIME_MIN'] = "Желаемое минимальное время доставки в формате ЧЧ:ММ";
    $MESS['MEASOFT_FIELDS_TIME_MAX'] = "Желаемое максимальное время доставки в формате ЧЧ:ММ";


    $MESS['MEASOFT_MESSAGES_SELECT'] = "Выбрать";
    $MESS['MEASOFT_MESSAGES_BASKET_EMPTY'] = 'Корзина в сессии пуста';

    $MESS["MEASOFT_ERROR_CONFIG"] = "Неправильные настройки модуля";
    $MESS["MEASOFT_ERROR_STATUS"] = "Неверный статус для отправки заказа";
    $MESS["MEASOFT_ERROR_ORDER_SEND"] = "Ошибка отправки заказа";

    $MESS["MEASOFT_ERROR_PHONE"] = "";
    $MESS["MEASOFT_ERROR_DATE_PUTN"] = "";
    $MESS["MEASOFT_ERROR_TIME_MIN"] = "";
    $MESS["MEASOFT_ERROR_TIME_MAX"] = "";
    $MESS["MEASOFT_ERROR_TIME_MIN_TIME_MAX"] = "";
    $MESS["MEASOFT_ERROR_"] = "";
    $MESS["MEASOFT_ERROR_"] = "";
    $MESS["MEASOFT_ERROR_"] = "";
    $MESS["MEASOFT_ERROR_"] = "";


?>