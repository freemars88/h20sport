<?

$MESS['MEASOFT_MODULE_NAME'] = "Курьерская служба";
$MESS['MEASOFT_MODULE_DESCRIPTION'] = "Модуль доставки \"Курьерская служба\"";
$MESS['MEASOFT_NAME'] = "Measoft";

$MESS['MEASOFT_MODULE_INSTALLED_OK'] = 'Настроить и активировать модуль вы можете <a href="/bitrix/admin/sale_delivery_handler_edit.php?SID=measoft&lang=ru">этой странице</a> или в разделе Магазин->Настройки->Службы доставки->Автоматизированные->Measoft';
$MESS['MEASOFT_MODULE_UNINSTALLED_OK'] = 'Всё удалено!';
$MESS['MEASOFT_MODULE_ERROR'] = "Ошибка";

?>