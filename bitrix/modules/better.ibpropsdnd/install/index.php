<?
IncludeModuleLangFile(__FILE__);
Class better_ibpropsdnd extends CModule
{
	const MODULE_ID = 'better.ibpropsdnd';
	var $MODULE_ID = 'better.ibpropsdnd'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("better.ibpropsdnd_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("better.ibpropsdnd_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("better.ibpropsdnd_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("better.ibpropsdnd_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CBetterIbpropsdnd', 'OnBuildGlobalMenu');
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CBetterIbpropsdnd', 'OnBuildGlobalMenu');
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/better.ibpropsdnd/install/scripts",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/better/ibpropsdnd",
            true, true);
		return true;
	}

	function UnInstallFiles()
	{
        DeleteDirFilesEx("/bitrix/js/better/ibpropsdnd");	    
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
?>
