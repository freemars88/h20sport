<?
if ($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_edit.php")
{
//    $GLOBALS['APPLICATION']->AddHeadScript('https://raw.github.com/carhartl/jquery-cookie/master/jquery.cookie.js');
    $GLOBALS['APPLICATION']->AddHeadScript('//code.jquery.com/jquery-2.2.4.min.js');
    $GLOBALS['APPLICATION']->AddHeadScript('/bitrix/js/better/ibpropsdnd/jquery.tinysort.min.js');
    $GLOBALS['APPLICATION']->AddHeadScript("/bitrix/js/better/ibpropsdnd/tdnd.js");
    $GLOBALS['APPLICATION']->AddHeadScript("/bitrix/js/better/ibpropsdnd/main.js");
    $GLOBALS['APPLICATION']->AddHeadString('<link href="/bitrix/js/better/ibpropsdnd/trowdnd.css" type="text/css" rel="stylesheet">');
if (intval($_COOKIE["better-dnd-sort-step"]) > 0)
    $GLOBALS['APPLICATION']->AddHeadString('<script>var betterDndSortStep = '.$_COOKIE["better-dnd-sort-step"].'</script>');
    //$GLOBALS['APPLICATION']->AddHeadString("<script>alert('f');</script>");
}

Class CBetterIbpropsdnd 
{
	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		$MODULE_ID = basename(dirname(__FILE__));
		$aMenu = array(
			//"parent_menu" => "global_menu_services",
			"parent_menu" => "global_menu_settings",
			"section" => $MODULE_ID,
			"sort" => 50,
			"text" => $MODULE_ID,
			"title" => '',
			"url" => "partner_modules.php?module=".$MODULE_ID,
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID."_items",
			"more_url" => array(),
			"items" => array()
		);

		if (file_exists($path = dirname(__FILE__).'/admin'))
		{
			if ($dir = opendir($path))
			{
				$arFiles = array();

				while(false !== $item = readdir($dir))
				{
					if (in_array($item,array('.','..','menu.php')))
						continue;

					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$MODULE_ID.'_'.$item))
						file_put_contents($file,'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.$MODULE_ID.'/admin/'.$item.'");?'.'>');

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach($arFiles as $item)
					$aMenu['items'][] = array(
						'text' => $item,
						'url' => $MODULE_ID.'_'.$item,
						'module_id' => $MODULE_ID,
						"title" => "",
					);
			}
		}
		//$aModuleMenu[] = $aMenu;
	}
}
?>
