<?

use Bitrix\Main\Localization\Loc;

if (CAmminaSmtp::getTestPeriodInfo() == \Bitrix\Main\Loader::MODULE_DEMO) {
	CAdminMessage::ShowMessage(array("MESSAGE" => Loc::getMessage("AMMINA_SMTP_SYS_MODULE_IS_DEMO"), "HTML" => true));
} elseif (CAmminaSmtp::getTestPeriodInfo() == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED) {
	CAdminMessage::ShowMessage(array("MESSAGE" => Loc::getMessage("AMMINA_SMTP_SYS_MODULE_IS_DEMO_EXPIRED"), "HTML" => true));
}

$module_id = "ammina.smtp";
$modulePermissions = $APPLICATION->GetGroupRight($module_id);
if ($modulePermissions >= "R") {

	global $MESS;
	IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/options.php");
	IncludeModuleLangFile(__FILE__);

	CModule::IncludeModule($module_id);

	if ($REQUEST_METHOD == "GET" && strlen($RestoreDefaults) > 0 && $modulePermissions >= "W" && check_bitrix_sessid()) {
		COption::RemoveOption($module_id);
		$z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
		while ($zr = $z->Fetch()) {
			$APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
		}
	}

	$arAllOptions = array(
		array("activate_module", GetMessage("ammina.smtp_OPTION_ACTIVATE_MODULE"), "N", Array("checkbox")),
		array("log", GetMessage("ammina.smtp_OPTION_LOG"), "N", Array("checkbox")),
		array("timeout", GetMessage("ammina.smtp_OPTION_TIMEOUT"), "30", Array("text", 10)),
	);

	$strWarning = "";
	if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $modulePermissions == "W" && check_bitrix_sessid()) {
		foreach ($arAllOptions as $option) {
			$name = $option[0];
			$val = $$name;
			if ($option[3][0] == "checkbox" && $val != "Y")
				$val = "N";
			COption::SetOptionString($module_id, $name, $val, $option[1]);
		}
	}

	if (strlen($strWarning) > 0)
		CAdminMessage::ShowMessage($strWarning);

	$aTabs = array();
	$aTabs[] = array(
		'DIV' => 'edit1',
		'TAB' => GetMessage('ammina.smtp_TAB_SETTINGS_TITLE'),
		'TITLE' => GetMessage('ammina.smtp_TAB_SETTINGS_DESC'),
	);
	$aTabs[] = array(
		'DIV' => 'edit2',
		'TAB' => GetMessage('ammina.smtp_TAB_SUPPORT_TITLE'),
		'TITLE' => GetMessage('ammina.smtp_TAB_SUPPORT_DESC'),
	);
	$aTabs[] = array(
		'DIV' => 'editrights',
		'TAB' => GetMessage('ammina.smtp_TAB_RIGHTS_TITLE'),
		'TITLE' => GetMessage('ammina.smtp_TAB_RIGHTS_DESC'),
	);
	$tabControl = new CAdminTabControl('tabControl', $aTabs);

	$tabControl->Begin();
	?>
	<form method="POST" action="<?
	echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANGUAGE_ID ?>"><?
		bitrix_sessid_post();
		$tabControl->BeginNextTab();
		foreach ($arAllOptions as $Option) {
			$val = COption::GetOptionString($module_id, $Option[0], $Option[2]);
			$type = $Option[3];
			?>
			<tr>
				<td valign="top" width="50%"><?
					if ($type[0] == "checkbox")
						echo "<label for=\"" . htmlspecialcharsbx($Option[0]) . "\">" . $Option[1] . "</label>";
					else
						echo $Option[1];
					?></td>
				<td valign="middle" width="50%">
					<?
					if ($type[0] == "checkbox") {
						?>
						<input type="checkbox" name="<?
						echo htmlspecialcharsbx($Option[0]) ?>" id="<?
						echo htmlspecialcharsbx($Option[0]) ?>" value="Y"<?
						if ($val == "Y") echo " checked"; ?>>
					<? } elseif ($type[0] == "text") {
						?>
						<input type="text" size="<?
						echo $type[1] ?>" value="<?
						echo htmlspecialcharsbx($val) ?>" name="<?
						echo htmlspecialcharsbx($Option[0]) ?>">
					<? } elseif ($type[0] == "textarea") {
						?>
						<textarea rows="<?
						echo $type[1] ?>" cols="<?
						echo $type[2] ?>" name="<?
						echo htmlspecialcharsbx($Option[0]) ?>"><?
							echo htmlspecialcharsbx($val) ?></textarea>
					<? } elseif ($type[0] == "selectbox") {
						?>
						<select name="<?
						echo htmlspecialcharsbx($Option[0]) ?>" id="<?
						echo htmlspecialcharsbx($Option[0]) ?>">
							<?
							foreach ($Option[4] as $v => $k) {
								?>
								<option value="<?= $v ?>"<?
								if ($val == $v) echo " selected"; ?>><?= $k ?></option><?
							}
							?>
						</select>
					<? } ?>
				</td>
			</tr>
			<?
		}
		$tabControl->BeginNextTab();
		?>
		<tr>
			<td>
				<? echo GetMessage("ammina.smtp_TAB_SUPPORT_CONTENT"); ?>
			</td>
		</tr>
		<?
		$tabControl->BeginNextTab();
		require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?>
		<?
		$tabControl->Buttons(); ?>
		<script language="JavaScript">
			function RestoreDefaults() {
				if (confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))
					window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid) . "&" . bitrix_sessid_get();?>";
			}
		</script>

		<input type="submit" <?
		if ($modulePermissions < "W") echo "disabled" ?> name="Update" value="<?= GetMessage("MAIN_SAVE") ?>">
		<input type="hidden" name="Update" value="Y">
		<input type="reset" name="reset" value="<?= GetMessage("MAIN_RESET") ?>">
		<input type="button" <?
		if ($modulePermissions < "W") echo "disabled" ?> title="<?= GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" onclick="RestoreDefaults();" value="<?= GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
		<?
		$tabControl->End();
		?>
	</form>
	<?
}
