<?
IncludeModuleLangFile(__FILE__);

if ($APPLICATION->GetGroupRight("blog") >= "R") {
	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "ammina.smtp",
		"sort" => 10000,
		"text" => GetMessage("AMMINA_SMTP_MENU_TEXT"),
		"title" => GetMessage("AMMINA_SMTP_MENU_TITLE"),
		"icon" => "ammina_smtp_menu_icon",
		"page_icon" => "ammina_smtp_page_icon",
		"items_id" => "menu_ammina_smtp",
		"items" => array(
			array(
				"text" => GetMessage("AMMINA_SMTP_MENU_DOMAINS_TEXT"),
				"title" => GetMessage("AMMINA_SMTP_MENU_DOMAINS_TITLE"),
				"url" => "ammina.smtp.domains.php?lang=" . LANGUAGE_ID,
				"more_url" => array("ammina.smtp.domains.edit.php"),
			),
			array(
				"text" => GetMessage("AMMINA_SMTP_MENU_ACCOUNTS_TEXT"),
				"title" => GetMessage("AMMINA_SMTP_MENU_ACCOUNTS_TITLE"),
				"url" => "ammina.smtp.accounts.php?lang=" . LANGUAGE_ID,
				"more_url" => array("ammina.smtp.accounts.edit.php"),
			),
		),
	);

	return $aMenu;
}
return false;
