<?

use Bitrix\Main\Localization\Loc;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
Bitrix\Main\Loader::includeModule('ammina.smtp');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/ammina.smtp/prolog.php");

Loc::loadMessages(__FILE__);

$modulePermissions = $APPLICATION->GetGroupRight("ammina.smtp");
if ($modulePermissions < "W") {
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

if (CAmminaSmtp::getTestPeriodInfo() == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED) {
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(array("MESSAGE" => Loc::getMessage("AMMINA_SMTP_SYS_MODULE_IS_DEMO_EXPIRED"), "HTML" => true));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}

$sTableID = "tbl_ammina_smtp_accounts";

$oSort = new CAdminSorting($sTableID, "EMAIL", "asc");
$arOrder = (strtoupper($by) === "ID" ? array($by => $order) : array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminUiList($sTableID, $oSort);

$arFilterFields = Array(
	"find_id",
	"find_email",
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array(
	"ID" => $find_id,
	"?EMAIL" => $find_email,
);

$queryObject = CLang::getList($b = "sort", $o = "asc", array("VISIBLE" => "Y"));
$listLang = array();
while ($lang = $queryObject->getNext())
	$listLang[$lang["LID"]] = $lang["NAME"];

$filterFields = array(
	array(
		"id" => "EMAIL",
		"name" => Loc::getMessage("AMMINA_SMTP_FILTER_EMAIL"),
		"filterable" => "?",
		"quickSearch" => "?",
		"default" => true,
	),
);

$arFilter = array();
$lAdmin->AddFilter($filterFields, $arFilter);

if (($arID = $lAdmin->GroupAction()) && $modulePermissions >= "W") {
	if ($_REQUEST['action_target'] == 'selected') {
		$arID = Array();
		$dbResultList = \Ammina\SMTP\AccountsTable::getList(array(
			"order" => $arOrder,
			"filter" => $arFilter,
			"select" => array("ID")));
		while ($arResult = $dbResultList->Fetch()) {
			$arID[] = $arResult['ID'];
		}
	}

	foreach ($arID as $ID) {
		if (strlen($ID) <= 0) {
			continue;
		}

		switch ($_REQUEST['action']) {
			case "delete":
				@set_time_limit(0);
				$rAccount = \Ammina\SMTP\AccountsTable::getList(array(
					"filter" => array("ID" => $ID),
					"select" => array("ID"),
				));
				$arAccountOld = $rAccount->Fetch();
				$DB->StartTransaction();
				$rOperation = \Ammina\SMTP\AccountsTable::delete($ID);
				if (!$rOperation->isSuccess()) {
					$DB->Rollback();
					if ($ex = $APPLICATION->GetException()) {
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					} else {
						$lAdmin->AddGroupError(Loc::getMessage("AMMINA_SMTP_DELETE_ERROR"), $ID);
					}
				}
				$DB->Commit();
				break;
		}
	}
}

$arHeader = array(
	array(
		"id" => "DOMAIN",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_DOMAIN"),
		"sort" => "DOMAIN_AREA_DOMAIN",
		"default" => true,
	),
	array(
		"id" => "IS_DEFAULT_DOMAIN",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_IS_DEFAULT_DOMAIN"),
		"sort" => "IS_DEFAULT_DOMAIN",
		"default" => true,
	),
	array(
		"id" => "ACTIVE",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_ACTIVE"),
		"sort" => "ACTIVE",
		"default" => true,
	),
	array(
		"id" => "IS_DEFAULT",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_IS_DEFAULT"),
		"sort" => "IS_DEFAULT",
		"default" => true,
	),
	array(
		"id" => "SMTP_HOST",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_SMTP_HOST"),
		"sort" => "SMTP_HOST",
		"default" => true,
	),
	array(
		"id" => "SMTP_PORT",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_SMTP_PORT"),
		"sort" => "SMTP_PORT",
		"default" => true,
	),
	array(
		"id" => "SECURE_TYPE",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_SECURE_TYPE"),
		"sort" => "SECURE_TYPE",
		"default" => true,
	),
	array(
		"id" => "EMAIL",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_EMAIL"),
		"sort" => "EMAIL",
		"default" => true,
	),
	array(
		"id" => "SENDER_NAME",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_SENDER_NAME"),
		"sort" => "SENDER_NAME",
		"default" => true,
	),
	array(
		"id" => "ACCOUNT_LOGIN",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_ACCOUNT_LOGIN"),
		"sort" => "ACCOUNT_LOGIN",
		"default" => true,
	),
	array(
		"id" => "ACCOUNT_PASSWORD",
		"content" => Loc::getMessage("AMMINA_SMTP_FIELD_ACCOUNT_PASSWORD"),
		"sort" => "ACCOUNT_PASSWORD",
		"default" => false,
	),
);

$lAdmin->AddHeaders($arHeader);

$rsItems = \Ammina\SMTP\AccountsTable::getList(array(
	"order" => $arOrder,
	"filter" => $arFilter,
	"select" => array("*", "DOMAIN_AREA_" => "DOMAIN")));
$rsItems = new CAdminUiResult($rsItems, $sTableID);
$rsItems->NavStart();

$lAdmin->SetNavigationParams($rsItems);

while ($dbrs = $rsItems->NavNext(true, "f_")) {
	$row =& $lAdmin->AddRow($f_ID, $dbrs, 'ammina.smtp.accounts.edit.php?ID=' . $f_ID . '&lang=' . LANGUAGE_ID, Loc::getMessage("AMMINA_SMTP_RECORD_EDIT"));
	$row->AddViewField("DOMAIN", '[<a href="/bitrix/admin/ammina.smtp.domains.edit.php?ID=' . $f_DOMAIN_ID . '">' . $f_DOMAIN_ID . '</a>] ' . $f_DOMAIN_AREA_DOMAIN);
	$row->AddCheckField("IS_DEFAULT_DOMAIN");
	$row->AddCheckField("ACTIVE");
	$row->AddCheckField("IS_DEFAULT");
	$arActions = array();

	if ($modulePermissions >= "W") {
		$arActions[] = array(
			"ICON" => "edit",
			"TEXT" => Loc::getMessage("MAIN_ADMIN_MENU_EDIT"),
			"DEFAULT" => true,
			"ACTION" => $lAdmin->ActionRedirect("ammina.smtp.accounts.edit.php?ID=" . $f_ID . "&lang=" . LANGUAGE_ID),
		);
		$arActions[] = array(
			"SEPARATOR" => true,
		);
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("AMMINA_SMTP_ACTION_DELETE"),
			"ACTION" => "if(confirm('" . GetMessage('AMMINA_SMTP_ACTION_DELETE_CONFIRM') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete"),
		);
	}

	if (count($arActions) > 0) {
		$row->AddActions($arActions);
	}
}

$lAdmin->AddFooter(
	array(
		array("title" => Loc::getMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsItems->SelectedRowsCount()),
		array("counter" => true, "title" => Loc::getMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"),
	)
);

if ($modulePermissions >= "W") {
	$aContext = array(
		array(
			"ICON" => "btn_new",
			"TEXT" => Loc::getMessage("AMMINA_SMTP_TO_ADD_DOMAIN"),
			"LINK" => "ammina.smtp.accounts.edit.php?lang=" . LANGUAGE_ID,
			"TITLE" => Loc::getMessage("AMMINA_SMTP_TO_ADD_DOMAIN_TITLE"),
		),
	);

	$lAdmin->AddAdminContextMenu($aContext);

	$lAdmin->AddGroupActionTable(Array());
}

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage("AMMINA_SMTP_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayFilter($filterFields);

if (CAmminaSmtp::getTestPeriodInfo() == \Bitrix\Main\Loader::MODULE_DEMO) {
	CAdminMessage::ShowMessage(array("MESSAGE" => Loc::getMessage("AMMINA_SMTP_SYS_MODULE_IS_DEMO"), "HTML" => true));
} elseif (CAmminaSmtp::getTestPeriodInfo() == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED) {
	CAdminMessage::ShowMessage(array("MESSAGE" => Loc::getMessage("AMMINA_SMTP_SYS_MODULE_IS_DEMO_EXPIRED"), "HTML" => true));
}

$lAdmin->DisplayList();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");