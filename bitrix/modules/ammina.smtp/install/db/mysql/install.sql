CREATE TABLE IF NOT EXISTS `am_smtp_accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOMAIN_ID` int(11) NOT NULL,
  `ACTIVE` char(1) NOT NULL DEFAULT 'Y',
  `IS_DEFAULT_DOMAIN` char(1) NOT NULL DEFAULT 'N',
  `IS_DEFAULT` char(1) NOT NULL DEFAULT 'N',
  `SMTP_HOST` varchar(255) NOT NULL,
  `SMTP_PORT` int(11) NOT NULL DEFAULT '25',
  `SECURE_TYPE` char(1) NOT NULL DEFAULT 'N',
  `EMAIL` varchar(255) NOT NULL,
  `SENDER_NAME` varchar(255) DEFAULT NULL,
  `ACCOUNT_LOGIN` varchar(255) DEFAULT NULL,
  `ACCOUNT_PASSWORD` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `am_smtp_accounts_DOMAIN_ID_index` (`DOMAIN_ID`)
) COMMENT='Ammina SMTP: Accounts';

CREATE TABLE IF NOT EXISTS `am_smtp_domains` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOMAIN` varchar(255) NOT NULL,
  `DKIM_PRIVATE` text COLLATE utf8_unicode_ci,
  `DKIM_PUBLIC` text COLLATE utf8_unicode_ci,
  `DKIM_SELECTOR` varchar(255) DEFAULT NULL,
  `DKIM_PASSPHRASE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `amsm_domains_DOMAIN_uindex` (`DOMAIN`)
) COMMENT='Ammins.smtp: Domains';
