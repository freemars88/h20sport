<?

IncludeModuleLangFile(__FILE__);

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client_partner.php");

class ammina_smtp extends CModule
{

	const MODULE_ID = 'ammina.smtp';

	var $MODULE_ID = 'ammina.smtp';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("ammina.smtp_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("ammina.smtp_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("ammina.smtp_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("ammina.smtp_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/db/" . $DBType . "/install.sql");
		if (!empty($errors)) {
			$APPLICATION->ThrowException(implode("", $errors));
			return false;
		}
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CAmminaSmtp', 'OnBuildGlobalMenu');
		RegisterModuleDependences('main', 'OnPageStart', self::MODULE_ID, 'CAmminaSmtp', 'OnPageStart');
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		if (array_key_exists("savedata", $arParams) && $arParams["savedata"] != "Y") {
			$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/db/" . $DBType . "/uninstall.sql");

			if (!empty($errors)) {
				$APPLICATION->ThrowException(implode("", $errors));
				return false;
			}

		}
		UnRegisterModuleDependences('main', 'OnPageStart', self::MODULE_ID, 'CAmminaSmtp', 'OnPageStart');
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CAmminaSmtp', 'OnBuildGlobalMenu');
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/", true, true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION, $step;
		CJSCore::Init(array("jquery2"));
		$step = intval($step);
		if ($step < 2) {
			$APPLICATION->IncludeAdminFile(GetMessage("ammina.smtp_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/step1.php");
		} elseif ($step == 2) {
			if (strlen($_REQUEST['AFIELDS']['NAME']) <= 0 || strlen($_REQUEST['AFIELDS']['EMAIL']) <= 0 || strlen($_REQUEST['AFIELDS']['PHONE']) <= 0) {
				LocalRedirect($APPLICATION->GetCurPageParam("lang=" . LANG . "&install=Y&id=" . self::MODULE_ID));
			}
			$this->doSendRegData();
			$this->InstallFiles();
			$this->InstallDB();
			RegisterModule(self::MODULE_ID);
			$GLOBALS["errors"] = $this->errors;

			$APPLICATION->IncludeAdminFile(GetMessage("ammina.smtp_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/step2.php");
		}
	}

	function DoUninstall()
	{
		global $APPLICATION, $step;
		$step = IntVal($step);
		if ($step < 2) {
			$APPLICATION->IncludeAdminFile(GetMessage("ammina.smtp_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/unstep1.php");
		} elseif ($step == 2) {
			UnRegisterModule(self::MODULE_ID);
			$this->UnInstallDB(array(
				"savedata" => $_REQUEST["savedata"],
			));
			$this->UnInstallFiles();
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("ammina.smtp_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/unstep2.php");
		}
	}

	function doSendRegData()
	{
		$strHost = $_SERVER['SERVER_NAME'];
		if (strlen($strHost) <= 0) {
			$strHost = $_SERVER['HTTP_HOST'];
		}
		$strError = "";
		$arClient = CUpdateClient::GetUpdatesList($strError);
		$arFields = array(
			"LEAD_NAME" => $_REQUEST['AFIELDS']['NAME'],
			"LEAD_EMAIL" => $_REQUEST['AFIELDS']['EMAIL'],
			"LEAD_PHONE" => $_REQUEST['AFIELDS']['PHONE'],
			"LEAD_UF_CRM_1551200754705" => md5('BITRIX' . CUpdateClientPartner::GetLicenseKey() . 'LICENSE'),
			"LEAD_UF_CRM_1551200838989" => $arClient['CLIENT'][0]['@']['LICENSE'],
			"LEAD_UF_CRM_1551200902405" => $strHost,
			"LEAD_UF_CRM_1551200882741" => self::MODULE_ID,
			"LEAD_UF_CRM_1551200966047" => $arClient['CLIENT'][0]['@']['DATE_FROM'],
			"LEAD_UF_CRM_1551200977499" => $arClient['CLIENT'][0]['@']['DATE_TO'],
			"from" => $_SERVER['HTTP_REFERER'],
		);
		$strUrl = "https://www.ammina24.ru/pub/form/6_kontaktnye_dannye_po_modulyu_ammina_ip/dyvcqd/?form_code=6_kontaktnye_dannye_po_modulyu_ammina_ip&sec=dyvcqd";
		$oHttpClient = new \Bitrix\Main\Web\HttpClient(array(
			'redirect' => true,
			'redirectMax' => 10,
			'version' => '1.1',
			'disableSslVerification' => true,
			'waitResponse' => true,
			'socketTimeout' => 15,
			'streamTimeout' => 30,
			'charset' => "UTF-8",
		));
		$strResult1 = $oHttpClient->get("https://www.ammina24.ru/pub/form/6_kontaktnye_dannye_po_modulyu_ammina_ip/dyvcqd/");
		$status = $oHttpClient->getStatus();
		if ($status == 200) {
			$s1 = strpos($strResult1, "'bitrix_sessid':'");
			$s2 = strpos($strResult1, "'", $s1 + 17);
			$strSessId = substr($strResult1, $s1 + 17, $s2 - $s1 - 17);
			$arFields['sessid'] = $strSessId;
			$oCookie = $oHttpClient->getCookies();
			$oHttpClient->setCookies($oCookie->toArray());
			$oHttpClient->setHeader('Referer', 'https://www.ammina24.ru/pub/form/6_kontaktnye_dannye_po_modulyu_ammina_ip/dyvcqd/');
			$response = $oHttpClient->post($strUrl, $arFields);
			unset($oHttpClient);
		}

	}
}

?>