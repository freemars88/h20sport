<?

namespace Ammina\SMTP;

use Bitrix\Main\Entity\DataManager;

class DomainsTable extends DataManager
{
	public static function getTableName()
	{
		return 'am_smtp_domains';
	}

	public static function getMap()
	{
		$fieldsMap = array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
			),
			'DOMAIN' => array(
				'data_type' => 'string',
			),
			'DKIM_PRIVATE' => array(
				'data_type' => 'text',
			),
			'DKIM_PUBLIC' => array(
				'data_type' => 'text',
			),
			'DKIM_SELECTOR' => array(
				'data_type' => 'string',
			),
			'DKIM_PASSPHRASE' => array(
				'data_type' => 'string',
			),
		);

		return $fieldsMap;
	}
}