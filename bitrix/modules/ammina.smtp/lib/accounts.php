<?

namespace Ammina\SMTP;

use Bitrix\Main\Entity\DataManager;

class AccountsTable extends DataManager
{
	public static function getTableName()
	{
		return 'am_smtp_accounts';
	}

	public static function getMap()
	{
		$fieldsMap = array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
			),
			'DOMAIN_ID' => array(
				'data_type' => 'integer',
			),
			'DOMAIN' => array(
				'data_type' => '\Ammina\SMTP\Domains',
				'reference' => array('=this.DOMAIN_ID' => 'ref.ID'),
			),
			'IS_DEFAULT_DOMAIN' => array(
				'data_type' => 'enum',
				'values' => array('N', 'Y'),
			),
			'ACTIVE' => array(
				'data_type' => 'enum',
				'values' => array('N', 'Y'),
			),
			'IS_DEFAULT' => array(
				'data_type' => 'enum',
				'values' => array('N', 'Y'),
			),
			'SMTP_HOST' => array(
				'data_type' => 'string',
			),
			'SMTP_PORT' => array(
				'data_type' => 'integer',
			),
			'SECURE_TYPE' => array(
				'data_type' => 'enum',
				'values' => array('N', 'S', 'T'),
			),
			'EMAIL' => array(
				'data_type' => 'string',
			),
			'SENDER_NAME' => array(
				'data_type' => 'string',
			),
			'ACCOUNT_LOGIN' => array(
				'data_type' => 'string',
			),
			'ACCOUNT_PASSWORD' => array(
				'data_type' => 'string',
			),
		);

		return $fieldsMap;
	}

	/**
	 * ���� ������� � �������� ���������� ������, ���� ����� ��� DKIM ������� ��� ��������� ��������
	 * ������� ������:
	 * 1. ���������� ��������  � $strFrom
	 * 2. ������� �� ��������� ��� ������, ������������ � $strFrom
	 * 3. ������� �� ���������
	 * ���� �� ������ �������, �� ���� �����
	 *
	 * ���� �� ������ ���������� �������, �� ���� �����
	 *
	 * @param $strFrom ����� From ������
	 *
	 * @return array|false|mixed
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	static public function getOptimalAccountDomainByFrom($strFrom)
	{
		static $arCacheAccountByFrom = array();
		if (!isset($arCacheAccountByFrom[strtolower($strFrom)])) {
			$arAccount = AccountsTable::getList(array(
				"filter" => array("ACTIVE" => "Y", "EMAIL" => $strFrom),
				"select" => array("*", "DOMAIN_AREA_" => "DOMAIN"),
			))->fetch();
			if (!$arAccount) {
				$arEmail = explode("@", $strFrom);
				$arDomain = DomainsTable::getList(array(
					"filter" => array("DOMAIN" => $arEmail[1]),
				))->fetch();
				if ($arDomain) {
					$arAccount = AccountsTable::getList(array(
						"filter" => array("ACTIVE" => "Y", "IS_DEFAULT_DOMAIN" => "Y", "DOMAIN_ID" => $arDomain['ID']),
						"select" => array("*", "DOMAIN_AREA_" => "DOMAIN"),
					))->fetch();
					if (!$arAccount) {
						$arAccount = AccountsTable::getList(array(
							"filter" => array("ACTIVE" => "Y", "DOMAIN_ID" => $arDomain['ID']),
							"select" => array("*", "DOMAIN_AREA_" => "DOMAIN"),
						))->fetch();
					}
				}
				if (!$arAccount) {
					$arAccount = AccountsTable::getList(array(
						"filter" => array("ACTIVE" => "Y", "IS_DEFAULT" => "Y"),
						"select" => array("*", "DOMAIN_AREA_" => "DOMAIN"),
					))->fetch();
					if (!$arAccount) {
						if ($arDomain) {
							$arAccount = array(
								"IS_DOMAIN" => true,
								"DOMAIN" => $arDomain,
							);
						}
					}
				}
			}
			if ($arAccount) {
				if (!$arAccount['IS_DOMAIN']) {
					$arAccount = array(
						"IS_ACCOUNT" => true,
						"ACCOUNT" => $arAccount,
						"DOMAIN" => DomainsTable::getList(array(
							"filter" => array("ID" => $arAccount['DOMAIN_ID']),
						))->fetch(),
					);
				}
				$arCacheAccountByFrom[strtolower($strFrom)] = $arAccount;
			}
		}
		$arAccount = $arCacheAccountByFrom[strtolower($strFrom)];
		return $arAccount;
	}
}