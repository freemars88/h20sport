<?

namespace Ammina\SMTP;

class Parser
{
	protected $strOriginalTo;
	protected $strOriginalSubject;
	protected $strOriginalMessage;
	protected $strOriginalAdditionalHeaders;
	protected $strOriginalAdditionalParameters;
	protected $strCurrentPartHeaderCharset = SITE_CHARSET;
	protected $strCurrentPartHeaderContentType = "";
	protected $strCurrentPartHeaderName = "";
	protected $strCurrentPartHeaderBoundary = "";
	public $strFieldTo = "";
	public $strFieldToName = "";
	public $strFieldSubject = "";
	public $arFieldsHeaders = array();
	public $arContents = array();
	public $strHeaderCharset = SITE_CHARSET;
	public $strHeaderContentType = "";
	public $strHeaderBoundary = "";
	public $strFieldFrom = "";
	public $strFieldFromName = "";
	public $strMessage = "";
	public $strAltMessage = "";
	public $bIsHtmlMessage = false;
	public $arAttachment = array();

	function __construct($to, $subject, $message, $additionalHeaders = '', $additionalParameters = '')
	{
		$this->strOriginalTo = $to;
		$this->strOriginalSubject = $subject;
		$this->strOriginalMessage = $message;
		$this->strOriginalAdditionalHeaders = $additionalHeaders;
		$this->strOriginalAdditionalParameters = $additionalParameters;
	}

	public function doParse()
	{
		list($this->strFieldTo, $this->strFieldToName) = $this->doNormalizeAndParseEmail($this->strOriginalTo);
		$this->strFieldSubject = $this->doNormalizeAndParseString($this->strOriginalSubject);
		$this->arFieldsHeaders = $this->doNormalizeAndParseHeaders($this->strOriginalAdditionalHeaders);
		$this->doParseMessage();
	}

	protected function doNormalizeAndParseString($strValue)
	{
		if (preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $strValue)) {
			$strValue = iconv_mime_decode($strValue, ICONV_MIME_DECODE_CONTINUE_ON_ERROR);
		}
		return $strValue;
	}

	protected function doNormalizeAndParseEmail($strValue)
	{
		if (preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $strValue)) {
			$strValue = iconv_mime_decode($strValue, ICONV_MIME_DECODE_CONTINUE_ON_ERROR);
		}
		$strEmail = $strValue;
		$strName = "";
		if (strpos($strValue, '<') !== false) {
			$strName = trim(substr($strValue, 0, strpos($strValue, '<')));
			$strValue = substr($strValue, strpos($strValue, '<') + 1);
			$strEmail = substr($strValue, 0, strpos($strValue, ">"));
		}
		return array($strEmail, $strName);
	}

	protected function doNormalizeAndParseHeaders($strHeaders)
	{
		$strHeaders = str_replace(["\r\n", "\r"], "\n", $strHeaders);
		$arHeaders = explode("\n", $strHeaders);
		foreach ($arHeaders as $strLine) {
			$arLine = explode(": ", $strLine);
			if (trim(strtolower($arLine[0])) == "content-type") {
				unset($arLine[0]);
				$strLine = implode(": ", $arLine);
				$arLine = explode("; ", $strLine);
				$this->strHeaderContentType = $arLine[0];
				foreach ($arLine as $strPart) {
					if (strpos(strtolower($strPart), "charset=") === 0) {
						$this->strHeaderCharset = substr($strPart, 8);
					} elseif (strpos(strtolower($strPart), "boundary=\"") === 0) {
						$this->strHeaderBoundary = substr($strPart, 10, strlen($strPart) - 11);
					}
				}
				break;
			}
		}
		if (!preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $strHeaders)) {
			$arData = explode("\n", $strHeaders);
			$arNormalizeLines = array();
			$strCurrentField = "";
			foreach ($arData as $val) {
				if (strpos($val, ": ") === false) {
					$strCurrentField .= $val;
				} else {
					if (strlen($strCurrentField) > 0) {
						$arNormalizeLines[] = $strCurrentField;
					}
					$strCurrentField = $val;
				}
			}
			if (strlen($strCurrentField) > 0) {
				$arNormalizeLines[] = $strCurrentField;
			}
			foreach ($arNormalizeLines as $k => $v) {
				$arVal = explode(": ", $v);
				$strName = $arVal[0];
				unset($arVal[0]);
				$strValue = implode(": ", $arVal);
				$arNormalizeLines[$k] = iconv_mime_encode($strName, $strValue);
			}
			$strHeaders = implode("\n", $arNormalizeLines);
		}
		$arHeadersOriginal = iconv_mime_decode_headers($strHeaders, ICONV_MIME_DECODE_CONTINUE_ON_ERROR, $this->strHeaderCharset);
		$arHeaders = array();
		$strFrom = "";
		foreach ($arHeadersOriginal as $k => $v) {
			$arHeaders[strtolower($k)] = array(
				"NAME" => $k,
				"VALUE" => $v,
			);
			if (strtolower($k) == "from") {
				$strFrom = $v;
			}
		}
		if (strlen($strFrom) <= 0) {
			$arSite = \CSite::GetArrayByID(SITE_ID);
			if ($arSite) {
				$strFrom = $arSite['EMAIL'];
			} else {
				$strFrom = \COption::GetOptionString("main", "email_from", $strFrom);
			}
		}
		$this->strFieldFrom = $strFrom;
		if (strpos($strFrom, '<') !== false) {
			$this->strFieldFromName = trim(substr($strFrom, 0, strpos($strFrom, '<')));
			$strFrom = substr($strFrom, strpos($strFrom, '<') + 1);
			$this->strFieldFrom = substr($strFrom, 0, strpos($strFrom, ">"));
		}
		return $arHeaders;
	}

	protected function doParseMessage()
	{
		if (strpos($this->strHeaderContentType, "multipart/") === 0) {
			if ($this->strHeaderContentType == "multipart/alternative") {
				$arContent = explode("--" . $this->strHeaderBoundary, $this->strOriginalMessage);
				foreach ($arContent as $k => $v) {
					if ($k > 0 && $k < (count($arContent) - 1)) {
						$this->strCurrentPartHeaderContentType = "";
						$this->strCurrentPartHeaderName = "";
						$this->strCurrentPartHeaderCharset = SITE_CHARSET;
						$this->strCurrentPartHeaderBoundary = "";
						list($arPartHeader, $strPartContent) = $this->doParseMessagePart(trim($v));
						if ($this->strCurrentPartHeaderContentType == "text/plain") {
							$this->strAltMessage = $strPartContent;
						} elseif ($this->strCurrentPartHeaderContentType == "text/html") {
							$this->strMessage = $strPartContent;
							$this->bIsHtmlMessage = true;
						}
					}
				}
			} elseif ($this->strHeaderContentType == "multipart/mixed") {
				$arContent = explode("--" . $this->strHeaderBoundary, $this->strOriginalMessage);
				foreach ($arContent as $k => $v) {
					if ($k > 0 && $k < (count($arContent) - 1)) {
						$this->strCurrentPartHeaderContentType = "";
						$this->strCurrentPartHeaderName = "";
						$this->strCurrentPartHeaderCharset = SITE_CHARSET;
						$this->strCurrentPartHeaderBoundary = "";
						list($arPartHeader, $strPartContent) = $this->doParseMessagePart(trim($v));
						if ($this->strCurrentPartHeaderContentType == "multipart/alternative") {
							$arChildContent = explode("--" . $this->strCurrentPartHeaderBoundary, $strPartContent);
							foreach ($arChildContent as $k1 => $v1) {
								if ($k1 > 0 && $k1 < (count($arChildContent) - 1)) {
									$this->strCurrentPartHeaderContentType = "";
									$this->strCurrentPartHeaderName = "";
									$this->strCurrentPartHeaderCharset = SITE_CHARSET;
									$this->strCurrentPartHeaderBoundary = "";
									list($arChildPartHeader, $strChildPartContent) = $this->doParseMessagePart(trim($v1));
									if ($this->strCurrentPartHeaderContentType == "text/plain") {
										$this->strAltMessage = $strChildPartContent;
									} elseif ($this->strCurrentPartHeaderContentType == "text/html") {
										$this->strMessage = $strChildPartContent;
										$this->bIsHtmlMessage = true;
									}
								}
							}
						} elseif (strlen($this->strCurrentPartHeaderName) > 0) {
							if ($arPartHeader['content-transfer-encoding']['VALUE'] == "base64") {
								$strPartContent = base64_decode($strPartContent);
							}
							$this->arAttachment[] = array(
								"headers" => $arPartHeader,
								"content-type" => $this->strCurrentPartHeaderContentType,
								"name" => $this->strCurrentPartHeaderName,
								"charset" => $this->strCurrentPartHeaderCharset,
								"content" => $strPartContent,
							);
						} elseif ($this->strCurrentPartHeaderContentType == "text/plain") {
							$this->strMessage = $strPartContent;
						} elseif ($this->strCurrentPartHeaderContentType == "text/html") {
							$this->strMessage = $strPartContent;
							$this->bIsHtmlMessage = true;
						}
					}
				}
			}
		} else {
			if ($this->strHeaderContentType == "text/html") {
				$this->bIsHtmlMessage = true;
			}
			if (preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $this->strOriginalMessage)) {
				$this->strMessage = iconv_mime_decode($this->strOriginalMessage, ICONV_MIME_DECODE_CONTINUE_ON_ERROR, $this->strHeaderCharset);
			} else {
				$this->strMessage = $this->strOriginalMessage;
			}
		}
	}

	protected function doParseMessagePart($strContent)
	{
		$strPartHeader = "";
		$strPartContent = "";
		$strContent = str_replace(["\r\n", "\r"], "\n", $strContent);
		$pos = strpos($strContent, "\n\n");
		$strPartHeader = substr($strContent, 0, $pos);
		$strPartContent = substr($strContent, $pos + 2);
		$arPartHeader = $this->doNormalizeAndParsePartHeaders($strPartHeader);
		if (preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $strPartContent)) {
			$strPartContent = iconv_mime_decode($strPartContent, ICONV_MIME_DECODE_CONTINUE_ON_ERROR, $this->strCurrentPartHeaderCharset);
		}
		return array($arPartHeader, $strPartContent);
	}

	protected function doNormalizeAndParsePartHeaders($strHeaders)
	{
		$strHeaders = str_replace(["\r\n", "\r"], "\n", $strHeaders);
		$arHeaders = explode("\n", $strHeaders);
		foreach ($arHeaders as $strLine) {
			$arLine = explode(": ", $strLine);
			if (trim(strtolower($arLine[0])) == "content-type") {
				unset($arLine[0]);
				$strLine = implode(": ", $arLine);
				$arLine = explode("; ", $strLine);
				$this->strCurrentPartHeaderContentType = $arLine[0];
				foreach ($arLine as $strPart) {
					if (strpos(strtolower($strPart), "charset=") === 0) {
						$this->strCurrentPartHeaderCharset = substr($strPart, 8);
					} elseif (strpos(strtolower($strPart), "boundary=\"") === 0) {
						$this->strCurrentPartHeaderBoundary = substr($strPart, 10, strlen($strPart) - 11);
					} elseif (strpos(strtolower($strPart), "name=\"") === 0) {
						$this->strCurrentPartHeaderName = substr($strPart, 6, strlen($strPart) - 7);
						if (preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $this->strCurrentPartHeaderName)) {
							$this->strCurrentPartHeaderName = iconv_mime_decode($this->strCurrentPartHeaderName, ICONV_MIME_DECODE_CONTINUE_ON_ERROR, $this->strCurrentPartHeaderCharset);
						}
					}
				}
				break;
			}
		}
		if (!preg_match('/=\?[\\d,\\w,-]*\?[q,Q,b,B]?.*\?=/', $strHeaders)) {
			$arData = explode("\n", $strHeaders);
			$arNormalizeLines = array();
			$strCurrentField = "";
			foreach ($arData as $val) {
				if (strpos($val, ": ") === false) {
					$strCurrentField .= $val;
				} else {
					if (strlen($strCurrentField) > 0) {
						$arNormalizeLines[] = $strCurrentField;
					}
					$strCurrentField = $val;
				}
			}
			if (strlen($strCurrentField) > 0) {
				$arNormalizeLines[] = $strCurrentField;
			}
			foreach ($arNormalizeLines as $k => $v) {
				$arVal = explode(": ", $v);
				$strName = $arVal[0];
				unset($arVal[0]);
				$strValue = implode(": ", $arVal);
				$arNormalizeLines[$k] = iconv_mime_encode($strName, $strValue);
			}
			$strHeaders = implode("\n", $arNormalizeLines);
		}
		$arHeadersOriginal = iconv_mime_decode_headers($strHeaders, ICONV_MIME_DECODE_CONTINUE_ON_ERROR, $this->strCurrentPartHeaderCharset);
		$arHeaders = array();
		foreach ($arHeadersOriginal as $k => $v) {
			$arHeaders[strtolower($k)] = array(
				"NAME" => $k,
				"VALUE" => $v,
			);
		}
		return $arHeaders;
	}
}