<?

namespace Ammina\SMTP;

class Mail
{
	static function custom_mail($to, $subject, $message, $additionalHeaders = '', $additionalParameters = '')
	{
		global $APPLICATION;
		$oParser = new Parser($to, $subject, $message, $additionalHeaders, $additionalParameters);
		$oParser->doParse();
		$arCurrentAccountDomain = \Ammina\SMTP\AccountsTable::getOptimalAccountDomainByFrom($oParser->strFieldFrom);
		if ($arCurrentAccountDomain) {
			$oMail = new \PHPMailer\PHPMailer\PHPMailer();
			if ($arCurrentAccountDomain['IS_ACCOUNT']) {
				$oMail->isSMTP();
				$oMail->Host = $arCurrentAccountDomain['ACCOUNT']['SMTP_HOST'];
				$oMail->SMTPAutoTLS = false;
				$oMail->SMTPAuth = true;
				$oMail->Username = $arCurrentAccountDomain['ACCOUNT']['ACCOUNT_LOGIN'];
				$oMail->Password = $arCurrentAccountDomain['ACCOUNT']['ACCOUNT_PASSWORD'];
				$oMail->SMTPSecure = ($arCurrentAccountDomain['ACCOUNT']['SECURE_TYPE'] == "S" ? "ssl" : ($arCurrentAccountDomain['ACCOUNT']['SECURE_TYPE'] == "T" ? "tls" : ""));
				$oMail->Port = $arCurrentAccountDomain['ACCOUNT']['SMTP_PORT'];
				$oMail->setFrom($arCurrentAccountDomain['ACCOUNT']['EMAIL'], $arCurrentAccountDomain['ACCOUNT']['SENDER_NAME']);
				$oMail->Timeout = \COption::GetOptionInt("ammina.smtp", "timeout", 30);
			} elseif ($arCurrentAccountDomain['IS_DOMAIN']) {
				$oMail->setFrom($oParser->strFieldFrom, $oParser->strFieldFromName);
				$oMail->isMail();
			}
			if (strlen($arCurrentAccountDomain['DOMAIN']['DKIM_PRIVATE']) > 0) {
				$oMail->DKIM_domain = $arCurrentAccountDomain['DOMAIN']['DOMAIN'];
				$oMail->DKIM_private = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina/smtp/keys/" . $arCurrentAccountDomain['DOMAIN']['DOMAIN'] . ".private.pem";
				$oMail->DKIM_selector = $arCurrentAccountDomain['DOMAIN']['DKIM_SELECTOR'];
				$oMail->DKIM_passphrase = $arCurrentAccountDomain['DOMAIN']['DKIM_PASSPHRASE'];
				$oMail->DKIM_identity = $oParser->strFieldFrom;
				$oMail->DKIM_copyHeaderFields = false;
				$oMail->Encoding = \PHPMailer\PHPMailer\PHPMailer::ENCODING_BASE64;
			}
			$oMail->addAddress($oParser->strFieldTo, $oParser->strFieldToName);
			$oMail->Subject = $oParser->strFieldSubject;
			$oMail->CharSet = $oParser->strHeaderCharset;
			$oMail->isHTML($oParser->bIsHtmlMessage);

			if (strpos($oParser->strHeaderContentType, "multipart/") === 0) {
				if ($oParser->strHeaderContentType == "multipart/alternative") {
					$oMail->Body = $oParser->strMessage;
					$oMail->AltBody = $oParser->strAltMessage;
				} elseif ($oParser->strHeaderContentType == "multipart/mixed") {
					$oMail->Body = $oParser->strMessage;
					$oMail->AltBody = $oParser->strAltMessage;
					foreach ($oParser->arAttachment as $arData) {
						$oMail->addStringEmbeddedImage($arData['content'], str_replace(["<", ">"], "", $arData['headers']['content-id']['VALUE']), $arData['name'], $arData['headers']['content-transfer-encoding']['VALUE'], $arData['content-type'], "attachment");
					}
				}
			} else {
				$oMail->Body = $oParser->strMessage;
			}
			foreach ($oParser->arFieldsHeaders as $key => $value) {
				if ($key == "content-transfer-encoding" || $key == "from" || $key == "date" || $key == "mime-version" || $key == "content-type") {
					continue;
				} elseif ($key == "bcc") {
					$arBcc = explode(",", $value['VALUE']);
					foreach ($arBcc as $strBcc) {
						$oMail->addBCC($strBcc);
					}
				} elseif ($key == "cc") {
					$arCc = explode(",", $value['VALUE']);
					foreach ($arCc as $strCc) {
						$oMail->addCC($strCc);
					}
				} elseif ($key == "reply-to") {
					$oMail->addReplyTo($value['VALUE']);
				} else {
					$oMail->addCustomHeader($value['NAME'], $value['VALUE']);
				}
			}
			$status = $oMail->send();

			if (\COption::GetOptionString("ammina.smtp", "log", "N") == "Y") {
				CheckDirPath($_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina/smtp/log/");
				if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina/.htaccess")) {
					file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina/.htaccess", 'Deny from All');
				}
				$f = fopen($_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina/smtp/log/" . date("Y-m-d") . ".log", "a+t");
				if ($f) {
					$bLock = true;
					$cnt = 100;
					while (!flock($f, LOCK_EX | LOCK_NB)) {
						$cnt--;
						if ($cnt <= 0) {
							$bLock = false;
							break;
						}
						usleep(10000);
					}
					if ($bLock) {
						if ($status) {
							$strLog = date("d.m.Y H:i:s") . ": Status: " . ($status ? "OK" : "Fail") . ". From: " . $oParser->strFieldFrom . ". Message: " . $oMail->getSentMIMEMessage() . "\n";
						} else {
							$strLog = date("d.m.Y H:i:s") . ": Status: " . ($status ? "OK" : "Fail") . ". From: " . $oParser->strFieldFrom . ". Error: " . $oMail->ErrorInfo . "\n";
						}
						fwrite($f, $strLog);
						flock($f, LOCK_UN);
					}
					fclose($f);
				}
			}
			return $status;
		} else {
			if ($additionalParameters != "") {
				return @mail($to, $subject, $message, $additionalHeaders, $additionalParameters);
			} else {
				return @mail($to, $subject, $message, $additionalHeaders);
			}
		}
		return true;
	}

}