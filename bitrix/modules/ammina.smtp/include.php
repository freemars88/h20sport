<?

IncludeModuleLangFile(__FILE__);

class CAmminaSmtp
{
	static protected $MODULE_TEST_PERIOD = false;

	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		return;
		$MODULE_ID = basename(dirname(__FILE__));
		$aMenu = array(
			//"parent_menu" => "global_menu_services",
			"parent_menu" => "global_menu_settings",
			"section" => $MODULE_ID,
			"sort" => 2000,
			"text" => GetMessage($MODULE_ID . "_MENU_INDEX_TITLE"),
			"title" => GetMessage($MODULE_ID . "_MENU_INDEX_TITLE"),
			"url" => $MODULE_ID . '.index.php',
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID . "_items",
			"more_url" => array(),
			"items" => array(),
		);
		if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '.index.php'))
			file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/index.php");?' . '>');
		if (file_exists($path = dirname(__FILE__) . '/admin')) {
			if ($dir = opendir($path)) {
				$arFiles = array();

				while (false !== $item = readdir($dir)) {
					if (in_array($item, array('.', '..', 'menu.php'/* , 'index.php' */)))
						continue;
					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '.' . $item))
						file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/' . $item . '");?' . '>');
					if (strpos($item, ".edit.php") !== false)
						continue;

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach ($arFiles as $item) {
					$aMenu['items'][] = array(
						'text' => GetMessage($MODULE_ID . "_MENU_" . $item . "_TITLE"),
						'url' => $MODULE_ID . '.' . $item,
						"more_url" => Array(str_replace(".php", ".edit.php", $MODULE_ID . '.' . $item)),
						'module_id' => $MODULE_ID,
						"title" => "",
					);
				}
			}
		}
		$aModuleMenu[] = $aMenu;
	}

	function OnPageStart()
	{
		return true;
	}

	static function isTestPeriodEnd()
	{
		if (self::$MODULE_TEST_PERIOD === false) {
			self::$MODULE_TEST_PERIOD = \Bitrix\Main\Loader::includeSharewareModule("ammina.smtp");
		}
		if (self::$MODULE_TEST_PERIOD == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED) {
			return true;
		}
		return false;
	}

	static function getTestPeriodInfo()
	{
		if (self::$MODULE_TEST_PERIOD === false) {
			self::$MODULE_TEST_PERIOD = \Bitrix\Main\Loader::includeSharewareModule("ammina.smtp");
		}
		return self::$MODULE_TEST_PERIOD;
	}
}

if (!function_exists("custom_mail")) {
	function custom_mail($to, $subject, $message, $additionalHeaders = '', $additionalParameters = '')
	{
		if (COption::GetOptionString("ammina.smtp", "activate_module", "N") == "Y") {
			return \Ammina\SMTP\Mail::custom_mail($to, $subject, $message, $additionalHeaders, $additionalParameters);
		} else {
			if ($additionalParameters != "") {
				return @mail($to, $subject, $message, $additionalHeaders, $additionalParameters);
			}
			return @mail($to, $subject, $message, $additionalHeaders);
		}
	}
} else {
	define("AMMINA_SMTP_CUSTOM_MAIL_EXISTS", true);
}

CModule::AddAutoloadClasses(
	"ammina.smtp", array(
		"PHPMailer\\PHPMailer\\Exception" => "classes/external/phpmailer/Exception.php",
		"PHPMailer\\PHPMailer\\PHPMailer" => "classes/external/phpmailer/PHPMailer.php",
		"PHPMailer\\PHPMailer\\POP3" => "classes/external/phpmailer/POP3.php",
		"PHPMailer\\PHPMailer\\SMTP" => "classes/external/phpmailer/SMTP.php",
		"Ammina\\SMTP\\DomainsTable" => "lib/domains.php",
		"Ammina\\SMTP\\AccountsTable" => "lib/accounts.php",
		"Ammina\\SMTP\\Parser" => "lib/parser.php",
		"Ammina\\SMTP\\Mail" => "lib/mail.php",
		"Bitrix\\Ammina\\SMTP\\Helpers\\Admin\\Blocks\\Domain" => "lib/helpers/admin/blocks/domain.php",
		"Bitrix\\Ammina\\SMTP\\Helpers\\Admin\\Blocks\\Dkim" => "lib/helpers/admin/blocks/dkim.php",
		"Bitrix\\Ammina\\SMTP\\Helpers\\Admin\\Blocks\\Account" => "lib/helpers/admin/blocks/account.php",
		"Bitrix\\Ammina\\SMTP\\Helpers\\Admin\\Blocks\\Connect" => "lib/helpers/admin/blocks/connect.php",
	)
);

?>