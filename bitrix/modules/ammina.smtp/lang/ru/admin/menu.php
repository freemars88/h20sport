<?
$MESS['AMMINA_SMTP_MENU_TEXT'] = "Ammina SMTP: Отправка почты через SMTP и DKIM";
$MESS['AMMINA_SMTP_MENU_TITLE'] = "Ammina SMTP: Отправка почты через SMTP и DKIM";
$MESS['AMMINA_SMTP_MENU_DOMAINS_TEXT'] = "Почтовые домены";
$MESS['AMMINA_SMTP_MENU_DOMAINS_TITLE'] = "Настройка почтовых доменов и ключей DKIM";
$MESS['AMMINA_SMTP_MENU_ACCOUNTS_TEXT'] = "Почтовые аккаунты";
$MESS['AMMINA_SMTP_MENU_ACCOUNTS_TITLE'] = "Настройка почтовых аккаунтов SMTP для отправки почты";
