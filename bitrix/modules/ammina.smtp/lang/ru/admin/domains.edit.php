<?
$MESS['AMMINA_SMTP_PAGE_TITLE_ADD'] = "Новый почтовый домен";
$MESS['AMMINA_SMTP_PAGE_TITLE_EDIT'] = "Редактирование почтового домена";
$MESS['AMMINA_SMTP_TO_LIST'] = "К списку";
$MESS['AMMINA_SMTP_TO_LIST_TITLE'] = "Вернуться к просмотру списка почтовых доменов";
$MESS['AMMINA_SMTP_TAB_DOMAIN'] = "Почтовый домен";
$MESS['AMMINA_SMTP_BLOCK_TITLE_DOMAIN'] = "Домен";
$MESS['AMMINA_SMTP_BLOCK_TITLE_DKIM'] = "DKIM подпись";
$MESS['AMMINA_SMTP_DNS_RECORDS_NOTE'] = '<p>Для использования DKIM подписи писем вам необходимо добавить DNS запись TXT со следующими параметрами:</p><p>Субдомен: #SUBDOMAIN#</p><p>Содержимое записи: #DNS_RECORD#</p><hr/><p>Не забудьте добавить SPF запись для домена. Пример DNS записи SPF:</p><p>example.org. IN TXT v=spf1 +a +mx ~all</p>';
