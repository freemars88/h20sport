<?
$MESS['AMMINA_SMTP_PAGE_TITLE'] = "Список почтовых доменов";
$MESS['AMMINA_SMTP_FILTER_DOMAIN'] = "Почтовый домен";
$MESS['AMMINA_SMTP_FIELD_DOMAIN'] = "Почтовый домен";
$MESS['AMMINA_SMTP_FIELD_DKIM_PRIVATE'] = "Приватный ключ DKIM";
$MESS['AMMINA_SMTP_FIELD_DKIM_PUBLIC'] = "Публичный ключ DKIM";
$MESS['AMMINA_SMTP_FIELD_DKIM_SELECTOR'] = "Селектор DKIM";
$MESS['AMMINA_SMTP_FIELD_DKIM_PASSPHRASE'] = "Ключевая фраза DKIM";
$MESS['AMMINA_SMTP_RECORD_EDIT'] = "Редактировать почтовый домен";
$MESS['AMMINA_SMTP_TO_ADD_DOMAIN'] = "Добавить";
$MESS['AMMINA_SMTP_TO_ADD_DOMAIN_TITLE'] = "Добавить новый почтовый домен";
$MESS['AMMINA_SMTP_ACTION_DKIM_GENERATE'] = "Сгенерировать DKIM ключ";
$MESS['AMMINA_SMTP_ACTION_DELETE'] = "Удалить";
$MESS['AMMINA_SMTP_ACTION_DELETE_CONFIRM'] = "Вы действительно хотите удалить почтовый домен? Будет удалена вся связанная информация";
$MESS['AMMINA_SMTP_DELETE_ERROR'] = "Ошибка удаления почтового домена";
