<?
$MESS['AMMINA_SMTP_PAGE_TITLE_ADD'] = "Новый почтовый аккаунт";
$MESS['AMMINA_SMTP_PAGE_TITLE_EDIT'] = "Редактирование почтового аккаунта";
$MESS['AMMINA_SMTP_TO_LIST'] = "К списку";
$MESS['AMMINA_SMTP_TO_LIST_TITLE'] = "Вернуться к просмотру списка почтовых аккаунтов";
$MESS['AMMINA_SMTP_TAB_ACCOUNT'] = "Почтовый аккаунт";
$MESS['AMMINA_SMTP_BLOCK_TITLE_ACCOUNT'] = "Настройки аккаунта";
$MESS['AMMINA_SMTP_BLOCK_TITLE_CONNECT'] = "Настройки подключения";
