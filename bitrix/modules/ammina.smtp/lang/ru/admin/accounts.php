<?
$MESS['AMMINA_SMTP_PAGE_TITLE'] = "Список почтовых аккаунтов";
$MESS['AMMINA_SMTP_FILTER_EMAIL'] = "EMail адрес отправителя";
$MESS['AMMINA_SMTP_FIELD_DOMAIN'] = "Почтовый домен";
$MESS['AMMINA_SMTP_FIELD_ACTIVE'] = "Активность";
$MESS['AMMINA_SMTP_FIELD_IS_DEFAULT'] = "По умолчанию";
$MESS['AMMINA_SMTP_FIELD_IS_DEFAULT_DOMAIN'] = "По умолчанию для домена";
$MESS['AMMINA_SMTP_FIELD_SMTP_HOST'] = "Адрес SMTP сервера";
$MESS['AMMINA_SMTP_FIELD_SMTP_PORT'] = "Порт SMTP сервера";
$MESS['AMMINA_SMTP_FIELD_SECURE_TYPE'] = "Тип соединения";
$MESS['AMMINA_SMTP_FIELD_EMAIL'] = "EMail адрес отправителя";
$MESS['AMMINA_SMTP_FIELD_SENDER_NAME'] = "Имя отправителя";
$MESS['AMMINA_SMTP_FIELD_ACCOUNT_LOGIN'] = "Логин почтового аккаунта";
$MESS['AMMINA_SMTP_FIELD_ACCOUNT_PASSWORD'] = "Пароль почтового аккаунта";
$MESS['AMMINA_SMTP_RECORD_EDIT'] = "Редактировать почтовый аккаунт";
$MESS['AMMINA_SMTP_TO_ADD_DOMAIN'] = "Добавить";
$MESS['AMMINA_SMTP_TO_ADD_DOMAIN_TITLE'] = "Добавить новый почтовый аккаунт";
$MESS['AMMINA_SMTP_ACTION_DELETE'] = "Удалить";
$MESS['AMMINA_SMTP_ACTION_DELETE_CONFIRM'] = "Вы действительно хотите удалить аккаунт?";
$MESS['AMMINA_SMTP_DELETE_ERROR'] = "Ошибка удаления аккаунта";

