<?

$MESS['ammina.smtp_PAGE_TITLE'] = "Модуль Ammina: Отправка почты через SMTP и DKIM подпись писем";
$MESS['ammina.smtp_TAB_SETTINGS_TITLE'] = "Настройки";
$MESS['ammina.smtp_TAB_SETTINGS_DESC'] = "Настройки модуля";
$MESS['ammina.smtp_TAB_SUPPORT_TITLE'] = "Техподдержка, развитие модуля";
$MESS['ammina.smtp_TAB_SUPPORT_DESC'] = "Техническая поддержка и пожелания по функционалу модуля ammina.smtp";
$MESS['ammina.smtp_TAB_SUPPORT_CONTENT'] = '
	<h3>Техническая поддержка</h3>
	<p>Техническая поддержка модуля осуществляется по электронной почте <a href="mailto:support@ammina.ru">support@ammina.ru</a></p>
	<h3>Развитие модуля, новый функционал</h3>
	<p>Если вы обнаружили что какого-то функционала модуля не хватает лично для вас - напишите нам.</p>
	<hr/>
	<h3>Наши контакты:</h3>
	<p>Электронная почта: <a href="mailto:support@ammina.ru">support@ammina.ru</a></p>
	<div style="clear:both;"></div>
';
$MESS['ammina.smtp_TAB_RIGHTS_TITLE'] = "Права на доступ";
$MESS['ammina.smtp_TAB_RIGHTS_DESC'] = "Настройка прав на доступ к модулю";
$MESS['ammina.smtp_OPTION_ACTIVATE_MODULE'] = "Активировать модуль";
$MESS['ammina.smtp_OPTION_LOG'] = "Логирование";
$MESS['ammina.smtp_OPTION_TIMEOUT'] = "Таймаут SMTP соединения";