<?
$MESS['AMMINA_SMTP_FIELD_DOMAIN_ID'] = "Почтовый домен";
$MESS['AMMINA_SMTP_FIELD_IS_DEFAULT_DOMAIN'] = "Аккаунт по умолчанию для домена";
$MESS['AMMINA_SMTP_FIELD_ACTIVE'] = "Активность";
$MESS['AMMINA_SMTP_FIELD_IS_DEFAULT'] = "Аккаунт по умолчанию для сайта";
$MESS['AMMINA_SMTP_FIELD_EMAIL'] = "EMail адрес отправителя";
$MESS['AMMINA_SMTP_FIELD_SENDER_NAME'] = "Имя отправителя";