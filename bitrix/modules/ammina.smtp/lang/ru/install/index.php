<?

$MESS["ammina.smtp_MODULE_NAME"] = "Ammina. Отправка почты через SMTP и DKIM подпись писем";
$MESS["ammina.smtp_MODULE_DESC"] = "Модуль Ammina: Отправка почты через SMTP и DKIM подпись писем.";
$MESS["ammina.smtp_PARTNER_NAME"] = "Ammina - решения для 1С-Битрикс";
$MESS["ammina.smtp_PARTNER_URI"] = "https://www.ammina.ru";
$MESS["ammina.smtp_INSTALL_TITLE"] = "Установка модуля Ammina: Отправка почты через SMTP и DKIM подпись писем";

$MESS['ammina.smtp_INSTALLFORM_ERROR_NAME'] = "укажите свое имя";
$MESS['ammina.smtp_INSTALLFORM_ERROR_EMAIL1'] = "укажите свою электронную почту";
$MESS['ammina.smtp_INSTALLFORM_ERROR_EMAIL2'] = "проверьте корректность указания электроннной почты";
$MESS['ammina.smtp_INSTALLFORM_ERROR_PHONE'] = "укажите свой телефон";
$MESS['ammina.smtp_INSTALLFORM_TITLE1'] = "После отправки данных за Вами будет закреплен менеджер, который будет Вас сопровождать.";
$MESS['ammina.smtp_INSTALLFORM_FIELD_NAME'] = "Ваше имя";
$MESS['ammina.smtp_INSTALLFORM_FIELD_NAME_PLACEHOLDER'] = "Ваше имя";
$MESS['ammina.smtp_INSTALLFORM_FIELD_EMAIL'] = "E-Mail";
$MESS['ammina.smtp_INSTALLFORM_FIELD_PHONE'] = "Номер телефона";
$MESS['ammina.smtp_INSTALLFORM_FIELDS_REQUIRED'] = "* все поля обязательны для заполнения";
$MESS['ammina.smtp_INSTALLFORM_RULES'] = 'Нажимая на кнопку &laquo;Установить модуль&raquo;, я даю согласие на обработку персональных данных и соглашаюсь c <a href="https://www.ammina.ru/confidential/" target="_blank">политикой конфиденциальности</a>.';
$MESS['ammina.smtp_INSTALLFORM_MANAGER'] = "Ваш личный менеджер:";
$MESS['ammina.smtp_INSTALLFORM_MANAGER1'] = "Проконсультирует Вас по использованию наших решений";
$MESS['ammina.smtp_INSTALLFORM_MANAGER2'] = "Свяжет с нашей технической поддержкой для бесплатной установки и настройки решения";
$MESS['ammina.smtp_INSTALLFORM_MANAGER3'] = "Ответит на все интересующие вопросы";
$MESS['ammina.smtp_INSTALLFORM_MANAGER4'] = "Расскажет о наших продуктах, услугах, акциях и скидках";
$MESS['ammina.smtp_INSTALLFORM_MANAGER5'] = "Поможет организовать техническую поддержку Вашего сайта";