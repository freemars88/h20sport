<?
$MESS['ammina.smtp_MENU_INDEX_TITLE'] = "Ammina: Отправка почты через SMTP и DKIM подпись писем";
$MESS['ammina.smtp_MENU_index.php_TITLE'] = "Инструкция";
$MESS['AMMINA_SMTP_SYS_MODULE_IS_DEMO'] = 'Модуль работает в демо-режиме. Приобрести полнофункциональную версию вы можете по адресу: <a href="https://marketplace.1c-bitrix.ru/solutions/ammina.smtp/" target="_blank">https://marketplace.1c-bitrix.ru/solutions/ammina.smtp/</a>';
$MESS['AMMINA_SMTP_SYS_MODULE_IS_DEMO_EXPIRED'] = 'Истекло время работы модуля в демо-режиме. Приобрести полнофункциональную версию вы можете по адресу: <a href="https://marketplace.1c-bitrix.ru/solutions/ammina.smtp/" target="_blank">https://marketplace.1c-bitrix.ru/solutions/ammina.smtp/</a>';