<?php

namespace Yandex\Market\Export\Xml\Tag;

use Yandex\Market;

class Dimensions extends Base
{
	public function getDefaultParameters()
	{
		return [
			'name' => 'dimensions',
			'value_type' => Market\Type\Manager::TYPE_DIMENSIONS
		];
	}

	public function getSourceRecommendation(array $context = [])
	{
		return [
			[
				'TYPE' => Market\Export\Entity\Manager::TYPE_CATALOG_PRODUCT,
				'FIELD' => 'YM_SIZE'
			]
		];
	}
}