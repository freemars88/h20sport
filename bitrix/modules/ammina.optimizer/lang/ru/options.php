<?

use Bitrix\Main\Localization\Loc;

$MESS['ammina.optimizer_PAGE_TITLE'] = "Ammina Optimizer: Оптимизация сайта (CSS, JS, HTML, изображения)";
$MESS['ammina.optimizer_TAB_SETTINGS_TITLE'] = "Настройки";
$MESS['ammina.optimizer_TAB_SETTINGS_DESC'] = "Настройки модуля";
$MESS['ammina.optimizer_TAB_SUPPORT_TITLE'] = "Техподдержка, развитие модуля";
$MESS['ammina.optimizer_TAB_SUPPORT_DESC'] = "Техническая поддержка и пожелания по функционалу модуля Ammina Optimizer: Оптимизация сайта (CSS, JS, HTML, изображения)";
$MESS['ammina.optimizer_TAB_SUPPORT_CONTENT'] = '
	<h3>Техническая поддержка</h3>
	<p>Техническая поддержка модуля осуществляется по электронной почте <a href="mailto:support@ammina.ru">support@ammina.ru</a></p>
	<h3>Развитие модуля, новый функционал</h3>
	<p>Если вы обнаружили что какого-то функционала модуля не хватает лично для вас - напишите нам.</p>
	<hr/>
	<h3>Наши контакты:</h3>
	<p>Электронная почта: <a href="mailto:support@ammina.ru">support@ammina.ru</a></p>
	<div style="clear:both;"></div>
';
$MESS['ammina.optimizer_TAB_RIGHTS_TITLE'] = "Права на доступ";
$MESS['ammina.optimizer_TAB_RIGHTS_DESC'] = "Настройка прав на доступ к модулю";

//$MESS['ammina.optimizer_OPTION_DISABLE_PAGES'] = "Не использовать оптимизацию на страницах";
//$MESS['ammina.optimizer_OPTION_JSON_ACTIVE'] = "Использовать оптимизацию для JSON данных";
//$MESS['ammina.optimizer_OPTION_AJAX_ACTIVE'] = "Использовать оптимизацию на страницах AJAX запросов";
$MESS['ammina.optimizer_OPTION_GOOGLE_PAGESPEED_APIKEY'] = "API ключ к Google PageSpeed Insights";
$MESS['ammina.optimizer_OPTION_GOOGLE_PAGESPEED_APIKEY_GET'] = "Получить API ключ";
$MESS['ammina.optimizer_OPTION_AMMINA_APIKEY'] = "API ключ к сервису оптимизации файлов Ammina";
$MESS['ammina.optimizer_OPTION_AMMINA_APIKEY_GET'] = "Получить API ключ";
$MESS['ammina.optimizer_OPTION_SHOW_SUPPORT_FORM'] = "Показывать форму технической поддержки на административых страницах модуля";
$MESS['ammina.optimizer_OPTION_DEFAULT_DOMAIN'] = "Домен по умолчанию (при использовании фоновой оптимизации и сервера Ammina)";
$MESS['ammina.optimizer_OPTION_DEFAULT_ISHTTPS'] = "Протокол HTTPS по умолчанию (при использовании фоновой оптимизации и сервера Ammina)";

$MESS['ammina.optimizer_OPTION_SEPARATOR_LIBRARY'] = "Настройки оптимизации CSS";
$MESS['ammina.optimizer_OPTION_LIB_PATH_YUICOMPRESSOR'] = "Путь к исполняемому файлу YUI Compressor";
$MESS['ammina.optimizer_OPTION_LIB_PATH_UGLIFYCSS'] = "Путь к исполняемому файлу Uglify CSS";
$MESS['ammina.optimizer_OPTION_LIB_PATH_UGLIFYJS'] = "Путь к исполняемому файлу UglifyJS";
$MESS['ammina.optimizer_OPTION_LIB_PATH_UGLIFYJS2'] = "Путь к исполняемому файлу UglifyJS 2";
$MESS['ammina.optimizer_OPTION_LIB_PATH_TERSERJS'] = "Путь к исполняемому файлу Terser JS";
$MESS['ammina.optimizer_OPTION_LIB_PATH_BABELMINIFY'] = "Путь к исполняемому файлу Babel Minify";
$MESS['ammina.optimizer_OPTION_LIB_PATH_OPTIPNG'] = "Путь к исполняемому файлу OptiPNG";
$MESS['ammina.optimizer_OPTION_LIB_PATH_PNGQUANT'] = "Путь к исполняемому файлу PNGQuant";
$MESS['ammina.optimizer_OPTION_LIB_PATH_JPEGOPTIM'] = "Путь к исполняемому файлу JpegOptim";
$MESS['ammina.optimizer_OPTION_LIB_PATH_GIFSICLE'] = "Путь к исполняемому файлу GIFSicle";
$MESS['ammina.optimizer_OPTION_LIB_PATH_SVGO'] = "Путь к исполняемому файлу SVGO";
$MESS['ammina.optimizer_OPTION_LIB_PATH_CWEBP'] = "Путь к исполняемому файлу CWebP";
$MESS['ammina.optimizer_OPTION_LIB_PATH_GIF2WEBP'] = "Путь к исполняемому файлу Gif2WebP (пакет CWebP)";

$MESS['ammina.optimizer_OPTION_SEPARATOR_PREOPT'] = "Предоптимизация изображений по событиям";
$MESS['ammina.optimizer_OPTION_PREOPT_SAVE_ACTIVE'] = "Предоптимизация изображений при сохранении изображения";
$MESS['ammina.optimizer_OPTION_PREOPT_RESIZE_ACTIVE'] = "Предоптимизация изображений при изменении размера изображения";

$MESS['ammina.optimizer_OPTION_SEPARATOR_PREOPTAGENT'] = "Агент фоновой оптимизации изображений";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_ACTIVE'] = "Активировать агента";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_ONLYCRON'] = "Исполнять агента фоновой оптимизации только в кроне за 1 цикл (иначе пошаговое исполнение на хитах)";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_PERIOD'] = "Период запуска агента фоновой оптимизации (мин.)";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_MAXTIME_STEP'] = "Длительность 1 шага при запуске агента фоновой оптимизации на хитах (сек.)";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_PERIOD_STEPS'] = "Время между шагами при запуске агента фоновой оптимизации на хитах (сек.)";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_MEMORYLIMIT'] = "Лимит памяти для агента (Мбайт). Если не установлен, то не менять системное значение";
$MESS['ammina.optimizer_OPTION_PREOPTAGENT_DIRS'] = "Какие каталоги проверяет агент";
$MESS['ammina.optimizer_PREOPTAGENT_STATUS'] = "Статус";
$MESS['ammina.optimizer_PREOPTAGENT_STATUS_NEXT'] = "В процессе.<br>Последний проверенный файл: #FILE#";
$MESS['ammina.optimizer_PREOPTAGENT_STATUS_OK'] = "Ожидание следующего цикла проверки";

$MESS['ammina.optimizer_OPTION_SEPARATOR_RESULTAGENT'] = "Агент фоновой проверки результатов оптимизации на AmminaServer";
$MESS['ammina.optimizer_OPTION_RESULTAGENT_ACTIVE'] = "Активировать агента";
$MESS['ammina.optimizer_OPTION_RESULTAGENT_ONLYCRON'] = "Исполнять агента фоновой проверки результатов оптимизации только в кроне за 1 цикл (иначе пошаговое исполнение на хитах)";
$MESS['ammina.optimizer_OPTION_RESULTAGENT_PERIOD'] = "Период запуска агента фоновой проверки результатов оптимизации (мин.)";
$MESS['ammina.optimizer_OPTION_RESULTAGENT_MAXTIME_STEP'] = "Длительность 1 шага при запуске агента фоновой проверки результатов оптимизации на хитах (сек.)";
$MESS['ammina.optimizer_OPTION_RESULTAGENT_PERIOD_STEPS'] = "Время между шагами при запуске агента фоновой проверки результатов оптимизации на хитах (сек.)";
$MESS['ammina.optimizer_OPTION_RESULTAGENT_MEMORYLIMIT'] = "Лимит памяти для агента (Мбайт). Если не установлен, то не менять системное значение";

/*$MESS['ammina.optimizer_OPTION_SEPARATOR_CSS'] = "Настройки оптимизации CSS";
$MESS['ammina.optimizer_OPTION_CSS_ACTIVE'] = "Активировать оптимизацию CSS";
$MESS['ammina.optimizer_OPTION_CSS_DISABLE_PAGES'] = "Не использовать оптимизацию CSS на страницах";
$MESS['ammina.optimizer_OPTION_CSS_INLINE_ACTIVE'] = "Включить CSS из файлов в качестве inline в HTML код страницы";
$MESS['ammina.optimizer_OPTION_CSS_SET_TO_ENDBODY'] = "Поместить inline CSS из файлов перед закрывающим тегом body";
$MESS['ammina.optimizer_OPTION_CSS_FONTFACE'] = "Добавить стилю определения шрифта @font-face свойство font-display:";
$MESS['ammina.optimizer_OPTION_CSS_FONTFACE_NONE'] = "нет";
$MESS['ammina.optimizer_OPTION_CSS_FONTFACE_AUTO'] = "auto";
$MESS['ammina.optimizer_OPTION_CSS_FONTFACE_SWAP'] = "swap";
$MESS['ammina.optimizer_OPTION_CSS_FONTFACE_FALLBACK'] = "fallback";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_ACTIVE'] = "Минифицировать CSS файлы";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_TYPE'] = "Библиотека минификации CSS файлов";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_TYPE_PHPWEE'] = "PHP Wee";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_TYPE_MATTHIASMULLIE'] = "Matthias Mullie";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_TYPE_PHPMINIFIER'] = "PHP Minifier";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_TYPE_YUICOMPRESSOR'] = "Node YUI Compressor";
$MESS['ammina.optimizer_OPTION_CSS_MINIFY_TYPE_UGLIFYCSS'] = "Node Uglify CSS";
$MESS['ammina.optimizer_OPTION_CSS_INCIMAGES'] = "Включить в CSS файлы изображения из файлов в виде inline кода";
$MESS['ammina.optimizer_OPTION_CSS_INCIMAGES_MAXSIZE'] = "Максимальный размер включаемого в CSS inline изображения из файла, байт";
$MESS['ammina.optimizer_OPTION_CSS_FILES_NO_OPTIMIZE'] = "Не оптимизировать CSS файлы";
$MESS['ammina.optimizer_OPTION_CSS_FILES_NO_MINIMIZE'] = "Не минимизировать CSS файлы";
$MESS['ammina.optimizer_OPTION_CSS_HEADER_LINK_ACTIVE'] = "Отправлять заголовок Link для предзагрузки CSS файлов";
$MESS['ammina.optimizer_OPTION_CSS_PATH_YUICOMPRESSOR'] = "Путь к исполняемому файлу YUI Compressor";
$MESS['ammina.optimizer_OPTION_CSS_PATH_UGLIFYCSS'] = "Путь к исполняемому файлу Uglify CSS";
$MESS['ammina.optimizer_OPTION_CSS_REMOTE_ACTIVE'] = "Оптимизировать CSS файлы со сторонних сайтов";
$MESS['ammina.optimizer_OPTION_CSS_REMOTE_DISABLE_LINKS'] = "Исключить из оптимизации CSS файлы со сторонних сайтов";
$MESS['ammina.optimizer_OPTION_CSS_REMOTE_GOOGLEFONTS_TYPE'] = "Тип обработки подключенный шрифтов Google Fonts";
$MESS['ammina.optimizer_OPTION_CSS_REMOTE_GOOGLEFONTS_TYPE_NONE'] = "Не обрабатывать";
$MESS['ammina.optimizer_OPTION_CSS_REMOTE_GOOGLEFONTS_TYPE_INLINE'] = "Обработать и вставить как Inline CSS";
$MESS['ammina.optimizer_OPTION_CSS_REMOTE_GOOGLEFONTS_TYPE_LINK'] = "Обработать и вставить как Link";

$MESS['ammina.optimizer_OPTION_SEPARATOR_JS'] = "Настройки оптимизации JS";
$MESS['ammina.optimizer_OPTION_JS_ACTIVE'] = "Активировать оптимизацию JS";
$MESS['ammina.optimizer_OPTION_JS_DISABLE_PAGES'] = "Не использовать оптимизацию JS на страницах";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_ACTIVE'] = "Минифицировать JS файлы";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE'] = "Библиотека минификации JS файлов";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_PHPWEE'] = "PHP Wee";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_MATTHIASMULLIE'] = "Matthias Mullie";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_PHPMINIFIER'] = "PHP Minifier";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_UGLIFYJS'] = "Node Uglify JS";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_YUICOMPRESSOR'] = "Node YUI Compressor";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_UGLIFYJS2'] = "Node Uglify JS 2";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_TERSERJS'] = "Node Terser JS";
$MESS['ammina.optimizer_OPTION_JS_MINIFY_TYPE_BABELMINIFY'] = "Node Babel Minify";

$MESS['ammina.optimizer_OPTION_JS_FILES_NO_OPTIMIZE'] = "Не оптимизировать JS файлы";
$MESS['ammina.optimizer_OPTION_JS_FILES_NO_MINIMIZE'] = "Не минимизировать JS файлы";
$MESS['ammina.optimizer_OPTION_JS_HEADER_LINK_ACTIVE'] = "Отправлять заголовок Link для предзагрузки JS файлов";
$MESS['ammina.optimizer_OPTION_JS_BXCORE_FILES_ACTIVE'] = 'Дополнительная обработка файлов ядра при подключении сторонних библиотек не через $' . "APPLICATION->AddHeadScript();";
$MESS['ammina.optimizer_OPTION_JS_PATH_UGLIFYJS'] = "Путь к исполняемому файлу UglifyJS";
$MESS['ammina.optimizer_OPTION_JS_PATH_YUICOMPRESSOR'] = "Путь к исполняемому файлу YUI Compressor";
$MESS['ammina.optimizer_OPTION_JS_PATH_UGLIFYJS2'] = "Путь к исполняемому файлу UglifyJS 2";
$MESS['ammina.optimizer_OPTION_JS_PATH_TERSERJS'] = "Путь к исполняемому файлу Terser JS";
$MESS['ammina.optimizer_OPTION_JS_PATH_BABELMINIFY'] = "Путь к исполняемому файлу Babel Minify";
$MESS['ammina.optimizer_OPTION_JS_REMOTE_ACTIVE'] = "Оптимизировать JS файлы со сторонних сайтов";
$MESS['ammina.optimizer_OPTION_JS_REMOTE_DISABLE_LINKS'] = "Исключить из оптимизации JS файлы со сторонних сайтов";
$MESS['ammina.optimizer_OPTION_JS_REGUIRE_ACTIVE'] = "Обрабатывать RequireJS (например - решение битроник)";
$MESS['ammina.optimizer_OPTION_JS_REQUIRE_BASE_TEMPLATE_PATH'] = "Базовый путь RequireJS относительно каталога шаблона (например - решение битроник)";
$MESS['ammina.optimizer_OPTION_JS_REQUIRE_PATHS'] = "Синонимы путей RequireJS (например - решение битроник)";

$MESS['ammina.optimizer_OPTION_SEPARATOR_IMG'] = "Настройки оптимизации изображений &laquo;на лету&raquo;";
$MESS['ammina.optimizer_OPTION_IMG_ACTIVE'] = "Активировать оптимизацию изображений";
$MESS['ammina.optimizer_OPTION_IMG_DISABLE_PAGES'] = "Не использовать оптимизацию изображений на страницах";
$MESS['ammina.optimizer_OPTION_IMG_FILES_NO_OPTIMIZE'] = "Не оптимизировать изображения";
$MESS['ammina.optimizer_OPTION_IMG_WEBP_ACTIVE'] = "Активировать поддержку WebP изображений";
$MESS['ammina.optimizer_OPTION_IMG_QUALITY'] = "Качество сохраняемого изображения";
$MESS['ammina.optimizer_OPTION_IMG_SRC_ACTIVE'] = "Активировать оптимизацию изображений в теге IMG";
$MESS['ammina.optimizer_OPTION_IMG_DATASRC_ACTIVE'] = "Активировать оптимизацию изображений в аттрибуте data-src тега IMG (если у вас используется LazyLoad на сайте)";
$MESS['ammina.optimizer_OPTION_IMG_BACKGROUND_ACTIVE'] = "Активировать оптимизацию изображений, указанных в правилах background[-image]";
$MESS['ammina.optimizer_OPTION_IMG_ALL_LINKS_ACTIVE'] = "Активировать оптимизацию изображений, указанных в коде, как заключенные в кавычки и находящиеся в папке /upload/";
$MESS['ammina.optimizer_OPTION_IMG_UPLOAD_PNG2JPG_ACTIVE'] = "Активировать преобразование файлов PNG из каталога /upload/ в JPG формат";
$MESS['ammina.optimizer_OPTION_IMG_UPLOAD_GIF2JPG_ACTIVE'] = "Активировать преобразование файлов GIF из каталога /upload/ в JPG формат";
$MESS['ammina.optimizer_OPTION_IMG_DIR_2JPG'] = "Также преобразование файлов из каталогов";
$MESS['ammina.optimizer_OPTION_IMG_REMOTE_ACTIVE'] = "Оптимизировать файлы изображений со сторонних сайтов";
$MESS['ammina.optimizer_OPTION_IMG_REMOTE_DISABLE_LINKS'] = "Исключить из оптимизации файлы изображений со сторонних сайтов";

$MESS['ammina.optimizer_OPTION_SEPARATOR_HTML'] = "Настройки оптимизации HTML";
$MESS['ammina.optimizer_OPTION_HTML_ACTIVE'] = "Активировать оптимизацию HTML";
$MESS['ammina.optimizer_OPTION_HTML_DISABLE_PAGES'] = "Не использовать оптимизацию HTML на страницах";
$MESS['ammina.optimizer_OPTION_HTML_MINIFY_TYPE'] = "Библиотека минификации HTML файлов";
$MESS['ammina.optimizer_OPTION_HTML_MINIFY_TYPE_PHPWEE'] = "PHP Wee";
$MESS['ammina.optimizer_OPTION_HTML_MINIFY_TYPE_TINY'] = "Tiny HTML Minifier";
$MESS['ammina.optimizer_OPTION_HTML_MINIFY_TYPE_PHPMINIFIER'] = "PHP Minifier";
$MESS['ammina.optimizer_OPTION_HTML_MINIFY_TYPE_HTMLMINIFIER'] = "Node HTML-Minifier";
$MESS['ammina.optimizer_OPTION_HTML_PHPWEE_JS_ACTIVE'] = "Сжимать inline JS (для библиотек PHPWee и Node HTML-Minifier)";
$MESS['ammina.optimizer_OPTION_HTML_PHPWEE_CSS_ACTIVE'] = "Сжимать inline CSS (для библиотек PHPWee и Node HTML-Minifier)";
$MESS['ammina.optimizer_OPTION_HTML_PATH_HTMLMINIFIER'] = "Путь к исполняемому файлу HTML-Minifier";

$MESS['ammina.optimizer_OPTION_SEPARATOR_LINK'] = "Дополнительные опции";
$MESS['ammina.optimizer_OPTION_HEADER_OTHER_LINK'] = "Дополнительные заголовки Link (каждый с новой строки)";
$MESS['ammina.optimizer_OPTION_HEADER_PREFETCH'] = "Отправлять так же в заголовках Link prefetch";
$MESS['ammina.optimizer_OPTION_HEADER_LINK_TO_HTML'] = "Добавлять Preload и Prefetch в HTML страницы";
$MESS['ammina.optimizer_OPTION_CLEAR_CACHE'] = "Очистить кэш файлов CSS и JS";
$MESS['ammina.optimizer_NOTE_NOIMAGICK'] = "На вашем сервере не установлен PHP модуль Imagick. Функционал оптимизации изображений будет ограничен. Для использования всех возможностей установите модуль PHP Imagick";
*/
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
$MESS['ammina.optimizer_'] = "";
