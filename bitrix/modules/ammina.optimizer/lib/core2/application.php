<?

namespace Ammina\Optimizer\Core2;

use Ammina\Optimizer\Core2\Optimizer\CSS;
use Ammina\Optimizer\Core2\Optimizer\Html;
use Ammina\Optimizer\Core2\Optimizer\Image;
use Ammina\Optimizer\Core2\Optimizer\JS;
use Ammina\Optimizer\Core2\Parser\Base;
use Bitrix\Main\Composite\Engine;
use Bitrix\Main\Composite\Helper;
use Bitrix\Main\Composite\Page;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Page\AssetMode;
use Bitrix\Main\Web\Json;
use CBXVirtualIo;
use CDiskQuota;
use CFile;
use COption;
use CTempFile;
use CUpdateClientPartner;
use CUtil;
use Imagick;
use ImagickException;

class Application
{
	const REQUEST_TYPE_UNKNOWN = 0;
	const REQUEST_TYPE_HTML = 1;
	const REQUEST_TYPE_AJAX = 2;
	const REQUEST_TYPE_AUTOCOMPOSITE = 3;
	/**
	 * @var Application
	 */
	protected static $_instance = NULL;

	protected $startTime = false;
	protected $endTime = false;
	protected $totalTime = false;

	protected $startTimeParsing = false;
	protected $endTimeParsing = false;
	protected $totalTimeParsing = false;

	protected $startTimeImageOptimize = false;
	protected $endTimeImageOptimize = false;
	protected $totalTimeImageOptimize = false;

	protected $startTimeCssOptimize = false;
	protected $endTimeCssOptimize = false;
	protected $totalTimeCssOptimize = false;

	protected $startTimeJsOptimize = false;
	protected $endTimeJsOptimize = false;
	protected $totalTimeJsOptimize = false;

	protected $startTimeHtmlOptimize = false;
	protected $endTimeHtmlOptimize = false;
	protected $totalTimeHtmlOptimize = false;

	protected $startTimeMakeHeadersLink = false;
	protected $endTimeMakeHeadersLink = false;
	protected $totalTimeMakeHeadersLink = false;

	protected $startTimeMakeHtml = false;
	protected $endTimeMakeHtml = false;
	protected $totalTimeMakeHtml = false;

	protected $startMemory = false;
	protected $endMemory = false;
	protected $totalMemory = false;
	protected $isMobileDevice = false;
	/**
	 * @var Base
	 */
	protected $oParser = NULL;
	/**
	 * @var \AMOPT_Mobile_Detect
	 */
	protected $oDetector = NULL;
	protected $arOptions = array();
	protected $iRequestType = self::REQUEST_TYPE_UNKNOWN;
	protected $strClearCache = false;
	protected $isJsonResponse = false;
	/**
	 * @var CSS
	 */
	protected $oCSSOptimizer = NULL;
	/**
	 * @var JS
	 */
	protected $oJSOptimizer = NULL;
	/**
	 * @var Image
	 */
	protected $oImageOptimizer = NULL;
	/**
	 * @var Html
	 */
	protected $oHtmlOptimizer = NULL;
	protected $bSupportWebP = false;
	protected $bPreventSupportWebP = false;
	protected $strBrowser = "";
	protected $strBrowserVersion = "";
	public $arSupportFontTypes = array();
	protected $arSupportHeaders = array();

	protected $arRequestStack = array();
	protected $iMaxStackPackageImages = 200;

	/**
	 * @return Application
	 */
	public static function getInstance()
	{
		if (self::$_instance === NULL) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	private function __construct()
	{
		global $APPLICATION;
		if (isset($_REQUEST['amopt_showstat'])) {
			if ($_REQUEST['amopt_showstat'] == "Y") {
				$_SESSION['AMOPT_SHOWSTAT'] = true;
			} else {
				unset($_SESSION['AMOPT_SHOWSTAT']);
			}
		}
		$this->strClearCache = false;
		if (isset($_REQUEST['amopt_clear_cache'])) {
			if ($_REQUEST['amopt_clear_cache'] == "Y") {
				$this->strClearCache = true;
			} elseif ($_REQUEST['amopt_clear_cache'] == "css") {
				$this->strClearCache = "css";
			}
		}
		$this->oDetector = new \AMOPT_Mobile_Detect();
		if ($this->oDetector->isMobile() || $this->oDetector->isTablet()) {
			$this->arOptions = \Ammina\Optimizer\SettingsTable::getSettings(SITE_ID, "m");
			$this->isMobileDevice = true;
		} else {
			$this->arOptions = \Ammina\Optimizer\SettingsTable::getSettings(SITE_ID, "d");
			$this->isMobileDevice = false;
		}
		if (isset($this->arOptions['PAGES']) && !empty($this->arOptions['PAGES'])) {
			$bSetOptions = false;
			foreach ($this->arOptions['PAGES'] as $k => $arPage) {
				if ($arPage['page']['ACTIVE'] == "Y") {
					if (\CAmminaOptimizer::doMathPageToRules($arPage['page']['PAGES'], $APPLICATION->GetCurPage(true))) {
						$bSetOptions = true;
						$this->arOptions = $arPage;
						break;
					}
				}
			}
			if (!$bSetOptions) {
				$this->arOptions = $this->arOptions['MAIN'];
			}
		} else {
			$this->arOptions = $this->arOptions['MAIN'];
		}
		$this->oCSSOptimizer = new CSS($this->arOptions['category']['css']['groups']);
		$this->oJSOptimizer = new JS($this->arOptions['category']['js']['groups']);
		$this->oImageOptimizer = new Image($this->arOptions['category']['images']['groups']);
		$this->oHtmlOptimizer = new Html($this->arOptions['category']['html']['groups']);
		$this->doCheckBrowserFeatures();
	}

	private function __clone()
	{
	}

	private function __wakeup()
	{
		throw new \Exception("Cannot unserialize a singleton.");
	}

	public function isAllowComposite()
	{
		return ($this->arOptions['category']['main']['groups']['request']['options']['ACTIVE_AUTOCOMPOSITE'] == "Y");
	}

	/**
	 * @return Base
	 */
	public function getParser()
	{
		return $this->oParser;
	}

	protected function fillRequestType(&$strContent)
	{
		$bFullJsonDetect = true;
		$start = strpos($strContent, '<');
		if ($start !== false) {
			$sub = trim(substr($strContent, 0, $start));
			if (strlen($sub) <= 0) {
				$bFullJsonDetect = false;
			}
		}
		if ($bFullJsonDetect) {
			$arJson = @json_decode($strContent, true);
			if (is_object($arJson) || is_array($arJson)) {
				$this->isJsonResponse = true;
			}
		}
		$testContent = strtoupper(substr($strContent, 0, 1000));
		$isRand = Helper::getAjaxRandom();
		if ($isRand !== false) {
			$isRand = true;
		}
		if ($isRand && Helper::isAjaxRequest()) {
			$this->iRequestType = self::REQUEST_TYPE_AUTOCOMPOSITE;
		} elseif (\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->isAjaxRequest() || $_REQUEST['ajax'] == "yes" || $_REQUEST['ajax'] == "Y" || $_REQUEST['AJAX'] == "yes" || $_REQUEST['AJAX'] == "Y" || (defined("PUBLIC_AJAX_MODE") && PUBLIC_AJAX_MODE === true) || $_REQUEST['AJAX_CALL'] == "Y" || (isset($_REQUEST['bxajaxid']) && strlen($_REQUEST['bxajaxid']) > 0)) {
			$this->iRequestType = self::REQUEST_TYPE_AJAX;

		} elseif (strpos($testContent, '<!DOCTYPE') !== false || strpos($testContent, '<HTML') !== false) {
			$this->iRequestType = self::REQUEST_TYPE_HTML;
		}
	}

	public function doCheckAutocompositeUpdateCache(&$strContent)
	{
		$bResult = false;
		/**
		 * @todo �������� ��������� ��� ��������� ��������
		 */
		//$dividedData = Engine::getDividedPageData($strContent);
		$htmlCacheChanged = false;
		if (Engine::getUseHTMLCache()) {
			$page = Page::getInstance();
			if ($page->isCacheable()) {
				$cacheExists = $page->exists();
				$rewriteCache = true;//$page->getMd5() !== $dividedData["md5"];
				if (Engine::getAutoUpdate() && Engine::getAutoUpdateTTL() > 0 && $cacheExists) {
					$mtime = $page->getLastModified();
					if ($mtime !== false && ($mtime + Engine::getAutoUpdateTTL()) > time()) {
						$rewriteCache = false;
					}
				}

				$invalidateCache = Engine::getAutoUpdate() === false && Engine::isInvalidationRequest();

				if (!$cacheExists || $rewriteCache || $invalidateCache) {
					$bResult = true;
				}
			}
		}
		return $bResult;
	}

	/**
	 * @param $strContent
	 *
	 * @throws \Bitrix\Main\SystemException
	 */
	public function EndContent(&$strContent)
	{
		$bUpdateCacheAutocomposite = false;
		if ($this->iRequestType == self::REQUEST_TYPE_AUTOCOMPOSITE) {
			$bUpdateCacheAutocomposite = $this->doCheckAutocompositeUpdateCache($strContent);
			if (!$bUpdateCacheAutocomposite) {
				return;
			}
		}
		$this->startTime = microtime(true);
		$this->startMemory = memory_get_usage();
		$bAllowOptimize = true;
		if ($this->arOptions['category']['main']['options']['ACTIVE'] != "Y") {
			$bAllowOptimize = false;
		}
		if ($bAllowOptimize) {
			$this->fillRequestType($strContent);
			if ($this->iRequestType == self::REQUEST_TYPE_HTML || $bUpdateCacheAutocomposite) {
				if ($this->arOptions['category']['main']['groups']['request']['options']['ACTIVE_HTML'] != "Y") {
					$bAllowOptimize = false;
				}
			} elseif ($this->iRequestType == self::REQUEST_TYPE_AJAX && $this->isJsonResponse) {
				if ($this->arOptions['category']['main']['groups']['request']['options']['ACTIVE_AJAX'] != "Y" || $this->arOptions['category']['main']['groups']['request']['options']['ACTIVE_JSON'] != "Y") {
					$bAllowOptimize = false;
				}
			} elseif ($this->iRequestType == self::REQUEST_TYPE_AJAX && !$this->isJsonResponse) {
				if ($this->arOptions['category']['main']['groups']['request']['options']['ACTIVE_AJAX'] != "Y") {
					$bAllowOptimize = false;
				}
			} elseif ($this->iRequestType == self::REQUEST_TYPE_AUTOCOMPOSITE) {
				if ($this->arOptions['category']['main']['groups']['request']['options']['ACTIVE_AUTOCOMPOSITE'] != "Y") {
					$bAllowOptimize = false;
				}
			} else {
				$bAllowOptimize = false;
			}
		}
		$bAllowAutoHeaders = false;
		if ($bAllowOptimize) {
			$this->oParser = Base::createParser($this->arOptions['category']['main']['groups']['parse']['options']['LIBRARY']);
			if ($this->iRequestType == self::REQUEST_TYPE_HTML || $bUpdateCacheAutocomposite) {
				$this->doOptimizeHtml($strContent);
				$bAllowAutoHeaders = true;
			} elseif ($this->iRequestType == self::REQUEST_TYPE_AJAX && $this->isJsonResponse) {
				$this->doOptimizeJson($strContent);
				$bAllowAutoHeaders = true;
			} elseif ($this->iRequestType == self::REQUEST_TYPE_AJAX && !$this->isJsonResponse) {
				$this->doOptimizeAjax($strContent);
				$bAllowAutoHeaders = true;
			} elseif ($this->iRequestType == self::REQUEST_TYPE_AUTOCOMPOSITE) {
				$this->doOptimizeAutocomposite($strContent);
				$bAllowAutoHeaders = true;
			}
		}
		$this->doSendStackRequest();
		if ($bAllowAutoHeaders) {
			$this->doMakeHeaders();
		}

		$this->endTime = microtime(true);
		$this->totalTime = $this->endTime - $this->startTime;
		$this->endMemory = memory_get_peak_usage();
		$this->totalMemory = $this->endMemory - $this->startMemory;
		if ($this->isShowStat()) {
			$strStatContent = $this->doShowStat();
			$strContent = str_ireplace("</body", $strStatContent . '<link rel="stylesheet" href="/bitrix/css/ammina.optimizer/public.css"></body', $strContent);
		}
	}

	protected function getAllHeaders()
	{
		$arResult = array(
			"preload" => array(),
			"prefetch" => array(),
			"preconnect" => array(),
			"auto" => array(),
		);
		if ($this->arOptions['category']['other']['options']['ACTIVE'] == "Y") {
			//css
			foreach ($this->oCSSOptimizer->arHeadersPreloadFiles as $arHeader) {
				if (strlen($arHeader['FILE']) > 0 && $arHeader['TYPE'] == "STYLE") {
					$arResult['auto'][] = array(
						"href" => $arHeader['FILE'],
						"as" => "style",
					);
				}
			}
			//js
			foreach ($this->oJSOptimizer->arHeadersPreloadFiles as $arHeader) {
				if (strlen($arHeader['FILE']) > 0 && $arHeader['TYPE'] == "SCRIPT") {
					$arResult['auto'][] = array(
						"href" => $arHeader['FILE'],
						"as" => "script",
					);
				}
			}
			//critical font
			foreach ($this->oCSSOptimizer->arCriticalFonts as $arFont) {
				foreach ($this->arSupportFontTypes as $strFontType) {
					if (isset($arFont['srcbytype'][$strFontType])) {
						foreach ($arFont['srcbytype'][$strFontType] as $strFile) {
							if (strlen($strFile) > 0) {
								$arResult['auto'][] = array(
									"href" => $strFile,
									"as" => "font",
									"crossorigin" => "crossorigin",
								);
							}
						}
						break;
					}
				}
			}
			if ($this->arOptions['category']['other']['groups']['headers']['options']['ACTIVE_PRELOAD'] == "Y") {
				{
					$arPreload = explode("\n", $this->arOptions['category']['other']['groups']['headers']['options']['PRELOAD']);
					foreach ($arPreload as $val) {
						$val = trim($val);
						if (strlen($val) > 0) {
							$arVal = explode(";", $val);
							$arResult['preload'][] = array(
								"href" => $arVal[0],
								"as" => $arVal[1],
								"crossorigin" => $arVal[2],
							);
						}
					}
				}
			}
			if ($this->arOptions['category']['other']['groups']['headers']['options']['ACTIVE_PREFETCH'] == "Y") {
				{
					$arPrefetch = explode("\n", $this->arOptions['category']['other']['groups']['headers']['options']['PREFETCH']);
					foreach ($arPrefetch as $val) {
						$val = trim($val);
						if (strlen($val) > 0) {
							$arVal = explode(";", $val);
							$arResult['prefetch'][] = array(
								"href" => $arVal[0],
								"as" => $arVal[1],
								"crossorigin" => $arVal[2],
							);
						}
					}
				}
			}
			if ($this->arOptions['category']['other']['groups']['headers']['options']['ACTIVE_PRECONNECT'] == "Y") {
				{
					$arPreconnect = explode("\n", $this->arOptions['category']['other']['groups']['headers']['options']['PRECONNECT']);
					foreach ($arPreconnect as $val) {
						$val = trim($val);
						if (strlen($val) > 0) {
							$arVal = explode(";", $val);
							$arResult['preconnect'][] = array(
								"href" => $arVal[0],
							);
						}
					}
				}
			}
			if ($this->arOptions['category']['other']['groups']['headers']['options']['ACTIVE_USERAGENTS'] == "Y") {
				if (!in_array("preload", $this->arSupportHeaders)) {
					$arResult['preload'] = array();
				}
				if (!in_array("prefetch", $this->arSupportHeaders)) {
					$arResult['prefetch'] = array();
				}
				if (!in_array("preconnect", $this->arSupportHeaders)) {
					$arResult['preconnect'] = array();
				}
			}

			foreach ($arResult as $k => $v) {
				$arNew = array();
				foreach ($v as $v1) {
					$arNew[md5(serialize($v1))] = $v1;
				}
				$arResult[$k] = array_values($arNew);
			}
		}

		return $arResult;
	}

	public function doMakeHeaders()
	{
		//if (!(($this->arOptions['category']['other']['groups']['links']['options']['ONLY_COMPOSITE'] == "Y" && Engine::isEnabled()) || $this->arOptions['category']['other']['groups']['links']['options']['ONLY_COMPOSITE'] != "Y")) {
		$arHeaders = $this->getAllHeaders();
		foreach ($arHeaders['auto'] as $arHeader) {
			$arCommand = array(
				"<" . $arHeader['href'] . ">",
			);
			if (in_array("preload", $this->arSupportHeaders)) {
				$arCommand[] = 'rel=preload';
			} else {
				$arCommand[] = 'rel=prefetch';
			}
			if (isset($arHeader['as'])) {
				$arCommand[] = 'as=' . $arHeader['as'];
			}
			if (isset($arHeader['crossorigin'])) {
				$arCommand[] = 'crossorigin';
			}
			header("Link: " . implode("; ", $arCommand), false);
		}
		foreach ($arHeaders['preload'] as $arHeader) {
			$arCommand = array(
				"<" . $arHeader['href'] . ">",
				'rel=preload',
			);
			if (isset($arHeader['as'])) {
				$arCommand[] = 'as=' . $arHeader['as'];
			}
			if (isset($arHeader['crossorigin'])) {
				$arCommand[] = 'crossorigin';
			}
			header("Link: " . implode("; ", $arCommand), false);
		}
		foreach ($arHeaders['prefetch'] as $arHeader) {
			$arCommand = array(
				"<" . $arHeader['href'] . ">",
				'rel=prefetch',
			);
			if (isset($arHeader['as'])) {
				$arCommand[] = 'as=' . $arHeader['as'];
			}
			if (isset($arHeader['crossorigin'])) {
				$arCommand[] = 'crossorigin';
			}
			header("Link: " . implode("; ", $arCommand), false);
		}
		foreach ($arHeaders['preconnect'] as $arHeader) {
			$arCommand = array(
				"<" . $arHeader['href'] . ">",
				'rel=preconnect',
			);
			header("Link: " . implode("; ", $arCommand), false);
		}
		//}
	}

	public function doMakeHeadersLink()
	{
		$isEngineEnabled = false;
		if (class_exists("Bitrix\\Main\\Composite\\Engine")) {
			$isEngineEnabled = Engine::isEnabled();
		}
		if ($this->arOptions['category']['other']['options']['ACTIVE'] == "Y" && $this->arOptions['category']['other']['groups']['links']['options']['SET_LINKS'] == "Y") {
			if (($this->arOptions['category']['other']['groups']['links']['options']['ONLY_COMPOSITE'] == "Y" && $isEngineEnabled) || $this->arOptions['category']['other']['groups']['links']['options']['ONLY_COMPOSITE'] != "Y") {

				$arHeaders = $this->getAllHeaders();

				foreach ($arHeaders['auto'] as $arHeader) {
					$arCommand = array(
						"href" => $arHeader['href'],
					);
					if ($this->arOptions['category']['other']['groups']['links']['options']['LINKS_TYPE'] == "preload") {
						$arCommand['rel'] = "preload";
					} else {
						$arCommand['rel'] = "prefetch";
					}
					if (isset($arHeader['as'])) {
						$arCommand['as'] = $arHeader['as'];
					}
					if (isset($arHeader['crossorigin'])) {
						$arCommand['crossorigin'] = NULL;
					}
					$this->oParser->AddTag("//head[1]", "link", $arCommand, "", true);
				}
				if ($this->arOptions['category']['other']['groups']['links']['options']['LINKS_TYPE'] == "preload") {
					foreach ($arHeaders['preload'] as $arHeader) {
						$arCommand = array(
							"href" => $arHeader['href'],
							"rel" => 'preload',
						);
						if (isset($arHeader['as'])) {
							$arCommand['as'] = $arHeader['as'];
						}
						if (isset($arHeader['crossorigin'])) {
							$arCommand['crossorigin'] = NULL;
						}
						$this->oParser->AddTag("//head[1]", "link", $arCommand, "", true);
					}
				} else {
					foreach ($arHeaders['prefetch'] as $arHeader) {
						$arCommand = array(
							"href" => $arHeader['href'],
							"rel" => 'prefetch',
						);
						if (isset($arHeader['as'])) {
							$arCommand["as"] = $arHeader['as'];
						}
						if (isset($arHeader['crossorigin'])) {
							$arCommand['crossorigin'] = NULL;
						}
						$this->oParser->AddTag("//head[1]", "link", $arCommand, "", true);
					}
				}
				foreach ($arHeaders['preconnect'] as $arHeader) {
					$arCommand = array(
						"href" => $arHeader['href'],
						"rel" => 'preconnect',
					);
					$this->oParser->AddTag("//head[1]", "link", $arCommand, "", true);
				}
			}
		}
	}

	public function doOptimizeHtml(&$strContent)
	{
		$this->startTimeParsing = microtime(true);
		$this->doCheckNormalEncode($strContent);
		$this->oParser->doParse($strContent);
		$this->endTimeParsing = microtime(true);
		$this->totalTimeParsing = $this->endTimeParsing - $this->startTimeParsing;

		$this->startTimeImageOptimize = microtime(true);
		if ($this->arOptions['category']['images']['options']['ACTIVE'] == "Y") {
			$this->oImageOptimizer->doOptimize();
		}
		$this->endTimeImageOptimize = microtime(true);
		$this->totalTimeImageOptimize = $this->endTimeImageOptimize - $this->startTimeImageOptimize;

		$this->startTimeCssOptimize = microtime(true);
		if ($this->arOptions['category']['css']['options']['ACTIVE'] == "Y") {
			$this->oCSSOptimizer->doOptimize();
		}
		$this->endTimeCssOptimize = microtime(true);
		$this->totalTimeCssOptimize = $this->endTimeCssOptimize - $this->startTimeCssOptimize;

		$this->startTimeJsOptimize = microtime(true);
		if ($this->arOptions['category']['js']['options']['ACTIVE'] == "Y") {
			$this->oJSOptimizer->doOptimize();
		}
		$this->endTimeJsOptimize = microtime(true);
		$this->totalTimeJsOptimize = $this->endTimeJsOptimize - $this->startTimeJsOptimize;

		$this->startTimeHtmlOptimize = microtime(true);
		if ($this->arOptions['category']['html']['options']['ACTIVE'] == "Y") {
			$this->oHtmlOptimizer->doOptimize();
		}
		$this->endTimeHtmlOptimize = microtime(true);
		$this->totalTimeHtmlOptimize = $this->endTimeHtmlOptimize - $this->startTimeHtmlOptimize;

		$this->startTimeMakeHeadersLink = microtime(true);
		$this->doMakeHeadersLink();
		$this->endTimeMakeHeadersLink = microtime(true);
		$this->totalTimeMakeHeadersLink = $this->endTimeMakeHeadersLink - $this->startTimeMakeHeadersLink;

		$this->startTimeMakeHtml = microtime(true);
		$strContent = $this->oParser->doSaveHTML();
		$this->endTimeMakeHtml = microtime(true);
		$this->totalTimeMakeHtml = $this->endTimeMakeHtml - $this->startTimeMakeHtml;
	}

	public function doOptimizeAjax(&$strContent)
	{
		if (strpos($strContent, '<') !== false && strpos($strContent, '</') !== false) {
			$this->doCheckNormalEncode($strContent);
			$this->oParser->doParsePart($strContent);
			if ($this->arOptions['category']['images']['options']['ACTIVE'] == "Y") {
				$this->oImageOptimizer->doOptimizePart();
			}

			if ($this->arOptions['category']['css']['options']['ACTIVE'] == "Y") {
				$this->oCSSOptimizer->doOptimizePart();
			}

			if ($this->arOptions['category']['js']['options']['ACTIVE'] == "Y") {
				$this->oJSOptimizer->doOptimizePart();
			}

			if ($this->arOptions['category']['html']['options']['ACTIVE'] == "Y") {
				$this->oHtmlOptimizer->doOptimize();
			}

			$this->doMakeHeadersLink();

			$strContent = $this->oParser->doSaveHTMLPart();
		}
	}

	public function doOptimizeJson(&$strContent)
	{
		$arData = Json::decode($strContent);
		$arData = $this->doOptimizeJsonData($arData);
		$strContent = Json::encode($arData);
	}

	protected function doOptimizeJsonData($arData)
	{
		if (is_array($arData)) {
			foreach ($arData as $k => $v) {
				$arData[$k] = $this->doOptimizeJsonData($v);
			}
		} else {
			if (strpos($arData, '<') !== false && strpos($arData, '</') !== false) {
				$this->doCheckNormalEncode($arData);
				$this->oParser->doParsePart($arData);
				if ($this->arOptions['category']['images']['options']['ACTIVE'] == "Y") {
					$this->oImageOptimizer->doOptimizePart();
				}

				if ($this->arOptions['category']['css']['options']['ACTIVE'] == "Y") {
					$this->oCSSOptimizer->doOptimizePart();
				}

				if ($this->arOptions['category']['js']['options']['ACTIVE'] == "Y") {
					$this->oJSOptimizer->doOptimizePart();
				}

				if ($this->arOptions['category']['html']['options']['ACTIVE'] == "Y") {
					$this->oHtmlOptimizer->doOptimize();
				}

				$this->doMakeHeadersLink();

				$arData = $this->oParser->doSaveHTMLPart();
			}
		}
		return $arData;
	}

	public static function JSUnEscape($s)
	{
		static $aSearch = array("\\\\", "\\'", '\\"', "\n", "\n", "\\n", "\\n", "*\\/", "<\\/");
		static $aReplace = array("\\", "'", "\"", "\n", "\n", "\n", "\n", "*/", "</");
		$val = str_replace($aSearch, $aReplace, $s);
		return $val;
	}

	public function doCheckNormalEncode(&$strContent)
	{
		if ($this->arOptions['category']['main']['groups']['parse']['options']['CHECK_ENCODING_UTF8'] == "Y") {
			if (defined("BX_UTF") && BX_UTF === true) {
				if (mb_check_encoding($strContent, "windows-1251") && !mb_check_encoding($strContent, "utf-8")) {
					$strContent = mb_convert_encoding($strContent, "utf-8");;
				}
			}
		}
	}

	public function doOptimizeAutocomposite($strContent)
	{
		$arData = \CUtil::JsObjectToPhp($strContent, true);
		$bAllowAutoHeaders = true;
		if (!empty($arData['dynamicBlocks'])) {
			foreach ($arData['dynamicBlocks'] as $k => $v) {
				//$arData['dynamicBlocks'][$k]['CONTENT'] = $this->JSUnEscape($arData['dynamicBlocks'][$k]['CONTENT']);
				$arData['dynamicBlocks'][$k]['CONTENT'] = $this->doOptimizeJsonData($this->JSUnEscape($arData['dynamicBlocks'][$k]['CONTENT']));
			}
		}
		//$this->oCSSOptimizer = new CSS($this->arOptions['category']['css']['groups']);
		//$this->oJSOptimizer = new JS($this->arOptions['category']['js']['groups']);
		if (!empty($arData['css'])) {
			$tmpCSSOptimizer = new CSS($this->arOptions['category']['css']['groups']);
			$strResult = $tmpCSSOptimizer->doOptimizeCssFilesArray($arData['css']);
			$this->oCSSOptimizer->arHeadersPreloadFiles = array_merge($this->oCSSOptimizer->arHeadersPreloadFiles, $tmpCSSOptimizer->arHeadersPreloadFiles);
			$arData['css'] = array($strResult);
			$bAllowAutoHeaders = true;
		}
		if (!empty($arData['js'])) {
			$tmpJSOptimizer = new JS($this->arOptions['category']['js']['groups']);
			$strResult = $tmpJSOptimizer->doOptimizeJsFilesArray($arData['js']);
			$this->oJSOptimizer->arHeadersPreloadFiles = array_merge($this->oJSOptimizer->arHeadersPreloadFiles, $tmpJSOptimizer->arHeadersPreloadFiles);
			$arData['js'] = array($strResult);
			$bAllowAutoHeaders = true;
		}
		if (!empty($arData['additional_js'])) {
			$tmpJSOptimizer = new JS($this->arOptions['category']['additional_js']['groups']);
			$strResult = $tmpJSOptimizer->doOptimizeJsFilesArray($arData['additional_js']);
			$this->oJSOptimizer->arHeadersPreloadFiles = array_merge($this->oJSOptimizer->arHeadersPreloadFiles, $tmpJSOptimizer->arHeadersPreloadFiles);
			$arData['additional_js'] = array($strResult);
			$bAllowAutoHeaders = true;
		}
		$strContent = \CUtil::PhpToJSObject($arData);
		$this->doSendStackRequest();
		if ($bAllowAutoHeaders) {
			$this->doMakeHeaders();
		}
		return $strContent;
	}

	public function isClearCache($strType = "")
	{
		if ($this->strClearCache === true) {
			return true;
		} elseif (strlen($strType) >= 0) {
			if ($this->strClearCache == $strType) {
				return true;
			}
		}
		return false;
	}

	public function isShowStat()
	{
		return (isset($_SESSION['AMOPT_SHOWSTAT']) && $_SESSION['AMOPT_SHOWSTAT']);
	}

	public function doShowStat()
	{
		if ($this->iRequestType == self::REQUEST_TYPE_HTML) {
			ob_start();
			?>
			<div class="amoptpub">
				<p><strong><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TITLE") ?></strong></p>
				<p><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME", array("#TIME#" => round($this->totalTime, 5))) ?>
					, <?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_ALSO") ?></p>
				<ul>
					<li><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_PARSING", array("#TIME#" => round($this->totalTimeParsing, 5))) ?></li>
					<li><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_IMAGE_OPTIMIZE", array("#TIME#" => round($this->totalTimeImageOptimize, 5))) ?></li>
					<li><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_CSS_OPTIMIZE", array("#TIME#" => round($this->totalTimeCssOptimize, 5))) ?></li>
					<li><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_JS_OPTIMIZE", array("#TIME#" => round($this->totalTimeJsOptimize, 5))) ?></li>
					<li><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_HTML_OPTIMIZE", array("#TIME#" => round($this->totalTimeHtmlOptimize, 5))) ?></li>
					<li><?= Loc::getMessage("AMMINA_OPTIMIZER_PANEL_STAT_TIME_MAKE_HTML", array("#TIME#" => round($this->totalTimeMakeHtml, 5))) ?></li>
				</ul>
				<?/*<p>������: <?= $this->totalMemory ?></p>*/ ?>
			</div>
			<?
			$strContent = ob_get_contents();
			ob_end_clean();
			return $strContent;
		}
		return '';
	}

	public function isSupportWebP()
	{
		//return true;
		return ($this->bSupportWebP || $this->bPreventSupportWebP);
	}

	public function setPreventSupportWebP($bSupport)
	{
		$this->bPreventSupportWebP = $bSupport;
	}

	protected function doCheckBrowserFeatures()
	{
		/**
		 * �������� ������������ � ��������� ������ ������������ ����������
		 */
		if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
			$this->bSupportWebP = true;
		}
		$arBrowsers = array(
			'IE',
			'Edge',
			'Firefox',
			'Chrome',
			'Safari',
			'Opera',
			'Opera Mini',
			'Opera Mobi',
			'UCBrowser',
			'SamsungBrowser',
		);
		foreach ($arBrowsers as $browser) {
			$strVersion = $this->oDetector->version($browser);
			if (strlen($strVersion) > 0) {
				$this->strBrowser = $browser;
				$this->strBrowserVersion = $strVersion;
				break;
			}
		}
		switch ($this->strBrowser) {
			case "IE":
				if (version_compare($this->strBrowserVersion, '9', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '6', '>=')) {
					$this->arSupportFontTypes[] = "eot";
				}
				if (version_compare($this->strBrowserVersion, '11', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '10', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				break;
			case "Edge":
				if (version_compare($this->strBrowserVersion, '18', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '14', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '76', '>=')) {
					$this->arSupportHeaders[] = "preload";
				}
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				if (version_compare($this->strBrowserVersion, '76', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
			case "Firefox":
				if (version_compare($this->strBrowserVersion, '65', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '39', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '3.6', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '3.5', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '2', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '3.5', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				if (version_compare($this->strBrowserVersion, '57', '>=')) {
					//$this->arSupportHeaders[] = "preload";//���� ����� ��������� ��������. �� ��������� false ���������. ������� ��������� ���������
				}
				if (version_compare($this->strBrowserVersion, '39', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
			case "Chrome":
				if (version_compare($this->strBrowserVersion, '32', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '36', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '5', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '50', '>=')) {
					$this->arSupportHeaders[] = "preload";
				}
				if (version_compare($this->strBrowserVersion, '8', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				if (version_compare($this->strBrowserVersion, '46', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
			case "Safari":
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '5.1', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '3.1', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '11.1', '>=')) {
					$this->arSupportHeaders[] = "preload";
				}
				if (version_compare($this->strBrowserVersion, '5', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				if (version_compare($this->strBrowserVersion, '11.1', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
			case "Opera":
				if (version_compare($this->strBrowserVersion, '19', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '23', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '11.5', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '10', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '37', '>=')) {
					$this->arSupportHeaders[] = "preload";
				}
				if (version_compare($this->strBrowserVersion, '15', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '15', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				if (version_compare($this->strBrowserVersion, '33', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
			case "Opera Mini":
				if (version_compare($this->strBrowserVersion, '1', '>=')) {
					$this->bSupportWebP = true;
				}
				break;
			case "Opera Mobi":
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '46', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '12', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '46', '>=')) {
					$this->arSupportHeaders[] = "preload";
				}
				if (version_compare($this->strBrowserVersion, '46', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '46', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
			case "UCBrowser":
				if (version_compare($this->strBrowserVersion, '11.8', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '11.8', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '11.8', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '11.8', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				break;
			case "SamsungBrowser":
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->bSupportWebP = true;
				}
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->arSupportFontTypes[] = "woff2";
				}
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->arSupportFontTypes[] = "woff";
				}
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->arSupportFontTypes[] = "ttf";
					$this->arSupportFontTypes[] = "otf";
				}
				if (version_compare($this->strBrowserVersion, '5', '>=')) {
					$this->arSupportHeaders[] = "preload";
				}
				if (version_compare($this->strBrowserVersion, '4', '>=')) {
					$this->arSupportHeaders[] = "prefetch";
				}
				if (version_compare($this->strBrowserVersion, '5', '>=')) {
					$this->arSupportHeaders[] = "dns-prefetch";
				}
				if (version_compare($this->strBrowserVersion, '5', '>=')) {
					$this->arSupportHeaders[] = "preconnect";
				}
				break;
		}
		if (empty($this->arSupportHeaders)) {
			$this->arSupportHeaders[] = "prefetch";
			$this->arSupportHeaders[] = "preconnect";
		}
		if (empty($this->arSupportFontTypes)) {
			$this->arSupportFontTypes[] = "woff";
			$this->arSupportFontTypes[] = "ttf";
			$this->arSupportFontTypes[] = "otf";
		}
	}

	/**
	 * @return CSS
	 */
	public function getCSSOptimizer()
	{
		return $this->oCSSOptimizer;
	}

	/**
	 * @return JS
	 */
	public function getJSOptimizer()
	{
		return $this->oJSOptimizer;
	}

	/**
	 * @return Image
	 */
	public function getImageOptimizer()
	{
		return $this->oImageOptimizer;
	}

	public function normalized_file_get_content($strFileName)
	{
		global $APPLICATION;
		$strContent = file_get_contents($strFileName);
		if (defined("BX_UTF") && BX_UTF === true) {
			if (strlen($strContent) > 0 && strlen(htmlspecialchars($strContent)) <= 0) {
				$strContent = $APPLICATION->ConvertCharset($strContent, "windows-1251", "utf-8");
			}
		}
		return $strContent;
	}

	public function doPushStackRequest($arRequest, $strTypeRequest, $strResultFilePath, $strFileNameResultUrl)
	{
		\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strFileNameResultUrl, "wait");
		$arFieldsRequest = array(
			"type_request" => $strTypeRequest,
			"request" => $arRequest,
			"result_file_path" => $strResultFilePath,
			"file_name_result_url" => $strFileNameResultUrl,
		);
		$this->arRequestStack[] = $arFieldsRequest;
		if (count($this->arRequestStack) >= $this->iMaxStackPackageImages) {
			$this->doSendStackRequest();
			$this->arRequestStack = array();
		}
		return true;
	}

	public function doSendStackRequest()
	{
		global $APPLICATION;
		if (!empty($this->arRequestStack)) {
			$arResult = $this->doSendStackRequestWorkServer();
			if ($arResult['status'] == "ok") {
				foreach ($this->arRequestStack as $kRequest => $vRequest) {
					$strFileNameResultUrl = $vRequest['file_name_result_url'];
					$strResultFilePath = $vRequest['result_file_path'];
					$arRequest = $vRequest['request'];
					$arCurrentResult = $arResult['ANSWER'][$kRequest];
					if ($arCurrentResult['status'] == "ok") {
						\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strFileNameResultUrl, $arCurrentResult['url']);
					} elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . $strFileNameResultUrl) && file_get_contents($_SERVER['DOCUMENT_ROOT'] . $strFileNameResultUrl) == "wait") {
						@unlink($_SERVER['DOCUMENT_ROOT'] . $strFileNameResultUrl);
					}
				}
			}
		}
	}

	public function doSendStackRequestWorkServer()
	{
		global $APPLICATION;
		if (!empty($this->arRequestStack)) {
			$arFields = array(
				"SYSTEM" => array(
					"HOST" => $_SERVER['HTTP_HOST'],
					"HTTPS" => $APPLICATION->IsHttps(),
				),
			);
			if (strlen($arFields['SYSTEM']['HOST']) <= 0) {
				$arFields['SYSTEM']['HOST'] = COption::GetOptionString("ammina.optimizer", "default_host", "");
				$arFields['SYSTEM']['HTTPS'] = (COption::GetOptionString("ammina.optimizer", "default_ishttps", "N") == "Y" ? true : false);
			}
			if (strlen($arFields['SYSTEM']['HOST']) <= 0) {
				$arFields['SYSTEM']['HOST'] = COption::GetOptionString("main", "server_name", "");
				$arFields['SYSTEM']['HTTPS'] = (COption::GetOptionString("main", "mail_link_protocol", "http") == "https" ? true : false);
			}
			$strUrl = COption::GetOptionString("ammina.optimizer", "ammina_workurl", "");
			$urltime = COption::GetOptionString("ammina.optimizer", "ammina_workurltime", "");
			if (strlen($strUrl) <= 0 || $urltime <= 0 || $urltime < (time() - 3600)) {
				$arUrlResponse = \CAmminaOptimizer::doRequestAmminaServer("GET_WORK_SERVER");
				if ($arUrlResponse['status'] == "ok") {
					$strUrl = $arUrlResponse['url'];
					$urltime = time();
					COption::SetOptionString("ammina.optimizer", "ammina_workurl", $strUrl);
					COption::SetOptionString("ammina.optimizer", "ammina_workurltime", $urltime);
				}
			}
			if (strlen($strUrl) > 0) {
				$arFields['k'] = COption::GetOptionString("ammina.optimizer", "ammina_apikey", "");
				$lk = COption::GetOptionString("ammina.optimizer", "ammina_lkey", "");
				$lktime = COption::GetOptionString("ammina.optimizer", "ammina_lkeytime", "");
				if (strlen($lk) <= 0 || $lktime <= 0 || $lktime < (time() - 3600)) {
					include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client.php");
					include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client_partner.php");
					$lk = md5('BITRIX' . CUpdateClientPartner::GetLicenseKey() . 'LICENSE');
					COption::SetOptionString("ammina.optimizer", "ammina_lkey", $lk);
					COption::SetOptionString("ammina.optimizer", "ammina_lkeytime", time());
				}
				$arFields['l'] = $lk;
				$arFields['version'] = "2.0";
				foreach ($this->arRequestStack as $kRequest => $vRequest) {
					$arRequest = $vRequest['request'];
					$arRequest['t'] = $vRequest['type_request'];
					$arFields["REQUESTS"][$kRequest] = $arRequest;
				}
				if (!defined("BX_UTF") || BX_UTF !== true) {
					$arFields = $GLOBALS['APPLICATION']->ConvertCharsetArray($arFields, (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET), "UTF-8");
				}
				$oHttpClient = new \Bitrix\Main\Web\HttpClient(array(
					'redirect' => true,
					'redirectMax' => 10,
					'version' => '1.1',
					'disableSslVerification' => true,
					'waitResponse' => true,
					'socketTimeout' => 15,
					'streamTimeout' => 30,
					'charset' => "UTF-8",
				));
				$response = $oHttpClient->post($strUrl, $arFields);
				$arResponse = json_decode($response, true);
				if (!defined("BX_UTF") || BX_UTF !== true) {
					$arResponse = $GLOBALS['APPLICATION']->ConvertCharsetArray($arResponse, "UTF-8", (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET));
				}
				return $arResponse;
			}
		}
		return false;
	}
}