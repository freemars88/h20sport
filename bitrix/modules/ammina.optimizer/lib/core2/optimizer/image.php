<?

namespace Ammina\Optimizer\Core2\Optimizer;

use Ammina\Optimizer\Core2\Application;
use Ammina\Optimizer\Core2\Optimizer\Image\Gif;
use Ammina\Optimizer\Core2\Optimizer\Image\Jpg;
use Ammina\Optimizer\Core2\Optimizer\Image\Lazy;
use Ammina\Optimizer\Core2\Optimizer\Image\Png;
use Ammina\Optimizer\Core2\Optimizer\Image\Svg;
use Ammina\Optimizer\Core2\Optimizer\Image\WebP;
use Sabberworm\CSS\CSSList\Document;
use Sabberworm\CSS\OutputFormat;
use Sabberworm\CSS\Parser;
use Sabberworm\CSS\Value\URL;

class Image
{
	protected $arOptions = false;
	/**
	 * @var Jpg
	 */
	protected $oJpgImage = NULL;
	/**
	 * @var Png
	 */
	protected $oPngImage = NULL;
	/**
	 * @var Gif
	 */
	protected $oGifImage = NULL;
	/**
	 * @var Svg
	 */
	protected $oSvgImage = NULL;
	/**
	 * @var WebP
	 */
	public $oWebPImage = NULL;
	/**
	 * @var Lazy
	 */
	protected $oLazy = NULL;

	protected $arAllImage = array();
	public $arInlineTypes = array(
		"png" => "image/png",
		"gif" => "image/gif",
		"jpg" => "image/jpeg",
		"jpeg" => "image/jpeg",
		"svg" => "image/svg+xml",
	);
	public $arInlineTypesWebP = array(
		"png" => "image/png",
		"gif" => "image/gif",
		"jpg" => "image/jpeg",
		"jpeg" => "image/jpeg",
		"svg" => "image/svg+xml",
		"webp" => "image/webp",
	);

	public $strBaseIdent = "";

	public function __construct($arOptions)
	{
		$this->oJpgImage = new Jpg();
		$this->oPngImage = new Png();
		$this->oGifImage = new Gif();
		$this->oSvgImage = new Svg();
		$this->oWebPImage = new WebP();
		$this->oLazy = new Lazy($this);
		$this->setOptions($arOptions);
	}

	public function setOptions($arOptions)
	{
		$this->arOptions = $arOptions;
		$this->strBaseIdent = $arOptions;
		unset($this->strBaseIdent['jpg_files']['DEFAULT']);
		unset($this->strBaseIdent['png_files']['DEFAULT']);
		unset($this->strBaseIdent['gif_files']['DEFAULT']);
		unset($this->strBaseIdent['svg_files']['DEFAULT']);
		unset($this->strBaseIdent['webp_files']['DEFAULT']);
		unset($this->strBaseIdent['events']['DEFAULT']);
		unset($this->strBaseIdent['external_images']['DEFAULT']);
		unset($this->strBaseIdent['lazy']['DEFAULT']);
		unset($this->strBaseIdent['other']['DEFAULT']);

		unset($this->strBaseIdent['jpg_files']['options']['EXCLUDE_FILES']);
		unset($this->strBaseIdent['jpg_files']['options']['INCLUDE_FILES']);
		unset($this->strBaseIdent['jpg_files']['options']['EXCLUDE_WEBP_FILES']);
		unset($this->strBaseIdent['jpg_files']['options']['INCLUDE_WEBP_FILES']);

		unset($this->strBaseIdent['png_files']['options']['EXCLUDE_FILES']);
		unset($this->strBaseIdent['png_files']['options']['INCLUDE_FILES']);
		unset($this->strBaseIdent['png_files']['options']['EXCLUDE_WEBP_FILES']);
		unset($this->strBaseIdent['png_files']['options']['INCLUDE_WEBP_FILES']);
		unset($this->strBaseIdent['png_files']['options']['EXCLUDE_JPG_FILES']);
		unset($this->strBaseIdent['png_files']['options']['INCLUDE_JPG_FILES']);

		unset($this->strBaseIdent['gif_files']['options']['EXCLUDE_FILES']);
		unset($this->strBaseIdent['gif_files']['options']['INCLUDE_FILES']);

		unset($this->strBaseIdent['svg_files']['options']['EXCLUDE_FILES']);
		unset($this->strBaseIdent['svg_files']['options']['INCLUDE_FILES']);

		unset($this->strBaseIdent['webp_files']['options']['EXCLUDE_FILES']);
		unset($this->strBaseIdent['webp_files']['options']['INCLUDE_FILES']);

		unset($this->strBaseIdent['events']);

		unset($this->strBaseIdent['external_images']['options']['EXCLUDE']);
		unset($this->strBaseIdent['external_images']['options']['INCLUDE']);

		$this->strBaseIdent = md5(serialize($this->strBaseIdent));
		$this->oJpgImage->setOptions($this->arOptions['jpg_files']);
		$this->oPngImage->setOptions($this->arOptions['png_files']);
		$this->oGifImage->setOptions($this->arOptions['gif_files']);
		$this->oSvgImage->setOptions($this->arOptions['svg_files']);
		$this->oWebPImage->setOptions($this->arOptions['webp_files']);
		$this->oLazy->setOptions($this->arOptions['lazy']);
	}

	public function doOptimizePart()
	{
		$this->arAllImage = array();
		$this->doOptimize();
		$this->arAllImage = array();
	}

	public function doOptimize()
	{
		$this->oLazy->doMakeLazy();
		$arCheckTypes = array(
			"CHECK_IMG" => false,
			"CHECK_BACKGROUND" => false,
			"CHECK_DATA_SRC" => false,
			"CHECK_ALL_OTHER" => false,
		);
		foreach ($this->arOptions as $k => $v) {
			if (in_array($k, array("jpg_files", "png_files", "gif_files", "svg_files"))) {
				if (isset($v['options']['CHECK_IMG'])) {
					$arCheckTypes['CHECK_IMG'] = $arCheckTypes['CHECK_IMG'] || ($v['options']['CHECK_IMG'] == "Y");
				}
				if (isset($v['options']['CHECK_BACKGROUND'])) {
					$arCheckTypes['CHECK_BACKGROUND'] = $arCheckTypes['CHECK_BACKGROUND'] || ($v['options']['CHECK_BACKGROUND'] == "Y");
				}
				if (isset($v['options']['CHECK_DATA_SRC'])) {
					$arCheckTypes['CHECK_DATA_SRC'] = $arCheckTypes['CHECK_DATA_SRC'] || ($v['options']['CHECK_DATA_SRC'] == "Y");
				}
				if (isset($v['options']['CHECK_ALL_OTHER'])) {
					$arCheckTypes['CHECK_ALL_OTHER'] = $arCheckTypes['CHECK_ALL_OTHER'] || ($v['options']['CHECK_ALL_OTHER'] == "Y");
				}
			}
		}
		$this->arAllImage = Application::getInstance()->getParser()->getAllImages($arCheckTypes);
		foreach ($this->arAllImage as $index => $arImage) {
			if ($arImage['TYPE'] == "ATTRIBUTE") {
				$this->arAllImage[$index]['OPTIMIZE_FILE'] = $this->doOptimizeImage($arImage['IMAGE']);
				if (strlen($this->arAllImage[$index]['OPTIMIZE_FILE']) > 0) {
					Application::getInstance()->getParser()->setImageAttribute($index, $this->arAllImage[$index]['OPTIMIZE_FILE']);
				}
			} elseif ($arImage['TYPE'] == "STYLE") {
				$this->arAllImage[$index]['OPTIMIZE_STYLE'] = $this->doOptimizeStyle($arImage['STYLE']);
				if (strlen($this->arAllImage[$index]['OPTIMIZE_STYLE']) > 0) {
					Application::getInstance()->getParser()->setImageAttribute($index, $this->arAllImage[$index]['OPTIMIZE_STYLE']);
				}
				//myPrint($this->arAllImage[$index]);
			} elseif ($arImage['TYPE'] == "SCRIPT") {
				$this->arAllImage[$index]['OPTIMIZE_SCRIPT'] = $this->doOptimizeScript($arImage['SCRIPT']);
				if (strlen($this->arAllImage[$index]['OPTIMIZE_SCRIPT']) > 0) {
					Application::getInstance()->getParser()->setImageScriptContent($index, $this->arAllImage[$index]['OPTIMIZE_SCRIPT']);
				}
			}
		}
	}

	public function doOptimizeImage($strFilePath, $bAllowConvert = true, $strBaseBath = false)
	{
		$strResult = $strFilePath;
		$strFilePath = $this->checkRemoteFile($strFilePath, $strBaseBath);
		if ($strFilePath === false) {
			return $strResult;
		}
		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strFilePath)) {
			$strFilePath = urldecode($strFilePath);
		}
		$arPathInfo = pathinfo($_SERVER['DOCUMENT_ROOT'] . $strFilePath);
		$arPathInfo['extension'] = strtolower($arPathInfo['extension']);
		if (!in_array($arPathInfo['extension'], array("jpg", "jpeg", "png", "gif", "svg"))) {
			return false;
		}
		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strFilePath)) {
			return false;
		}
		if ($arPathInfo['extension'] == "jpg" || $arPathInfo['extension'] == "jpeg") {
			$strResult = $this->oJpgImage->doOptimizeImage($strFilePath);
			if ($bAllowConvert && Application::getInstance()->isSupportWebP() && $this->arOptions['jpg_files']['options']['CONVERT_WEBP'] == "Y") {
				if (!(\CAmminaOptimizer::doMathPageToRules($this->arOptions['jpg_files']['options']['EXCLUDE_WEBP_FILES'], $strFilePath) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['options']['INCLUDE_WEBP_FILES'], $strFilePath))) {
					$strResult = $this->oWebPImage->doOptimizeImage($strFilePath);
				}
			}
		} elseif ($arPathInfo['extension'] == "png") {
			$strResult = $this->oPngImage->doOptimizeImage($strFilePath);
			if ($bAllowConvert) {
				if (!Application::getInstance()->isSupportWebP() && $this->arOptions['png_files']['options']['CONVERT_JPG'] == "Y") {
					if (!(\CAmminaOptimizer::doMathPageToRules($this->arOptions['png_files']['options']['EXCLUDE_JPG_FILES'], $strFilePath) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['options']['INCLUDE_JPG_FILES'], $strFilePath))) {
						$strResult = $this->oJpgImage->doConvertAndOptimizeImage($strFilePath, array(
							"BG_COLOR" => $this->arOptions['png_files']['options']['CONVERT_JPG_BG'],
						));
					}
				} elseif (Application::getInstance()->isSupportWebP() && $this->arOptions['png_files']['options']['CONVERT_WEBP'] == "Y") {
					if (!(\CAmminaOptimizer::doMathPageToRules($this->arOptions['png_files']['options']['EXCLUDE_WEBP_FILES'], $strFilePath) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['options']['INCLUDE_WEBP_FILES'], $strFilePath))) {
						$strResult = $this->oWebPImage->doOptimizeImage($strFilePath);
					}
				}
			}
		} elseif ($arPathInfo['extension'] == "gif") {
			$strResult = $this->oGifImage->doOptimizeImage($strFilePath);
			if ($bAllowConvert) {
				if (!Application::getInstance()->isSupportWebP() && $this->arOptions['gif_files']['options']['CONVERT_JPG'] == "Y") {
					if (!(\CAmminaOptimizer::doMathPageToRules($this->arOptions['gif_files']['options']['EXCLUDE_JPG_FILES'], $strFilePath) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['options']['INCLUDE_JPG_FILES'], $strFilePath))) {
						$strResult = $this->oJpgImage->doConvertAndOptimizeImage($strFilePath, array(
							"BG_COLOR" => $this->arOptions['gif_files']['options']['CONVERT_JPG_BG'],
						));
					}
				} elseif (Application::getInstance()->isSupportWebP() && $this->arOptions['gif_files']['options']['CONVERT_WEBP'] == "Y") {
					if (!(\CAmminaOptimizer::doMathPageToRules($this->arOptions['gif_files']['options']['EXCLUDE_WEBP_FILES'], $strFilePath) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['options']['INCLUDE_WEBP_FILES'], $strFilePath))) {
						$strResult = $this->oWebPImage->doOptimizeImage($strFilePath);
					}
				}
			}
		} elseif ($arPathInfo['extension'] == "svg") {
			$strResult = $this->oSvgImage->doOptimizeImage($strFilePath);
		}
		return $strResult;
	}

	public function doOptimizeStyle($strStyle)
	{
		global $APPLICATION;
		$arIdent = array(
			"STYLE" => $strStyle,
			"IDENT" => $this->strBaseIdent,
			"IS_WEBP" => Application::getInstance()->isSupportWebP(),
		);
		$strIdent = md5(serialize($arIdent));
		$strCacheFile = "/bitrix/ammina.cache/img/ammina.optimizer/" . SITE_ID . "/style/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".css";
		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile) || Application::getInstance()->isClearCache("image") || Application::getInstance()->isClearCache("css")) {
			$oCssParser = new Parser('.style{' . $strStyle . '}');
			$oCssDocument = $oCssParser->parse();
			$this->doOptimizeCssImages($oCssDocument, $APPLICATION->GetCurPage(true));
			$strResult = trim($oCssDocument->render(OutputFormat::createCompact()));
			$strResult = substr($strResult, 7, strlen($strResult) - 8);
			\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strCacheFile, $strResult);
			return $strResult;
		}
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile)) {
			return file_get_contents($_SERVER['DOCUMENT_ROOT'] . $strCacheFile);
		}
	}

	public function doOptimizeScript($strScript)
	{
		global $APPLICATION;
		$strTmpScript = $strScript;
		$strTmpScript = str_replace('"', "'", $strTmpScript);
		$arTmpScript = explode("'", $strTmpScript);
		$arReplace = array();
		foreach ($arTmpScript as $k => $v) {
			$arPathInfo = pathinfo($v);
			if (isset($arPathInfo['extension'])) {
				$arPathInfo['extension'] = strtolower($arPathInfo['extension']);
				if (isset($this->arInlineTypes[$arPathInfo['extension']])) {
					$strNewImage = $this->doOptimizeImage($v);
					if ($strNewImage === false) {
						$strNewImage = $v;
					}
					if ($strNewImage != $v) {
						$arReplace[$v] = $strNewImage;
					}
				}
			}
		}
		if (!empty($arReplace)) {
			foreach ($arReplace as $k => $v) {
				$strScript = str_replace($k, $v, $strScript);
			}
		}
		return $strScript;
	}

	/**
	 * @param $oCssDocument Document
	 */
	protected function doOptimizeCssImages(&$oCssDocument, $strFileName)
	{
		$arAllValues = $oCssDocument->getAllValues();
		foreach ($arAllValues as $oValue) {
			if ($oValue instanceof URL) {
				/**
				 * @var $oValue URL
				 */
				$strUrl = $oValue->getURL()->getString();
				$strNewUrl = $this->doOptimizeImage($strUrl, true, $strFileName);
				if ($strNewUrl === false) {
					$strNewUrl = $strUrl;
				}
				$oValue->getURL()->setString($strNewUrl);
				if ($this->arOptions['other']['options']['INLINE_IMG'] == "Y") {
					$oValue->getURL()->setString($this->getImageInline($strNewUrl));
				}
			}
		}
	}

	protected function getImageInline($strFileName)
	{
		$strFullName = $_SERVER['DOCUMENT_ROOT'] . $strFileName;
		$iMaxFileSize = $this->arOptions['other']['options']['MAX_SIZE_INLINE'];
		if (file_exists($strFullName) && filesize($strFullName) <= $iMaxFileSize) {
			$arPathInfo = pathinfo($strFullName);
			$arAllowInlineTypes = explode(",", $this->arOptions['other']['options']['INLINE_TYPES']);
			$arPathInfo['extension'] = strtolower($arPathInfo['extension']);
			if (in_array($arPathInfo['extension'], $arAllowInlineTypes) && isset($this->arInlineTypesWebP[$arPathInfo['extension']])) {
				return 'data:' . $this->arInlineTypesWebP[$arPathInfo['extension']] . ';base64,' . base64_encode(file_get_contents($strFullName));
			}
		}
		return $strFileName;
	}

	protected function checkRemoteFile($strFileName, $strBasePath = false)
	{
		global $APPLICATION;
		if ($strBasePath === false) {
			$strBasePath = $APPLICATION->GetCurPage(true);
		}
		if (strpos($strFileName, 'data:') !== 0) {
			if (strpos($strFileName, '://') !== false || strpos($strFileName, '//') === 0) {

			} elseif (strpos($strBasePath, '://') !== false || strpos($strBasePath, '//') === 0) {
				$strFileName = \CAmminaOptimizer::Rel2AbsUrl($strBasePath, $strFileName);
			} else {
				$strFileName = Rel2Abs(dirname($strBasePath), $strFileName);
			}
		}

		if (strpos($strFileName, '://') !== false || strpos($strFileName, '//') === 0) {
			if ($this->arOptions['external_images']['options']['ACTIVE'] == "Y") {
				if (\CAmminaOptimizer::doMathPageToRules($this->arOptions['external_images']['options']['EXCLUDE'], $strFileName) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['external_images']['options']['INCLUDE'], $strFileName)) {
					return false;
				}
				$strIdent = md5($strFileName);
				$arUrl = parse_url($strFileName);
				$arPath = pathinfo($arUrl['path']);
				if (strlen($arPath['extension']) <= 0 || !in_array($arPath['extension'], array("jpg", "jpeg", "png", "gif", "svg"))) {
					return false;
				}
				$strCacheFile = "/upload/ammina.optremote/" . SITE_ID . "/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . "." . $arPath['extension'];
				if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile) || Application::getInstance()->isClearCache("image")) {
					$strContent = \CAmminaOptimizer::doRequestPageRemote($strFileName);
					if (strlen($strContent) > 0) {
						\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strCacheFile, $strContent);
					}
				}
				if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile)) {
					return $strCacheFile;
				} else {
					return false;
				}
			}
		} elseif (strpos($strFileName, 'data:') === 0) {
			return false;
		}
		return $strFileName;
	}

}
