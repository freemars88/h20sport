<?

namespace Ammina\Optimizer\Core2\Optimizer;

use Ammina\Optimizer\Core2\Application;
use Ammina\Optimizer\Core2\Driver\Js\AmminaBabelMinify;
use Ammina\Optimizer\Core2\Driver\Js\AmminaTerserJs;
use Ammina\Optimizer\Core2\Driver\Js\AmminaUglifyJs;
use Ammina\Optimizer\Core2\Driver\Js\AmminaUglifyJs2;
use Ammina\Optimizer\Core2\Driver\Js\AmminaYUICompressor;
use Ammina\Optimizer\Core2\Driver\Js\BabelMinify;
use Ammina\Optimizer\Core2\Driver\Js\MatthiasMullie;
use Ammina\Optimizer\Core2\Driver\Js\PHPWee;
use Ammina\Optimizer\Core2\Driver\Js\TerserJs;
use Ammina\Optimizer\Core2\Driver\Js\UglifyJs;
use Ammina\Optimizer\Core2\Driver\Js\UglifyJs2;
use Ammina\Optimizer\Core2\Driver\Js\YUICompressor;

class JS
{
	protected $arOptions = false;
	protected $strBaseIdent = "";
	public $arHeadersPreloadFiles = array();
	protected $arAllJSScript = array();
	protected $strMainResultFile = false;

	/**
	 * @var PHPWee
	 */
	protected $oDriverPHPWee = NULL;
	/**
	 * @var MatthiasMullie
	 */
	protected $oDriverMatthiasMullie = NULL;
	/**
	 * @var UglifyJs
	 */
	protected $oDriverUglifyJS = NULL;
	/**
	 * @var UglifyJs2
	 */
	protected $oDriverUglifyJS2 = NULL;
	/**
	 * @var TerserJs
	 */
	protected $oDriverTerserJS = NULL;
	/**
	 * @var BabelMinify
	 */
	protected $oDriverBabelMinify = NULL;
	/**
	 * @var YUICompressor
	 */
	protected $oDriverYUICompressor = NULL;
	/**
	 * @var AmminaUglifyJs
	 */
	protected $oDriverAmminaUglifyJS = NULL;
	/**
	 * @var AmminaUglifyJs2
	 */
	protected $oDriverAmminaUglifyJS2 = NULL;
	/**
	 * @var AmminaTerserJs
	 */
	protected $oDriverAmminaTerserJS = NULL;
	/**
	 * @var AmminaBabelMinify
	 */
	protected $oDriverAmminaBabelMinify = NULL;
	/**
	 * @var AmminaYUICompressor
	 */
	protected $oDriverAmminaYUICompressor = NULL;

	public function __construct($arOptions)
	{
		$this->oDriverPHPWee = new PHPWee();
		$this->oDriverMatthiasMullie = new MatthiasMullie();
		$this->oDriverUglifyJS = new UglifyJs();
		$this->oDriverUglifyJS2 = new UglifyJs2();
		$this->oDriverTerserJS = new TerserJs();
		$this->oDriverBabelMinify = new BabelMinify();
		$this->oDriverYUICompressor = new YUICompressor();
		$this->oDriverAmminaUglifyJS = new AmminaUglifyJs();
		$this->oDriverAmminaUglifyJS2 = new AmminaUglifyJs2();
		$this->oDriverAmminaTerserJS = new AmminaTerserJs();
		$this->oDriverAmminaBabelMinify = new AmminaBabelMinify();
		$this->oDriverAmminaYUICompressor = new AmminaYUICompressor();
		$this->setOptions($arOptions);
	}

	public function setOptions($arOptions)
	{
		$this->arOptions = $arOptions;
		$this->strBaseIdent = $arOptions;
		unset($this->strBaseIdent['minify']['DEFAULT']);
		unset($this->strBaseIdent['external_js']['DEFAULT']);
		unset($this->strBaseIdent['other']['DEFAULT']);
		unset($this->strBaseIdent['ext']['DEFAULT']);
		unset($this->strBaseIdent['external_js']['options']['EXCLUDE']);
		unset($this->strBaseIdent['external_js']['options']['INCLUDE']);
		unset($this->strBaseIdent['other']['options']['EXCLUDE_FILES']);
		$this->strBaseIdent = md5(serialize($this->strBaseIdent));
	}

	public function doOptimizePart()
	{
		$this->arAllJSScript = array();
		$this->strMainResultFile = false;
		$this->doOptimize();
		$this->arAllJSScript = array();
		$this->strMainResultFile = false;
	}

	public function doOptimizeJsFilesArray($arFiles)
	{
		$this->arAllJSScript = array();
		foreach ($arFiles as $strFile) {
			$this->arAllJSScript[] = array(
				"src" => $strFile,
				"OPTIMIZE_FILE" => $this->doOptimizeJsFile($strFile),
			);
		}
		$this->strMainResultFile = $this->doMakeResultFile();
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile) && filesize($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile) > 0) {
			$this->arHeadersPreloadFiles[] = array(
				"FILE" => $this->strMainResultFile,
				"TYPE" => "SCRIPT",
			);
		}
		return $this->strMainResultFile;
	}

	public function doOptimize()
	{
		//�������� ��� script
		$this->arAllJSScript = Application::getInstance()->getParser()->getAllJsScript();
		if ($this->arOptions['ext']['options']['CHECK_CORE_FILES'] == "Y") {
			$arMoved = array();
			$iMovedBefore = false;
			$bStart = false;
			foreach ($this->arAllJSScript as $i => $arScript) {
				if ($iMovedBefore === false && strlen(trim($arScript['src'])) > 0) {
					$iMovedBefore = $i;// + 1;
				}
				if ($bStart === false) {
					if (strpos($arScript['CONTENT'], 'if(!window.BX)window.BX={};') !== false) {
						$arMoved[] = $i;
						$bStart = true;
					}
				} elseif ($bStart === true) {
					if (isset($arScript['CONTENT']) && strlen($arScript['CONTENT']) > 0) {
						$arMoved[] = $i;
					} else {
						$bStart = 0;
					}
				} else {
					if ($iMovedBefore !== false) {
						//break;
						if (strpos($arScript['CONTENT'], '(window.BX||top.BX).message({') === 0) {
							$strEndPos = strpos($arScript['CONTENT'], '})');
							if ($strEndPos >= strlen($arScript['CONTENT']) - 4) {
								$arMoved[] = $i;
							}
						} elseif (strpos($arScript['CONTENT'], 'BX.message({') === 0) {
							$strEndPos = strpos($arScript['CONTENT'], '})');
							if ($strEndPos >= strlen($arScript['CONTENT']) - 4) {
								$arMoved[] = $i;
							}
						}
					}
				}
			}
			if (!empty($arMoved) && $iMovedBefore !== false) {
				foreach ($arMoved as $val) {
					//if ($val>$iMovedBefore) {
					Application::getInstance()->getParser()->moveJsScriptBefore($val, $iMovedBefore);
					//}
				}
				$this->arAllJSScript = Application::getInstance()->getParser()->getAllJsScript();
			}
		}
		$this->doOptimizeJsLinks();

		$this->strMainResultFile = $this->doMakeResultFile();

		if ($this->arOptions['other']['options']['INLINE_JS'] == "Y" && filesize($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile) <= $this->arOptions['other']['options']['MAX_SIZE_INLINE']) {
			/*foreach ($this->arAllCssLinks as $k => $arCssLink) {
				if (strlen($arCssLink['OPTIMIZE']) > 0) {
					Application::getInstance()->getParser()->removeCssLink($k);
				}
			}
			if ($this->arOptions['other']['options']['INLINE_BEFORE_BODY'] == "Y") {
				Application::getInstance()->getParser()->AddTag("//body[1]", "style", array(), file_get_contents($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile));
			} else {
				Application::getInstance()->getParser()->AddTag("//head[1]", "style", array(), file_get_contents($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile));
			}
			*/
		} else {
			$isFillAttr = false;
			foreach ($this->arAllJSScript as $k => $arJsLink) {
				if (strlen($arJsLink['OPTIMIZE_FILE']) > 0) {
					if (!$isFillAttr) {
						Application::getInstance()->getParser()->setSrcForJsScript($k, $this->strMainResultFile);
						$isFillAttr = true;
					} else {
						Application::getInstance()->getParser()->removeJsScript($k);
					}
				}
			}
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile) && filesize($_SERVER['DOCUMENT_ROOT'] . $this->strMainResultFile) > 0) {
				$this->arHeadersPreloadFiles[] = array(
					"FILE" => $this->strMainResultFile,
					"TYPE" => "SCRIPT",
				);
			}
		}

		//myPrint($this->arAllJSScript, true, $_SERVER['DOCUMENT_ROOT'] . "/1.html", true);
		//myPrint($this->strMainResultFile, true, $_SERVER['DOCUMENT_ROOT'] . "/1.html", true);
	}

	protected function doOptimizeJsLinks()
	{
		foreach ($this->arAllJSScript as $k => $arJsLink) {
			if (isset($arJsLink['src']) && strlen($arJsLink['src']) > 0 && (!isset($arJsLink['type']) || $arJsLink['type'] == "text/javascript")) {
				$this->arAllJSScript[$k]['OPTIMIZE_FILE'] = $this->doOptimizeJsFile($arJsLink['src']);
			}
			//$this->arAllCssLinks[$k]['OPTIMIZE'] = $this->doOptimizeCssFile($arCssLink['href']);
		}
	}

	protected function doOptimizeJsFile($strFileName)
	{
		global $APPLICATION;
		if (strpos($strFileName, '://') !== false || strpos($strFileName, '//') === 0) {
			return $this->doOptimizeRemoteJsFile($strFileName);
		} else {
			$strFileNameOriginal = $strFileName;
			$strFileName = explode("?", $strFileName);
			$strFileName = $strFileName[0];
			if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strFileName)) {
				return false;
			}
			$strIdent = md5(serialize(array(
				"fileName" => $strFileNameOriginal,
				"mtime" => filemtime($_SERVER['DOCUMENT_ROOT'] . $strFileName),
				"extCache" => $this->strBaseIdent,
			)));
			$strCacheFile = "/bitrix/ammina.cache/js/ammina.optimizer/" . SITE_ID . "/atom/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".js";
			$strCacheFileTmp = "/bitrix/ammina.cache/js/ammina.optimizer/" . SITE_ID . "/atom/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".tmp.js";
			$strOptimizedFileWait = "/bitrix/ammina.cache/js/ammina.optimizer/" . SITE_ID . "/atom/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".js.wait";
			$strOptimizedFileInfo = "/bitrix/ammina.cache/js/ammina.optimizer/" . SITE_ID . "/atom/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".js.info";
			if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile) || Application::getInstance()->isClearCache("js")) {
				if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait)) {
					@unlink($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait);
				}
				if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileInfo)) {
					@unlink($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileInfo);
				}
				$strContent = ';' . Application::getInstance()->normalized_file_get_content($_SERVER['DOCUMENT_ROOT'] . $strFileName) . ';';
				\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strCacheFile, '/* Ammina JS file original ' . $strFileName . ' */' . "\n" . $strContent);
			}
			$this->doCheckMinify($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileWait, $strOptimizedFileInfo);

			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile)) {
				return $strCacheFile;
			}
		}
	}

	protected function doOptimizeRemoteJsFile($strFileName)
	{
		global $APPLICATION;
		if ($this->arOptions['external_js']['options']['ACTIVE'] == "Y") {
			if (\CAmminaOptimizer::doMathPageToRules($this->arOptions['external_js']['options']['EXCLUDE'], $strFileName) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['external_js']['options']['INCLUDE'], $strFileName)) {
				return false;
			}
			$strIdent = md5($strFileName);
			$strCacheFile = "/bitrix/ammina.cache/js.remote/" . SITE_ID . "/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".js";
			if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile) || Application::getInstance()->isClearCache("js")) {
				$strContent = \CAmminaOptimizer::doRequestPageRemote($strFileName);
				if (strlen($strContent) <= 0) {
					return false;
				}
				\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strCacheFile, '/* Ammina JS remote file ' . $strFileName . ' */' . "\n" . $strContent);
			}
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile)) {
				$strResult = $this->doOptimizeJsFile($strCacheFile);
				return $strResult;
			}
		}
		return false;
	}

	protected function doCheckMinify($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileWait, $strOptimizedFileInfo)
	{
		if ($this->arOptions['minify']['options']['ACTIVE'] == "Y") {
			$bMakeMinimized = true;
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait)) {
				if (file_get_contents($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait) >= (time() - 60)) {
					$bMakeMinimized = false;
				}
			} elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileInfo)) {
				$bMakeMinimized = false;
			}
			if ($bMakeMinimized) {
				if (\CAmminaOptimizer::doMathPageToRules($this->arOptions['minify']['options']['EXCLUDE'], $strFileName) && !\CAmminaOptimizer::doMathPageToRules($this->arOptions['minify']['options']['INCLUDE'], $strFileName)) {
					$bResult = true;
					if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileInfo)) {
						$arInfo = array(
							"SOURCE" => $strFileName,
							"RESULT" => $strCacheFile,
							"SOURCE_SIZE" => filesize($_SERVER['DOCUMENT_ROOT'] . $strFileName),
							"RESULT_SIZE" => filesize($_SERVER['DOCUMENT_ROOT'] . $strCacheFile),
						);
						\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileInfo, serialize($arInfo));
					}
				} else {
					\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait, time());
					$bResult = false;
					if ($this->arOptions['minify']['options']['LIBRARY'] == "phpwee") {
						$bResult = $this->oDriverPHPWee->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['phpwee']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "matthiasmullie") {
						$bResult = $this->oDriverMatthiasMullie->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['matthiasmullie']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "yuicompressor") {
						$bResult = $this->oDriverYUICompressor->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['yuicompressor']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "uglifyjs") {
						$bResult = $this->oDriverUglifyJS->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['uglifyjs']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "uglifyjs2") {
						$bResult = $this->oDriverUglifyJS2->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['uglifyjs2']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "terserjs") {
						$bResult = $this->oDriverTerserJS->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['terserjs']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "babelminify") {
						$bResult = $this->oDriverBabelMinify->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['babelminify']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "amminayuicompressor") {
						$bResult = $this->oDriverAmminaYUICompressor->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['amminayuicompressor']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "amminauglifyjs") {
						$bResult = $this->oDriverAmminaUglifyJS->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['amminauglifyjs']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "amminauglifyjs2") {
						$bResult = $this->oDriverAmminaUglifyJS2->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['amminauglifyjs2']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "amminaterserjs") {
						$bResult = $this->oDriverAmminaTerserJS->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['amminaterserjs']);
					} elseif ($this->arOptions['minify']['options']['LIBRARY'] == "amminababelminify") {
						$bResult = $this->oDriverAmminaBabelMinify->optimizeJs($strFileName, $strCacheFile, $strCacheFileTmp, $strOptimizedFileInfo, $this->arOptions['minify']['options_variant']['amminababelminify']);
					}
				}
				if ($bResult) {
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait)) {
						@unlink($_SERVER['DOCUMENT_ROOT'] . $strOptimizedFileWait);
					}
				}
			}
		}
	}

	protected function doMakeResultFile()
	{
		global $APPLICATION;
		$arAllAtomFiles = array();
		foreach ($this->arAllJSScript as $k => $arJsScript) {
			if (strlen($arJsScript['OPTIMIZE_FILE']) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . $arJsScript['OPTIMIZE_FILE'])) {
				$arAllAtomFiles[$arJsScript['OPTIMIZE_FILE']] = array(
					"FILE" => $arJsScript['OPTIMIZE_FILE'],
					"MKTIME" => filemtime($_SERVER['DOCUMENT_ROOT'] . $arJsScript['OPTIMIZE_FILE']),
					"SIZE" => filesize($_SERVER['DOCUMENT_ROOT'] . $arJsScript['OPTIMIZE_FILE']),
				);
			}
		}
		ksort($arAllAtomFiles);
		$strIdent = md5(serialize($arAllAtomFiles) . $this->strBaseIdent);
		$strCacheFile = "/bitrix/ammina.cache/js/ammina.optimizer/" . SITE_ID . "/full/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".js";
		$strCacheFileInfo = "/bitrix/ammina.cache/js/ammina.optimizer/" . SITE_ID . "/full/" . substr($strIdent, 0, 2) . "/" . substr($strIdent, 0, 6) . "/" . $strIdent . ".js.info";
		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile) || Application::getInstance()->isClearCache("js")) {
			$strFullContent = "";
			$arInfo = array(
				"ATOM_FILES" => array(),
				"SOURCE_FILES" => array(),
				"SOURCE_FILES_SIZE" => 0,
				"RESULT_FILE_SIZE" => 0,
			);
			foreach ($this->arAllJSScript as $k => $arJsScript) {
				if (strlen($arJsScript['OPTIMIZE_FILE']) > 0) {
					$strFullContent .= file_get_contents($_SERVER['DOCUMENT_ROOT'] . $arJsScript['OPTIMIZE_FILE']);
					$arAtomInfo = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $arJsScript['OPTIMIZE_FILE'] . ".info"));
					$arInfo['ATOM_FILES'][] = $arJsScript['OPTIMIZE_FILE'];
					$arInfo['SOURCE_FILES'][] = $arAtomInfo['SOURCE'];
					$arInfo['SOURCE_FILES_SIZE'] += $arAtomInfo['SOURCE_SIZE'];
				}
			}
			\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strCacheFile, $strFullContent);
			$arInfo['RESULT_FILE_SIZE'] = filesize($_SERVER['DOCUMENT_ROOT'] . $strCacheFile);
			\CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strCacheFileInfo, serialize($arInfo));
		}
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strCacheFile)) {
			return $strCacheFile;
		}
	}
}