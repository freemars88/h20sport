<?

namespace Ammina\Optimizer\Core2\Parser;

class DOMParser extends Base
{
	/**
	 * @var \DOMDocument
	 */
	protected $DOM = NULL;
	/**
	 * @var \DOMXPath
	 */
	protected $XPath = NULL;
	protected $arAllCssLinks = array();
	protected $arAllCssNodeLinks = array();
	protected $arAllJsScript = array();
	protected $arAllJsNodeLinks = array();
	protected $arAllJsScriptWithBxLoadCss = array();
	protected $arAllJsNodeLinksWithBxLoadCss = array();
	protected $arAllImages = array();
	protected $arAllImagesNodeLinks = array();
	protected $arAllLazy = array();
	protected $arAllLazyAttrLinks = array();
	protected $arImageTypes = array(
		"jpg" => "jpg",
		"jpeg" => "jpeg",
		"png" => "png",
		"gif" => "gif",
		"svg" => "svg",
	);
	protected $bPartIsInBodyAdd = false;


	public function doParse($strHTML)
	{
		global $APPLICATION;
		//������ ������� ����� script ����������
		$arScriptContent = array();
		$arHtml = explode("</script>", $strHTML);
		$index = 1;
		foreach ($arHtml as $k => $v) {
			$iStart = strpos($v, '<script');
			if ($iStart !== false) {
				$iStartContent = strpos($v, '>', $iStart + 1);
				$strCont = substr($v, $iStartContent + 1);
				$arScriptContent[$index] = trim($strCont);
				$arHtml[$k] = substr($v, 0, $iStartContent + 1) . $index;
				$index++;
			}
		}
		$strHTML = implode("</script>", $arHtml);

		$this->strHTML = $strHTML;
		if (!defined("BX_UTF") || BX_UTF !== true) {
			$this->DOM = new \DOMDocument("", (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET));
		} else {
			$this->DOM = new \DOMDocument("", "UTF-8");
		}
		$this->DOM->preserveWhiteSpace = false;
		$this->DOM->formatOutput = true;
		@$this->DOM->loadHTML($this->strHTML);
		$this->XPath = new \DOMXPath($this->DOM);
		$this->XPath->registerNamespace("php", "http://php.net/xpath");
		$this->XPath->registerPHPFunctions();
		$oScripts = $this->XPath->query("//script");
		for ($i = 0; $i < $oScripts->length; $i++) {
			$oNode = $oScripts->item($i);
			$index = intval($oNode->nodeValue);
			if ($index > 0) {
				$oNode->nodeValue = "";
				$oCData = $this->DOM->createCDATASection($this->doNormalizeCharsetValue($arScriptContent[$index]));
				$oNode->appendChild($oCData);
			}
		}
	}

	public function doCheckHeadEncode($strHTML, $strEncode)
	{
		$tmpHtml = str_replace('"', "", $strHTML);
		$tmpHtml = str_replace('\'', "", $tmpHtml);
		if (stripos($tmpHtml, 'http-equiv=Content-Type') === false) {
			$strHTML = str_replace('</head>', '<meta http-equiv="Content-Type" content="text/html; charset=' . $strEncode . '"></head>', $strHTML);
		}
		return $strHTML;
	}

	public function doParsePart($strHTML, $strEncode = "")
	{
		global $APPLICATION;
		$this->bPartIsInBodyAdd = false;
		if (strlen($strEncode) <= 0) {
			if (!defined("BX_UTF") || BX_UTF !== true) {
				$strEncode = (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET);
			} else {
				$strEncode = "UTF-8";
			}
		}
		if (strpos($strHTML, '<body') === false) {
			$strHTML = '<!DOCTYPE html><html xml:lang="ru" lang="ru"><head><title>test</title><meta http-equiv="Content-Type" content="text/html; charset=' . $strEncode . '"></head><body><div>' . $strHTML . '</div></body></html>';
			$this->bPartIsInBodyAdd = true;
		} else {
			$strHTML = $this->doCheckHeadEncode($strHTML, $strEncode);
		}
		//������ ������� ����� script ����������
		$arScriptContent = array();
		$arHtml = explode("</script>", $strHTML);
		$index = 1;
		foreach ($arHtml as $k => $v) {
			$iStart = strpos($v, '<script');
			if ($iStart !== false) {
				$iStartContent = strpos($v, '>', $iStart + 1);
				$strCont = substr($v, $iStartContent + 1);
				$arScriptContent[$index] = trim($strCont);
				$arHtml[$k] = substr($v, 0, $iStartContent + 1) . $index;
				$index++;
			}
		}
		$strHTML = implode("</script>", $arHtml);

		$this->strHTML = $strHTML;
		$this->DOM = new \DOMDocument("", $strEncode);
		$this->DOM->preserveWhiteSpace = false;
		$this->DOM->formatOutput = true;
		@$this->DOM->loadHTML($this->strHTML);
		$this->XPath = new \DOMXPath($this->DOM);
		$this->XPath->registerNamespace("php", "http://php.net/xpath");
		$this->XPath->registerPHPFunctions();
		$oScripts = $this->XPath->query("//script");
		for ($i = 0; $i < $oScripts->length; $i++) {
			$oNode = $oScripts->item($i);
			$index = intval($oNode->nodeValue);
			if ($index > 0) {
				$oNode->nodeValue = "";
				$oCData = $this->DOM->createCDATASection($this->doNormalizeCharsetValue($arScriptContent[$index]));
				$oNode->appendChild($oCData);
			}
		}
	}

	public function doSaveHTML()
	{
		if ($this->bMinifiedOutput) {
			$this->DOM->preserveWhiteSpace = false;
			$this->DOM->formatOutput = false;
		} else {
			$this->DOM->preserveWhiteSpace = false;
			$this->DOM->formatOutput = true;
		}
		$this->DOM->normalizeDocument();
		return $this->DOM->saveHTML();
	}

	public function doSaveHTMLPart()
	{
		if ($this->bMinifiedOutput) {
			$this->DOM->preserveWhiteSpace = false;
			$this->DOM->formatOutput = false;
		} else {
			$this->DOM->preserveWhiteSpace = false;
			$this->DOM->formatOutput = true;
		}
		$this->DOM->normalizeDocument();
		if ($this->bPartIsInBodyAdd) {
			$strResult = trim($this->DOM->saveHTML());
			$ar = explode("<body>", $strResult, 2);
			$strResult = $ar[1];
			$ar = explode("</body>", $strResult, 2);
			$strResult = $ar[0];
			/*$oNode = $this->XPath->query("//body[1]/div[1]")->item(0);
			$strResult = trim($this->DOM->saveHTML($oNode));*/
			$strResult = substr($strResult, strpos($strResult, ">") + 1);
			$strResult = substr($strResult, 0, strrpos($strResult, "</div>"));
			return $strResult;
		} else {
			return $this->DOM->saveHTML();
		}
	}

	/**
	 * @param $oNode \DOMNode
	 */
	protected function getChildNodes(&$oNode)
	{
		?>
		<ul>
			<?
			$oChild = $oNode->childNodes;

			foreach ($oChild as $oChildNode) {
				/*
			 * var $oChildNode \DOMNode
			 */
				?>
				<li>
					<?= htmlspecialchars($oChildNode->nodeName) ?>
					<?
					if ($oNode->hasChildNodes()) {
						$this->getChildNodes($oChildNode);
					}
					?>
				</li>
				<?
			}
			?>
		</ul>
		<?
	}

	public function getAllClassesAndIdent()
	{
		$arResult = array(
			"CLASSES" => array(),
			"IDENT" => array(),
		);
		$rAllNodes = $this->XPath->query("//*[@class]|//*[@id]");
		for ($i = 0; $i < $rAllNodes->length; $i++) {
			$attrList = $rAllNodes->item($i)->attributes;
			foreach ($attrList as $attr) {
				if ($attr->localName == "class") {
					$arClasses = explode(" ", $attr->nodeValue);
					foreach ($arClasses as $class) {
						$class = strtolower(trim($class));
						if (strlen($class) > 0) {
							$arResult['CLASSES'][$class] = 1;
						}
					}
				} elseif ($attr->localName == "id") {
					$arIdent = explode(" ", $attr->nodeValue);
					foreach ($arIdent as $id) {
						if (strlen($id) > 0) {
							$arResult['IDENT'][$id] = 1;
						}
					}
				}
			}
		}
		ksort($arResult['CLASSES'], SORT_NATURAL | SORT_ASC);
		ksort($arResult['IDENT'], SORT_NATURAL | SORT_ASC);
		return $arResult;
	}

	public function getAllCssLinks()
	{
		$this->arAllCssLinks = array();
		$this->arAllCssNodeLinks = array();
		$index = 1;
		$rAllNodes = $this->XPath->query("//link");
		for ($i = 0; $i < $rAllNodes->length; $i++) {
			$this->arAllCssNodeLinks[$index] = $rAllNodes->item($i);
			$attrList = $this->arAllCssNodeLinks[$index]->attributes;
			$this->arAllCssLinks[$index] = array();
			foreach ($attrList as $attr) {
				/**
				 * @var $attr \DOMAttr
				 */
				$this->arAllCssLinks[$index][$attr->localName] = $attr->nodeValue;
			}
			if ((!isset($this->arAllCssLinks[$index]['data-skip']) || !$this->arAllCssLinks[$index]['data-skip']) && ($this->arAllCssLinks[$index]['rel'] == "stylesheet")) {
				$index++;
			} else {
				unset($this->arAllCssLinks[$index]);
				unset($this->arAllCssNodeLinks[$index]);
			}
		}
		return $this->arAllCssLinks;
	}

	public function removeCssLink($index)
	{
		if (isset($this->arAllCssNodeLinks[$index])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllCssNodeLinks[$index];
			$oNode->parentNode->removeChild($oNode);
			unset($this->arAllCssNodeLinks[$index]);
		}
	}

	public function setHrefForCssLink($index, $href)
	{
		if (isset($this->arAllCssNodeLinks[$index])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllCssNodeLinks[$index];
			$rAttributes = $oNode->attributes;
			for ($i = 0; $i < $rAttributes->length; $i++) {
				$oAttr = $rAttributes->item($i);
				if ($oAttr->nodeName == "href") {
					$oAttr->nodeValue = $href;
				}
			}
		}
	}

	public function getAllCssStyle()
	{

	}

	public function AddTag($strXPathParent, $strTag, $arAttributes = array(), $strTagContent = '', $bFirstChild = false)
	{
		$oNewTagNode = $this->DOM->createElement($strTag, $strTagContent);
		foreach ($arAttributes as $k => $v) {
			$oAttr = $this->DOM->createAttribute($k);
			if ($v !== NULL) {
				$oAttr->value = $v;
			}
			$oNewTagNode->appendChild($oAttr);
		}
		$rParentNodes = $this->XPath->query($strXPathParent);
		if ($rParentNodes->length > 0) {
			if ($bFirstChild) {
				$rParentNodes->item(0)->insertBefore($oNewTagNode, $rParentNodes->item(0)->firstChild);
			} else {
				$rParentNodes->item(0)->appendChild($oNewTagNode);
			}
		}
	}

	public function AddTagBeforeCssLink($iCssLinkIndex, $strTag, $arAttributes = array(), $strTagContent = '')
	{
		$oNewTagNode = $this->DOM->createElement($strTag, $strTagContent);
		foreach ($arAttributes as $k => $v) {
			$oAttr = $this->DOM->createAttribute($k);
			if ($v !== NULL) {
				$oAttr->value = $v;
			}
			$oNewTagNode->appendChild($oAttr);
		}
		/**
		 * @var $oLink \DOMNode
		 */
		$oLink = $this->arAllCssNodeLinks[$iCssLinkIndex];
		$oLink->parentNode->insertBefore($oNewTagNode, $oLink);
	}

	public function getAllJsScript()
	{
		$this->arAllJsScript = array();
		$this->arAllJsNodeLinks = array();
		$index = 1;
		$rAllNodes = $this->XPath->query("//script");
		for ($i = 0; $i < $rAllNodes->length; $i++) {
			$this->arAllJsNodeLinks[$index] = $rAllNodes->item($i);
			$attrList = $this->arAllJsNodeLinks[$index]->attributes;
			$this->arAllJsScript[$index] = array();
			foreach ($attrList as $attr) {
				/**
				 * @var $attr \DOMAttr
				 */
				$this->arAllJsScript[$index][$attr->localName] = $attr->nodeValue;
			}

			$this->arAllJsScript[$index]['CONTENT'] = $this->arAllJsNodeLinks[$index]->textContent;

			if ((!isset($this->arAllJsScript[$index]['data-skip']) || !$this->arAllJsScript[$index]['data-skip']) && (!isset($this->arAllJsScript[$index]['data-skip-moving']) || !$this->arAllJsScript[$index]['data-skip-moving'])) {
				$index++;
			} else {
				unset($this->arAllJsScript[$index]);
				unset($this->arAllJsNodeLinks[$index]);
			}
		}
		return $this->arAllJsScript;
	}

	public function removeJsScript($index)
	{
		if (isset($this->arAllJsNodeLinks[$index])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllJsNodeLinks[$index];
			$oNode->parentNode->removeChild($oNode);
			unset($this->arAllJsNodeLinks[$index]);
		}
	}

	public function moveJsScriptBefore($index, $iBeforeIndex)
	{
		if (isset($this->arAllJsNodeLinks[$index]) && isset($this->arAllJsNodeLinks[$iBeforeIndex])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllJsNodeLinks[$index];
			/**
			 * @var $oNodeTo \DOMNode
			 */
			$oNodeTo =& $this->arAllJsNodeLinks[$iBeforeIndex];
			$oNodeTo->parentNode->insertBefore($oNode, $oNodeTo);
			/*
			$oNode->parentNode->removeChild($oNode);
			unset($this->arAllJsNodeLinks[$index]);
			*/
		}
	}

	public function setSrcForJsScript($index, $src)
	{
		if (isset($this->arAllJsNodeLinks[$index])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllJsNodeLinks[$index];
			$rAttributes = $oNode->attributes;
			for ($i = 0; $i < $rAttributes->length; $i++) {
				$oAttr = $rAttributes->item($i);
				if ($oAttr->nodeName == "src") {
					$oAttr->nodeValue = $src;
				}
			}
		}
	}

	public function getAllImages($arCheckTypes)
	{
		$this->arAllImages = array();
		$this->arAllImagesNodeLinks = array();
		$index = 1;
		$rAllNodes = $this->XPath->query("//*");
		for ($i = 0; $i < $rAllNodes->length; $i++) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode = $rAllNodes->item($i);
			if (!$arCheckTypes['CHECK_ALL_OTHER'] && !$arCheckTypes['CHECK_BACKGROUND'] && $oNode->nodeName != "img") {
				continue;
			}
			/**
			 * @todo �������� ��� CHECK_ALL_OTHER ������������������
			 */
			if ($arCheckTypes['CHECK_ALL_OTHER'] && $oNode->nodeName == "script") {
				$strNodeValue = strtolower($oNode->nodeValue);
				foreach ($this->arImageTypes as $k => $v) {
					if (strpos($strNodeValue, "." . $k) !== false) {
						$this->arAllImages[$index] = array(
							"SCRIPT" => $oNode->nodeValue,
							"TYPE" => "SCRIPT",
						);
						$this->arAllImagesNodeLinks[$index] = $oNode;
						$index++;
						break;
					}
				}
			} else {
				$attrList = $oNode->attributes;
				foreach ($attrList as $oAttr) {
					/**
					 * @var $oAttr \DOMAttr
					 */
					if ($oAttr->localName == "src") {
						if ($arCheckTypes['CHECK_IMG']) {
							if ($this->isImage($oAttr->nodeValue)) {
								$this->arAllImages[$index] = array(
									"IMAGE" => urldecode($oAttr->nodeValue),
									"TYPE" => "ATTRIBUTE",
									"ATTR" => "SRC",
								);
								$this->arAllImagesNodeLinks[$index] = $oAttr;
								$index++;
							}
						}
					} elseif ($oAttr->localName == "data-src") {
						if ($arCheckTypes['CHECK_DATA_SRC'] || $arCheckTypes['CHECK_ALL_OTHER']) {
							if ($this->isImage($oAttr->nodeValue)) {
								$this->arAllImages[$index] = array(
									"IMAGE" => urldecode($oAttr->nodeValue),
									"TYPE" => "ATTRIBUTE",
									"ATTR" => "DATA-SRC",
								);
								$this->arAllImagesNodeLinks[$index] = $oAttr;
								$index++;
							}
						}
					} elseif ($oAttr->localName == "style") {
						if ($arCheckTypes['CHECK_BACKGROUND'] || $arCheckTypes['CHECK_ALL_OTHER']) {
							if (stripos($oAttr->nodeValue, 'url') !== false) {
								$this->arAllImages[$index] = array(
									"STYLE" => $oAttr->nodeValue,
									"TYPE" => "STYLE",
								);
								$this->arAllImagesNodeLinks[$index] = $oAttr;
								$index++;
							}
						}
					} elseif ($arCheckTypes['CHECK_ALL_OTHER']) {
						if ($this->isImage($oAttr->nodeValue)) {
							$this->arAllImages[$index] = array(
								"IMAGE" => urldecode($oAttr->nodeValue),
								"TYPE" => "ATTRIBUTE",
								"ATTR" => $oAttr->localName,
							);
							$this->arAllImagesNodeLinks[$index] = $oAttr;
							$index++;
						}
					}
				}
			}
			/*
			$this->arAllCssNodeLinks[$index] = $rAllNodes->item($i);
			$attrList = $this->arAllCssNodeLinks[$index]->attributes;
			$this->arAllCssLinks[$index] = array();
			foreach ($attrList as $attr) {
				/**
				 * @var $attr \DOMAttr
				 *//*
				$this->arAllCssLinks[$index][$attr->localName] = $attr->nodeValue;
			}
			if ((!isset($this->arAllCssLinks[$index]['data-skip']) || !$this->arAllCssLinks[$index]['data-skip']) && ($this->arAllCssLinks[$index]['rel'] == "stylesheet")) {
				$index++;
			} else {
				unset($this->arAllCssLinks[$index]);
				unset($this->arAllCssNodeLinks[$index]);
			}
*/
		}
		return $this->arAllImages;
	}

	protected function isImage($strImage)
	{
		$strImage = urldecode($strImage);
		if (strpos($strImage, '://') !== false || strpos($strImage, '//') === 0) {
			$arUrl = parse_url($strImage);
			$arPath = pathinfo($arUrl['path']);
		} else {
			$arPath = pathinfo($strImage);
		}
		if (isset($arPath['extension'])) {
			if (strpos($arPath['extension'], '?') !== false) {
				$arPath['extension'] = explode("?", $arPath['extension']);
				$arPath['extension'] = $arPath['extension'][0];
			}
			if (isset($this->arImageTypes[strtolower($arPath['extension'])])) {
				return true;
			}
		}
		return false;
	}

	public function setImageAttribute($index, $strContent)
	{
		if (isset($this->arAllImagesNodeLinks[$index])) {
			/**
			 * @var $oAttr \DOMAttr
			 */
			$oAttr =& $this->arAllImagesNodeLinks[$index];
			$oAttr->nodeValue = $strContent;
		}
	}

	public function setImageScriptContent($index, $strContent)
	{
		if (isset($this->arAllImagesNodeLinks[$index])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllImagesNodeLinks[$index];
			$oNode->nodeValue = $strContent;
		}
	}

	public function getAllLazyTags($strLazyClasses, $bLazyImg = false)
	{
		$this->arAllLazy = array();
		$this->arAllLazyAttrLinks = array();
		$arAllQuery = array();
		if ($bLazyImg) {
			$arAllQuery[] = '//img';
		}
		$arLazyClass = explode(",", $strLazyClasses);
		foreach ($arLazyClass as $k => &$v) {
			$v = trim($v);
			if (strlen($v) <= 0) {
				unset($arLazyClass[$k]);
			} else {
				$arAllQuery[] = '//*[php:functionString("amminainclassxpath",@class,"' . $v . '")="Y"]';
			}
		}
		$index = 1;
		$rAllNodes = $this->XPath->query(implode("|", $arAllQuery));
		for ($i = 0; $i < $rAllNodes->length; $i++) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode = $rAllNodes->item($i);
			if ($oNode->nodeName == "img") {
				$attrList = $oNode->attributes;
				foreach ($attrList as $oAttr) {
					/**
					 * @var $oAttr \DOMAttr
					 */
					if ($oAttr->localName == "src") {
						$this->arAllLazy[$index] = array(
							"IMAGE" => $oAttr->nodeValue,
							"TYPE" => "img",
							"ATTRIBUTE" => "src",
						);
						$this->arAllLazyAttrLinks[$index] = array(
							"NODE" => $oNode,
							"ATTR" => $oAttr,
						);
						$index++;
						break;
					}
				}
			} else {
				$attrList = $oNode->attributes;
				foreach ($attrList as $oAttr) {
					/**
					 * @var $oAttr \DOMAttr
					 */
					if ($oAttr->localName == "style") {
						$strNodeVal = $oAttr->nodeValue;
						if (strpos($strNodeVal, 'url') !== false) {
							$this->arAllLazy[$index] = array(
								"STYLE" => $strNodeVal,
								"TYPE" => "style",
								"TAG" => $oNode->nodeName,
							);
							$this->arAllLazyAttrLinks[$index] = array(
								"NODE" => $oNode,
								"ATTR" => $oAttr,
							);
							$index++;
							break;
						}
					}
				}
			}
		}
		return $this->arAllLazy;
	}

	public function setLazyAttribute($index, $strContent)
	{
		if (isset($this->arAllLazyAttrLinks[$index]['ATTR'])) {
			/**
			 * @var $oAttr \DOMAttr
			 */
			$oAttr =& $this->arAllLazyAttrLinks[$index]['ATTR'];
			$oAttr->nodeValue = $strContent;
		}
	}

	public function setLazyNodeAttribute($index, $strAttribute, $strContent)
	{
		if (isset($this->arAllLazyAttrLinks[$index]['NODE'])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllLazyAttrLinks[$index]['NODE'];
			$bExistsAttribute = false;
			$attrList = $oNode->attributes;
			foreach ($attrList as $oAttr) {
				/**
				 * @var $oAttr \DOMAttr
				 */
				if ($oAttr->localName == $strAttribute) {
					$bExistsAttribute = true;
					$oAttr->nodeValue = $strContent;
					break;
				}
			}
			if (!$bExistsAttribute) {
				/**
				 * @var $oAttr \DOMAttr
				 */
				$oAttr = $this->DOM->createAttribute($strAttribute);
				$oAttr->value = $strContent;
				$oNode->appendChild($oAttr);
			}
		}
	}

	public function addLazyClassAttribute($index, $strClass)
	{
		if (isset($this->arAllLazyAttrLinks[$index]['NODE'])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllLazyAttrLinks[$index]['NODE'];
			$bExistsAttribute = false;
			$attrList = $oNode->attributes;
			foreach ($attrList as $oAttr) {
				/**
				 * @var $oAttr \DOMAttr
				 */
				if ($oAttr->localName == "class") {
					$bExistsAttribute = true;
					$oAttr->nodeValue = $oAttr->nodeValue . " " . $strClass;
					break;
				}
			}
			if (!$bExistsAttribute) {
				/**
				 * @var $oAttr \DOMAttr
				 */
				$oAttr = $this->DOM->createAttribute("class");
				$oAttr->value = $strClass;
				$oNode->appendChild($oAttr);
			}
		}
	}

	protected function doNormalizeCharsetValue($strValue)
	{
		global $APPLICATION;
		if (!defined("BX_UTF") || BX_UTF !== true) {
			return $APPLICATION->ConvertCharset($strValue, (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET), "UTF-8");
		}
		return $strValue;
	}

	public function removeWhiteSpace()
	{
		$this->DOM->normalizeDocument();
		$oTextNodes = $this->XPath->query('//text()');
		$arSkip = array("style", "pre", "code", "script", "textarea");
		foreach ($oTextNodes as $oNode) {
			/**
			 * @var $oNode \DOMNode
			 */
			$strNodePath = $oNode->getNodePath();
			$bSkip = false;
			foreach ($arSkip as $strSkip) {
				if (strpos($strNodePath, "/" . $strSkip) !== false) {
					$bSkip = true;
					break;
				}
			}
			if ($bSkip) {
				continue;
			}
			$oNode->nodeValue = preg_replace("/\s{2,}/", " ", $oNode->nodeValue);
		}
	}

	public function removeComments()
	{
		$this->DOM->normalizeDocument();
		$oNodes = $this->XPath->query('//comment()');
		foreach ($oNodes as $oNode) {
			/**
			 * @var $oNode \DOMNode
			 */
			$strVal = $oNode->nodeValue;
			if (strpos($strVal, '[') !== 0) {
				$oNode->parentNode->removeChild($oNode);
			}
		}
	}

	public function removeScriptAttr()
	{
		$this->DOM->normalizeDocument();
		$oNodes = $this->XPath->query('//script');
		foreach ($oNodes as $oNode) {
			/**
			 * @var $oNode \DOMElement
			 */
			if ($oNode->hasAttribute("type") && strtolower($oNode->getAttribute("type")) !== 'text/javascript') {
				continue;
			}
			$oNode->removeAttribute("type");
		}
	}

	public function removeStyleAttr()
	{
		$this->DOM->normalizeDocument();
		$oNodes = $this->XPath->query('//style');
		foreach ($oNodes as $oNode) {
			/**
			 * @var $oNode \DOMElement
			 */
			if ($oNode->hasAttribute("type") && strtolower($oNode->getAttribute("type")) !== 'text/css') {
				continue;
			}
			$oNode->removeAttribute("type");
		}
	}

	public function removePreTag()
	{
		$oNodes = $this->XPath->query('//pre');
		foreach ($oNodes as $oNode) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode->parentNode->removeChild($oNode);
		}
	}

	public function getAllJsScriptWithBxLoadCss()
	{
		$this->arAllJsScriptWithBxLoadCss = array();
		$this->arAllJsNodeLinksWithBxLoadCss = array();
		$index = 1;
		$rAllNodes = $this->XPath->query("//script");
		for ($i = 0; $i < $rAllNodes->length; $i++) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode = $rAllNodes->item($i);
			$strValue = $oNode->textContent;
			if (strpos(strtolower($strValue), 'bx.loadcss([') !== false) {
				$this->arAllJsNodeLinksWithBxLoadCss[$index] = $rAllNodes->item($i);
				$this->arAllJsScriptWithBxLoadCss[$index]['CONTENT'] = $this->arAllJsNodeLinksWithBxLoadCss[$index]->textContent;
				$index++;
			}
		}
		return $this->arAllJsScriptWithBxLoadCss;
	}

	public function setJsScriptWithBxLoadCssContent($index, $strContent)
	{
		if (isset($this->arAllJsNodeLinksWithBxLoadCss[$index])) {
			/**
			 * @var $oNode \DOMNode
			 */
			$oNode =& $this->arAllJsNodeLinksWithBxLoadCss[$index];
			$oNode->nodeValue = $strContent;
		}
	}

}