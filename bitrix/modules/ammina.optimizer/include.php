<?

use Ammina\Optimizer\Core2\AppBackground;
use Bitrix\Main\Composite\Helper;
use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

class CAmminaOptimizer
{
	static protected $MODULE_TEST_PERIOD = false;
	static protected $oCssObject = false;
	static protected $oJsObject = false;
	static protected $oImgObject = false;

	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		return;
	}

	static function isTestPeriodEnd()
	{
		if (self::$MODULE_TEST_PERIOD === false) {
			self::$MODULE_TEST_PERIOD = \Bitrix\Main\Loader::includeSharewareModule("ammina.optimizer");
		}
		if (self::$MODULE_TEST_PERIOD == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED) {
			return true;
		}
		return false;
	}

	static function getTestPeriodInfo()
	{
		if (self::$MODULE_TEST_PERIOD === false) {
			self::$MODULE_TEST_PERIOD = \Bitrix\Main\Loader::includeSharewareModule("ammina.optimizer");
		}
		return self::$MODULE_TEST_PERIOD;
	}

	static public function OnEndBufferContent(&$content)
	{
		global $APPLICATION;
		/**
		 * 1. �������� CSS ������ �� �����
		 * 2. ����������� ������������� ���� � ���������� � ������
		 * 3. ������������ ����� CSS
		 * 4. ���������� ����� CSS
		 *
		 * 1. �������� JS �����
		 * 2. ������������ �����
		 * 3. ���������� �����
		 * 4. �������� inline-JS
		 * 5. ������������� inline-JS
		 * 6. ��������� ����� � inline-JS � ����� ��������
		 *
		 * 1. ��� ������� ��������� �������� ������ �� �������� (img, background)
		 * 2. ������ ������
		 * 3. ����������� �������
		 *
		 * 1. ������� HTML
		 */
		if (self::isTestPeriodEnd()) {
			return false;
		}
		if (defined('AMMINA_OPTIMIZER_STOP') && AMMINA_OPTIMIZER_STOP === true) {
			return;
		}
		if (AppBackground::isInstance()) {
			AppBackground::getInstance()->doEndContent();
		}
		if (defined('BX_CRONTAB') && BX_CRONTAB === true) {
			return;
		}
		if (\CAmminaOptimizer::doMathPageToRules(array(
			"/bitrix/admin/",
			"/bitrix/services/",
			"/bitrix/activities/",
			"/bitrix/gadgets/",
			"/bitrix/panel/",
			"/bitrix/tools/",
			"/bitrix/wizards/",

		), $APPLICATION->GetCurPage())) {
			return;
		}
		$AMMINA_OPTIMIZER_APP = \Ammina\Optimizer\Core2\Application::getInstance();
		$AMMINA_OPTIMIZER_APP->EndContent($content);
		return;
		/*
		if (\CAmminaOptimizer::doMathPageToRules(array(
			"/bitrix/admin/",
			"/bitrix/services/",
			"/bitrix/activities/",
			"/bitrix/gadgets/",
			"/bitrix/panel/",
			"/bitrix/tools/",
			"/bitrix/wizards/",

		), $APPLICATION->GetCurPage())) {
			return;
		}

		if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true)) && $_REQUEST['amopt_stop'] != "Y") {
			$arJson = @json_decode($content, true);

			if (!\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->isAjaxRequest() && !(is_object($arJson) || is_array($arJson)) && stripos($content, '<!DOCTYPE') === false && stripos($content, '<html') === false) {
				return;
			}
			if (\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->isAjaxRequest() && COption::GetOptionString("ammina.optimizer", "ajax_active", "Y") != "Y") {
				return;
			}
			if (is_object($arJson) || is_array($arJson)) {
				if (COption::GetOptionString("ammina.optimizer", "json_active", "N") == "Y") {
					if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "img_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
						self::doRecursiveOptimizeImg($arJson);
					}
					if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "css_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
						self::doRecursiveOptimizeCss($arJson);
					}
					if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "js_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
						self::doRecursiveOptimizeJs($arJson);
					}
					$content = json_encode($arJson);
				}
				return;
			}
			if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "img_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
				$oImg = new \Ammina\Optimizer\Core\Img();
				$oImg->doOptimize($content);
				unset($oImg);
			}
			if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "css_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
				$oCss = new \Ammina\Optimizer\Core\Css();
				$oCss->doOptimize($content);
				unset($oCss);
			}
			if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "js_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
				$oJs = new \Ammina\Optimizer\Core\Js();
				$oJs->doOptimize($content);
				unset($oJs);
			}
			if (!\CAmminaOptimizer::doMathPageToRules(COption::GetOptionString("ammina.optimizer", "html_disable_pages", ""), $APPLICATION->GetCurPageParam("", array(), true))) {
				$oHtml = new \Ammina\Optimizer\Core\Html();
				$oHtml->doOptimize($content);
				unset($oHtml);
			}
			$arLink = explode("\n", COption::GetOptionString("ammina.optimizer", "header_other_link", ""));
			foreach ($arLink as $strLink) {
				$strLink = trim($strLink);
				if (strlen($strLink) > 0) {
					CAmminaOptimizer::doPreLoadHeaders($strLink, $content);
					//header("Link: " . CAmminaOptimizer::doCheckPreFetch($strLink, $content), false);
				}
			}
		}
		*/
	}

	static public function doMathPageToRules($strRules, $strPage)
	{
		if (is_array($strRules)) {
			$arRules = $strRules;
		} else {
			$arRules = explode("\n", $strRules);
		}
		foreach ($arRules as $strRule) {
			$strRule = trim($strRule);
			if (strlen($strRule) > 0) {
				if (strpos($strRule, 'PREG:') === 0) {
					$strRule = trim(substr($strRule, 5));
					$strPattern = "/" . $strRule . "/ui";
					$aMatch = array();
					if (preg_match($strPattern, $strPage, $aMatch)) {
						return true;
					}
				} elseif (strpos($strRule, 'PART:') === 0) {
					$strRule = trim(substr($strRule, 5));
					if (stripos($strPage, $strRule) !== false) {
						return true;
					}
				} else {
					if (stripos($strPage, $strRule) === 0) {
						return true;
					}
				}
			}
		}
		return false;
	}

	static public function clearCacheFiles()
	{
		self::doRmDir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina.cache/css/ammina.optimizer");
		self::doRmDir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/ammina.cache/js/ammina.optimizer");
	}

	static public function doRmDir($strPath)
	{
		if (file_exists($strPath) && is_dir($strPath)) {
			$arFiles = scandir($strPath);
			foreach ($arFiles as $strFile) {
				if (in_array($strFile, array(".", ".."))) {
					continue;
				}
				$strFullName = $strPath . "/" . $strFile;
				if (is_dir($strFullName)) {
					self::doRmDir($strFullName);
					@rmdir($strFullName);
				} else {
					@unlink($strFullName);
				}
			}
		}
	}

	static public function doRecursiveOptimizeCss(&$arJson)
	{
		if (is_array($arJson)) {
			foreach ($arJson as $k => &$v) {
				self::doRecursiveOptimizeCss($arJson[$k]);
			}
		} else {
			if (self::$oCssObject === false) {
				self::$oCssObject = new \Ammina\Optimizer\Core\Css();
			}
			self::$oCssObject->doOptimize($arJson);
		}
	}

	static public function doRecursiveOptimizeJs(&$arJson)
	{
		if (is_array($arJson)) {
			foreach ($arJson as $k => &$v) {
				self::doRecursiveOptimizeJs($arJson[$k]);
			}
		} else {
			if (self::$oJsObject === false) {
				self::$oJsObject = new \Ammina\Optimizer\Core\Js();
			}
			self::$oJsObject->doOptimize($arJson);
		}
	}

	static public function doRecursiveOptimizeImg(&$arJson)
	{
		if (is_array($arJson)) {
			foreach ($arJson as $k => &$v) {
				self::doRecursiveOptimizeImg($arJson[$k]);
			}
		} else {
			if (self::$oImgObject === false) {
				self::$oImgObject = new \Ammina\Optimizer\Core\Img();
			}
			self::$oImgObject->doOptimize($arJson);
		}
	}

	static public function OnEndContentComposite($strContent)
	{
		$strContent = \Ammina\Optimizer\Core2\Application::getInstance()->doOptimizeAutocomposite($strContent);
		return $strContent;
	}

	static public function OnProlog()
	{
		global $APPLICATION;
		if (Helper::getAjaxRandom() !== false && Helper::isAjaxRequest()) {
			if (\Ammina\Optimizer\Core2\Application::getInstance()->isAllowComposite()) {
				$strCurrentContent = ob_get_contents();
				$APPLICATION->buffer_man = true;
				ob_end_clean();
				$APPLICATION->buffered = false;
				$APPLICATION->buffer_man = false;
				ob_start(array("CAmminaOptimizer", "OnEndContentComposite"));
				ob_start(array(&$APPLICATION, "EndBufferContent"));
				echo $strCurrentContent;
			}
		}

		if (isset($GLOBALS['AOPT_SETAGENT_NEXT']) && is_array($GLOBALS['AOPT_SETAGENT_NEXT'])) {
			foreach ($GLOBALS['AOPT_SETAGENT_NEXT'] as $k => $v) {
				\CAgent::Update($k, array(
					"NEXT_EXEC" => $v,
				));
			}
		}
		if (strpos($APPLICATION->GetCurPage(), '/bitrix/admin/') === 0) {
			$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/ammina.optimizer.css");
		} else {
			header("x-ammina-module: optimizer" . (in_array(self::getTestPeriodInfo(), array(\Bitrix\Main\Loader::MODULE_DEMO, \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED)) ? ", demo" : ""), false);
			if (self::isTestPeriodEnd()) {
				return false;
			}
			$AMMINA_OPTIMIZER_APP = \Ammina\Optimizer\Core2\Application::getInstance();
			if ($APPLICATION->GetGroupRight("ammina.optimizer") >= "W") {
				$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/ammina.optimizer.pub.css");
				$arMenu[] = array(
					"TEXT" => Loc::getMessage("AMMINA_OPTIMIZER_PANEL_BUTTON_SHOWSTAT"),
					"TITLE" => Loc::getMessage("AMMINA_OPTIMIZER_PANEL_BUTTON_SHOWSTAT_TITLE"),
					"CHECKED" => $AMMINA_OPTIMIZER_APP->isShowStat(),
					"ACTION" => "jsUtils.Redirect([], '" . CUtil::addslashes($APPLICATION->GetCurPageParam("amopt_showstat=" . ($AMMINA_OPTIMIZER_APP->isShowStat() ? "N" : "Y"), array("amopt_clear_cache", "amopt_showstat"))) . "');",
					"HK_ID" => "top_panel_debug_compr",
				);
				$arMenu[] = array("SEPARATOR" => true);
				$APPLICATION->AddPanelButton(array(
					"HREF" => $APPLICATION->GetCurPageParam("amopt_clear_cache=Y", array("amopt_clear_cache")),
					"TYPE" => "BIG",
					"ICON" => "bx-panel-clear-cache-icon",
					"TEXT" => Loc::getMessage("AMMINA_OPTIMIZER_PANEL_BUTTON_CLEAR"),
					"ALT" => Loc::getMessage("AMMINA_OPTIMIZER_PANEL_BUTTON_CLEAR"),
					"MAIN_SORT" => "3000",
					"SORT" => 10,
					"MENU" => $arMenu,
					"HK_ID" => "amopt_top_panel_clear_cache",
					/*"HINT" => array(
						"TITLE" => GetMessage("top_panel_cache_new_tooltip_title"),
						"TEXT" => GetMessage("top_panel_cache_new_tooltip"),
					),*/
				));
			}
		}
	}

	static public function doRequestPageRemote($strUrl, $strUserAgent = '')
	{
		if (!function_exists("curl_init")) {
			return false;
		}
		$strResult = false;
		$oCurl = curl_init($strUrl);
		if ($oCurl) {
			curl_setopt($oCurl, CURLOPT_AUTOREFERER, true);
			curl_setopt($oCurl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($oCurl, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($oCurl, CURLOPT_CONNECTTIMEOUT, 15);
			curl_setopt($oCurl, CURLOPT_TIMEOUT, 60);
			curl_setopt($oCurl, CURLOPT_MAXREDIRS, 30);
			curl_setopt($oCurl, CURLOPT_ENCODING, "gzip");
			curl_setopt($oCurl, CURLOPT_HEADER, false);
			if (strlen($strUserAgent) > 0) {
				curl_setopt($oCurl, CURLOPT_USERAGENT, $strUserAgent);
			}
			$strResult = curl_exec($oCurl);
			$arInfo = curl_getinfo($oCurl);
			if ($arInfo['http_code'] != "200") {
				$strResult = false;
			}/* else {
				$arSections = explode("\x0d\x0a\x0d\x0a", $strResult, 2);
				while (!strncmp($arSections[1], 'HTTP/', 5)) {
					$arSections = explode("\x0d\x0a\x0d\x0a", $arSections[1], 2);
				}
				$strHeaders = $arSections[0];
				$strResult = $arSections[1];
				/*if (preg_match('/^Content-Encoding: gzip/mi', $strHeaders)) {
					$strResult = self::gunzip($strResult);
				}
				*//*
			}*/
			curl_close($oCurl);
		}
		return $strResult;
	}

	/*
		static public function gunzip($zipped)
		{
			$offset = 0;
			return gzinflate(substr($zipped, $offset + 10));
			if (substr($zipped, 0, 2) == "\x1f\x8b")
				$offset = 2;
			if (substr($zipped, $offset, 1) == "\x08") {
				return gzinflate(substr($zipped, $offset + 8));
			}
			return false;
		}
	*/
	static public function Rel2AbsUrl($strBaseUrl, $strPath)
	{
		$arUrlBase = parse_url($strBaseUrl);
		$arUrlFile = parse_url($strPath);
		$strResult = "";
		$arNew = array();
		if (strlen($arUrlFile['scheme']) > 0) {
			$arNew['scheme'] = $arUrlFile['scheme'];
			$arNew['host'] = $arUrlFile['host'];
			$arNew['path'] = $arUrlFile['path'];
			$arNew['query'] = $arUrlFile['query'];
			$arNew['fragment'] = $arUrlFile['fragment'];
		} else {
			$arNew['scheme'] = $arUrlBase['scheme'];
			if (strlen($arUrlFile['host']) > 0) {
				$arNew['host'] = $arUrlFile['host'];
				$arNew['path'] = $arUrlFile['path'];
				$arNew['query'] = $arUrlFile['query'];
				$arNew['fragment'] = $arUrlFile['fragment'];
			} else {
				$arNew['host'] = $arUrlBase['host'];
				if (strlen($arUrlFile['path']) > 0) {
					$arPath = pathinfo($arUrlBase['path']);
					$strPathDir = $arPath['dirname'] . "/";
					if (strlen($arPath['extension']) <= 0) {
						$strPathDir = $arUrlBase['path'];
					}
					$arNew['path'] = Rel2Abs($strPathDir, $arUrlFile['path']);
					$arNew['query'] = $arUrlFile['query'];
					$arNew['fragment'] = $arUrlFile['fragment'];
				} else {
					$arNew['path'] = $arUrlBase['path'];
					if (strlen($arUrlFile['query']) > 0) {
						$arNew['query'] = $arUrlFile['query'];
						$arNew['fragment'] = $arUrlFile['fragment'];
					} else {
						$arNew['query'] = $arUrlBase['query'];
						if (strlen($arUrlFile['fragment']) > 0) {
							$arNew['fragment'] = $arUrlFile['fragment'];
						} else {
							$arNew['fragment'] = $arUrlBase['fragment'];
						}
					}
				}
			}
		}

		if (strlen($arNew['scheme']) > 0) {
			$strResult .= $arNew['scheme'] . "://";
		}
		if (strlen($arNew['host']) > 0) {
			$strResult .= $arNew['host'];
		}
		if (strlen($arNew['path']) > 0) {
			$strResult .= $arNew['path'];
		}
		if (strlen($arNew['query']) > 0) {
			$strResult .= "?" . $arNew['query'];
		}
		if (strlen($arNew['fragment']) > 0) {
			$strResult .= "#" . $arNew['fragment'];
		}

		return $strResult;
	}

	public static function SaveFileContent($abs_path, $strContent)
	{
		CheckDirPath(dirname($abs_path) . "/");
		file_put_contents($abs_path, $strContent);
		@chmod($abs_path, BX_FILE_PERMISSIONS);
	}

	public static function showSupportForm()
	{
		if (COption::GetOptionString("ammina.optimizer", "show_support_form", "Y") == "Y") {
			$oCache = new CPHPCache();
			$strCode = "";
			if ($oCache->InitCache(3600, 'ammina_support', 'ammina/support')) {
				$res = $oCache->GetVars();
				$strCode = $res['CODE'];
			}
			if ($oCache->StartDataCache()) {
				$client = new \Bitrix\Main\Web\HttpClient(array(
					'redirect' => true,
					'redirectMax' => 10,
					'socketTimeout' => 15,
					'streamTimeout' => 15,
					'disableSslVerification' => true,
				));
				$strCode = $client->get("https://www.ammina24.ru/upload/support.widget.txt");
				$status = intval($client->getStatus());
				if ($status != 200) {
					$strCode = "";
				}
				$oCache->EndDataCache(array(
					"CODE" => $strCode,
				));
			}
			if (strlen($strCode) > 0) {
				echo $strCode;
			}
		}
	}

	public static function getExtOptionsForFiles($type = "css")
	{
		$arCacheOptions = array(
			"css" => array("css_fontface", "css_minify_active", "css_minify_type", "css_incimages", "css_incimages_maxsize", "css_files_no_optimize", "css_files_no_minimize", "css_path_yuicompressor", "css_path_uglifycss", "css_remote_active", "css_remote_disable_links", "css_remote_googlefonts_type"),
			"js" => array("js_minify_active", "js_minify_type", "js_files_no_optimize", "js_files_no_minimize", "js_bxcore_files_active", "js_path_uglifyjs", "js_path_yuicompressor", "js_path_uglifyjs2", "js_path_terserjs", "js_path_babelminify", "js_remote_active", "js_remote_disable_links"),
		);
		$arCache = array();
		foreach ($arCacheOptions[$type] as $val) {
			$arCache[$val] = COption::GetOptionString("ammina.optimizer", $val, "");
		}
		return serialize($arCache);
	}

	public static function doPreLoadHeaders($strLink, &$strContent)
	{
		$lastStatus = CHTTP::GetLastStatus();
		if (strlen($lastStatus) <= 0 || strtoupper($lastStatus) == "200 OK") {
			header("Link: " . $strLink, false);
			if (COption::GetOptionString("ammina.optimizer", "header_link_to_html", "Y") == "Y") {
				CAmminaOptimizer::doCheckLinkToHtml($strLink, $strContent);
			}
			if (COption::GetOptionString("ammina.optimizer", "header_prefetch", "Y") == "Y") {
				$strFetch = str_replace('rel=preload;', 'rel=prefetch;', $strLink);
				header("Link: " . $strFetch, false);
				if (COption::GetOptionString("ammina.optimizer", "header_link_to_html", "Y") == "Y") {
					CAmminaOptimizer::doCheckLinkToHtml($strFetch, $strContent);
				}
			}
		}
	}

	public static function doCheckLinkToHtml($strLink, &$strContent)
	{
		$arData = explode(";", $strLink);
		$arLinkData = array();
		foreach ($arData as $val) {
			$val = trim($val);
			if (substr($val, 0, 1) == '<') {
				$arLinkData[] = 'href="' . substr($val, 1, strlen($val) - 2) . '"';
			} elseif (strpos($val, 'rel=') === 0) {
				$arLinkData[] = 'rel="' . substr($val, 4) . '"';
			} elseif (strpos($val, 'as=') === 0) {
				$arLinkData[] = 'as="' . substr($val, 3) . '"';
			} elseif (strpos($val, 'crossorigin') === 0) {
				$arLinkData[] = 'crossorigin="anonymous"';
			}
		}
		if (!empty($arLinkData)) {
			$strNewLink = '<link ' . implode(" ", $arLinkData) . ' />';
			$strContent = str_replace('</head>', $strNewLink . "\n" . "</head>", $strContent);
		}
	}

	/**
	 * @param $strTypeRequest (GET_WORK_SERVER)
	 * @param $arFields
	 *
	 * @return mixed
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function doRequestAmminaServer($strTypeRequest, $arFields = array())
	{
		$arFields['k'] = COption::GetOptionString("ammina.optimizer", "ammina_apikey", "");
		$lk = COption::GetOptionString("ammina.optimizer", "ammina_lkey", "");
		$lktime = COption::GetOptionString("ammina.optimizer", "ammina_lkeytime", "");
		if (strlen($lk) <= 0 || $lktime <= 0 || $lktime < (time() - 3600)) {
			include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client.php");
			include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client_partner.php");
			$lk = md5('BITRIX' . CUpdateClientPartner::GetLicenseKey() . 'LICENSE');
			COption::SetOptionString("ammina.optimizer", "ammina_lkey", $lk);
			COption::SetOptionString("ammina.optimizer", "ammina_lkeytime", time());
		}
		$arFields['l'] = $lk;
		$arFields['t'] = $strTypeRequest;
		if (!defined("BX_UTF") || BX_UTF !== true) {
			$arFields = $GLOBALS['APPLICATION']->ConvertCharsetArray($arFields, (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET), "UTF-8");
		}
		$strUrl = "https://www.fastbitrix.ru/api/api.php";
		$oHttpClient = new \Bitrix\Main\Web\HttpClient(array(
			'redirect' => true,
			'redirectMax' => 10,
			'version' => '1.1',
			'disableSslVerification' => true,
			'waitResponse' => true,
			'socketTimeout' => 15,
			'streamTimeout' => 30,
			'charset' => "UTF-8",
		));
		$response = $oHttpClient->post($strUrl, $arFields);
		$arResponse = json_decode($response, true);
		if (!defined("BX_UTF") || BX_UTF !== true) {
			$arResponse = $GLOBALS['APPLICATION']->ConvertCharsetArray($arResponse, "UTF-8", (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET));
		}
		return $arResponse;
	}

	/**
	 * @param $strTypeRequest
	 * @param $arFields
	 */
	public static function doRequestWorkServer($strTypeRequest, $arFields = array())
	{
		global $APPLICATION;
		$arFields['SYSTEM'] = array(
			"HOST" => $_SERVER['HTTP_HOST'],
			"HTTPS" => $APPLICATION->IsHttps(),
		);
		$strUrl = COption::GetOptionString("ammina.optimizer", "ammina_workurl", "");
		$urltime = COption::GetOptionString("ammina.optimizer", "ammina_workurltime", "");
		if (strlen($strUrl) <= 0 || $urltime <= 0 || $urltime < (time() - 3600)) {
			$arUrlResponse = self::doRequestAmminaServer("GET_WORK_SERVER");
			if ($arUrlResponse['status'] == "ok") {
				$strUrl = $arUrlResponse['url'];
				$urltime = time();
				COption::SetOptionString("ammina.optimizer", "ammina_workurl", $strUrl);
				COption::SetOptionString("ammina.optimizer", "ammina_workurltime", $urltime);
			}
		}
		if (strlen($strUrl) > 0) {
			$arFields['k'] = COption::GetOptionString("ammina.optimizer", "ammina_apikey", "");
			$lk = COption::GetOptionString("ammina.optimizer", "ammina_lkey", "");
			$lktime = COption::GetOptionString("ammina.optimizer", "ammina_lkeytime", "");
			if (strlen($lk) <= 0 || $lktime <= 0 || $lktime < (time() - 3600)) {
				include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client.php");
				include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/classes/general/update_client_partner.php");
				$lk = md5('BITRIX' . CUpdateClientPartner::GetLicenseKey() . 'LICENSE');
				COption::SetOptionString("ammina.optimizer", "ammina_lkey", $lk);
				COption::SetOptionString("ammina.optimizer", "ammina_lkeytime", time());
			}
			$arFields['l'] = $lk;
			$arFields['t'] = $strTypeRequest;
			if (!defined("BX_UTF") || BX_UTF !== true) {
				$arFields = $GLOBALS['APPLICATION']->ConvertCharsetArray($arFields, (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET), "UTF-8");
			}
			$oHttpClient = new \Bitrix\Main\Web\HttpClient(array(
				'redirect' => true,
				'redirectMax' => 10,
				'version' => '1.1',
				'disableSslVerification' => true,
				'waitResponse' => true,
				'socketTimeout' => 15,
				'streamTimeout' => 30,
				'charset' => "UTF-8",
			));
			$response = $oHttpClient->post($strUrl, $arFields);
			$arResponse = json_decode($response, true);
			if (!defined("BX_UTF") || BX_UTF !== true) {
				$arResponse = $GLOBALS['APPLICATION']->ConvertCharsetArray($arResponse, "UTF-8", (strlen(LANG_CHARSET) > 0 ? LANG_CHARSET : SITE_CHARSET));
			}
			return $arResponse;
		}
		return false;
	}

	public static function doRequestWorkServerResultFile($strFileUrl, $strFileResult, $strFileNameFileUrl)
	{
		$oHttpClient = new \Bitrix\Main\Web\HttpClient(array(
			'redirect' => true,
			'redirectMax' => 10,
			'version' => '1.1',
			'disableSslVerification' => true,
			'waitResponse' => true,
			'socketTimeout' => 15,
			'streamTimeout' => 30,
			'charset' => "UTF-8",
		));
		$response = $oHttpClient->get($strFileUrl);
		$status = intval($oHttpClient->getStatus());
		if ($status != 200) {
			return false;
		}
		CAmminaOptimizer::SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $strFileResult, $response);
		@chmod($_SERVER['DOCUMENT_ROOT'] . $strFileResult, BX_FILE_PERMISSIONS);
		@unlink($_SERVER["DOCUMENT_ROOT"] . $strFileNameFileUrl);
		return true;
	}

	public static function OnFileSave(&$arFile, $strFileName, $strSavePath, $bForceMD5, $bSkipExt, $dirAdd)
	{
		$arPath = pathinfo($strFileName);
		if (in_array(strtolower($arPath['extension']), array("jpg", "jpeg", "png", "gif", "svg"))) {
			$upload_dir = COption::GetOptionString("main", "upload_dir", "upload");
			$io = CBXVirtualIo::GetInstance();
			if ($bForceMD5 != true && COption::GetOptionString("main", "save_original_file_name", "N") == "Y") {
				$dir_add = $dirAdd;
				if ($dir_add == '') {
					$i = 0;
					while (true) {
						$dir_add = substr(md5(uniqid("", true)), 0, 3);
						if (!$io->FileExists($_SERVER["DOCUMENT_ROOT"] . "/" . $upload_dir . "/" . $strSavePath . "/" . $dir_add . "/" . $strFileName)) {
							break;
						}
						if ($i >= 25) {
							$j = 0;
							while (true) {
								$dir_add = substr(md5(mt_rand()), 0, 3) . "/" . substr(md5(mt_rand()), 0, 3);
								if (!$io->FileExists($_SERVER["DOCUMENT_ROOT"] . "/" . $upload_dir . "/" . $strSavePath . "/" . $dir_add . "/" . $strFileName)) {
									break;
								}
								if ($j >= 25) {
									$dir_add = substr(md5(mt_rand()), 0, 3) . "/" . md5(mt_rand());
									break;
								}
								$j++;
							}
							break;
						}
						$i++;
					}
				}
				if (substr($strSavePath, -1, 1) <> "/") {
					$strSavePath .= "/" . $dir_add;
				} else {
					$strSavePath .= $dir_add . "/";
				}
			} else {
				$strFileExt = ($bSkipExt == true || ($ext = GetFileExtension($strFileName)) == '' ? '' : "." . $ext);
				while (true) {
					if (substr($strSavePath, -1, 1) <> "/") {
						$strSavePath .= "/" . substr($strFileName, 0, 3);
					} else {
						$strSavePath .= substr($strFileName, 0, 3) . "/";
					}

					if (!$io->FileExists($_SERVER["DOCUMENT_ROOT"] . "/" . $upload_dir . "/" . $strSavePath . "/" . $strFileName))
						break;

					//try the new name
					$strFileName = md5(uniqid("", true)) . $strFileExt;
				}
			}

			$arFile["SUBDIR"] = $strSavePath;
			$arFile["FILE_NAME"] = $strFileName;
			$strDirName = $_SERVER["DOCUMENT_ROOT"] . "/" . $upload_dir . "/" . $strSavePath . "/";
			$strDbFileNameX = $strDirName . $strFileName;
			$strPhysicalFileNameX = $io->GetPhysicalName($strDbFileNameX);

			CheckDirPath($strDirName);

			if (is_set($arFile, "content")) {
				$f = fopen($strPhysicalFileNameX, "w");
				if (!$f)
					return false;
				if (fwrite($f, $arFile["content"]) === false)
					return false;
				fclose($f);
			} elseif (
				!copy($arFile["tmp_name"], $strPhysicalFileNameX)
				&& !move_uploaded_file($arFile["tmp_name"], $strPhysicalFileNameX)
			) {
				CFile::DoDelete($arFile["old_file"]);
				return false;
			}

			if (isset($arFile["old_file"]))
				CFile::DoDelete($arFile["old_file"]);

			@chmod($strPhysicalFileNameX, BX_FILE_PERMISSIONS);

			//flash is not an image
			$flashEnabled = !CFile::IsImage($arFile["ORIGINAL_NAME"], $arFile["type"]);

			$imgArray = CFile::GetImageSize($strDbFileNameX, false, $flashEnabled);

			if (is_array($imgArray)) {
				$arFile["WIDTH"] = $imgArray[0];
				$arFile["HEIGHT"] = $imgArray[1];

				if ($imgArray[2] == IMAGETYPE_JPEG) {
					$exifData = CFile::ExtractImageExif($strPhysicalFileNameX);
					if ($exifData && isset($exifData['Orientation'])) {
						//swap width and height
						if ($exifData['Orientation'] >= 5 && $exifData['Orientation'] <= 8) {
							$arFile["WIDTH"] = $imgArray[1];
							$arFile["HEIGHT"] = $imgArray[0];
						}

						$properlyOriented = CFile::ImageHandleOrientation($exifData['Orientation'], $io->GetPhysicalName($strDbFileNameX));
						if ($properlyOriented) {
							$jpgQuality = intval(COption::GetOptionString('main', 'image_resize_quality', '95'));
							if ($jpgQuality <= 0 || $jpgQuality > 100)
								$jpgQuality = 95;

							imagejpeg($properlyOriented, $strPhysicalFileNameX, $jpgQuality);
							clearstatcache(true, $strPhysicalFileNameX);
						}

						$arFile['size'] = filesize($strPhysicalFileNameX);
					}
				}
			} else {
				$arFile["WIDTH"] = 0;
				$arFile["HEIGHT"] = 0;
			}
			$strFullFileName = "/" . $upload_dir . "/" . $arFile["SUBDIR"] . "/" . $arFile["FILE_NAME"];
			\Ammina\Optimizer\Core2\AppBackground::getInstance()->doOptimizeImage($strFullFileName);
			return true;
		}
	}

	public static function OnAfterResizeImage($file, $arSizeParams, &$callbackData, &$cacheImageFile, &$cacheImageFileTmp, &$arImageSize)
	{
		$strFullFileName = $_SERVER["DOCUMENT_ROOT"] . $cacheImageFile;
		$arPath = pathinfo($strFullFileName);
		if (in_array(strtolower($arPath['extension']), array("jpg", "jpeg", "png", "gif", "svg"))) {
			\Ammina\Optimizer\Core2\AppBackground::getInstance()->doOptimizeImage($cacheImageFile);
		}
	}


}

function amopt_substr_count($haystack, $needle, $offset = NULL, $length = NULL)
{
	if (function_exists('mb_strlen') && ((int)ini_get('mbstring.func_overload') & 2)) {
		//$mbIntEnc = NULL;
		//$mbIntEnc = mb_internal_encoding();
		//mb_internal_encoding('8bit');
		if (!is_null($offset) || !is_null($length)) {
			$checkString = substr($haystack, $offset, $length);
			$result = substr_count($checkString, $needle);
		} else {
			$result = substr_count($haystack, $needle);
		}
		//if ($mbIntEnc !== NULL) {
		//	mb_internal_encoding($mbIntEnc);
		//}
		return $result;
	} else {
		return substr_count($haystack, $needle, $offset, $length);
	}
}

CModule::AddAutoloadClasses(
	"ammina.optimizer", array(
		"Ammina\\Optimizer\\Core\\Base" => "lib/core/base.php",
		"Ammina\\Optimizer\\Core\\Css" => "lib/core/css.php",
		"Ammina\\Optimizer\\Core\\Js" => "lib/core/js.php",
		"Ammina\\Optimizer\\Core\\Html" => "lib/core/html.php",
		"Ammina\\Optimizer\\Core\\Img" => "lib/core/img.php",

		"Ammina\\Optimizer\\PageTable" => "lib/page.php",
		"Ammina\\Optimizer\\HistoryTable" => "lib/history.php",
		"Ammina\\Optimizer\\SettingsTable" => "lib/settings.php",
		"Ammina\\Optimizer\\StatTypesTable" => "lib/stat.types.php",
		"Ammina\\Optimizer\\FilesOptimizedTable" => "lib/files.optimized.php",
		"Ammina\\Optimizer\\FilesOriginalsTable" => "lib/files.original.php",

		"Ammina\\Optimizer\\Agent\\CheckPage" => "lib/agent/check.page.php",
		"Ammina\\Optimizer\\Agent\\CheckImages" => "lib/agent/check.images.php",

		"Ammina\\Optimizer\\Workers\\Cache\\Stat" => "lib/workers/cache/stat.php",
		"Ammina\\Optimizer\\Workers\\Cache\\Clear" => "lib/workers/cache/clear.php",
		"Ammina\\Optimizer\\Workers\\Cache\\Files" => "lib/workers/cache/files.php",

		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\Page" => "lib/helpers/admin/blocks/page.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\PageMonitoring" => "lib/helpers/admin/blocks/page.monitoring.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\History" => "lib/helpers/admin/blocks/history.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\HistoryInfo" => "lib/helpers/admin/blocks/history.info.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringInfo" => "lib/helpers/admin/blocks/monitoring.info.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringBase" => "lib/helpers/admin/blocks/monitoring.base.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringPerformance" => "lib/helpers/admin/blocks/monitoring.performance.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringAccessibility" => "lib/helpers/admin/blocks/monitoring.accessibility.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringBestPractices" => "lib/helpers/admin/blocks/monitoring.best.practices.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringSeo" => "lib/helpers/admin/blocks/monitoring.seo.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\MonitoringPwa" => "lib/helpers/admin/blocks/monitoring.pwa.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\StatFileOptimized" => "lib/helpers/admin/blocks/stat.file.optimized.php",
		"Ammina\\Optimizer\\Helpers\\Admin\\Blocks\\StatFileOriginal" => "lib/helpers/admin/blocks/stat.file.original.php",

		"PHPWee\\Minify" => "lib/ext/phpmin/phpwee.php",
		"PHPWee\\CssMin" => "lib/ext/phpmin/src/CssMin/CssMin.php",
		"PHPWee\\HtmlMin" => "lib/ext/phpmin/src/HtmlMin/HtmlMin.php",
		"PHPWee\\JsMin" => "lib/ext/phpmin/src/JsMin/JsMin.php",

		"Psr\\Cache\\InvalidArgumentException" => "lib/ext/psr/InvalidArgumentException.php",
		"Psr\\Cache\\CacheException" => "lib/ext/psr/CacheException.php",
		"Psr\\Cache\\CacheItemInterface" => "lib/ext/psr/CacheItemInterface.php",
		"Psr\\Cache\\CacheItemPoolInterface" => "lib/ext/psr/CacheItemPoolInterface.php",
		"Psr\\Log\\AbstractLogger" => "lib/ext/psr/Log/AbstractLogger.php",
		"Psr\\Log\\InvalidArgumentException" => "lib/ext/psr/Log/InvalidArgumentException.php",
		"Psr\\Log\\LoggerAwareInterface" => "lib/ext/psr/Log/LoggerAwareInterface.php",
		"Psr\\Log\\LoggerAwareTrait" => "lib/ext/psr/Log/LoggerAwareTrait.php",
		"Psr\\Log\\LoggerInterface" => "lib/ext/psr/Log/LoggerInterface.php",
		"Psr\\Log\\LoggerTrait" => "lib/ext/psr/Log/LoggerTrait.php",
		"Psr\\Log\\LogLevel" => "lib/ext/psr/Log/LogLevel.php",
		"Psr\\Log\\NullLogger" => "lib/ext/psr/Log/NullLogger.php",

		"MatthiasMullie\\PathConverter\\Converter" => "lib/ext/matthiasmullie/src/Converter.php",
		"MatthiasMullie\\PathConverter\\ConverterInterface" => "lib/ext/matthiasmullie/src/ConverterInterface.php",
		"MatthiasMullie\\PathConverter\\NoConverter" => "lib/ext/matthiasmullie/src/NoConverter.php",
		"MatthiasMullie\\Minify\\CSS" => "lib/ext/matthiasmullie/src/CSS.php",
		"MatthiasMullie\\Minify\\Exception" => "lib/ext/matthiasmullie/src/Exception.php",
		"MatthiasMullie\\Minify\\JS" => "lib/ext/matthiasmullie/src/JS.php",
		"MatthiasMullie\\Minify\\Minify" => "lib/ext/matthiasmullie/src/Minify.php",
		"MatthiasMullie\\Minify\\Exceptions\\BasicException" => "lib/ext/matthiasmullie/src/Exceptions/BasicException.php",
		"MatthiasMullie\\Minify\\Exceptions\\FileImportException" => "lib/ext/matthiasmullie/src/Exceptions/FileImportException.php",
		"MatthiasMullie\\Minify\\Exceptions\\IOException" => "lib/ext/matthiasmullie/src/Exceptions/IOException.php",

		"ImageOptimizer\\Optimizer" => "lib/ext/image-optimizer/Optimizer.php",
		"ImageOptimizer\\Command" => "lib/ext/image-optimizer/Command.php",
		"ImageOptimizer\\CommandOptimizer" => "lib/ext/image-optimizer/CommandOptimizer.php",
		"ImageOptimizer\\OptimizerFactory" => "lib/ext/image-optimizer/OptimizerFactory.php",
		"ImageOptimizer\\SmartOptimizer" => "lib/ext/image-optimizer/SmartOptimizer.php",
		"ImageOptimizer\\SuppressErrorOptimizer" => "lib/ext/image-optimizer/SuppressErrorOptimizer.php",
		"ImageOptimizer\\ChainOptimizer" => "lib/ext/image-optimizer/ChainOptimizer.php",
		"ImageOptimizer\\Exception\\CommandNotFound" => "lib/ext/image-optimizer/Exception/CommandNotFound.php",
		"ImageOptimizer\\Exception\\Exception" => "lib/ext/image-optimizer/Exception/Exception.php",
		"ImageOptimizer\\TypeGuesser\\ExtensionTypeGuesser" => "lib/ext/image-optimizer/TypeGuesser/ExtensionTypeGuesser.php",
		"ImageOptimizer\\TypeGuesser\\GdTypeGuesser" => "lib/ext/image-optimizer/TypeGuesser/GdTypeGuesser.php",
		"ImageOptimizer\\TypeGuesser\\SmartTypeGuesser" => "lib/ext/image-optimizer/TypeGuesser/SmartTypeGuesser.php",
		"ImageOptimizer\\TypeGuesser\\TypeGuesser" => "lib/ext/image-optimizer/TypeGuesser/TypeGuesser.php",

		"Symfony\\Component\\OptionsResolver\\Options" => "lib/ext/symphony/OptionsResolver/Options.php",
		"Symfony\\Component\\OptionsResolver\\OptionsResolver" => "lib/ext/symphony/OptionsResolver/OptionsResolver.php",
		"Symfony\\Component\\OptionsResolver\\Exception\ExceptionInterface" => "lib/ext/symphony/OptionsResolver/Exception/ExceptionInterface.php",
		"Symfony\\Component\\OptionsResolver\\Exception\AccessException" => "lib/ext/symphony/OptionsResolver/Exception/AccessException.php",
		"Symfony\\Component\\OptionsResolver\\Exception\InvalidArgumentException" => "lib/ext/symphony/OptionsResolver/Exception/InvalidArgumentException.php",
		"Symfony\\Component\\OptionsResolver\\Exception\InvalidOptionsException" => "lib/ext/symphony/OptionsResolver/Exception/InvalidOptionsException.php",
		"Symfony\\Component\\OptionsResolver\\Exception\MissingOptionsException" => "lib/ext/symphony/OptionsResolver/Exception/MissingOptionsException.php",
		"Symfony\\Component\\OptionsResolver\\Exception\NoSuchOptionException" => "lib/ext/symphony/OptionsResolver/Exception/NoSuchOptionException.php",
		"Symfony\\Component\\OptionsResolver\\Exception\OptionDefinitionException" => "lib/ext/symphony/OptionsResolver/Exception/OptionDefinitionException.php",
		"Symfony\\Component\\OptionsResolver\\Exception\UndefinedOptionsException" => "lib/ext/symphony/OptionsResolver/Exception/UndefinedOptionsException.php",

		"Symfony\\Component\\Process\\ExecutableFinder" => "lib/ext/symphony/Process/ExecutableFinder.php",
		"Symfony\\Component\\Process\\InputStream" => "lib/ext/symphony/Process/InputStream.php",
		"Symfony\\Component\\Process\\PhpExecutableFinder" => "lib/ext/symphony/Process/PhpExecutableFinder.php",
		"Symfony\\Component\\Process\\PhpProcess" => "lib/ext/symphony/Process/PhpProcess.php",
		"Symfony\\Component\\Process\\Process" => "lib/ext/symphony/Process/Process.php",
		"Symfony\\Component\\Process\\ProcessUtils" => "lib/ext/symphony/Process/ProcessUtils.php",
		"Symfony\\Component\\Process\\Exception\ExceptionInterface" => "lib/ext/symphony/Process/Exception/ExceptionInterface.php",
		"Symfony\\Component\\Process\\Exception\InvalidArgumentException" => "lib/ext/symphony/Process/Exception/InvalidArgumentException.php",
		"Symfony\\Component\\Process\\Exception\LogicException" => "lib/ext/symphony/Process/Exception/LogicException.php",
		"Symfony\\Component\\Process\\Exception\ProcessFailedException" => "lib/ext/symphony/Process/Exception/ProcessFailedException.php",
		"Symfony\\Component\\Process\\Exception\ProcessSignaledException" => "lib/ext/symphony/Process/Exception/ProcessSignaledException.php",
		"Symfony\\Component\\Process\\Exception\ProcessTimedOutException" => "lib/ext/symphony/Process/Exception/ProcessTimedOutException.php",
		"Symfony\\Component\\Process\\Exception\RuntimeException" => "lib/ext/symphony/Process/Exception/RuntimeException.php",
		"Symfony\\Component\\Process\\Pipes\PipesInterface" => "lib/ext/symphony/Process/Pipes/PipesInterface.php",
		"Symfony\\Component\\Process\\Pipes\AbstractPipes" => "lib/ext/symphony/Process/Pipes/AbstractPipes.php",
		"Symfony\\Component\\Process\\Pipes\UnixPipes" => "lib/ext/symphony/Process/Pipes/UnixPipes.php",
		"Symfony\\Component\\Process\\Pipes\WindowsPipes" => "lib/ext/symphony/Process/Pipes/WindowsPipes.php",

		"TinyHtmlMinifier\\TinyHtmlMinifier" => "lib/ext/tiny-html-minifier/tiny-html-minifier.php",
		"TinyHtmlMinifier\\TinyMinify" => "lib/ext/tiny-html-minifier/tiny-html-minifier.php",

		"PHPMinifier\\CssMinifier" => "lib/ext/phpminifier/CssMinifier.php",
		"PHPMinifier\\HtmlMinifier" => "lib/ext/phpminifier/HtmlMinifier.php",
		"PHPMinifier\\JavascriptMinifier" => "lib/ext/phpminifier/JavascriptMinifier.php",
		"PHPMinifier\\Minifier" => "lib/ext/phpminifier/Minifier.php",
		"PHPMinifier\\PhpMinifier" => "lib/ext/phpminifier/PhpMinifier.php",

		"AMOPT_Mobile_Detect" => "lib/ext/mobiledetect/Mobile_Detect.php",

		"Sabberworm\\CSS\\OutputFormat" => "lib/ext/Sabberworm/CSS/OutputFormat.php",
		"Sabberworm\\CSS\\Parser" => "lib/ext/Sabberworm/CSS/Parser.php",
		"Sabberworm\\CSS\\Renderable" => "lib/ext/Sabberworm/CSS/Renderable.php",
		"Sabberworm\\CSS\\Settings" => "lib/ext/Sabberworm/CSS/Settings.php",
		"Sabberworm\\CSS\\Comment\\Comment" => "lib/ext/Sabberworm/CSS/Comment/Comment.php",
		"Sabberworm\\CSS\\Comment\\Commentable" => "lib/ext/Sabberworm/CSS/Comment/Commentable.php",
		"Sabberworm\\CSS\\CSSList\\AtRuleBlockList" => "lib/ext/Sabberworm/CSS/CSSList/AtRuleBlockList.php",
		"Sabberworm\\CSS\\CSSList\\CSSBlockList" => "lib/ext/Sabberworm/CSS/CSSList/CSSBlockList.php",
		"Sabberworm\\CSS\\CSSList\\CSSList" => "lib/ext/Sabberworm/CSS/CSSList/CSSList.php",
		"Sabberworm\\CSS\\CSSList\\Document" => "lib/ext/Sabberworm/CSS/CSSList/Document.php",
		"Sabberworm\\CSS\\CSSList\\KeyFrame" => "lib/ext/Sabberworm/CSS/CSSList/KeyFrame.php",
		"Sabberworm\\CSS\\Parsing\\OutputException" => "lib/ext/Sabberworm/CSS/Parsing/OutputException.php",
		"Sabberworm\\CSS\\Parsing\\ParserState" => "lib/ext/Sabberworm/CSS/Parsing/ParserState.php",
		"Sabberworm\\CSS\\Parsing\\SourceException" => "lib/ext/Sabberworm/CSS/Parsing/SourceException.php",
		"Sabberworm\\CSS\\Parsing\\UnexpectedTokenException" => "lib/ext/Sabberworm/CSS/Parsing/UnexpectedTokenException.php",
		"Sabberworm\\CSS\\Property\\AtRule" => "lib/ext/Sabberworm/CSS/Property/AtRule.php",
		"Sabberworm\\CSS\\Property\\Charset" => "lib/ext/Sabberworm/CSS/Property/Charset.php",
		"Sabberworm\\CSS\\Property\\CSSNamespace" => "lib/ext/Sabberworm/CSS/Property/CSSNamespace.php",
		"Sabberworm\\CSS\\Property\\Import" => "lib/ext/Sabberworm/CSS/Property/Import.php",
		"Sabberworm\\CSS\\Property\\Selector" => "lib/ext/Sabberworm/CSS/Property/Selector.php",
		"Sabberworm\\CSS\\Rule\\Rule" => "lib/ext/Sabberworm/CSS/Rule/Rule.php",
		"Sabberworm\\CSS\\RuleSet\\AtRuleSet" => "lib/ext/Sabberworm/CSS/RuleSet/AtRuleSet.php",
		"Sabberworm\\CSS\\RuleSet\\DeclarationBlock" => "lib/ext/Sabberworm/CSS/RuleSet/DeclarationBlock.php",
		"Sabberworm\\CSS\\RuleSet\\RuleSet" => "lib/ext/Sabberworm/CSS/RuleSet/RuleSet.php",
		"Sabberworm\\CSS\\Value\\CalcFunction" => "lib/ext/Sabberworm/CSS/Value/CalcFunction.php",
		"Sabberworm\\CSS\\Value\\CalcRuleValueList" => "lib/ext/Sabberworm/CSS/Value/CalcRuleValueList.php",
		"Sabberworm\\CSS\\Value\\Color" => "lib/ext/Sabberworm/CSS/Value/Color.php",
		"Sabberworm\\CSS\\Value\\CSSFunction" => "lib/ext/Sabberworm/CSS/Value/CSSFunction.php",
		"Sabberworm\\CSS\\Value\\CSSString" => "lib/ext/Sabberworm/CSS/Value/CSSString.php",
		"Sabberworm\\CSS\\Value\\LineName" => "lib/ext/Sabberworm/CSS/Value/LineName.php",
		"Sabberworm\\CSS\\Value\\PrimitiveValue" => "lib/ext/Sabberworm/CSS/Value/PrimitiveValue.php",
		"Sabberworm\\CSS\\Value\\RuleValueList" => "lib/ext/Sabberworm/CSS/Value/RuleValueList.php",
		"Sabberworm\\CSS\\Value\\Size" => "lib/ext/Sabberworm/CSS/Value/Size.php",
		"Sabberworm\\CSS\\Value\\URL" => "lib/ext/Sabberworm/CSS/Value/URL.php",
		"Sabberworm\\CSS\\Value\\Value" => "lib/ext/Sabberworm/CSS/Value/Value.php",
		"Sabberworm\\CSS\\Value\\ValueList" => "lib/ext/Sabberworm/CSS/Value/ValueList.php",
	)
);

CModule::AddAutoloadClasses(
	"ammina.optimizer", array(
		"Ammina\\Optimizer\\Core2\\Settings" => "lib/core2/settings.php",
		"Ammina\\Optimizer\\Core2\\LibAvailable" => "lib/core2/lib.available.php",
		"Ammina\\Optimizer\\Core2\\Application" => "lib/core2/application.php",
		"Ammina\\Optimizer\\Core2\\AppBackground" => "lib/core2/appbackground.php",
		"Ammina\\Optimizer\\Core2\\Parser\\Base" => "lib/core2/parser/base.php",
		"Ammina\\Optimizer\\Core2\\Parser\\PHPParser" => "lib/core2/parser/phpparser.php",
		"Ammina\\Optimizer\\Core2\\Parser\\DOMParser" => "lib/core2/parser/domparser.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\CSS" => "lib/core2/optimizer/css.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\JS" => "lib/core2/optimizer/js.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image" => "lib/core2/optimizer/image.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Html" => "lib/core2/optimizer/html.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\Base" => "lib/core2/optimizer/image/base.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\Jpg" => "lib/core2/optimizer/image/jpg.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\Png" => "lib/core2/optimizer/image/png.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\Gif" => "lib/core2/optimizer/image/gif.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\Svg" => "lib/core2/optimizer/image/svg.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\WebP" => "lib/core2/optimizer/image/webp.php",
		"Ammina\\Optimizer\\Core2\\Optimizer\\Image\\Lazy" => "lib/core2/optimizer/image/lazy.php",
	)
);

CModule::AddAutoloadClasses(
	"ammina.optimizer", array(
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\Base" => "lib/core2/driver/css/base.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\PHPWee" => "lib/core2/driver/css/phpwee.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\MatthiasMullie" => "lib/core2/driver/css/matthiasmullie.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\UglifyCss" => "lib/core2/driver/css/uglifycss.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\YUICompressor" => "lib/core2/driver/css/yuicompressor.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\Ammina" => "lib/core2/driver/css/ammina.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\AmminaUglifyCss" => "lib/core2/driver/css/amminauglifycss.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Css\\AmminaYUICompressor" => "lib/core2/driver/css/amminayuicompressor.php",

		"Ammina\\Optimizer\\Core2\\Driver\\Image\\Base" => "lib/core2/driver/image/base.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\Imagick" => "lib/core2/driver/image/imagick.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\JpegOptim" => "lib/core2/driver/image/jpegoptim.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\PngQuant" => "lib/core2/driver/image/pngquant.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\OptiPng" => "lib/core2/driver/image/optipng.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\GifSicle" => "lib/core2/driver/image/gifsicle.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\Svgo" => "lib/core2/driver/image/svgo.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\PhpGD" => "lib/core2/driver/image/phpgd.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\CWebP" => "lib/core2/driver/image/cwebp.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\Ammina" => "lib/core2/driver/image/ammina.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaImagick" => "lib/core2/driver/image/amminaimagick.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaJpegOptim" => "lib/core2/driver/image/amminajpegoptim.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaPngQuant" => "lib/core2/driver/image/amminapngquant.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaOptiPng" => "lib/core2/driver/image/amminaoptipng.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaGifSicle" => "lib/core2/driver/image/amminagifsicle.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaSvgo" => "lib/core2/driver/image/amminasvgo.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Image\\AmminaCWebP" => "lib/core2/driver/image/amminacwebp.php",

		"Ammina\\Optimizer\\Core2\\Driver\\Js\\Base" => "lib/core2/driver/js/base.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\PHPWee" => "lib/core2/driver/js/phpwee.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\MatthiasMullie" => "lib/core2/driver/js/matthiasmullie.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\UglifyJs" => "lib/core2/driver/js/uglifyjs.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\UglifyJs2" => "lib/core2/driver/js/uglifyjs2.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\TerserJs" => "lib/core2/driver/js/terserjs.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\BabelMinify" => "lib/core2/driver/js/babelminify.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\YUICompressor" => "lib/core2/driver/js/yuicompressor.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\Ammina" => "lib/core2/driver/js/ammina.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\AmminaUglifyJs" => "lib/core2/driver/js/amminauglifyjs.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\AmminaUglifyJs2" => "lib/core2/driver/js/amminauglifyjs2.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\AmminaTerserJs" => "lib/core2/driver/js/amminaterserjs.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\AmminaBabelMinify" => "lib/core2/driver/js/amminababelminify.php",
		"Ammina\\Optimizer\\Core2\\Driver\\Js\\AmminaYUICompressor" => "lib/core2/driver/js/amminayuicompressor.php",
	)
);

if (!class_exists('Bitrix\Main\ORM\Event')) {
	class_alias('Bitrix\Main\Entity\Event', 'Bitrix\Main\ORM\Event');
	class_alias('Bitrix\Main\Entity\EventResult', 'Bitrix\Main\ORM\EventResult');
	class_alias('Bitrix\Main\Entity\DataManager', 'Bitrix\Main\ORM\Data\DataManager');
	class_alias('Bitrix\Main\Entity\Result', 'Bitrix\Main\ORM\Data\Result');
	class_alias('Bitrix\Main\Entity\AddResult', 'Bitrix\Main\ORM\Data\AddResult');
	class_alias('Bitrix\Main\Entity\UpdateResult', 'Bitrix\Main\ORM\Data\UpdateResult');
	class_alias('Bitrix\Main\Entity\DeleteResult', 'Bitrix\Main\ORM\Data\DeleteResult');
	class_alias('Bitrix\Main\Entity\ExpressionField', 'Bitrix\Main\ORM\Fields\ExpressionField');
}
if (!class_exists("Bitrix\\Main\\Composite\\Engine", false)) {
	class_alias("Bitrix\\Main\\Page\\Frame", "Bitrix\\Main\\Composite\\Engine");
}


if (!function_exists("myPrint")) {
	function myPrint(&$Var, $bIsHtmlSpecialChars = true, $strFileName = false, $bAppend = false)
	{
		if (defined("WEBAVK_CRON_UNIQ_IDENT") && $strFileName === false) {
			$bIsHtmlSpecialChars = false;
		}
		$strContent = '<pre style="text-align:left;background-color:#222222;color:#ffffff;font-size:11px;overflow: auto;">';
		if ($bIsHtmlSpecialChars) {
			$strContent .= htmlspecialchars(print_r($Var, true));
		} else {
			$strContent .= print_r($Var, true);
		}
		$strContent .= '</pre>';
		if ($strFileName) {
			if ($bAppend) {
				CAmminaOptimizer::SaveFileContent($strFileName, file_get_contents($strFileName) . "\n" . $strContent);
			} else {
				CAmminaOptimizer::SaveFileContent($strFileName, $strContent);
			}
		} else {
			echo $strContent;
		}
	}
}

if (!function_exists("amminainclassxpath")) {
	function amminainclassxpath($str, $substr)
	{
		if (stripos($str, $substr) !== false) {
			return "Y";
		}
		return "N";
	}
}

?>