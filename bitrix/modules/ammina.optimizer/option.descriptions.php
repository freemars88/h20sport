<?

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arAllowLibrary = \Ammina\Optimizer\Core2\LibAvailable::getCurrentCheckData();

$arLibraryCss = array(
	"sabberworm" => array(),
	"phpwee" => array(),
	"matthiasmullie" => array(),
	"yuicompressor" => array(
		"DISABLED" => !$arAllowLibrary['npm']['yuicompressor'],
	),
	"uglifycss" => array(
		"DISABLED" => !$arAllowLibrary['npm']['uglifycss'],
	),
	"amminayuicompressor" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminauglifycss" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$arLibraryJs = array(
	"phpwee" => array(),
	"matthiasmullie" => array(),
	"yuicompressor" => array(
		"DISABLED" => !$arAllowLibrary['npm']['yuicompressor'],
	),
	"uglifyjs" => array(
		"DISABLED" => !$arAllowLibrary['npm']['uglifyjs'],
	),
	"uglifyjs2" => array(
		"DISABLED" => !$arAllowLibrary['npm']['uglifyjs2'],
	),
	"terserjs" => array(
		"DISABLED" => !$arAllowLibrary['npm']['terserjs'],
	),
	"babelminify" => array(
		"DISABLED" => !$arAllowLibrary['npm']['babelminify'],
	),
	"amminayuicompressor" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminauglifyjs" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminauglifyjs2" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminaterserjs" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminababelminify" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$strDefaultLibraryJs = "phpwee";
if (!$arLibraryJs['uglifyjs']['DISABLED']) {
	$strDefaultLibraryJs = "uglifyjs";
} elseif (!$arLibraryJs['amminauglifyjs']['DISABLED']) {
	$strDefaultLibraryJs = "amminauglifyjs";
}
$arLibraryImagesJpg = array(
	"phpimagick" => array(
		"DISABLED" => !$arAllowLibrary['packages']['phpimagick'],
	),
	"jpegoptim" => array(
		"DISABLED" => !$arAllowLibrary['packages']['jpegoptim'],
	),
	"amminaphpimagick" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminajpegoptim" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$strDefaultLibraryImagesJpg = "phpimagick";
if ($arLibraryImagesJpg['phpimagick']['DISABLED']) {
	if (!$arLibraryImagesJpg['jpegoptim']['DISABLED']) {
		$strDefaultLibraryImagesJpg = "jpegoptim";
	} elseif (!$arLibraryImagesJpg['amminaphpimagick']['DISABLED']) {
		$strDefaultLibraryImagesJpg = "amminaphpimagick";
	}
}
$arLibraryImagesPng = array(
	"phpimagick" => array(
		"DISABLED" => !$arAllowLibrary['packages']['phpimagick'],
	),
	"optipng" => array(
		"DISABLED" => !$arAllowLibrary['packages']['optipng'],
	),
	"pngquant" => array(
		"DISABLED" => !$arAllowLibrary['packages']['pngquant'],
	),
	"amminaphpimagick" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminaoptipng" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminapngquant" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$strDefaultLibraryImagesPng = "pngquant";
if ($arLibraryImagesPng['pngquant']['DISABLED']) {
	if (!$arLibraryImagesPng['amminapngquant']['DISABLED']) {
		$strDefaultLibraryImagesPng = "amminapngquant";
	} elseif (!$arLibraryImagesPng['phpimagick']['DISABLED']) {
		$strDefaultLibraryImagesPng = "phpimagick";
	}
}
$arLibraryImagesGif = array(
	"phpimagick" => array(
		"DISABLED" => !$arAllowLibrary['packages']['phpimagick'],
	),
	"gifsicle" => array(
		"DISABLED" => !$arAllowLibrary['packages']['gifsicle'],
	),
	"amminaphpimagick" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
	"amminagifsicle" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$arLibraryImagesSvg = array(
	"svgo" => array(
		"DISABLED" => !$arAllowLibrary['npm']['svgo'],
	),
	"amminasvgo" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$strDefaultLibraryImagesSvg = "svgo";
if ($arLibraryImagesSvg['svgo']['DISABLED']) {
	if (!$arLibraryImagesSvg['amminasvgo']['DISABLED']) {
		$strDefaultLibraryImagesSvg = "amminasvgo";
	}
}
$arLibraryHtml = array(
	"amminaoptimizer" => array(),
);

$arLibraryParser = array(
	"domparser" => array(
		"DISABLED" => !$arAllowLibrary['packages']['dom'],
	),
	//"phpparser" => array(),
);
$arLibraryImagesWebP = array(
	"phpgd" => array(
		"DISABLED" => !$arAllowLibrary['packages']['phpgd'],
	),
	/*
	"phpimagick" => array(
		"DISABLED" => !$arAllowLibrary['packages']['phpimagickwebp'],
	),*/
	"cwebp" => array(
		"DISABLED" => !$arAllowLibrary['packages']['cwebp'],
	),
	"amminacwebp" => array(
		"DISABLED" => !$arAllowLibrary['other']['amminaapi'],
	),
);
$strDefaultLibraryImagesWebP = "cwebp";
if ($arLibraryImagesWebP['cwebp']['DISABLED']) {
	if (!$arLibraryImagesWebP['amminacwebp']['DISABLED']) {
		$strDefaultLibraryImagesWebP = "amminacwebp";
	} elseif (!$arLibraryImagesWebP['phpgd']['DISABLED']) {
		$strDefaultLibraryImagesWebP = "phpgd";
	}
}

$defaultParser = "domparser";
if (!$arAllowLibrary['packages']['dom']) {
	$defaultParser = "phpparser";
}
return array(
	"category" => array(
		"main" => array(
			"options" => array(
				"ACTIVE" => array(
					"TYPE" => "checkbox",
					"DEFAULT" => "N",
					"SHORT" => "Y",
				),
			),
			"groups" => array(
				"parse" => array(
					"options" => array(
						//����������
						"LIBRARY" => array(
							"TYPE" => "select",
							"DEFAULT" => "domparser",
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryParser,
						),
						"CHECK_ENCODING_UTF8" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
					),
				),
				"request" => array(
					"options" => array(
						//������������ HTML �������
						"ACTIVE_HTML" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),

						//������������ AJAX �������
						"ACTIVE_AJAX" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//������������ JSON �������
						"ACTIVE_JSON" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						"ACTIVE_AUTOCOMPOSITE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						/*
						//������������ JSON �������
						"ACTIVE_COMPONENTS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						*/
					),
				),
			),
		),
		"css" => array(
			"options" => array(
				"ACTIVE" => array(
					"TYPE" => "checkbox",
					"DEFAULT" => "N",
					"SHORT" => "Y",
				),
			),
			"groups" => array(
				"minify" => array(
					"options" => array(
						//��������������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => "sabberworm",
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryCss,
						),
						//�� �������������� �����
						"EXCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������������� �����
						"INCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"external_css" => array(
					"options" => array(
						//�������������� ���������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//���������
						"EXCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//��������
						"INCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"images" => array(
					"options" => array(
						//��������� ����������� �� CSS
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//�������������� �����������
						"OPTIMIZE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//Inline image
						"INLINE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//������������ ������ ����������� inline
						"MAX_IMAGE_SIZE" => array(
							"TYPE" => "text.bytes",
							"DEFAULT" => "8192",
							"SHORT" => "Y",
						),
					),
				),
				"fonts" => array(
					"options" => array(
						//����������� �������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//���������� ���������
						"LINK" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
						),
						"LINK_FONTS" => array(
							"TYPE" => "text",
							"DEFAULT" => "",
						),
						//���������� font-face
						"FONT_FACE" => array(
							"TYPE" => "select",
							"DEFAULT" => "swap",
							"VARIANTS" => array(
								"none" => array(),
								"auto" => array(),
								"swap" => array(),
								"fallback" => array(),
							),
						),
						//�������������� Google ������
						"GOOGLE_FONTS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��� ����������� Google (� ������ user agent, ����������� �����������)
						"GOOGLE_FONTS_TYPE" => array(
							"TYPE" => "select",
							"DEFAULT" => "ua",
							"VARIANTS" => array(
								"ua" => array(),
								//"standart" => array(),
							),
						),
						//���������� ������ inline
						"INLINE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
					),
				),
				"critical" => array(
					"options" => array(
						//�������� ����������� CSS � ��������� �� inline
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						"USE_TAG_IDENT" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						"ADD_CLASS" => array(
							"TYPE" => "textarea",
							"DEFAULT" => "",
						),
						"ADD_IDENT" => array(
							"TYPE" => "textarea",
							"DEFAULT" => "",
						),
						"ONLY_CLASS" => array(
							"TYPE" => "textarea",
							"DEFAULT" => "",
						),
						"ONLY_IDENT" => array(
							"TYPE" => "textarea",
							"DEFAULT" => "",
						),
						"IGNORE_CLASS" => array(
							"TYPE" => "textarea",
							"DEFAULT" => "",
						),
						"IGNORE_IDENT" => array(
							"TYPE" => "textarea",
							"DEFAULT" => "",
						),
						"MAX_CRITICAL_RECORD" => array(
							"TYPE" => "text",
							"DEFAULT" => "5",
						),
						"USE_UNCRITICAL" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						"MODE_FULL" => array(
							"TYPE" => "select",
							"DEFAULT" => "link",
							"SHORT" => "Y",
							"VARIANTS" => array(
								"link" => array(),
								"script" => array(),
								"scriptwait" => array(),
							),
						),
						"WAIT_FULL" => array(
							"TYPE" => "text",
							"DEFAULT" => "100",
						),
					),
				),
				"other" => array(
					"options" => array(
						//��������� �� ����������� CSS �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
						),
						//�������� CSS inline � ��������
						"INLINE_CSS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//������������ ������ CSS ��� inline
						"MAX_SIZE_INLINE" => array(
							"TYPE" => "text.bytes",
							"DEFAULT" => "65536",
							"SHORT" => "Y",
						),
						//���������� Inline ����� </body>
						"INLINE_BEFORE_BODY" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						"CHECK_BX_CSS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
						),
						/****
						 * //���������� � CSS ����� Outline CSS
						 * "OUTLINE_CSS" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "Y",
						 * "SHORT" => "Y",
						 * ),
						 * //����������� ������ ouline css ��� ���������� � �����
						 * "MIN_SIZE_OUTLINE" => array(
						 * "TYPE" => "text.bytes",
						 * "DEFAULT" => "1024",
						 * ),
						 * //Outline ����� � ��������� ����
						 * "OUTLINE_TO_SEPARATE" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "N",
						 * "SHORT" => "Y",
						 * ),
						 * //��������������/�������������� inline CSS
						 * "OPTIMIZE_INLINE" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "N",
						 * "SHORT" => "Y",
						 * ),
						 * "CHECK_BX_CSS" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "Y",
						 * ),*//*
						"CHECK_TAGS_IN_SCRIPT" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
						),*/
					),
				),
			),
		),
		"js" => array(
			"options" => array(
				"ACTIVE" => array(
					"TYPE" => "checkbox",
					"DEFAULT" => "N",
					"SHORT" => "Y",
				),
			),
			"groups" => array(
				"minify" => array(
					"options" => array(
						//��������������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => $strDefaultLibraryJs,
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryJs,
						),
						//�� �������������� �����
						"EXCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������������� �����
						"INCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"external_js" => array(
					"options" => array(
						//�������������� ���������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//���������
						"EXCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => implode("\n", array("https://www.googletagmanager.com/")),
							"SHORT" => "N",
						),
						//��������
						"INCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"other" => array(
					"options" => array(
						//�������� JS inline � ��������
						"INLINE_JS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//������������ ������ JS ��� inline
						"MAX_SIZE_INLINE" => array(
							"TYPE" => "text.bytes",
							"DEFAULT" => "65536",
							"SHORT" => "Y",
						),
						/****
						 * //���������� � JS ����� Outline JS
						 * "OUTLINE_JS" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "Y",
						 * "SHORT" => "Y",
						 * ),
						 * //����������� ������ ouline js ��� ���������� � �����
						 * "MIN_SIZE_OUTLINE" => array(
						 * "TYPE" => "text.bytes",
						 * "DEFAULT" => "1024",
						 * ),
						 * //Outline js � ��������� ����
						 * "OUTLINE_TO_SEPARATE" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "Y",
						 * "SHORT" => "Y",
						 * ),
						 * //��������������/�������������� inline JS
						 * "OPTIMIZE_INLINE" => array(
						 * "TYPE" => "checkbox",
						 * "DEFAULT" => "Y",
						 * "SHORT" => "Y",
						 * ),*/
						//��������� �� ����������� JS �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
						),
					),
				),
				"ext" => array(
					"options" => array(
						//�������������� ��������� ������ ����
						"CHECK_CORE_FILES" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//RequireJS
					),
				),
			),
		),
		"images" => array(
			"options" => array(
				"ACTIVE" => array(
					"TYPE" => "checkbox",
					"DEFAULT" => "N",
					"SHORT" => "Y",
				),
			),
			"groups" => array(
				"jpg_files" => array(
					"options" => array(
						//�������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��������
						"QUALITY" => array(
							"TYPE" => "text",
							"DEFAULT" => "80",
							"SHORT" => "N",
						),
						//������������ ��� IMG
						"CHECK_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ����� background[-image]
						"CHECK_BACKGROUND" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ data-src
						"CHECK_DATA_SRC" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ��� � ��������
						"CHECK_ALL_OTHER" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "N",
						),
						/*
						//��������� ������ ����������� (��� ����) �� ���������
						"IMAGES_FROM_DIR" => array(
							"TYPE" => "files.static",
							"DEFAULT" => array("/upload/", "/local/templates/", "/bitrix/templates/"),
							"SHORT" => "N",
						),
						*/
						//���������� �����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => $strDefaultLibraryImagesJpg,
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryImagesJpg,
						),
						//��������� �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� �����
						"INCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//������������� � WebP
						"CONVERT_WEBP" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��������� �����
						"EXCLUDE_WEBP_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� �����
						"INCLUDE_WEBP_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"png_files" => array(
					"options" => array(
						//��������� PNG ������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//������������ ��� IMG
						"CHECK_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ����� background[-image]
						"CHECK_BACKGROUND" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ data-src
						"CHECK_DATA_SRC" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ��� � ��������
						"CHECK_ALL_OTHER" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "N",
						),
						//���������� �����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => $strDefaultLibraryImagesPng,
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryImagesPng,
						),
						//��������� �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� �����
						"INCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//������������� � WebP
						"CONVERT_WEBP" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��������� �� ����������� �����
						"EXCLUDE_WEBP_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� � ����������� �����
						"INCLUDE_WEBP_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//������������� � JPG
						"CONVERT_JPG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//������������� � JPG
						"CONVERT_JPG_BG" => array(
							"TYPE" => "text",
							"DEFAULT" => "ffffff",
							"SHORT" => "Y",
							"DISABLED" => !$arAllowLibrary['packages']['phpgd'],
						),
						//��������� �� ����������� �����
						"EXCLUDE_JPG_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� � ����������� �����
						"INCLUDE_JPG_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"gif_files" => array(
					"options" => array(
						//��������� HIF ������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//������������ ��� IMG
						"CHECK_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ����� background[-image]
						"CHECK_BACKGROUND" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ data-src
						"CHECK_DATA_SRC" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ��� � ��������
						"CHECK_ALL_OTHER" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "N",
						),
						//��������
						"QUALITY" => array(
							"TYPE" => "text",
							"DEFAULT" => "75",
							"SHORT" => "N",
						),
						//���������� �����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => "phpimagick",
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryImagesGif,
						),
						//��������� �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� �����
						"INCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//������������� � WebP
						"CONVERT_WEBP" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��������� �� ����������� �����
						"EXCLUDE_WEBP_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� � ����������� �����
						"INCLUDE_WEBP_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//������������� � JPG
						"CONVERT_JPG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//������������� � JPG
						"CONVERT_JPG_BG" => array(
							"TYPE" => "text",
							"DEFAULT" => "ffffff",
							"SHORT" => "Y",
							"DISABLED" => !$arAllowLibrary['packages']['phpgd'],
						),
						//��������� �� ����������� �����
						"EXCLUDE_JPG_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� � ����������� �����
						"INCLUDE_JPG_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"svg_files" => array(
					"options" => array(
						//�������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//������������ ��� IMG
						"CHECK_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ����� background[-image]
						"CHECK_BACKGROUND" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ data-src
						"CHECK_DATA_SRC" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������������ ��� � ��������
						"CHECK_ALL_OTHER" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "N",
						),
						/*
						//��������� ������ ����������� (��� ����) �� ���������
						"IMAGES_FROM_DIR" => array(
							"TYPE" => "files.static",
							"DEFAULT" => array("/upload/", "/local/templates/", "/bitrix/templates/"),
							"SHORT" => "N",
						),
						*/
						//���������� �����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => $strDefaultLibraryImagesSvg,
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryImagesSvg,
						),
						//��������� �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� �����
						"INCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"webp_files" => array(
					"options" => array(
						//�������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��������
						"QUALITY" => array(
							"TYPE" => "text",
							"DEFAULT" => "80",
							"SHORT" => "N",
						),
						//���������� �����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => $strDefaultLibraryImagesWebP,
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryImagesWebP,
						),
						//��������� �����
						"EXCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//�������� �����
						"INCLUDE_FILES" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				/****
				 * "events" => array(
				 * "options" => array(
				 * //�������������� ���������
				 * "ACTIVE" => array(
				 * "TYPE" => "checkbox",
				 * "DEFAULT" => "Y",
				 * "SHORT" => "Y",
				 * ),
				 * //������������ ������� ���������� �����������
				 * "USE_EVENTS_CHANGE" => array(
				 * "TYPE" => "checkbox",
				 * "DEFAULT" => "Y",
				 * "SHORT" => "Y",
				 * ),
				 * //������������ ������� ��������� �������
				 * "USE_EVENTS_RESIZE" => array(
				 * "TYPE" => "checkbox",
				 * "DEFAULT" => "Y",
				 * "SHORT" => "Y",
				 * ),
				 * //���������
				 * "EXCLUDE" => array(
				 * "TYPE" => "files.static",
				 * "DEFAULT" => "",
				 * "SHORT" => "N",
				 * ),
				 * //��������
				 * "INCLUDE" => array(
				 * "TYPE" => "files.static",
				 * "DEFAULT" => "",
				 * "SHORT" => "N",
				 * ),
				 * ),
				 * ),*/
				"external_images" => array(
					"options" => array(
						//�������������� ���������
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//���������
						"EXCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//��������
						"INCLUDE" => array(
							"TYPE" => "files.static",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
					),
				),
				"lazy" => array(
					"options" => array(
						//������������ LazyLaod
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//����� ��� ���������� ��������
						"LAZY_CLASS" => array(
							"TYPE" => "text",
							"DEFAULT" => "lazyclass",
							"SHORT" => "Y",
						),
						"CHECK_ALL_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//��� lazyLoad
						"TYPE" => array(
							"TYPE" => "select",
							"DEFAULT" => "N",
							"SHORT" => "Y",
							"VARIANTS" => array(
								"blur" => array(),
								"blurgrey" => array(),
								"mainimg" => array(),
							),
						),
						"MAINIMG" => array(
							"TYPE" => "file",
							"DEFAULT" => "/bitrix/",
							"SHORT" => "N",
						),
						//������������� � WebP
						"CONVERT_WEBP" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						"INLINE_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						"MAX_SIZE_INLINE" => array(
							"TYPE" => "text.bytes",
							"DEFAULT" => "8192",
						),
						"INCLUDE_JQUERY" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
					),
				),
				"other" => array(
					"options" => array(
						//���������� ����������� Inline
						"INLINE_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//������������ ������ inline �����������
						"MAX_SIZE_INLINE" => array(
							"TYPE" => "text.bytes",
							"DEFAULT" => "8192",
						),
						//���� ������ ��� inline �����������
						"INLINE_TYPES" => array(
							"TYPE" => "text",
							"DEFAULT" => "jpg,jpeg,png,gif,svg,webp",
						),
						/*//�������� ������ ����������� � ������������ � ����������� data
						"RESIZE_IMG" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),*/
						/*//�������� ��� IMG �� ��� Picture ��� �����������
						"IMG_TO_PICTURE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),*/
					),
				),
			),
		),
		"html" => array(
			"options" => array(
				"ACTIVE" => array(
					"TYPE" => "checkbox",
					"DEFAULT" => "N",
					"SHORT" => "Y",
				),
			),
			"groups" => array(
				/*"html" => array(
					"options" => array(
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//����������
						"LIBRARY" => array(
							"TYPE" => "select.options",
							"DEFAULT" => "amminaoptimizer",
							"SHORT" => "Y",
							"VARIANTS" => $arLibraryHtml,
						),
					),
				),
				*/
				"tags" => array(
					"options" => array(
						"ACTIVE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//������� �����������
						"REMOVE_COMMENTS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������� ��� pre
						"REMOVE_PRE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "N",
						),
						//������� ������ ��������� ���� script
						"REMOVE_ATTR_SCRIPT" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������� ������ ��������� ���� style
						"REMOVE_ATTR_STYLE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
						//������� ������ �������, ��������� � ��
						"REMOVE_WHITE_SPACE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "N",
						),
					),
				),
			),
		),
		"other" => array(
			"options" => array(
				"ACTIVE" => array(
					"TYPE" => "checkbox",
					"DEFAULT" => "N",
					"SHORT" => "Y",
				),
			),
			"groups" => array(
				"headers" => array(
					"options" => array(
						//���������� ��������� preload
						"ACTIVE_PRELOAD" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//�������� preload ����������
						"PRELOAD" => array(
							"TYPE" => "headers.preload",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//���������� ��������� prefetch
						"ACTIVE_PREFETCH" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//�������� prefetch ����������
						"PREFETCH" => array(
							"TYPE" => "headers.prefetch",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//���������� ��������� preconnect
						"ACTIVE_PRECONNECT" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "N",
							"SHORT" => "Y",
						),
						//�������� preconnect ����������
						"PRECONNECT" => array(
							"TYPE" => "headers.preconnect",
							"DEFAULT" => "",
							"SHORT" => "N",
						),
						//��������� UserAgent ��� �������� prefetch/preconnect ����������
						"ACTIVE_USERAGENTS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
					),
				),
				"links" => array(
					"options" => array(
						//���������� � HTML ����� ���� link � ������������ � ����������� headers
						"SET_LINKS" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						"ONLY_COMPOSITE" => array(
							"TYPE" => "checkbox",
							"DEFAULT" => "Y",
							"SHORT" => "Y",
						),
						//��� ������ �� ��������� (��� ���������)
						"LINKS_TYPE" => array(
							"TYPE" => "select",
							"DEFAULT" => "prefetch",
							"SHORT" => "Y",
							"VARIANTS" => array(
								"prefetch" => array(),
								"preload" => array(),
							),
						),
					),
				),
			),
		),
	),
);