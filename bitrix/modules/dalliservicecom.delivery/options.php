<?php
$module_id = "dalliservicecom.delivery";
global $MESS;
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/options.php');
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule('sale');

include_once($GLOBALS['DOCUMENT_ROOT'].'/bitrix/modules/'.$module_id.'/include.php');
$MODULE_RIGHT = $APPLICATION->GetGroupRight($sModuleId);
CJSCore::Init(array('dalliservicecom', 'jquery'));
if ($MODULE_RIGHT < "R")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
if ($REQUEST_METHOD == "GET" && strlen($RestoreDefaults) > 0) {
    COption::RemoveOption($module_id);
    $z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
    while ($zr = $z->Fetch())
        $APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
}
$pvz_res = DalliservicecomDeliveryDB::GetPvzList();
$pvz_count = $pvz_res->SelectedRowsCount();
if($pvz_count > 0){
    $pvz_mess = GetMessage("CHECK_PVZ_OK").$pvz_count;
    $pvz_butt_mess = GetMessage("UPDATE_PVZ"); 
} else {
    $pvz_mess = GetMessage("CHECK_PVZ_NOT_OK");
    $pvz_butt_mess = GetMessage("UPLOAD_PVZ");
}

function optString($module_id, $array){
    foreach ($array as $option => $default) {
        if (!COption::GetOptionString($module_id, $option))
            COption::SetOptionString($module_id, $option, $default);
    }
}

$optionsStrings = array(
    "DALLI_FILIAL" => "MSK",
    "DALLI_TOKEN" => "",
    "WEIGHT_DEFAULT" => "",
    "PERSON" => "FIO",
    "ADDRESS" => "ADDRESS",
    "PHONE" => "PHONE",
    "ZIP" => "ZIP",
    "STREET" => "STREET",
    "HOUSE" => "HOUSE",
    "HOUSING" => "HOUSING",
    "FLAT" => "FLAT",
    "ARTICLE" => "ARTNUMBER",
    "BARCODE" => "BARCODE",
    "PAYTYPE_CARD" => "",
    "PAYTYPE_CASH" => "",
    "PAYTYPE_NO" => "",
    "TIME_DEFAULT" => "10:00-18:00",
    "TIME_PROP_ID" => "",
    "NEED_ROUNDING_ITEMS_RETPRICE"=>"NO",
    "NEED_ROUNDING_ITEMS_PRICE"=>"NO",
);
optString($module_id, $optionsStrings);

$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage('SETTINGS'), "ICON" => "", "TITLE" => GetMessage("SETTINGS")),
    array("DIV" => "edit2", "TAB" => "FAQ", "ICON" => "", "TITLE" => "FAQ"),
    array("DIV" => "edit3", "TAB" => GetMessage("ACCESS"), "ICON" => "", "TITLE" => GetMessage("ACCESS")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs, true, true);
$db_props = CSaleOrderProps::GetList(array(), array('ACTIVE'=>'Y', "PERSON_TYPE_ID"=>1), false, false, array('NAME', 'CODE'));
while ($props = $db_props->Fetch())
{
    $propsArr[$props['CODE']] = $props['NAME'];
}
$db_ptype = CSalePaySystem::GetList(array(), array("ACTIVE"=>"Y", "PERSON_TYPE_ID"=>1), false, false, array('ID', 'NAME'));
while ($ptype = $db_ptype->Fetch())
{
    $paysystemArr[$ptype['ID']] = $ptype['NAME'];
}

$id_or_number = array('default'=>'', 'id'=>GetMessage("ID_ORDER"), 'number'=>GetMessage("NUMBER_ORDER"));

$arAllOptions[] = Array(
    array("DALLI_TOKEN", GetMessage("DALLI_TOKEN"), array("text", 33)),
    array("DALLI_FILIAL", GetMessage("DALLI_FILIAL"), array("select", array("MSK"=>GetMessage("MSK"), "SPB"=>GetMessage("SPB")))),
    array("PVZ_CHECK", GetMessage("PVZ_CHECK"), array("additional", 33)),
    array("DS_COURIER_COST_DEFAULT", GetMessage("DS_COURIER_COST_DEFAULT"), array("text", 7)),
    array("DS_PVZ_COST_DEFAULT", GetMessage("DS_PVZ_COST_DEFAULT"), array("text", 7)),
    array("SDEK_COURIER_COST_DEFAULT", GetMessage("SDEK_COURIER_COST_DEFAULT"), array("text", 7)),
    array("SDEK_PVZ_COST_DEFAULT", GetMessage("SDEK_PVZ_COST_DEFAULT"), array("text", 7)),
    array("BOXBERRY_PVZ_COST_DEFAULT", GetMessage("BOXBERRY_PVZ_COST_DEFAULT"), array("text", 7)),

    array("DS_COURIER_COST_MIN", GetMessage("DS_COURIER_COST_MIN"), array("text", 7)),
    array("DS_PVZ_COST_MIN", GetMessage("DS_PVZ_COST_MIN"), array("text", 7)),
    array("SDEK_COURIER_COST_MIN", GetMessage("SDEK_COURIER_COST_MIN"), array("text", 7)),
    array("SDEK_PVZ_COST_MIN", GetMessage("SDEK_PVZ_COST_MIN"), array("text", 7)),
    array("BOXBERRY_PVZ_COST_MIN", GetMessage("BOXBERRY_PVZ_COST_MIN"), array("text", 7)),
//    array("PICKUP_PVZ_COST_DEFAULT", GetMessage("PICKUP_PVZ_COST_DEFAULT"), array("text", 7)),
//    array("HEIGHT_DEFAULT", GetMessage("HEIGHT_DEFAULT"), array("text", 7)),
//    array("LENGTH_DEFAULT", GetMessage("LENGTH_DEFAULT"), array("text", 7)),
//    array("WIDTH_DEFAULT", GetMessage("WIDTH_DEFAULT"), array("text", 7)),
    array("PERSON", GetMessage("PERSON"), array("text", 7)),
   // array("PERSON", GetMessage("PERSON"), array("select" ,$propsArr)),
    array("ADDRESS", GetMessage("ADDRESS"), array("text", 7)),
    array("PHONE", GetMessage("PHONE"), array("text", 7)),
    array("ZIP", GetMessage("ZIP"), array("text", 7)),
	array("AUTO_DEFINE_ZONE", GetMessage("AUTO_DEFINE_ZONE"), array("checkbox")),
    array("SEPARATE_ADDR", GetMessage("SEPARATE_ADDR"), array("checkbox")),
    array("STREET", GetMessage("STREET"), array("text", 7)),
    array("HOUSE", GetMessage("HOUSE"), array("text", 7)),
	array("HOUSING", GetMessage("HOUSING"), array("text", 7)),
    array("FLAT", GetMessage("FLAT"), array("text", 7)),

    array("ARTICLE", GetMessage("ARTICLE"), array("text", 7)),
    array("BARCODE", GetMessage("BARCODE"), array("text", 7)),

    // array("ADDRESS", GetMessage("ADDRESS"), array("select" ,$propsArr)),
    array("PAYTYPE_CASH", GetMessage("PAYTYPE_CASH"), array("multiselect" ,$paysystemArr)),
    array("PAYTYPE_CARD", GetMessage("PAYTYPE_CARD"), array("multiselect" ,$paysystemArr)),
    array("PAYTYPE_NO", GetMessage("PAYTYPE_NO"), array("multiselect" ,$paysystemArr)),
    array("SHOW_SUCCESS_PVZ_WRITE", GetMessage("SHOW_SUCCESS_PVZ_WRITE"), array("checkbox")),
    array("ID_OR_NUMBER", GetMessage("ID_OR_NUMBER"), array("select" ,$id_or_number)),
    array("WEIGHT_DEFAULT", GetMessage("WEIGHT_DEFAULT"), array("text", 11)),
    array("TIME_DEFAULT", GetMessage("TIME_DEFAULT"), array("text", 11)),

    array("TIME_PROP_ID", GetMessage("TIME_PROP_ID"), array("text", 11)),
    array("NEED_ROUNDING_ITEMS_RETPRICE", GetMessage("NEED_ROUNDING_ITEMS_RETPRICE"), array("select", array("YES"=>GetMessage("YES"), "NO"=>GetMessage("NO")))),
    array("NEED_ROUNDING_ITEMS_PRICE", GetMessage("NEED_ROUNDING_ITEMS_PRICE"), array("select", array("YES"=>GetMessage("YES"), "NO"=>GetMessage("NO")))),
);

if ($REQUEST_METHOD == 'POST' && strlen($Update)>0 && check_bitrix_sessid()) {
    foreach ($arAllOptions as $arOpt) {
        foreach ($arOpt as $ar) {
            $val = ${$ar[0]};
            if ($ar[2][0] == 'checkbox' && $val != 'Y')
                $val = 'N';
            if($ar[2][0] == 'multiselect' && $val !='')
                $val = serialize($val);
            $f = COption::SetOptionString($module_id, $ar[0], $val);
        }
    }
}


$tabControl->Begin();?>
<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
    <?if(!function_exists('curl_exec'))
        echo CAdminMessage::ShowMessage(GetMessage('NO_CURL'));
    $token = COption::GetOptionString($module_id, "DALLI_TOKEN");
    $pvz_update_diaabled = '';
    if(!DalliservicecomDelivery::checkToken($token)){
        echo CAdminMessage::ShowMessage(GetMessage('TOKEN_ERROR'));
        $pvz_update_diaabled = 'disabled ';
    }
        ?>
    <?  bitrix_sessid_post(); ?>
    <? $tabControl->BeginNextTab();

    function ds_help_hint($option){
        return sprintf("<span id='ds_%s'></span>&nbsp;", strtolower($option));
    }


    if (is_array($arAllOptions)) {
        foreach ($arAllOptions[0] as $Option) {
            $val = COption::GetOptionString($module_id, $Option[0]);

            $type = $Option[2];

            if($type[0] == 'multiselect'){
                if(!$val)
                    $val = array();
                else
                    $val = unserialize($val);
            }
            if ($type[0] == 'checkbox')
                $label = '<label for="' . htmlspecialcharsbx($Option[0]) . '">' . $Option[1] . '</label>';
            elseif($type[0] == 'additional'){
                $label='<input type="button"'.$pvz_update_diaabled.'title="'.$pvz_butt_mess.'" onclick="updatePvz(this)" value="'.$pvz_butt_mess.'" />';
               // var_dump($label);
            }
            else
                $label = $Option[1];

            if ($type[0] == 'checkbox')
                $input = '<input type="checkbox" name="' . htmlspecialcharsbx($Option[0]) . '" id="' . htmlspecialcharsbx($Option[0]) . '" value="Y"' . ($val == 'Y' ? ' checked' : '') . '>';
            elseif ($type[0] == 'text')
                $input = '<input type="text" size="' . $type[1] . '" maxlength="255" value="' . htmlspecialcharsbx($val) . '" name="' . htmlspecialcharsbx($Option[0]) . '">';
            elseif ($type[0] == 'pwd')
                $input = '<input type="tpassword" size="' . $type[1] . '" maxlength="255" value="' . htmlspecialcharsbx($val) . '" name="' . htmlspecialcharsbx($Option[0]) . '">';
            elseif ($type[0] == 'textarea')
                $input = '<textarea rows="' . $type[1] . '" cols="' . $type[2] . '" name="' . htmlspecialcharsbx($Option[0]) . '">' . htmlspecialcharsbx($val) . '</textarea>';
            elseif ($type[0] == 'select') {
                $input = '<select name="' . htmlspecialcharsbx($Option[0]) . '">';
                foreach ($type[1] as $key => $valOpt) {
                    $input .= '<option value="' . htmlspecialcharsbx($key) . '" ' . (($key == htmlspecialcharsbx($val)) ? 'selected="selected"' : '') . '>' . htmlspecialcharsbx($valOpt) . '</option>';
                }
                $input .= '</select>';
            }
            elseif ($type[0] == 'multiselect') {
                $input = '<select multiple name="' . htmlspecialcharsbx($Option[0]) . '[]">';
                foreach ($type[1] as $key => $valOpt) {
                    $input .= '<option value="' . htmlspecialcharsbx($key) . '" ' . ((in_array($key, $val)) ? 'selected="selected"' : '') . '>' . htmlspecialcharsbx($valOpt) . '</option>';
                }
                $input .= '</select>';
            }elseif ($type[0] == 'additional'){
                $input = $pvz_mess;
                //var_dump($arAllOptions);
            }
            $hint = ds_help_hint($Option[0]);


            
            $display = '';

            if(($Option[0] == 'STREET' || $Option[0] == 'HOUSE' || $Option[0] == 'HOUSING' || $Option[0] == 'FLAT') && COption::GetOptionString($module_id, 'SEPARATE_ADDR') != "Y")
                $display = " style='display:none'";
            echo '<tr'.$display.'>
                            <td valign="top">                               
                                '.$label.$hint.'
                            </td>
                            <td valign="top" nowrap>
                                '.$input.'
                            </td>
                        </tr>';
            if($Option[0] == 'PVZ_CHECK')
                echo '<tr class="heading"><td colspan="2">'.GetMessage('FIX_TARIFFS').'</td></tr>';
            if($Option[0] == 'BOXBERRY_PVZ_COST_DEFAULT')
                echo '<tr class="heading"><td colspan="2">'.GetMessage('MIN_TARIFFS').'</td></tr>';
            if($Option[0] == 'BOXBERRY_PVZ_COST_MIN')
                echo '<tr class="heading"><td colspan="2">'.GetMessage('SET_ORDER_PROP').'</td></tr>';
            if($Option[0] == 'FLAT')
                echo '<tr class="heading"><td colspan="2">'.GetMessage('SET_ORDER_PRODUCT_PROP').'</td></tr>';
            if($Option[0] == 'BARCODE')
                echo '<tr class="heading"><td colspan="2">'.GetMessage('PAYTYPE_COMPABILITY').'</td></tr>';
            if($Option[0] == 'PAYTYPE_NO')
                echo '<tr class="heading"><td colspan="2">'.GetMessage('ADDITIONAL_SETTINGS').'</td></tr>';
            unset($hint);
        }
    }
    $tabControl->BeginNextTab();
    ?>
    <tr class="heading">
        <td><?=GetMessage('MODULE_SETTINGS')?></td>
    </tr>
    <tr>
        <td><?=GetMessage('MODULE_SETTINGS_DESCRIPTION')?></td>
    </tr>
    <tr class="heading">
        <td><?=GetMessage('HANDLER')?></td>
    </tr>
    <tr>
        <td><?=GetMessage('HANDLER_DESCRIPTION')?></td>
    </tr>
    <tr class="heading">
        <td><?=GetMessage('SEND_ORDER')?></td>
    </tr>
    <tr>
        <td><?=GetMessage('SEND_ORDER_DESCRIPTION')?></td>
    </tr>
    <tr class="heading">
        <td><?=GetMessage('ORDERS_LOG')?></td>
    </tr>
    <tr>
        <td><?=GetMessage('ORDERS_LOG_DESCRIPTION')?></td>
    </tr>
    <tr class="heading">
        <td><?=GetMessage('PVZ_SYNCH')?></td>
    </tr>
    <tr>
        <td><?=GetMessage('PVZ_SYNCH_DESCRIPTION')?></td>
    </tr>



    <?$tabControl->BeginNextTab();?>

    <?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");?>
    <?$tabControl->Buttons();?>
    <input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
    <input type="reset" name="reset" value="<? echo GetMessage("MAIN_RESET") ?>">
    <input type="button" <? if ($MODULE_RIGHT < "W") echo "disabled" ?> title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="RestoreDefaults();" value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
    <?$tabControl->End();?>
</form>
<script language="JavaScript">
    function RestoreDefaults()
    {
        if (confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))
            window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)."&".bitrix_sessid_get();?>";
    }
    BX.hint_replace(BX('ds_dalli_token'), '<?=GetMessage("DS_TOKEN")?>');
    BX.hint_replace(BX('ds_weight_default'), '<?=GetMessage("DS_WEIGHT")?>');
    BX.hint_replace(BX('ds_ds_courier_cost_default'), '<?=GetMessage("DS_COURIER_COST_DEFAULT_HINT")?>');
    BX.hint_replace(BX('ds_ds_pvz_cost_default'), '<?=GetMessage("DS_PVZ_COST_DEFAULT_HINT")?>');
    BX.hint_replace(BX('ds_sdek_courier_cost_default'), '<?=GetMessage("SDEK_COURIER_COST_DEFAULT_HINT")?>');
    BX.hint_replace(BX('ds_sdek_pvz_cost_default'), '<?=GetMessage("SDEK_PVZ_COST_DEFAULT_HINT")?>');
    BX.hint_replace(BX('ds_boxberry_pvz_cost_default'), '<?=GetMessage("BOXBERRY_PVZ_COST_DEFAULT_HINT")?>');
    //BX.hint_replace(BX('pickup_pvz_cost_default'), '<?=GetMessage("PICKUP_PVZ_COST_DEFAULT_HINT")?>');

    //Min JS set hints
    BX.hint_replace(BX('ds_ds_courier_cost_min'), '<?=GetMessage("DS_COURIER_COST_MIN_HINT")?>');
    BX.hint_replace(BX('ds_ds_pvz_cost_min'), '<?=GetMessage("DS_PVZ_COST_MIN_HINT")?>');
    BX.hint_replace(BX('ds_sdek_courier_cost_min'), '<?=GetMessage("SDEK_COURIER_COST_MIN_HINT")?>');
    BX.hint_replace(BX('ds_sdek_pvz_cost_min'), '<?=GetMessage("SDEK_PVZ_COST_MIN_HINT")?>');
    BX.hint_replace(BX('ds_boxberry_pvz_cost_min'), '<?=GetMessage("BOXBERRY_PVZ_COST_MIN_HINT")?>');

    //    BX.hint_replace(BX('ds_height'), '<?//=GetMessage("DS_HEIGHT")?>');
    //    BX.hint_replace(BX('ds_length'), '<?//=GetMessage("DS_LENGTH")?>');
    //    BX.hint_replace(BX('ds_width'), '<?//=GetMessage("DS_WIDTH")?>');
    BX.hint_replace(BX('ds_person'), '<?=GetMessage("DS_PERSON")?>');
    BX.hint_replace(BX('ds_address'), '<?=GetMessage("DS_ADDRESS")?>');
    BX.hint_replace(BX('ds_phone'), '<?=GetMessage("DS_PHONE")?>');
    BX.hint_replace(BX('ds_zip'), '<?=GetMessage("DS_ZIP")?>');
    BX.hint_replace(BX('ds_street'), '<?=GetMessage("DS_STREET")?>');
    BX.hint_replace(BX('ds_house'), '<?=GetMessage("DS_HOUSE")?>');
	BX.hint_replace(BX('ds_housing'), '<?=GetMessage("DS_HOUSING")?>');
    BX.hint_replace(BX('ds_flat'), '<?=GetMessage("DS_FLAT")?>');

    BX.hint_replace(BX('ds_article'), '<?=GetMessage("DS_ARTICLE")?>');
    BX.hint_replace(BX('ds_barcode'), '<?=GetMessage("DS_BARCODE")?>');

    BX.hint_replace(BX('ds_paytype_card'), '<?=GetMessage("DS_PAYTYPE_CARD")?>');
    BX.hint_replace(BX('ds_paytype_cash'), '<?=GetMessage("DS_PAYTYPE_CASH")?>');
    BX.hint_replace(BX('ds_paytype_no'), '<?=GetMessage("DS_PAYTYPE_NO")?>');
    BX.hint_replace(BX('ds_separate_addr'), '<?=GetMessage("DS_SEPARATE_ADDR")?>');
	BX.hint_replace(BX('ds_auto_define_zone'), '<?=GetMessage("DS_AUTO_DEFINE_ZONE")?>');
    BX.hint_replace(BX('ds_show_success_pvz_write'), '<?=GetMessage("DS_SHOW_PVZ_SUCCESS_WRITE")?>');
    BX.hint_replace(BX('ds_id_or_number'), '<?=GetMessage("DS_ID_OR_NUMBER")?>');
    BX.hint_replace(BX('ds_time_default'), '<?=GetMessage("DS_TIME_DEFAULT")?>');
    BX.hint_replace(BX('ds_time_prop_id'), '<?=GetMessage("DS_TIME_PROP_ID")?>');
	
	/*BX.hint_replace(BX('ds_multi_string_address_hint'), '<?=GetMessage("DS_MULTI_STRING_ADDRESS_HINT")?>');
	BX.hint_replace(BX('ds_multi_string_address_street_hint'), '<?=GetMessage("DS_MULTI_STRING_ADDRESS_STREET_HINT")?>');
	BX.hint_replace(BX('ds_multi_string_address_house_hint'), '<?=GetMessage("DS_MULTI_STRING_ADDRESS_HOUSE_HINT")?>');
	BX.hint_replace(BX('ds_multi_string_address_housing_hint'), '<?=GetMessage("DS_MULTI_STRING_ADDRESS_HOUSING_HINT")?>');
	BX.hint_replace(BX('ds_multi_string_address_flat_hint'), '<?=GetMessage("DS_MULTI_STRING_ADDRESS_FLAT_HINT")?>');*/


    $( "#SEPARATE_ADDR" ).on( "click", function() {
        $( this ).closest('tr').nextAll("tr:eq(0), tr:eq(1), tr:eq(2), tr:eq(3)").toggle();
    });

</script>