<? 
$sModuleId = "dalliservicecom.delivery";
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/prolog.php");
CModule::IncludeModule($sModuleId);
CJSCore::Init(array('jquery', 'dalliservicecom'));
$token = COption::GetOptionString($sModuleId, 'DALLI_TOKEN');

$MODULE_RIGHT = $APPLICATION->GetGroupRight($sModuleId);
if ($MODULE_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "dalliservicecom_delivery";
$oSort = new CAdminSorting($sTableID, "ORDERNO", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

IncludeModuleLangFile(__FILE__);

// MAKE THE FILTER
$arFilterFields = array(
    'filter_orderno',
    'filter_ordercode',
    'filter_company',
    'filter_person',
    'filter_phone',
    'filter_zipcode',
    'filter_town',
    'filter_towncode',
    'filter_zipcode',
    'filter_address',
    'filter_date',
    'filter_time_from_to',
    'filter_weight',
    'filter_quantity',
    'filter_paytype',
    'filter_service',
    'filter_price',
    'filter_inshprice',
    'filter_enclosure',
    'filter_deliveryprice',
    'filter_eventtime',
    'filter_createtimegmt',
    'filter_status',
    'filter_deliveredto',
    'filter_delivereddate',
    'filter_deliveredtime'
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array();
// SETTING THE FILTER CURRENT VALUES
if (strlen($filter_orderno) > 0)
    $arFilter["ORDERNO"] = $filter_orderno;
if (strlen($filter_ordercode) > 0)
    $arFilter["ORDERCODE"] = $filter_ordercode;
if (strlen($filter_company) > 0)
    $arFilter["COMPANY"] = $filter_company;
if (strlen($filter_person) > 0)
    $arFilter["PERSON"] = $filter_person;
if (strlen($filter_phone) > 0)
    $arFilter["PHONE"] = $filter_phone;
if (strlen($filter_zipcode) > 0)
    $arFilter["ZIPCODE"] = $filter_zipcode;
if (strlen($filter_town) > 0)
    $arFilter["TOWN"] = $filter_town;
if (strlen($filter_towncode) > 0)
    $arFilter["TOWNCODE"] = $filter_towncode;
if (strlen($filter_address) > 0)
    $arFilter["ADDRESS"] = $filter_address;
if (strlen($filter_date) > 0)
    $arFilter["DATE"] = $filter_date;
if (strlen($filter_time_from_to) > 0)
    $arFilter["TIME_FROM_TO"] = $filter_time_from_to;
if (strlen($filter_weight) > 0)
    $arFilter["WEIGHT"] = $filter_weight;
if (strlen($filter_quantity) > 0)
    $arFilter["QUANTITY"] = $filter_quantity;
if (strlen($filter_paytype) > 0)
    $arFilter["PAYTYPE"] = $filter_paytype;
if (strlen($filter_service) > 0)
    $arFilter["SERVICE"] = $filter_service;
if (strlen($filter_price) > 0)
    $arFilter["PRICE"] = $filter_price;
if (strlen($filter_inshprice) > 0)
    $arFilter["INSHPRICE"] = $filter_inshprice;
if (strlen($filter_enclosure) > 0)
    $arFilter["ENCLOSURE"] = $filter_enclosure;
if (strlen($filter_instruction) > 0)
    $arFilter["INSTRUCTION"] = $filter_instruction;
if (strlen($filter_deliveryprice) > 0)
    $arFilter["DELIVERYPRICE"] = $filter_deliveryprice;
if (strlen($filter_eventtime) > 0)
    $arFilter["EVENTTIME"] = $filter_eventtime;
if (strlen($filter_createtimegmt) > 0)
    $arFilter["CREATETIMEGMT"] = $filter_createtimegmt;
if (strlen($filter_status) > 0)
    $arFilter["STATUS"] = $filter_status;
if (strlen($filter_deliveredto) > 0)
    $arFilter["DELIVEREDTO"] = $filter_deliveredto;
if (strlen($filter_delivereddate) > 0)
    $arFilter["DELIVEREDDATE"] = $filter_delivereddate;
if (strlen($filter_deliveredtime) > 0)
    $arFilter["DELIVEREDTIME"] = $filter_deliveredtime;

// LOAD DATA
if(DalliservicecomDelivery::checkToken($token))
    $dbResultList = DalliservicecomDeliveryDB::GetList($arFilter, array($by=>$order));

$dbResultList = new CAdminResult($dbResultList, $sTableID);
$dbResultList->NavStart();


// NAV PARAMS
//$lAdmin->NavText($dbResultList->GetNavPrint('RRR'));

// THE LIST HEADER
$lAdminHeader = array(
    array("id" => "ORDERNO", "content" => GetMessage("ORDERNO"), "sort" => "ORDERNO", "default" => true),
    array("id" => "ORDERCODE", "content" => GetMessage("ORDERCODE"), "sort" => "ORDERCODE", "default" => true),
    array("id" => "COMPANY", "content" => GetMessage("COMPANY"), "sort" => "COMPANY", "default" => false),
    array("id" => "PERSON", "content" => GetMessage("PERSON"), "sort" => "PERSON", "default" => true),
    array("id" => "PHONE", "content" => GetMessage("PHONE"), "sort" => "PHONE", "default" => true),
    array("id" => "ZIPCODE", "content" => GetMessage("ZIPCODE"), "sort" => "ZIPCODE", "default" => false),
    array("id" => "TOWN", "content" => GetMessage("TOWN"), "sort" => "TOWN", "default" => true),
    array("id" => "TOWNCODE", "content" => GetMessage("TOWNCODE"), "sort" => "TOWNCODE", "default" => false),
    array("id" => "ADDRESS", "content" => GetMessage("ADDRESS"), "sort" => "ADDRESS", "default" => true),
    array("id" => "DATE", "content" => GetMessage("DATE"), "sort" => "DATE", "default" => true),
    array("id" => "TIME_FROM_TO", "content" => GetMessage("TIME_FROM_TO"), "sort" => "TIME_FROM_TO", "default" => true),
    array("id" => "WEIGHT", "content" => GetMessage("WEIGHT"), "sort" => "WEIGHT", "default" => true),
    array("id" => "QUANTITY", "content" => GetMessage("QUANTITY"), "sort" => "QUANTITY", "default" => false),
    array("id" => "PAYTYPE", "content" => GetMessage("PAYTYPE"), "sort" => "PAYTYPE", "default" => true),
    array("id" => "SERVICE", "content" => GetMessage("SERVICE"), "sort" => "SERVICE", "default" => true),
    array("id" => "PRICE", "content" => GetMessage("PRICE"), "sort" => "PRICE", "default" => true),
    array("id" => "INSHPRICE", "content" => GetMessage("INSHPRICE"), "sort" => "INSHPRICE", "default" => false),
    array("id" => "ENCLOSURE", "content" => GetMessage("ENCLOSURE"), "sort" => "ENCLOSURE", "default" => false),
    array("id" => "INSTRUCTION", "content" => GetMessage("INSTRUCTION"), "sort" => "INSTRUCTION", "default" => false),
    array("id" => "DELIVERYPRICE", "content" => GetMessage("DELIVERYPRICE"), "sort" => "DELIVERYPRICE", "default" => true),
    array("id" => "EVENTTIME", "content" => GetMessage("EVENTTIME"), "sort" => "EVENTTIME", "default" => false),
    array("id" => "CREATETIMEGMT", "content" => GetMessage("CREATETIMEGMT"), "sort" => "CREATETIMEGMT", "default" => false),
    array("id" => "STATUS", "content" => GetMessage("STATUS"), "sort" => "STATUS", "default" => true),
    array("id" => "DELIVEREDTO", "content" => GetMessage("DELIVEREDTO"), "sort" => "DELIVEREDTO", "default" => false),
    array("id" => "DELIVEREDDATE", "content" => GetMessage("DELIVEREDDATE"), "sort" => "DELIVEREDDATE", "default" => true),
    array("id" => "DELIVEREDTIME", "content" => GetMessage("DELIVEREDTIME"), "sort" => "DELIVEREDTIME", "default" => true)
);

$lAdmin->AddHeaders($lAdminHeader);

$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();
//echo "<pre>";
// MAKE THE LIST
while ($arResult = $dbResultList->GetNext()) {
    //print_r($arResult);
    switch ($arResult['PAYTYPE']) {
        case 'CARD':
            $arResult['PAYTYPE'] = GetMessage('CASH');
            break;
        case 'CASH':
            $arResult['PAYTYPE'] = GetMessage('CARD');
            break;
        case 'NO':
            $arResult['PAYTYPE'] = GetMessage('NO');
            break;
    }
    switch ($arResult['SERVICE']) {
        case '1':
            $arResult['SERVICE'] = GetMessage('MSK_MO');
            break;
        case '2':
            $arResult['SERVICE'] = GetMessage('EXPRESS_MSK');
            break;
        case '3':
            $arResult['SERVICE'] = GetMessage('SPECIAL_MSK');
            break;
        case '4':
            $arResult['SERVICE'] = GetMessage('PVZ_DS');
            break;
        case '5':
            $arResult['SERVICE'] = GetMessage('TOOK_FROM_MAGAZ');
            break;
        case '6':
            $arResult['SERVICE'] = GetMessage('TOOK_FROM_SUPPLIER');
            break;
        case '7':
            $arResult['SERVICE'] = GetMessage('PRODUCT_BACK');
            break;
        case '8':
            $arResult['SERVICE'] = GetMessage('CASH_BACK');
            break;
        case '9':
            $arResult['SERVICE'] = GetMessage('PVZ_SDEK');
            break;
        case '10':
            $arResult['SERVICE'] = GetMessage('COURIER_SDEK');
            break;
        case '11':
            $arResult['SERVICE'] = GetMessage('SPB_LO');
            break;
        case '12':
            $arResult['SERVICE'] = GetMessage('NO_STANDART');
            break;
        case '13':
            $arResult['SERVICE'] = GetMessage('PVZ_BOXBERRY');
            break;
        case '14':
            $arResult['SERVICE'] = GetMessage('PVZ_PICKUP');
            break;
    }
    switch ($arResult['STATUS']) {
        case 'NEW':
            $arResult['STATUS'] = GetMessage('NEW');
            break;
        case 'ACCEPTED':
            $arResult['STATUS'] = GetMessage('ACCEPTED');
            break;
        case 'DEPARTURING':
            $arResult['STATUS'] = GetMessage('DEPARTURING');
            break;
        case 'DEPARTURE':
            $arResult['STATUS'] = GetMessage('DEPARTURE');
            break;
        case 'DELIVERY':
            $arResult['STATUS'] = GetMessage('DELIVERY');
            break;
        case 'COURIERDELIVERED':
            $arResult['STATUS'] = GetMessage('COURIERDELIVERED');
            break;
        case 'COMPLETE':
            $arResult['STATUS'] = GetMessage('COMPLETE');
            break;
        case 'PARTIALLY':
            $arResult['STATUS'] = GetMessage('PARTIALLY');
            break;
        case 'COURIERRETURN':
            $arResult['STATUS'] = GetMessage('COURIERRETURN');
            break;
        case 'CANCELED':
            $arResult['STATUS'] = GetMessage('CANCELED');
            break;
        case 'RETURNING':
            $arResult['STATUS'] = GetMessage('RETURNING');
            break;
        case 'RETURNED':
            $arResult['STATUS'] = GetMessage('RETURNED');
            break;
        case 'CONFIRM':
            $arResult['STATUS'] = GetMessage('CONFIRM');
            break;
        case 'DATECHANGE':
            $arResult['STATUS'] = GetMessage('DATECHANGE');
            break;
        case 'PICKUPREADY':
            $arResult['STATUS'] = GetMessage('PICKUPREADY');
            break;

    }
    if($arResult['DELIVEREDDATE'] == '0000-00-00')
        $arResult['DELIVEREDDATE'] = '-';
    if($arResult['DELIVEREDTIME'] == '0000-00-00 00:00:00')
        $arResult['DELIVEREDTIME'] = '-';

    $row = &$lAdmin->AddRow($arResult['ORDERNO'], $arResult, "", GetMessage('ORDER'));
    $row->AddField("ORDERNO", $arResult['ORDERNO']);
    $row->AddField("ORDERCODE", $arResult['ORDERCODE']);
    $row->AddField("COMPANY", $arResult['COMPANY']);
    $row->AddField("PERSON", $arResult['PERSON']);
    $row->AddField("PHONE", $arResult['PHONE']);
    $row->AddField("ZIPCODE", $arResult['ZIPCODE']);
    $row->AddField("TOWN", $arResult['TOWN']);
    $row->AddField("TOWNCODE", $arResult['TOWNCODE']);
    $row->AddField("ADDRESS", $arResult['ADDRESS']);
    $row->AddField("DATE", $arResult['DATE']);
    $row->AddField("TIME_FROM_TO", $arResult['TIME_FROM_TO']);
    $row->AddField("WEIGHT", $arResult['WEIGHT']);
    $row->AddField("QUANTITY", $arResult['QUANTITY']);
    $row->AddField("PAYTYPE", $arResult['PAYTYPE']);
    $row->AddField("SERVICE", $arResult['SERVICE']);
    $row->AddField("PRICE", $arResult['PRICE']);
    $row->AddField("INSHPRICE", $arResult['INSHPRICE']);
    $row->AddField("ENCLOSURE", $arResult['ENCLOSURE']);
    $row->AddField("INSTRUCTION", $arResult['INSTRUCTION']);
    $row->AddField("DELIVERYPRICE", $arResult['DELIVERYPRICE']);
    $row->AddField("EVENTTIME", $arResult['EVENTTIME']);
    $row->AddField("CREATETIMEGMT", $arResult['CREATETIMEGMT']);
    $row->AddField("STATUS", $arResult['STATUS']);
    $row->AddField("DELIVEREDTO", $arResult['DELIVEREDTO']);
    $row->AddField("DELIVEREDDATE", $arResult['DELIVEREDDATE']);
    $row->AddField("DELIVEREDTIME", $arResult['DELIVEREDTIME']);
}

$APPLICATION->SetTitle(GetMessage('ORDERS_IN_DALLI_SERVICE'));
/*$aContext = array(
    array(
        "TEXT" => '',
        "TITLE" => ,
        "ICON" => "btn_green",
        "LINK" => 'javascript:func()',
        "LINK_PARAM" => "id=''"
    )
);*/
//$lAdmin->AddAdminContextMenu($aContext);
$lAdmin->AddAdminContextMenu();
$lAdmin->CheckListMode();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");?>
<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?">
<?
$oFilter = new CAdminFilter(
    $sTableID . "_filter",
    array(
        GetMessage('ORDERNO'),
        GetMessage('ORDERCODE')
    )
);
$oFilter->SetDefaultRows(array("filter_id"));
$oFilter->Begin();?>

    <tr>
        <td><?= GetMessage('ORDERNO') ?>:</td>
        <td align="left">
            <input type="text" name="filter_orderno" size="50" value="<?= htmlspecialcharsEx($filter_orderno) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('ORDERCODE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_ordercode" size="50" value="<?= htmlspecialcharsEx($filter_ordercode)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('COMPANY') ?>:</td>
        <td align="left">
            <input type="text" name="filter_company" size="50" value="<?= htmlspecialcharsEx($filter_company)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('PERSON') ?>:</td>
        <td align="left">
            <input type="text" name="filter_person" size="50" value="<?= htmlspecialcharsEx($filter_person) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('PHONE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_phone" size="50" value="<?= htmlspecialcharsEx($filter_phone)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('ZIPCODE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_zipcode" size="50" value="<?= htmlspecialcharsEx($filter_zipcode) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('TOWN') ?>:</td>
        <td align="left">
            <input type="text" name="filter_town" size="50" value="<?= htmlspecialcharsEx($filter_town)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('TOWNCODE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_towncode" size="50" value="<?= htmlspecialcharsEx($filter_new_link) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('ADDRESS') ?>:</td>
        <td align="left">
            <input type="text" name="filter_address" size="50" value="<?= htmlspecialcharsEx($filter_address)?>">
            &nbsp;<?=ShowFilterLogicHelp()?>
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('DATE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_date" size="50" value="<?= htmlspecialcharsEx($filter_date) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('TIME_FROM_TO') ?>:</td>
        <td align="left">
            <input type="text" name="filter_time_from_to" size="50" value="<?= htmlspecialcharsEx($filter_time_from_to)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('WEIGHT') ?>:</td>
        <td align="left">
            <input type="text" name="filter_weight" size="50" value="<?= htmlspecialcharsEx($filter_weight)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('QUANTITY') ?>:</td>
        <td align="left">
            <input type="text" name="filter_quantity" size="50" value="<?= htmlspecialcharsEx($filter_quantity) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('PAYTYPE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_paytype" size="50" value="<?= htmlspecialcharsEx($filter_paytype)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('SERVICE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_service" size="50" value="<?= htmlspecialcharsEx($filter_service) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('PRICE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_price" size="50" value="<?= htmlspecialcharsEx($filter_price)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('INSHPRICE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_inshprice" size="50" value="<?= htmlspecialcharsEx($filter_inshprice) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('ENCLOSURE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_enclosure" size="50" value="<?= htmlspecialcharsEx($filter_enclosure)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('INSTRUCTION') ?>:</td>
        <td align="left">
            <input type="text" name="filter_instruction" size="50" value="<?= htmlspecialcharsEx($filter_instruction) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('DELIVERYPRICE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_deliveryprice" size="50" value="<?= htmlspecialcharsEx($filter_deliveryprice)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('EVENTTIME') ?>:</td>
        <td align="left">
            <input type="text" name="filter_eventtime" size="50" value="<?= htmlspecialcharsEx($filter_eventtime) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('CREATETIMEGMT') ?>:</td>
        <td align="left">
            <input type="text" name="filter_createtimegmt" size="50" value="<?= htmlspecialcharsEx($filter_createtimegmt)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('STATUS') ?>:</td>
        <td align="left">
            <input type="text" name="filter_status" size="50" value="<?= htmlspecialcharsEx($filter_status) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('DELIVEREDTO') ?>:</td>
        <td align="left">
            <input type="text" name="filter_deliveredto" size="50" value="<?= htmlspecialcharsEx($filter_deliveredto)?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('DELIVEREDDATE') ?>:</td>
        <td align="left">
            <input type="text" name="filter_delivereddate" size="50" value="<?= htmlspecialcharsEx($filter_delivereddate) ?>">
        </td>
    </tr>
    <tr>
        <td><?= GetMessage('DELIVEREDTIME') ?>:</td>
        <td align="left">
            <input type="text" name="filter_deliveredtime" size="50" value="<?= htmlspecialcharsEx($filter_deliveredtime)?>">
        </td>
    </tr>
<?$oFilter->Buttons(
    array(
        "table_id" => $sTableID,
        "url" => $APPLICATION->GetCurPage(),
        "form" => "find_form"
    )
);
$oFilter->End();?>
</form>
<?if(DalliservicecomDelivery::checkToken($token)):?>
    <input type="button" value="<?=GetMessage("UPDATE_ORDERS")?>" onclick="updateOrders(this)"><br><br>
<?endif;?>


<?// DISPLAY LIST
$lAdmin->DisplayList();?>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");?>
