<?CModule::AddAutoloadClasses(
    "dalliservicecom.delivery",
    array(
        'DalliservicecomDelivery' => "classes/general/dalliservicecom_delivery.php",
        'DalliservicecomDeliveryDB' => "classes/mysql/dalliservicecom_delivery.php",
    )
);
global $APPLICATION;
CJSCore::RegisterExt('dalliservicecom', array(
    'js' => '/bitrix/js/dalliservicecom.delivery/dalliservicecom.js',
    'css' => '/bitrix/js/dalliservicecom.delivery/css/dalliservicecom.css',
    'lang' => '/bitrix/modules/dalliservicecom.delivery/lang/'.LANGUAGE_ID.'/js/dalliservicecom_js.php'
));
if (!defined("ADMIN_SECTION")){
    $APPLICATION->AddHeadString('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>',true);
    CUtil::InitJSCore( array('ajax' , 'popup', 'jquery', 'dalliservicecom'));
}
?>
