<?global $MESS;
$MESS['CHOOZE'] = "ВЫБРАТЬ";
$MESS['CONFIRM_CHOOZE'] = "Подтвердить выбор";
$MESS['ADDITIONAL'] = "Дополнительно";
$MESS['ORDERS_UPDATE'] = "Заказы успешно обновлены";
$MESS['ORDERS_UPDATE_ERROR'] = "Что-то пошло не так. Попробуйте позже!"; 
$MESS['PVZ_UPDATE'] = "Список ПВЗ успешно обновлен!";
$MESS['PVZ_UPDATE_ERROR'] = "Что-то пошло не так. Попробуйте позже!";
$MESS['PVZ_WRITE_SUCCESS'] = "<div style='text-align: center'>Адрес ПВЗ записан в поле <br>\"Адрес доставки\" блока \"Покупатель\"</div>";

?>