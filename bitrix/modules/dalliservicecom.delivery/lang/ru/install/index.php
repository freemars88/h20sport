<?
$MESS["dalliservicecom.delivery_MODULE_NAME"] = "Dalli-Service";
$MESS["dalliservicecom.delivery_MODULE_DESC"] = "Курьерская служба доставки для интернет-магазинов";
$MESS["dalliservicecom.delivery_PARTNER_NAME"] = "Dalli-Service";
$MESS["dalliservicecom.delivery_PARTNER_URI"] = "https://dalli-service.com";
$MESS["DS_INSTALL_TITLE"] = "Установка модуля интеграции со службой доставки Dalli-Service";
$MESS["DS_PERM_D"] = "Доступ запрещен"; 
$MESS["DS_PERM_W"] = "Полный доступ";
?>