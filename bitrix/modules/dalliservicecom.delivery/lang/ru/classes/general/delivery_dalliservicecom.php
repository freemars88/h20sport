<?
global $MESS;
$MESS["DS_HANDLER_NAME"] = "DALLI-SERVICE";
$MESS["DS_HANDLER_DESCR"] = "Курьерская служба доставки для интернет-магазинов";
$MESS["DS_COURIER_NAME"] = "Курьерская доставка DALLI-SERVICE";
$MESS["DS_COURIER_DESCR"] = "Курьерская доставка DALLI-SERVICE";
$MESS["DS_PVZ_NAME"] = "ПВЗ DALLI-SERVICE";
$MESS["DS_PVZ_DESCR"] = "ПВЗ DALLI-SERVICE";
$MESS["SDEK_COURIER_NAME"] = "Курьерская доставка SDEK";
$MESS["SDEK_COURIER_DESCR"] = "Курьерская доставка SDEK";
$MESS["SDEK_PVZ_NAME"] = "ПВЗ SDEK";
$MESS["SDEK_PVZ_DESCR"] = "ПВЗ SDEK";
$MESS["BOXBERRY_PVZ_NAME"] = "ПВЗ BOXBERRY";
$MESS["BOXBERRY_PVZ_DESCR"] = "ПВЗ BOXBERRY"; 
$MESS["PICKUP_PVZ_NAME"] = "ПВЗ PICK-UP";
$MESS["PICKUP_PVZ_DESCR"] = "ПВЗ PICK-UP";
$MESS["DS_CHOOZE_PVZ"] = "Выберите ПВЗ";
$MESS["DS_CHOOZE_PVZ_TITLE"] = "Выберите пункт выдачи заказа";
$MESS["DALLI_SERVICE_DESCR"] = "Самовывоз из пункта выдачи заказа Dalli-Service";
$MESS["MOSKOW_REGION"] = "Московская";
$MESS["MOSKOW"] = "Москва";
$MESS["LENINGRAD"] = "Санкт-Петербург";
$MESS["LENINGRAD_REGION"] = "Ленинградская";
$MESS["ERROR_MESS"] = "Не удалось рассчитать стоимость доставки";
$MESS["RUB"] = "р.";
?>