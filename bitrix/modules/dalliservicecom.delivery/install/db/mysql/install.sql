create table if not exists ds_orders_log
(
    ORDERNO varchar(12) NULL,
    ORDERCODE varchar(12) NOT NULL,
    COMPANY varchar(255) NOT NULL,
    PERSON varchar(255) NOT NULL,
    PHONE  varchar(255) NOT NULL,
    ZIPCODE varchar(10) NOT NULL,
    TOWN varchar(20) NOT NULL,
    TOWNCODE varchar(20) NOT NULL,
    ADDRESS varchar(100) NOT NULL,
    DATE date NOT NULL,
    TIME_FROM_TO CHAR(17) NOT NULL,
    WEIGHT double(18,2) NOT NULL,
    QUANTITY int NOT NULL,
    PAYTYPE CHAR(4) NOT NULL,
    SERVICE TINYINT NOT NULL,
    PRICE double(18,2) NOT NULL,
    INSHPRICE double(18,2) NOT NULL,
    ENCLOSURE TEXT NULL,
    INSTRUCTION TEXT NULL,
    DELIVERYPRICE double(18,2) NOT NULL,
    EVENTTIME datetime NULL,
    CREATETIMEGMT datetime NULL,
    STATUS varchar(20) NOT NULL,
    DELIVEREDTO TEXT NULL,
    DELIVEREDDATE date NULL,
    DELIVEREDTIME datetime NULL
);

create table if not exists ds_pvz_list
(
    CODE varchar(12) NULL,
    NAME varchar(100) NOT NULL,
    TOWN varchar(100) NOT NULL,
    ADDRESS varchar(255) NOT NULL,
    ADDRESSREDUCE  varchar(255) NULL,
    DESCRIPTION text NULL,
    ONLYPREPAID varchar(2) NULL,
    WORKSHEDULE varchar(255) NULL,
    GPS varchar(100) NOT NULL,
    PHONE varchar(100) NULL,
    PARTNER varchar(12) NOT NULL,
    WEIGHTLIMIT double(18,2) NULL
);

delete from b_sale_delivery_srv where CODE = 'dalli_service:pickup_pvz'; 