<?
IncludeModuleLangFile(__FILE__);
Class dalliservicecom_delivery extends CModule
{
    var $MODULE_ID = 'dalliservicecom.delivery';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__).'/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = GetMessage('dalliservicecom.delivery_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('dalliservicecom.delivery_MODULE_DESC');

        $this->PARTNER_NAME = GetMessage('dalliservicecom.delivery_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('dalliservicecom.delivery_PARTNER_URI');
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallDB();
        $this->InstallFiles();

        $APPLICATION->IncludeAdminFile(GetMessage('DS_INSTALL_TITLE'), $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/step1.php');
    }

    function InstallDB()
    {
        global $DB, $APPLICATION, $errors;

        if(!$DB->Query("SELECT 'x' FROM ds_orders_log", true))
            $errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/db/mysql/install.sql');

        if (!empty($errors))
        {
            $APPLICATION->ThrowException(implode('. ', $errors));
            return false;
        }
        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'DalliservicecomDelivery', 'showSendForm');
        RegisterModuleDependences('main', 'OnEpilog', $this->MODULE_ID, 'DalliservicecomDelivery', 'sendOrder2DS');
        $date = new DateTime();
        $date->modify("+1 day");
        CAgent::AddAgent('DalliservicecomDeliveryDB::orderSynchAgent();', $this->MODULE_ID, '', 3600, '', 'N', '');
        CAgent::AddAgent('DalliservicecomDeliveryDB::pvzSynchAgent();', $this->MODULE_ID, '', 86400, '', 'Y', $date->format("d.m.Y")." 07:20:00");

        return true;
    }

    function DoUninstall()
    {
        global $APPLICATION;
        $this->UnInstallFiles();
        $this->UnInstallDB();
        UnRegisterModule($this->MODULE_ID);
    }

    function UnInstallDB()
    {
        global $APPLICATION, $DB, $errors;
        $errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/db/mysql/uninstall.sql');
        if (!empty($errors))
        {
            $APPLICATION->ThrowException(implode('', $errors));
            return false;
        }
        UnRegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'DalliservicecomDelivery', 'showSendForm');
        UnRegisterModuleDependences('main', 'OnEpilog', $this->MODULE_ID, 'DalliservicecomDelivery', 'sendOrder2DS');
        CAgent::RemoveModuleAgents($this->MODULE_ID);
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/', true, true);
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/sale_delivery', $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/sale_delivery/', true, true);
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/js', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js/'.$this->MODULE_ID, true, true);
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/images', $_SERVER['DOCUMENT_ROOT'].'/bitrix/images/'.$this->MODULE_ID, true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/');
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/sale_delivery', $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/sale_delivery/');
        DeleteDirFilesEx('/bitrix/js/'.$this->MODULE_ID);
        DeleteDirFilesEx('/bitrix/images/'.$this->MODULE_ID);
    }

    function GetModuleRightList()
    {
        $arr = array(
            'reference_id' => array('D', 'W'),
            'reference' => array(
                '[D] '.GetMessage('DS_PERM_D'),
                '[W] '.GetMessage('DS_PERM_W')
            )
        );
        return $arr;
    }
}
?>