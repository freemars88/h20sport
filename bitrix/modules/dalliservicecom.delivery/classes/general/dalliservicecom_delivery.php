<?php 
IncludeModuleLangFile(__FILE__);
class DalliservicecomDelivery {
    const MODULE_ID = 'dalliservicecom.delivery';
    static public function showSendForm(&$items){
        global $APPLICATION;

        $MODULE_RIGHT = $APPLICATION->GetGroupRight(self::MODULE_ID);

        $token = COption::GetOptionString(self::MODULE_ID, 'DALLI_TOKEN');

        if ($MODULE_RIGHT == "W" && self::checkToken($token)){

            CUtil::InitJSCore( array('ajax', 'popup', 'jquery', 'dalliservicecom'));

            if ($_SERVER['REQUEST_METHOD']=='GET' && $GLOBALS['APPLICATION']->GetCurPage()=='/bitrix/admin/sale_order_view.php' && $_REQUEST['ID']>0){

                $id = $_GET["ID"];
                $arOrder = CSaleOrder::GetByID($id);
				$price = 0;

                /**
                 * ����� ������.
                 **/
                $orderId = COption::GetOptionString(self::MODULE_ID, "ID_OR_NUMBER") == 'number' ? $arOrder['ACCOUNT_NUMBER'] : $arOrder['ID'];

                /**
                 * ���������� ��������� ������
                 **/
                $separate_addr = COption::GetOptionString(self::MODULE_ID, 'SEPARATE_ADDR') == "Y" ? true : false;


                $paySystemId = $arOrder["PAY_SYSTEM_ID"];
                $description = $arOrder['USER_DESCRIPTION'];

                $person_prop = COption::GetOptionString(self::MODULE_ID, "PERSON");
                $address_prop = COption::GetOptionString(self::MODULE_ID, "ADDRESS");
                $phone_prop = COption::GetOptionString(self::MODULE_ID, "PHONE");
                $zip_prop = COption::GetOptionString(self::MODULE_ID, "ZIP");

                if($separate_addr){
                    $street_prop = COption::GetOptionString(self::MODULE_ID, "STREET");
                    $house_prop = COption::GetOptionString(self::MODULE_ID, "HOUSE");
					$housing_prop = COption::GetOptionString(self::MODULE_ID, "HOUSING");
                    $flat_prop = COption::GetOptionString(self::MODULE_ID, "FLAT");
                }

                $paytype_cash_arr = unserialize(COption::GetOptionString(self::MODULE_ID, "PAYTYPE_CASH"));
                $paytype_card_arr = unserialize(COption::GetOptionString(self::MODULE_ID, "PAYTYPE_CARD"));
                $paytype_no_arr = unserialize(COption::GetOptionString(self::MODULE_ID, "PAYTYPE_NO"));

                $time_min_max = false;

                $time_prop = COption::GetOptionString(self::MODULE_ID, "TIME_PROP_ID");

                if(is_array($paytype_cash_arr) && in_array($paySystemId, $paytype_cash_arr))
                    $cash = 'selected';
                if(is_array($paytype_card_arr) && in_array($paySystemId, $paytype_card_arr))
                    $card = 'selected';
                if(is_array($paytype_no_arr) && in_array($paySystemId, $paytype_no_arr))
                    $no = 'selected';


                $delivery = $arOrder["DELIVERY_ID"];
                $price_delivery = $arOrder["PRICE_DELIVERY"];

                $db_props = CSaleOrderPropsValue::GetOrderProps($id);
                
                $zm_i = 1;

                while ($arProps = $db_props->Fetch()){

                    if($time_prop && $arProps["ORDER_PROPS_ID"] == $time_prop){
                        $time_min_max = $arProps["VALUE"];
                    }

                    switch ($arProps["CODE"]) {
                        case $person_prop:
                            $fio = $arProps["VALUE"];
                        break;
                        case $phone_prop:
                            $phone = $arProps["VALUE"];
                        break;
                        case $zip_prop:
                            $zip = $arProps["VALUE"];
                        break;
                        case "LOCATION":
                            $cityArr = CSaleLocation::GetByID($arProps["VALUE"], LANGUAGE_ID);
                            $city = $cityArr["CITY_NAME"];
                            $region = $arLocationTo["REGION_NAME"];
                        break;
                    }

                    if($separate_addr && ($delivery == 'dalli_service:dalli_courier' || $delivery == 'dalli_service:sdek_courier')){
                        if ($arProps["CODE"] == $street_prop) $address .= GetMessage('STREET').' '.$arProps["VALUE"].($zm_i < 10 ? ", " : " ");
                        if ($arProps["CODE"] == $house_prop) $address .= GetMessage('HOUSE').' '.$arProps["VALUE"].($zm_i < 10 ? ", " : " ");
                        if ($arProps["CODE"] == $housing_prop) $address .= GetMessage('HOUSING').' '.$arProps["VALUE"].($zm_i < 10 ? ", " : " ");
                        if ($arProps["CODE"] == $flat_prop) $address .= GetMessage('FLAT').' '.$arProps["VALUE"].($zm_i < 10 ? ", " : " ");
                        
                        $zm_i++;
                    }else{
                        if ($arProps["CODE"] == $address_prop) $address = $arProps["VALUE"];
                    }

                }
				
                switch ($delivery) {
                    case "dalli_service:dalli_courier":
                        if(strpos($region, GetMessage('MOSKOW_REGION')) !== false || $city == GetMessage('MOSKOW'))
                            $msk_mo = 'selected';

                        elseif(strpos($region, GetMessage('LENINGRAD_REGION')) !== false || $city == GetMessage('LENINGRAD'))
                            $spb_lo = 'selected';
                    break;
                    case "dalli_service:sdek_courier":
                        $kur_sdek = 'selected';
                    break;
                    case "dalli_service:dalli_pvz":
                        $pvz_ds = 'selected';
                    break;
                    case "dalli_service:sdek_pvz":
                        $pvz_sdek = 'selected';
                    break;
                    case "dalli_service:boxberry_pvz":
                        $pvz_boxberry = 'selected';
                    break;
                    case "dalli_service:pickup_pvz":
                        $pvz_pickup = 'selected';
                    break;
                }

                $time_min = "10:00";
                $time_max = "18:00";
                //$time_setting_default = COption::GetOptionString(self::MODULE_ID, "TIME_DEFAULT");
                
                if(!$time_min_max){
                    $time_min_max = COption::GetOptionString(self::MODULE_ID, "TIME_DEFAULT");
                }
                
                if(strlen(trim($time_min_max)) > 0){
                    $timeArr = explode('-', $time_min_max);
                    $time_min = $timeArr[0];
                    $time_max = $timeArr[1];
                }
                $basketItems = CSaleBasket::GetList(
                    array(),
                    array("ORDER_ID" => $id),
                    false,
                    false,
                    array("PRICE", "QUANTITY", "NAME", "WEIGHT", "*")
                );
                $items_count = 0;
                while ($arItems = $basketItems->Fetch())
                {
                    $items_count++;

                    $zm_article = '';
                    $zm_barcode = '';

                    if($arItems["PRODUCT_ID"]){
                        $zm_res_item = CIBlockElement::GetByID($arItems["PRODUCT_ID"]);
                        
                        if($zm_ar_item = $zm_res_item->GetNext()){
                            $res_item_props = CIBlockElement::GetProperty($zm_ar_item['IBLOCK_ID'], $arItems["PRODUCT_ID"], "sort", "asc", array());
                            
                            while($ar_item_prop = $res_item_props->GetNext()){
                                if($ar_item_prop['CODE'] == COption::GetOptionString(self::MODULE_ID, 'ARTICLE')){
                                    $zm_article = $ar_item_prop['VALUE'];
                                }
                                if($ar_item_prop['CODE'] == COption::GetOptionString(self::MODULE_ID, 'BARCODE')){
                                    $zm_barcode = $ar_item_prop['VALUE'];
                                }
                            }
                        }
                    }

                    if($arItems["WEIGHT"] == '' || $arItems["WEIGHT"] == 0)
                        $arItems["WEIGHT"] = COption::GetOptionString(self::MODULE_ID, 'WEIGHT_DEFAULT')*1000;
                    if($arItems["WEIGHT"] == '' || $arItems["WEIGHT"] == 0)
                        $arItems["WEIGHT"] = 0.1*1000;
                    $weight += $arItems["WEIGHT"]*$arItems["QUANTITY"];
                    //$price_order += $arItems['PRICE'] * $arItems['QUANTITY'];
                    $items_weight = $arItems['WEIGHT']/1000;
                    if(COption::GetOptionString(self::MODULE_ID, 'NEED_ROUNDING_ITEMS_RETPRICE')  == "YES"){
                        $items_price = ceil((float)$arItems['PRICE']);
                        $price += $items_price * $arItems['QUANTITY'];
                    }elseif (COption::GetOptionString(self::MODULE_ID, 'NEED_ROUNDING_ITEMS_PRICE') == "YES"){
                        $items_price = ceil((float)$arItems['PRICE'] * $arItems["QUANTITY"]) / $arItems["QUANTITY"];
                        $price += $items_price * $arItems['QUANTITY'];
                    }else{
                        $items_price = (float)$arItems['PRICE'];
                        $price = $arOrder['PRICE'] - $price_delivery;
                    }
                    $item_name = preg_replace("/[^,.-;\w\d\s]/ui", "", $arItems["NAME"]);

                    /**
                     * ������ �������� ������� � ����� ��������
                     */
                    $items_trs .= "<tr>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<input size="2" type="text" name="item_quantity_%s" value="%s">', $items_count, $arItems["QUANTITY"]);
                        $items_trs .= "</td>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<input size="3" type="text" name="item_article_%s" value="%s">', $items_count, $zm_article);
                        $items_trs .= "</td>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<input size="3" type="text" name="item_barcode_%s" value="%s">', $items_count, $zm_barcode);
                        $items_trs .= "</td>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<input size="2" type="text" name="item_mass_%s" value="%s">', $items_count, $items_weight);
                        $items_trs .= "</td>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<input size="3" type="text" name="item_retprice_%s" value="%s">', $items_count, $items_price);
                        $items_trs .= "</td>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<input size="23" type="text" name="item_name_%s" value="%s">', $items_count, $item_name);
                        $items_trs .= "</td>";
                        $items_trs .= "<td>";
                            $items_trs .= sprintf('<a title="%s" onclick="remove_prod_pos_line(this)" class="prod_pos">X</a>', GetMessage('REMOVE_PROD_POS'));
                        $items_trs .= "</td>";
                    $items_trs .= "</tr>";

                }
                $price +=  $price_delivery;
                $weight = $weight/1000;
                $place_quantity = 1;

                $serviseSelect = '<select name="service">';
                $serviseSelect .= sprintf('<optgroup label="%s">',GetMessage('SERVICE_KUR'));
                        $serviseSelect .= sprintf('<option %s value="1">%s</option>', $msk_mo, GetMessage('MSK_MO'));
                        $serviseSelect .= sprintf('<option %s value="11">%s</option>', $spb_lo, GetMessage('SPB_LO'));
                        $serviseSelect .= sprintf('<option %s value="10">%s</option>', $kur_sdek, GetMessage('KUR_SDEK'));
                    $serviseSelect .= '</optgroup>';
                    $serviseSelect .= sprintf('<optgroup label="%s">',GetMessage('SERVICE_PVZ'));
                        $serviseSelect .= sprintf('<option %s value="12">%s</option>', $pvz_ds, GetMessage('PVZ_DS_SPB'));
                        $serviseSelect .= sprintf('<option %s value="4">%s</option>', $pvz_ds, GetMessage('PVZ_DS'));
                        $serviseSelect .= sprintf('<option %s value="13">%s</option>', $pvz_boxberry, GetMessage('PVZ_BOXBERRY'));
                        $serviseSelect .= sprintf('<option %s value="9">%s</option>', $pvz_sdek, GetMessage('PVZ_SDEK'));
                    $serviseSelect .= '</optgroup>';
                $serviseSelect .= '</select>';

                $paytypeSelect = "<select name=\"paytype\"><option $cash value=\"CASH\">".GetMessage("CASH")."</option><option $card value=\"CARD\">".GetMessage("CARD")."</option><option $no value=\"NO\">".GetMessage("NO")."</option></select>";
                $d = strtotime("+1 day");
                $date = date("Y-m-d", $d);?>
                <script>
                items_count = <?=$items_count;?>;
                var content = '<div class="adm-detail-content-item-block adm-workarea" style="width:500px;"><form id="dalliform">'+
                    '<?=bitrix_sessid_post()?>'+
                    '<table class="adm-detail-content-table edit-table">'+
                    '<input type="hidden" name="action"  value="send2dalli">'+
                    '<tr><td><span id="hint_id"></span>&nbsp;<?=GetMessage("ORDER_ID")?></td><td><input type="text" name="id"  value="<?=$orderId?>"></td></tr>'+
                    '<tr><td><span id="hint_city"></span>&nbsp;<?=GetMessage("ORDER_CITY")?></td><td><input type="text" name="city"  value="<?=$city?>"></td></tr>'+
                    '<tr><td><span id="hint_zipcode"></span>&nbsp;<?=GetMessage("ORDER_ZIP")?></td><td><input type="text" name="zipcode" value="<?=$zip?>"></td></tr>'+
                    '<tr><td><span id="hint_address"></span>&nbsp;<?=GetMessage("ORDER_ADDRESS")?></td><td><textarea name="address" rows="2" cols="30" size="30"><?=str_replace(array("\r","\n"), ' ', $address)?></textarea></td></tr>'+
                    '<tr><td><span id="hint_fio"></span>&nbsp;<?=GetMessage("ORDER_FULL_NAME")?></td><td><input type="text" name="fio" value="<?=$fio?>"></td></tr>'+
                    '<tr><td><span id="hint_phone"></span>&nbsp;<?=GetMessage("ORDER_PHONE")?></td><td><input type="text" name="phone" value="<?=$phone?>"></td></tr>'+
                    '<tr><td><span id="hint_date"></span>&nbsp;<?=GetMessage("ORDER_DATE")?></td><td><input type="text" name="date" value="<?=$date?>"></td></tr>'+
                    '<tr><td><span id="hint_time_from_to"></span>&nbsp;<?=GetMessage("ORDER_TIME_FROM_TO")?></td><td><input size="4" type="text" name="time_min" value="<?=$time_min?>">'+
                    '<span> - </span><input size="4" type="text" name="time_max" value="<?=$time_max?>"></td></tr>'+
                    '<tr><td><span id="hint_service"></span>&nbsp;<?=GetMessage("ORDER_SERVICE")?></td><td><?=$serviseSelect?></td></tr>'+
                    '<tr><td><span id="hint_weight"></span>&nbsp;<?=GetMessage("ORDER_WEIGHT")?></td><td><input type="text" name="weight" value="<?=$weight?>"></td></tr>'+
                    '<tr><td><span id="hint_quantity"></span>&nbsp;<?=GetMessage("ORDER_PLACE_QUANTITY")?></td><td><input type="text" name="place_quantity" value="<?=$place_quantity?>"></td></tr>'+
                    '<tr><td><span id="hint_priced"></span>&nbsp;<?=GetMessage("ORDER_PRICE_DELIVERY")?></td><td><input type="text" name="priced" value="<?=$price_delivery?>"></td></tr>'+
                    '<tr><td><span id="hint_paytype"></span>&nbsp;<?=GetMessage("ORDER_PAYTYPE")?></td><td><?=$paytypeSelect?></td></tr>'+
                    '<tr><td><span id="hint_price"></span>&nbsp;<?=GetMessage("ORDER_PRICE")?></td><td><input type="text" name="price" value="<?=$price?>"></td></tr>'+
                    '<tr><td><span id="hint_inshprice"></span>&nbsp;<?=GetMessage("ORDER_INSHPRICE")?></td><td><input type="text" name="inshprice" value="<?=$price?>"></td></tr>'+
                    '<tr><td><span id="hint_instruction"></span>&nbsp;<?=GetMessage("ORDER_INSTRUCTION")?></td><td><textarea rows="4" cols="30" name="instruction"><?=str_replace(array("\r","\n"), ' ', $description)?></textarea></td></tr>'+
                    '</table>'+
                    '<span><?=GetMessage("PROD_POS")?>:</span><table class="adm-detail-content-table edit-table"><thead><tr><td><?=GetMessage("QUANTITY")?></td><td><?=GetMessage("ORDER_ARTICLE")?></td><td><?=GetMessage("ORDER_BARCODE")?></td><td><?=GetMessage("MASS")?></td><td><?=GetMessage("PRICE")?></td><td><?=GetMessage("NAME")?></td></tr></thead><tbody><?=$items_trs?></tbody></table>'+
                    '<button title="<?=GetMessage("ADD_PROD_POS")?>" class="adm-btn add-prod-pos">+</button><br><br><button class="adm-btn" onclick="send2Dalli(); return false;" type="submit"><?=GetMessage("ORDER_SEND")?></button>'+
                    '</form><div id="responce" style="text-align:center;display:none"><span id="dallianswer"></span></br></br><button class="adm-btn" onclick="hideBlock(); return false;"><?=GetMessage("ORDER_SEND_AGAIN")?></button></div>';


                    function showFormDS(){
                        popup = new BX.PopupWindow('send2Dalli', '', {
                            content: content,
                            closeIcon: {right: '7px', top: '7px'},
                            zIndex: 0,
                            autoHide: true,
                            offsetLeft: 0,
                            lightShadow : true,
                            closeByEsc : true,
                            offsetTop: 0,
                            draggable: true,
                            overlay: {backgroundColor: 'grey', opacity: '30' },
                            events: {
                                onPopupClose: function () {
                                    popup.destroy();
                                },
                                onAfterPopupShow: function () {
                                    BX.hint_replace(BX('hint_id'), '<?=GetMessage('HINT_ID')?>');
                                    BX.hint_replace(BX('hint_city'), '<?=GetMessage('HINT_CITY')?>');
                                    BX.hint_replace(BX('hint_zipcode'), '<?=GetMessage('HINT_ZIPCODE')?>');
                                    BX.hint_replace(BX('hint_address'), '<?=GetMessage('HINT_ADDRESS')?>');
                                    BX.hint_replace(BX('hint_fio'), '<?=GetMessage('HINT_FIO')?>');
                                    BX.hint_replace(BX('hint_phone'), '<?=GetMessage('HINT_PHONE')?>');
                                    BX.hint_replace(BX('hint_date'), '<?=GetMessage('HINT_DATE')?>');
                                    BX.hint_replace(BX('hint_time_from_to'), '<?=GetMessage('HINT_TIME_FROM_TO')?>');
                                    BX.hint_replace(BX('hint_service'), '<?=GetMessage('HINT_SERVICE')?>');
                                    BX.hint_replace(BX('hint_weight'), '<?=GetMessage('HINT_WEIGHT')?>');
                                    BX.hint_replace(BX('hint_quantity'), '<?=GetMessage('HINT_QUANTITY')?>');
                                    BX.hint_replace(BX('hint_paytype'), '<?=GetMessage('HINT_PAYTYPE')?>');
                                    BX.hint_replace(BX('hint_priced'), '<?=GetMessage('HINT_PRICED')?>');
                                    BX.hint_replace(BX('hint_price'), '<?=GetMessage('HINT_PRICE')?>');
                                    BX.hint_replace(BX('hint_inshprice'), '<?=GetMessage('HINT_INSHPRICE')?>');
                                    BX.hint_replace(BX('hint_instruction'), '<?=GetMessage('HINT_INSTRUCTION')?>');

                                    jQuery(".add-prod-pos").click(function() {
                                        items_count++;
                                        jQuery(this).prev("table").find("tbody").append("<tr><td><input size=\"2\" type=\"text\" name=\"item_quantity_"+items_count+"\"></td>" +
                                            "<td><input size=\"3\" type=\"text\" name=\"item_article_"+items_count+"\"></td>" +
                                            "<td><input size=\"3\" type=\"text\" name=\"item_barcode_"+items_count+"\"></td>" +
                                            "<td><input size=\"2\" type=\"text\" name=\"item_mass_"+items_count+"\"></td>" +
                                            "<td><input size=\"3\" type=\"text\" name=\"item_retprice_"+items_count+"\"></td>" +
                                            "<td><input size=\"23\" type=\"text\" name=\"item_name_"+items_count+"\"></td>" +
                                            "<td><a title=\"<?=GetMessage('REMOVE_PROD_POS')?>\" onclick=\"remove_prod_pos_line(this)\" class=\"prod_pos\">X</a></td></tr>");
                                        return false;
                                    });
                                }
                            }
                        });
                        popup.show();
                    }

                </script>
                <?$items[] = array(
                    "TEXT"=>"Dalli Service",
                    "LINK"=>"javascript:void(0)",
                    "TITLE"=> GetMessage('POPUP_TITLE'),
                    "ICON"=>"btn",
                    "ONCLICK" => 'showFormDS()'
                );?>
            <?}
        }
    }
    
    static public function sendOrder2DS()
    {
        if($_SERVER['REQUEST_METHOD'] == "POST" && $_POST['action'] == 'send2dalli' && defined('ADMIN_SECTION'))
        {
            if (!check_bitrix_sessid())
                return;
            global $APPLICATION;
            $APPLICATION->RestartBuffer();
            $arrItems = array();
            $items = '';
            unset ($_POST['action']);
            unset ($_POST['sessid']);
            $data = $_POST;
            foreach ($data as $key=>&$param)
            {
                $param = htmlspecialchars($param);
                if(strpos($key, 'item_quantity_') !== false){
                    $index = substr($key, 14);
                    $arrItems[$index]['quantity'] = $param;
                }
                if(strpos($key, 'item_article_') !== false){
                    $index = substr($key, 13);
                    $arrItems[$index]['article'] = $param;
                }
                if(strpos($key, 'item_barcode_') !== false){
                    $index = substr($key, 13);
                    $arrItems[$index]['barcode'] = $param;
                }
                if(strpos($key, 'item_mass_') !== false){
                    $index = substr($key, 10);
                    $arrItems[$index]['mass'] = $param;
                }
                if(strpos($key, 'item_retprice_') !== false){
                    $index = substr($key, 14);
                    $arrItems[$index]['retprice'] = $param;
                }
                if(strpos($key, 'item_name_') !== false){
                    $index = substr($key, 10);
                    $arrItems[$index]['name'] = $param;
                }
            }
            unset($param);
            if(count($arrItems)>0){
                foreach ($arrItems as $item){
                    $items .= "<item quantity=\"$item[quantity]\" mass=\"$item[mass]\" retprice=\"$item[retprice]\" barcode=\"$item[barcode]\" article=\"$item[article]\">" . htmlspecialchars_decode($item['name']) . "</item>";
                }
            }

            $items = "<items>".$items."</items>";

            $token = COption::GetOptionString(self::MODULE_ID, "DALLI_TOKEN");
            $xml_data = <<<EOD
                <?xml version="1.0" encoding="UTF-8"?>
                <neworder>
                    <auth token="$token" />
                    <order orderno="{$data['id']}">
                    <receiver>
                        <town>{$data['city']}</town>
                        <address>{$data['address']}</address>
                        <zipcode>{$data['zipcode']}</zipcode>
                        <person>{$data['fio']}</person>
                        <phone>{$data['phone']}</phone>
                        <date>{$data['date']}</date>
                        <time_min>{$data['time_min']}</time_min>
                        <time_max>{$data['time_max']}</time_max>
                    </receiver>
                    <service>{$data['service']}</service>
                    <weight>{$data['weight']}</weight>
                    <quantity>{$data['place_quantity']}</quantity>
                    <paytype>{$data['paytype']}</paytype>
                    <priced>{$data['priced']}</priced>
                    <price>{$data['price']}</price>
                    <inshprice>{$data['inshprice']}</inshprice>
                    <instruction>{$data['instruction']}</instruction>
                    $items
                    </order>
                </neworder>
EOD;

            $arResult = self::send_xml($xml_data, 0);
            echo json_encode($arResult['neworder']['#']['createorder']['0']['@']);
            die();
        }
    }
    
    static public function send_xml($xml_data, $convert = true)
    {
        if(function_exists('curl_exec'))
        {
            if(LANG_CHARSET == 'windows-1251' && $convert)
                $xml_data = iconv('windows-1251','utf-8', $xml_data);
            $url = COption::GetOptionString(self::MODULE_ID, "DALLI_FILIAL") == "MSK" ? "https://api.dalli-service.com/v1/" : "http://spbapi.dalli-service.com/v1/";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            if(LANG_CHARSET == 'windows-1251' && $convert)
                $result = iconv('utf-8','windows-1251',$result);
            require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/xml.php');
            $objXML = new CDataXML();
            $objXML->LoadString($result);
            $arResult = $objXML->GetArray();
            return $arResult;
        }
    }

    static public function send_json($partner, $city)
    {
        if(function_exists('curl_exec'))
        {
            $json_data["secret"] = "76107a8d3d555fd8f85ce74cfbc405c9";
            $json_data["partner"] = $partner;
            $json_data["town"] = $city;
            $json = json_encode($json_data);
            $ch = curl_init('https://api.dalli-service.com/v1/pointsInfo.php');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        }
    }
    
    static public function log($data)
    {
        $f = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/log.txt", "a+");
        if(gettype($data) == "array" || gettype($data) == "object")
        {
            fwrite($f, print_r(array($data),true)); 
        } else
        {
            fwrite($f, $data); 
        }       
        fclose($f);         
    }

    static public function getNumEnding($n, $forms)
    {
        return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
    }

    static public function deliveryPeriodUnify($deliveryPeriod, $partner)
    {
        if(strpos($deliveryPeriod, '-') !== false && $partner != 'DS')
        {
            $arrPeriod = explode('-', $deliveryPeriod);
            $arrPeriod = array_unique($arrPeriod);
            if(count($arrPeriod)>1)
            {
                foreach ($arrPeriod as &$period)
                {
                    $period = (int)$period+1;
                }
                unset ($period);
                $deliveryPeriod = implode('-', $arrPeriod);
            }
            else
            {
                $deliveryPeriod = (int)$arrPeriod['0']+1;
                unset($arrPeriod);
            }

        }
        elseif(strpos($deliveryPeriod, '-') !== false && $partner == 'DS')
        {
            $arrPeriod = explode('-', $deliveryPeriod);
            $arrPeriod = array_unique($arrPeriod);
            if(count($arrPeriod)==1)
            {
                $deliveryPeriod = $arrPeriod['0'];
                unset($arrPeriod);
            }
        }
        elseif(strpos($deliveryPeriod, '-') === false && $partner != 'DS')
        {
             $deliveryPeriod = (int)$deliveryPeriod+1;
        }

        $forms = array(GetMessage('DAY'), GetMessage('DAY_DEC'), GetMessage('DAYS'));
        if ($arrPeriod) $n = $arrPeriod['1'];
        else $n = $deliveryPeriod;
        $days = self::getNumEnding($n, $forms);
        return $deliveryPeriod.$days;
    }

    static public function checkToken($token){
        if(strlen ($token) > 0){
            $xml_data = <<<EOD
    <?xml version="1.0" encoding="UTF-8"?>
    <services>
      <auth token="$token"></auth>
    </services>
EOD;
            $arResult = DalliservicecomDelivery::send_xml($xml_data);
            if((int) $arResult['request']['@']['error'] != '401')
                return true;
        }
        return false;
    }
} 
?>  