<?php 
IncludeModuleLangFile(__FILE__);
class DalliservicecomDeliveryDB {
    const TABLE_ORDER = 'ds_orders_log';
    const TABLE_PVZ = 'ds_pvz_list';
    const MODULE_ID = 'dalliservicecom.delivery';

    /*static public function CheckOrder($orderno){
        $DB = CDatabase::GetModuleConnection(self::MODULE_ID);
        $strSql = "SELECT ID FROM ".self::TABLE_NAME." WHERE ORDERNO = ".$orderno;
        $res = $DB->Query($strSql, false, $err_mess.__LINE__);
        if($arr = $res->Fetch()) return $arr['ID'];
        return false;
    }*/

    static public function GetList($arFilter = array(), $aSort=array()) {
        $DB = CDatabase::GetModuleConnection(self::MODULE_ID);
        $arSqlSearch = array();
        $strSql = "
                SELECT 
                    *
                FROM
                    ds_orders_log
                WHERE
            ";

        if (is_array($arFilter))
        {
            foreach ($arFilter as $key => $val)
            {
                if (strlen($val)<=0 || $val=="NOT_REF")
                    continue;
                switch(strtoupper($key))
                {
                    case "ORDERNO":
                    case "ORDERCODE":
                    case "COMPANY":
                    case "PERSON":
                    case "PHONE":
                    case "ZIPCODE":
                    case "TOWN":
                    case "TOWNCODE":
                    case "ADDRESS":
                    case "DATE":
                    case "TIME_FROM_TO":
                    case "TIME_MAX":
                    case "WEIGHT":
                    case "QUANTITY":
                    case "PAYTYPE":
                    case "SERVICE":
                    case "PRICE":
                    case "INSHPRICE":
                    case "ENCLOSURE":
                    case "INSTRUCTION":
                    case "DELIVERYPRICE":
                    case "EVENTTIME":
                    case "CREATETIMEGMT":
                    case "STATUS":
                    case "DELIVEREDTO":
                    case "DELIVEREDDATE":
                    case "DELIVEREDTIME":
                    $val = $DB->ForSql($val);
                    $arSqlSearch[] = GetFilterQuery($key, $val,"Y");
                   // GetFilterQuery

                        break;
                        /*$arSqlSearch[] = GetFilterQuery($key, $val,"N");
                        break;*/
                    /*case "COMMENT":
                        //$arSqlSearch[] = GetFilterQuery($key, $val,"N");
                        $arSqlSearch[] = "(COMMENT='".$DB->ForSql($val)."')";
                        break;
                    case "DATE_TIME_CREATE_FROM":
                        $arSqlSearch[] = "DATE_TIME_CREATE>=".$DB->CharToDateFunction($val);
                        break;
                    case "DATE_TIME_CREATE_TO":
                        $arSqlSearch[] = "DATE_TIME_CREATE<".$DB->CharToDateFunction($val);
                        break;*/
                    default:
                        $arSqlSearch[] = $key." = '".$DB->ForSql($val)."'";
                        break;
                }
            }
        }

        $strSqlSearch = GetFilterSqlSearch($arSqlSearch);

        $arOrder = array();

        foreach($aSort as $key=>$val)
        {
            $ord = (strtoupper($val) <> "ASC"? "DESC": "ASC");
            $key = strtoupper($key);
            if($key == 'ID')
                $key = 'ORDERNO';
            $arOrder[] = $key." ".$ord;
        }
        if(count($arOrder) == 0)
            $arOrder[] = "ORDERNO DESC";
        $strSqlOrder = "\nORDER BY ".implode(", ",$arOrder);
       // var_dump($strSql.' '.$strSqlSearch.' '.$strSqlOrder);//exit;
        $dbResult = $DB->Query($strSql.' '.$strSqlSearch.' '.$strSqlOrder, false, $err_mess.__LINE__);

        return $dbResult;
    }

    static public function GetPvzList($arFilter = array()) {
        $DB = CDatabase::GetModuleConnection(self::MODULE_ID);
        $arSqlSearch = array();
        $strSql = "
                SELECT 
                    *
                FROM
                    ds_pvz_list
                WHERE
            ";

        if (is_array($arFilter))
        {
            foreach ($arFilter as $key => $val)
            {
                //if (strlen($val)<=0)
                  //  continue;
                $arSqlSearch[] = $key." = '".$DB->ForSql($val)."'";
            }
        }

        $strSqlSearch = GetFilterSqlSearch($arSqlSearch);

//        var_dump($strSql.' '.$strSqlSearch.' '.$strSqlOrder);//exit;
        $dbResult = $DB->Query($strSql.' '.$strSqlSearch, false, $err_mess.__LINE__);

        return $dbResult;
    }

    static public function Add($arFields, $table_name)
    {
        $DB = CDatabase::GetModuleConnection(self::MODULE_ID);
        foreach ($arFields as &$field){
            if($field){
                $field = "'".$DB->ForSql($field)."'";
            }
        }
        unset ($field);
        $DB->PrepareFields($table_name);
       // $ID = (int) self::CheckOrder($arFields['ORDERNO']);
        $DB->StartTransaction();
       // if ($ID) {
         //   $DB->Update(self::TABLE_NAME, $arFields, "WHERE ID='".$ID."'", $err_mess.__LINE__);
       // } else {
            $DB->Insert($table_name, $arFields, $err_mess.__LINE__);
       // }
        if (strlen($err_mess)<=0){
            $DB->Commit();
            return true;
        }
        else {
            $DB->Rollback();
            return false;
        }
    }

static public function clearTable($table_name){
    $DB = CDatabase::GetModuleConnection(self::MODULE_ID);
    $strSql = "TRUNCATE TABLE $table_name";
    $res = $DB->Query($strSql, false, $err_mess.__LINE__);
    return $res;
}

    static public function orderSynchAgent(){
        $error = 0;
        self::clearTable(self::TABLE_ORDER);
        $token = COption::GetOptionString(self::MODULE_ID, "DALLI_TOKEN");
        $date = new DateTime();
        $date->modify('-1 month');
        $date_from = $date->format('Y-m-d');
        $date_to = date('Y-m-d');
        $xml_data = <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<statusreq module=\"bitrix\">
    <auth token="$token"></auth>
    <orderno></orderno>
    <datefrom>$date_from</datefrom>
    <dateto>$date_to</dateto>
    <done></done>
    <quickstatus>NO</quickstatus>
</statusreq>
EOD;
        $arResult = DalliservicecomDelivery::send_xml($xml_data);
        if($arResult['request']['@']['error'] > 0 || !$arResult || $arResult == '')
            $error++;

        if((int)($arResult['statusreq']['@']['count']) > 0 ){
            
            foreach ($arResult['statusreq']['#']['order'] as $order) {
                $arFields = array();
                $arFields['ORDERNO'] = (string) $order['@']['orderno'];
                $arFields['ORDERCODE'] = (string) $order['@']['ordercode'];
                $arFields['COMPANY'] = (string) $order['#']['receiver'][0]['#']['company'][0]['#'];
                $arFields['PERSON'] = (string) $order['#']['receiver'][0]['#']['person'][0]['#'];
                $arFields['PHONE'] = (string) $order['#']['receiver'][0]['#']['phone'][0]['#'];
                $arFields['ZIPCODE'] = (string) $order['#']['receiver'][0]['#']['zipcode'][0]['#'];
                $arFields['TOWN'] = (string) $order['#']['receiver'][0]['#']['town'][0]['#'];
                $arFields['TOWNCODE'] = (string) $order['#']['receiver'][0]['#']['town'][0]['@']['code'];
                $arFields['ADDRESS'] = (string) $order['#']['receiver'][0]['#']['address'][0]['#'];
                $arFields['DATE'] = (string) $order['#']['receiver'][0]['#']['date'][0]['#'];
                $arFields['TIME_FROM_TO'] = (string) $order['#']['receiver'][0]['#']['time_min'][0]['#']."-".(string) $order['#']['receiver'][0]['#']['time_max'][0]['#'];
                $arFields['WEIGHT'] = (string) $order['#']['weight'][0]['#'];
                $arFields['QUANTITY'] = (string) $order['#']['quantity'][0]['#'];
                $arFields['PAYTYPE'] = (string) $order['#']['paytype'][0]['#'];
                $arFields['SERVICE'] = (string) $order['#']['service'][0]['#'];
                $arFields['PRICE'] = (string) $order['#']['price'][0]['#'];
                $arFields['INSHPRICE'] = (string) $order['#']['inshprice'][0]['#'];
                $arFields['ENCLOSURE'] = (string) $order['#']['enclosure'][0]['#'];
                $arFields['INSTRUCTION'] = (string) $order['#']['instruction'][0]['#'];
                $arFields['DELIVERYPRICE'] = (string) $order['#']['deliveryprice'][0]['@']['total'];
                $arFields['EVENTTIME'] = (string) $order['#']['status'][0]['@']['eventtime'];
                $arFields['CREATETIMEGMT'] = (string) $order['#']['status'][0]['@']['createtimegmt'];
                $arFields['STATUS'] = (string) $order['#']['status'][0]['#'];
                $arFields['DELIVEREDTO'] = (string) $order['#']['deliveredto'][0]['#'];
                $arFields['DELIVEREDDATE'] = (string) $order['#']['delivereddate'][0]['#'];
                $arFields['DELIVEREDTIME'] = (string) $order['#']['deliveredtime'][0]['#'];
                $add = self::add($arFields, self::TABLE_ORDER);
                if(!$add)
                    $error++;
            }
        }
        /*if($error > 0)
            return false;*/
        return "DalliservicecomDeliveryDB::orderSynchAgent();";
    }

    static public function pvzSynchAgent(){
        self::clearTable(self::TABLE_PVZ);
        $token = COption::GetOptionString(self::MODULE_ID, "DALLI_TOKEN");

        foreach (array("DS","SDEK","BOXBERRY") as $partner):
            $xml_data = '<?xml version="1.0" encoding="UTF-8"?>';
            $xml_data .= '<pointsInfo module="bitrix">';
            $xml_data .= sprintf('    <auth token="%s"></auth>',$token);
            $xml_data .= sprintf('    <partner>%s</partner>',$partner);
            $xml_data .= '</pointsInfo>';

            $arResult = DalliservicecomDelivery::send_xml($xml_data);
            if($arResult['request']['@']['error'] > 0 || !$arResult || $arResult == '')
                    $error++;

            if(count($arResult['pointsInfo']['#']['point']) > 0){
                foreach ($arResult['pointsInfo']['#']['point'] as $point) {
                    $arFields = array();
                    $arFields['CODE'] = $point['@']['code'];
                    $arFields['NAME'] = preg_replace("/[^,.-;\w\d\s]/ui", "", $point['#']['name']['0']['#']);
                    $arFields['TOWN'] = $point['#']['town']['0']['#'];
                    $arFields['ADDRESS'] = preg_replace("/[^,.-;\w\d\s]/ui", "", $point['#']['address']['0']['#']);
                    $arFields['ADDRESSREDUCE'] = preg_replace("/[^,.-;\w\d\s]/ui", "", $point['#']['addressReduce']['0']['#']);
                    $arFields['DESCRIPTION'] = preg_replace("/[^,.-;\w\d\s]/ui", "", $point['#']['description']['0']['#']);
                    $arFields['ONLYPREPAID'] = $point['#']['onlyPrepaid']['0']['#'];
                    $arFields['WORKSHEDULE'] = preg_replace("/[^,.-;\w\d\s]/ui", "", $point['#']['workShedule']['0']['#']);
                    $arFields['GPS'] = $point['#']['GPS']['0']['#'];
                    $arFields['PHONE'] = $point['#']['phone']['0']['#'];
                    $arFields['PARTNER'] = $point['#']['partner']['0']['#'];
                    $arFields['WEIGHTLIMIT'] = $point['#']['weightLimit']['0']['#'] ? $point['#']['weightLimit']['0']['#'] : 0;
                    $add = self::add($arFields, self::TABLE_PVZ);
                    if(!$add)
                        $error++;
                }
            }
        endforeach;

        /*if($error > 0)
            return false;*/
        return "DalliservicecomDeliveryDB::pvzSynchAgent();";
    }
}
?>  