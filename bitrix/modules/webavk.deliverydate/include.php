<?
IncludeModuleLangFile(__FILE__);

Class CWebavkDeliverydate {

	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu) {
		$MODULE_ID = basename(dirname(__FILE__));
		$aMenu = array(
			//"parent_menu" => "global_menu_services",
			"parent_menu" => "global_menu_store",
			"section" => $MODULE_ID,
			"sort" => 1000,
			"text" => GetMessage($MODULE_ID . "_MENU_INDEX_TITLE"),
			"title" => GetMessage($MODULE_ID . "_MENU_INDEX_TITLE"),
			"url" => $MODULE_ID . '.index.php',
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID . "_items",
			"more_url" => array(),
			"items" => array()
		);
		if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '.index.php'))
			file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/index.php");?' . '>');
		if (file_exists($path = dirname(__FILE__) . '/admin')) {
			if ($dir = opendir($path)) {
				$arFiles = array();

				while (false !== $item = readdir($dir)) {
					if (in_array($item, array('.', '..', 'menu.php', 'user_date_bsm.php')))
						continue;
					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '.' . $item))
						file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/' . $item . '");?' . '>');
					if (strpos($item, ".edit.php") !== false)
						continue;

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach ($arFiles as $item) {
					$aMenu['items'][] = array(
						'text' => GetMessage($MODULE_ID . "_MENU_" . $item . "_TITLE"),
						'url' => $MODULE_ID . '.' . $item,
						"more_url" => Array(str_replace(".php", ".edit.php", $MODULE_ID . '.' . $item)),
						'module_id' => $MODULE_ID,
						"title" => ""
					);
				}
			}
		}
		$aModuleMenu[] = $aMenu;
	}

	function DoAdminTabControlBegin(&$oAdminTabControl) {
		global $str_PERSON_TYPE_ID;
		if ($oAdminTabControl->name == 'form_order_buyers') {
			CJSCore::Init(array("jquery"));
			foreach ($oAdminTabControl->tabs as $index => $oTab) {
				$ak = array_keys($oAdminTabControl->tabs[$index]['FIELDS']);
				if (isset($oAdminTabControl->tabs[$index]['FIELDS']['NEWO_BUYER'])) {
					$dbProp = CSaleOrderProps::GetList(array(), array("PERSON_TYPE_ID", "ACTIVE" => "Y", "CODE" => "F_DELIVERY_DATE"));
					$arCalendar = array();
					while ($arProp = $dbProp->Fetch()) {
						ob_start();
						echo CalendarDate("ORDER_PROP_" . $arProp['ID'], "", "form_order_buyers_form");
						$arCalendar[$arProp['ID']] = ob_get_contents();
						ob_end_clean();
					}
					?>
					<script type="text/javascript">
						<!--
						var calendarsReplace =<?= CUtil::PhpToJSObject($arCalendar) ?>;
						var flagWADDCheckCalendarInterval = false;
						function doWebavkReplaceCalendar()
						{
							if ($(".bx-core-waitwindow").length > 0)
								return false;
							if (flagWADDCheckCalendarInterval)
							{
								setTimeout('doWebavkReplaceCalendar();', 100);
							}
							clearInterval(flagWADDCheckCalendarInterval);
							flagWADDCheckCalendarInterval = false;
							$.each(calendarsReplace, function(k, v) {
								if ($("form[name='form_order_buyers_form'] input[name='ORDER_PROP_" + k + "']").length > 0)
								{
									val = $("form[name='form_order_buyers_form'] input[name='ORDER_PROP_" + k + "']").val();
									$("form[name='form_order_buyers_form'] input[name='ORDER_PROP_" + k + "']").replaceWith(v);
									$("form[name='form_order_buyers_form'] input[name='ORDER_PROP_" + k + "']").val(val);
								}
							});
						}
						$(document).ready(function() {
							doWebavkReplaceCalendar();
							$("form[name='form_order_buyers_form']").delegate("#buyer_type_id", "change", function() {
								flagWADDCheckCalendarInterval = setInterval('doWebavkReplaceCalendar();', 200);
								//doWebavkReplaceCalendar();
							});
						});
						//-->
					</script>
					<?
				}
			}
		}
	}

	function agentStopList() {
		$rDays = WebAVK\DeliveryDate\Days\DaysTable::getList();
		while ($arDays = $rDays->Fetch()) {
			if ($arDays['TIME_STOP'] != $arDays['TIME_STOP_DEFAULT']) {
				$ob = new WebAVK\DeliveryDate\Days\DaysTable();
				$ob->update($arDays['ID'], array(
					"TIME_STOP" => $arDays['TIME_STOP_DEFAULT']
				));
			}
		}
		return 'CWebavkDeliverydate::agentStopList();';
	}

	/*
	  function DoAdminProlog() {
	  global $APPLICATION;
	  if ($APPLICATION->GetCurPage() == "/bitrix/admin/sale_order.php") {
	  CJSCore::Init(array("jquery"));
	  ob_start();
	  echo CalendarDate("filter_prop_F_DELIVERY_DATE", "", "find_form");
	  $calendar = ob_get_contents();
	  ob_end_clean();
	  ob_start();
	  ?>
	  <script type="text/javascript">
	  <!--
	  var calendarCode =<?= CUtil::PhpToJSObject($calendar) ?>;
	  function doWebavkReplaceCalendarFilter()
	  {
	  if ($(".bx-core-waitwindow").length > 0)
	  return false;
	  $("input[name='filter_prop_F_DELIVERY_DATE']").each(function() {
	  $(this).after(calendarCode);
	  $(this).parent().find(".adm-input-wrap-calendar").css("margin-left", "0");
	  $(this).parent().find(".adm-input-wrap-calendar input").val($(this).val());
	  $(this).remove();
	  });
	  }
	  $(document).ready(function() {
	  doWebavkReplaceCalendarFilter();
	  /*$("form[name='form_order_buyers_form']").delegate("#buyer_type_id", "change", function() {
	  flagWADDCheckCalendarInterval = setInterval('doWebavkReplaceCalendar();', 200);
	  //doWebavkReplaceCalendar();
	  }); *//*
	  });
	  //-->
	  </script>
	  <?
	  $cont = ob_get_contents();
	  ob_end_clean();
	  $APPLICATION->AddHeadString($cont);
	  }
	  }
	 */
}

CModule::AddAutoloadClasses(
		"webavk.deliverydate", array(
	"WebAVK\\DeliveryDate\\Days\\DaysTable" => "lib/days.php",
	"WebAVK\\DeliveryDate\\Days\\DaysDeliveryTable" => "lib/days.delivery.php",
	"WebAVK\\DeliveryDate\\Days\\DaysStoreTable" => "lib/days.store.php",
	"WebAVK\\DeliveryDate\\DataManager" => "lib/dm.php",
	"CWebavkDeliveryDateTools" => "classes/general/tools.php",
		)
);
?>