<?

$MESS['webavk.deliverydate_OPTION_minutestep'] = "Шаг в минутах при выводе списков выбора времени";
$MESS['webavk.deliverydate_OPTION_totalmaxorders'] = "Максимальное общее количество заказов в день";
$MESS['webavk.deliverydate_OPTION_DAYS_DELIVERY_HEADING'] = "Через сколько дней возможна доставка (1 - на следующий день, 2 - не раньше чем послезавтра и т.д.)";
$MESS['webavk.deliverydate_OPTION_DAYS_DELIVERY_HEADER'] = "Служба доставки";
$MESS['webavk.deliverydate_OPTION_DAYS_HEADER'] = "Кол-во дней до доставки";
$MESS['webavk.deliverydate_OPTION_DAYS_MAX_DELIVERY_HEADER'] = "Максимум доставок в день";
$MESS['webavk.deliverydate_OPTION_orderstatus'] = "Статусы заказов, при которых заказ считается доставляемым на определенный день";
$MESS['webavk.deliverydate_OPTION_CNT_orderstatus'] = "Статусы заказов, которые учитывать в доставляемых для таблицы информации";
$MESS['webavk.deliverydate_OPTION_DAYS_STORE_HEADING'] = "На сколько дней корректируется ближайшая доставка в зависимости от склада, и приоритет складов при расчете даты доставки";
$MESS['webavk.deliverydate_OPTION_DAYS_STORE_HEADER'] = "Склад";
$MESS['webavk.deliverydate_OPTION_DAYS_STORE_DAYS_HEADER'] = "Кол-во дней коррекции";
$MESS['webavk.deliverydate_OPTION_maxdeliveryorders'] = "Максимальное количество доставок в день";
$MESS['webavk.deliverydate_OPTION_DAYS_STORE_PRIORITY_HEADER'] = "Приоритет";
