<?
$MESS["webavk.deliverydate_MODULE_NAME"] = "WebAVK. Ближайшая дата доставки";
$MESS["webavk.deliverydate_MODULE_DESC"] = "Модуль предназначен для ограничения ближайшей даты доставки для служб доставки в зависимости от количества уже сделанных заказов и статусов. Поддерживает нерабочие дни, ограничение по дням недели и статусы заказов";
$MESS["webavk.deliverydate_PARTNER_NAME"] = "WebAVK - решения для 1С-Битрикс";
$MESS["webavk.deliverydate_PARTNER_URI"] = "http://www.web-avk.ru";
?>