<?

$MESS['webavk.deliverydate_MODULE_SALE_REQUIRED'] = "Для работы модуля WebAVK.DeliveryDate требуется установленный модуль Sale (Интернет-магазин)";
$MESS['webavk.deliverydate_MODULE_CATALOG_REQUIRED']="Для работы модуля WebAVK.DeliveryDate требуется установленный модуль Catalog (Торговый каталог)";
$MESS['webavk.deliverydate_PAGE_TITLE'] = "Правила дат доставки и ограничения количества заказов";
$MESS['webavk.deliverydate_NAVPRINT'] = "Правила";
$MESS['webavk.deliverydate_ROW_TITLE'] = "Перейти к редактированию правила";
$MESS['webavk.deliverydate_FIELD_HEADER_NAME'] = "Название";
$MESS['webavk.deliverydate_FIELD_HEADER_SORT'] = "Сортировка";
$MESS['webavk.deliverydate_FIELD_HEADER_ACTIVE'] = "Активность";
$MESS['webavk.deliverydate_FIELD_HEADER_DELIVERY'] = "Службы доставки";
$MESS['webavk.deliverydate_FIELD_HEADER_STORE'] = "Склады";
$MESS['webavk.deliverydate_FIELD_HEADER_DELIVERY_DATE'] = "Дата";
$MESS['webavk.deliverydate_FIELD_HEADER_WEEK_DAY'] = "День недели";
$MESS['webavk.deliverydate_FIELD_HEADER_MONTH_DAY'] = "День месяца";
$MESS['webavk.deliverydate_FIELD_HEADER_PERIOD_START'] = "Начало периода";
$MESS['webavk.deliverydate_FIELD_HEADER_PERIOD_END'] = "Конец периода";
$MESS['webavk.deliverydate_FIELD_HEADER_MAX_ORDERS'] = "Максимально количество заказов";
$MESS['webavk.deliverydate_FIELD_HEADER_TIME_START'] = "Время начала доставки";
$MESS['webavk.deliverydate_FIELD_HEADER_TIME_END'] = "Время окончания доставки";
$MESS['webavk.deliverydate_FIELD_HEADER_TIME_STOP'] = "Время стоп-листа";
$MESS['webavk.deliverydate_FIELD_HEADER_TIME_STOP_DEFAULT'] = "Время стоп-листа по умолчанию";
$MESS['webavk.deliverydate_FIELD_HEADER_TIME_INTERVALS'] = "Периоды времени доставки";
$MESS['webavk.deliverydate_WEEKDAY_1'] = "Понедельник";
$MESS['webavk.deliverydate_WEEKDAY_2'] = "Вторник";
$MESS['webavk.deliverydate_WEEKDAY_3'] = "Среда";
$MESS['webavk.deliverydate_WEEKDAY_4'] = "Четверг";
$MESS['webavk.deliverydate_WEEKDAY_5'] = "Пятница";
$MESS['webavk.deliverydate_WEEKDAY_6'] = "Суббота";
$MESS['webavk.deliverydate_WEEKDAY_7'] = "Воскресенье";
$MESS['webavk.deliverydate_MONTHDAY_FIRST'] = "первый";
$MESS['webavk.deliverydate_MONTHDAY_LAST'] = "последний";
$MESS['webavk.deliverydate_FILTER_FIELD_ID'] = "ID";
$MESS['webavk.deliverydate_FILTER_FIELD_ACTIVE'] = "Активность";
$MESS['webavk.deliverydate_FILTER_FIELD_DELIVERY_ID'] = "Служба доставки";
$MESS['webavk.deliverydate_FILTER_FIELD_WEEK_DAY'] = "День недели";
$MESS['webavk.deliverydate_POST_SAVE_ERROR'] = "Ошибка сохранения записи ";
$MESS['webavk.deliverydate_ERROR_DELETE'] = "Ошибка удаления записи";
$MESS['webavk.deliverydate_ERROR_ACTIVATE'] = "Ошибка активации/деактивации записи";
$MESS['webavk.deliverydate_CONTEXT_ADD'] = "Добавить";
$MESS['webavk.deliverydate_CONTEXT_ADD_DESCRIPTION'] = "Добавить новое правило для доставки";
$MESS['webavk.deliverydate_DELETE'] = "Удалить";
$MESS['webavk.deliverydate_DELETE_CONFIRM'] = "Вы действительно хотите удалить правило? Правило будет удалено без возможности восстановления";
$MESS['webavk.deliverydate_NOTE'] = '<h3>Внимание!!!</h3>
<p>Не забудьте указать в <a href="/bitrix/admin/settings.php?mid=webavk.deliverydate">форме настроек</a> ближайшие даты доставок, в зависимости от службы доставки, и статусы заказов, при которых заказ считается доставляемым на определенный день.</p>
';
?>