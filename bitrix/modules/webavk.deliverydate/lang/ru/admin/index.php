<?

$MESS['webavk.deliverydate_PAGE_TITLE'] = "Модуль WebAVK - Ближайшая дата доставки";
$MESS['webavk.deliverydate_TAB_INSTRUCTION_TITLE'] = "Инструкция";
$MESS['webavk.deliverydate_TAB_INSTRUCTION_DESC'] = "Инструкция по использованию модуля";
$MESS['webavk.deliverydate_TAB_INSTRUCTION_CONTENT'] = '
<p>Модуль WebAVK: Ближайшая дата доставки позволяет информировать пользователя о ближайшей возможной дате доставки товара магазином, выбирать пользователем дату доставки при оформлении заказа, а также ограничить количество заказов, принимаемых к доставке по определенным дням.</p>
<p>Возможности модуля:</p>
<ul>
	<li>Индивидуальные правила для различных служб доставки (например для курьеров и самовывоза)</li>
	<li>Индивидуальные правила для различных складов (например для склада магазина и удаленного склада)</li>
	<li>Конкретная дата доставки</li>
	<li>День недели</li>
	<li>День месяца (конкретный номер, либо последний день)</li>
	<li>Период действия правила (например - в праздничные и выходные дни доставка не осуществляется)</li>
	<li>Индивидуальные параметры доставки (&laquo;не ранее, чем через XX дней&raquo;, либо &laquo;не ранее XX.XX.XXXX&raquo;) для отдельных товаров</li>
	<li>Указание пользователем желаемой даты доставки при оформлении заказа, на основании правил и индивидуальных параметров товаров, заказ на которые оформляет пользователь</li>
	<li>Компонент для информирования пользователей о дате ближайшей доставки (как в целом по сайту, так и для отдельных товаров)</li>
	<li>Подсчет количества заказов, выбранных для доставки на определенную дату, осуществляется в соответствии с выбранными статусами заказов.</li>
	<li>Прекращение приема заказа на следующий (либо указанный в настройках) день при достижении времени стоп-листа.</li>
	<li>Временное изменение времени стоп-листа приема заказов и автоматическое его восстановление до времени стоп-листа по умолчании при завершении текущих суток.</li>
</ul>
<h3>Настройка модуля</h3>
<ol>
	<li><img src="/bitrix/images/webavk.deliverydate/instruction1.jpg" style="float:right" /><a href="/bitrix/admin/settings.php?lang=ru&mid=webavk.deliverydate">Выполните настройку модуля</a>.<br>
	Для этого:
		<ul>
			<li>выберите все статусы заказов, в которых заказы будут считаться, как доставляемые на определенную дату</li>
			<li>укажите для необходимых служб доставки количество дней, через которое возможна доставка</li>
			<li>укажите на сколько дней корректируется доставка в зависимости от склада</li>
			<li>укажите приоритет списания товаров со складов (с которых списывается в первую очередь - более низкое значение, с удаленных складов - большее)</li>
		</ul>
		<div style="clear:both;"></div>
	</li>
	<li style="margin-top:10px;padding-top:10px;border-top: 1px dotted #cccccc;"><img src="/bitrix/images/webavk.deliverydate/instruction2.jpg" style="float:right" /><a href="/bitrix/admin/sale_order_props.php?lang=ru">Создайте новые свойства заказов</a> для всех типов плательщиков.<br>
	Главные требования для вновь создаваемых свойств:
		<ul>
			<li>название - Дата доставки, мнемонический код - F_DELIVERY_DATE, тип - [TEXT] Строка</li>
			<li>название - Время доставки, мнемонический код - F_DELIVERY_TIME, тип - [TEXT] Строка</li>
		</ul>
		<div style="clear:both;"></div>
	</li>
	<li style="margin-top:10px;padding-top:10px;border-top: 1px dotted #cccccc;">
		<a href="/bitrix/admin/webavk.deliverydate.days.php">Создайте</a> одно или несколько правил для доставки (например 7 правил по дням недели для одной из служб доставки)
		<div style="clear:both;"></div>
	</li>
	<li style="margin-top:10px;padding-top:10px;border-top: 1px dotted #cccccc;"><img src="/bitrix/images/webavk.deliverydate/instruction3.jpg" style="float:right" />Если вы хотите использовать возможность задания индивидуальных параметров ближайшей доставки у отдельных товаров, то необходимо в соответствующих инфоблоках создать 2 свойства:
		<ul>
			<li>
				<strong>Название</strong>: &laquo;Доставка: через сколько дней&raquo;<br/>
				<strong>Тип</strong>: &laquo;Число&raquo;<br/>
				<strong>Символьный код</strong>: &laquo;DELIVERY_DAYS&raquo;<br/>
			</li>
			<li>
				<strong>Название</strong>: &laquo;Доставка: ближайшая дата доставки&raquo;<br/>
				<strong>Тип</strong>: &laquo;Дата/время&raquo;<br/>
				<strong>Символьный код</strong>: &laquo;DELIVERY_DATE&raquo;<br/>
			</li>
		</ul>
		После этого разместите в коде вашего шаблона компонент webavk.deliverydate и настройте его
		<div style="clear:both;"></div>
	</li>
	<li style="margin-top:10px;padding-top:10px;border-top: 1px dotted #cccccc;">
		Для подключения возможности выбора пользователем даты доставки при оформлении заказа можно использовать кастомизированный шаблон <strong>webavk_deliverydate</strong> стандартного компонента bitrix:sale.order.ajax, расположенный в папке /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate/, либо интегрировать данную возможность в свой шаблон. Пример интеграции показан непосредственно в коде вышеуказанного шаблона. Изменения шаблона следующие:
		<ul>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate/props.php<br/>
				<strong>Строки</strong>: 6-9<br/>
				<strong>Описание</strong>: Отключение вывода в свойства F_DELIVERY_DATE в блоке заполнения информации о плательщике и адресе доставки
			</li>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate/delivery_date.php<br/>
				<strong>Строки</strong>: новый файл<br/>
				<strong>Описание</strong>: Вывод свойства с доступной датой доставки
			</li>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate/template.php<br/>
				<strong>Строки</strong>: 135<br/>
				<strong>Описание</strong>: Подключение файла /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate/delivery_date.php
			</li>
		</ul>
		<div style="clear:both;"></div>
		<br/>
		Также доступен шаблон компонента <strong>webavk_deliverydate_visual</strong>, расположенный в папке /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/ , являющийся кастомизированная копия стандартного шаблона <strong>visual</strong>. Пример интеграции показан непосредственно в коде вышеуказанного шаблона. Изменения шаблона следующие:
		<ul>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/props.php<br/>
				<strong>Строки</strong>: 8-11<br/>
				<strong>Описание</strong>: Отключение вывода в свойства F_DELIVERY_DATE в блоке заполнения информации о плательщике и адресе доставки
			</li>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/delivery_date.php<br/>
				<strong>Строки</strong>: новый файл<br/>
				<strong>Описание</strong>: Вывод свойства с доступной датой доставки
			</li>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/template.php<br/>
				<strong>Строки</strong>: 103 и 108<br/>
				<strong>Описание</strong>: Подключение файла /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/delivery_date.php
			</li>
		</ul>
		<div style="clear:both;"></div>
		<br/>
		Также доступен шаблон компонента <strong>webavk_deliverydate_calendar_visual</strong>, расположенный в папке /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_calendar_visual/ , являющийся кастомизированная копия стандартного шаблона <strong>visual</strong> с выводом доступных дат доставки в виде календаря. Пример интеграции показан непосредственно в коде вышеуказанного шаблона. Изменения шаблона следующие:
		<ul>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_calendar_visual/props.php<br/>
				<strong>Строки</strong>: 8-11<br/>
				<strong>Описание</strong>: Отключение вывода в свойства F_DELIVERY_DATE в блоке заполнения информации о плательщике и адресе доставки
			</li>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/delivery_date.php<br/>
				<strong>Строки</strong>: новый файл<br/>
				<strong>Описание</strong>: Вывод свойства с доступной датой доставки
			</li>
			<li>
				<strong>Файл</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/template.php<br/>
				<strong>Строки</strong>: 4-7, 65, 79-118, 151 и 156<br/>
				<strong>Описание</strong>: Подключение jQuery, календаря jQueryUI, подключение файла /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/delivery_date.php
			</li>
			<li>
				<strong>Каталог</strong>: /bitrix/templates/.default/components/bitrix/sale.order.ajax/webavk_deliverydate_visual/jquery-custom/<br/>
				<strong>Описание</strong>: файлы jQuery, jQueryUI, файлы стилей для jQueryUI
			</li>
		</ul>
		<div style="clear:both;"></div>
	</li>
</ol>
';
$MESS['webavk.deliverydate_TAB_SUPPORT_TITLE'] = "Техподдержка, развитие модуля, интеграция на сайт";
$MESS['webavk.deliverydate_TAB_SUPPORT_DESC'] = "Техническая поддержка, пожелания по функционалу и интеграция на ваш сайт модуля webavk.ibcomments";
$MESS['webavk.deliverydate_TAB_SUPPORT_CONTENT'] = '
	<h3>Техническая поддержка</h3>
	<p>Техническая поддержка модуля осуществляется по электронной почте</p>
	<h3>Развитие модуля, новый функционал</h3>
	<p>Если вы обнаружили что какого-то функционала модуля не хватает лично для вас - напишите нам. Функционал будет добавлен.</p>
	<h3>Интеграция на ваш сайт</h3>
	<p>Если у вас возникли трудности по интеграции модуля на сайт, либо есть необходимость сделать индивидуальный дизайн компонента отзывов для вашего сайта - обращайтесь. Стоимость расчитывается индивидуально в зависимости от требований</p>
	<hr/>
	<h3>Наши контакты:</h3>
	<p>Электронная почта: <a href="mailto:support@web-avk.ru">support@web-avk.ru</a></p>
	<p>Skype: <a href="skype:web.avk?chat"><img src="http://mystatus.skype.com/smallicon/web.avk?' . time() . '" style="border: none;"> Skype</a></p>
	<div style="clear:both;"></div>
';
?>