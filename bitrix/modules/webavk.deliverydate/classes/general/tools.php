<?

class CWebavkDeliveryDateTools
{

	public static $CACHE_DELIVERY_SETTINGS = array();
	public static $CACHE_ORDER_CNT = array();
	public static $CACHE_STORE_PRIORITY = false;

	function getEarliestDate($deliveryID, $minDays = 1, $minDate = false, $arAllSkladUse = false, $arAllElements = false)
	{
		$arElementsQuantity = array();
		if ($arAllElements !== false && $arAllElements !== 0) {
			if (!is_array($arAllElements))
				$arAllElements = array($arAllElements);
			foreach ($arAllElements as $k => $v) {
				$arElementsQuantity[$v] = 1;
			}
		}
		$arResult = CWebavkDeliveryDateTools::GetAllowDeliveryDatesForOrder($deliveryID, 1, $arElementsQuantity, true, $arAllSkladUse);
		if (isset($arResult[0])) {
			$startDate = MakeTimeStamp($arResult[0], CSite::GetDateFormat("SHORT"));
		} else {
			return(false);
		}
		return ConvertTimeStamp($startDate, "SHORT");
	}

	function getDeliverySettings($deliveryID, $arAllSkladUse = false)
	{
		if (is_array($arAllSkladUse)) {
			ksort($arAllSkladUse);
		}
		if (!isset(self::$CACHE_DELIVERY_SETTINGS[$deliveryID][md5(serialize($arAllSkladUse))])) {
			$cache_id = $deliveryID . "|" . serialize($arAllSkladUse);
			$obCache = new CPHPCache();
			if ($obCache->InitCache(3600, $cache_id, "/webavk.deliverydate/")) {
				self::$CACHE_DELIVERY_SETTINGS[$deliveryID][md5(serialize($arAllSkladUse))] = $obCache->GetVars();
			}
		}
		if (!isset(self::$CACHE_DELIVERY_SETTINGS[$deliveryID][md5(serialize($arAllSkladUse))])) {
			$obCache->StartDataCache();
			$arF = array(
				"ACTIVE" => "Y",
				"DELIVERY.DELIVERY_ID" => $deliveryID
			);
			if (is_array($arAllSkladUse) && !empty($arAllSkladUse)) {
				$arF['STORE.STORE_ID'] = array_keys($arAllSkladUse);
			}
			$rDays = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
						"order" => array("SORT" => "ASC"),
						"filter" => $arF
			));
			while ($arDays = $rDays->Fetch()) {
				if (strlen($arDays['DELIVERY_DATE']) > 0) {
					$arDays['DELIVERY_DATE'] = MakeTimeStamp($arDays['DELIVERY_DATE']);
				}
				if (strlen($arDays['PERIOD_START']) > 0) {
					$arDays['PERIOD_START'] = MakeTimeStamp($arDays['PERIOD_START']);
				}
				if (strlen($arDays['PERIOD_END']) > 0) {
					$arDays['PERIOD_END'] = MakeTimeStamp($arDays['PERIOD_END']);
				}
				if (strlen($arDays['TIME_STOP']) > 0) {
					$ar = explode(":", $arDays['TIME_STOP']);
					$arDays['TIME_STOP'] = $ar[0] * 60 + $ar[1];
				}
				$rStores = WebAVK\DeliveryDate\Days\DaysStoreTable::getList(array(
							"filter" => array(
								"DAYS_ID" => $arDays['ID']
							)
				));
				while ($arStores = $rStores->Fetch()) {
					$arDays['STORES'][] = $arStores['STORE_ID'];
				}
				$rDelivery = WebAVK\DeliveryDate\Days\DaysDeliveryTable::getList(array(
							"filter" => array(
								"DAYS_ID" => $arDays['ID']
							)
				));
				while ($arDelivery = $rDelivery->Fetch()) {
					$arDays['DELIVERY'][] = $arDelivery['DELIVERY_ID'];
				}
				self::$CACHE_DELIVERY_SETTINGS[$deliveryID][md5(serialize($arAllSkladUse))][$arDays['ID']] = $arDays;
			}

			$obCache->EndDataCache(self::$CACHE_DELIVERY_SETTINGS[$deliveryID][md5(serialize($arAllSkladUse))]);
		}
		return self::$CACHE_DELIVERY_SETTINGS[$deliveryID][md5(serialize($arAllSkladUse))];
	}

	function getStartDate($deliveryID, $arAllSkladUse = false)
	{
		$result = time();
		$complete = false;
		$i = 0;
		while (!$complete && $i < 90) {
			$arSettings = CWebavkDeliveryDateTools::getRulesAllowDate($deliveryID, $result, $arAllSkladUse);
			foreach ($arSettings as $arSetting) {
				if ($arSetting['TIME_STOP'] > 0) {
					if ($arSetting['TIME_STOP'] > (date("H", $result) * 60 + date("i", $result))) {
						$complete = true;
						break;
					}
				}
			}
			if (!$complete) {
				$result = mktime(0, 0, 0, date("m", $result), date("d", $result), date("Y", $result)) + 3600 * 24;
			}
			$i++;
		}
		if (!$complete)
			return false;
		return $result;
	}

	function getRulesAllowDate($deliveryID, $dateTimeStamp, $arAllSkladUse = false)
	{
		$arResult = array();
		$arSettings = CWebavkDeliveryDateTools::getDeliverySettings($deliveryID, $arAllSkladUse);
		foreach ($arSettings as $arSetting) {
			$isOk = true;
			if ($arSetting['DELIVERY_DATE'] > 0) {
				if ($dateTimeStamp != $arSetting['DELIVERY_DATE'])
					$isOk = false;
			}
			if ($arSetting['PERIOD_START'] > 0) {
				if ($dateTimeStamp < $arSetting['PERIOD_START'])
					$isOk = false;
			}
			if ($arSetting['PERIOD_END'] > 0) {
				if ($dateTimeStamp > ($arSetting['PERIOD_END'] + 3600 * 24 - 1))
					$isOk = false;
			}
			if ($arSetting['MONTH_DAY'] > 0) {
				if ($arSetting['MONTH_DAY'] == 32) {
					$lastDayOfMonth = date('d', mktime(0, 0, 0, date("m", $dateTimeStamp) + 1, 0, date("Y", $dateTimeStamp)));
					if ($lastDayOfMonth != date("d", $dateTimeStamp)) {
						$isOk = false;
					}
				} elseif ($arSetting['MONTH_DAY'] != date("d", $dateTimeStamp)) {
					$isOk = false;
				}
			}
			if ($arSetting['WEEK_DAY'] > 0) {
				if (date("N", $dateTimeStamp) != $arSetting['WEEK_DAY'])
					$isOk = false;
			}
			if ($isOk)
				$arResult[$arSetting['ID']] = $arSetting;
		}
		return $arResult;
	}

	function checkDeliveryAllowDate($deliveryID, $dateTimeStamp, $arAllSkladUse = false)
	{
		$totalMaxOrders = COption::GetOptionString("webavk.deliverydate", "totalmaxorders", '');
		$arSettings = CWebavkDeliveryDateTools::getRulesAllowDate($deliveryID, $dateTimeStamp, $arAllSkladUse);
		foreach ($arSettings as $k => $arSetting) {
			$totalOrders = CWebavkDeliveryDateTools::getTotalOrdersDate($dateTimeStamp, $arSetting['DELIVERY']);
			$arSettings[$k]['TOTAL_ORDERS'] = $totalOrders;
		}
		$isOk = false;
		if (($totalMaxOrders > 0 && CWebavkDeliveryDateTools::getTotalOrdersDate($dateTimeStamp, false) <= $totalMaxOrders) || $totalMaxOrders <= 0) {
			foreach ($arSettings as $arSetting) {
				if ($arSetting['MAX_ORDERS'] == 0) {
					$isOk = false;
					break;
				}
				if ($arSetting['MAX_ORDERS'] > 0 && $arSetting['MAX_ORDERS'] > $arSetting['TOTAL_ORDERS']) {
					$isOk = true;
				}
			}
		}
		return $isOk;
	}

	function getTotalOrdersDate($orderDeliveryDateTimeStamp, $iDeliveryId = false)
	{
		if ($iDeliveryId !== false) {
			if (!is_array($iDeliveryId)) {
				$iDeliveryId = array($iDeliveryId);
			}
		}
		$orderDeliveryDateTimeStamp = MakeTimeStamp(ConvertTimeStamp($orderDeliveryDateTimeStamp, "SHORT"), CSite::GetDateFormat("SHORT"));
		if (empty($iDeliveryId)) {
			$cacheIdent = $orderDeliveryDateTimeStamp . "=ALL";
		} else {
			$cacheIdent = $orderDeliveryDateTimeStamp . "=" . implode("|" . $iDeliveryId);
		}
		if (!isset(self::$CACHE_ORDER_CNT[$cacheIdent])) {
			$arF = array("@STATUS_ID" => unserialize(COption::GetOptionString("webavk.deliverydate", "orderstatus")), "CANCELED" => "N", "PROPERTY_VAL_BY_CODE_F_DELIVERY_DATE" => date("d.m.Y", $orderDeliveryDateTimeStamp));
			if ($iDeliveryId !== false) {
				foreach ($iDeliveryId as $did) {
					unset($arF["DELIVERY_ID"]);
					unset($arF["~DELIVERY_ID"]);
					if (intval($did) >= 1) {
						$arF["DELIVERY_ID"] = $did;
					} else {
						if (strpos($did, ":") !== false) {
							$arF["DELIVERY_ID"] = $did;
						} else {
							$arF["~DELIVERY_ID"] = $did . ":%";
						}
					}
					$rOrders = CSaleOrder::GetList(array(), $arF, array("LID"));
					while ($arOrders = $rOrders->Fetch()) {
						self::$CACHE_ORDER_CNT[$cacheIdent]+=$arOrders['CNT'];
					}
				}
			} else {
				unset($arF["DELIVERY_ID"]);
				unset($arF["~DELIVERY_ID"]);
				$rOrders = CSaleOrder::GetList(array(), $arF, array("LID"));
				while ($arOrders = $rOrders->Fetch()) {
					self::$CACHE_ORDER_CNT[$cacheIdent]+=$arOrders['CNT'];
				}
			}
		}
		return self::$CACHE_ORDER_CNT[$cacheIdent];
	}

	function GetAllowDeliveryDatesForOrder($deliveryID, $days = 30, $arElementsQuantity = array(), $useStores = true, $arOnlyStores = false)
	{
		$obCache = new CPHPCache();
		$cache_id = md5(serialize(array(
			$deliveryID,
			$days,
			$arElementsQuantity,
			$useStores,
			$arOnlyStores
		)));
		if ($obCache->InitCache(600, $cache_id, "/GetAllowDeliveryDatesForOrder/")) {
			$arResult = $obCache->GetVars();
		}
		if ($obCache->StartDataCache()) {
			$arResult = self::_GetAllowDeliveryDatesForOrder($deliveryID, $days, $arElementsQuantity, $useStores, $arOnlyStores);
			if (empty($arResult) && strpos($deliveryID, ":") !== false) {
				$deliveryID = explode(":", trim($deliveryID));
				$deliveryID = $deliveryID[0];
				$arResult = self::_GetAllowDeliveryDatesForOrder($deliveryID, $days, $arElementsQuantity, $useStores, $arOnlyStores);
			}
			$obCache->EndDataCache($arResult);
		}
		return $arResult;
	}

	function _GetAllowDeliveryDatesForOrder($deliveryID, $days = 30, $arElementsQuantity = array(), $useStores = true, $arOnlyStores = false)
	{
		$WEBAVK_DELIVERYDATE_STORE_PRIORITY = CWebavkDeliveryDateTools::getStorePriority();
		/* if (strpos($deliveryID, ":") !== false) {
		  $deliveryID = explode(":", trim($deliveryID));
		  $deliveryID = $deliveryID[0];
		  } */
		$arResult = array();
		$arElementsID = array_keys($arElementsQuantity);
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("catalog");
		$arItemsBySklad = array();
		$arAllSkladUse = array();
		$arCheckSklad = array();
		if ($useStores) {
			if (!empty($arElementsID)) {
				$rProdStores = CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arElementsID));
				while ($arProdStores = $rProdStores->Fetch()) {
					if ($arOnlyStores !== false) {
						if (isset($arOnlyStores[$arProdStores['STORE_ID']])) {
							$arItemsBySklad[$arProdStores['PRODUCT_ID']][$arProdStores['STORE_ID']] = $arProdStores['AMOUNT'];
						} else {
							$arItemsBySklad[$arProdStores['PRODUCT_ID']][$arProdStores['STORE_ID']] = 0;
						}
					} else {
						$arItemsBySklad[$arProdStores['PRODUCT_ID']][$arProdStores['STORE_ID']] = $arProdStores['AMOUNT'];
					}
				}
				foreach ($arElementsQuantity as $pid => $pq) {
					$cnt = $pq;
					foreach ($WEBAVK_DELIVERYDATE_STORE_PRIORITY as $k => $v) {
						if ($cnt <= 0)
							break;
						if (isset($arItemsBySklad[$pid][$v]) && $arItemsBySklad[$pid][$v] > 0 && $cnt > 0) {
							if ($cnt > $arItemsBySklad[$pid][$v]) {
								$cnt-=$arItemsBySklad[$pid][$v];
							} else {
								$cnt = 0;
							}
							$arAllSkladUse[$pid] = array($v);
						}
					}
				}
			} else {
				foreach ($arOnlyStores as $k => $v) {
					$arAllSkladUse[0][] = $k;
				}
			}
			$a = array();
			foreach ($arAllSkladUse as $pid => $v) {
				foreach ($v as $v1) {
					$t = COption::GetOptionString("webavk.deliverydate", "storedays_" . $v1, '');
					if (strlen($t) > 0)
						$a[$v1] = $t;
				}
			}
			$arAllSkladUse = $a;
			if (empty($arElementsID)) {
				asort($arAllSkladUse, SORT_NUMERIC);
			} else {
				arsort($arAllSkladUse, SORT_NUMERIC);
			}
			$ak = array_keys($arAllSkladUse);
			$minDelta = $arAllSkladUse[$ak[0]];
			foreach ($arAllSkladUse as $sklad => $deltaTime) {
				if ($deltaTime == $minDelta) {
					$arCheckSklad[$sklad] = $deltaTime;
				}
			}
		}
		$arTMP = array();
		if (empty($arElementsID)) {
			$arElementsID = array(0 => 1);
		}
		foreach ($arElementsID as $elementID) {
			$replaceDeliveryDays = false;
			$replaceDeliveryDate = false;
			if ($elementID > 0) {
				$rEl = CIBlockElement::GetList(array(), array("ID" => $elementID), false, false, array("ID", "IBLOCK_ID", "PROPERTY_DELIVERY_DATE", "PROPERTY_DELIVERY_DAYS", "PROPERTY_CML2_LINK"));
				if ($arEl = $rEl->Fetch()) {
					if (strlen($arEl['PROPERTY_DELIVERY_DAYS_VALUE']) > 0) {
						$replaceDeliveryDays = $arEl['PROPERTY_DELIVERY_DAYS_VALUE'];
					}
					if (strlen($arEl['PROPERTY_DELIVERY_DATE_VALUE']) > 0) {
						$replaceDeliveryDate = $arEl['PROPERTY_DELIVERY_DATE_VALUE'];
					}
				}
			}
			$minDelta = 0;
			if (is_array($arCheckSklad) && !empty($arCheckSklad)) {
				$ak = array_keys($arCheckSklad);
				$minDelta = $arCheckSklad[$ak[0]];
			}
			$defaultDays = COption::GetOptionString("webavk.deliverydate", "deliverydays_" . $deliveryID, '');
			if (strlen($defaultDays) > 0) {
				$defaultDays+=$minDelta;
			}
			$minDate = $replaceDeliveryDate;
			$minDays = ($replaceDeliveryDays !== false ? $replaceDeliveryDays : $defaultDays);
			$startDate = CWebavkDeliveryDateTools::getStartDate($deliveryID, $arCheckSklad);
			if ($startDate <= 0)
				return false;
			if (strlen($minDays) <= 0)
				return false;
			if (strlen($minDate) > 0) {
				$minDate = MakeTimeStamp($minDate, CSite::GetDateFormat("SHORT"));
				if ($minDate >= time()) {
					$startDate = $minDate;
				} else {
					$startDate+=$minDays * 3600 * 24;
				}
			} elseif ($minDays > 0) {
				$startDate+=$minDays * 3600 * 24;
			}
			$startDate = MakeTimeStamp(ConvertTimeStamp($startDate, "SHORT"), CSite::GetDateFormat("SHORT"));
			$i = 0;
			$complete = false;
			while ($i < 90 && count($arTMP[$elementID]) < $days) {
				if (CWebavkDeliveryDateTools::checkDeliveryAllowDate($deliveryID, $startDate, $arCheckSklad)) {
					$arTMP[$elementID][] = ConvertTimeStamp($startDate, "SHORT");
				}
				$startDate = mktime(0, 0, 0, date("m", $startDate), date("d", $startDate), date("Y", $startDate)) + 3600 * 24;
				$i++;
			}
		}
		foreach ($arTMP as $k => $v) {
			foreach ($v as $v1) {
				$arResult[MakeTimeStamp($v1, CSite::GetDateFormat("SHORT"))][$k] = $v1;
			}
		}
		foreach ($arResult as $k => $v) {
			if (count($v) != count($arTMP))
				unset($arResult[$k]);
		}
		ksort($arResult, SORT_NUMERIC);
		$arNew = array();
		foreach ($arResult as $k => $v) {
			$arNew[] = ConvertTimeStamp($k, "SHORT");
		}
		$arResult = $arNew;
		return $arResult;
	}

	function GetTimeIntervalsForOrder($deliveryID, $findDate, $arElementsQuantity, $useStores = true)
	{
		$obCache = new CPHPCache();
		$cache_id = md5(serialize(array(
			$deliveryID, $findDate, $arElementsQuantity, $useStores
		)));
		if ($obCache->InitCache(600, $cache_id, "/GetTimeIntervalsForOrder/")) {
			$arResult = $obCache->GetVars();
		}
		if ($obCache->StartDataCache()) {
			$arResult = self::_GetTimeIntervalsForOrder($deliveryID, $findDate, $arElementsQuantity, $useStores);
			if (empty($arResult) && strpos($deliveryID, ":") !== false) {
				$deliveryID = explode(":", trim($deliveryID));
				$deliveryID = $deliveryID[0];
				$arResult = self::_GetTimeIntervalsForOrder($deliveryID, $findDate, $arElementsQuantity, $useStores);
			}
			$obCache->EndDataCache($arResult);
		}
		return $arResult;
	}

	function _GetTimeIntervalsForOrder($deliveryID, $findDate, $arElementsQuantity, $useStores = true)
	{
		$WEBAVK_DELIVERYDATE_STORE_PRIORITY = CWebavkDeliveryDateTools::getStorePriority();
		/* if (strpos($deliveryID, ":") !== false) {
		  $deliveryID = explode(":", trim($deliveryID));
		  $deliveryID = $deliveryID[0];
		  }
		 * 
		 */
		$arResult = array();
		$arElementsID = array_keys($arElementsQuantity);
		if (!empty($arElementsID)) {
			CModule::IncludeModule("iblock");
			CModule::IncludeModule("catalog");
			$arItemsBySklad = array();
			$arAllSkladUse = array();
			$arCheckSklad = array();
			if ($useStores) {
				$rProdStores = CCatalogStoreProduct::GetList(array(), array("PRODUCT_ID" => $arElementsID));
				while ($arProdStores = $rProdStores->Fetch()) {
					$arItemsBySklad[$arProdStores['PRODUCT_ID']][$arProdStores['STORE_ID']] = $arProdStores['AMOUNT'];
				}
				foreach ($arElementsQuantity as $pid => $pq) {
					$cnt = $pq;
					foreach ($WEBAVK_DELIVERYDATE_STORE_PRIORITY as $k => $v) {
						if ($cnt <= 0)
							break;
						if (isset($arItemsBySklad[$pid][$v]) && $arItemsBySklad[$pid][$v] > 0 && $cnt > 0) {
							if ($cnt > $arItemsBySklad[$pid][$v]) {
								$cnt-=$arItemsBySklad[$pid][$v];
							} else {
								$cnt = 0;
							}
							$arAllSkladUse[$pid] = array($v);
						}
					}
				}
				$a = array();
				foreach ($arAllSkladUse as $pid => $v) {
					foreach ($v as $v1) {
						$t = COption::GetOptionString("webavk.deliverydate", "storedays_" . $v1, '');
						if (strlen($t) > 0)
							$a[$v1] = $t;
					}
				}
				$arAllSkladUse = $a;
				arsort($arAllSkladUse, SORT_NUMERIC);
				$ak = array_keys($arAllSkladUse);
				$minDelta = $arAllSkladUse[$ak[0]];
				foreach ($arAllSkladUse as $sklad => $deltaTime) {
					if ($deltaTime == $minDelta) {
						$arCheckSklad[$sklad] = $deltaTime;
					}
				}
			}
			$arSettings = CWebavkDeliveryDateTools::getRulesAllowDate($deliveryID, $findDate, $arCheckSklad);
			foreach ($arSettings as $k => $arSetting) {
				$totalOrders = CWebavkDeliveryDateTools::getTotalOrdersDate($findDate, $arSetting['DELIVERY']);
				$arSettings[$k]['TOTAL_ORDERS'] = $totalOrders;
			}
			$arCurrentRule = array();
			foreach ($arSettings as $arSetting) {
				if ($arSetting['MAX_ORDERS'] > 0 && $arSetting['MAX_ORDERS'] > $arSetting['TOTAL_ORDERS']) {
					$arCurrentRule = $arSetting;
				}
			}
			if (strlen(trim($arCurrentRule['TIME_INTERVALS'])) > 0) {
				return explode("\n", trim($arCurrentRule['TIME_INTERVALS']));
			}
		}
	}

	function getStorePriority()
	{
		CModule::IncludeModule("sale");
		if (self::$CACHE_STORE_PRIORITY === false) {
			$arResult = array();
			$rSaleStore = CCatalogStore::GetList(array("SORT" => "ASC", "TITLE" => "ASC"), array());
			while ($arSaleStore = $rSaleStore->Fetch()) {
				$arResult[$arSaleStore['ID']] = COption::GetOptionString("webavk.deliverydate", "storepriority_" . $arSaleStore['ID'], "0");
			}
			asort($arResult, SORT_NUMERIC);
			$arResult = array_keys($arResult);
			self::$CACHE_STORE_PRIORITY = $arResult;
		}
		$arResult = self::$CACHE_STORE_PRIORITY;
		return($arResult);
	}

	function getAllActiveDeliveryAndCntOrders($dayCnt = 10)
	{
		$arResult = array(
			"DELIVERY" => array(),
			"ORDERS" => array()
		);
		$iTime = time();
		for ($i = 0; $i <= $dayCnt; $i++) {
			$sTime = date("d.m.Y", $iTime + $i * 3600 * 24);
			$arResult['ORDERS'][$sTime] = array();
			$rOrders = CSaleOrder::GetList(array(), array("@STATUS_ID" => unserialize(COption::GetOptionString("webavk.deliverydate", "ordercntstatus")), "CANCELED" => "N", "PROPERTY_VAL_BY_CODE_F_DELIVERY_DATE" => $sTime), array("DELIVERY_ID"));
			while ($arOrders = $rOrders->Fetch()) {
				$arResult['ORDERS'][$sTime][$arOrders['DELIVERY_ID']] = $arOrders['CNT'];
				if (!isset($arResult['DELIVERY'][$arOrders['DELIVERY_ID']])) {
					$arResult['DELIVERY'][$arOrders['DELIVERY_ID']] = array();
				}
			}
		}
		ksort($arResult['DELIVERY']);
		$dbDeliveryServices = CSaleDeliveryHandler::GetList(array("SORT" => "ASC"), array());
		while ($arDeliveryService = $dbDeliveryServices->Fetch()) {
			foreach ($arDeliveryService['PROFILES'] as $kProfile => $arProfile) {
				if (isset($arResult['DELIVERY'][$arDeliveryService['SID'] . ":" . $kProfile])) {
					$arResult['DELIVERY'][$arDeliveryService['SID'] . ":" . $kProfile] = array(
						"NAME" => "[" . $arDeliveryService['SID'] . ":" . $kProfile . "] " . $arProfile['TITLE'],
						"DESCRIPTION" => $arProfile['DESCRIPTION']
					);
				}
			}
		}
		$dbDelivery = CSaleDelivery::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("ACTIVE" => "Y"));
		while ($arDelivery = $dbDelivery->Fetch()) {
			if (isset($arResult['DELIVERY'][$arDelivery['ID']])) {
				$arResult['DELIVERY'][$arDelivery['ID']] = array(
					"NAME" => "[" . $arDelivery['ID'] . "] " . $arDelivery['NAME'],
					"DESCRIPTION" => ""
				);
			}
		}
		return $arResult;
	}

}
