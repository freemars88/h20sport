<?php

namespace WebAVK\DeliveryDate\Days;

class DaysTable extends \WebAVK\DeliveryDate\DataManager {

	public static function getTableName() {
		return 'wadd_days';
	}

	public static function getFilePath() {
		return __FILE__;
	}

	public static function getMap() {
		$fieldsMap = array(
			"ID" => array(
				"data_type" => "integer",
				"primary" => true
			),
			"NAME" => array(
				"data_type" => "string"
			),
			"ACTIVE" => array(
				"data_type" => "boolean",
				"values" => array("N", "Y")
			),
			"SORT" => array(
				"data_type" => "integer"
			),
			"STORE" => array(
				"data_type" => 'WebAVK\DeliveryDate\Days\DaysStoreTable',
				'reference' => array('=this.ID' => 'ref.DAYS_ID')
			),
			"DELIVERY" => array(
				"data_type" => 'WebAVK\DeliveryDate\Days\DaysDeliveryTable',
				'reference' => array('=this.ID' => 'ref.DAYS_ID')
			),
			"DELIVERY_DATE" => array(
				"data_type" => "datetime"
			),
			"WEEK_DAY" => array(
				"data_type" => "integer"
			),
			"MONTH_DAY" => array(
				"data_type" => "string"
			),
			"PERIOD_START" => array(
				"data_type" => "datetime"
			),
			"PERIOD_END" => array(
				"data_type" => "datetime"
			),
			"MAX_ORDERS" => array(
				"data_type" => "integer"
			),
			"TIME_START" => array(
				"data_type" => "string"
			),
			"TIME_END" => array(
				"data_type" => "string"
			),
			"TIME_STOP" => array(
				"data_type" => "string"
			),
			"TIME_STOP_DEFAULT" => array(
				"data_type" => "string",
			),
			"TIME_INTERVALS" => array(
				"data_type" => "string",
			),
		);
		return $fieldsMap;
	}

	public static function add($data) {
		$arNewDelivery = false;
		$arNewStore = false;
		if (isset($data['DELIVERY']))
			$arNewDelivery = $data['DELIVERY'];
		if (isset($data['STORE']))
			$arNewStore = $data['STORE'];
		unset($data['DELIVERY']);
		unset($data['STORE']);
		$result = parent::add($data);
		if ($result->isSuccess()) {
			if ($arNewDelivery !== false) {
				foreach ($arNewDelivery as $did) {
					DaysDeliveryTable::add(array("DAYS_ID" => $result->getId(), "DELIVERY_ID" => $did));
				}
			}
			if ($arNewStore !== false) {
				foreach ($arNewStore as $sid) {
					DaysStoreTable::add(array("DAYS_ID" => $result->getId(), "STORE_ID" => $sid));
				}
			}
		}
		return $result;
	}

	public static function update($primary, array $data) {
		$arNewDelivery = false;
		$arNewStore = false;
		if (isset($data['DELIVERY']))
			$arNewDelivery = $data['DELIVERY'];
		if (isset($data['STORE']))
			$arNewStore = $data['STORE'];
		unset($data['DELIVERY']);
		unset($data['STORE']);
		$result = parent::update($primary, $data);
		if ($arNewDelivery !== false) {
			$rDelivery = DaysDeliveryTable::getList(array(
						"filter" => array("DAYS_ID" => $primary)
			));
			while ($arDelivery = $rDelivery->Fetch()) {
				DaysDeliveryTable::delete(array("DAYS_ID" => $arDelivery['DAYS_ID'], "DELIVERY_ID" => $arDelivery['DELIVERY_ID']));
			}
			foreach ($arNewDelivery as $did) {
				DaysDeliveryTable::add(array("DAYS_ID" => $primary, "DELIVERY_ID" => $did));
			}
		}
		if ($arNewStore !== false) {
			$rStore = DaysStoreTable::getList(array(
						"filter" => array("DAYS_ID" => $primary)
			));
			while ($arStore = $rStore->Fetch()) {
				DaysStoreTable::delete(array("DAYS_ID" => $arStore['DAYS_ID'], "STORE_ID" => $arStore['STORE_ID']));
			}
			foreach ($arNewStore as $sid) {
				DaysStoreTable::add(array("DAYS_ID" => $primary, "STORE_ID" => $sid));
			}
		}
		return $result;
	}

	public static function delete($primary) {
		$result = parent::delete($primary);
		$rDelivery = DaysDeliveryTable::getList(array(
					"filter" => array("DAYS_ID" => $primary)
		));
		while ($arDelivery = $rDelivery->Fetch()) {
			DaysDeliveryTable::delete(array("DAYS_ID" => $arDelivery['DAYS_ID'], "DELIVERY_ID" => $arDelivery['DELIVERY_ID']));
		}
		$rStore = DaysStoreTable::getList(array(
					"filter" => array("DAYS_ID" => $primary)
		));
		while ($arStore = $rStore->Fetch()) {
			DaysStoreTable::delete(array("DAYS_ID" => $arStore['DAYS_ID'], "STORE_ID" => $arStore['STORE_ID']));
		}
		return $result;
	}

}

