<?php

namespace WebAVK\DeliveryDate\Days;

class DaysStoreTable extends \WebAVK\DeliveryDate\DataManager {

	public static function getTableName() {
		return 'wadd_days_store';
	}

	public static function getFilePath() {
		return __FILE__;
	}

	public static function getMap() {
		$fieldsMap = array(
			"DAYS_ID" => array(
				"data_type" => "integer",
				"primary" => true
			),
			"STORE_ID" => array(
				"data_type" => "integer",
				"primary" => true
			)
		);
		return $fieldsMap;
	}

}
