<?php

namespace WebAVK\DeliveryDate\Days;

class DaysDeliveryTable extends \WebAVK\DeliveryDate\DataManager {

	public static function getTableName() {
		return 'wadd_days_delivery';
	}

	public static function getFilePath() {
		return __FILE__;
	}

	public static function getMap() {
		$fieldsMap = array(
			"DAYS_ID" => array(
				"data_type" => "integer",
				"primary" => true
			),
			"DELIVERY_ID" => array(
				"data_type" => "string",
				"primary" => true
			)
		);
		return $fieldsMap;
	}

}
