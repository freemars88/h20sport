ALTER TABLE `wadd_days` ADD COLUMN `NAME` VARCHAR(255) NOT NULL COMMENT 'Название'  AFTER `ID` , ADD COLUMN `TIME_INTERVALS` TEXT NULL DEFAULT NULL COMMENT 'Временные интервалы'  AFTER `TIME_STOP_DEFAULT`;

CREATE  TABLE IF NOT EXISTS `wadd_days_delivery` (
  `DAYS_ID` INT(11) NOT NULL COMMENT 'ID правила' ,
  `DELIVERY_ID` VARCHAR(255) NOT NULL COMMENT 'Служба доставки' ,
  PRIMARY KEY (`DAYS_ID`, `DELIVERY_ID`)
)
COMMENT = 'Службы доставки для правил';

CREATE  TABLE IF NOT EXISTS `wadd_days_store` (
  `DAYS_ID` INT(11) NOT NULL COMMENT 'ID правила' ,
  `STORE_ID` INT(11) NOT NULL COMMENT 'ID склада' ,
  PRIMARY KEY (`DAYS_ID`, `STORE_ID`)
)
COMMENT = 'Склады для правил доставки';

INSERT INTO `wadd_days_delivery` (`DAYS_ID`, `DELIVERY_ID`) SELECT `ID`, `DELIVERY_ID` FROM `wadd_days`;

ALTER TABLE `wadd_days` DROP COLUMN `DELIVERY_ID` ;