CREATE TABLE IF NOT EXISTS `wadd_days` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `NAME` varchar(255) NOT NULL COMMENT 'Название',
  `ACTIVE` char(1) NOT NULL DEFAULT 'Y' COMMENT 'Активность',
  `SORT` int(11) NOT NULL DEFAULT '100' COMMENT 'Индекс сортировки',
  `DELIVERY_DATE` date DEFAULT NULL COMMENT 'Дата',
  `WEEK_DAY` int(11) DEFAULT NULL COMMENT 'День недели',
  `MONTH_DAY` varchar(255) DEFAULT NULL COMMENT 'День месяца',
  `PERIOD_START` date DEFAULT NULL COMMENT 'Начало периода',
  `PERIOD_END` date DEFAULT NULL COMMENT 'Конец периода',
  `MAX_ORDERS` int(11) NOT NULL DEFAULT '0' COMMENT 'Максимально количество заказов',
  `TIME_START` varchar(255) NOT NULL DEFAULT '00:00' COMMENT 'Время начала доставки',
  `TIME_END` varchar(255) NOT NULL DEFAULT '00:00' COMMENT 'Время окончания доставки',
  `TIME_STOP` varchar(255) NOT NULL DEFAULT '17:00' COMMENT 'Время стоп-листа',
  `TIME_STOP_DEFAULT` varchar(255) NOT NULL DEFAULT '17:00' COMMENT 'Время стоп-листа по умолчанию',
  `TIME_INTERVALS` text COMMENT 'Временные интервалы',
  PRIMARY KEY (`ID`)
) COMMENT='Параметры ближайшей доставки';

CREATE TABLE IF NOT EXISTS `wadd_days_delivery` (
  `DAYS_ID` int(11) NOT NULL COMMENT 'ID правила',
  `DELIVERY_ID` varchar(255) NOT NULL COMMENT 'Служба доставки',
  PRIMARY KEY (`DAYS_ID`,`DELIVERY_ID`)
) COMMENT='Службы доставки для правил';

CREATE TABLE IF NOT EXISTS `wadd_days_store` (
  `DAYS_ID` int(11) NOT NULL COMMENT 'ID правила',
  `STORE_ID` int(11) NOT NULL COMMENT 'ID склада',
  PRIMARY KEY (`DAYS_ID`,`STORE_ID`)
) COMMENT='Склады для правил доставки';


