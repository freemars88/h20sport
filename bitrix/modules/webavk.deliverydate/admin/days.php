<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webavk.deliverydate/prolog.php");
CModule::IncludeModule('webavk.deliverydate');
IncludeModuleLangFile(__FILE__);

$modulePermissions = $APPLICATION->GetGroupRight("webavk.deliverydate");
if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
if (!CModule::IncludeModule('sale')) {
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(GetMessage("webavk.deliverydate_MODULE_SALE_REQUIRED"));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}
if (!CModule::IncludeModule('catalog')) {
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(GetMessage("webavk.deliverydate_MODULE_CATALOG_REQUIRED"));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}
CJSCore::Init(array('jquery'));
ClearVars();
$APPLICATION->SetTitle(GetMessage('webavk.deliverydate_PAGE_TITLE'));

$arSaleDeliverys = array();
$rSaleDelivery = CSaleDelivery::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array());
while ($arSaleDelivery = $rSaleDelivery->Fetch()) {
	$arSaleDeliverys[$arSaleDelivery['ID']] = '(' . $arSaleDelivery['LID'] . ') ' . $arSaleDelivery['NAME'];
}
$rSaleDelivery = CSaleDeliveryHandler::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array());
while ($arSaleDelivery = $rSaleDelivery->Fetch()) {
	if (strlen($arSaleDelivery['LID']) > 0) {
		$arSaleDeliverys[$arSaleDelivery['SID']] = '(' . $arSaleDelivery['LID'] . ') ' . $arSaleDelivery['NAME'];
	} else {
		$arSaleDeliverys[$arSaleDelivery['SID']] = $arSaleDelivery['NAME'];
	}
	foreach ($arSaleDelivery['PROFILES'] as $profileId => $arProfile) {
		$arSaleDeliverys[$arSaleDelivery['SID'] . ":" . $profileId] = $arSaleDelivery['NAME'] . " [" . $arProfile['TITLE'] . "]";
	}
}

$arSaleStores = array();
$rSaleStore = CCatalogStore::GetList(array("SORT" => "ASC", "TITLE" => "ASC"), array());
while ($arSaleStore = $rSaleStore->Fetch()) {
	$arSaleStores[$arSaleStore['ID']] = $arSaleStore['TITLE'];
}

$arWeekDays = array(
	"1" => GetMessage("webavk.deliverydate_WEEKDAY_1"),
	"2" => GetMessage("webavk.deliverydate_WEEKDAY_2"),
	"3" => GetMessage("webavk.deliverydate_WEEKDAY_3"),
	"4" => GetMessage("webavk.deliverydate_WEEKDAY_4"),
	"5" => GetMessage("webavk.deliverydate_WEEKDAY_5"),
	"6" => GetMessage("webavk.deliverydate_WEEKDAY_6"),
	"7" => GetMessage("webavk.deliverydate_WEEKDAY_7"),
);
$arMonthDay = array();
$arMonthDay[1] = GetMessage("webavk.deliverydate_MONTHDAY_FIRST");
for ($i = 2; $i <= 31; $i++)
	$arMonthDay[$i] = sprintf("%02d", $i);
$arMonthDay[32] = GetMessage("webavk.deliverydate_MONTHDAY_LAST");

$arTime = array();
$step = COption::GetOptionInt("webavk.deliverydate", "minuteselstep", 15);
for ($h = 0; $h <= 23; $h++) {
	for ($m = 0; $m <= 59; $m = $m + $step) {
		$time = sprintf("%02d:%02d", $h, $m);
		$arTime[$time] = $time;
	}
}

$sTableID = "t_webavk_deliverydate_days";
$oSort = new CAdminSorting($sTableID, "SORT", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_ID",
	"filter_active",
	"filter_delivery_id",
	"filter_week_day",
);
$lAdmin->InitFilter($arFilterFields);
$arFilter = array();

if (!empty($filter_ID_from))
	$arFilter[">=ID"] = $filter_ID_from;
if (!empty($filter_ID_to))
	$arFilter["<=ID"] = $filter_ID_to;
if (!empty($filter_active))
	$arFilter["ACTIVE"] = $filter_active;
if (!empty($filter_delivery_id))
	$arFilter["DELIVERY_ID"] = $filter_delivery_id;
if (!empty($filter_week_day))
	$arFilter["WEEK_DAY"] = $filter_week_day;

if ($lAdmin->EditAction() && $modulePermissions >= "W") {
	foreach ($FIELDS as $ID => $arFields) {
		if (!$lAdmin->IsUpdated($ID))
			continue;
		foreach ($arFields as $k => $v) {
			if (in_array($k, array("DELIVERY_DATE", "WEEK_DAY", "MONTH_DAY", "PERIOD_START", "PERIOD_END"))) {
				if ($v === "0" || strlen($v) < 1)
					$arFields[$k] = false;
			}
			if (in_array($k, array("DELIVERY_DATE", "PERIOD_START", "PERIOD_END"))) {
				if ($arFields[$k] !== false)
					$arFields[$k] = new Bitrix\Main\Type\Date($arFields[$k]);
			}
		}
		$DB->StartTransaction();
		$ID = IntVal($ID);
		$ob = new WebAVK\DeliveryDate\Days\DaysTable();

		if (!$ob->update($ID, $arFields)) {
			$lAdmin->AddUpdateError(GetMessage("webavk.deliverydate_POST_SAVE_ERROR") . $ID . ": " . $ob->LAST_ERROR, $ID);
			$DB->Rollback();
		}
		$DB->Commit();
	}
}

if (($arID = $lAdmin->GroupAction()) && $modulePermissions >= "W") {
	if ($_REQUEST['action_target'] == 'selected') {
		$arID = Array();
		$dbResultList = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
					"select" => array("ID"),
					"filter" => $arFilter,
					"order" => array($by => $order)
		));
		while ($arResult = $dbResultList->Fetch())
			$arID[] = $arResult['ID'];
	}
	$ob = new WebAVK\DeliveryDate\Days\DaysTable();

	foreach ($arID as $ID) {
		if (strlen($ID) <= 0)
			continue;

		switch ($_REQUEST['action']) {
			case "delete":
				@set_time_limit(0);

				$DB->StartTransaction();

				if (!$ob->delete($ID)) {
					$DB->Rollback();

					if ($ex = $APPLICATION->GetException())
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					else
						$lAdmin->AddGroupError(GetMessage("webavk.deliverydate_ERROR_DELETE"), $ID);
				}

				$DB->Commit();

				break;
			case "activate":
			case "deactivate":
				$arFields = array(
					"ACTIVE" => (($_REQUEST['action'] == "activate") ? "Y" : "N")
				);
				if (!$ob->update($ID, $arFields)) {
					if ($ex = $APPLICATION->GetException())
						$lAdmin->AddGroupError($ex->GetString(), $ID);
					else
						$lAdmin->AddGroupError(GetMessage("webavk.deliverydate_ERROR_ACTIVATE"), $ID);
				}
				break;
		}
	}
}
$arHeaders = array(
	array("id" => "ID", "content" => "ID", "sort" => "ID", "default" => true),
	array("id" => "ACTIVE", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_ACTIVE"), "sort" => "ACTIVE", "default" => true),
	array("id" => "NAME", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_NAME"), "sort" => "NAME", "default" => true),
	array("id" => "SORT", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_SORT"), "sort" => "SORT", "default" => true),
	array("id" => "DELIVERY", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_DELIVERY"), "default" => true),
	array("id" => "STORE", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_STORE"), "default" => true),
	array("id" => "DELIVERY_DATE", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_DELIVERY_DATE"), "sort" => "DELIVERY_DATE", "default" => true),
	array("id" => "WEEK_DAY", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_WEEK_DAY"), "sort" => "WEEK_DAY", "default" => true),
	array("id" => "MONTH_DAY", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_MONTH_DAY"), "sort" => "MONTH_DAY", "default" => true),
	array("id" => "PERIOD_START", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_PERIOD_START"), "sort" => "PERIOD_START", "default" => true),
	array("id" => "PERIOD_END", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_PERIOD_END"), "sort" => "PERIOD_END", "default" => true),
	array("id" => "MAX_ORDERS", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_MAX_ORDERS"), "sort" => "MAX_ORDERS", "default" => true),
	array("id" => "TIME_START", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_TIME_START"), "sort" => "TIME_START", "default" => true),
	array("id" => "TIME_END", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_TIME_END"), "sort" => "TIME_END", "default" => true),
	array("id" => "TIME_STOP", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_TIME_STOP"), "sort" => "TIME_STOP", "default" => true),
	array("id" => "TIME_STOP_DEFAULT", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_TIME_STOP_DEFAULT"), "sort" => "TIME_STOP_DEFAULT", "default" => true),
	array("id" => "TIME_INTERVALS", "content" => GetMessage("webavk.deliverydate_FIELD_HEADER_TIME_INTERVALS"), "default" => false),
);

$lAdmin->AddHeaders($arHeaders);
$arVisibleColumns = $lAdmin->GetVisibleHeaderColumns();
$arSelectColumns = $arVisibleColumns;
if (in_array("DELIVERY", $arSelectColumns))
	unset($arSelectColumns[array_search("DELIVERY", $arSelectColumns)]);
if (in_array("STORE", $arSelectColumns))
	unset($arSelectColumns[array_search("STORE", $arSelectColumns)]);

$rDays = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
			"select" => $arSelectColumns,
			"filter" => $arFilter,
			"order" => array($by => $order)
		));
$rDays = new CAdminResult($rDays, $sTableID);
$rDays->NavStart();
$lAdmin->NavText($rDays->GetNavPrint(GetMessage("webavk.deliverydate_NAVPRINT")));
while ($arDays = $rDays->GetNext()) {
	/*
	  if (strlen($arDays['DELIVERY_DATE']) > 0) {
	  $arDays['DELIVERY_DATE'] = ConvertTimeStamp(MakeTimeStamp($arDays['DELIVERY_DATE'], "YYYY-MM-DD"), FORMAT_DATE);
	  }
	  if (strlen($arDays['PERIOD_START']) > 0) {
	  $arDays['PERIOD_START'] = ConvertTimeStamp(MakeTimeStamp($arDays['PERIOD_START'], "YYYY-MM-DD"), FORMAT_DATE);
	  }
	  if (strlen($arDays['PERIOD_END']) > 0) {
	  $arDays['PERIOD_END'] = ConvertTimeStamp(MakeTimeStamp($arDays['PERIOD_END'], "YYYY-MM-DD"), FORMAT_DATE);
	  }
	 * 
	 */
	$row = & $lAdmin->AddRow($arDays["ID"], $arDays, "webavk.deliverydate.days.edit.php?ID=" . $arDays["ID"] . "&lang=" . LANGUAGE_ID, GetMessage("webavk.deliverydate_ROW_TITLE"));

	$row->AddCheckField("ACTIVE");
	$row->AddInputField("SORT");
	$row->AddInputField("NAME");

	$arSaleDeliverysList = array(false => GetMessage("MAIN_ALL"));
	foreach ($arSaleDeliverys as $k => $v) {
		$arSaleDeliverysList[$k] = '[' . $k . '] ' . $v;
	}
	if (in_array("DELIVERY", $arVisibleColumns)) {
		$arDelivery = array();
		$rD = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
					"select" => array("DELIVERY.DELIVERY_ID"),
					"filter" => array("ID" => $arDays['ID'])
		));
		while ($arD = $rD->Fetch()) {
			if (intval($arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID']) > 0) {
				$arDelivery[] = '[<a href="/bitrix/admin/sale_delivery_edit.php?ID=' . $arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID'] . '">' . $arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID'] . '</a>] ' . $arSaleDeliverys[$arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID']];
			} elseif (strlen($arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID']) > 0) {
				$arDelivery[] = '[<a href="/bitrix/admin/sale_delivery_handler_edit.php?SID=' . $arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID'] . '">' . $arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID'] . '</a>] ' . $arSaleDeliverys[$arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID']];
			}
		}
		$row->AddViewField("DELIVERY", implode("<br/>", $arDelivery));
		//$row->AddSelectField("DELIVERY", $arSaleDeliverysList);
	}
	if (in_array("STORE", $arVisibleColumns)) {
		$arStore = array();
		$rS = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
					"select" => array("STORE.STORE_ID"),
					"filter" => array("ID" => $arDays['ID'])
		));
		while ($arS = $rS->Fetch()) {
			if (intval($arS['WEBAVK_DELIVERYDATE_DAYS_DAYS_STORE_STORE_ID']) > 0) {
				$arStore[] = '[<a href="/bitrix/admin/cat_store_edit.php?ID=' . $arS['WEBAVK_DELIVERYDATE_DAYS_DAYS_STORE_STORE_ID'] . '">' . $arS['WEBAVK_DELIVERYDATE_DAYS_DAYS_STORE_STORE_ID'] . '</a>] ' . $arSaleStores[$arS['WEBAVK_DELIVERYDATE_DAYS_DAYS_STORE_STORE_ID']];
			}
		}
		$row->AddViewField("STORE", implode("<br/>", $arStore));
		//$row->AddSelectField("DELIVERY", $arSaleDeliverysList);
	}
	/*
	  $row->AddSelectField("DELIVERY_ID", $arSaleDeliverysList);
	  if (intval($arDays['DELIVERY_ID']) > 0) {
	  $row->AddViewField("DELIVERY_ID", '[<a href="/bitrix/admin/sale_delivery_edit.php?ID=' . $arDays['DELIVERY_ID'] . '">' . $arDays['DELIVERY_ID'] . '</a>] ' . $arSaleDeliverys[$arDays['DELIVERY_ID']]);
	  } elseif (strlen($arDays['DELIVERY_ID']) > 0) {
	  $row->AddViewField("DELIVERY_ID", '[<a href="/bitrix/admin/sale_delivery_handler_edit.php?SID=' . $arDays['DELIVERY_ID'] . '">' . $arDays['DELIVERY_ID'] . '</a>] ' . $arSaleDeliverys[$arDays['DELIVERY_ID']]);
	  } else {
	  $row->AddViewField("DELIVERY_ID");
	  }
	 */
	$row->AddCalendarField("DELIVERY_DATE");
	$arWeekDaysList = array(false => GetMessage("MAIN_ALL"));
	foreach ($arWeekDays as $k => $v) {
		$arWeekDaysList[$k] = '[' . $k . '] ' . $v;
	}
	$row->AddSelectField("WEEK_DAY", $arWeekDaysList);
	$arMonthDayList = array(false => GetMessage("MAIN_ALL"));
	foreach ($arMonthDay as $k => $v) {
		$arMonthDayList[$k] = '[' . $k . '] ' . $v;
	}
	$row->AddSelectField("MONTH_DAY", $arMonthDayList);
	$row->AddCalendarField("PERIOD_START");
	$row->AddCalendarField("PERIOD_END");
	$row->AddInputField("MAX_ORDERS", array("size" => 10));
	$row->AddSelectField("TIME_START", $arTime);
	$row->AddSelectField("TIME_END", $arTime);
	$row->AddSelectField("TIME_STOP", $arTime);
	$row->AddSelectField("TIME_STOP_DEFAULT", $arTime);


	$arActions = array();
	$arActions[] = array("ICON" => "edit", "TEXT" => GetMessage("MAIN_EDIT"), "ACTION" => $lAdmin->ActionRedirect("webavk.deliverydate.days.edit.php?ID=" . $arDays["ID"] . "&lang=" . LANGUAGE_ID), "DEFAULT" => true);
	if ($modulePermissions >= "W") {
		$arActions[] = array("SEPARATOR" => true);
		$arActions[] = array("ICON" => "delete", "TEXT" => GetMessage("webavk.deliverydate_DELETE"), "ACTION" => "if(confirm('" . GetMessage("webavk.deliverydate_DELETE_CONFIRM") . "')) " . $lAdmin->ActionDoGroup($arDays['ID'], "delete"));
	}
	$row->AddActions($arActions);
}

$arFooterArray = array(
	array(
		"title" => GetMessage('MAIN_ADMIN_LIST_SELECTED'),
		"value" => $rDays->SelectedRowsCount()
	)
);
$lAdmin->AddFooter($arFooterArray);

if ($modulePermissions >= "W") {
	$lAdmin->AddGroupActionTable(Array(
		"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
		"activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
		"deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
			)
	);
}

if ($modulePermissions >= "W") {
	$aContext = array(
		array(
			"TEXT" => GetMessage("webavk.deliverydate_CONTEXT_ADD"),
			"LINK" => "webavk.deliverydate.days.edit.php?lang=" . LANG,
			"TITLE" => GetMessage("webavk.deliverydate_CONTEXT_ADD_DESCRIPTION"),
			"ICON" => "btn_new"
		),
	);
	$lAdmin->AddAdminContextMenu($aContext);
}
$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?">
	<?
	$oFilter = new CAdminFilter(
			$sTableID . "_filters", array(
		"filter_ID" => GetMessage("webavk.deliverydate_FILTER_FIELD_ID"),
		"filter_active" => GetMessage("webavk.deliverydate_FILTER_FIELD_ACTIVE"),
		"filter_delivery_id" => GetMessage("webavk.deliverydate_FILTER_FIELD_DELIVERY_ID"),
		"filter_week_day" => GetMessage("webavk.deliverydate_FILTER_FIELD_WEEK_DAY"),
			)
	);
	$oFilter->Begin();
	?>
	<tr>
		<td><?= GetMessage('webavk.deliverydate_FILTER_FIELD_ID') ?>:</td>
		<td>
			<? echo GetMessage("webavk.deliverydate_FROM"); ?>
			<input type="text" name="filter_ID_from" id="filter_ID_from" value="<? echo (IntVal($filter_ID_from) > 0) ? IntVal($filter_ID_from) : "" ?>" size="10">
			<? echo GetMessage("webavk.deliverydate_TO"); ?>
			<input type="text" name="filter_ID_to" id="filter_ID_to" value="<? echo (IntVal($filter_ID_to) > 0) ? IntVal($filter_ID_to) : "" ?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?= GetMessage('webavk.deliverydate_FILTER_FIELD_ACTIVE') ?>:</td>
		<td>
			<select name="filter_active">
				<option value=""><? echo GetMessage("MAIN_ALL") ?></option>
				<option value="Y"<? if ($filter_active == "Y") echo " selected" ?>><? echo GetMessage("MAIN_YES") ?></option>
				<option value="N"<? if ($filter_active == "N") echo " selected" ?>><? echo GetMessage("MAIN_NO") ?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?= GetMessage('webavk.deliverydate_FILTER_FIELD_DELIVERY_ID') ?>:</td>
		<td>
			<select name="filter_delivery_id[]" size="3" multiple="multiple">
				<?
				foreach ($arSaleDeliverys as $k => $v) {
					?>
					<option value="<?= $k ?>"<? if (in_array($k, $filter_delivery_id)) { ?> selected="selected"<? } ?>>[<?= $k ?>] <?= $v ?></option>
					<?
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?= GetMessage('webavk.deliverydate_FILTER_FIELD_WEEK_DAY') ?>:</td>
		<td>
			<select name="filter_week_day[]" multiple="multiple" size="3">
				<?
				foreach ($arWeekDays as $k => $v) {
					?>
					<option value="<?= $k ?>"<? if (in_array($k, $filter_week_day)) { ?> selected="selected"<? } ?>><?= $v ?></option>
					<?
				}
				?>
			</select>
		</td>
	</tr>
	<?
	$oFilter->Buttons(
			array(
				"table_id" => $sTableID,
				"url" => $APPLICATION->GetCurPage(),
				"form" => "find_form"
			)
	);
	$oFilter->End();
	?>
</form>

<?
$lAdmin->DisplayList();
echo BeginNote();
?>
<?= GetMessage("webavk.deliverydate_NOTE") ?>
<?
echo EndNote();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>