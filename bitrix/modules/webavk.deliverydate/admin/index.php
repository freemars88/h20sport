<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webavk.ibcomments/prolog.php");
CModule::IncludeModule('webavk.deliverydate');
IncludeModuleLangFile(__FILE__);

$modulePermissions = $APPLICATION->GetGroupRight("webavk.deliverydate");

if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$APPLICATION->SetTitle(GetMessage('webavk.deliverydate_PAGE_TITLE'));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if ($message)
	echo $message->Show();

$aTabs = array();
$aTabs[] = array(
	'DIV' => 'edit1',
	'TAB' => GetMessage('webavk.deliverydate_TAB_INSTRUCTION_TITLE'),
	//'ICON'=>'ticket_dict_edit',
	'TITLE' => GetMessage('webavk.deliverydate_TAB_INSTRUCTION_DESC')
);
$aTabs[] = array(
	'DIV' => 'edit3',
	'TAB' => GetMessage('webavk.deliverydate_TAB_SUPPORT_TITLE'),
	//'ICON'=>'ticket_dict_edit',
	'TITLE' => GetMessage('webavk.deliverydate_TAB_SUPPORT_DESC')
);
$tabControl = new CAdminTabControl('tabControl', $aTabs);

$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<tr>
	<td>
		<? echo GetMessage("webavk.deliverydate_TAB_INSTRUCTION_CONTENT"); ?>	
	</td>
</tr>
<?
$tabControl->BeginNextTab();
?>
<tr>
	<td>
		<? echo GetMessage("webavk.deliverydate_TAB_SUPPORT_CONTENT"); ?>	
	</td>
</tr>
<?
//$tabControl->Buttons(Array("disabled"=>!$bAdmin, 'back_url' => $LIST_URL . '?lang='.LANGUAGE_ID));
$tabControl->End();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>