<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule('webavk.deliverydate');
IncludeModuleLangFile(__FILE__);
$modulePermissions = $APPLICATION->GetGroupRight("webavk.deliverydate");
if ($modulePermissions < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if (!CModule::IncludeModule('sale')) {
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(GetMessage("webavk.deliverydate_MODULE_SALE_REQUIRED"));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}
if (!CModule::IncludeModule('catalog')) {
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(GetMessage("webavk.deliverydate_MODULE_CATALOG_REQUIRED"));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	die();
}
//CJSCore::Init(array('jquery'));
ClearVars();

$errorMessage = "";
$bVarsFromForm = false;

$ID = IntVal($ID);

$arSaleDeliverys = array();
$arSaleDeliverysRef = array();
$rSaleDelivery = CSaleDelivery::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array());
while ($arSaleDelivery = $rSaleDelivery->Fetch()) {
	$arSaleDeliverys[$arSaleDelivery['ID']] = '(' . $arSaleDelivery['LID'] . ') ' . $arSaleDelivery['NAME'];
	$arSaleDeliverysRef['reference_id'][] = $arSaleDelivery['ID'];
	$arSaleDeliverysRef['reference'][] = "[" . $arSaleDelivery['ID'] . "] " . '(' . $arSaleDelivery['LID'] . ') ' . $arSaleDelivery['NAME'];
}
$rSaleDelivery = CSaleDeliveryHandler::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array());
while ($arSaleDelivery = $rSaleDelivery->Fetch()) {
	if (strlen($arSaleDelivery['LID']) > 0) {
		$arSaleDeliverys[$arSaleDelivery['SID']] = '(' . $arSaleDelivery['LID'] . ') ' . $arSaleDelivery['NAME'];
		$arSaleDeliverysRef['reference_id'][] = $arSaleDelivery['SID'];
		$arSaleDeliverysRef['reference'][] = "[" . $arSaleDelivery['SID'] . "] " . '(' . $arSaleDelivery['LID'] . ') ' . $arSaleDelivery['NAME'];
	} else {
		$arSaleDeliverys[$arSaleDelivery['SID']] = $arSaleDelivery['NAME'];
		$arSaleDeliverysRef['reference_id'][] = $arSaleDelivery['SID'];
		$arSaleDeliverysRef['reference'][] = "[" . $arSaleDelivery['SID'] . "] " . $arSaleDelivery['NAME'];
	}
	foreach ($arSaleDelivery['PROFILES'] as $profileId => $arProfile) {
		$arSaleDeliverys[$arSaleDelivery['SID'] . ":" . $profileId] = $arSaleDelivery['NAME'] . " [" . $arProfile['TITLE'] . "]";
		$arSaleDeliverysRef['reference_id'][] = $arSaleDelivery['SID'] . ":" . $profileId;
		$arSaleDeliverysRef['reference'][] = "[" . $arSaleDelivery['SID'] . ":" . $profileId . "] " . $arSaleDelivery['NAME'] . " [" . $arProfile['TITLE'] . "]";
	}
}

$arSaleStores = array();
$arSaleStoreRef = array();
$rSaleStore = CCatalogStore::GetList(array("SORT" => "ASC", "TITLE" => "ASC"), array());
while ($arSaleStore = $rSaleStore->Fetch()) {
	$arSaleStores[$arSaleStore['ID']] = $arSaleStore['TITLE'];
	$arSaleStoreRef['reference_id'][] = $arSaleStore['ID'];
	$arSaleStoreRef['reference'][] = "[" . $arSaleStore['ID'] . "] " . $arSaleStore['TITLE'];
}

$arWeekDays = array(
	"reference_id" => array("1", "2", "3", "4", "5", "6", "7"),
	"reference" => array(
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_1"),
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_2"),
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_3"),
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_4"),
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_5"),
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_6"),
		GetMessage("webavk.deliverydate_DAYS_WEEKDAY_7")
	),
);

$arMonthDay = array(
	"reference_id" => array(1),
	"reference" => array(GetMessage("webavk.deliverydate_DAYS_MONTHDAY_FIRST"))
);
for ($i = 2; $i <= 31; $i++) {
	$arMonthDay["reference_id"][] = $i;
	$arMonthDay["reference"][] = sprintf("%02d", $i);
}
$arMonthDay["reference_id"][] = 32;
$arMonthDay["reference"][] = GetMessage("webavk.deliverydate_DAYS_MONTHDAY_LAST");

$arTime = array();
$step = COption::GetOptionInt("webavk.deliverydate", "minuteselstep", 15);
for ($h = 0; $h <= 23; $h++) {
	for ($m = 0; $m <= 59; $m = $m + $step) {
		$time = sprintf("%02d:%02d", $h, $m);
		$arTime['reference_id'][] = $time;
		$arTime['reference'][] = $time;
	}
}

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("webavk.deliverydate_DAYS_TAB"), "ICON" => "webavk.deliverydate", "TITLE" => GetMessage("webavk.deliverydate_DAYS_TAB_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $modulePermissions >= "W" && check_bitrix_sessid()) {
	$arFields = array(
		"NAME" => $_REQUEST['NAME'],
		"SORT" => intval($_REQUEST['SORT']) < 1 ? 100 : intval($_REQUEST['SORT']),
		"ACTIVE" => $_REQUEST['ACTIVE'] == "Y" ? "Y" : "N",
		"DELIVERY" => $_REQUEST['DELIVERY'],
		"STORE" => $_REQUEST['STORE'],
		"DELIVERY_DATE" => (strlen(trim($_REQUEST['DELIVERY_DATE'])) > 0 ? new Bitrix\Main\Type\Date(trim($_REQUEST['DELIVERY_DATE'])) : false),
		"WEEK_DAY" => (intval($_REQUEST['WEEK_DAY']) <= 0 ? false : intval($_REQUEST['WEEK_DAY'])),
		"MONTH_DAY" => (intval($_REQUEST['MONTH_DAY']) <= 0 ? false : intval($_REQUEST['MONTH_DAY'])),
		"PERIOD_START" => (strlen(trim($_REQUEST['PERIOD_START'])) > 0 ? new Bitrix\Main\Type\Date(trim($_REQUEST['PERIOD_START'])) : false),
		"PERIOD_END" => (strlen(trim($_REQUEST['PERIOD_END'])) > 0 ? new Bitrix\Main\Type\Date(trim($_REQUEST['PERIOD_END'])) : false),
		"MAX_ORDERS" => intval($_REQUEST['MAX_ORDERS']),
		"TIME_START" => trim($_REQUEST['TIME_START']),
		"TIME_END" => trim($_REQUEST['TIME_END']),
		"TIME_STOP" => trim($_REQUEST['TIME_STOP']),
		"TIME_STOP_DEFAULT" => trim($_REQUEST['TIME_STOP_DEFAULT']),
		"TIME_INTERVALS" => trim($_REQUEST['TIME_INTERVALS']),
	);
	//if (strlen($arFields['DELIVERY_ID']) <= 0 || !isset($arFields['DELIVERY_ID'], $arSaleDeliverys))
	//	$errorMessage .= GetMessage("webavk.deliverydate_DAYS_ERROR_DELIVERY_ID") . ".<br>";
	if (StrLen($errorMessage) <= 0) {
		if ($ID > 0) {
			$ob = new WebAVK\DeliveryDate\Days\DaysTable();
			$oRes = $ob->update($ID, $arFields);
			if (!$oRes->isSuccess()) {
				$errorMessage .= implode("<br>", $oRes->getErrorMessages());
				/*
				  if ($ex = $APPLICATION->GetException())
				  $errorMessage .= $ex->GetString() . ".<br>";
				  else
				  $errorMessage .= GetMessage("webavk.deliverydate_DAYS_ERROR_SAVE") . ".<br>";
				 * 
				 */
			}
		} else {
			$ob = new WebAVK\DeliveryDate\Days\DaysTable();
			$oRes = $ob->add($arFields);
			$ID = false;
			if (!$oRes->isSuccess()) {
				$errorMessage .= implode("<br>", $oRes->getErrorMessages());
			} else {
				$ID = intval($oRes->getId());
			}
			/*
			  $ID = IntVal($ID);
			  if ($ID <= 0) {
			  if ($ex = $APPLICATION->GetException())
			  $errorMessage .= $ex->GetString() . ".<br>";
			  else
			  $errorMessage .= GetMessage("webavk.deliverydate_DAYS_ERROR_SAVE") . ".<br>";
			  }
			 * 
			 */
		}
	}

	if (strlen($errorMessage) <= 0) {
		if (strlen($apply) <= 0)
			LocalRedirect("/bitrix/admin/webavk.deliverydate.days.php?lang=" . LANG . GetFilterParams("filter_", false));
		else
			LocalRedirect("/bitrix/admin/webavk.deliverydate.days.edit.php?lang=" . LANG . "&ID=" . $ID . GetFilterParams("filter_", false));
	}
	else {
		$bVarsFromForm = true;
	}
}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webavk.deliverydate/prolog.php");

if ($ID > 0)
	$APPLICATION->SetTitle(str_replace("#ID#", $ID, GetMessage("webavk.deliverydate_DAYS_UPDATE_TITLE")));
else
	$APPLICATION->SetTitle(GetMessage("webavk.deliverydate_DAYS_ADD_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$dbDeliveryDateDays = WebAVK\DeliveryDate\Days\DaysTable::getById($ID);
$arDeliveryDateDays = $dbDeliveryDateDays->Fetch();

if (!$arDeliveryDateDays) {
	$ID = 0;
	$str_ACTIVE = "Y";
	$str_SORT = 100;
	$str_TIME_STOP = "23:45";
	$str_TIME_STOP_DEFAULT = "23:45";
} else {
	$strPrefix = "str_";
	foreach ($arDeliveryDateDays as $key => $val) {
		$varname = $strPrefix . $key;
		global $$varname;

		if (!is_array($val) && !is_object($val)) {
			$$varname = htmlspecialcharsEx($val);
		} else {
			$$varname = $val;
		}
	}
}
if ($ID > 0) {
	$arDELIVERY_ID = array();
	$rD = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
				"select" => array("DELIVERY.DELIVERY_ID"),
				"filter" => array("ID" => $ID)
	));
	while ($arD = $rD->Fetch()) {
		if (intval($arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID']) > 0) {
			$arDELIVERY_ID[] = $arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID'];
		} elseif (strlen($arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID']) > 0) {
			$arDELIVERY_ID[] = $arD['WEBAVK_DELIVERYDATE_DAYS_DAYS_DELIVERY_DELIVERY_ID'];
		}
	}
	$arSTORE_ID = array();
	$rS = WebAVK\DeliveryDate\Days\DaysTable::getList(array(
				"select" => array("STORE.STORE_ID"),
				"filter" => array("ID" => $ID)
	));
	while ($arS = $rS->Fetch()) {
		if (intval($arS['WEBAVK_DELIVERYDATE_DAYS_DAYS_STORE_STORE_ID']) > 0) {
			$arSTORE_ID[] = $arS['WEBAVK_DELIVERYDATE_DAYS_DAYS_STORE_STORE_ID'];
		}
	}
}
/*
  if (strlen($str_DELIVERY_DATE) > 0) {
  $str_DELIVERY_DATE = ConvertTimeStamp(MakeTimeStamp($str_DELIVERY_DATE, "YYYY-MM-DD"), FORMAT_DATE);
  }
  if (strlen($str_PERIOD_START) > 0) {
  $str_PERIOD_START = ConvertTimeStamp(MakeTimeStamp($str_PERIOD_START, "YYYY-MM-DD"), FORMAT_DATE);
  }
  if (strlen($str_PERIOD_END) > 0) {
  $str_PERIOD_END = ConvertTimeStamp(MakeTimeStamp($str_PERIOD_END, "YYYY-MM-DD"), FORMAT_DATE);
  }
 */
if ($bVarsFromForm) {
	$DB->InitTableVarsForEdit("wadd_days", "", "str_");
	$arDELIVERY_ID = $_REQUEST['DELIVERY'];
	$arSTORE_ID = $_REQUEST['STORE'];
}
?>

<?
$aMenu = array(
	array(
		"TEXT" => GetMessage("webavk.deliverydate_DAYS_LIST"),
		"LINK" => "/bitrix/admin/webavk.deliverydate.days.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_list"
	)
);

if ($ID > 0) {
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => GetMessage("webavk.deliverydate_DAYS_ADD"),
		"LINK" => "/bitrix/admin/webavk.deliverydate.days.edit.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_new"
	);

	if ($modulePermissions >= "W") {
		$aMenu[] = array(
			"TEXT" => GetMessage("webavk.deliverydate_DAYS_DELETE"),
			"LINK" => "javascript:if(confirm('" . GetMessage("webavk.deliverydate_DAYS_DELETE_CONFIRM") . "')) window.location='/bitrix/admin/webavk.deliverydate.days.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "#tb';",
			"WARNING" => "Y",
			"ICON" => "btn_delete"
		);
	}
}
$context = new CAdminContextMenu($aMenu);
$context->Show();
?>

<?
if (strlen($errorMessage) > 0)
	echo CAdminMessage::ShowMessage(Array("DETAILS" => $errorMessage, "TYPE" => "ERROR", "MESSAGE" => GetMessage("webavk.deliverydate_DAYS_SAVE_ERROR"), "HTML" => true));
?>
<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?" name="form1">
	<? echo GetFilterHiddens("filter_"); ?>
	<input type="hidden" name="Update" value="Y">
	<input type="hidden" name="lang" value="<? echo LANG ?>">
	<input type="hidden" name="ID" value="<? echo $ID ?>">
	<?= bitrix_sessid_post() ?>
	<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
	?>
	<? if ($ID > 0): ?>
		<tr>
			<td width="40%">ID:</td>
			<td width="60%"><?= $ID ?></td>
		</tr>
	<? endif; ?>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_NAME") ?></td>
		<td>
			<input type="text" name="NAME" size="40" maxlength="255" value="<?= $str_NAME ?>">
		</td>
	</tr>
	<tr>
		<td width="40%"><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_SORT") ?></td>
		<td width="60%">
			<input type="text" name="SORT" size="10" maxlength="15" value="<?= $str_SORT ?>">
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_ACTIVE") ?></td>
		<td>
			<input type="checkbox" name="ACTIVE" value="Y"<? if ($str_ACTIVE == "Y") echo " checked" ?>>
		</td>
	</tr>
	<tr class="adm-detail-required-field">
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_DELIVERY_ID") ?></td>
		<td>
			<?= SelectBoxMFromArray("DELIVERY[]", $arSaleDeliverysRef, $arDELIVERY_ID, GetMessage("webavk.deliverydate_DAYS_FIELD_DELIVERY_ID_NULL"), false, 8, "class='typeselect' style='width:400px;'") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_STORE_ID") ?></td>
		<td>
			<?= SelectBoxMFromArray("STORE[]", $arSaleStoreRef, $arSTORE_ID, GetMessage("webavk.deliverydate_DAYS_FIELD_STORE_ID_NULL"), false, 8, "class='typeselect' style='width:400px;'") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_DELIVERY_DATE") ?></td>
		<td>
			<?= CalendarDate("DELIVERY_DATE", $str_DELIVERY_DATE, "form1") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_WEEK_DAY") ?></td>
		<td>
			<?= SelectBoxFromArray("WEEK_DAY", $arWeekDays, $str_WEEK_DAY, GetMessage("webavk.deliverydate_DAYS_FIELD_WEEK_DAY_NULL")) ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_MONTH_DAY") ?></td>
		<td>
			<?= SelectBoxFromArray("MONTH_DAY", $arMonthDay, $str_MONTH_DAY, GetMessage("webavk.deliverydate_DAYS_FIELD_MONTH_DAY_NULL")) ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_DELIVERY_DATE") ?></td>
		<td>
			<?= CalendarPeriod("PERIOD_START", $str_PERIOD_START, "PERIOD_END", $str_PERIOD_END, "form1") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_TIME_INTERVALS") ?></td>
		<td>
			<textarea name="TIME_INTERVALS" cols="40" rows="6"><?= $str_TIME_INTERVALS ?></textarea>
		</td>
	</tr>
	<tr class="heading">
		<td colspan="2"><? echo GetMessage("webavk.deliverydate_DAYS_LOGISTIC") ?></td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_MAX_ORDERS") ?></td>
		<td>
			<input type="text" name="MAX_ORDERS" size="6" maxlength="6" value="<?= $str_MAX_ORDERS ?>" />
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_TIME_START") ?></td>
		<td>
			<?= SelectBoxFromArray("TIME_START", $arTime, $str_TIME_START, "") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_TIME_END") ?></td>
		<td>
			<?= SelectBoxFromArray("TIME_END", $arTime, $str_TIME_END, "") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_TIME_STOP") ?></td>
		<td>
			<?= SelectBoxFromArray("TIME_STOP", $arTime, $str_TIME_STOP, "") ?>
		</td>
	</tr>
	<tr>
		<td><? echo GetMessage("webavk.deliverydate_DAYS_FIELD_TIME_STOP_DEFAULT") ?></td>
		<td>
			<?= SelectBoxFromArray("TIME_STOP_DEFAULT", $arTime, $str_TIME_STOP_DEFAULT, "") ?>
		</td>
	</tr>
	<?
	$tabControl->EndTab();
	?>

	<?
	$tabControl->Buttons(
			array(
				"disabled" => ($modulePermissions < "W"),
				"back_url" => "/bitrix/admin/webavk.deliverydate.days.php?lang=" . LANG . GetFilterParams("filter_")
			)
	);
	?>

	<?
	$tabControl->End();
	?>

</form>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>