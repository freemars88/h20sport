<?

IncludeModuleLangFile(__FILE__);

Class CWebavkTmplpro {

	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu) {
		return;
		$MODULE_ID = basename(dirname(__FILE__));
		$aMenu = array(
			//"parent_menu" => "global_menu_services",
			"parent_menu" => "global_menu_settings",
			"section" => $MODULE_ID,
			"sort" => 2000,
			"text" => GetMessage($MODULE_ID . "_MENU_INDEX_TITLE"),
			"title" => GetMessage($MODULE_ID . "_MENU_INDEX_TITLE"),
			"url" => $MODULE_ID . '.index.php',
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID . "_items",
			"more_url" => array(),
			"items" => array()
		);
		if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '.index.php'))
			file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/index.php");?' . '>');
		if (file_exists($path = dirname(__FILE__) . '/admin')) {
			if ($dir = opendir($path)) {
				$arFiles = array();

				while (false !== $item = readdir($dir)) {
					if (in_array($item, array('.', '..', 'menu.php'/* , 'index.php' */)))
						continue;
					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '.' . $item))
						file_put_contents($file, '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/' . $item . '");?' . '>');
					if (strpos($item, ".edit.php") !== false)
						continue;

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach ($arFiles as $item) {
					$aMenu['items'][] = array(
						'text' => GetMessage($MODULE_ID . "_MENU_" . $item . "_TITLE"),
						'url' => $MODULE_ID . '.' . $item,
						"more_url" => Array(str_replace(".php", ".edit.php", $MODULE_ID . '.' . $item)),
						'module_id' => $MODULE_ID,
						"title" => ""
					);
				}
			}
		}
		$aModuleMenu[] = $aMenu;
	}

}

CModule::AddAutoloadClasses(
		"webavk.tmplpro", array(
	"CWebavkTmplProTools" => "classes/general/tools.php",
	"CWebavkTmplProJSCore" => "classes/general/jscore.php",
		)
);
?>