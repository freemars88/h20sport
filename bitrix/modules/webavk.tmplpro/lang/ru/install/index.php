<?
$MESS["webavk.tmplpro_MODULE_NAME"] = "WebAVK. Шаблоны Pro";
$MESS["webavk.tmplpro_MODULE_DESC"] = "Модуль WebAVK: Шаблоны Pro позволяет использовать облегченное написание шаблонов с единым (едиными) файлами стилями, с layout страниц и различными цветовыми решениями страниц сайта без создания для этих целей отдельных шаблонов сайта.";
$MESS["webavk.tmplpro_PARTNER_NAME"] = "WebAVK - решения для 1С-Битрикс";
$MESS["webavk.tmplpro_PARTNER_URI"] = "http://www.web-avk.ru";
?>