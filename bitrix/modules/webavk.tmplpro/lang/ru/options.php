<?

$MESS['webavk.tmplpro_PAGE_TITLE'] = "Модуль WebAVK: Шаблоны Pro";
$MESS['webavk.tmplpro_TAB_INSTRUCTION_TITLE'] = "Инструкция";
$MESS['webavk.tmplpro_TAB_INSTRUCTION_DESC'] = "Инструкция по использованию модуля";
$MESS['webavk.tmplpro_TAB_INSTRUCTION_CONTENT'] = '
<p>Модуль WebAVK: Шаблоны Pro позволяет создавать новые шаблоны сайта с использованием единого набора файлов стилей, JS-скриптов и нескольких вариантов разметок страниц.</p>
<p>При установке модуля в системной папке /bitrix/templates/ создается новый пустой демонстрационный шаблон webavk_tmplpro_demo с базовыми настройками и файлами. Скопируйте его и разрабатывайте свои собственные шаблоны.</p>
<h3>Структура основных файлов шаблона</h3>
<table class="adm-list-table">
	<thead>
		<tr class="adm-list-table-header">
			<td class="adm-list-table-cell"><div class="adm-list-table-cell-inner">Файл или каталог</div></td>
			<td class="adm-list-table-cell"><div class="adm-list-table-cell-inner">Описание</div></td>
		</tr>
	</thead>
	<tbody>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">block/</td>
			<td class="adm-list-table-cell">Каталог подключаемых блоков шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">css/</td>
			<td class="adm-list-table-cell">Каталог стилей шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;01-reset.css</td>
			<td class="adm-list-table-cell">Сброс стилей</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;02-styles.css</td>
			<td class="adm-list-table-cell">Основной файл стилей шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">js/</td>
			<td class="adm-list-table-cell">Каталог JavaScript скриптов шаблона сайта</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;99-script.js</td>
			<td class="adm-list-table-cell">Файл JavaScript для сайта</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">layout/</td>
			<td class="adm-list-table-cell">Каталог разметок шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include.php</td>
			<td class="adm-list-table-cell">Основной файл для выполнения основных операций по выбору необходимой разметки шаблона для текущей страницы</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.default/</td>
			<td class="adm-list-table-cell">Каталог разметки страницы по умолчанию</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include.php</td>
			<td class="adm-list-table-cell">Основной файл разметки шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mainpage/</td>
			<td class="adm-list-table-cell">Каталог разметки главной страницы</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include.php</td>
			<td class="adm-list-table-cell">Основной файл разметки шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">lib/</td>
			<td class="adm-list-table-cell">Основной каталог подключаемых библиотек</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;01-jquery/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;02-jquery.cookie/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;03-jquery.mousewhell/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;04-jquery.form/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;05-jquery.mockajax/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;06-jquery.validate/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;07-jquery.timers/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;08-jquery.fancybox/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;09-jquery.placeholder/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10-jquery.uniform/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11-jquery.scrollto/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12-jquery.rating/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;13-jquery.slideviewerpro/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14-jquery.jcarousel/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15-jquery.jscrollpane/<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16-jqueryui/
			</td>
			<td class="adm-list-table-cell">Каталоги автоматически подключаемых библиотек</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">header.php</td>
			<td class="adm-list-table-cell">Системный файл для интеграции шаблона с требованиями 1С-Битрикс к шаблонам сайтов</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">footer.php</td>
			<td class="adm-list-table-cell">Системный файл для интеграции шаблона с требованиями 1С-Битрикс к шаблонам сайтов</td>
		</tr>
	</tbody>
</table>
<h3>Описание функций класса CWebavkTmplProTools</h3>
<table class="adm-list-table">
	<thead>
		<tr class="adm-list-table-header">
			<td class="adm-list-table-cell"><div class="adm-list-table-cell-inner">Функция</div></td>
			<td class="adm-list-table-cell"><div class="adm-list-table-cell-inner">Параметр</div></td>
			<td class="adm-list-table-cell"><div class="adm-list-table-cell-inner">Описание</div></td>
		</tr>
	</thead>
	<tbody>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell" rowspan="3">InitLibFromDir($strDir = false, $autoLoad = true)</td>
			<td class="adm-list-table-cell"></td>
			<td class="adm-list-table-cell">Инициализация и подключение библиотек шаблона (из каталога lib/)</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$strDir</td>
			<td class="adm-list-table-cell">Каталог, в котором происходит поиск библиотек. По умолчанию поиск происходит в подкаталоге lib/ текущего шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$autoLoad</td>
			<td class="adm-list-table-cell">Инициализировать и загружать библиотеки:<br/>
				<strong>true</strong> - загружать все<br/>
				<strong>false</strong> - не загружать<br/>
				<strong>array(lib1, lib2, etc)</strong> - загружать автоматически только указанные библиотеки. В этом случае для загрузки нужно библиотеки необходимо дополнительно вызывать функцию класса <strong>CWebavkTmplProJSCore::Init(array(lib1,lib2,etc))</strong>, и в качестве параметра передавать массив библиотек, которые необходимо загрузить.
			</td>
		</tr>
		
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell" rowspan="3">IncludeCSSFromDir($strDir = false, $subDirAfter = false)</td>
			<td class="adm-list-table-cell"></td>
			<td class="adm-list-table-cell">Инициализация и подключение стилей шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$strDir</td>
			<td class="adm-list-table-cell">Каталог, в котором происходит поиск файлов стилей. По умолчанию поиск происходит в подкаталоге css/ текущего шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$subDirAfter</td>
			<td class="adm-list-table-cell">Подкаталог каталога $strDir, из которого подключать дополнительные файлы стилей. Можно использовать для создания скинов, либо корректировок шаблона в зависимости от сайта</td>
		</tr>
		
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell" rowspan="3">IncludeJSFromDir($strDir = false, $subDirAfter = false)</td>
			<td class="adm-list-table-cell"></td>
			<td class="adm-list-table-cell">Инициализация и подключение Java-скриптов шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$strDir</td>
			<td class="adm-list-table-cell">Каталог, в котором происходит поиск файлов Java-скриптов. По умолчанию поиск происходит в подкаталоге js/ текущего шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$subDirAfter</td>
			<td class="adm-list-table-cell">Подкаталог каталога $strDir, из которого подключать дополнительные файлы Java-скриптов</td>
		</tr>
		
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell" rowspan="4">ShowPropertyTemplate($PROPERTY_ID, $default_value = false, $TEMPLATE = "#VALUE#")</td>
			<td class="adm-list-table-cell"></td>
			<td class="adm-list-table-cell">Показ свойства страницы по шаблону отображения.<br/>
			Можно использовать например для вывода заголовка H1, если необходимо избежать вывода пустого тега h1 без значения.<br/>
			В этом случае в нужном месте страницы (шаблона) необходимо вызвать функцию:<br/>
			<strong>CWebavkTmplProTools::ShowPropertyTemplate("h1_title", "", "&LT;h1>#VALUE#&LT;/h1>");</strong>
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$PROPERTY_ID</td>
			<td class="adm-list-table-cell">Код свойства для вывода</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$default_value</td>
			<td class="adm-list-table-cell">Значение по умолчанию</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$TEMPLATE</td>
			<td class="adm-list-table-cell">Шаблон отображения свойства. Место вывода значения свойства в шаблоне определяется конструкцией #VALUE#</td>
		</tr>
		
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell" rowspan="4">IncludeBlockFromDir($blockName, $strDir = false, $subDir = false)</td>
			<td class="adm-list-table-cell"></td>
			<td class="adm-list-table-cell">
			Простое подключение включаемых файлов шаблона. Удобно использовать для (например) выделения в блоки шапки сайта, подвала сайта, левого блока, правого блока, блока пользователя и т.д.
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$blockName</td>
			<td class="adm-list-table-cell">Имя блока (без расширения типа файла). Будет подключен блок из выбранного каталога с полным именем $blockName.php</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$strDir</td>
			<td class="adm-list-table-cell">Каталог, в котором происходит поиск файлов включаемых блоков. По умолчанию поиск происходит в подкаталоге block/ текущего шаблона</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">$subDir</td>
			<td class="adm-list-table-cell">Подкаталог каталога $strDir, в котором в ПЕРВУЮ очередь происходит поиск включаемых блоков</td>
		</tr>
	</tbody>
</table>
';
$MESS['webavk.tmplpro_TAB_SUPPORT_TITLE'] = "Техподдержка, развитие модуля";
$MESS['webavk.tmplpro_TAB_SUPPORT_DESC'] = "Техническая поддержка и пожелания по функционалу модуля webavk.ibcomments";
$MESS['webavk.tmplpro_TAB_SUPPORT_CONTENT'] = '
	<h3>Техническая поддержка</h3>
	<p>Техническая поддержка модуля осуществляется по электронной почте</p>
	<h3>Развитие модуля, новый функционал</h3>
	<p>Если вы обнаружили что какого-то функционала модуля не хватает лично для вас - напишите нам. Функционал будет добавлен.</p>
	<hr/>
	<h3>Наши контакты:</h3>
	<p>Электронная почта: <a href="mailto:support@web-avk.ru">support@web-avk.ru</a></p>
	<p>Skype: <a href="skype:web.avk?chat"><img src="http://mystatus.skype.com/smallicon/web.avk?' . time() . '" style="border: none;"> Skype</a></p>
	<div style="clear:both;"></div>
';
?>