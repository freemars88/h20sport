<?
$module_id = "webavk.tmplpro";
$modulePermissions = $APPLICATION->GetGroupRight($module_id);
if ($modulePermissions >= "R") {

	global $MESS;
	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/main/lang/", "/options.php"));
	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/webavk.tmplpro/lang/", "/options.php"));

	CModule::IncludeModule("iblock");
	CModule::IncludeModule("webavk.tmplpro");


	$aTabs = array();
	$aTabs[] = array(
		'DIV' => 'edit1',
		'TAB' => GetMessage('webavk.tmplpro_TAB_INSTRUCTION_TITLE'),
		//'ICON'=>'ticket_dict_edit',
		'TITLE' => GetMessage('webavk.tmplpro_TAB_INSTRUCTION_DESC')
	);
	$aTabs[] = array(
		'DIV' => 'edit3',
		'TAB' => GetMessage('webavk.tmplpro_TAB_SUPPORT_TITLE'),
		//'ICON'=>'ticket_dict_edit',
		'TITLE' => GetMessage('webavk.tmplpro_TAB_SUPPORT_DESC')
	);
	$tabControl = new CAdminTabControl('tabControl', $aTabs);

	$tabControl->Begin();
	$tabControl->BeginNextTab();
	?>
	<tr>
		<td>
			<? echo GetMessage("webavk.tmplpro_TAB_INSTRUCTION_CONTENT"); ?>	
		</td>
	</tr>
	<?
	$tabControl->BeginNextTab();
	?>
	<tr>
		<td>
			<? echo GetMessage("webavk.tmplpro_TAB_SUPPORT_CONTENT"); ?>	
		</td>
	</tr>
	<?
//$tabControl->Buttons(Array("disabled"=>!$bAdmin, 'back_url' => $LIST_URL . '?lang='.LANGUAGE_ID));
	$tabControl->End();
}
?>
