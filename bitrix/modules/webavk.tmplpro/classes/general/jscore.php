<?

class CWebavkTmplProJSCore {

	const USE_ADMIN = 'admin';
	const USE_PUBLIC = 'public';

	private static $arRegisteredExt = array();
	private static $arCurrentlyLoadedExt = array();
	private static $bInited = false;

	/*
	  ex: CWebavkTmplProJSCore::RegisterExt('timeman', array(
	  'js' => '/bitrix/js/timeman/core_timeman.js',
	  'css' => '/bitrix/js/timeman/css/core_timeman.css',
	  'lang' => '/bitrix/modules/timeman/lang/#LANG#/js_core_timeman.php',
	  'rel' => array(needed extensions for automatic inclusion),
	  'use' => CJSCore::USE_ADMIN|CJSCore::USE_PUBLIC
	  ));
	 */

	public static function RegisterExt($name, $arPaths) {
		if (isset($arPaths['use'])) {
			switch ($arPaths['use']) {
				case CJSCore::USE_PUBLIC:
					if (defined("ADMIN_SECTION") && ADMIN_SECTION === true)
						return;

					break;
				case CJSCore::USE_ADMIN:
					if (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
						return;

					break;
			}
		}

		self::$arRegisteredExt[$name] = $arPaths;
	}

	public static function Init($arExt = array(), $bReturn = false) {
		global $USER;

		if (!self::$bInited) {
			self::$bInited = true;
		}
		if (!is_array($arExt) && strlen($arExt) > 0)
			$arExt = array($arExt);

		$bReturn = ($bReturn === true); // prevent syntax mistake

		$bNeedCore = false;
		if (count($arExt) > 0) {
			foreach ($arExt as $ext) {
				if (
						self::$arRegisteredExt[$ext]
						&& (
						!isset(self::$arRegisteredExt[$ext]['skip_core'])
						|| !self::$arRegisteredExt[$ext]['skip_core']
						)
				) {
					$bNeedCore = true;
					break;
				}
			}
		} else {
			$bNeedCore = true;
		}

		$ret = '';
		if ($bNeedCore && !self::$arCurrentlyLoadedExt['core']) {
			$autoTimeZone = "N";
			if (is_object($GLOBALS["USER"]))
				$autoTimeZone = trim($USER->GetParam("AUTO_TIME_ZONE"));

			$arLang = array(
				'LANGUAGE_ID' => LANGUAGE_ID,
				'FORMAT_DATE' => FORMAT_DATE,
				'FORMAT_DATETIME' => FORMAT_DATETIME,
				'COOKIE_PREFIX' => COption::GetOptionString("main", "cookie_name", "BITRIX_SM"),
				'USER_ID' => $USER->GetID(),
				'SERVER_TIME' => time(),
				'SERVER_TZ_OFFSET' => date("Z"),
				'USER_TZ_OFFSET' => CTimeZone::GetOffset(),
				'USER_TZ_AUTO' => $autoTimeZone == 'N' ? 'N' : 'Y',
				'bitrix_sessid' => bitrix_sessid(),
			);
			if (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
				$arLang["SITE_ID"] = SITE_ID;

			$ret .= self::_loadCSS('/bitrix/js/main/core/css/core.css', $bReturn);
			$ret .= self::_loadJS('/bitrix/js/main/core/core.js', $bReturn);
			$ret .= self::_loadLang(BX_ROOT . '/modules/main/lang/' . LANGUAGE_ID . '/js_core.php', $bReturn, $arLang);

			self::$arCurrentlyLoadedExt['core'] = true;
		}

		for ($i = 0, $len = count($arExt); $i < $len; $i++) {
			$ret .= self::_loadExt($arExt[$i], $bReturn);
		}

		if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
			echo $ret;
		return $bReturn ? $ret : true;
	}

	public static function GetHTML($arExt) {
		$tmp = self::$arCurrentlyLoadedExt;
		self::$arCurrentlyLoadedExt = array();
		$res = self::Init($arExt, true);
		self::$arCurrentlyLoadedExt = $tmp;
		return $res;
	}

	public static function GetScriptsList() {
		$scriptsList = array();
		foreach (self::$arCurrentlyLoadedExt as $ext => $q) {
			if ($ext != 'core')
				$scriptsList[] = self::$arRegisteredExt[$ext]['js'];
		}
		return $scriptsList;
	}

	private function _loadExt($ext, $bReturn) {
		$ret = '';

		$ext = preg_replace('/[^a-z0-9_\.]/i', '', $ext);
		if (
				!self::IsExtRegistered($ext)
				|| (
				isset(self::$arCurrentlyLoadedExt[$ext])
				&& self::$arCurrentlyLoadedExt[$ext]
				)
		)
			return '';

		self::$arCurrentlyLoadedExt[$ext] = true;

		if (is_array(self::$arRegisteredExt[$ext]['rel'])) {
			foreach (self::$arRegisteredExt[$ext]['rel'] as $rel_ext) {
				if (self::IsExtRegistered($rel_ext) && !self::$arCurrentlyLoadedExt[$rel_ext]) {
					$ret .= self::_loadExt($rel_ext, $bReturn);
				}
			}
		}

		if (self::$arRegisteredExt[$ext]['css'])
			$ret .= self::_loadCSS(self::$arRegisteredExt[$ext]['css'], $bReturn);
		if (self::$arRegisteredExt[$ext]['js'])
			$ret .= self::_loadJS(self::$arRegisteredExt[$ext]['js'], $bReturn);
		if (self::$arRegisteredExt[$ext]['lang'] || self::$arRegisteredExt[$ext]['lang_additional'])
			$ret .= self::_loadLang(
							self::$arRegisteredExt[$ext]['lang'], $bReturn, isset(self::$arRegisteredExt[$ext]['lang_additional']) ? self::$arRegisteredExt[$ext]['lang_additional'] : false
			);

		return $ret;
	}

	public static function ShowTimer($params) {
		$id = $params['id'] ? $params['id'] : 'timer_' . RandString(7);

		self::Init(array('timer'));

		$arJSParams = array();
		if ($params['from'])
			$arJSParams['from'] = MakeTimeStamp($params['from']) . '000';
		elseif ($params['to'])
			$arJSParams['to'] = MakeTimeStamp($params['to']) . '000';

		if ($params['accuracy'])
			$arJSParams['accuracy'] = intval($params['accuracy']) . '000';

		$res = '<span id="' . htmlspecialcharsbx($id) . '"></span>';
		$res .= '<script type="text/javascript">BX.timer(\'' . CUtil::JSEscape($id) . '\', ' . CUtil::PhpToJSObject($arJSParams) . ')</script>';

		return $res;
	}

	public static function IsExtRegistered($ext) {
		$ext = preg_replace('/[^a-z0-9_\.]/i', '', $ext);
		return is_array(self::$arRegisteredExt[$ext]);
	}

	public static function getExtInfo($ext) {
		return self::$arRegisteredExt[$ext];
	}

	private function _RegisterStandardExt() {
		require_once($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/jscore.php');
	}

	private static function _loadJS($js, $bReturn) {
		/** @global CMain $APPLICATION */
		global $APPLICATION;

		if (is_array($js)) {
			$ret = '';
			foreach ($js as $js_file)
				$ret .= self::_loadJS($js_file, $bReturn);
			return $ret;
		}

		if ($bReturn)
			return '<script type="text/javascript" src="' . CUtil::GetAdditionalFileURL($js) . '"></script>' . "\r\n";
		else {
			if ($APPLICATION->IsJSOptimized())
				$APPLICATION->AddHeadScript($js);
			else
				$APPLICATION->AddHeadString('<script type="text/javascript" src="' . CUtil::GetAdditionalFileURL($js) . '"></script>', true);
		}
		return '';
	}

	private static function _loadLang($lang, $bReturn, $arAdditionalMess = false) {
		/** @global CMain $APPLICATION */
		global $APPLICATION;
		$jsMsg = '';

		if ($lang) {
			$lang_filename = $_SERVER['DOCUMENT_ROOT'] . $lang;
			if (file_exists($lang_filename)) {
				$mess_lang = __IncludeLang($lang_filename, true, true);
				if (!empty($mess_lang)) {
					$jsMsg = '(window.BX||top.BX).message(' . CUtil::PhpToJSObject($mess_lang, false) . ');';
				}
			}
		}

		if (is_array($arAdditionalMess))
			$jsMsg = '(window.BX||top.BX).message(' . CUtil::PhpToJSObject($arAdditionalMess, false) . ');' . $jsMsg;

		if ($jsMsg !== '') {
			$jsMsg = '<script type="text/javascript">' . $jsMsg . '</script>';
			if ($bReturn)
				return $jsMsg . "\r\n";
			elseif ($APPLICATION->IsJSOptimized())
				$APPLICATION->AddLangJS($jsMsg);
			else
				$APPLICATION->AddHeadString($jsMsg, true);
		}

		return $jsMsg;
	}

	private static function _loadCSS($css, $bReturn) {
		/** @global CMain $APPLICATION */
		global $APPLICATION;

		if (is_array($css)) {
			$ret = '';
			foreach ($css as $css_file)
				$ret .= self::_loadCSS($css_file, $bReturn);
			return $ret;
		}

		$css_filename = $_SERVER['DOCUMENT_ROOT'] . $css;

		if (!file_exists($css_filename))
			return '';

		if ($bReturn)
			return '<link href="' . CUtil::GetAdditionalFileURL($css) . '" type="text/css" rel="stylesheet" />' . "\r\n";
		else
			$APPLICATION->SetAdditionalCSS($css);

		return '';
	}

}

?>