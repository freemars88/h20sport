<?

class CWebavkTmplProTools
{

	function InitLibFromDir($strDir = false, $autoLoad = true)
	{
		if (!$strDir)
			$strDir = SITE_TEMPLATE_PATH . "/lib/";
		$arFiles = scandir($_SERVER['DOCUMENT_ROOT'] . $strDir);
		asort($arFiles);
		$arExt = array();
		$arInit = array();
		foreach ($arFiles as $fileName)
		{
			if (in_array($fileName, array(".", "..")))
				continue;
			$extName = explode("-", $fileName);
			if (preg_match('/^[\d]*$/i', $extName[0]))
			{
				unset($extName[0]);
			}
			$extName = implode("-", $extName);
			if (strlen($extName) > 0)
			{
				$arExtFiles = scandir($_SERVER['DOCUMENT_ROOT'] . $strDir . "/" . $fileName);
				asort($arExtFiles);
				$arCurrentExt = array();
				foreach ($arExtFiles as $extFile)
				{
					if (in_array($extFile, array(".", "..")))
						continue;
					$relName = $strDir . "/" . $fileName . "/" . $extFile;
					$fullName = $_SERVER['DOCUMENT_ROOT'] . $relName;
					$arPath = pathinfo($fullName);
					if (strtolower($arPath['extension'] == "js"))
					{
						$arCurrentExt['js'][] = $relName;
					} elseif (strtolower($arPath['extension'] == "css"))
					{
						$arCurrentExt['css'][] = $relName;
					}
				}
				CWebavkTmplProJSCore::RegisterExt($extName, $arCurrentExt);
				if ($autoLoad === true || (is_array($autoLoad) && in_array($extName, $autoLoad)))
				{
					$arInit[] = $extName;
				}
			}
		}
		if (count($arInit) > 0)
		{
			CWebavkTmplProJSCore::Init($arInit);
		}
	}

	function IncludeCSSFromDir($strDir = false, $subDirAfter = false)
	{
		global $APPLICATION;
		if (!$strDir)
			$strDir = SITE_TEMPLATE_PATH . "/css/";
		$arFiles = scandir($_SERVER['DOCUMENT_ROOT'] . $strDir);
		asort($arFiles);
		foreach ($arFiles as $strFile)
		{
			if (in_array($strFile, array(".", "..")))
				continue;
			$arFile = pathinfo($_SERVER['DOCUMENT_ROOT'] . $strDir . "/" . $strFile);
			if (strtolower($arFile['extension']) == "css")
			{
				$APPLICATION->SetAdditionalCSS($strDir . $strFile);
			}
		}
		if ($subDirAfter !== false)
		{
			$strDir = $strDir . $subDirAfter . "/";
			$arFiles = scandir($_SERVER['DOCUMENT_ROOT'] . $strDir);
			asort($arFiles);
			foreach ($arFiles as $strFile)
			{
				if (in_array($strFile, array(".", "..")))
					continue;
				$arFile = pathinfo($_SERVER['DOCUMENT_ROOT'] . $strDir . "/" . $strFile);
				if (strtolower($arFile['extension']) == "css")
				{
					$APPLICATION->SetAdditionalCSS($strDir . $strFile);
				}
			}
		}
	}

	function IncludeJSFromDir($strDir = false, $subDirAfter = false)
	{
		global $APPLICATION;
		if (!$strDir)
			$strDir = SITE_TEMPLATE_PATH . "/js/";
		$arFiles = scandir($_SERVER['DOCUMENT_ROOT'] . $strDir);
		asort($arFiles);
		foreach ($arFiles as $strFile)
		{
			if (in_array($strFile, array(".", "..")))
				continue;
			$arFile = pathinfo($_SERVER['DOCUMENT_ROOT'] . $strDir . "/" . $strFile);
			if (strtolower($arFile['extension']) == "js")
			{
				$APPLICATION->AddHeadScript($strDir . $strFile);
			}
		}
		if ($subDirAfter !== false)
		{
			$strDir = $strDir . $subDirAfter . "/";
			$arFiles = scandir($_SERVER['DOCUMENT_ROOT'] . $strDir);
			asort($arFiles);
			foreach ($arFiles as $strFile)
			{
				if (in_array($strFile, array(".", "..")))
					continue;
				$arFile = pathinfo($_SERVER['DOCUMENT_ROOT'] . $strDir . "/" . $strFile);
				if (strtolower($arFile['extension']) == "js")
				{
					$APPLICATION->AddHeadScript($strDir . $strFile);
				}
			}
		}
	}

	function ShowPropertyTemplate($PROPERTY_ID, $default_value = false, $TEMPLATE = '#VALUE#')
	{
		global $APPLICATION;
		$APPLICATION->AddBufferContent("CWebavkTmplProTools::GetPropertyTemplate", $PROPERTY_ID, $default_value, $TEMPLATE);
	}

	function GetPropertyTemplate($PROPERTY_ID, $default_value = false, $TEMPLATE = '#VALUE#')
	{
		global $APPLICATION;
		$propValue = $APPLICATION->GetProperty($PROPERTY_ID, $default_value);
		if (strlen($propValue) > 0)
		{
			return str_replace("#VALUE#", $propValue, $TEMPLATE);
		}
		return $default_value;
	}

	function IncludeBlockFromDir($blockName, $strDir = false, $subDir = false)
	{
		global $APPLICATION, $USER;
		if (!$strDir)
			$strDir = SITE_TEMPLATE_PATH . "/block/";
		if ($subDir !== false)
		{
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strDir . $subDir . "/" . $blockName . ".php"))
			{
				$strDir .= '.' . $strDir;
			}
		}

		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $strDir . $blockName . ".php"))
		{
			$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => $strDir . $blockName . ".php",
				"EDIT_TEMPLATE" => ""
					), false
			);
		}
	}

	function doAddJSFile($strFileName, $bFindMin = true)
	{
		global $APPLICATION;
		if ($bFindMin)
		{
			$arPathInfo = pathinfo($strFileName);
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $arPathInfo['dirname'] . "/" . $arPathInfo['filename'] . ".min." . $arPathInfo['extension']) && filesize($_SERVER['DOCUMENT_ROOT'] . $arPathInfo['dirname'] . "/" . $arPathInfo['filename'] . ".min." . $arPathInfo['extension']) > 0)
			{
				$APPLICATION->AddHeadScript($arPathInfo['dirname'] . "/" . $arPathInfo['filename'] . ".min." . $arPathInfo['extension']);
			} else
			{
				$APPLICATION->AddHeadScript($strFileName);
			}
		} else
		{
			$APPLICATION->AddHeadScript($strFileName);
		}
	}

	function doAddCSSFile($strFileName, $bFindMin = true)
	{
		global $APPLICATION;
		if ($bFindMin)
		{
			$arPathInfo = pathinfo($strFileName);
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $arPathInfo['dirname'] . "/" . $arPathInfo['filename'] . ".min." . $arPathInfo['extension']) && filesize($_SERVER['DOCUMENT_ROOT'] . $arPathInfo['dirname'] . "/" . $arPathInfo['filename'] . ".min." . $arPathInfo['extension']) > 0)
			{
				$APPLICATION->SetAdditionalCSS($arPathInfo['dirname'] . "/" . $arPathInfo['filename'] . ".min." . $arPathInfo['extension']);
			} else
			{
				$APPLICATION->SetAdditionalCSS($strFileName);
			}
		} else
		{
			$APPLICATION->SetAdditionalCSS($strFileName);
		}
	}

}
