<?php
//<title>Google Merchant Center</title>
/** @global CUser $USER */
/** @global CMain $APPLICATION */
/** @var int $IBLOCK_ID */
/** @var string $SETUP_SERVER_NAME */
/** @var string $SETUP_FILE_NAME */
/** @var array $V */
/** @var string $XML_DATA */
use Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Iblock,
	Bitrix\Catalog;

IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/catalog/export_yandex.php');
set_time_limit(0);

global $USER, $APPLICATION;
$bTmpUserCreated = false;
if (!CCatalog::IsUserExists())
{
    $bTmpUserCreated = true;
    if (isset($USER)) {
        $USER_TMP = $USER;
        unset($USER);
    }
    $USER = new CUser();
}

CCatalogDiscountSave::Disable();
/** @noinspection PhpDeprecationInspection */
CCatalogDiscountCoupon::ClearCoupon();
if ($USER->IsAuthorized()) {
    /** @noinspection PhpDeprecationInspection */
    CCatalogDiscountCoupon::ClearCouponsByManage($USER->GetID());
}

$arYandexFields = array(
//	'typePrefix', 'vendor', 'vendorCode', 'model',
//	'author', 'name', 'publisher', 'series', 'year',
//	'ISBN', 'volume', 'part', 'language', 'binding',
//	'page_extent', 'table_of_contents', 'performed_by', 'performance_type',
//	'storage', 'format', 'recording_length', 'artist', 'title', 'year', 'media',
//	'starring', 'director', 'originalName', 'country', 'aliases',
//	'description', 'sales_notes', 'promo', 'provider', 'tarifplan',
//	'xCategory', 'additional', 'worldRegion', 'region', 'days', 'dataTour',
//	'hotel_stars', 'room', 'meal', 'included', 'transport', 'price_min', 'price_max',
//	'options', 'manufacturer_warranty', 'country_of_origin', 'downloadable', 'adult', 'param',
//	'place', 'hall', 'hall_part', 'is_premiere', 'is_kids', 'date'

    'title', 'description', 'param', 'g:product_type', 'g:google_product_category', 'g:price'
);

$formatList = array(
    'none' => array(
        'vendor', 'vendorCode', 'sales_notes', 'manufacturer_warranty', 'country_of_origin', 'adult'
    ),
);

/**
  * Преобразуем тестовую строку в Utf-8
  *
  * @param string $text -текст
  * @return string $text
  */
function getNiceSubStr($str, $len, $chr = ' ') {
    return mb_substr($str, 0, mb_strpos($str, $chr, $len));
}
function gmcConvertText($text) {
    return iconv("windows-1251", "utf-8", $text );
    //return mb_convert_encoding($text, mb_detect_encoding($text), 'Utf-8');
}

if (!function_exists("yandex_replace_special")) {

    function yandex_replace_special($arg) {
        if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
            return $arg[0];
        else
            return " ";
    }

}

if (!function_exists("yandex_text2xml")) {

    function yandex_text2xml($text, $bHSC = false, $bDblQuote = false) {
        global $APPLICATION;

        $bHSC = (true == $bHSC ? true : false);
        $bDblQuote = (true == $bDblQuote ? true : false);

        if ($bHSC) {
            $text = htmlspecialcharsbx($text);
            if ($bDblQuote)
                $text = str_replace('&quot;', '"', $text);
        }
        $text = preg_replace("/[\x1-\x8\xB-\xC\xE-\x1F]/", "", $text);
        $text = str_replace("'", "&apos;", $text);
        $text = str_replace("&nbsp;", "", $text);
        $text = str_replace("&nbsp;", "", $text);
        //$text = $APPLICATION->ConvertCharset($text, LANG_CHARSET, 'utf-8');
        return $text;
    }

}

if (!function_exists('yandex_get_value')) {

    function yandex_get_value($arOffer, $param, $PROPERTY, $arProperties, $arUserTypeFormat, $usedProtocol) {
        global $iblockServerName;

//        print_r($arProperties);
//        exit();

        $strProperty = '';
        $bParam = (strncmp($param, 'PARAM_', 6) == 0);
        if (isset($arProperties[$PROPERTY]) && !empty($arProperties[$PROPERTY])) {
            $PROPERTY_CODE = $arProperties[$PROPERTY]['CODE'];
            if (!isset($arOffer['PROPERTIES'][$PROPERTY_CODE]) && !isset($arOffer['PROPERTIES'][$PROPERTY]))
                return $strProperty;
            $arProperty = isset($arOffer['PROPERTIES'][$PROPERTY_CODE]) ? $arOffer['PROPERTIES'][$PROPERTY_CODE] : $arOffer['PROPERTIES'][$PROPERTY];

            $value = '';
            $description = '';
            switch ($arProperties[$PROPERTY]['PROPERTY_TYPE']) {
                case 'USER_TYPE':
                    if ($arProperty['MULTIPLE'] == 'Y') {
                        if (!empty($arProperty['~VALUE'])) {
                            $arValues = array();
                            foreach ($arProperty["~VALUE"] as $oneValue) {
                                $isArray = is_array($oneValue);
                                if ( ($isArray && !empty($oneValue)) || (!$isArray && $oneValue != '') ) {
                                    $arValues[] = call_user_func_array($arUserTypeFormat[$PROPERTY], array(
                                        $arProperty,
                                        array("VALUE" => $oneValue),
                                        array('MODE' => 'SIMPLE_TEXT'),
                                            )
                                    );
                                }
                            }
                            $value = implode(', ', $arValues);
                        }
                    } else {
                        $isArray = is_array($arProperty['~VALUE']);
                        if (
                                ($isArray && !empty($arProperty['~VALUE'])) || (!$isArray && $arProperty['~VALUE'] != '')
                        ) {
                            $value = call_user_func_array($arUserTypeFormat[$PROPERTY], array(
                                $arProperty,
                                array("VALUE" => $arProperty["~VALUE"]),
                                array('MODE' => 'SIMPLE_TEXT'),
                                    )
                            );
                        }
                    }
                    break;
                case Iblock\PropertyTable::TYPE_ELEMENT:
                    if (!empty($arProperty['VALUE'])) {
                        $arCheckValue = array();
                        if (!is_array($arProperty['VALUE'])) {
                            $arProperty['VALUE'] = (int) $arProperty['VALUE'];
                            if (0 < $arProperty['VALUE']) {
                                $arCheckValue[] = $arProperty['VALUE'];
                            }
                        } else {
                            foreach ($arProperty['VALUE'] as &$intValue) {
                                $intValue = (int) $intValue;
                                if (0 < $intValue) {
                                    $arCheckValue[] = $intValue;
                                }
                            }
                            if (isset($intValue)) {
                                unset($intValue);
                            }
                        }



                        if (!empty($arCheckValue)) {
                            $dbRes = CIBlockElement::GetList(
                                array(),
                                array('ID' => $arCheckValue),
                                false,
                                false,
                                array('NAME')
                            );
                            while ($arRes = $dbRes->Fetch()) {
                                $value.= ($value ? ', ' : '') . $arRes['NAME'];
                            }
                        }
                    }



                    break;
                case Iblock\PropertyTable::TYPE_SECTION:
                    if (!empty($arProperty['VALUE'])) {
                        $arCheckValue = array();
                        if (!is_array($arProperty['VALUE'])) {
                            $arProperty['VALUE'] = (int) $arProperty['VALUE'];
                            if (0 < $arProperty['VALUE'])
                                $arCheckValue[] = $arProperty['VALUE'];
                        }
                        else {
                            foreach ($arProperty['VALUE'] as &$intValue) {
                                $intValue = (int) $intValue;
                                if (0 < $intValue)
                                    $arCheckValue[] = $intValue;
                            }
                            if (isset($intValue))
                                unset($intValue);
                        }
                        if (!empty($arCheckValue)) {
                            $dbRes = CIBlockSection::GetList(
                                            array(), array('IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID'], 'ID' => $arCheckValue), false, array('NAME')
                            );
                            while ($arRes = $dbRes->Fetch()) {
                                $value .= ($value ? ', ' : '') . $arRes['NAME'];
                            }
                        }
                    }
                    break;
                case Iblock\PropertyTable::TYPE_LIST:
                    if (!empty($arProperty['VALUE'])) {
                        if (is_array($arProperty['VALUE']))
                            $value .= implode(', ', $arProperty['VALUE']);
                        else
                            $value .= $arProperty['VALUE'];
                    }
                    break;
                case Iblock\PropertyTable::TYPE_FILE:
                    if (!empty($arProperty['VALUE'])) {
                        if (is_array($arProperty['VALUE'])) {
                            foreach ($arProperty['VALUE'] as &$intValue) {
                                $intValue = (int) $intValue;
                                if ($intValue > 0) {
                                    if ($ar_file = CFile::GetFileArray($intValue)) {
                                        if (substr($ar_file["SRC"], 0, 1) == "/")
                                            $strFile = $usedProtocol . $iblockServerName . CHTTP::urnEncode($ar_file['SRC'], 'utf-8');
                                        else
                                            $strFile = $ar_file["SRC"];
                                        $value .= ($value ? ', ' : '') . $strFile;
                                    }
                                }
                            }
                            if (isset($intValue))
                                unset($intValue);
                        }
                        else {
                            $arProperty['VALUE'] = (int) $arProperty['VALUE'];
                            if ($arProperty['VALUE'] > 0) {
                                if ($ar_file = CFile::GetFileArray($arProperty['VALUE'])) {
                                    if (substr($ar_file["SRC"], 0, 1) == "/")
                                        $strFile = $usedProtocol . $iblockServerName . CHTTP::urnEncode($ar_file['SRC'], 'utf-8');
                                    else
                                        $strFile = $ar_file["SRC"];
                                    $value = $strFile;
                                }
                            }
                        }
                    }
                    break;
                default:
                    if ($bParam && $arProperty['WITH_DESCRIPTION'] == 'Y') {
                        $description = $arProperty['DESCRIPTION'];
                        $value = $arProperty['VALUE'];
                    } else {
                        $value = is_array($arProperty['VALUE']) ? implode(', ', $arProperty['VALUE']) : $arProperty['VALUE'];
                    }
            }

            // !!!! check multiple properties and properties like CML2_ATTRIBUTES

            if ($bParam) {
                if (is_array($description)) {
                    foreach ($value as $key => $val) {
                        $strProperty.= $strProperty ? "\n" : "";
                        $strProperty.= '<param name="' . yandex_text2xml($description[$key], true) . '">' . yandex_text2xml($val, true) . '</param>';
                    }
                } else {

                    $tagNamesGMC = array(
                        'ATT_BRAND' => 'g:brand', // Производитель
                        'GTIN' => 'g:gtin', // GTIN
                        'GOOGLE_PRODUCT_CAREGORY' => 'g:google_product_category',
                    );

//                    print_r($arProperty['CODE']);
//                    exit();

                    if (isset($tagNamesGMC[$arProperty['CODE']])) {
                        $strProperty.= '<'.$tagNamesGMC[$arProperty['CODE']].'>' . yandex_text2xml($value, true) . '</'.$tagNamesGMC[$arProperty['CODE']].'>';
                    } else {
                        $strProperty.= '<param name="' . yandex_text2xml($arProperty['NAME'], true) . '">' . yandex_text2xml($value, true) . '</param>';
                    }
                }
            } else {
                $param_h = yandex_text2xml($param, true);
                $strProperty .= '<' . $param_h . '>' . yandex_text2xml($value, true) . '</' . $param_h . '>';
            }
        }

        return $strProperty;
    }

}

$arRunErrors = array();

if ($XML_DATA && CheckSerializedData($XML_DATA))
{
    $XML_DATA = unserialize(stripslashes($XML_DATA));
    if (!is_array($XML_DATA)) $XML_DATA = array();
}
if (!is_array($XML_DATA))
    $arRunErrors[] = GetMessage('YANDEX_ERR_BAD_XML_DATA');

$yandexFormat = 'none';
if (isset($XML_DATA['TYPE']) && isset($formatList[$XML_DATA['TYPE']]))
    $yandexFormat = $XML_DATA['TYPE'];

$productFormat = ($yandexFormat != 'none' ? ' type="'.htmlspecialcharsbx($yandexFormat).'"' : '');

$fields = array();
$parametricFields = array();
$fieldsExist = !empty($XML_DATA['XML_DATA']) && is_array($XML_DATA['XML_DATA']);
$parametricFieldsExist = false;
if ($fieldsExist) {
    foreach ($XML_DATA['XML_DATA'] as $key => $value) {
        if ($key == 'PARAMS')
            $parametricFieldsExist = (!empty($value) && is_array($value));
        if (is_array($value))
            continue;
        $value = (string) $value;
        if ($value == '')
            continue;
        $fields[$key] = $value;
    }
    unset($key, $value);
    $fieldsExist = !empty($fields);
}

if ($parametricFieldsExist)
    $parametricFields = $XML_DATA['XML_DATA']['PARAMS'];

$needProperties = !empty($XML_DATA['XML_DATA']) && is_array($XML_DATA['XML_DATA']);

$IBLOCK_ID = (int)$IBLOCK_ID;
$db_iblock = CIBlock::GetByID($IBLOCK_ID);
if (!($ar_iblock = $db_iblock->Fetch()))
{
    $arRunErrors[] = str_replace('#ID#', $IBLOCK_ID, GetMessage('YANDEX_ERR_NO_IBLOCK_FOUND_EXT'));
}
/*elseif (!CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, 'iblock_admin_display'))
{
	$arRunErrors[] = str_replace('#IBLOCK_ID#',$IBLOCK_ID,GetMessage('CET_ERROR_IBLOCK_PERM'));
} */
else
{
    $SETUP_SERVER_NAME = trim($SETUP_SERVER_NAME);

    if (strlen($SETUP_SERVER_NAME) <= 0) {
        if (strlen($ar_iblock['SERVER_NAME']) <= 0) {
            $b = "sort";
            $o = "asc";
            $rsSite = CSite::GetList($b, $o, array("LID" => $ar_iblock["LID"]));
            if ($arSite = $rsSite->Fetch())
                $ar_iblock["SERVER_NAME"] = $arSite["SERVER_NAME"];
            if (strlen($ar_iblock["SERVER_NAME"]) <= 0 && defined("SITE_SERVER_NAME"))
                $ar_iblock["SERVER_NAME"] = SITE_SERVER_NAME;
            if (strlen($ar_iblock["SERVER_NAME"]) <= 0)
                $ar_iblock["SERVER_NAME"] = COption::GetOptionString("main", "server_name", "");
        }
    }
    else {
        $ar_iblock['SERVER_NAME'] = $SETUP_SERVER_NAME;
    }
    $ar_iblock['PROPERTY'] = array();
    $rsProps = CIBlockProperty::GetList(
        array('SORT' => 'ASC', 'NAME' => 'ASC'),
        array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y', 'CHECK_PERMISSIONS' => 'N')
    );
    while ($arProp = $rsProps->Fetch()) {
        $arProp['ID'] = (int) $arProp['ID'];
        $arProp['USER_TYPE'] = (string) $arProp['USER_TYPE'];
        $arProp['CODE'] = (string) $arProp['CODE'];
        $ar_iblock['PROPERTY'][$arProp['ID']] = $arProp;
    }
}

$SETUP_SERVER_NAME = (isset($SETUP_SERVER_NAME) ? trim($SETUP_SERVER_NAME) : '');
$COMPANY_NAME = (isset($COMPANY_NAME) ? trim($COMPANY_NAME) : '');
$SITE_ID = (isset($SITE_ID) ? (string) $SITE_ID : '');
if ($SITE_ID === '')
	$SITE_ID = $ar_iblock['LID'];
$iterator = Main\SiteTable::getList(array(
			'select' => array('LID', 'SERVER_NAME', 'SITE_NAME', 'DIR'),
			'filter' => array('=LID' => $SITE_ID, '=ACTIVE' => 'Y')
		));
$site = $iterator->fetch();
unset($iterator);
if (empty($site))
{
	$arRunErrors[] = GetMessage('BX_CATALOG_EXPORT_YANDEX_ERR_BAD_SITE');
} else
{
	$site['SITE_NAME'] = (string) $site['SITE_NAME'];
	if ($site['SITE_NAME'] === '')
		$site['SITE_NAME'] = (string) Main\Config\Option::get('main', 'site_name');
	$site['COMPANY_NAME'] = $COMPANY_NAME;
	if ($site['COMPANY_NAME'] === '')
		$site['COMPANY_NAME'] = (string) Main\Config\Option::get('main', 'site_name');
	$site['SERVER_NAME'] = (string) $site['SERVER_NAME'];
	if ($SETUP_SERVER_NAME !== '')
		$site['SERVER_NAME'] = $SETUP_SERVER_NAME;
	if ($site['SERVER_NAME'] === '')
	{
		$site['SERVER_NAME'] = (defined('SITE_SERVER_NAME') ? SITE_SERVER_NAME : (string) Main\Config\Option::get('main', 'server_name')
				);
	}
	if ($site['SERVER_NAME'] === '')
	{
		$arRunErrors[] = GetMessage('BX_CATALOG_EXPORT_YANDEX_ERR_BAD_SERVER_NAME');
	}
}

global $iblockServerName;
$iblockServerName = $ar_iblock["SERVER_NAME"];

$arProperties = array();
if (isset($ar_iblock['PROPERTY']))
    $arProperties = $ar_iblock['PROPERTY'];

$boolOffers = false;
$arOffers = false;
$arOfferIBlock = false;
$intOfferIBlockID = 0;
$arSelectOfferProps = array();
$arSelectedPropTypes = array(
    Iblock\PropertyTable::TYPE_STRING,
    Iblock\PropertyTable::TYPE_NUMBER,
    Iblock\PropertyTable::TYPE_LIST,
    Iblock\PropertyTable::TYPE_ELEMENT,
    Iblock\PropertyTable::TYPE_SECTION
);
$arOffersSelectKeys = array(
    YANDEX_SKU_EXPORT_ALL,
    YANDEX_SKU_EXPORT_MIN_PRICE,
    YANDEX_SKU_EXPORT_PROP,
);
$arCondSelectProp = array(
    'ZERO',
    'NONZERO',
    'EQUAL',
    'NONEQUAL',
);
$arPropertyMap = array();
$arSKUExport = array();

$arCatalog = CCatalog::GetByIDExt($IBLOCK_ID);
if (empty($arCatalog))
{
    $arRunErrors[] = str_replace('#ID#', $IBLOCK_ID, GetMessage('YANDEX_ERR_NO_IBLOCK_IS_CATALOG'));
}
else
{
    $arOffers = CCatalogSku::GetInfoByProductIBlock($IBLOCK_ID);
    if (!empty($arOffers['IBLOCK_ID']))
    {
        $intOfferIBlockID = $arOffers['IBLOCK_ID'];
        $rsOfferIBlocks = CIBlock::GetByID($intOfferIBlockID);
        if (($arOfferIBlock = $rsOfferIBlocks->Fetch()))
        {
            $boolOffers = true;
            $rsProps = CIBlockProperty::GetList(
                array('SORT' => 'ASC', 'NAME' => 'ASC'),
                array('IBLOCK_ID' => $intOfferIBlockID, 'ACTIVE' => 'Y', 'CHECK_PERMISSIONS' => 'N')
            );
            while ($arProp = $rsProps->Fetch())
            {
                $arProp['ID'] = (int)$arProp['ID'];
                if ($arOffers['SKU_PROPERTY_ID'] != $arProp['ID'])
                {
                    $arProp['USER_TYPE'] = (string)$arProp['USER_TYPE'];
                    $arProp['CODE'] = (string)$arProp['CODE'];
                    $ar_iblock['OFFERS_PROPERTY'][$arProp['ID']] = $arProp;
                    $arProperties[$arProp['ID']] = $arProp;
                    if (in_array($arProp['PROPERTY_TYPE'], $arSelectedPropTypes))
                            $arSelectOfferProps[] = $arProp['ID'];
                    if ($arProp['CODE'] !== '')
                    {
                        foreach ($ar_iblock['PROPERTY'] as &$arMainProp)
                        {
                            if ($arMainProp['CODE'] == $arProp['CODE'])
                            {
                                $arPropertyMap[$arProp['ID']] = $arMainProp['CODE'];
                                break;
                            }
                        }
                        if (isset($arMainProp))
                            unset($arMainProp);
                    }
                }
            }
            $arOfferIBlock['LID'] = $ar_iblock['LID'];
        }
        else
        {
            $arRunErrors[] = GetMessage('YANDEX_ERR_BAD_OFFERS_IBLOCK_ID');
        }
    }
    if ($boolOffers)
    {
        if (empty($XML_DATA['SKU_EXPORT']))
        {
            $arRunErrors[] = GetMessage('YANDEX_ERR_SKU_SETTINGS_ABSENT');
        }
        else
        {
            $arSKUExport = $XML_DATA['SKU_EXPORT'];;
            if (empty($arSKUExport['SKU_EXPORT_COND']) || !in_array($arSKUExport['SKU_EXPORT_COND'],$arOffersSelectKeys))
            {
                $arRunErrors[] = GetMessage('YANDEX_SKU_EXPORT_ERR_CONDITION_ABSENT');
            }
            if (YANDEX_SKU_EXPORT_PROP == $arSKUExport['SKU_EXPORT_COND'])
            {
                if (empty($arSKUExport['SKU_PROP_COND']) || !is_array($arSKUExport['SKU_PROP_COND']))
                {
                    $arRunErrors[] = GetMessage('YANDEX_SKU_EXPORT_ERR_PROPERTY_ABSENT');
                }
                else
                {
                    if (empty($arSKUExport['SKU_PROP_COND']['PROP_ID']) || !in_array($arSKUExport['SKU_PROP_COND']['PROP_ID'],$arSelectOfferProps))
                    {
                        $arRunErrors[] = GetMessage('YANDEX_SKU_EXPORT_ERR_PROPERTY_ABSENT');
                    }
                    if (empty($arSKUExport['SKU_PROP_COND']['COND']) || !in_array($arSKUExport['SKU_PROP_COND']['COND'],$arCondSelectProp))
                    {
                        $arRunErrors[] = GetMessage('YANDEX_SKU_EXPORT_ERR_PROPERTY_COND_ABSENT');
                    }
                    else
                    {
                        if ($arSKUExport['SKU_PROP_COND']['COND'] == 'EQUAL' || $arSKUExport['SKU_PROP_COND']['COND'] == 'NONEQUAL')
                        {
                            if (empty($arSKUExport['SKU_PROP_COND']['VALUES']))
                            {
                                $arRunErrors[] = GetMessage('YANDEX_SKU_EXPORT_ERR_PROPERTY_VALUES_ABSENT');
                            }
                        }
                    }
                }
            }
        }
    }
}

$arUserTypeFormat = array();
foreach($arProperties as $key => $arProperty)
{
    $arProperty["USER_TYPE"] = (string)$arProperty["USER_TYPE"];
    $arUserTypeFormat[$arProperty["ID"]] = false;
    if ($arProperty["USER_TYPE"] !== '')
    {
        $arUserType = CIBlockProperty::GetUserType($arProperty["USER_TYPE"]);
        if (isset($arUserType["GetPublicViewHTML"]))
        {
            $arUserTypeFormat[$arProperty["ID"]] = $arUserType["GetPublicViewHTML"];
            $arProperties[$key]['PROPERTY_TYPE'] = 'USER_TYPE';
        }
    }
}

$bAllSections = false;
$arSections = array();
if (empty($arRunErrors))
{
    if (is_array($V))
    {
        foreach ($V as $key => $value)
        {
            if (trim($value)=="0")
            {
                $bAllSections = true;
                break;
            }
            $value = (int)$value;
            if ($value > 0)
            {
                $arSections[] = $value;
            }
        }
    }

    if (!$bAllSections && empty($arSections))
    {
        $arRunErrors[] = GetMessage('YANDEX_ERR_NO_SECTION_LIST');
    }
}

if (!empty($XML_DATA['PRICE']))
{
    if ((int)$XML_DATA['PRICE'] > 0)
    {
        $rsCatalogGroups = CCatalogGroup::GetGroupsList(array('CATALOG_GROUP_ID' => $XML_DATA['PRICE'],'GROUP_ID' => 2));
        if (!($arCatalogGroup = $rsCatalogGroups->Fetch()))
        {
            $arRunErrors[] = GetMessage('YANDEX_ERR_BAD_PRICE_TYPE');
        }
    }
    else
    {
        $arRunErrors[] = GetMessage('YANDEX_ERR_BAD_PRICE_TYPE');
    }
}

$usedProtocol = (isset($USE_HTTPS) && $USE_HTTPS == 'Y' ? 'https://' : 'http://');
$filterAvailable = (isset($FILTER_AVAILABLE) && $FILTER_AVAILABLE == 'Y');
$disableReferers = (isset($DISABLE_REFERERS) && $DISABLE_REFERERS == 'Y');

if (strlen($SETUP_FILE_NAME) <= 0) {
    $arRunErrors[] = GetMessage("CATI_NO_SAVE_FILE");
}
elseif (preg_match(BX_CATALOG_FILENAME_REG,$SETUP_FILE_NAME)) {
    $arRunErrors[] = GetMessage("CES_ERROR_BAD_EXPORT_FILENAME");
} else {
    $SETUP_FILE_NAME = Rel2Abs("/", $SETUP_FILE_NAME);
}
if (empty($arRunErrors))
{
/*  if ($GLOBALS["APPLICATION"]->GetFileAccessPermission($SETUP_FILE_NAME) < "W")
    {
        $arRunErrors[] = str_replace('#FILE#', $SETUP_FILE_NAME,GetMessage('YANDEX_ERR_FILE_ACCESS_DENIED'));
    } */
}

$sectionFileName = '';
$itemFileName = '';
if (strlen($SETUP_FILE_NAME) <= 0)
{
	$arRunErrors[] = GetMessage("CATI_NO_SAVE_FILE");
} elseif (preg_match(BX_CATALOG_FILENAME_REG, $SETUP_FILE_NAME))
{
	$arRunErrors[] = GetMessage("CES_ERROR_BAD_EXPORT_FILENAME");
} else
{
	$SETUP_FILE_NAME = Rel2Abs("/", $SETUP_FILE_NAME);
}
if (empty($arRunErrors))
{
	/* 	if ($GLOBALS["APPLICATION"]->GetFileAccessPermission($SETUP_FILE_NAME) < "W")
	  {
	  $arRunErrors[] = str_replace('#FILE#', $SETUP_FILE_NAME,GetMessage('YANDEX_ERR_FILE_ACCESS_DENIED'));
	  } */
	$sectionFileName = $SETUP_FILE_NAME;
	$itemFileName = $SETUP_FILE_NAME . '_items';
}

$itemsFile = null;

if (empty($arRunErrors))
{
	CheckDirPath($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME);

	if (!$fp = @fopen($_SERVER["DOCUMENT_ROOT"] . $sectionFileName, "wb"))
	{
		$arRunErrors[] = str_replace('#FILE#', $sectionFileName, GetMessage('YANDEX_ERR_FILE_OPEN_WRITING'));
	}/* else
	{
		if (!@fwrite($fp, '<? $disableReferers = ' . ($disableReferers ? 'true' : 'false') . ';' . "\n"))
		{
			$arRunErrors[] = str_replace('#FILE#', $sectionFileName, GetMessage('YANDEX_ERR_SETUP_FILE_WRITE'));
			@fclose($fp);
		} else
		{
			if (!$disableReferers)
			{
				fwrite($fp, 'if (!isset($_GET["referer1"]) || strlen($_GET["referer1"])<=0) $_GET["referer1"] = "yandext";' . "\n");
				fwrite($fp, '$strReferer1 = htmlspecialchars($_GET["referer1"]);' . "\n");
				fwrite($fp, 'if (!isset($_GET["referer2"]) || strlen($_GET["referer2"]) <= 0) $_GET["referer2"] = "";' . "\n");
				fwrite($fp, '$strReferer2 = htmlspecialchars($_GET["referer2"]);' . "\n");
			}
		}
	}*/
	}

if (empty($arRunErrors))
{
    /** @noinspection PhpUndefinedVariableInspection */

    fwrite($fp, '<?header("Content-Type: text/xml; charset=utf-8");'."\n");
    fwrite($fp, 'echo "<"."?xml version=\"1.0\" encoding=\"utf-8\"?".">"?>');
    fwrite($fp, "\n".'<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">'."\n");
    fwrite($fp, '<channel>'."\n");
//    fwrite($fp, '<title>'. $APPLICATION->ConvertCharset(htmlspecialcharsbx(COption::GetOptionString('main', 'site_name', '')), LANG_CHARSET, 'utf-8') ."</title>\n");
    fwrite($fp, '<title>' . $APPLICATION->ConvertCharset(htmlspecialcharsbx($site['COMPANY_NAME']), LANG_CHARSET, 'utf-8') . "</title>\n");
    fwrite($fp, "<link>" . $usedProtocol . htmlspecialcharsbx($site['SERVER_NAME']) . "</link>\n");


    //*****************************************//

    //*****************************************//
    $intMaxSectionID = 0;

    $strTmpCat = '';
    $strTmpOff = '';

    $arSectionIDs = array();
    $arAvailGroups = array();
    if (!$bAllSections) {
        for ($i = 0, $intSectionsCount = count($arSections); $i < $intSectionsCount; $i++) {
            $sectionIterator = CIBlockSection::GetNavChain($IBLOCK_ID, $arSections[$i], array('ID', 'IBLOCK_SECTION_ID', 'NAME', 'LEFT_MARGIN', 'RIGHT_MARGIN'));
            $curLEFT_MARGIN = 0;
            $curRIGHT_MARGIN = 0;
            while ($section = $sectionIterator->Fetch()) {
                $section['ID'] = (int) $section['ID'];
                $section['IBLOCK_SECTION_ID'] = (int) $section['IBLOCK_SECTION_ID'];
                if ($arSections[$i] == $section['ID']) {
                    $curLEFT_MARGIN = (int) $section['LEFT_MARGIN'];
                    $curRIGHT_MARGIN = (int) $section['RIGHT_MARGIN'];
                    $arSectionIDs[] = $section['ID'];
                }
                $arAvailGroups[$section['ID']] = array(
                    'ID' => $section['ID'],
                    'IBLOCK_SECTION_ID' => $section['IBLOCK_SECTION_ID'],
                    'NAME' => $section['NAME']
                );
                if ($intMaxSectionID < $section['ID'])
                    $intMaxSectionID = $section['ID'];
            }
            unset($section, $sectionIterator);

            $filter = array("IBLOCK_ID" => $IBLOCK_ID, ">LEFT_MARGIN" => $curLEFT_MARGIN, "<RIGHT_MARGIN" => $curRIGHT_MARGIN, "ACTIVE" => "Y", "IBLOCK_ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");
            $sectionIterator = CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), $filter, false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
            while ($section = $sectionIterator->Fetch()) {
                $section["ID"] = (int) $section["ID"];
                $section["IBLOCK_SECTION_ID"] = (int) $section["IBLOCK_SECTION_ID"];
                $arSectionIDs[] = $section["ID"];
                $arAvailGroups[$section["ID"]] = $section;
                if ($intMaxSectionID < $section["ID"])
                    $intMaxSectionID = $section["ID"];
            }
            unset($section, $sectionIterator);
        }
        if (!empty($arSectionIDs))
            $arSectionIDs = array_unique($arSectionIDs);
    }
    else {
        $filter = array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y", "IBLOCK_ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");
        $sectionIterator = CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), $filter, false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
        while ($section = $sectionIterator->Fetch()) {
            $section["ID"] = (int) $section["ID"];
            $section["IBLOCK_SECTION_ID"] = (int) $section["IBLOCK_SECTION_ID"];
            $arAvailGroups[$section["ID"]] = $section;
            if ($intMaxSectionID < $section["ID"])
                $intMaxSectionID = $section["ID"];
        }
        unset($section, $sectionIterator);

        if (!empty($arAvailGroups))
            $arSectionIDs = array_keys($arAvailGroups);
    }

    foreach ($arAvailGroups as &$value) {
        $strTmpCat.= '<category id="' . $value['ID'] . '"' . ($value['IBLOCK_SECTION_ID'] > 0 ? ' parentId="' . $value['IBLOCK_SECTION_ID'] . '"' : '') . '>' . yandex_text2xml($value['NAME'], true) . '</category>' . "\n";
    }
    if (isset($value))
        unset($value);

    $intMaxSectionID += 100000000;

	fwrite($fp, "<categories>\n");
	fwrite($fp, $strTmpCat);
	fwrite($fp, "</categories>\n");
	//fclose($fp);
	unset($strTmpCat);

    //*****************************************//
    $boolNeedRootSection = false;

    /*CCatalogProduct::setPriceVatIncludeMode(true);
    CCatalogProduct::setUsedCurrency($BASE_CURRENCY);
    CCatalogProduct::setUseDiscount(true);
*/

    //exit($arCatalog['CATALOG_TYPE'].'='.CCatalogSku::TYPE_CATALOG);
    //exit($arCatalog['CATALOG_TYPE'].'='.CCatalogSku::TYPE_OFFERS);

    if ($arCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_FULL || $arCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_CATALOG || $arCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_OFFERS)
    {
        $arSelect = array(
            "ID", "LID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "NAME",
            "PREVIEW_PICTURE", "PREVIEW_TEXT", "PREVIEW_TEXT_TYPE", "DETAIL_PICTURE", "LANG_DIR", "DETAIL_PAGE_URL",
            "CATALOG_AVAILABLE", 'PROPERTY_CML2_ARTICLE'
        );

        $filter = array("IBLOCK_ID" => $IBLOCK_ID);
        if (!$bAllSections && !empty($arSectionIDs))
        {
            $filter["INCLUDE_SUBSECTIONS"] = "Y";
            $filter["SECTION_ID"] = $arSectionIDs;
        }
        $filter["ACTIVE"] = "Y";
        $filter["ACTIVE_DATE"] = "Y";
        if ($filterAvailable)
            $filter['CATALOG_AVAILABLE'] = 'Y';

        $res = CIBlockElement::GetList(array('ID' => 'ASC'), $filter, false, false, $arSelect);

        $total_sum = 0;
        $is_exists = false;
        $cnt = 0;
        while ($obElement = $res->GetNextElement()) {
            $cnt++;
            $arAcc = $obElement->GetFields();

            if ($needProperties) {
                $arAcc["PROPERTIES"] = $obElement->GetProperties();
            }


			if (strpos(strtolower($arAcc["PROPERTIES"]['CML2_ARTICLE']['VALUE']), "(брак)") !== false)
			{
				continue;
			}
			if (strpos(strtolower($arAcc["PROPERTIES"]['CML2_ARTICLE']['VALUE']), "(бу)") !== false)
			{
				continue;
			}
			if (strpos(strtolower($arAcc["PROPERTIES"]['CML2_ARTICLE']['VALUE']), "_бу") !== false)
			{
				continue;
			}

            // $str_AVAILABLE = ' available="'.($arAcc['CATALOG_AVAILABLE'] == 'Y' ? 'true' : 'false').'"';
            $str_AVAILABLE = $arAcc['CATALOG_AVAILABLE'] == 'Y' ? true : false;

            $fullPrice = 0;
            $minPrice = 0;
            $minPriceRUR = 0;
            $minPriceGroup = 0;
            $minPriceCurrency = "";

            if ($XML_DATA['PRICE'] > 0) {
                $rsPrices = CPrice::GetListEx(array(), array(
                    'PRODUCT_ID' => $arAcc['ID'],
                    'CATALOG_GROUP_ID' => $XML_DATA['PRICE'],
                    'CAN_BUY' => 'Y',
                    'GROUP_GROUP_ID' => array(2),
                    '+<=QUANTITY_FROM' => 1,
                    '+>=QUANTITY_TO' => 1,
                ));
                if ($arPrice = $rsPrices->Fetch()) {
                    if ($arOptimalPrice = CCatalogProduct::GetOptimalPrice(
                        $arAcc['ID'], 1, array(2), // anonymous
                        'N', array($arPrice), $ar_iblock['LID'], array()
                    )) {
                        $minPrice = $arOptimalPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
                        $fullPrice = $arOptimalPrice['RESULT_PRICE']['BASE_PRICE'];
                        $minPriceCurrency = $arOptimalPrice['RESULT_PRICE']['CURRENCY'];
                        if ($minPriceCurrency == $RUR)
                            $minPriceRUR = $minPrice;
                        else
                            $minPriceRUR = CCurrencyRates::ConvertCurrency($minPrice, $minPriceCurrency, $RUR);
                        $minPriceGroup = $arOptimalPrice['PRICE']['CATALOG_GROUP_ID'];
                    }
                }
            } else {
                if ($arPrice = CCatalogProduct::GetOptimalPrice(
                    $arAcc['ID'], 1, array(2), // anonymous
                    'N', array(), $ar_iblock['LID'], array()
                )) {
                    $minPrice = $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
                    $fullPrice = $arPrice['RESULT_PRICE']['BASE_PRICE'];
                    $minPriceCurrency = $arPrice['RESULT_PRICE']['CURRENCY'];
                    if ($minPriceCurrency == $RUR)
                        $minPriceRUR = $minPrice;
                    else
                        $minPriceRUR = CCurrencyRates::ConvertCurrency($minPrice, $minPriceCurrency, $RUR);
                    $minPriceGroup = $arPrice['PRICE']['CATALOG_GROUP_ID'];
                }
            }

            if ($minPrice <= 0)
                continue;

            if (strlen($arAcc['DETAIL_PAGE_URL']) <= 0)
                $arAcc['DETAIL_PAGE_URL'] = '/';
            else
                $arAcc['DETAIL_PAGE_URL'] = str_replace(' ', '%20', $arAcc['DETAIL_PAGE_URL']);

            $strTmpOff.= "<item>\n";
            $strTmpOff.= "<g:id>" . $arAcc["ID"] . "</g:id>\n";

            $referer = '';
            //if (!$disableReferers)
/*                $referer = (strpos($arAcc['DETAIL_PAGE_URL'], '?') === false ? '?' : '&amp;') . 'r1=<?=$strReferer1; ?>&amp;r2=<?=$strReferer2; ?>';*/

            $strTmpOff.= "<link>" . $usedProtocol . $ar_iblock['SERVER_NAME'] . htmlspecialcharsbx($arAcc["DETAIL_PAGE_URL"]) . "</link>\n"; // . $referer
            $strTmpOff.= "<g:condition>new</g:condition>\n";

            if ($str_AVAILABLE) {
                $stock = 'in stock';
            } else {
                $stock = 'out of stock';
            }

            $strTmpOff.= "<g:availability>" . $stock . "</g:availability>\n";
            $strTmpOff.= "<g:price>" . $minPrice . " RUB</g:price>\n";

            //$strTmpOff.= "<price>".$minPrice."</price>\n";
//          if ($minPrice < $fullPrice)
//              $strTmpOff.= "<oldprice>".$fullPrice."</oldprice>\n";
//              $strTmpOff.= "<currencyId>".$minPriceCurrency."</currencyId>\n";

            // $strTmpOff.= $strTmpOff_tmp;

            $arAcc["DETAIL_PICTURE"] = (int) $arAcc["DETAIL_PICTURE"];
            $arAcc["PREVIEW_PICTURE"] = (int) $arAcc["PREVIEW_PICTURE"];
            if ($arAcc["DETAIL_PICTURE"] > 0 || $arAcc["PREVIEW_PICTURE"] > 0) {
                $pictNo = ($arAcc["DETAIL_PICTURE"] > 0 ? $arAcc["DETAIL_PICTURE"] : $arAcc["PREVIEW_PICTURE"]);
                if ($ar_file = CFile::GetFileArray($pictNo)) {
                    if (substr($ar_file["SRC"], 0, 1) == "/")
                        $strFile = $usedProtocol . $ar_iblock['SERVER_NAME'] . CHTTP::urnEncode($ar_file["SRC"], 'utf-8');
                    else
                        $strFile = $ar_file["SRC"];
                    $strTmpOff.= "<g:image_link>" . $strFile . "</g:image_link>\n";
                }
            }

            $y = 0;
            foreach ($arYandexFields as $key) {
                switch ($key) {
                    case 'title':
//                        if ($yandexFormat == 'vendor.model' || $yandexFormat == 'artist.title')
//                            continue;

                        $title_text = yandex_text2xml($arAcc["~NAME"], true);
                        $strTmpOff .= "<title>" . $title_text . "</title>\n"; //
                        //$strTmpOff .= "<title>" . yandex_text2xml($arAcc["~NAME"], true) . "</title>\n";
                        break;


                    case 'description':
                        $strTmpOff .=
                                "<description>" . gmcConvertText(yandex_text2xml(TruncateText(
($arAcc["PREVIEW_TEXT_TYPE"] == "html" ?
strip_tags(preg_replace_callback("'&[^;]*;'", "yandex_replace_special",
$arAcc["~PREVIEW_TEXT"])) : preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $arAcc["~PREVIEW_TEXT"])), 255), true)
) .
                                "</description>\n";
                        break;
                }
            }

            $strTmpOff.= "</item>\n";

            /*if (100 <= $cnt) {
                $cnt = 0;
                CCatalogDiscount::ClearDiscountCache(array(
                    'PRODUCT' => true,
                    'SECTIONS' => true,
                    'PROPERTIES' => true
                ));
            }*/
        }
    }


    fwrite($fp, $strTmpOff);
    fwrite($fp, "</channel>\n");
    fwrite($fp, "</rss>\n");

    fclose($fp);
}

CCatalogDiscountSave::Enable();

if (!empty($arRunErrors))
    $strExportErrorMessage = implode('<br>', $arRunErrors);

if ($bTmpUserCreated)
{
    unset($USER);
    if (isset($USER_TMP))
    {
        $USER = $USER_TMP;
        unset($USER_TMP);
    }
}