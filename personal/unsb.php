<?

define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
if ($USER->IsAuthorized() && $USER->GetID() == $_REQUEST['id'])
{
	global $USER;
	$USER->Update($USER->GetID(), array(
		"UF_NOTSEND_BONUS" => 1
	));
	LocalRedirect("/personal/");
}
?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>