<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Подборка предложений");
$APPLICATION->SetPageProperty("title", "Подборка предложений");
$APPLICATION->SetTitle("Подборка предложений");
?>
	<div class="purchase-online-area pt-80 product-style-2">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title text-center">
						<h2 class="title-border">Надо иметь</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<?
					$GLOBALS['arrFilter'] = array(
						"PROPERTY_IS_INESSA_NEED" => "Y",
						"HAS_DETAIL_PICTURE" => "Y",
					);
					?>
					<?
					$APPLICATION->IncludeComponent("bitrix:catalog.top", "mainpage.top", Array(
						"ACTION_VARIABLE" => "action", // Название переменной, в которой передается действие
						"ADD_PICT_PROP" => "-", // Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "Y", // Добавлять в корзину свойства товаров и предложений
						"ADD_TO_BASKET_ACTION" => "ADD", // Показывать кнопку добавления в корзину или покупки
						"BASKET_URL" => "/personal/cart/", // URL, ведущий на страницу с корзиной покупателя
						"CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y", // Учитывать права доступа
						"CACHE_TIME" => "300", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"COMPARE_NAME" => "CATALOG_COMPARE_LIST", // Уникальное имя для списка сравнения
						"COMPATIBLE_MODE" => "Y", // Включить режим совместимости
						"CONVERT_CURRENCY" => "N", // Показывать цены в одной валюте
						"CUSTOM_FILTER" => "", // Фильтр товаров
						"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
						"DISPLAY_COMPARE" => "N", // Разрешить сравнение товаров
						"ELEMENT_COUNT" => "40", // Количество выводимых элементов
						"ELEMENT_SORT_FIELD" => "shows", // По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id", // Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc", // Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc", // Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT", // Выделять товары в списке
						"FILTER_NAME" => "arrFilter", // Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y", // Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y", // Недоступные торговые предложения
						"IBLOCK_ID" => "16", // Инфоблок
						"IBLOCK_TYPE" => "catalog", // Тип инфоблока
						"LABEL_PROP" => "", // Свойство меток товара
						"LINE_ELEMENT_COUNT" => "4", // Количество элементов выводимых в одной строке таблицы
						"MESS_BTN_ADD_TO_BASKET" => "В корзину", // Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить", // Текст кнопки "Купить"
						"MESS_BTN_COMPARE" => "Сравнить", // Текст кнопки "Сравнить"
						"MESS_BTN_DETAIL" => "Подробнее", // Текст кнопки "Подробнее"
						"MESS_NOT_AVAILABLE" => "Нет в наличии", // Сообщение об отсутствии товара
						"OFFERS_CART_PROPERTIES" => "", // Свойства предложений, добавляемые в корзину
						"OFFERS_FIELD_CODE" => array(// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0", // Максимальное количество предложений для показа (0 - все)
						"OFFERS_PROPERTY_CODE" => array(// Свойства предложений
							0 => "",
							1 => "",
						),
						"OFFERS_SORT_FIELD" => "sort", // По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id", // Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc", // Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc", // Порядок второй сортировки предложений товара
						"PARTIAL_PRODUCT_PROPERTIES" => "N", // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(// Тип цены
							0 => "BASE",
							1 => "OLDPRICE",
						),
						"PRICE_VAT_INCLUDE" => "Y", // Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons", // Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N", // Схема отображения
						"PRODUCT_ID_VARIABLE" => "id", // Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPERTIES" => "", // Характеристики товара
						"PRODUCT_PROPS_VARIABLE" => "prop", // Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity", // Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]", // Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "N", // Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE" => array(// Свойства
							0 => "CML2_ARTICLE",
							1 => "BRAND",
							2 => "",
						),
						"PROPERTY_CODE_MOBILE" => array(// Свойства товаров, отображаемые на мобильных устройствах
							0 => "CML2_ARTICLE",
							1 => "BRAND",
						),
						"SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
						"SEF_MODE" => "N", // Включить поддержку ЧПУ
						"SHOW_CLOSE_POPUP" => "N", // Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N", // Показывать процент скидки
						"SHOW_MAX_QUANTITY" => "N", // Показывать остаток товара
						"SHOW_OLD_PRICE" => "N", // Показывать старую цену
						"SHOW_PRICE_COUNT" => "1", // Выводить цены для количества
						"SHOW_SLIDER" => "Y", // Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000", // Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N", // Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue", // Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N", // Отправлять данные электронной торговли в Google и Яндекс
						"USE_PRICE_COUNT" => "N", // Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N", // Разрешить указание количества товара
						"VIEW_MODE" => "SECTION", // Показ элементов
					), false
					);
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="purchase-online-area pt-80 product-style-2">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title text-center">
						<h2 class="title-border">Рекомендовано иметь</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<?
					$GLOBALS['arrFilter'] = array(
						"PROPERTY_IS_INESSA_RECOMENDED" => "Y",
						"HAS_DETAIL_PICTURE" => "Y",
					);
					?>
					<?
					$APPLICATION->IncludeComponent("bitrix:catalog.top", "mainpage.top", Array(
						"ACTION_VARIABLE" => "action", // Название переменной, в которой передается действие
						"ADD_PICT_PROP" => "-", // Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "Y", // Добавлять в корзину свойства товаров и предложений
						"ADD_TO_BASKET_ACTION" => "ADD", // Показывать кнопку добавления в корзину или покупки
						"BASKET_URL" => "/personal/cart/", // URL, ведущий на страницу с корзиной покупателя
						"CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y", // Учитывать права доступа
						"CACHE_TIME" => "300", // Время кеширования (сек.)
						"CACHE_TYPE" => "A", // Тип кеширования
						"COMPARE_NAME" => "CATALOG_COMPARE_LIST", // Уникальное имя для списка сравнения
						"COMPATIBLE_MODE" => "Y", // Включить режим совместимости
						"CONVERT_CURRENCY" => "N", // Показывать цены в одной валюте
						"CUSTOM_FILTER" => "", // Фильтр товаров
						"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
						"DISPLAY_COMPARE" => "N", // Разрешить сравнение товаров
						"ELEMENT_COUNT" => "40", // Количество выводимых элементов
						"ELEMENT_SORT_FIELD" => "shows", // По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id", // Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc", // Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc", // Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT", // Выделять товары в списке
						"FILTER_NAME" => "arrFilter", // Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y", // Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y", // Недоступные торговые предложения
						"IBLOCK_ID" => "16", // Инфоблок
						"IBLOCK_TYPE" => "catalog", // Тип инфоблока
						"LABEL_PROP" => "", // Свойство меток товара
						"LINE_ELEMENT_COUNT" => "4", // Количество элементов выводимых в одной строке таблицы
						"MESS_BTN_ADD_TO_BASKET" => "В корзину", // Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить", // Текст кнопки "Купить"
						"MESS_BTN_COMPARE" => "Сравнить", // Текст кнопки "Сравнить"
						"MESS_BTN_DETAIL" => "Подробнее", // Текст кнопки "Подробнее"
						"MESS_NOT_AVAILABLE" => "Нет в наличии", // Сообщение об отсутствии товара
						"OFFERS_CART_PROPERTIES" => "", // Свойства предложений, добавляемые в корзину
						"OFFERS_FIELD_CODE" => array(// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0", // Максимальное количество предложений для показа (0 - все)
						"OFFERS_PROPERTY_CODE" => array(// Свойства предложений
							0 => "",
							1 => "",
						),
						"OFFERS_SORT_FIELD" => "sort", // По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id", // Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc", // Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc", // Порядок второй сортировки предложений товара
						"PARTIAL_PRODUCT_PROPERTIES" => "N", // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(// Тип цены
							0 => "BASE",
							1 => "OLDPRICE",
						),
						"PRICE_VAT_INCLUDE" => "Y", // Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons", // Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N", // Схема отображения
						"PRODUCT_ID_VARIABLE" => "id", // Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPERTIES" => "", // Характеристики товара
						"PRODUCT_PROPS_VARIABLE" => "prop", // Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity", // Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]", // Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "N", // Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE" => array(// Свойства
							0 => "CML2_ARTICLE",
							1 => "BRAND",
							2 => "",
						),
						"PROPERTY_CODE_MOBILE" => array(// Свойства товаров, отображаемые на мобильных устройствах
							0 => "CML2_ARTICLE",
							1 => "BRAND",
						),
						"SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
						"SEF_MODE" => "N", // Включить поддержку ЧПУ
						"SHOW_CLOSE_POPUP" => "N", // Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N", // Показывать процент скидки
						"SHOW_MAX_QUANTITY" => "N", // Показывать остаток товара
						"SHOW_OLD_PRICE" => "N", // Показывать старую цену
						"SHOW_PRICE_COUNT" => "1", // Выводить цены для количества
						"SHOW_SLIDER" => "Y", // Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000", // Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N", // Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue", // Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N", // Отправлять данные электронной торговли в Google и Яндекс
						"USE_PRICE_COUNT" => "N", // Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N", // Разрешить указание количества товара
						"VIEW_MODE" => "SECTION", // Показ элементов
					), false
					);
					?>
				</div>
			</div>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>