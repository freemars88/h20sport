<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$userName = CUser::GetFullName();
if (!$userName)
	$userName = CUser::GetLogin();
?>
<script>
<? if ($userName): ?>
		BX.localStorage.set("eshop_user_name", "<?= CUtil::JSEscape($userName) ?>", 604800);
<? else: ?>
		BX.localStorage.remove("eshop_user_name");
<? endif ?>

<? if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"]) > 0 && preg_match('#^/\w#', $_REQUEST["backurl"])): ?>
		document.location.href = "<?= CUtil::JSEscape($_REQUEST["backurl"]) ?>";
<? endif ?>
</script>

<?
$APPLICATION->SetTitle("Авторизация");
?>
<div class="login-area pt-80 pb-80">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				<div class="customer-login text-left">
					<h4 class="title-1 title-border text-uppercase mb-30">Вы зарегистрированы и успешно авторизовались.</h4>
					<p><a href="<?= SITE_DIR ?>">Вернуться на главную страницу</a></p>
				</div>					
			</div>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>