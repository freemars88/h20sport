<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
$APPLICATION->RestartBuffer();
if ($_REQUEST['ajaxAction'] == "deleteFromHeaderBasket" && isset($_REQUEST['id'])) {
	$dbBasketItems = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N"));
	while ($arItem = $dbBasketItems->Fetch()) {
		if ($arItem['ID'] == $_REQUEST['id']) {
			CSaleBasket::Delete($arItem['ID']);
			$arRes = array(
				"is_ok" => true,
			);
			echo json_encode($arRes);
		}
	}
	die();
} elseif ($_REQUEST['ajaxAction'] == "addToBasket" && isset($_REQUEST['id'])) {
	$arProductParams = array();
	if (!is_array($_REQUEST['id']) && strlen($_REQUEST['id']) > 0) {
		$arElement = getElementData($_REQUEST['id']);
		if ($arElement['PROPERTIES']['CML2_LINK']['VALUE'] > 0) {
			$arElement = getElementData($arElement['PROPERTIES']['CML2_LINK']['VALUE']);
		}
		if ($arElement['IBLOCK_SECTION_ID'] > 0) {
			$arSection = CIBlockSection::GetByID($arElement['IBLOCK_SECTION_ID'])->GetNext();
		}
		$arProductParams[] = array(
			"NAME" => "Артикул",
			"CODE" => "CML2_ARTICLE",
			"VALUE" => $arElement['PROPERTIES']['CML2_ARTICLE']['VALUE'],
			"SORT" => 100,
		);
		if (intval($_REQUEST['cnt']) > 1) {
			$arProduct = CCatalogProduct::GetByID($_REQUEST['id']);
			if (intval($_REQUEST['cnt']) > $arProduct['QUANTITY']) {
				$_REQUEST['cnt'] = $arProduct['QUANTITY'];
			}
		}
		$res = Add2BasketByProductID(intval($_REQUEST['id']), (intval($_REQUEST['cnt']) > 0 ? intval($_REQUEST['cnt']) : 1), array(), $arProductParams);
		if ($res) {
			$arRes = array(
				"is_ok" => true,
				"title" => "Товар добавлен в корзину!",
				"message" => '<div class="b-popupaddok"><div class="b-popupaddok__title">Товар успешно добавлен в корзину.</div><div class="b-popupaddok__buttons"><a href="/personal/cart/" class="b-popupaddok__button b-popupaddok__button_inbasket">В корзину</a><a href="javascript:$.fancybox.close();" class="b-popupaddok__button b-popupaddok__button_incat">В каталог</a></div></div>',
			);
			echo json_encode($arRes);
		}
	} elseif (is_array($_REQUEST['id'])) {
		$isOk = true;
		foreach ($_REQUEST['id'] as $k => $v) {
			if (intval($v) > 0 && intval($_REQUEST['cnt'][$k]) > 0) {
				$res = Add2BasketByProductID(intval($v), intval($_REQUEST['cnt'][$k]), array(), $arProductParams);
				if (!$res) {
					$isOk = false;
				}
			}
		}
		if ($isOk) {
			$arRes = array(
				"is_ok" => true,
				"title" => "Товары добавлены в корзину!",
				"message" => '<div class="b-popupaddok"><div class="b-popupaddok__title">Товар успешно добавлен в корзину.</div><div class="b-popupaddok__buttons"><a href="/personal/cart/" class="b-popupaddok__button b-popupaddok__button_inbasket">В корзину</a><a href="javascript:$.fancybox.close();" class="b-popupaddok__button b-popupaddok__button_incat">В каталог</a></div></div>',
			);
			echo json_encode($arRes);
		}
	}
	die();
} elseif ($_REQUEST['ajaxAction'] == "updateBasketCnt") {
	unset($_SESSION["SALE_BASKET_NUM_PRODUCTS"]);
	$arRes = array();
	ob_start();
	$APPLICATION->IncludeComponent(
		"bitrix:sale.basket.basket.line", "header", Array(
			"HIDE_ON_BASKET_PAGES" => "N",
			"PATH_TO_AUTHORIZE" => "",
			"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
			"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
			"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
			"PATH_TO_PROFILE" => SITE_DIR . "personal/",
			"PATH_TO_REGISTER" => SITE_DIR . "login/",
			"POSITION_FIXED" => "N",
			"SHOW_AUTHOR" => "N",
			"SHOW_DELAY" => "N",
			"SHOW_EMPTY_VALUES" => "Y",
			"SHOW_IMAGE" => "Y",
			"SHOW_NOTAVAIL" => "N",
			"SHOW_NUM_PRODUCTS" => "Y",
			"SHOW_PERSONAL_LINK" => "N",
			"SHOW_PRICE" => "Y",
			"SHOW_PRODUCTS" => "Y",
			"SHOW_SUMMARY" => "Y",
			"SHOW_TOTAL_PRICE" => "Y",
		)
	);
	$arRes['content'] = ob_get_contents();
	ob_end_clean();
	echo json_encode($arRes);
	die();
} elseif ($_REQUEST['ajaxAction'] == "recheckBasket") {
	$dbBasketItems = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N"));
	while ($arItem = $dbBasketItems->Fetch()) {
		if (isset($_REQUEST['QUANTITY_INPUT'][$arItem['ID']])) {
			CSaleBasket::Update($arItem['ID'], array("QUANTITY" => $_REQUEST['QUANTITY_INPUT'][$arItem['ID']]));
		}
	}
	unset($_REQUEST["BasketOrder"]);
	$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "json", Array(
		"ACTION_VARIABLE" => "basketAction", // Название переменной действия
		"AUTO_CALCULATION" => "Y", // Автопересчет корзины
		"COLUMNS_LIST" => array(// Выводимые колонки
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "WEIGHT",
			3 => "DELETE",
			4 => "DELAY",
			5 => "TYPE",
			6 => "PRICE",
			7 => "QUANTITY",
		),
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N", // Рассчитывать скидку для каждой позиции (на все количество товара)
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "N", // Спрятать поле ввода купона
		"OFFERS_PROPS" => "", // Свойства, влияющие на пересчет корзины
		"PATH_TO_ORDER" => "/personal/order/make/", // Страница оформления заказа
		"PRICE_VAT_SHOW_VALUE" => "N", // Отображать значение НДС
		"QUANTITY_FLOAT" => "N", // Использовать дробное значение количества
		"SET_TITLE" => "Y", // Устанавливать заголовок страницы
		"TEMPLATE_THEME" => "blue", // Цветовая тема
		"USE_GIFTS" => "N", // Показывать блок "Подарки"
		"USE_PREPAYMENT" => "N", // Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
	), false
	);
	die();
} elseif ($_REQUEST['ajaxAction'] == "delBasket") {
	$dbBasketItems = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N", "ID" => intval($_REQUEST['id'])));
	if ($arItem = $dbBasketItems->Fetch()) {
		CSaleBasket::Delete($arItem['ID']);
	}
	unset($_REQUEST["BasketOrder"]);
	$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "json", Array(
		"ACTION_VARIABLE" => "basketAction", // Название переменной действия
		"AUTO_CALCULATION" => "Y", // Автопересчет корзины
		"COLUMNS_LIST" => array(// Выводимые колонки
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "WEIGHT",
			3 => "DELETE",
			4 => "DELAY",
			5 => "TYPE",
			6 => "PRICE",
			7 => "QUANTITY",
		),
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N", // Рассчитывать скидку для каждой позиции (на все количество товара)
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "N", // Спрятать поле ввода купона
		"OFFERS_PROPS" => "", // Свойства, влияющие на пересчет корзины
		"PATH_TO_ORDER" => "/personal/order/make/", // Страница оформления заказа
		"PRICE_VAT_SHOW_VALUE" => "N", // Отображать значение НДС
		"QUANTITY_FLOAT" => "N", // Использовать дробное значение количества
		"SET_TITLE" => "Y", // Устанавливать заголовок страницы
		"TEMPLATE_THEME" => "blue", // Цветовая тема
		"USE_GIFTS" => "N", // Показывать блок "Подарки"
		"USE_PREPAYMENT" => "N", // Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
	), false
	);
	die();
} elseif ($_REQUEST['ajaxAction'] == "addToWishlist" && isset($_REQUEST['id'])) {
	CModule::IncludeModule("webavk.h2osport");
	$bResult = \WebAVK\H2OSport\UserTable::AddToWishList($_REQUEST['id']);
	if ($bResult) {
		$arRes = array(
			"is_ok" => true,
			"title" => "Товар добавлен в Wishlist!",
			"message" => '<div class="b-popupaddok"><div class="b-popupaddok__title">Товар успешно добавлен в список желаний.</div><div class="b-popupaddok__buttons"><a href="/personal/wishlist/" class="b-popupaddok__button b-popupaddok__button_inbasket">В список желаний</a><a href="javascript:$.fancybox.close();" class="b-popupaddok__button b-popupaddok__button_incat">В каталог</a></div></div>',
			//"message" => '<p class="text-center">Товары успешно добавлены в корзину.</p><div class="row"><div class="col-md-45 col-md-offset-10"><a href="/personal/cart/" class="btn btn-default btn-basket-popup">В корзину</a></div><div class="col-md-45 col-md-offset-10"><a href="' . $sectUrl . '" class="btn btn-default">В каталог</a></div></div>'
		);
		echo json_encode($arRes);
	} else {
		$arRes = array(
			"is_ok" => false,
			"title" => "Ошибка добавления товара в Wishlist!",
			//"message" => '<p class="text-center">Товары успешно добавлены в корзину.</p><div class="row"><div class="col-md-45 col-md-offset-10"><a href="/personal/cart/" class="btn btn-default btn-basket-popup">В корзину</a></div><div class="col-md-45 col-md-offset-10"><a href="' . $sectUrl . '" class="btn btn-default">В каталог</a></div></div>'
		);
		echo json_encode($arRes);
	}
	die();
} elseif ($_REQUEST['ajaxAction'] == "deleteFromWishlist" && isset($_REQUEST['id'])) {
	CModule::IncludeModule("webavk.h2osport");
	$bResult = \WebAVK\H2OSport\UserTable::DeleteFromWishList($_REQUEST['id']);
	if ($bResult) {
		$arRes = array(
			"is_ok" => true,
			"title" => "Товар удален из Wishlist!",
			//"message" => '<p class="text-center">Товары успешно добавлены в корзину.</p><div class="row"><div class="col-md-45 col-md-offset-10"><a href="/personal/cart/" class="btn btn-default btn-basket-popup">В корзину</a></div><div class="col-md-45 col-md-offset-10"><a href="' . $sectUrl . '" class="btn btn-default">В каталог</a></div></div>'
		);
		echo json_encode($arRes);
	} else {
		$arRes = array(
			"is_ok" => false,
			"title" => "Ошибка удаления товара из Wishlist!",
			//"message" => '<p class="text-center">Товары успешно добавлены в корзину.</p><div class="row"><div class="col-md-45 col-md-offset-10"><a href="/personal/cart/" class="btn btn-default btn-basket-popup">В корзину</a></div><div class="col-md-45 col-md-offset-10"><a href="' . $sectUrl . '" class="btn btn-default">В каталог</a></div></div>'
		);
		echo json_encode($arRes);
	}
	die();
} elseif ($_REQUEST['ajaxAction'] == "updateWishlistCnt") {
	$arRes = array();
	ob_start();
	$APPLICATION->IncludeComponent(
		"webavk:blank.component", "header.favorites", Array(
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "N",
		)
	);
	$arRes['content'] = ob_get_contents();
	ob_end_clean();
	echo json_encode($arRes);
	die();
} else /* if ($_REQUEST['ajaxAction'] == "getFeedback")
  {
  if ($_SERVER['REQUEST_METHOD'] == "POST")
  {
  $arSendEmails = array(
  "10" => "shop@krasniykarandash.ru, n.libar@krasniykarandash.ru, a.grishanova@krasniykarandash.ru",
  "11" => "e.korzukov@krasniykarandash.ru, a.prokopenkova@krasniykarandash.ru, d.filatov@krasniykarandash.ru",
  "12" => "shop@krasniykarandash.ru, n.libar@krasniykarandash.ru, a.grishanova@krasniykarandash.ru, g.levina@krasniykarandash.ru",
  "13" => "shop@krasniykarandash.ru, n.libar@krasniykarandash.ru, a.grishanova@krasniykarandash.ru",
  "14" => "shop@krasniykarandash.ru, n.libar@krasniykarandash.ru, a.grishanova@krasniykarandash.ru",
  "15" => "o.markelova@krasniykarandash.ru, d.filatov@krasniykarandash.ru",
  );
  $_REQUEST['form_text_16'] = $arSendEmails[$_REQUEST['form_dropdown_SIMPLE_QUESTION_781']];
  $_POST['form_text_16'] = $arSendEmails[$_REQUEST['form_dropdown_SIMPLE_QUESTION_781']];
  $GLOBALS['form_text_16'] = $arSendEmails[$_REQUEST['form_dropdown_SIMPLE_QUESTION_781']];
  }

  $APPLICATION->IncludeComponent("bitrix:form.result.new", "feedback", Array(
  "CACHE_TIME" => "3600", // Время кеширования (сек.)
  "CACHE_TYPE" => "A", // Тип кеширования
  "CHAIN_ITEM_LINK" => "", // Ссылка на дополнительном пункте в навигационной цепочке
  "CHAIN_ITEM_TEXT" => "", // Название дополнительного пункта в навигационной цепочке
  "EDIT_URL" => "", // Страница редактирования результата
  "IGNORE_CUSTOM_TEMPLATE" => "N", // Игнорировать свой шаблон
  "LIST_URL" => "", // Страница со списком результатов
  "SEF_MODE" => "N", // Включить поддержку ЧПУ
  "SUCCESS_URL" => "/ajax/actions.php?ok=yes&ajaxAction=getFeedback", // Страница с сообщением об успешной отправке
  "USE_EXTENDED_ERRORS" => "N", // Использовать расширенный вывод сообщений об ошибках
  "VARIABLE_ALIASES" => array(
  "RESULT_ID" => "RESULT_ID",
  "WEB_FORM_ID" => "WEB_FORM_ID",
  ),
  "WEB_FORM_ID" => "2", // ID веб-формы
  ), false
  );
  die();
  } else */
	if ($_REQUEST['ajaxAction'] == "subscribe" || strlen($_REQUEST['sender_subscription']) > 0) {
		$APPLICATION->RestartBuffer();
		$APPLICATION->IncludeComponent("bitrix:sender.subscribe", "ajax", Array(
			"USE_PERSONALIZATION" => "N", // Определять подписку текущего пользователя
			"CONFIRMATION" => "Y", // Запрашивать подтверждение подписки по email
			"SHOW_HIDDEN" => "N", // Показать скрытые рассылки для подписки
			"AJAX_MODE" => "N", // Включить режим AJAX
			"AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
			"AJAX_OPTION_HISTORY" => "Y", // Включить эмуляцию навигации браузера
			"AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
			"CACHE_TYPE" => "N", // Тип кеширования
			"CACHE_TIME" => "3600", // Время кеширования (сек.)
			"SET_TITLE" => "N", // Устанавливать заголовок страницы
		), false
		);
		die();
	} else /* if ($_REQUEST['ajaxAction'] == "setSwitchCatalog" || strlen($_REQUEST['mode']) > 0)
  {
  $APPLICATION->set_cookie("catSw", $_REQUEST['mode']);
  die();
  } else *//* if ($_REQUEST['ajaxAction'] == "sendProduct")
  {
  $APPLICATION->RestartBuffer();
  if ($_REQUEST['ELEMENT_ID'] > 0 && strlen($_REQUEST['FRIEND_EMAIL']) > 0 && check_email($_REQUEST['FRIEND_EMAIL']) > 0 && strlen($_REQUEST['FRIEND_NAME']) > 0)
  {
  $arProduct = CIBlockElement::GetList(array(), array("ID" => $_REQUEST['ELEMENT_ID']), false, false, array("ID", "NAME", "DETAIL_PAGE_URL"))->GetNext(false, false);
  if ($arProduct)
  {
  $arFields = array(
  "FRIEND_NAME" => $_REQUEST['FRIEND_NAME'],
  "FRIEND_EMAIL" => $_REQUEST['FRIEND_EMAIL'],
  "PRODUCT_LINK" => $arProduct['DETAIL_PAGE_URL'],
  "PRODUCT_NAME" => $arProduct['NAME']
  );
  CEvent::Send("KK_SEND_FRIEND_PRODUCT", array(SITE_ID), $arFields);
  ?>
  <p>Спасибо. Ссылка Вашему другу отправлена</p>
  <?

  die();
  }
  }
  echo "fail";
  die();
  } else */
		if ($_REQUEST['ajaxAction'] == "updateBasketCoupon") {
			$APPLICATION->RestartBuffer();
			$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "action", Array(
				"ACTION_VARIABLE" => "basketAction", // Название переменной действия
				"AUTO_CALCULATION" => "Y", // Автопересчет корзины
				"COLUMNS_LIST" => array(// Выводимые колонки
					0 => "NAME",
					1 => "DISCOUNT",
					2 => "WEIGHT",
					3 => "DELETE",
					4 => "DELAY",
					5 => "TYPE",
					6 => "PRICE",
					7 => "QUANTITY",
				),
				"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N", // Рассчитывать скидку для каждой позиции (на все количество товара)
				"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
				"GIFTS_CONVERT_CURRENCY" => "N",
				"GIFTS_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_HIDE_NOT_AVAILABLE" => "N",
				"GIFTS_MESS_BTN_BUY" => "Выбрать",
				"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
				"GIFTS_PAGE_ELEMENT_COUNT" => "4",
				"GIFTS_PLACE" => "BOTTOM",
				"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
				"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
				"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
				"GIFTS_SHOW_IMAGE" => "Y",
				"GIFTS_SHOW_NAME" => "Y",
				"GIFTS_SHOW_OLD_PRICE" => "N",
				"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
				"HIDE_COUPON" => "N", // Спрятать поле ввода купона
				"OFFERS_PROPS" => "", // Свойства, влияющие на пересчет корзины
				"PATH_TO_ORDER" => "/personal/order/make/", // Страница оформления заказа
				"PRICE_VAT_SHOW_VALUE" => "N", // Отображать значение НДС
				"QUANTITY_FLOAT" => "N", // Использовать дробное значение количества
				"SET_TITLE" => "Y", // Устанавливать заголовок страницы
				"TEMPLATE_THEME" => "blue", // Цветовая тема
				"USE_GIFTS" => "N", // Показывать блок "Подарки"
				"USE_PREPAYMENT" => "N", // Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
			), false
			);
			echo "fail";
			die();
		} elseif ($_REQUEST['ajaxAction'] == "voteItem" && isset($_REQUEST['vote_id'])) {
			ob_start();
			$APPLICATION->IncludeComponent("bitrix:iblock.vote", "single", Array(
				"EXTCLASS" => $_REQUEST['extclass'],
				"CACHE_TIME" => "3600",
				"CACHE_TYPE" => "N",
				"DISPLAY_AS_RATING" => "vote_avg",
				"ELEMENT_CODE" => "",
				"ELEMENT_ID" => $_REQUEST['vote_id'],
				"IBLOCK_ID" => IBLOCK_CATALOG,
				"IBLOCK_TYPE" => "catalog",
				"MAX_VOTE" => "5",
				"MESSAGE_404" => "",
				"SET_STATUS_404" => "N",
				"SHOW_RATING" => "N",
				"VOTE_NAMES" => array(
					0 => "1",
					1 => "2",
					2 => "3",
					3 => "4",
					4 => "5",
					5 => "",
				),
			), false
			);

			$arRes = array(
				"is_ok" => true,
				"html" => ob_get_contents(),
			);
			ob_end_clean();
			echo json_encode($arRes);
			die();
		} elseif ($_REQUEST['ajaxAction'] == "quickView" && isset($_REQUEST['id'])) {
			ob_start();
			$APPLICATION->IncludeComponent("bitrix:catalog.element", "quick.view", Array(
				"ACTION_VARIABLE" => "action", // Название переменной, в которой передается действие
				"ADD_DETAIL_TO_SLIDER" => "N", // Добавлять детальную картинку в слайдер
				"ADD_ELEMENT_CHAIN" => "N", // Включать название элемента в цепочку навигации
				"ADD_PICT_PROP" => "-", // Дополнительная картинка основного товара
				"ADD_PROPERTIES_TO_BASKET" => "N", // Добавлять в корзину свойства товаров и предложений
				"ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
				"ADD_TO_BASKET_ACTION" => array(// Показывать кнопки добавления в корзину и покупки
					0 => "BUY",
				),
				"ADD_TO_BASKET_ACTION_PRIMARY" => "", // Выделять кнопки добавления в корзину и покупки
				"BACKGROUND_IMAGE" => "-", // Установить фоновую картинку для шаблона из свойства
				"BASKET_URL" => "/personal/cart/", // URL, ведущий на страницу с корзиной покупателя
				"BRAND_USE" => "N", // Использовать компонент "Бренды"
				"BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
				"CACHE_GROUPS" => "Y", // Учитывать права доступа
				"CACHE_TIME" => "36000000", // Время кеширования (сек.)
				"CACHE_TYPE" => "A", // Тип кеширования
				"CHECK_SECTION_ID_VARIABLE" => "N", // Использовать код группы из переменной, если не задан раздел элемента
				"COMPATIBLE_MODE" => "Y", // Включить режим совместимости
				"CONVERT_CURRENCY" => "N", // Показывать цены в одной валюте
				"DETAIL_PICTURE_MODE" => array(// Режим показа детальной картинки
					0 => "POPUP",
					1 => "MAGNIFIER",
				),
				"DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
				"DISABLE_INIT_JS_IN_COMPONENT" => "Y", // Не подключать js-библиотеки в компоненте
				"DISPLAY_COMPARE" => "N", // Разрешить сравнение товаров
				"DISPLAY_NAME" => "Y", // Выводить название элемента
				"DISPLAY_PREVIEW_TEXT_MODE" => "E", // Показ описания для анонса
				"ELEMENT_CODE" => "", // Код элемента
				"ELEMENT_ID" => $_REQUEST['id'], // ID элемента
				"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
				"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
				"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
				"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
				"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
				"GIFTS_MESS_BTN_BUY" => "Выбрать",
				"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
				"GIFTS_SHOW_IMAGE" => "Y",
				"GIFTS_SHOW_NAME" => "Y",
				"GIFTS_SHOW_OLD_PRICE" => "Y",
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y", // Недоступные торговые предложения
				"IBLOCK_ID" => "16", // Инфоблок
				"IBLOCK_TYPE" => "catalog", // Тип инфоблока
				"IMAGE_RESOLUTION" => "16by9", // Соотношение сторон изображения товара
				"LABEL_PROP" => "", // Свойство меток товара
				"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#", // URL на страницу, где будет показан список связанных элементов
				"LINK_IBLOCK_ID" => "", // ID инфоблока, элементы которого связаны с текущим элементом
				"LINK_IBLOCK_TYPE" => "", // Тип инфоблока, элементы которого связаны с текущим элементом
				"LINK_PROPERTY_SID" => "", // Свойство, в котором хранится связь
				"MAIN_BLOCK_OFFERS_PROPERTY_CODE" => "", // Свойства предложений, отображаемые в блоке справа от картинки
				"MAIN_BLOCK_PROPERTY_CODE" => "", // Свойства, отображаемые в блоке справа от картинки
				"MESSAGE_404" => "", // Сообщение для показа (по умолчанию из компонента)
				"MESS_BTN_ADD_TO_BASKET" => "В корзину", // Текст кнопки "Добавить в корзину"
				"MESS_BTN_BUY" => "Купить", // Текст кнопки "Купить"
				"MESS_BTN_SUBSCRIBE" => "Подписаться", // Текст кнопки "Уведомить о поступлении"
				"MESS_COMMENTS_TAB" => "Комментарии", // Текст вкладки "Комментарии"
				"MESS_DESCRIPTION_TAB" => "Описание", // Текст вкладки "Описание"
				"MESS_NOT_AVAILABLE" => "Нет в наличии", // Сообщение об отсутствии товара
				"MESS_PRICE_RANGES_TITLE" => "Цены", // Название блока c расширенными ценами
				"MESS_PROPERTIES_TAB" => "Характеристики", // Текст вкладки "Характеристики"
				"META_DESCRIPTION" => "-", // Установить описание страницы из свойства
				"META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
				"OFFERS_CART_PROPERTIES" => array(// Свойства предложений, добавляемые в корзину
					0 => "",
				),
				"OFFERS_FIELD_CODE" => array(// Поля предложений
					0 => "NAME",
					1 => "",
				),
				"OFFERS_LIMIT" => "0", // Максимальное количество предложений для показа (0 - все)
				"OFFERS_PROPERTY_CODE" => array(// Свойства предложений
					0 => "",
					1 => "",
				),
				"OFFERS_SORT_FIELD" => "sort", // По какому полю сортируем предложения товара
				"OFFERS_SORT_FIELD2" => "id", // Порядок второй сортировки предложений товара
				"OFFERS_SORT_ORDER" => "asc", // Порядок сортировки предложений товара
				"OFFERS_SORT_ORDER2" => "desc", // Поле для второй сортировки предложений товара
				"OFFER_ADD_PICT_PROP" => "-", // Дополнительные картинки предложения
				"OFFER_TREE_PROPS" => "", // Свойства для отбора предложений
				"PARTIAL_PRODUCT_PROPERTIES" => "N", // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
				"PRICE_CODE" => array(// Тип цены
					0 => "BASE",
				),
				"PRICE_VAT_INCLUDE" => "Y", // Включать НДС в цену
				"PRICE_VAT_SHOW_VALUE" => "N", // Отображать значение НДС
				"PRODUCT_ID_VARIABLE" => "id", // Название переменной, в которой передается код товара для покупки
				"PRODUCT_INFO_BLOCK_ORDER" => "sku,props", // Порядок отображения блоков информации о товаре
				"PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons", // Порядок отображения блоков покупки товара
				"PRODUCT_PROPERTIES" => array(// Характеристики товара
					0 => "",
				),
				"PRODUCT_PROPS_VARIABLE" => "prop", // Название переменной, в которой передаются характеристики товара
				"PRODUCT_QUANTITY_VARIABLE" => "quantity", // Название переменной, в которой передается количество товара
				"PRODUCT_SUBSCRIPTION" => "Y", // Разрешить оповещения для отсутствующих товаров
				"PROPERTY_CODE" => array(// Свойства
					0 => "CML2_ARTICLE",
					1 => "",
				),
				"SECTION_CODE" => "", // Код раздела
				"SECTION_ID" => "", // ID раздела
				"SECTION_ID_VARIABLE" => "SECTION_ID", // Название переменной, в которой передается код группы
				"SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
				"SEF_MODE" => "N", // Включить поддержку ЧПУ
				"SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
				"SET_CANONICAL_URL" => "N", // Устанавливать канонический URL
				"SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "N", // Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N", // Устанавливать статус 404
				"SET_TITLE" => "N", // Устанавливать заголовок страницы
				"SET_VIEWED_IN_COMPONENT" => "Y", // Включить сохранение информации о просмотре товара для старых шаблонов
				"SHOW_404" => "N", // Показ специальной страницы
				"SHOW_CLOSE_POPUP" => "N", // Показывать кнопку продолжения покупок во всплывающих окнах
				"SHOW_DEACTIVATED" => "N", // Показывать деактивированные товары
				"SHOW_DISCOUNT_PERCENT" => "N", // Показывать процент скидки
				"SHOW_MAX_QUANTITY" => "N", // Показывать остаток товара
				"SHOW_OLD_PRICE" => "Y", // Показывать старую цену
				"SHOW_PRICE_COUNT" => "1", // Выводить цены для количества
				"SHOW_SLIDER" => "N", // Показывать слайдер для товаров
				"STRICT_SECTION_CHECK" => "N", // Строгая проверка раздела для показа элемента
				"TEMPLATE_THEME" => "blue", // Цветовая тема
				"USE_COMMENTS" => "N", // Включить отзывы о товаре
				"USE_ELEMENT_COUNTER" => "Y", // Использовать счетчик просмотров
				"USE_ENHANCED_ECOMMERCE" => "N", // Включить отправку данных в электронную торговлю
				"USE_GIFTS_DETAIL" => "N", // Показывать блок "Подарки" в детальном просмотре
				"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N", // Показывать блок "Товары к подарку" в детальном просмотре
				"USE_MAIN_ELEMENT_SECTION" => "N", // Использовать основной раздел для показа элемента
				"USE_PRICE_COUNT" => "N", // Использовать вывод цен с диапазонами
				"USE_PRODUCT_QUANTITY" => "Y", // Разрешить указание количества товара
				"USE_RATIO_IN_RANGES" => "N", // Учитывать коэффициенты для диапазонов цен
				"USE_VOTE_RATING" => "N", // Включить рейтинг товара
				"VOTE_DISPLAY_AS_RATING" => "vote_avg",
			), false
			);

			$arRes = array(
				"is_ok" => true,
				"html" => ob_get_contents(),
			);
			ob_end_clean();
			echo json_encode($arRes);
			die();
		} elseif ($_REQUEST['ajaxAction'] == "oneClickBuy" && $_REQUEST['id'] > 0 && strlen($_REQUEST['phone']) > 0 && strlen($_REQUEST['fio']) > 0) {
			$dbBasketItems = CSaleBasket::GetList(array(), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N", "ID" => intval($_REQUEST['id'])));
			if ($arItem = $dbBasketItems->Fetch()) {
				CSaleBasket::Delete($arItem['ID']);
			}
			$arProductParams = array();
			$res = Add2BasketByProductID(intval($_REQUEST['id']), 1, array(), $arProductParams);
			if ($res) {
				$bLogout = false;
				if (!$USER->IsAuthorized()) {
					$USER->Authorize(NOREG_USERID);
					$bLogout = true;
				}

				$arFields = array(// параметры заказа
					"LID" => LANG,
					"PERSON_TYPE_ID" => 1,
					"PAYED" => "N",
					"CANCELED" => "N",
					"STATUS_ID" => "N",
					"CURRENCY" => "RUB",
					"USER_ID" => $USER->GetID(),
					"DATE_CANCELED" => false,
				);

				$arFields["PRICE_DELIVERY"] = 0;
				$totalPrice = 0;
				$arFields["PRICE"] = $totalPrice;
				$order = \Bitrix\Sale\Order::create(SITE_ID, $arFields['USER_ID'], "RUB");
				$order->setField('STATUS_ID', \Bitrix\Sale\OrderStatus::getInitialStatus());
				$order->setPersonTypeId(1);
				$propertyCollection = $order->getPropertyCollection();
				$apost = array(
					"PROPERTIES" => array(
						1 => $_REQUEST['fio'],
						3 => $_REQUEST['phone'],
						2 => $USER->GetEmail(),
					),
				);
				$propertyCollection->setValuesFromPost($apost, array());
				$basket = Bitrix\Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), SITE_ID);
				$result = $basket->refreshData(array('PRICE', 'QUANTITY', 'COUPONS'));
				if ($result->isSuccess()) {
					$basket = $basket->getOrderableItems();
					$order->setBasket($basket);
				}
				/*
				  $shipmentCollection = $order->getShipmentCollection();
				  $shipment = $shipmentCollection->createItem();
				  $shipmentItemCollection = $shipment->getShipmentItemCollection();
				  $shipment->setField('CURRENCY', $order->getCurrency());
				  foreach ($order->getBasket() as $item)
				  {
				  $shipmentItem = $shipmentItemCollection->createItem($item);
				  $shipmentItem->setQuantity($item->getQuantity());
				  }

				  $arDeliveryServiceAll = Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList($shipment, Bitrix\Sale\Delivery\Restrictions\Manager::MODE_MANAGER);
				  if (!empty($arDeliveryServiceAll))
				  {
				  if (array_key_exists($deliveryId, $arDeliveryServiceAll))
				  {
				  $deliveryObj = $arDeliveryServiceAll[$deliveryId];
				  } else
				  {
				  $deliveryObj = reset($arDeliveryServiceAll);
				  }
				  if ($deliveryObj->isProfile())
				  {
				  $name = $deliveryObj->getNameWithParent();
				  } else
				  {
				  $name = $deliveryObj->getName();
				  }

				  $shipment->setFields(array(
				  'DELIVERY_ID' => $deliveryObj->getId(),
				  'DELIVERY_NAME' => $name,
				  'CURRENCY' => $order->getCurrency()
				  ));

				  $res = $shipmentCollection->calculateDelivery();
				  } else
				  {
				  $service = Bitrix\Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
				  $shipment->setFields(array(
				  'DELIVERY_ID' => $service['ID'],
				  'DELIVERY_NAME' => $service['NAME'],
				  'CURRENCY' => $order->getCurrency()
				  ));
				  }
				 */
				/*
				  //	$order->setFields($arFieldsa);
				  $oPaymentCollection = $order->getPaymentCollection();
				  $oPayment = $oPaymentCollection->createItem();
				  $oPayment->setField("SUM", $arFieldsa["PRICE"]);
				  $oPayment->setFields(array(
				  "PAY_SYSTEM_ID" => 1,
				  "PAY_SYSTEM_NAME" => "Наличными при получении заказа"
				  ));
				 *
				 */
				$oResult = $order->save();
				$ORDER_ID = false;
				if ($oResult->isSuccess()) {
					$ORDER_ID = $oResult->getId();
				}

				/*
				  $arEventFields = array(
				  "ORDER_DATA" => $message,
				  "DELIVERY_FORMATED" => $deliveryName,
				  "TOTAL_SUM" => $totalPrice,
				  "ORDER_TABLE" => $orderInfoToMessage,
				  "ORDER_DATE" => $orderData,
				  "ORDER_USER" => $name,
				  "ORDER_ID" => $ORDER_ID,
				  "EMAIL" => $email,
				  "SALE_EMAIL" => $admin,
				  "SUPER_THEME" => $superTheme
				  );
				  if ($regMail == "Y")
				  {
				  $USER->Logout();
				  }
				  $_SESSION['OLD_ONE_CLICK'] = $tovarID;
				 *
				 */
				//if (CEvent::Send("SALE_NEW_ORDER", LANG, $arEventFields)) {
				//SendSMS($ORDER_ID, "N");
				//}
				if ($bLogout) {
					$USER->Logout();
				}
				if ($ORDER_ID > 0) {
					$arRes = array(
						"is_ok" => true,
						"title" => '<div id="oneclick-result"><p>Заказ успешно создан. <br/>В ближайшее время наш менеджер свяжется с вами <br/>для подтверждения заказа</p>',
					);
					echo json_encode($arRes);
				} else {
					$arRes = array(
						"is_ok" => false,
						"title" => "Ошибка создания заказа",
						//"message" => '<p class="text-center">Товары успешно добавлены в корзину.</p><div class="row"><div class="col-md-45 col-md-offset-10"><a href="/personal/cart/" class="btn btn-default btn-basket-popup">В корзину</a></div><div class="col-md-45 col-md-offset-10"><a href="' . $sectUrl . '" class="btn btn-default">В каталог</a></div></div>'
					);
					echo json_encode($arRes);
				}
			}
			die();
		} elseif ($_REQUEST['ajaxAction'] == "addToBasketComplect" && $_REQUEST['product-id'] > 0) {
			$arElement = getElementData($_REQUEST['product-id']);
			$arProducts = array();
			foreach ($arElement['PROPERTIES']['SET_ELEMENTS']['VALUE'] as $k => $v) {
				$arProducts[$_REQUEST['SET'][$v]] = $_REQUEST['SET'][$v] . "=" . $arElement['PROPERTIES']['SET_ELEMENTS']['DESCRIPTION'][$k];
			}
			ksort($arProducts, SORT_NUMERIC);
			$strIdent = implode("/", $arProducts);
			$arComplect = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_SET_ALL, "PROPERTY_LINK_SET_CATALOG" => $_REQUEST['product-id'], "PROPERTY_SYS_IDENT" => $strIdent), false, false, array("ID"))->Fetch();
			if ($arComplect) {
				$arProductParams = array();
				$res = Add2BasketByProductID($arComplect['ID'], 1, array(), $arProductParams);
				if ($res) {
					$arRes = array(
						"is_ok" => true,
						"title" => "Комплект добавлен в корзину!",
						"message" => '<div class="row"><div class="col-md-12">Комплект успешно добавлен в корзину.</div></div><div class="row"><div class="col-md-12 text-right"><a href="/personal/cart/" class="btn btn-default btn-basket-popup">В корзину</a><a href="javascript:$.fancybox.close();" class="btn btn-default">В каталог</a></div></div>',
					);
					echo json_encode($arRes);
				}
			}
		}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
